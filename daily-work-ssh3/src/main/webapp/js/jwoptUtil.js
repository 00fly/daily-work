var opt = $.extend({},opt);
/* 定义全局对象 */

/***
 * 定义操作的添加
 */


opt.add = function(type) {
	if (type == 'notice') { //通知添加
		$('#noticelist_addNoticeId').show();
		$('#nlfrm').form('clear');
		$('#noId').html(0);
		$("span#nltitleId").html("添加公告");
		$('#noticelist_addNoticeId').dialog({
			title: '添加公告',
			width: $(window).width() * 0.9,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="content"]', {
				resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="content"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('notice');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#noticelist_addNoticeId').dialog('close');
		}
		}]
		});
	} else if (type == 'tranPostion') {   //调岗
		$('#tranPostion_tpdg').show();
		$('#tpfrm').form('clear');
		$('#tpId').val(0);
		$('#tranPostion_tpdg').dialog({
			title: '调职申请',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="applyReason"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="applyReason"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('tranPostion');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#tranPostion_tpdg').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});

		/*
		$('#departTPTreeId').combotree({   //旧部门
			url: '/qx/org!queryOrgComTree?orgCode=1',
			required: true,
			onClick: function(node) {
			var posurl = "/qx/position!queryPositionByCode?orgCode="+node.id;
			$("#positioOldId").combobox({ 
				url:posurl,
				textField: "text",
				valueField: "id",
				multiple: false,
				panelHeight: "auto",
				onChange: function(oldVal, newVal) {
				var userurl = "/qx/user!queryUserByPosCode?posCode="+oldVal;
				$("#userId").combobox({
					url:userurl,
					textField: "text",
					valueField: "id",
					multiple: false,
					panelHeight: "auto",
				});
			}
			});
		},
		onLoadSuccess: function(row, data) {
			var t = $(this);
			if (data) {
				$(data).each(function(index, d) {
					if (this.state == 'closed') {
						t.tree('expandAll');
					}
				});
			}
		}
		});

		 */
		//调往信息
		$('#departNewTPTreeId').combotree({   //新部门
			url: '/qx/org!queryOrgComTree?orgCode=1',
			required: true,
			onClick: function(node) {
			var posurl = "/qx/position!queryPositionByCode?orgCode="+node.id;
			$("#positioNewId").combobox({ 
				url:posurl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				onLoadSuccess: function(row1, data) {
				var data = $('#positioNewId').combobox('getData');
				if (data.length > 0) {
					$('#positioNewId').combobox('select', data[0].id);
				} 
			}
			});
		},
		onLoadSuccess: function(row, data) {
			var t = $(this);
			if (data) {
				$(data).each(function(index, d) {
					if (this.state == 'closed') {
						t.tree('expandAll');
					}
				});
			}
		}
		});

	}else if(type=='position'){
		if($("#orgCodeId").html()==''){
			$.messager.alert('提示','请选择职位所属的组织机构!','info');
		}else{
			$('#position_dg').dialog('open').dialog('setTitle','添加');
			$("#titleId").html("添加职位");
			$('#posfm').form('clear');
			$("#posId").html("");
		}
	}else if(type=='leaveApply'){
		$('#leaveApply_ladg').show();
		$('#lafrm').form('clear');
		$('#laId').val(0);
		$('#leaveApply_ladg').dialog({
			title: '请假申请',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
				editor = KindEditor.create('textarea[name="leaveReson"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="leaveReson"]');
			},
			buttons: [{
				text: '保存',
				handler: function() {
				opt.save('leaveApply');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#leaveApply_ladg').dialog('close');
			}
			}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});
		/*
		$('#departLATreeId').combotree({   //部门部门
			url: '/qx/org!queryOrgComTree?orgCode=1',
			required: true,
			onClick: function(node) {
			var posurl = "/qx/position!queryPositionByCode?orgCode="+node.id;
			$("#positionId").combobox({ 
				url:posurl,
				textField: "text",
				valueField: "id",
				multiple: false,
				panelHeight: "auto",
				onChange: function(oldVal, newVal) {
				var userurl = "/qx/user!queryUserByPosCode?posCode="+oldVal;
				$("#userLaId").combobox({
					url:userurl,
					textField: "text",
					valueField: "id",
					multiple: false,
					panelHeight: "auto",
				});
			}
			});
		},
		onLoadSuccess: function(row, data) {
			var t = $(this);
			if (data) {
				$(data).each(function(index, d) {
					if (this.state == 'closed') {
						t.tree('expandAll');
					}
				});
			}
		}
		});

		 */
		//请假性质
		var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_LEAVE_PROPERTY";
		$("#laPryId").combobox({ 
			url:configUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			panelHeight: "auto",
			onLoadSuccess: function () { 
			var data = $('#laPryId').combobox('getData');
			if (data.length > 0) {
				$('#laPryId').combobox('select', data[0].id);
			} 
		}
		});
	}else if(type=='process'){   //流程
		$('#process_prodg').show();
		$('#profrm').form('clear');
		$('#proId').val(0);
		$("#isAbleId").attr("checked","checked");
		//$("span#nltitleId").html("添加公告");
		$('#process_prodg').dialog({
			title: '添加流程',
			width: $(window).width() * 0.45,
			height: $(window).height() * 0.75,
			padding: 5,
			modal: true,
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('process');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#process_prodg').dialog('close');
			}
			}]
		});

		var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=SYS_PROCEE_BUSI";
		$("#busiFunId").combobox({
			url:configUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
		});

	}else if(type=='procesNode'){
		$('#processNode_pnodg').show();
		$('#pnfrm').form('clear');
		$('#pnId').val(0);
		$("#renId").attr("checked","checked");
		$("#notStartId").attr("checked","checked");
		//$("span#nltitleId").html("添加公告");
		$('#processNode_pnodg').dialog({
			title: '添加流程节点',
			width: $(window).width() * 0.45,
			height: $(window).height() * 0.85,
			padding: 5,
			modal: true,
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('procesNode');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#processNode_pnodg').dialog('close');
			}
			}]
		});

		$("#sortId").numberspinner({
			min: 1,
			max: 100,
			required:true,
			editable: false
		});
		//节点所属流程
		var prourl = "/qx/process!queryProcessByALL";
		$("input[name='pn.processId']").combobox({
			url:prourl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			panelHeight: "auto",
			onChange: function(oldVal, newVal) {
			var userurl = "/qx/processNode!queryProcessNodeById?processId="+oldVal;
			$("#nodeIdNextId").combobox({
				url:userurl,
				textField: "text",
				valueField: "id",
				multiple: true,
				panelHeight: "auto"
			});
		}
		});
	}/*else if(type=='workPlan'){
		$('#workPlan_addwpId').show();
		$('#wpfrm').form('clear');
		$('#wpId').html(0);
		$('#workPlan_addwpId').dialog({
			title: '添加工作计划',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.90,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="planContent"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="planContent"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('workPlan');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#workPlan_addwpId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");
			} 
		}
		});

		//计划年份
		var wpYearUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_WORKPLAN_YEAR";
		$("#wpyearId").combobox({ 
			url:wpYearUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			panelHeight: "auto",
		});

		//计划月份
		var wpMonthUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_WORKPLAN_MONTH";
		$("#wpMonthId").combobox({ 
			url:wpMonthUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			panelHeight: "auto",
		});

		//计划状态
		var wpStatusUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_WORKPLAN_STATUS";
		$("#wpStatusId").combobox({ 
			url:wpStatusUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			panelHeight: "auto",
			editable:false,
			onLoadSuccess: function () { 
			var data = $('#wpStatusId').combobox('getData');
			if (data.length > 0) {
				$('#wpStatusId').combobox('select', data[0].id);
			} 
		}
		});

		//发送者
		var departUserUrl = "/qx/user!queryDepartUser";
		$("#wpdepartUserId").combobox({ 
			url:departUserUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			onLoadSuccess: function () { 
			var data = $('#wpdepartUserId').combobox('getData');
			if (data.length > 0) {
				$('#wpdepartUserId').combobox('select', data[0].id);
			} 
		}
		});


		//年、月
		var date=new Date;
		var year=date.getFullYear(); 
		var month=date.getMonth()+1;

		$("#wpyearId").combobox('setValue',year);
		$("#wpMonthId").combobox('setValue',month);

		var wpCode = (new Date().getTime()+Math.random(100));
		$("#wpCodeId").html(wpCode);

		datagrid = $('#dgId').datagrid({
			url : '/qx/workItem!queryWorkItemList?wpCode='+wpCode,
			title : '工作项',
			idField:'id',
			singleSelect:true,
			columns:[[
			          {field:'id',width:80,hidden:'true',title:'Item ID'},
			          {field:'planId',width:80,hidden:'true',title:'Item ID'},
			          {field:'xulei',width:30,title:'序号',align:'center',},
			          {field:'workItem',width:700,
			        	  editor:{type : 'validatebox',
			        	  options : {
			        	  required : true
			          }},
			          title:'项内容'},
			          {field:'directorCofirm',width:60,align:'center',title:'主管确认',hidden:'true'},
			          {field:'employeeConfirm',width:60,align:'center',title:'员工确认',hidden:'true'}
			          ]],
			          toolbar : [ {
			        	  text : '增加',
			        	  iconCls : 'icon-add',
			        	  handler : function() {
			        	  add();
			          }
			          }, '-', {
			        	  text : '删除',
			        	  iconCls : 'icon-remove',
			        	  handler : function() {
			        	  del();
			          }
			          }, '-', {
			        	  text : '修改',
			        	  iconCls : 'icon-edit',
			        	  handler : function() {
			        	  edit();
			          }
			          }, '-', {
			        	  text : '保存',
			        	  iconCls : 'icon-save',
			        	  handler : function() {
			        	  if (editRow != undefined) {
			        		  datagrid.datagrid('endEdit', editRow);
			        	  }
			          }
			          }, '-', {
			        	  text : '取消编辑',
			        	  iconCls : 'icon-undo',
			        	  handler : function() {
			        	  datagrid.datagrid('unselectAll');
			        	  datagrid.datagrid('rejectChanges');
			        	  editRow = undefined;
			          }
			          }, '-', {
			        	  text : '取消选中',
			        	  iconCls : 'icon-undo',
			        	  handler : function() {
			        	  datagrid.datagrid('unselectAll');
			          }
			          }, '-' ],
			        onLoadSuccess: function(){
						editRow = undefined;
		            },
					onDblClickRow : function(rowIndex, rowData) {
					if (editRow != undefined) {
						datagrid.datagrid('endEdit', editRow);
						$(".datagrid-editable-input").height(25);
					}

					if (editRow == undefined) {
						datagrid.datagrid('beginEdit', rowIndex);
						editRow = rowIndex;
						datagrid.datagrid('unselectAll');
						$(".datagrid-editable-input").height(25);
					}
				},
		onAfterEdit : function(rowIndex, rowData, changes) {
			var inserted = datagrid.datagrid('getChanges', 'inserted');
			var updated = datagrid.datagrid('getChanges', 'updated');
			if (inserted.length < 1 && updated.length < 1) {
				editRow = undefined;
				datagrid.datagrid('unselectAll');
				return;
			}

			var url = '';
			if (inserted.length > 0) {
				url = '/qx/workItem!saveWorkItem?wpCode='+wpCode;
			}
			if (updated.length > 0) {
				url = '/qx/workItem!updateWorkItem';
			}

			$.ajax({
				url : url,
				data : rowData,
				success : function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result==1) {
					datagrid.datagrid('acceptChanges');
					$.messager.show({
						msg : obj.msg,
						title : '提示'
					});
					editRow = undefined;
					datagrid.datagrid('reload');
				} else {
					datagrid.datagrid('beginEdit', editRow);
					$.messager.alert('提示', obj.msg, 'error');
				}
				datagrid.datagrid('unselectAll');
			}
			});
		},
		onRowContextMenu : function(e, rowIndex, rowData) {
			e.preventDefault();
			$(this).datagrid('unselectAll');
			$(this).datagrid('selectRow', rowIndex);
			$('#menu').menu('show', {
				left : e.pageX,
				top : e.pageY
			});
		}
		});

	}*/
	else if(type=='remind'){
		$('#remind_addRId').show();
		$('#remindfrm').form('clear');
		$('#rId').html(0);
		$('#remind_addRId').dialog({
			title: '添加备忘录',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.90,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="remindContent"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="remindContent"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('remind');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#remind_addRId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");
			} 
		}
		});

		//提示方式
		var remindWayUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_REMIND_WAY";
		$("#remindWayId").combobox({ 
			url:remindWayUrl,
			textField: "text",
			valueField: "id",
			required:true,
			multiple: false,
			editable:false,
			panelHeight: "auto",
			onLoadSuccess: function () { 
			var data = $('#remindWayId').combobox('getData');
			if (data.length > 0) {
				$('#remindWayId').combobox('select', data[0].id);
			} 
		} 
		});

		var d = new Date();
		var minute = 0;
		if(d.getMinutes()<59){
			minute = d.getMinutes() +1;
		}else{
			minute = 59;
		}

		var tstr = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
		$("#remindDateId").datetimebox('setValue', tstr);

		var str = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+minute+":"+d.getSeconds();
		$("#finishDateId").datetimebox('setValue', str);	
	}else if(type=='trainingApply'){
		$('#training_addTId').show();
		$('#trainingfrm').form('clear');
		$('#taId').html(0);
		$('#training_addTId').dialog({
			title: '添加培训',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="trainingContent"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="trainingContent"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('trainingApply');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#training_addTId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");
			} 
		}
		});

		//加载培训的组员
		var userUrl = "/qx/user!queryAllUserByOrgCodeToTran";
		$.ajax({
			type: "POST",
			url: userUrl,
			success: function(data){
			var obj = eval('('+data+')');
			if (obj.result==1){
				var userHtml = "";
				if(obj.datas.length==0){
					userHtml="<div style='color:red;position:absolute;top:50%;left:45%'>没有你要找的数据!</div>"
				}else{
					for ( var i = 0; i < obj.datas.length; i++) {
						if(obj.datas[i].sign==1){
							userHtml = userHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='posUsers' checked='checked' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
						}else{
							userHtml = userHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='posUsers' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
						}

						if(i%6==0&&i!=0){
							userHtml = userHtml+ "<br/><br/>"
						}
					}
				}
				$("#userTransDiv").html(userHtml);
			} else {
				$.messager.show({    // show error message
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='recruitmentApply'){
		$('#recruitment_addTId').show();
		$('#recruitmentfrm').form('clear');
		$('#raId').html(0);
		$('#recruitment_addTId').dialog({
			title: '添加招聘',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.90,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="requirementDesc"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="requirementDesc"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('recruitmentApply');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#recruitment_addTId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});

	}else if(type=='custiomServiceRec'){
		$('#custiomService_addRId').show();
		$('#custiomServicefrm').form('clear');
		$('#cId').html(0);
		$('#custiomService_addRId').dialog({
			title: '添加客服工单',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.90,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="serviceDesc"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="serviceDesc"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('custiomServiceRec');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#custiomService_addRId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});

		var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_SERVICE_TYPE";
		$(".serviceTypeId").combobox({ 
			url:configUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			panelHeight: "auto",
			onLoadSuccess: function () { 
			var data = $('.serviceTypeId').combobox('getData');
			if (data.length > 0) {
				$('.serviceTypeId').combobox('select', data[0].id);
			} 
		}
		});
	}else if(type=='documentApply'){
		$('#documentApply_addRId').show();
		$('#documentApplyfrm').form('clear');
		$('#docId').html(0);
		$('#documentApply_addRId').dialog({
			title: '添加制度发布',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.90,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="content"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="content"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('documentApply');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#documentApply_addRId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});
	}else if(type=='projectManageMent'){
		$('#project_addwpId').show();
		$('#projectfrm').form('clear');
		$('#projectId').html(0);
		$('#project_addwpId').dialog({
			title: '添加项目',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.90,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="projectContent"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="projectContent"]');
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('projectManageMent');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#project_addwpId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});
	}else if(type=='personalThingManage'){
		$('#personalThing_addwpId').show();
		$('#ptmfrm').form('clear');
		$('#ptmId').html(0);
		$('#personalThing_addwpId').dialog({
			title: '添加个人事务',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.90,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="thingContent"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="thingContent"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('personalThingManage');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#personalThing_addwpId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});
	}else if(type=='workLog'){
		$('#workLog_addwpId').show();
		$('#workLogfrm').form('clear');
		$('#workLogId').html(0);
		$('#workLog_addwpId').dialog({
			title: '添加工作日志',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.90,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="logContent"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="logContent"]');
		},
		buttons: [{
			text: '保存',
			handler: function() {
			opt.save('workLog');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#workLog_addwpId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});
	}else if(type=='eventReport'){
		$('#eventReport_addwpId').show();
		$('#eventReportfrm').form('clear');
		$('#eventReportId').html(0);
		$('#eventReport_addwpId').dialog({
			title: '添加事项',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.90,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="eventContent"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="eventContent"]');
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('eventReport');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#eventReport_addwpId').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});


		//接收方式
		var posurl = "/qx/config!queryConfigByDomainCode?dataDomain=SYS_RECEIVE_WAY";
		$("#receiveWayId").combobox({ 
			url:posurl,
			textField: "text",
			valueField: "id",
			multiple: false,
			onLoadSuccess: function () { 
			var data = $('#receiveWayId').combobox('getData');
			if (data.length > 0) {
				$('#receiveWayId').combobox('select', data[0].id);
			} 
		}
		});

		//事项类型
		var typeurl = "/qx/config!queryConfigByDomainCode?dataDomain=SYS_EVEN_TYPE";
		$("#evenTypeId").combobox({ 
			url:typeurl,
			textField: "text",
			valueField: "id",
			multiple: false,
			onLoadSuccess: function () { 
			var data = $('#evenTypeId').combobox('getData');
			if (data.length > 0) {
				$('#evenTypeId').combobox('select', data[0].id);
			} 
		}
		});


		//行政接收人
		var userurl = "/qx/user!queryUsersByDepartCode?departCode=XZ";
		$("#adminReceiveUserId").combobox({ 
			url:userurl,
			textField: "text",
			valueField: "id",
			multiple: false,
			groupField:'group',
			onLoadSuccess: function () { 
			var data = $('#adminReceiveUserId').combobox('getData');
			if (data.length > 0) {
				$('#adminReceiveUserId').combobox('select', data[0].id);
			} 
		}
		});

	}else if(type=='appeal'){  //考勤申诉
		$('#appealSub').form('clear');
		$('#addAppealId').show();
		$('#addAppealId').dialog({
			title: '考勤申请、更正',
			width: 700,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor3 = KindEditor.create('textarea[name="complainContent"]', {
				themeType : 'qq',
				items : [
				         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
				         ],
				         resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="complainContent"]');
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('appeal');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#addAppealId').dialog('close');   
		}
		}]
		});
		$('#appealSub').form('clear');

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");

				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");

				//职位
				$("#positionNameId").val(obj.positionName);
				$("#positionNameId").attr('readonly','readonly');
				$("#positionNameId").addClass("bgcolor");

			} 
		}
		});
	}else if (type == 'materialPurchase') { //物资请购添加
		$('#materialPurchase_ladg').show();
		$('#lafrm').form('clear');
		$('#laId').val(0);
		var mpCode = (new Date().getTime());
		$("#mpCodeId").html(mpCode);
		$('#materialPurchase_ladg').dialog({
			title: '物资请购申请',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor = KindEditor.create('textarea[name="applyReason"]', {
				themeType: 'qq',
				items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
				resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="applyReason"]');
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('materialPurchase');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#materialPurchase_ladg').dialog('close');
		}
		}]
		});

		//创建明细列表
		var editRow = undefined;
		var datagrid;

		datagrid = $('#edg').datagrid({
			title: '物资请购列表',
			singleSelect: true,
			url: '/qx/materialPurchaseDetail!queryMaterialPurchasesByPage?mpCode=' + mpCode,
			idField: 'id',
			onClickRow: onClickRow,
			columns: [[{
				field: 'id',
				width: 80,
				hidden: 'true',
				title: 'Item ID'
			},
			{
				field: 'materialName',
				width: 100,
				editor: {
				type: 'validatebox',
				options: {
				required: true
			}
			},
			title: '物品名称'
			},
			{
				field: 'materialType',
				width: 80,
				align: 'center',
				title: '类型',
				editor: 'text',
			},
			{
				field: 'specification',
				width: 80,
				align: 'center',
				editor: 'text',
				title: '规格'
			},
			{
				field: 'unit',
				width: 50,
				align: 'center',
				editor: 'text',
				title: '单位'
			},
			{
				field: 'count',
				width: 50,
				align: 'right',
				editor: 'numberbox',
				title: '数量'
			},
			{
				field: 'barCode',
				width: 150,
				editor: 'text',
				title: '条形码'
			},
			{
				field: 'memo',
				width: 150,
				align: 'center',
				editor: 'text',
				title: '备注'
			}]],
			onDblClickRow: function(rowIndex, rowData) {
			if (editRow != undefined) {
				datagrid.datagrid('endEdit', editRow);
			}

			if (editRow == undefined) {
				datagrid.datagrid('beginEdit', rowIndex);
				editRow = rowIndex;
				datagrid.datagrid('unselectAll');
			}
			$(".datagrid-editable-input").height(22);
		},
		onAfterEdit: function(rowIndex, rowData, changes) {
			var inserted = datagrid.datagrid('getChanges', 'inserted');
			var updated = datagrid.datagrid('getChanges', 'updated');
			if (inserted.length < 1 && updated.length < 1) {
				editRow = undefined;
				datagrid.datagrid('unselectAll');
				return;
			}

			var url = '';
			if (inserted.length > 0) {
				url = '/qx/materialPurchaseDetail!saveMaterialPurchasesByPage?mpCode=' + mpCode;
			}
			if (updated.length > 0) {
				url = '/qx/materialPurchaseDetail!updateMaterialPurchaseDetail?mpCode=' + row.mpCode;;
			}

			$.ajax({
				url: url,
				data: rowData,
				success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					datagrid.datagrid('acceptChanges');
					$.messager.show({
						msg: obj.msg,
						title: '提示'
					});
					editRow = undefined;
					datagrid.datagrid('reload');
				} else {
					datagrid.datagrid('beginEdit', editRow);
					$.messager.alert('提示', obj.msg, 'error');
				}
				datagrid.datagrid('unselectAll');
			}
			});

		},
		onRowContextMenu: function(e, rowIndex, rowData) {
			e.preventDefault();
			$(this).datagrid('unselectAll');
			$(this).datagrid('selectRow', rowIndex);
			$('#menu').menu('show', {
				left: e.pageX,
				top: e.pageY
			});
		},
		toolbar: [{
			text: '添加',
			iconCls: 'icon-add',
			handler: function() {
			append();
		}
		},
		'-', {
			text: '删除',
			iconCls: 'icon-remove',
			handler: function() {
			removeit();
		}
		},
		'-', {
			text: '保存',
			iconCls: 'icon-save',
			handler: function() {
			accept();
		}
		},
		'-', {
			iconCls: 'icon-undo',
			text: '撤销',
			handler: function() {
			reject();
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/materialPurchase!loadMaterialPurchaseToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});
	}else if(type == 'carApply'){   //车辆申请
		$('#carApply_ladg').show();
		$('#lafrm').form('clear');
		$('#laId').val(0);
		$('#carApply_ladg').dialog({
			title: '车辆申请',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor0 = KindEditor.create('textarea[name="applyReason"]', {
				themeType: 'qq',
				items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
				resizeType: 1
			});
			editor1 = KindEditor.create('textarea[name="requirement"]', {
				themeType: 'qq',
				items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
				resizeType: 1
			});
			/**
                editor2 = KindEditor.create('textarea[name="memo"]', {
                    themeType: 'qq',
                    items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
                    resizeType: 1
                });
			 ***/
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="applyReason"]');
			KindEditor.remove('textarea[name="requirement"]');
			//KindEditor.remove('textarea[name="memo"]');
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('carApply');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#carApply_ladg').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/carApply!loadCarApplyToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});

	}else if (type == 'materialApply') { //物资请购添加
		$('#materialApply_ladg').show();
		$('#lafrm').form('clear');
		$('#laId').val(0);
		$('#materialApply_ladg').dialog({
			title: '物资领用申请',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor0 = KindEditor.create('textarea[name="applyReason"]', {
				themeType: 'qq',
				items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
				resizeType: 1
			});
			/*
                editor1 = KindEditor.create('textarea[name="memo"]', {
                    themeType: 'qq',
                    items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
                    resizeType: 1
                });
			 */
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="applyReason"]');
			//KindEditor.remove('textarea[name="memo"]');
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('materialApply');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#materialApply_ladg').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/materialApply!loadMaterialApplyToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});

		var wpCode = (new Date().getTime()+Math.random(100));
		$("#wpCodeId").html(wpCode);

		mpgrid = $('#dgId').datagrid({
			url : '/qx/materialApplyDetail!queryMaterialPurchasesByPage?mpCode='+wpCode,
			title : '物资领用',
			idField:'id',
			singleSelect:true,
			columns: [[{
				field: 'id',
				width: 80,
				hidden: 'true',
				title: 'Item ID'
			},
			{
				field: 'materialName',
				width: 100,
				editor: {
				type: 'validatebox',
				options: {
				required: true
			}
			},
			title: '物品名称'
			},
			{
				field: 'materialType',
				width: 80,
				align: 'center',
				title: '类型',
				editor: 'text',
			},
			{
				field: 'specification',
				width: 80,
				align: 'center',
				editor: 'text',
				title: '规格'
			},
			{
				field: 'unit',
				width: 50,
				align: 'center',
				editor: 'text',
				title: '单位'
			},
			{
				field: 'count',
				width: 50,
				align: 'right',
				editor: 'numberbox',
				title: '数量'
			},
			{
				field: 'barCode',
				width: 150,
				editor: 'text',
				title: '条形码'
			},
			{
				field: 'memo',
				width: 150,
				align: 'center',
				editor: 'text',
				title: '备注'
			}]],
			toolbar : [ {
				text : '增加',
				iconCls : 'icon-add',
				handler : function() {
				add();
			}
			}, '-', {
				text : '删除',
				iconCls : 'icon-remove',
				handler : function() {
				del();
			}
			}, '-', {
				text : '修改',
				iconCls : 'icon-edit',
				handler : function() {
				edit();
			}
			}, '-', {
				text : '保存',
				iconCls : 'icon-save',
				handler : function() {
				if (editRow != undefined) {
					mpgrid.datagrid('endEdit', editRow);
				}
			}
			}, '-', {
				text : '取消编辑',
				iconCls : 'icon-undo',
				handler : function() {
				mpgrid.datagrid('unselectAll');
				mpgrid.datagrid('rejectChanges');
				editRow = undefined;
			}
			}, '-', {
				text : '取消选中',
				iconCls : 'icon-undo',
				handler : function() {
				mpgrid.datagrid('unselectAll');
			}
			}, '-' ],
			onDblClickRow : function(rowIndex, rowData) {
			if (editRow != undefined) {
				mpgrid.datagrid('endEdit', editRow);
				$(".datagrid-editable-input").height(25);
			}

			if (editRow == undefined) {
				mpgrid.datagrid('beginEdit', rowIndex);
				editRow = rowIndex;
				mpgrid.datagrid('unselectAll');
				$(".datagrid-editable-input").height(25);
			}
		},
		onAfterEdit : function(rowIndex, rowData, changes) {
			var inserted = mpgrid.datagrid('getChanges', 'inserted');
			var updated = mpgrid.datagrid('getChanges', 'updated');
			if (inserted.length < 1 && updated.length < 1) {
				editRow = undefined;
				mpgrid.datagrid('unselectAll');
				return;
			}

			var url = '';
			if (inserted.length > 0) {
				url = '/qx/materialApplyDetail!saveMaterialApplysByPage?mpCode='+wpCode;
			}
			if (updated.length > 0) {
				url = '/qx/materialApplyDetail!updateMaterialApplyDetail';
			}

			$.ajax({
				url : url,
				data : rowData,
				success : function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result==1) {
					mpgrid.datagrid('acceptChanges');
					$.messager.show({
						msg : obj.msg,
						title : '提示'
					});
					editRow = undefined;
					mpgrid.datagrid('reload');
				} else {
					mpgrid.datagrid('beginEdit', editRow);
					$.messager.alert('提示', obj.msg, 'error');
				}
				mpgrid.datagrid('unselectAll');
			}
			});
		},
		onRowContextMenu : function(e, rowIndex, rowData) {
			e.preventDefault();
			$(this).datagrid('unselectAll');
			$(this).datagrid('selectRow', rowIndex);
			$('#menu').menu('show', {
				left : e.pageX,
				top : e.pageY
			});
		}
		});

	}else if (type == 'meetingApply') { //会议申请添加
		$('#meetingApply_ladg').show();
		$('#lafrm').form('clear');
		$('#laId').val(0);
		$('#meetingApply_ladg').dialog({
			title: '会议申请',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor1 = KindEditor.create('textarea[name="meetingContent"]', {
				themeType: 'qq',
				items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
				resizeType: 1
			});
			/**
                editor2 = KindEditor.create('textarea[name="attendees"]', {
                    themeType: 'qq',
                    items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
                    resizeType: 1
                });
			 */
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="meetingContent"]');
			//KindEditor.remove('textarea[name="attendees"]');
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('meetingApply');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#meetingApply_ladg').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/meetingApply!loadMeetingApplyToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});

		//会议室
		var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_MEETING_ROOM";
		$("#meetingRoom").combobox({ 
			url:configUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			panelHeight: "auto",
			onLoadSuccess: function () { 
			var data = $('#meetingRoom').combobox('getData');
			if (data.length > 0) {
				$('#meetingRoom').combobox('select', data[0].id);
			} 
		}
		});
	}else if (type == 'marketingManagement') { //营销管理
		$('#marketingManagement_ladg').show();
		$('#lafrm').form('clear');
		$('#laId').val(0);
		$('#marketingManagement_ladg').dialog({
			title: '营销管理',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor0 = KindEditor.create('textarea[name="content"]', {
				uploadJson: '../kindeditor/jsp/upload_json.jsp',
				fileManagerJson: '../kindeditor/jsp/file_manager_json.jsp',
				allowFileManager: true
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="content"]');
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('marketingManagement');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#marketingManagement_ladg').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/marketingManagement!loadMarketingManagementToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});

		//营销类型
		var typeUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_MARKETINGMAN_TYPE";
		$("#marketingInfoType").combobox({
			url: typeUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			panelHeight: "auto",
			onLoadSuccess: function() {
			var data = $('#marketingInfoType').combobox('getData');
			if (data.length > 0) {
				$('#marketingInfoType').combobox('select', data[0].id);
			}
		}
		});

		//部门选择
		var typeUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_MARKETINGMAN_TYPE";
		$("#marketingInfoType").combobox({
			url: typeUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			panelHeight: "auto",
			onLoadSuccess: function() {
			var data = $('#marketingInfoType').combobox('getData');
			if (data.length > 0) {
				$('#marketingInfoType').combobox('select', data[0].id);
			}
		}
		});

	}else if(type=='video'){   //视频管理
		$('#videoSub').form('clear');
		$('#addVideoId').show();
		$('#addVideoId').dialog({
			title: '添加视频',
			width: 600,
			height: 400,
			padding: 5,
			modal: true,
			onOpen: function() {
		},
		onBeforeClose: function() {
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('video');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#addVideoId').dialog('close');   
		}
		}]
		});
		$('#VideoSub').form('clear');

		//加载用户信息
		$.ajax({
			url: "/qx/leaveApply!loadLeaveApplyToUser",
			data: {
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$("#userNameId").val(obj.userName);
				$("#userNameId").attr('readonly','readonly');
				$("#userNameId").addClass("bgcolor");


				//部门
				$("#departNameId").val(obj.departName);
				$("#departNameId").attr('readonly','readonly');
				$("#departNameId").addClass("bgcolor");


			} 
		}
		});
	}else if (type == 'specialAccountMan') {
		$('#specialAccountMan_ladg').show();
		$('#spafrm').form('clear');
		$('#spaId').val(0);
		$("#isAbleId").attr("checked","checked");
		$('#specialAccountMan_ladg').dialog({
			title: '特殊账号添加',
			width: $(window).width() * 0.5,
			height: $(window).height() * 0.75,
			padding: 5,
			modal: true,
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('specialAccountMan');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#specialAccountMan_ladg').dialog('close');
			}
			}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/specialAccountMan!loadSpecialAccountManToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});

		//所属部门
		$('#departNewTPTreeId').combotree({   //新部门
			url: '/qx/org!queryOrgComTree?orgCode=1',
			required: true,
			onClick: function(node) {
			var posurl = "/qx/user!queryUserByOrgCode?orgCode="+node.id;
			$("#userId").combobox({ 
				url:posurl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				onLoadSuccess: function() {
				var data = $('#userId').combobox('getData');
				if (data.length > 0) {
					$('#userId').combobox('select', data[0].id);
				}
			}
			});
		},
		onLoadSuccess: function(row, data) {
			var t = $(this);
			if (data) {
				$(data).each(function(index, d) {
					if (this.state == 'closed') {
						t.tree('expandAll');
					}
				});
			}
		}
		});
	}else if(type=="importPapersMan"){
		$('#importPapersMan_addRId').show();
		$('#importPapersManfrm').form('clear');
		$('#impPaperId').val(0);
		$('#importPapersMan_addRId').dialog({
			title: '重要文件添加',
			width: $(window).width() * 0.5,
			height: $(window).height() * 0.55,
			padding: 5,
			modal: true,
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('importPapersMan');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#importPapersMan_addRId').dialog('close');
			}
			}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/specialAccountMan!loadSpecialAccountManToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly', 'readonly');
					$("#departNameId").addClass("bgcolor");

				}
			}
		});
	}else if (type == 'importantPapersApply') {
		$('#importantPapersApply_ladg').show();
		$('#lafrm').form('clear');
		$('#laId').val(0);
		$('#importantPapersApply_ladg').dialog({
			title: '重要文件浏览申请',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor0 = KindEditor.create('textarea[name="applyReason"]', {
				themeType: 'qq',
				items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
				resizeType: 1
			});
		},
		onBeforeClose: function() {
			KindEditor.remove('textarea[name="applyReason"]');
		},
		buttons: [{
			text: '提交',
			handler: function() {
			opt.save('importantPapersApply');
		}
		},
		{
			text: '取消',
			handler: function() {
			$('#importantPapersApply_ladg').dialog('close');
		}
		}]
		});

		//加载用户信息
		$.ajax({
			url: "/qx/importantPapersApply!loadImportantPapersApplyToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});

	}else if(type=="damageReport"){
		$('#damageReport_addRId').show();
		$('#damageReportfrm').form('clear');
		$('#drId').val(0);
		$('#damageReport_addRId').dialog({
			title: '添加损坏报告申请',
			width: $(window).width() * 0.8,
			height: $(window).height() * 0.95,
			padding: 5,
			modal: true,
			onOpen: function() {
			editor0 = KindEditor.create('textarea[name="reasonDesc"]', {
				themeType: 'qq',
				items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
				resizeType: 1
			});
		},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="reasonDesc"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('damageReport');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#damageReport_addRId').dialog('close');
			}
			}]
		});
		

		//加载用户信息
		$.ajax({
			url: "/qx/importantPapersApply!loadImportantPapersApplyToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});
		
		
		var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_DAMAGE_TYPE";
		$("#damageTypeId").combobox({ 
			url:configUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			panelHeight: "auto",
			onLoadSuccess: function () { 
			var data = $('#damageTypeId').combobox('getData');
			if (data.length > 0) {
				$('#damageTypeId').combobox('select', data[0].id);
			} 
		}
		});
		
		//物资类别
		var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_MATERIAL_TYPE";
		$("#materialTypeId").combobox({ 
			url:configUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			panelHeight: "auto",
			onLoadSuccess: function () { 
			var data = $('#materialTypeId').combobox('getData');
			if (data.length > 0) {
				$('#materialTypeId').combobox('select', data[0].id);
			} 
		}
		});
		
		
		//责任认定
		var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_RESPONSIBILITY_CONFIRM";
		$("#responsibilityConfirmId").combobox({ 
			url:configUrl,
			textField: "text",
			valueField: "id",
			multiple: false,
			editable:false,
			panelHeight: "auto",
			onLoadSuccess: function () { 
			var data = $('#responsibilityConfirmId').combobox('getData');
			if (data.length > 0) {
				$('#responsibilityConfirmId').combobox('select', data[0].id);
			} 
		}
		});
		
		
		//支付费用
		$("#companyId").attr("checked","checked");
	}

}

/***
 * 定义操作的保存
 */
opt.save = function(type) {
	if (type == 'notice') {
		//判断值是否为合法
		var flag = false;
		$('#nlfrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
		}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法

		var contentHtml = editor.html();
		var id = $('#noId').html();
		url = "/qx/notice!saveNotice";
		var title = $('#titleId').val();
		var memo = $('#memoId').val();
		$.ajax({
			url: url,
			data: {
			title: title,
			memo: memo,
			contentHtml: contentHtml,
			id: id
		},
		/*
			$('#nlfrm').form('submit',{
				url: url,
				onSubmit: function(){
				return $(this).form('validate');
			},*/
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#noticelist_addNoticeId').dialog('close');
				$('#noticelist_noticegrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	} else if (type == 'tranPostion') {
		var applyReason = editor.html();
		$("#applyReasonValId").val(applyReason);
		var url = "/qx/transferposition!saveTransferPosition?applyReason="+applyReason;
		$('#tpfrm').form('submit', {
			type:'post',
			url: url,
			onSubmit: function() {
			return $(this).form('validate');
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#tranPostion_tpdg').dialog('close');
				$('#tranPostion_tpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='leaveApply'){
		var leaveReson = editor.html();
		$("#leaveResonValId").val(leaveReson);
		var url = "/qx/leaveApply!saveLeaveApply";
		$('#lafrm').form('submit', {
			url: url,
			type: 'POST',
			onSubmit: function() {
			return $(this).form('validate');
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#leaveApply_ladg').dialog('close');
				$('#leaveApply_lagrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='process'){
		var url = "/qx/process!saveProcess";
		$('#profrm').form('submit', {
			url: url,
			onSubmit: function() {
			return $(this).form('validate');
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#process_prodg').dialog('close');
				$('#process_progrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='procesNode'){
		var processId = $("input[name='pn.processId']").val();
		var nodeType = $("input[name='pn.startNode']:checked").val();
		$.ajax({
			url: "/qx/processNode!checkStartProcessNode",
			data: {
			id:$("#pnId").val(),
			processId:processId,
			nodeType: nodeType
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				var pnurl = "/qx/processNode!saveProcessNode";
				$('#pnfrm').form('submit', {
					url: pnurl,
					onSubmit: function() {
					return $(this).form('validate');
				},
				success: function(data) {
					var obj = eval('(' + data + ')');
					if (obj.result == 1) {
						$('#processNode_pnodg').dialog('close');
						$('#processNode_pngrid').datagrid("reload");
						$.messager.show({
							title: '提示',
							msg: obj.errorMsg
						});
					} else {
						$.messager.show({
							title: '提示',
							msg: obj.errorMsg
						});
					}
				}
				});
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='workPlan'){
		var planContent = editor.html();
		var id = $('#wpId').html();
		url = "/qx/workPlan!saveWorkPlan";
		var status = $("input[name='status']").val();
		var year = $("input[name='year']").val();
		var month = $("input[name='month']").val();
		var title = $("#titleId").val();
		var wpCode = $("#wpCodeId").html();
		var departUser = $("input[name='departUser']").val(); 
		$.ajax({
			url: url,
			data: {
			year: year,
			month: month,
			status: status,
			planContent: planContent,
			id: id,
			title:title,
			wpCode:wpCode,
			departUser:departUser
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#workPlan_addwpId').dialog('close');
				$('#workPlan_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});

	}else if(type=='remind'){
		var remindContent = editor.html();
		var reContext = editor.text();
		var id = $('#rId').html();
		url = "/qx/remind!saveRemind";
		var finishDate = $("input[name='finishDate']").val();
		var remindWay = $("input[name='remindWay']").val();
		var remindDate = $("input[name='remindDate']").val();
		$.ajax({
			url: url,
			data: {
			remindWay: remindWay,
			finishDate: finishDate,
			remindContent: remindContent,
			reContext:reContext,
			remindDate:remindDate,
			id: id
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#remind_addRId').dialog('close');
				$('#remind_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='trainingApply'){
		var trainingRequirement = editor.html();
		var id = $('#taId').html();
		var checks = $(".posUsers"); //class='posUsers'
		var memo = $("#memoId").val();
		var title = $("#titleId").val();
		var objarray = checks.length;
		var strBuffer = "";
		for (i = 0; i < objarray; i++) {
			if (checks[i].checked == true) {
				modelid = checks[i].value;
				strBuffer += modelid;
				strBuffer += "#";
			}
		}
		var surl = "/qx/trainingApply!saveTrainingApply";
		$.ajax({
			url: surl,
			data: {
			id:id,
			trainingRequirement: trainingRequirement,
			userBuffer: strBuffer,
			title:title,
			memo:memo
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#training_addTId').dialog('close');
				$('#training_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});

	}else if(type=='recruitmentApply'){
		var requirementDesc = editor.html();
		var id = $('#raId').html();
		var memo = $("#memoId").val();
		var applyDate = $("input[name='applyDate']").val();
		var surl = "/qx/recruitmentApply!saveRecruitmentApply";
		$.ajax({
			url: surl,
			data: {
			id:id,
			requirementDesc: requirementDesc,
			memo:memo,
			applyDate:applyDate
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#recruitment_addTId').dialog('close');
				$('#recruitment_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});

	}else if(type=='custiomServiceRec'){
		var serviceDesc = editor.html();
		var id = $('#cId').html();
		var memo = $("#memoId").val();
		var serviceType = $("input[name='serviceType']").val();
		var surl = "/qx/custiomServiceRec!saveCustiomServiceRec";
		$.ajax({
			url: surl,
			data: {
			id:id,
			serviceDesc: serviceDesc,
			memo:memo,
			serviceType:serviceType
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#custiomService_addRId').dialog('close');
				$('#custiomService_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='documentApply'){
		//判断值是否为合法
		var flag = false;
		$('#documentApplyfrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
		}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法

		var content = editor.html();
		var id = $('#docId').html();
		var memo = $("#memoId").val();
		var docTitle = $("input[name='docTitle']").val();
		var surl = "/qx/documentApply!saveDocumentApply";
		$.ajax({
			url: surl,
			data: {
			id:id,
			docTitle: docTitle,
			memo:memo,
			content:content
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#documentApply_addRId').dialog('close');
				$('#documentApply_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='projectManageMent'){
		//判断值是否为合法
		var flag = false;
		$('#projectfrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
		}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法

		var projectContent = editor.html();
		var id = $('#projectId').html();
		var memo = $("#memoId").val();
		var projectName = $("#projectNameId").val();
		var surl = "/qx/projectManageMent!saveProjectManageMent";
		$.ajax({
			url: surl,
			data: {
			id:id,
			projectName: projectName,
			memo:memo,
			projectContent:projectContent
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#project_addwpId').dialog('close');
				$('#project_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='personalThingManage'){
		//判断值是否为合法
		var flag = false;
		$('#ptmfrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
		}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法

		var thingContent = editor.html();
		var id = $('#ptmId').html();
		var memo = $("#memoId").val();
		var thingName = $("#thingNameId").val();
		var surl = "/qx/personalThingManage!savePersonalThingManage";
		$.ajax({
			url: surl,
			data: {
			id:id,
			thingName: thingName,
			memo:memo,
			thingContent:thingContent
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#personalThing_addwpId').dialog('close');
				$('#personalThing_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='workLog'){
		//判断值是否为合法
		var flag = false;
		$('#workLogfrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
		}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法

		var logContent = editor.html();
		var id = $('#workLogId').html();
		var memo = $("#memoId").val();
		var workLogDate = $("#workLogDateId").datetimebox('getValue');	;
		var surl = "/qx/workLog!saveWorkLog";
		$.ajax({
			url: surl,
			data: {
			id:id,
			workLogDate: workLogDate,
			memo:memo,
			logContent:logContent
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#workLog_addwpId').dialog('close');
				$('#workLog_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='eventReport'){
		//判断值是否为合法
		var flag = false;
		$('#eventReportfrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
		}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法

		var eventContent = editor.html();
		var id = $('#eventReportId').html();
		var memo = $("#memoId").val();
		var receiveWay = $("input[name='receiveWay']").val();
		var evenType = $("input[name='evenType']").val();
		var adminReceiveUserId = $("input[name='adminReceiveUserId']").val();
		//var eventReportTime = $("#eventReportTimeId").datetimebox('getValue');
		var surl = "/qx/eventReport!saveEventReport";
		$.ajax({
			url: surl,
			data: {
			id:id,
			//eventReportTime: eventReportTime,
			receiveWay:receiveWay,
			eventContent:eventContent,
			adminReceiveUserId:adminReceiveUserId,
			evenType:evenType
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#eventReport_addwpId').dialog('close');
				$('#eventReport_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='appeal'){  //考勤申诉
		var complainContent = editor3.html();
		var url = "/qx/appeal!saveByAppeal?complainContent2="+complainContent;
		$('#appealSub').form('submit',{
			url: url,
			onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(data){
			var obj = eval('('+data+')');
			if (obj.result==2){
				$.messager.show({
					title: 'Error',
					msg: result.errorMsg
				});
			} else {
				$('#addAppealId').dialog('close');        // close the dialog
				$('#appealgrid').datagrid("reload");    // reload the user data
				$.messager.show({
					title:'操作结果',
					msg:'操作成功...',
					showType:'show'
				});
			}
		}
		});
	}else if (type == 'materialPurchase') {  //物资请购
		url = "/qx/materialPurchase!saveMaterialPurchase";
		var id = $('#laId').val();
		var applyDate = $('#applyDate').datetimebox('getValue');
		var res = editor.html();
		var memo = $('#memo').val();
		var mpCode = $("#mpCodeId").html();
		var did = $('#departmentId').val();
		$.ajax({
			url: url,
			data: {
			applyReason: res,
			applyDate: applyDate,
			memo: memo,
			id: id,
			departmentId: did,
			mpCode: mpCode
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#materialPurchase_ladg').dialog('close');
				$('#materialPurchase_lagrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type == 'carApply'){  //保存车辆
		url = "/qx/carApply!saveCarApply";
		var id = $('#laId').val();
		var applyDate = $('#applyDate').datetimebox('getValue');
		var reason = editor0.html();
		var req = editor1.html();
		var memo = $("#memoId").val();
		var did = $('#departmentId').val();
		$.ajax({
			url: url,
			data: {
			applyDate: applyDate,
			applyReason: reason,
			requirement: req,
			memo: memo,
			id: id,
			departmentId: did
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#carApply_ladg').dialog('close');
				$('#carApply_lagrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if (type == 'materialApply') {
		url = "/qx/materialApply!saveMaterialApply";
		var id = $('#laId').val();
		var applyDate = $('#applyDate').datetimebox('getValue');
		var res = editor0.html();
		var memo = $("input[name='memo']").val();
		var did = $('#departmentId').val();
		var mpCode = $("#wpCodeId").html();
		$.ajax({
			url: url,
			data: {
			applyReason: res,
			applyDate: applyDate,
			memo: memo,
			id: id,
			departmentId: did,
			mpCode:mpCode
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#materialApply_ladg').dialog('close');
				$('#materialApply_lagrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if (type == 'meetingApply') {
		//判断值是否为合法
		var flag = false;
		$('#lafrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
		}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法
		
		url = "/qx/meetingApply!saveMeetingApply";
		var id = $('#laId').val();
		var applyDate = $('#applyDate').datetimebox('getValue');
		var startTime = $("#startTime").datetimebox('getValue');
		var endTime = $("#endTime").datetimebox('getValue');
		var meetingTopic = $('#meetingTopic').val();
		var meetingContent = editor1.html();
		// var attendees = editor2.html();
		var meetingRoom = $("input[name='meetingRoom']").val();
		var memo = $('#memo').val();
		var did = $('#departmentId').val();
		$.ajax({
			url: url,
			data: {
			applyDate: applyDate,
			meetingTopic: meetingTopic,
			meetingContent: meetingContent,
			//attendees: attendees,
			meetingRoom: meetingRoom,
			memo: memo,
			id: id,
			startTime:startTime,
			endTime:endTime,
			departmentId: did
		},

		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#meetingApply_ladg').dialog('close');
				$('#meetingApply_lagrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if (type == 'marketingManagement') { //保存营销信息
		url = "/qx/marketingManagement!saveMarketingManagement";
		var id = $('#laId').val();
		var content = editor0.html();
		var martype = $("input[name='marketingInfoType']").val();
		//var receiveUserIds = $('#receiveUserIds').val();
		var memo = $('#memo').val();
		// var did = $('#departmentId').val();
		$.ajax({
			url: url,
			data: {
			content: content,
			marketingInfoType: martype,
			//receiveUserIds: receiveUserIds,
			memo: memo,
			id: id 
			//departmentId: did
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#marketingManagement_ladg').dialog('close');
				$('#marketingManagement_lagrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=='video'){   //保存视频管理
		var url = "/qx/video!saveByVideo";
		$('#videoSub').form('submit',{
			url: url,
			onSubmit: function(){
			return $(this).form('validate');
		},
		success: function(data){
			var obj = eval('('+data+')');
			if (obj.result==2){
				$.messager.show({
					title: 'Error',
					msg: result.errorMsg
				});
			} else {
				$('#addVideoId').dialog('close');        // close the dialog
				$('#videogrid').datagrid("reload");    // reload the user data
				$.messager.show({
					title:'操作结果',
					msg:'操作成功...',
					showType:'show'
				});
			}
		}
		});

	}else if(type == 'specialAccountMan'){
		//判断值是否为合法
		var flag = false;
		$('#spafrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
		}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法

		var id = $('#spaId').val();
		var specialAccount = $("input[name='specialAccount']").val();
		var userId = $("input[name='userId']").val();
		var passWord = $('#passWordId').val();
		var passWord2 = $('#passWordId2').val();
		if(passWord!=passWord2){
			$.messager.alert('提示','两个密码不一致!','warning');
			return false;
		}
		var memo = $('#memoId').val();
		var status = $('input[name="status"]:checked').val(); 
		$.ajax({
			url: "/qx/specialAccountMan!checkSpecialAccountManToSave",
			data: {
			userId:userId,
			specialAccount: specialAccount,
			passWord: passWord,
			status: status,
			memo: memo,
			id: id,
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$.ajax({
					url: "/qx/specialAccountMan!saveSpecialAccountMan",
					data: {
					userId:userId,
					specialAccount: specialAccount,
					passWord: passWord,
					status: status,
					memo: memo,
					id: id,
				},
				success: function(data) {
					var obj = eval('(' + data + ')');
					if (obj.result == 1) {
						$('#specialAccountMan_ladg').dialog('close');
						$('#specialAccountMan_lagrid').datagrid("reload");
						$.messager.show({
							title: '提示',
							msg: obj.errorMsg
						});
					} else {
						$.messager.show({
							title: '提示',
							msg: obj.errorMsg
						});
					}
				}
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}else if(type=="importPapersMan"){
		url = "/qx/importPapersMan!saveImportPapersMan"
			$('#importPapersManfrm').form('submit',{
				url: url,
				onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(data){
				var obj = eval('('+data+')');
				if (obj.result==2){
					$.messager.show({
						title: '提示',
						msg: obj.errorMsg
					});
				} else if(obj.result==1){
					$('#importPapersMan_addRId').dialog('close');        // close the dialog
					$('#importPapersMan_wpgrid').datagrid("reload");    // reload the user data
				}else{
					$.messager.show({
						title: '提示',
						msg: "请检查网络是否连接正常!"
					});
				}
			}
			});
	}else if (type == 'importantPapersApply') {
		url = "/qx/importantPapersApply!saveImportantPapersApply";
		var id = $('#laId').val();
		var submitDate = $('#submitDate').datetimebox('getValue');
		var reason = editor0.html();
		var memo = $('#memo').val();
		var did = $('#departmentId').val();
		$.ajax({
			url: url,
			data: {
			submitDate: submitDate,
			applyReason: reason,
			memo: memo,
			id: id,
			departmentId: did
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#importantPapersApply_ladg').dialog('close');
				$('#importantPapersApply_lagrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});

	}else if(type=="damageReport"){
		//判断值是否为合法
		var flag = false;
		$('#damageReportfrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
			}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法
		
		var id = $("#drId").val();
		var materialName = $("input[name='materialName']").val();
		var damageType = $("input[name='damageType']").val();
		var materialType = $("input[name='materialType']").val();
		var specModel = $("input[name='specModel']").val();
		var unitPrice = $("input[name='unitPrice']").val();
		var responsibilityConfirm = $("input[name='responsibilityConfirm']").val();
		var billCost= $("input[name='billCost']:checked").val();
		var fileDate = $("#fileDate").datebox('getValue');
		var reasonDesc = editor0.html();
		
		$.ajax({
			url: "/qx/damageReport!saveDamageReport",
			data: {
			materialName: materialName,
			damageType: damageType,
			materialType: materialType,
			id: id,
			specModel: specModel,
			unitPrice:unitPrice,
			responsibilityConfirm:responsibilityConfirm,
			fileDate:fileDate,
			billCost:billCost,
			reasonDesc:reasonDesc
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#damageReport_addRId').dialog('close');
				$('#damageReport_wpgrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}

}

/***
 * 定义操作的删除
 */
opt.remove = function(type) {
	if (type == 'notice') {
		var row = $('#noticelist_noticegrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.title + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/notice!deleteNotice",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#noticelist_noticegrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的公告!', 'warning');
		}
	} else if (type == 'tranPostion') {
		var row = $('#tranPostion_tpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/transferposition!deleteTransferPosition",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#tranPostion_tpgrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的调岗申请!', 'warning');
		}
	} else if(type=='position'){
		var row = $('#position_list').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.names + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/position!deletePosition",
						data: {
						id: row.poliId
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#position_list').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的职位!', 'warning');
		}
	}else if(type=='leaveApply'){
		var row = $('#leaveApply_lagrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/leaveApply!deleteLeaveApply",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#leaveApply_lagrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的请假!', 'warning');
		}
	}else if(type=='process'){   //删除流程
		var row = $('#process_progrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.processName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/process!deleteProcess",
						data: {
						processId: row.processId
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#process_progrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的流程!', 'warning');
		}
	}else if(type=='procesNode'){
		var row = $('#processNode_pngrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.nodeName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/processNode!deleteProcessNode",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#processNode_pngrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的流程节点!', 'warning');
		}
	}else if(type=='workPlan'){  //工作计划
		var row = $('#workPlan_wpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/workPlan!deleteWorkPlan",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#workPlan_wpgrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的工作计划!', 'warning');
		}
	}else if(type=='remind'){
		var row = $('#remind_wpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.id + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/remind!deleteRemind",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#remind_wpgrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的备忘录!', 'warning');
		}

	}else if(type=='trainingApply'){
		var row = $('#training_wpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/trainingApply!deleteTrainingApply",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#training_wpgrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的培训!', 'warning');
		}
	}else if(type=='recruitmentApply'){
		var row = $('#recruitment_wpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/recruitmentApply!deleteRecruitmentApply",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#recruitment_wpgrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的招聘!', 'warning');
		}
	}else if(type=='custiomServiceRec'){
		var row = $('#custiomService_wpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.id + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/custiomServiceRec!deleteCustiomServiceRec",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#custiomService_wpgrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的客服工单!', 'warning');
		}
	}else if(type=='documentApply'){
		var row = $('#documentApply_wpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.id + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/documentApply!deleteDocumentApply",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#documentApply_wpgrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的制度发布!', 'warning');
		}
	}else if(type=='projectManageMent'){
		var row = $('#project_wpgrid').datagrid('getSelected');
		if(row){
			$.ajax({
				type: "POST",
				url: "/qx/projectManageMent!loadProjectManageMentUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					if(obj.userId==row.userId){
						$.messager.confirm('提示', '你确定删除【' + row.id + '】?',
								function(r) {
							if (r) {
								$.ajax({
									type: "POST",
									url: "/qx/projectManageMent!deleteProjectManageMent",
									data: {
									id: row.id
								},
								success: function(data) {
									var obj = eval('(' + data + ')');
									if (obj.result == 1) {
										$('#project_wpgrid').datagrid("reload");
										$.messager.show({ 
											title: '提示',
											msg: obj.errorMsg
										});
									} else {
										$.messager.show({
											title: '提示',
											msg: obj.errorMsg
										});
									}
								}
								});
							}
						});

					}else{
						$.messager.alert('提示', '该项目不是你发起，无权删除该项目!', 'warning');
					}
				}
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的项目!', 'warning');
		}
	}else if(type=='personalThingManage'){
		var row = $('#personalThing_wpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.id + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/personalThingManage!deletePersonalThingManage",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#personalThing_wpgrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的个人事务!', 'warning');
		}
	}else if(type=='workLog'){
		var row = $('#workLog_wpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.id + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/workLog!deleteWorkLog",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#workLog_wpgrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的工作日志!', 'warning');
		}
	}else if(type=='eventReport'){
		var row = $('#eventReport_wpgrid').datagrid('getSelected');
		if(row){
			$.messager.confirm('提示', '你确定删除【' + row.id + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/eventReport!deleteEventReport",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#eventReport_wpgrid').datagrid("reload");
							$.messager.show({ 
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要删除的事项!', 'warning');
		}
	}else if(type=='appeal'){  //考勤申诉
		var row = $('#appealgrid').datagrid('getSelected');
		if (row){
			$.messager.confirm('提示','你确定删除【'+row.userId+'】?',function(r){
				if (r){
					$.ajax({
						type: "POST",
						url: "/qx/appeal!deleteById",
						data: {id:row.id},
						success: function(data){
							var obj = eval('('+data+')');
							if (obj.result==1){
								$('#appealgrid').datagrid('reload');    // reload the user data
							} else {
								$.messager.show({    // show error message
									title: 'Error',
									msg: result.errorMsg
								});
							}
						}
					});
				}
			});
		}else{
			$.messager.alert('提示','请选择你要删除的考勤申诉!','warning');
		}
	}else if (type == 'materialPurchase') {  //删除物资请购
		var row = $('#materialPurchase_lagrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/materialPurchase!deleteMaterialPurchase",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#materialPurchase_lagrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的数据!', 'warning');
		}
	}else if(type == 'carApply'){  //删除车辆
		var row = $('#carApply_lagrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/carApply!deleteCarApply",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#carApply_lagrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的数据!', 'warning');
		}
	}else if (type == 'materialApply') {
		var row = $('#materialApply_lagrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/materialApply!deleteMaterialApply",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#materialApply_lagrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的数据!', 'warning');
		}
	}else if (type == 'meetingApply') { //会议申请删除
		var row = $('#meetingApply_lagrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.meetingTopic + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/meetingApply!deleteMeetingApply",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#meetingApply_lagrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else if(obj.result==2){
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}else{
							$.messager.show({ // show error message
								title: '提示',
								msg: "请检查网络是否连接正常!"
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的数据!', 'warning');
		}
	}else if (type == 'marketingManagement') {
		var row = $('#marketingManagement_lagrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/marketingManagement!deleteMarketingManagement",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#marketingManagement_lagrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的数据!', 'warning');
		}
	}else if(type=='video'){   //删除任务
		var row = $('#videogrid').datagrid('getSelected');
		if(userId==row.userId){
			if (row){
				$.messager.confirm('提示','你确定删除【'+row.userId+'】?',function(r){
					if (r){
						$.ajax({
							type: "POST",
							url: "/qx/video!deleteById",
							data: {id:row.id},
							success: function(data){
								var obj = eval('('+data+')');
								if (obj.result==1){
									$('#videogrid').datagrid('reload');    // reload the user data
								} else {
									$.messager.show({    // show error message
										title: 'Error',
										msg: result.errorMsg
									});
								}
							}
						});
					}
				});
			}else{
				$.messager.alert('提示','请选择你要删除的视频记录!','warning');
			}
		}else{
			$.messager.alert('提示','你没有权限删除视频记录!','warning');
		}
	}else if(type == 'specialAccountMan'){
		var row = $('#specialAccountMan_lagrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.specialAccount + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/specialAccountMan!deleteSpecialAccountMan",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#specialAccountMan_lagrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的数据!', 'warning');
		}
	}else if(type=='importPapersMan'){
		var row = $('#importPapersMan_wpgrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.fileName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/importPapersMan!deleteImportPapersMan",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#importPapersMan_wpgrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: "删除重要文件成功"
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: "删除重要文件失败"
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的数据!', 'warning');
		}
	}else if (type == 'importantPapersApply') {
		var row = $('#importantPapersApply_lagrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.userName + '】?',
					function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/importantPapersApply!deleteImportantPapersApply",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#importantPapersApply_lagrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的数据!', 'warning');
		}
	}else if (type == 'damageReport') {
		var row = $('#damageReport_wpgrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定删除【' + row.materialName + '】?',
			function(r) {
				if (r) {
					$.ajax({
						type: "POST",
						url: "/qx/damageReport!deleteDamageReport",
						data: {
						id: row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#damageReport_wpgrid').datagrid("reload");
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						} else {
							$.messager.show({ // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}
					}
					});
				}
			});
		} else {
			$.messager.alert('提示', '请选择你要删除的数据!', 'warning');
		}
	}
}

/****
 * 定义更新操作
 * */
opt.edit = function(type) {
	if (type == 'notice') { //菜单更新
		var row = $('#noticelist_noticegrid').datagrid('getSelected');
		if (row) {
			$('#noticelist_addNoticeId').show();
			$('#nlfrm').form('clear');
			$('#noId').html(row.id);
			$("#nltitleId").html("修改公告");
			$('#noticelist_addNoticeId').dialog({
				title: '修改公告',
				width: $(window).width() * 0.9,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="content"]', {
					resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="content"]');
			},
			buttons: [{
				text: '保存',
				handler: function() {
				opt.save('notice');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#noticelist_addNoticeId').dialog('close');
			}
			}]
			});
			url = '/qx/notice!loadNoticeById';
			$.ajax({
				type: "POST",
				url: url,
				data: {
				id: row.id
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$('#titleId').val(obj.title);
					$('#menoId').val(obj.memo);
					editor.html(obj.content);
				} else {
					$.messager.show({ // show error message
						title: '提示',
						msg: obj.errorMsg
					});
				}
			}
			});
		} else {
			$.messager.alert('提示', '请选择你要编辑的公告!', 'warning');
		}
	}else if(type == 'position'){
		var row = $('#position_list').datagrid('getSelected'); 
		if(row){
			$('#position_dg').dialog('open').dialog('setTitle','修改');
			$("#titleId").html("修改职位");
			$('#posfm').form('clear');
			var url = '/qx/position!loadPositionById';
			$.ajax({
				type: "POST",
				url: url,
				data: {
				id: row.poliId
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$('#posId').html(obj.id);
					$("#namesId").val(obj.names);
				} else {
					$.messager.show({ // show error message
						title: '提示',
						msg: obj.errorMsg
					});
				}
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要编辑的职位!', 'warning');
		}
	}else if(type == 'leaveApply'){  //请假
		var row = $('#leaveApply_lagrid').datagrid('getSelected'); 
		if(row){
			$('#leaveApply_ladg').show();
			$('#lafrm').form('clear');
			$('#laId').val(row.id);
			$('#leaveApply_ladg').dialog({
				title: '请假申请',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
					editor = KindEditor.create('textarea[name="leaveReson"]', {
						themeType : 'qq',
						items : [
						         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
						         ],
						         resizeType: 1
					});
				},
				onBeforeClose: function() {
					KindEditor.remove('textarea[name="leaveReson"]');
				},
				buttons: [{
					text: '保存',
					handler: function() {
					opt.save('leaveApply');
				}
				},
				{
					text: '取消',
					handler: function() {
					$('#leaveApply_ladg').dialog('close');
				}
				}]
			});
            
			

			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");

					//职位
					$("#positionNameId").val(obj.positionName);
					$("#positionNameId").attr('readonly','readonly');
					$("#positionNameId").addClass("bgcolor");

				} 
			}
			});
			//部门的关联关系
			/*
			$('#departLATreeId').combotree({  
				url: '/qx/org!queryOrgComTree?orgCode=1',
				required: true,
				onClick: function(node) {
				var posurl = "/qx/position!queryPositionByCode?orgCode="+node.id;
				$("#positionId").combobox({ 
					url:posurl,
					textField: "text",
					valueField: "id",
					multiple: false,
					panelHeight: "auto",
					onChange: function(oldVal, newVal) {
					var userurl = "/qx/user!queryUserByPosCode?posCode="+oldVal;
					$("#userLaId").combobox({
						url:userurl,
						textField: "text",
						valueField: "id",
						multiple: false,
						panelHeight: "auto",
					});
				   }
				});
			},
			onLoadSuccess: function(row, data) {
				var t = $(this);
				if (data) {
					$(data).each(function(index, d) {
						if (this.state == 'closed') {
							t.tree('expandAll');
						}
					});
				}
			}
			});
			 */
			//请假性质
			var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_LEAVE_PROPERTY";
			$("#laPryId").combobox({ 
				url:configUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				panelHeight: "auto",
				onLoadSuccess: function() {
				$("#laPryId").combobox("setValue",row.leavePropertyId);
			}
			});

			//加载一条数据
			var url = '/qx/leaveApply!loadLeaveApplyById?id='+row.id;
			$('#lafrm').form('load',url);
			
			editor.html(row.applyReason);
			/*
			$("#lafrm").ajaxStop(function(){
				    flag = 2;
				    var posurl = "/qx/position!queryPositionByCode?orgCode="+$("input[name='la.departmentId']").val();
					$("#positionId").combobox({ 
						url:posurl,
						textField: "text",
						valueField: "id",
						multiple: false,
						panelHeight: "auto",
						onChange: function(oldVal, newVal) {
						var userurl = "/qx/user!queryUserByPosCode?posCode="+oldVal;
						$("#userLaId").combobox({
							url:userurl,
							textField: "text",
							valueField: "id",
							multiple: false,
							panelHeight: "auto",
						});
					   }
					});
			 });
			 */

			/*
			var posurl = "/qx/position!queryPositionByCode?orgCode="+node.id;
			$("#positionId").combobox({ 
				url:posurl,
				textField: "text",
				valueField: "id",
				multiple: false,
				panelHeight: "auto",
				onChange: function(oldVal, newVal) {
				var userurl = "/qx/user!queryUserByPosCode?posCode="+oldVal;
				$("#userLaId").combobox({
					url:userurl,
					textField: "text",
					valueField: "id",
					multiple: false,
					panelHeight: "auto",
				});
			   }
			});
			 */
		}else{
			$.messager.alert('提示', '请选择你要编辑的请假!', 'warning');
		}
	}else if(type=='process'){
		var row = $('#process_progrid').datagrid('getSelected'); 
		if(row){
			$('#process_prodg').show();
			$('#profrm').form('clear');
			$('#proId').val(row.processId);
			//$("span#nltitleId").html("添加公告");
			$('#process_prodg').dialog({
				title: '修改流程',
				width: $(window).width() * 0.45,
				height: $(window).height() * 0.75,
				padding: 5,
				modal: true,
				buttons: [{
					text: '提交',
					handler: function() {
					opt.save('process');
				}
				},
				{
					text: '取消',
					handler: function() {
					$('#process_prodg').dialog('close');
				}
				}]
			});

			var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=SYS_PROCEE_BUSI";
			$("#busiFunId").combobox({
				url:configUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
			});


			var url = '/qx/process!loadProcessById?processId='+row.processId;
			$('#process_prodg').form('load',url);
		}else{
			$.messager.alert('提示', '请选择你要编辑的职位!', 'warning');
		}
	}else if(type=='procesNode'){
		var row = $('#processNode_pngrid').datagrid('getSelected');
		if(row){
			$('#processNode_pnodg').show();
			$('#pnfrm').form('clear');

			//加载复combox
			/*
			var dburl = "/qx/processNode!queryProcessNodeByTwoId?processId="+row.processId+"&id="+row.id;
			$("#nodeIdNextId").combobox({
				url:dburl,
				textField: "text",
				valueField: "id",
				multiple: true,
				panelHeight: "auto"
			});
			 */
			$("input[name='pn.nodeIdNext']").val(row.nodeIdNext);
			//$("input[name='pn.nodeIdNext']").combobox('setValues', ['N3','N6']);
			//processId
			$("input[name='pn.nextNodeProcessName']").val(row.nextNodeProcessName);
			$('#pnId').val(row.id);
			$('#processNode_pnodg').dialog({
				title: '修改流程节点',
				width: $(window).width() * 0.45,
				height: $(window).height() * 0.85,
				padding: 5,
				modal: true,
				buttons: [{
					text: '提交',
					handler: function() {
					opt.save('procesNode');
				}
				},
				{
					text: '取消',
					handler: function() {
					$('#processNode_pnodg').dialog('close');
				}
				}]
			});


			$("input[name='pn.nodeName']").val(row.nodeName);
			$("#nodeId").val(row.nodeId);

			if(row.startNode==1){
				$("#yesStartId").attr("checked","checked");
			}else{
				$("#notStartId").attr("checked","checked");
			}
			if(row.nodeType==1){
				$("#renId").attr("checked","checked");
			}else{
				$("#autoId").attr("checked","checked");
			}


			$("#sortId").numberspinner({
				min: 1,
				max: 100,
				required:true,
				editable: false
			});

			$("#sortId").numberspinner('setValue',row.sort);


			//节点所属流程
			var prourl = "/qx/process!queryProcessByALL?id="+row.id;
			$("#proId").combobox({
				url:prourl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				panelHeight: "auto",
				onChange: function(oldVal, newVal) {
				var pnurl = "/qx/processNode!queryProcessNodeByTwoId?processId="+row.processId+"&id="+row.id;
				$("#nodeIdNextId").combobox({
					url:pnurl,
					textField: "text",
					valueField: "id",
					multiple: true,
					panelHeight: "auto"
				});
			}
			});


			var url = '/qx/processNode!loadProcessNodeById?id='+row.id;
			$.ajax({
				url: url,
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');

			}
			});
			$("#proId").combobox('setValue',row.processId);
		}else{
			$.messager.alert('提示', '请选择你要编辑的节点!', 'warning');
		}
	}/*else if(type=='workPlan'){
		var row = $('#workPlan_wpgrid').datagrid('getSelected');
		if (row) {
			$('#workPlan_addwpId').show();
			$('#wpfrm').form('clear');
			$('#wpId').html(row.id);
			$('#workPlan_addwpId').dialog({
				title: '修改工作计划',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="planContent"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="planContent"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('workPlan');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#workPlan_addwpId').dialog('close');
			}
			}]
			});

			//加载用户信息
			$("#userNameId").val(row.userName);
			$("#userNameId").attr('readonly','readonly');
			$("#userNameId").addClass("bgcolor");

			//部门
			$("#departNameId").val(row.departmentName);
			$("#departNameId").attr('readonly','readonly');
			$("#departNameId").addClass("bgcolor");
			editor.html(row.planContent);

			//计划年份
			var wpYearUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_WORKPLAN_YEAR";
			$("#wpyearId").combobox({ 
				url:wpYearUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				panelHeight: "auto",
				onLoadSuccess: function () { 
				$("#wpyearId").combobox('setValue',row.year);
			}

			});

			//计划月份
			var wpMonthUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_WORKPLAN_MONTH";
			$("#wpMonthId").combobox({ 
				url:wpMonthUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				panelHeight: "auto",
				onLoadSuccess: function () { 
				$("#wpMonthId").combobox('setValue',row.month);
			}
			});

			//计划状态
			var wpStatusUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_WORKPLAN_STATUS";
			$("#wpStatusId").combobox({ 
				url:wpStatusUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				panelHeight: "auto",
				editable:false,
				onLoadSuccess: function () { 
				$("#wpStatusId").combobox('setValue',row.status);
			}
			});

			//发送者
			var departUserUrl = "/qx/user!queryDepartUser";
			$("#wpdepartUserId").combobox({ 
				url:departUserUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				onLoadSuccess: function () { 
				$("#wpdepartUserId").combobox('setValue',row.departUser);
			}
			});
			$("#titleId").val(row.title);




			var wpCode = row.wpCode;

			datagrid = $('#dgId').datagrid({
				url : '/qx/workItem!queryWorkItemList?wpCode='+wpCode,
				title : '工作项',
				idField:'id',
				singleSelect:true,
				columns:[[
				          {field:'id',width:80,hidden:'true',title:'Item ID'},
				          {field:'planId',width:80,hidden:'true',title:'Item ID'},
				          {field:'xulei',width:30,title:'序号',align:'center',},
				          {field:'workItem',width:700,
				        	  editor:{type : 'validatebox',
				        	  options : {
				        	  required : true
				          }},
				          title:'项内容'},
				          {field:'directorCofirm',width:60,align:'center',title:'主管确认',hidden:'true'},
				          {field:'employeeConfirm',width:60,align:'center',title:'员工确认',hidden:'true'}
				          ]],
				          toolbar : [ {
				        	  text : '增加',
				        	  iconCls : 'icon-add',
				        	  handler : function() {
				        	  add();
				          }
				          }, '-', {
				        	  text : '删除',
				        	  iconCls : 'icon-remove',
				        	  handler : function() {
				        	  del();
				          }
				          }, '-', {
				        	  text : '修改',
				        	  iconCls : 'icon-edit',
				        	  handler : function() {
				        	  edit();
				          }
				          }, '-', {
				        	  text : '保存',
				        	  iconCls : 'icon-save',
				        	  handler : function() {
				        	  if (editRow != undefined) {
				        		  datagrid.datagrid('endEdit', editRow);
				        	  }
				          }
				          }, '-', {
				        	  text : '取消编辑',
				        	  iconCls : 'icon-undo',
				        	  handler : function() {
				        	  datagrid.datagrid('unselectAll');
				        	  datagrid.datagrid('rejectChanges');
				        	  editRow = undefined;
				          }
				          }, '-', {
				        	  text : '取消选中',
				        	  iconCls : 'icon-undo',
				        	  handler : function() {
				        	  datagrid.datagrid('unselectAll');
				          }
				          }, '-' ],
				          onDblClickRow : function(rowIndex, rowData) {
				if (editRow != undefined) {
					datagrid.datagrid('endEdit', editRow);
					$(".datagrid-editable-input").height(25);
				}

				if (editRow == undefined) {
					datagrid.datagrid('beginEdit', rowIndex);
					editRow = rowIndex;
					datagrid.datagrid('unselectAll');
					$(".datagrid-editable-input").height(25);
				}
			},
			onAfterEdit : function(rowIndex, rowData, changes) {
				var inserted = datagrid.datagrid('getChanges', 'inserted');
				var updated = datagrid.datagrid('getChanges', 'updated');
				if (inserted.length < 1 && updated.length < 1) {
					editRow = undefined;
					datagrid.datagrid('unselectAll');
					return;
				}

				var url = '';
				if (inserted.length > 0) {
					url = '/qx/workItem!saveWorkItem?wpCode='+wpCode;
				}
				if (updated.length > 0) {
					url = '/qx/workItem!updateWorkItem';
				}

				$.ajax({
					url : url,
					data : rowData,
					success : function(data) {
					var obj = eval('(' + data + ')');
					if (obj.result==1) {
						datagrid.datagrid('acceptChanges');
						$.messager.show({
							msg : obj.msg,
							title : '提示'
						});
						editRow = undefined;
						datagrid.datagrid('reload');
					} else {
						datagrid.datagrid('beginEdit', editRow);
						$.messager.alert('提示', obj.msg, 'error');
					}
					datagrid.datagrid('unselectAll');
				}
				});
			},
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
			});
		} else {
			$.messager.alert('提示', '请选择你要编辑的工作计划!', 'warning');
		}
	}*/else if(type=='remind'){
		var row = $('#remind_wpgrid').datagrid('getSelected');
		if (row) {
			$('#remind_addRId').show();
			$('#remindfrm').form('clear');
			$('#rId').html(row.id);
			$('#remind_addRId').dialog({
				title: '修改备忘录',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="remindContent"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="remindContent"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('remind');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#remind_addRId').dialog('close');
			}
			}]
			});

			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");
				} 
			}
			});

			editor.html(row.remindContent);
			//提示方式
			var remindWayUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_REMIND_WAY";
			$("#remindWayId").combobox({ 
				url:remindWayUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				panelHeight: "auto",
				onLoadSuccess: function() {
				$("#remindWayId").combobox("setValue",row.remindWay);
			}
			});

			//$("#remindWayId").combobox("setValue",row.remindWay);
			$("#finishDateId").datetimebox('setValue', row.finishDate);
			$("#remindDateId").datetimebox('setValue', row.remindDate);
		}else{
			$.messager.alert('提示', '请选择你要编辑的备忘录!', 'warning');
		}

	}else if(type=='trainingApply'){
		var row = $('#training_wpgrid').datagrid('getSelected');
		if (row) {
			$('#training_addTId').show();
			$('#trainingfrm').form('clear');
			$('#taId').html(row.id);
			$('#training_addTId').dialog({
				title: '修改培训',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="trainingContent"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="trainingContent"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('trainingApply');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#training_addTId').dialog('close');
			}
			}]
			});

			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");
				} 
			}
			});

			editor.html(row.trainingRequirement);
			$("#memoId").val(row.memo);
			$("#titleId").val(row.title);
			//加载培训的组员
			var userUrl = "/qx/user!queryAllUserByOrgCodeToTran?id="+row.id;
			$.ajax({
				type: "POST",
				url: userUrl,
				success: function(data){
				var obj = eval('('+data+')');
				if (obj.result==1){
					var userHtml = "";
					if(obj.datas.length==0){
						userHtml="<div style='color:red;position:absolute;top:50%;left:45%'>没有你要找的数据!</div>"
					}else{
						for ( var i = 0; i < obj.datas.length; i++) {
							if(obj.datas[i].sign==1){
								userHtml = userHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='posUsers' checked='checked' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
							}else{
								userHtml = userHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='posUsers' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
							}

							if(i%6==0&&i!=0){
								userHtml = userHtml+ "<br/><br/>"
							}
						}
					}
					$("#userTransDiv").html(userHtml);
				} else {
					$.messager.show({    // show error message
						title: '提示',
						msg: obj.errorMsg
					});
				}
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要编辑的培训!', 'warning');
		}
	}else if(type=='recruitmentApply'){  //更新招聘操作
		var row = $('#recruitment_wpgrid').datagrid('getSelected');
		if (row) {
			$('#recruitment_addTId').show();
			$('#recruitmentfrm').form('clear');
			$('#raId').html(row.id);
			$('#recruitment_addTId').dialog({
				title: '修改招聘',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="requirementDesc"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="requirementDesc"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('recruitmentApply');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#recruitment_addTId').dialog('close');
			}
			}]
			});


			editor.html(row.requirementDesc);
			$("#memoId").val(row.memo);
			$('#applyDateId').datebox('setValue', row.applyDate);	

			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");

					//职位
					$("#positionNameId").val(obj.positionName);
					$("#positionNameId").attr('readonly','readonly');
					$("#positionNameId").addClass("bgcolor");

				} 
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要编辑的招聘!', 'warning');
		}
	}else if(type=='custiomServiceRec'){
		var row = $('#custiomService_wpgrid').datagrid('getSelected');
		if (row) {
			$('#custiomService_addRId').show();
			$('#custiomServicefrm').form('clear');
			$('#cId').html(row.id);
			$('#custiomService_addRId').dialog({
				title: '修改客服工单',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="serviceDesc"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="serviceDesc"]');
			},
			buttons: [{
				text: '保存',
				handler: function() {
				opt.save('custiomServiceRec');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#custiomService_addRId').dialog('close');
			}
			}]
			});

			var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_SERVICE_TYPE";
			$(".serviceTypeId").combobox({ 
				url:configUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				panelHeight: "auto",
				onLoadSuccess: function() {
				$(".serviceTypeId").combobox('setValue',row.serviceType);		
			}
			});


			editor.html(row.serviceDesc);
			$("#memoId").val(row.memo);


			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");
				} 
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要编辑的客服工单!', 'warning');
		}
	}else if(type=='documentApply'){
		var row = $('#documentApply_wpgrid').datagrid('getSelected');
		if (row) {
			$('#documentApply_addRId').show();
			$('#documentApplyfrm').form('clear');
			$('#docId').html(row.id);
			$('#documentApply_addRId').dialog({
				title: '修改制度发布',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="content"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="content"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('documentApply');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#documentApply_addRId').dialog('close');
			}
			}]
			});
			editor.html(row.content);
			$("#memoId").val(row.memo);
			$("#docTitleId").val(row.docTitle);	

			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");
				} 
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要编辑的客服工单!', 'warning');
		}
	}else if(type=='projectManageMent'){
		var row = $('#project_wpgrid').datagrid('getSelected');
		if (row) {
			$.ajax({
				type: "POST",
				url: "/qx/projectManageMent!loadProjectManageMentUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					if(row.userId==obj.userId){
						$('#project_addwpId').show();
						$('#projectfrm').form('clear');
						$('#projectId').html(row.id);
						$('#project_addwpId').dialog({
							title: '修改项目',
							width: $(window).width() * 0.8,
							height: $(window).height() * 0.90,
							padding: 5,
							modal: true,
							onOpen: function() {
							editor = KindEditor.create('textarea[name="projectContent"]', {
								themeType : 'qq',
								items : [
								         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
								         ],
								         resizeType: 1
							});
						},
						onBeforeClose: function() {
							KindEditor.remove('textarea[name="projectContent"]');
						},
						buttons: [{
							text: '提交',
							handler: function() {
							opt.save('projectManageMent');
						}
						},
						{
							text: '取消',
							handler: function() {
							$('#project_addwpId').dialog('close');
						}
						}]
						});
						editor.html(row.projectContent);
						$("#memoId").val(row.memo);
						$("#projectNameId").val(row.projectName);	

						//加载用户信息
						$.ajax({
							url: "/qx/leaveApply!loadLeaveApplyToUser",
							data: {
						},
						success: function(data) {
							var obj = eval('(' + data + ')');
							if (obj.result == 1) {
								$("#userNameId").val(obj.userName);
								$("#userNameId").attr('readonly','readonly');
								$("#userNameId").addClass("bgcolor");

								//部门
								$("#departNameId").val(obj.departName);
								$("#departNameId").attr('readonly','readonly');
								$("#departNameId").addClass("bgcolor");
							} 
						}
						});
					}else{
						$.messager.alert('提示', '该项目不是你发起，无权修改该项目!', 'warning');
					}
				}
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要编辑的项目!', 'warning');
		}
	}else if(type=='personalThingManage'){
		var row = $('#personalThing_wpgrid').datagrid('getSelected');
		if (row) {
			$('#personalThing_addwpId').show();
			$('#ptmfrm').form('clear');
			$('#ptmId').html(row.id);
			$('#personalThing_addwpId').dialog({
				title: '修改个人事务',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="thingContent"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="thingContent"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('personalThingManage');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#personalThing_addwpId').dialog('close');
			}
			}]
			});
			editor.html(row.thingContent);
			$("#memoId").val(row.memo);
			$("#thingNameId").val(row.thingName);	

			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");
				} 
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要编辑的个人事务!', 'warning');
		}
	}else if(type=='workLog'){
		var row = $('#workLog_wpgrid').datagrid('getSelected');
		if (row) {
			$('#workLog_addwpId').show();
			$('#workLogfrm').form('clear');
			$('#workLogId').html(row.id);
			$('#workLog_addwpId').dialog({
				title: '修改工作日志',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="logContent"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="logContent"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('workLog');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#workLog_addwpId').dialog('close');
			}
			}]
			});
			editor.html(row.logContent);
			$("#memoId").val(row.memo);
			$('#workLogDateId').datetimebox('setValue', row.workLogDate);

			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");
				} 
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要编辑的工作日志!', 'warning');
		}
	}else if(type=='eventReport'){
		var row = $('#eventReport_wpgrid').datagrid('getSelected');
		if (row) {
			$('#eventReport_addwpId').show();
			$('#eventReportfrm').form('clear');
			$('#eventReportId').html(row.id);
			$('#eventReport_addwpId').dialog({
				title: '修改事项',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="eventContent"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="eventContent"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('eventReport');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#eventReport_addwpId').dialog('close');
			}
			}]
			});

			editor.html(row.eventContent);
			$('#eventReportTimeId').datetimebox('setValue', row.eventReportTime);

			//接收方式
			var posurl = "/qx/config!queryConfigByDomainCode?dataDomain=SYS_RECEIVE_WAY";
			$("#receiveWayId").combobox({ 
				url:posurl,
				textField: "text",
				valueField: "id",
				multiple: false,
				onLoadSuccess: function() {
				$('#receiveWayId').combobox('setValue', row.receiveWay);	
			}
			});


			//事项类型
			var typeurl = "/qx/config!queryConfigByDomainCode?dataDomain=SYS_EVEN_TYPE";
			$("#evenTypeId").combobox({ 
				url:typeurl,
				textField: "text",
				valueField: "id",
				multiple: false,
				onLoadSuccess: function() {
				$('#evenTypeId').combobox('setValue', row.evenType);
			}
			});
			//行政接收人
			var userurl = "/qx/user!queryUsersByDepartCode?departCode=XZ";
			$("#adminReceiveUserId").combobox({ 
				url:userurl,
				textField: "text",
				valueField: "id",
				multiple: false,
				groupField:'group',
				onLoadSuccess: function() {
				$('#adminReceiveUserId').combobox('setValue', row.adminReceiveUserId);
			}
			});


			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");
				} 
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要编辑的事项!', 'warning');
		}
	}else if(type=='tranPostion'){
		var row = $('#tranPostion_tpgrid').datagrid('getSelected');
		if (row) {
			$('#tranPostion_tpdg').show();
			$('#tpfrm').form('clear');
			$('#tpId').val(row.id);
			$('#tranPostion_tpdg').dialog({
				title: '编辑调职申请',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="applyReason"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="applyReason"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('tranPostion');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#tranPostion_tpdg').dialog('close');
			}
			}]
			});

			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');
					$("#departNameId").addClass("bgcolor");

					//职位
					$("#positionNameId").val(obj.positionName);
					$("#positionNameId").attr('readonly','readonly');
					$("#positionNameId").addClass("bgcolor");

				} 
			}
			});

			$("#applyDateID").datebox('setValue', row.applyDate);
			$("#transferDateID").datebox('setValue', row.transferDate);
			$("#salaryID").numberbox('setValue', row.salary);
			editor.html(row.applyReson);
			//调往信息
			$('#departNewTPTreeId').combotree({   //新部门
				url: '/qx/org!queryOrgComTree?orgCode=1',
				required: true,
				onClick: function(node) {
				var posurl = "/qx/position!queryPositionByCode?orgCode="+node.id;
				$("#positioNewId").combobox({ 
					url:posurl,
					textField: "text",
					valueField: "id",
					multiple: false,
					editable:false,
					onLoadSuccess: function(row1, data) {
					var data = $('#positioNewId').combobox('getData');
					if (data.length > 0) {
						$('#positioNewId').combobox('select', data[0].id);
					} 
				}
				});
			},
			onLoadSuccess: function(row1, data) {
				var t = $(this);
				if (data) {
					$(data).each(function(index, d) {
						if (this.state == 'closed') {
							t.tree('expandAll');
						}
					});
				}

				$('#departNewTPTreeId').combotree("setValue",row.departmentIdNew);

				var posurl1 = "/qx/position!queryPositionByCode?orgCode="+row.departmentIdNew;
				$("#positioNewId").combobox({ 
					url:posurl1,
					textField: "text",
					valueField: "id",
					multiple: false,
					editable:false,
					onLoadSuccess: function(row1, data) {
					$('#positioNewId').combobox("setValue",row.positionIdNew);
				}
				});
			}
			});


		}else{
			$.messager.alert('提示', '请选择你要编辑的调岗!', 'warning');
		}
	}else if(type=='appeal'){  //更新考勤申诉
		var row = $('#appealgrid').datagrid('getSelected');
		if (row){
			$('#addAppealId').show();
			url = '/qx/appeal!loadById?id='+row.id;
			$('#appealSub').form('clear');


			//加载编辑框内容
			$.ajax({
				url: url,
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					editor3.html(obj.complainContent);
				} 
			}
			});


			$('#appealSub').form('load',url);
			//	$('#addTaskId').dialog('open').dialog('setTitle','添加');
			$('#addAppealId').dialog({
				title: '修改外出申请',
				width: 700,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor3 = KindEditor.create('textarea[name="complainContent"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="complainContent"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('appeal');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#addAppealId').dialog('close');   
			}
			}]
			});


			//加载用户信息
			$.ajax({
				url: "/qx/leaveApply!loadLeaveApplyToUser",
				data: {
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly','readonly');

					//部门
					$("#departNameId").val(obj.departName);
					$("#departNameId").attr('readonly','readonly');

					//职位
					$("#positionNameId").val(obj.positionName);
					$("#positionNameId").attr('readonly','readonly');

				} 
			}
			}); 
		}else{
			$.messager.alert('提示','请选择你要修改的考勤申诉!','warning');
		}
	}else if (type == 'materialPurchase') { //物资请购申请更新
		var row = $('#materialPurchase_lagrid').datagrid('getSelected');
		if (row) {
			$('#materialPurchase_ladg').show();
			$('#lafrm').form('clear');
			$('#laId').val(row.id);
			$('#materialPurchase_ladg').dialog({
				title: '修改物资请购申请',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor = KindEditor.create('textarea[name="applyReason"]', {
					themeType: 'qq',
					items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
					resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="applyReason"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('materialPurchase');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#materialPurchase_ladg').dialog('close');
			}
			}]
			});

			//创建明细列表
			var editRow = undefined;
			var datagrid;

			datagrid = $('#edg').datagrid({
				title: '物资请购列表',
				singleSelect: true,
				url: '/qx/materialPurchaseDetail!queryMaterialPurchasesByPage?mpCode=' + row.mpCode,
				idField: 'id',
				onClickRow: onClickRow,
				columns: [[{
					field: 'id',
					width: 80,
					hidden: 'true',
					title: 'Item ID'
				},
				{
					field: 'materialName',
					width: 100,
					editor: {
					type: 'validatebox',
					options: {
					required: true
				}
				},
				title: '物品名称'
				},
				{
					field: 'materialType',
					width: 80,
					align: 'center',
					title: '类型',
					editor: 'text',
				},
				{
					field: 'specification',
					width: 80,
					align: 'center',
					editor: 'text',
					title: '规格'
				},
				{
					field: 'unit',
					width: 50,
					align: 'center',
					editor: 'text',
					title: '单位'
				},
				{
					field: 'count',
					width: 50,
					align: 'right',
					editor: 'numberbox',
					title: '数量'
				},
				{
					field: 'barCode',
					width: 150,
					editor: 'text',
					title: '条形码'
				},
				{
					field: 'memo',
					width: 150,
					align: 'center',
					editor: 'text',
					title: '备注'
				}]],
				onDblClickRow: function(rowIndex, rowData) {
				if (editRow != undefined) {
					datagrid.datagrid('endEdit', editRow);
				}

				if (editRow == undefined) {
					datagrid.datagrid('beginEdit', rowIndex);
					editRow = rowIndex;
					datagrid.datagrid('unselectAll');
				}
				$(".datagrid-editable-input").height(22);
			},
			onAfterEdit: function(rowIndex, rowData, changes) {
				var inserted = datagrid.datagrid('getChanges', 'inserted');
				var updated = datagrid.datagrid('getChanges', 'updated');
				if (inserted.length < 1 && updated.length < 1) {
					editRow = undefined;
					datagrid.datagrid('unselectAll');
					return;
				}

				$("#mpCodeId").html(row.mpCode);
				var url = '';
				if (inserted.length > 0) {
					url = '/qx/materialPurchaseDetail!saveMaterialPurchasesByPage?mpCode=' + row.mpCode;
				}
				if (updated.length > 0) {
					url = '/qx/materialPurchaseDetail!updateMaterialPurchaseDetail?mpCode=' + row.mpCode;;
				}

				$.ajax({
					url: url,
					data: rowData,
					success: function(data) {
					var obj = eval('(' + data + ')');
					if (obj.result == 1) {
						datagrid.datagrid('acceptChanges');
						$.messager.show({
							msg: obj.msg,
							title: '提示'
						});
						editRow = undefined;
						datagrid.datagrid('reload');
					} else {
						datagrid.datagrid('beginEdit', editRow);
						$.messager.alert('提示', obj.msg, 'error');
					}
					datagrid.datagrid('unselectAll');
				}
				});

			},
			onRowContextMenu: function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left: e.pageX,
					top: e.pageY
				});
			},
			toolbar: [{
				text: '添加',
				iconCls: 'icon-add',
				handler: function() {
				append();
			}
			},
			'-', {
				text: '删除',
				iconCls: 'icon-remove',
				handler: function() {
				removeit();
			}
			},
			'-', {
				text: '保存',
				iconCls: 'icon-save',
				handler: function() {
				accept();
			}
			},
			'-', {
				iconCls: 'icon-undo',
				text: '撤销',
				handler: function() {
				reject();
			}
			}]
			});

			url = '/qx/materialPurchase!loadMaterialPurchaseById';
			$.ajax({
				type: "POST",
				url: url,
				data: {
				id: row.id
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					editor.html(obj.applyReason);
					$('#applyDate').datetimebox('setValue', obj.applyDate); // set datetimebox value
					$('#memo').val(obj.memo);
					editor.html(obj.content);
				} else {
					$.messager.show({ // show error message
						title: '提示',
						msg: obj.errorMsg
					});
				}
			}
			});
		} else {
			$.messager.alert('提示', '请选择你要编辑的物资申请!', 'warning');
		}

		//加载用户信息
		$.ajax({
			url: "/qx/materialPurchase!loadMaterialPurchaseToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});
	}else if(type=='carApply'){
		var row = $('#carApply_lagrid').datagrid('getSelected');
		if (row) {
			$('#carApply_ladg').show();
			$('#lafrm').form('clear');
			$('#laId').val(row.id);
			$('#carApply_ladg').dialog({
				title: '修改车辆申请',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor0 = KindEditor.create('.[name="applyReason"]', {
					themeType: 'qq',
					items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
					resizeType: 1
				});
				editor1 = KindEditor.create('.[name="requirement"]', {
					themeType: 'qq',
					items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
					resizeType: 1
				});
				/*
                    editor2 = KindEditor.create('.[name="memo"]', {
                        themeType: 'qq',
                        items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
                        resizeType: 1
                    });
				 */
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="applyReason"]');
				KindEditor.remove('textarea[name="requirement"]');
				//KindEditor.remove('textarea[name="memo"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('carApply');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#carApply_ladg').dialog('close');
			}
			}]
			});
			url = '/qx/carApply!loadCarApplyById';
			$.ajax({
				type: "POST",
				url: url,
				data: {
				id: row.id
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$('#applyDate').datetimebox('setValue', obj.applyDate); // set datetimebox value
					editor0.html(obj.applyReason);
					editor1.html(obj.requirement);
					$("input[name='memo']").val(obj.memo);
				} else {
					$.messager.show({ // show error message
						title: '提示',
						msg: obj.errorMsg
					});
				}
			}
			});
		} else {
			$.messager.alert('提示', '请选择你要编辑的车辆申请!', 'warning');
		}

		//加载用户信息
		$.ajax({
			url: "/qx/carApply!loadCarApplyToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});
	}else if (type == 'materialApply') { //物资领用申请更新
		var row = $('#materialApply_lagrid').datagrid('getSelected');
		if (row) {
			$('#materialApply_ladg').show();
			$('#lafrm').form('clear');
			$('#laId').val(row.id);
			$('#materialApply_ladg').dialog({
				title: '修改物资领用申请',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor0 = KindEditor.create('textarea[name="applyReason"]', {
					themeType: 'qq',
					items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
					resizeType: 1
				});
				/*
                    editor1 = KindEditor.create('textarea[name="memo"]', {
                        themeType: 'qq',
                        items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
                        resizeType: 1
                    });
				 */
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="applyReason"]');
				//KindEditor.remove('textarea[name="memo"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('materialApply');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#materialApply_ladg').dialog('close');
			}
			}]
			});
			url = '/qx/materialApply!loadMaterialApplyById';
			$.ajax({
				type: "POST",
				url: url,
				data: {
				id: row.id
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$('#applyDate').datetimebox('setValue', obj.applyDate); // set datetimebox value
					editor0.html(obj.applyReason);
					$("input[name='memo']").html(obj.memo);
				} else {
					$.messager.show({ // show error message
						title: '提示',
						msg: obj.errorMsg
					});
				}
			}
			});

			//加载用户信息
			$.ajax({
				url: "/qx/materialApply!loadMaterialApplyToUser",
				data: {},
				success: function(data) {
					var obj = eval('(' + data + ')');
					if (obj.result == 1) {
						$("#userNameId").val(obj.userName);
						$("#userNameId").attr('readonly', 'readonly');
						$("#userNameId").addClass("bgcolor");

						//部门
						$("#departname").val(obj.departName);
						$("#departname").attr('readonly', 'readonly');
						$("#departname").addClass("bgcolor");

						$("#departmentId").val(obj.departmentId);

					}
				}
			});

			var wpCode = row.mpCode;

			mpgrid = $('#dgId').datagrid({
				url : '/qx/materialApplyDetail!queryMaterialPurchasesByPage?mpCode='+wpCode,
				title : '物资领用',
				idField:'id',
				singleSelect:true,
				columns: [[{
					field: 'id',
					width: 80,
					hidden: 'true',
					title: 'Item ID'
				},
				{
					field: 'materialName',
					width: 100,
					editor: {
					type: 'validatebox',
					options: {
					required: true
				}
				},
				title: '物品名称'
				},
				{
					field: 'materialType',
					width: 80,
					align: 'center',
					title: '类型',
					editor: 'text',
				},
				{
					field: 'specification',
					width: 80,
					align: 'center',
					editor: 'text',
					title: '规格'
				},
				{
					field: 'unit',
					width: 50,
					align: 'center',
					editor: 'text',
					title: '单位'
				},
				{
					field: 'count',
					width: 50,
					align: 'right',
					editor: 'numberbox',
					title: '数量'
				},
				{
					field: 'barCode',
					width: 150,
					editor: 'text',
					title: '条形码'
				},
				{
					field: 'memo',
					width: 150,
					align: 'center',
					editor: 'text',
					title: '备注'
				}]],
				toolbar : [ {
					text : '增加',
					iconCls : 'icon-add',
					handler : function() {
					add();
				}
				}, '-', {
					text : '删除',
					iconCls : 'icon-remove',
					handler : function() {
					del();
				}
				}, '-', {
					text : '修改',
					iconCls : 'icon-edit',
					handler : function() {
					edit();
				}
				}, '-', {
					text : '保存',
					iconCls : 'icon-save',
					handler : function() {
					if (editRow != undefined) {
						mpgrid.datagrid('endEdit', editRow);
					}
				}
				}, '-', {
					text : '取消编辑',
					iconCls : 'icon-undo',
					handler : function() {
					mpgrid.datagrid('unselectAll');
					mpgrid.datagrid('rejectChanges');
					editRow = undefined;
				}
				}, '-', {
					text : '取消选中',
					iconCls : 'icon-undo',
					handler : function() {
					mpgrid.datagrid('unselectAll');
				}
				}, '-' ],
				onDblClickRow : function(rowIndex, rowData) {
				if (editRow != undefined) {
					mpgrid.datagrid('endEdit', editRow);
					$(".datagrid-editable-input").height(25);
				}

				if (editRow == undefined) {
					mpgrid.datagrid('beginEdit', rowIndex);
					editRow = rowIndex;
					mpgrid.datagrid('unselectAll');
					$(".datagrid-editable-input").height(25);
				}
			},
			onAfterEdit : function(rowIndex, rowData, changes) {
				var inserted = mpgrid.datagrid('getChanges', 'inserted');
				var updated = mpgrid.datagrid('getChanges', 'updated');
				if (inserted.length < 1 && updated.length < 1) {
					editRow = undefined;
					mpgrid.datagrid('unselectAll');
					return;
				}

				var url = '';
				if (inserted.length > 0) {
					url = '/qx/materialApplyDetail!saveMaterialApplysByPage?mpCode='+wpCode;
				}
				if (updated.length > 0) {
					url = '/qx/materialApplyDetail!updateMaterialApplyDetail';
				}

				$.ajax({
					url : url,
					data : rowData,
					success : function(data) {
					var obj = eval('(' + data + ')');
					if (obj.result==1) {
						mpgrid.datagrid('acceptChanges');
						$.messager.show({
							msg : obj.msg,
							title : '提示'
						});
						editRow = undefined;
						mpgrid.datagrid('reload');
					} else {
						mpgrid.datagrid('beginEdit', editRow);
						$.messager.alert('提示', obj.msg, 'error');
					}
					mpgrid.datagrid('unselectAll');
				}
				});
			},
			onRowContextMenu : function(e, rowIndex, rowData) {
				e.preventDefault();
				$(this).datagrid('unselectAll');
				$(this).datagrid('selectRow', rowIndex);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
			});
		} else {
			$.messager.alert('提示', '请选择你要编辑的物资领用!', 'warning');
		}
	}else if (type == 'meetingApply') { //会议修改
		var row = $('#meetingApply_lagrid').datagrid('getSelected');
		if (row) {
			$('#meetingApply_ladg').show();
			$('#lafrm').form('clear');
			$('#laId').val(row.id);
			$('#meetingApply_ladg').dialog({
				title: '修改会议申请',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor1 = KindEditor.create('textarea[name="meetingContent"]', {
					themeType: 'qq',
					items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
					resizeType: 1
				});
				/*
                    editor2 = KindEditor.create('textarea[name="attendees"]', {
                        themeType: 'qq',
                        items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
                        resizeType: 1
                    });
				 */
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="meetingContent"]');
				// KindEditor.remove('textarea[name="attendees"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('meetingApply');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#meetingApply_ladg').dialog('close');
			}
			}]
			});
			url = '/qx/meetingApply!loadMeetingApplyById';
			$.ajax({
				type: "POST",
				url: url,
				data: {
				id: row.id
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$('#applyDate').datetimebox('setValue', obj.applyDate); // set datetimebox value
					$('#startTime').datetimebox('setValue', obj.startTime); 
					$('#endTime').datetimebox('setValue', obj.endTime); 
					$('#meetingTopic').val(obj.meetingTopic);
					editor1.html(obj.meetingContent);
					//editor2.html(obj.attendees);
					$('#meetingRoom').val(obj.meetingRoom);
					$('#memo').val(obj.memo);
				} else {
					$.messager.show({ // show error message
						title: '提示',
						msg: obj.errorMsg
					});
				}
			}
			});

			//会议室
			var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_MEETING_ROOM";
			$("#meetingRoom").combobox({ 
				url:configUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				panelHeight: "auto",
				onLoadSuccess: function () { 
				$('#meetingRoom').combobox('select', row.meetingRoom);
			}
			});
		} else {
			$.messager.alert('提示', '请选择你要编辑的会议申请!', 'warning');
		}

		//加载用户信息
		$.ajax({
			url: "/qx/meetingApply!loadMeetingApplyToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});
	}else if (type == 'marketingManagement') { //营销管理
		var row = $('#marketingManagement_lagrid').datagrid('getSelected');
		if (row) {
			$('#marketingManagement_ladg').show();
			$('#lafrm').form('clear');
			$('#laId').val(row.id);
			$('#marketingManagement_ladg').dialog({
				title: '营销管理',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor0 = KindEditor.create('textarea[name="content"]', {
					uploadJson: '../kindeditor/jsp/upload_json.jsp',
					fileManagerJson: '../kindeditor/jsp/file_manager_json.jsp',
					allowFileManager: true
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="content"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('marketingManagement');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#marketingManagement_ladg').dialog('close');
			}
			}]
			});

			//加载用户信息
			$("#userNameId").val(row.userName);
			$("#userNameId").attr('readonly', 'readonly');
			$("#userNameId").addClass("bgcolor");

			//部门
			$("#departname").val(row.departmentName);
			$("#departname").attr('readonly', 'readonly');
			$("#departname").addClass("bgcolor");

			//营销类型
			var typeUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_MARKETINGMAN_TYPE";
			$("#marketingInfoType").combobox({
				url: typeUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				panelHeight: "auto",
				onLoadSuccess: function() {
				$('#marketingInfoType').combobox('select', row.marketingInfoTypeId);
			}
			});

			//创建日期
			$("#createDate").datetimebox('setValue', row.createDate);
			editor0.html(row.content);
			$("#memo").val(row.memo);
		}
	}else if(type=='video'){   //更新任务
		var row = $('#videogrid').datagrid('getSelected');
		if(userId==row.userId){
			if (row){
				$('#addVideoId').show();
				url = '/qx/video!loadById?id='+row.id;
				$('#videoSub').form('clear');
				$('#videoSub').form('load',url);
				//	$('#addTaskId').dialog('open').dialog('setTitle','添加');
				$('#addVideoId').dialog({
					title: '修改视频',
					width: 600,
					height: 400,
					padding: 5,
					modal: true,
					onOpen: function() {
					editor3 = KindEditor.create('textarea[name="complainContent"]', {
						themeType : 'qq',
						items : [
						         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
						         ],
						         resizeType: 1
					});
				},
				onBeforeClose: function() {
					KindEditor.remove('textarea[name="complainContent"]');
				},
				buttons: [{
					text: '提交',
					handler: function() {
					opt.save('video');
				}
				},
				{
					text: '取消',
					handler: function() {
					$('#addVideoId').dialog('close');   
				}
				}]
				});

				$("#departNameId").val(row.departmentId);
				$("#departNameId").attr('readonly', 'readonly');
				$("#departNameId").addClass("bgcolor");
				$("#userNameId").val(row.userName);
				$("#userNameId").attr('readonly', 'readonly');
				$("#userNameId").addClass("bgcolor");
				
			}else{
				$.messager.alert('提示','请选择你要修改的视频记录!','warning');
			}
		}else{
			$.messager.alert('提示','你没有权限修改该视频记录!','warning');
		}
	}else if(type=="specialAccountMan"){
		var row = $('#specialAccountMan_lagrid').datagrid('getSelected');
		if (row) {
			$('#specialAccountMan_ladg').show();
			$('#spafrm').form('clear');
			$('#spaId').val(row.id);
			//$("#isAbleId").attr("checked","checked");
			$("#specialAccount").val(row.specialAccount);
			$("#passWordId").val(row.passWord);
			$("#passWordId2").val(row.passWord);
			$('#specialAccountMan_ladg').dialog({
				title: '特殊账号修改',
				width: $(window).width() * 0.5,
				height: $(window).height() * 0.75,
				padding: 5,
				modal: true,
				buttons: [{
					text: '提交',
					handler: function() {
					opt.save('specialAccountMan');
				}
				},
				{
					text: '取消',
					handler: function() {
					$('#specialAccountMan_ladg').dialog('close');
				}
				}]
			});

			//加载用户信息
			$.ajax({
				url: "/qx/specialAccountMan!loadSpecialAccountManToUser",
				data: {},
				success: function(data) {
					var obj = eval('(' + data + ')');
					if (obj.result == 1) {
						$("#userNameId").val(obj.userName);
						$("#userNameId").attr('readonly', 'readonly');
						$("#userNameId").addClass("bgcolor");

						//部门
						$("#departname").val(obj.departName);
						$("#departname").attr('readonly', 'readonly');
						$("#departname").addClass("bgcolor");

						$("#departmentId").val(obj.departmentId);

					}
				}
			});

			if(row.status==1){
				$("#isAbleId").attr("checked","checked");
			}else{
				$("#noAbleId").attr("checked","checked");
			}

			//所属部门
			$('#departNewTPTreeId').combotree({   //新部门
				url: '/qx/org!queryOrgComTree?orgCode=1',
				required: true,
				onClick: function(node) {
				var posurl = "/qx/user!queryUserByOrgCode?orgCode="+node.id;
				$("#userId").combobox({ 
					url:posurl,
					textField: "text",
					valueField: "id",
					multiple: false,
					editable:false,
					onLoadSuccess: function() {
					var data = $('#userId').combobox('getData');
					if (data.length > 0) {
						$('#userId').combobox('select', data[0].id);
					} 
				}
				});
			},
			onLoadSuccess: function(row1, data) {
				var t = $(this);
				if (data) {
					$(data).each(function(index, d) {
						if (this.state == 'closed') {
							t.tree('expandAll');
						}
					});
				}

				//加载完之后，就加载用户信息
				var surl = "/qx/user!queryUserByOrgCode?orgCode="+row.departmentId;
				$("#userId").combobox({ 
					url:surl,
					textField: "text",
					valueField: "id",
					multiple: false,
					editable:false,
					onLoadSuccess: function() {
					$('#userId').combobox('setValue', row.userId);
				}
				});
			}
			});

			$('#departNewTPTreeId').combotree('setValue', row.departmentId);
		} else {
			$.messager.alert('提示', '请选择你要编辑的申请!', 'warning');
		}
	}else if(type == 'importPapersMan'){
		var row = $('#importPapersMan_wpgrid').datagrid('getSelected');
		if (row){
			$('#importPapersMan_addRId').show();
			$('#importPapersManfrm').form('clear');
			$('#impPaperId').val(row.id);
			$('#importPapersMan_addRId').dialog({
				title: '重要文件修改',
				width: $(window).width() * 0.5,
				height: $(window).height() * 0.55,
				padding: 5,
				modal: true,
				buttons: [{
					text: '提交',
					handler: function() {
					opt.save('importPapersMan');
				}
				},
				{
					text: '取消',
					handler: function() {
					$('#importPapersMan_addRId').dialog('close');
				}
				}]
			});

			//加载用户信息
			$("#userNameId").val(row.userName);
			$("#userNameId").attr('readonly', 'readonly');
			$("#userNameId").addClass("bgcolor");

			//部门
			$("#departNameId").val(row.departmentName);
			$("#departNameId").attr('readonly', 'readonly');
			$("#departNameId").addClass("bgcolor");

			$("#memoId").val(row.memo);
		}else{
			$.messager.alert('提示','请选择你要修改的信息!','warning');
		}
	}else if (type == 'importantPapersApply') { //申请浏览
		var row = $('#importantPapersApply_lagrid').datagrid('getSelected');
		if (row) {
			$('#importantPapersApply_ladg').show();
			$('#lafrm').form('clear');
			$('#laId').val(row.id);
			$('#importantPapersApply_ladg').dialog({
				title: '修改申请信息',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor0 = KindEditor.create('.[name="applyReason"]', {
					themeType: 'qq',
					items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
					resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="applyReason"]');
			},
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('importantPapersApply');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#importantPapersApply_ladg').dialog('close');
			}
			}]
			});
			url = '/qx/importantPapersApply!loadImportantPapersApplyById';
			$.ajax({
				type: "POST",
				url: url,
				data: {
				id: row.id
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$('#submitDate').datetimebox('setValue', obj.submitDate); // set datetimebox value
					editor0.html(obj.applyReason);
					$('#memo').val(obj.memo);
				} else {
					$.messager.show({ // show error message
						title: '提示',
						msg: obj.errorMsg
					});
				}
			}
			});
		} else {
			$.messager.alert('提示', '请选择你要编辑的申请!', 'warning');
		}

		//加载用户信息
		$.ajax({
			url: "/qx/importantPapersApply!loadImportantPapersApplyToUser",
			data: {},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					$("#userNameId").val(obj.userName);
					$("#userNameId").attr('readonly', 'readonly');
					$("#userNameId").addClass("bgcolor");

					//部门
					$("#departname").val(obj.departName);
					$("#departname").attr('readonly', 'readonly');
					$("#departname").addClass("bgcolor");

					$("#departmentId").val(obj.departmentId);

				}
			}
		});

	}else if (type == 'damageReport') { //申请浏览
		var row = $('#damageReport_wpgrid').datagrid('getSelected');
		if (row) {
			$('#damageReport_addRId').show();
			$('#damageReportfrm').form('clear');
			$('#drId').val(row.id);
			$('#damageReport_addRId').dialog({
				title: '修改损坏报告申请',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.95,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor0 = KindEditor.create('textarea[name="reasonDesc"]', {
					themeType: 'qq',
					items: ['bold', 'italic', 'underline', 'fontname', 'fontsize', 'forecolor', 'hilitecolor', 'plug-align', 'plug-order', 'plug-indent', 'link'],
					resizeType: 1
				});
			},
				onBeforeClose: function() {
					KindEditor.remove('textarea[name="reasonDesc"]');
				},
				buttons: [{
					text: '提交',
					handler: function() {
					opt.save('damageReport');
				}
				},
				{
					text: '取消',
					handler: function() {
					$('#damageReport_addRId').dialog('close');
				}
				}]
			});
			
			$("#materialNameId").val(row.materialName);
			$("#specModelId").val(row.specModel);
			$("#fileDate").datebox('setValue', row.fileDate);	
			editor0.html(row.reasonDesc);

			//加载用户信息
			$("#userNameId").val(row.userName);
			$("#userNameId").attr('readonly', 'readonly');
			$("#userNameId").addClass("bgcolor");

			//部门
			$("#departname").val(row.departmentName);
			$("#departname").attr('readonly', 'readonly');
			$("#departname").addClass("bgcolor");
			
			
			var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_DAMAGE_TYPE";
			$("#damageTypeId").combobox({ 
				url:configUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				panelHeight: "auto",
				onLoadSuccess: function () { 
					$('#damageTypeId').combobox('select',row.damageType);
				}
			});
			
			//物资类别
			var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_MATERIAL_TYPE";
			$("#materialTypeId").combobox({ 
				url:configUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				panelHeight: "auto",
				onLoadSuccess: function () { 
					$('#materialTypeId').combobox('select', row.materialType);
				}
			});
			
			
			//责任认定
			var configUrl = "/qx/config!queryConfigByDomainCode?dataDomain=BUSI_RESPONSIBILITY_CONFIRM";
			$("#responsibilityConfirmId").combobox({ 
				url:configUrl,
				textField: "text",
				valueField: "id",
				multiple: false,
				editable:false,
				panelHeight: "auto",
				onLoadSuccess: function () { 
					$('#responsibilityConfirmId').combobox('select', row.responsibilityConfirm);
				}
			});
			
			$("#unitPriceId").val(row.unitPrice);
			//支付费用
			if(row.billCost){
				$("#companyId").attr("checked","checked");
			}else{
				$("#personalId").attr("checked","checked");
			}
		} else {
			$.messager.alert('提示', '请选择你要编辑的申请!', 'warning');
		}
	}
}

/****
 * 定义刷新操作
 * */
opt.refresh = function(type) {
	if(type=="tranPostion"){
		$('#tranPostion_tpgrid').datagrid('reload');
	}else if (type == 'notice') { //公告刷新
		$('#noticelist_noticegrid').datagrid("reload");
	}else if(type=='position'){
		$('#position_list').datagrid('reload');
	}else if(type=='leaveApply'){
		$('#leaveApply_lagrid').datagrid('reload');
	}else if(type=='process'){
		$('#process_progrid').datagrid('reload');
	}else if(type=='procesNode'){
		$('#processNode_pngrid').datagrid('reload');
	}else if(type=='workPlan'){
		$('#workPlan_wpgrid').datagrid('reload');
	}else if(type=='remind'){
		$('#remind_wpgrid').datagrid('reload');
	}else if(type=='trainingApply'){
		$('#training_wpgrid').datagrid('reload');
	}else if(type=='recruitmentApply'){
		$('#recruitment_wpgrid').datagrid('reload');
	}else if(type=='custiomServiceRec'){
		$('#custiomService_wpgrid').datagrid('reload');	
	}else if(type=='documentApply'){
		$('#documentApply_wpgrid').datagrid('reload');
	}else if(type=='projectManageMent'){
		$('#project_wpgrid').datagrid('reload');
	}else if(type=='personalThingManage'){
		$('#personalThing_wpgrid').datagrid('reload');
	}else if(type=='workLog'){
		$('#workLog_wpgrid').datagrid('reload');
	}else if(type=='eventReport'){
		$('#eventReport_wpgrid').datagrid('reload');
	}else if(type=='appeal'){
		$('#appealgrid').datagrid("reload");
	}else if (type == "materialPurchase") {
		$('#materialPurchase_lagrid').datagrid('reload');
	}else if (type == "carApply") {
		$('#carApply_lagrid').datagrid('reload');
	} else if (type == "materialApply") {
		$('#materialApply_lagrid').datagrid('reload');
	}else if (type == "meetingApply") {
		$('#meetingApply_lagrid').datagrid('reload');
	}else if (type == "marketingManagement") {
		$('#marketingManagement_lagrid').datagrid('reload');
	}else if(type=='video'){   //刷新视频
		$('#videogrid').datagrid("reload");
	}else if(type=="specialAccountMan"){
		$('#specialAccountMan_lagrid').datagrid('reload');
	}else if(type=='importPapersMan'){
		$('#importPapersMan_wpgrid').datagrid('reload');
	}else if (type == "importantPapersApply") {
		$('#importantPapersApply_lagrid').datagrid('reload');
	}else if (type == "damageReport") {
		$('#damageReport_wpgrid').datagrid('reload');
	}

}



/****
 * 复制操作
 ***/
opt.copy = function(type){
	if(type=="workPlan"){   
		var row = $('#workPlan_wpgrid').datagrid('getSelected');
		if (row) {
			$.messager.confirm('提示', '你确定复制【' + row.title + '】?',
					function(r) {
				if (r) {
					$.ajax({
						url: "/qx/workPlan!copyWorkPlan",
						data: {
						id:row.id
					},
					success: function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
							$('#workPlan_wpgrid').datagrid('reload');
							$.messager.show({ 
								title: '提示',
								msg: "复制工作计划成功!"
							});
						} else {
							$.messager.show({
								title: '提示',
								msg: "复制工作计划失败!"
							});
						}
					}
					});
				}
			});
		}else{
			$.messager.alert('提示', '请选择你要复制的工作计划!', 'warning');
		}
	}
}


/***
 *查看详情
 **/
opt.browse = function(type,dom,isCheck){
	//审核前隐藏
	$("#nextMutiShenId").hide();
	$("#nextShenId").html("");
	$("#nextNodeShenId").html("");


	if(type=="leaveApply"){                /***1.查看请假***/
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.leaveProperty);
			$("#browse3").html(row.departmentName);
			$("#browse4").html(row.positionName);
			$("#browse5").html(row.startDate+"至"+row.endDate);
			$("#browse6").html(row.applyReason);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$.messager.alert('提示', '请选择你要'+title+'的记录!', 'warning');
		}
	}else if(type=="trainingApply"){           /***2.查看培训***/
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.title);
			$("#browse4").html(row.traineeUsers);
			$("#browse5").html(row.trainingRequirement);
			$("#browse6").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$.messager.alert('提示', '请选择你要'+title+'的记录!', 'warning');
		}
	}else if(type=='recruitmentApply'){    /***3.查看招聘***/
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.applyDate);
			$("#browse4").html(row.requirementDesc);
			$("#browse6").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$.messager.alert('提示', '请选择你要'+title+'的记录!', 'warning');
		}
	}else if(type=='custiomServiceRec'){    /***4.查看客服工单***/
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.createDate);
			$("#browse5").html(row.serviceTypeName);
			$("#browse4").html(row.serviceDesc);
			$("#browse6").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$.messager.alert('提示', '请选择你要'+title+'的记录!', 'warning');
		}
	}else if(type=='projectManageMent'){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "查看";
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				onOpen: function() {
				editor1 = KindEditor.create('textarea[name="plContent"]', {
					themeType : 'qq',
					items : [
					         'bold','italic','underline','fontname','fontsize','forecolor','hilitecolor','plug-align','plug-order','plug-indent','link'
					         ],
					         resizeType: 1
				});
			},
			onBeforeClose: function() {
				KindEditor.remove('textarea[name="plContent"]');
			},
			});

			$("#proId").html(row.id);
			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.submitDate);
			$("#browse4").html(row.handlerName);
			$("#browse6").html(row.projectContent);


			$("#btnId").show();
			$("#contentTRId").show();

			//显示项目评论轨迹
			$('.search-background').fadeIn(50);
			$("#content").load("/qx/projectManageMent!queryProjectTraceByPage?page=1&projectId="+row.id, $('.search-background').fadeOut(100));

			$.ajax({
				url : '/qx/projectManageMent!queryProjectTraceToTotal',
				data : {
				page : 1,
				projectId:row.id
			},
			success : function(data) {
				var htmlStr = "<ul style='width: 504px; padding:0px; margin:8px;'>";  
				var obj = eval('('+data+')');
				var num = obj.total;
				for(var i=1;i<=num;i++){
					htmlStr = htmlStr +"<li id='"+i+"'>"+i+"</li>";
				}
				htmlStr=htmlStr+"</ul>";
				$("#paging_button").html(htmlStr);
				$("#1").css({'background-color' : '#006699'});
				//执行
				$("#paging_button li").click(function(){
					$('.search-background').fadeIn(50);
					$("#paging_button li").css({'background-color' : ''});
					$(this).css({'background-color' : '#006699'});
					$("#content").load("/qx/projectManageMent!queryProjectTraceByPage?page=" + this.id+"&projectId="+row.id, $('.search-background').fadeOut(50));
					return false;
				});
			}
			});

		}else{
			title = "查看";
			$.messager.alert('提示', '请选择你要'+title+'的记录!', 'warning');
		}
	}else if(type=='documentApply'){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.docTitle);
			$("#browse4").html(row.content);
			$("#browse6").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/attachments!queryAttachmentsToDocumentSee",
				data: {
				id:row.id
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:150px;'>文件名称</th><th style='background-color:#F0F0F0;width:100px;'>创建时间</th><th style='background-color:#F0F0F0;width:80px;'>下载地址</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].attachmentName+"</td>"+
					"<td>"+obj.datas[i].createTime+"</td>"+
					"<td>"+obj.datas[i].downOpt+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse8").html(htmlStr);
			}
			});

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$.messager.alert('提示', '请选择你要'+title+'的记录!', 'warning');
		}
	}else if(type=='notice'){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.title);
			$("#browse4").html(row.content);
			$("#browse6").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$.messager.alert('提示', '请选择你要'+title+'的记录!', 'warning');
		}
	}else if(type=='eventReport'){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			$('#eventSeeTableId').show();
			$('#eventSeeTableId').dialog({
				title: "查看",
				width: $(window).width() * 0.9,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			$("#eventTable").load('/qx/eventReport!queryEventReportSee?id='+row.id)
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=="tranPostion"){                /***查看调岗***/
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentNameOld);
			$("#browse3").html(row.applyDate);
			$("#browse4").html("从&nbsp;【"+row.positionNameOld+"】&nbsp;调往&nbsp;【"+row.positionNameNew+"】");
			$("#browse5").html(row.salary);
			$("#browse6").html(row.applyReson);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:"transferPosition"
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$.messager.alert('提示', '请选择你要'+title+'的记录!', 'warning');
		}
	}if(type=='goOutApply'){   //start abiao
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userId);
			$("#browse2").html(row.departmentId);
			$("#browse3").html(row.positionId);
			$("#browse4").html(row.applyDate);
			$("#browse5").html(row.leaveStartDate +"至"+row.leaveEndDate);
			$("#browse6").html(row.goOutReason);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=="appeal"){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userId);
			$("#browse2").html(row.departmentId);
			$("#browse3").html(row.positionId);
			$("#browse4").html(row.complainDate);
			$("#browse5").html(row.complainContent);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:"attendanceComplainApply"
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse6").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=="video"){
		var row = dom.datagrid('getSelected'); 
		if(row){
			$('#browse').show();
			$('#browse').dialog({
				title: '观看视频',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.videoName);
			$("#browse2").html(row.videoUrl);
			$("#browse3").html(row.videoSize);
			$("#browse4").html(row.submitDate);
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=="materialPurchase"){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.applyDate);
			$("#browse4").html(row.applyReason);
			$("#browse5").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=="carApply"){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.applyDate);
			$("#browse4").html(row.applyReason);
			$("#browse5").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=="materialApply"){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.applyDate);
			$("#browse4").html(row.applyReason);
			$("#browse5").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=="meetingApply"){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.applyDate);
			$("#browse4").html(row.applyReason);
			$("#browse5").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=="video"){
		var row = dom.datagrid('getSelected'); 
		if(row){
			$('#browse').show();
			$('#browse').dialog({
				title: '观看视频',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.videoName);
			$("#browse2").html(row.videoUrl);
			$("#browse3").html(row.videoSize);
			$("#browse4").html(row.submitDate);
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=="importantPapersApply"){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.submitDate);
			$("#browse4").html(row.applyReason);
			$("#browse5").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}else if(type=='damageReport'){
		var row = dom.datagrid('getSelected'); 
		if(row){
			//标题变化
			var title = "";
			if(isCheck==1){
				title = "审批";
			}else{
				title = "查看";
			}
			$('#browse').show();
			$('#browse').dialog({
				title: title,
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true
			});

			//显示详情
			$("#browse1").html(row.userName);
			$("#browse2").html(row.departmentName);
			$("#browse3").html(row.submitDate);
			$("#browse4").html(row.applyReason);
			$("#browse5").html(row.memo);

			//流程信息
			$.ajax({
				url: "/qx/util!queryProcessTrace",
				data: {
				id:row.id,
				busiName:type
			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				var htmlStr = "<table style='width:100%' border='1px;' ><tr align='center'><th style='background-color:#F0F0F0;width:70px;'>处理人</th><th style='background-color:#F0F0F0;width:70px;'>职位</th><th style='background-color:#F0F0F0;width:80px;'>处理结果</th><th style='background-color:#F0F0F0;width:100px;'>处理时间</th><th style='background-color:#F0F0F0;width:200px;'>意见</th></tr>";
				for ( var i = 0; i < obj.datas.length; i++) {
					htmlStr = htmlStr + "<tr align='center'>"+
					"<td>"+obj.datas[i].userName+"</td>"+
					"<td>"+obj.datas[i].positionName+"</td>"+
					"<td>"+obj.datas[i].nextNodeName+"</td>"+
					"<td>"+obj.datas[i].handleDate+"</td>"+
					"<td>"+obj.datas[i].handleCase+"</td>"+
					"</tr>";
				}
				htmlStr = htmlStr+"</table>";
				$("#browse7").html(htmlStr);
			}
			});

			//审核意见
			$("#opinionId").html(row.radioUrl)

			//是否显示审核按钮
			if(isCheck==1){  //显示审核按键
				$("#btnId").show();
				$("#opinionTRId").show();
				$("#contentTRId").show();
			}else{
				$("#btnId").hide();
				$("#opinionTRId").hide();
				$("#contentTRId").hide();
			}
		}else{
			$.messager.alert('提示', '请选择你要查看的记录!', 'warning');
		}
	}
}


/***
 * 审核操作
 */
opt.check = function(){
	var nextNode = $('input[name="nextNodeId"]:checked').val();
	var nextNodeName = $('input[name="nextNodeId"]:checked').attr("title");;
	var content = $('#contentId').val();
	var id = $("#check_Id").html();
	var departId = $("#check_departId").html();
	var classType = $("#check_classType").html();
	var puStatusId = $("#puStatusId").html();

	var userMuti = "";
	if(puStatusId==4){   //需要选择下一位
		userMuti = $("#userMutiId").combobox('getValue')
	}

	if(nextNode==undefined){
		$.messager.alert('提示', '请选择审批意见?','warning');
		return false;
	}
	$.messager.confirm('提示', '你确定审批该记录?', function(r){
		if (r){
			$.ajax({
				url : '/qx/util!changeNextNodeUtil',
				data : {
				id : id,
				nextNode:nextNode,
				classType:classType,
				departId:departId,
				nextNodeName:nextNodeName,
				content:content,
				userMuti:userMuti
			},
			success : function(data) {
				var obj = eval('('+data+')');
				if (obj.result==1){
					$.messager.show({
						msg : "审批成功!",
						title : '提示'
					});
					$('#browse').dialog('close');
					refreshDom.datagrid('reload');
				} else {
					$.messager.show({
						msg :  "审批失败,请检查网络问题!",
						title : '提示'
					});
				}
			}
			});
		}
	});


}


/***
 * 审核操作2
 */
function optProcessNode(classType,id,departId,nextNode,nextNodeName){
	$.messager.confirm('提示', '你确定执行【'+nextNodeName+'】操作?', function(r){
		if (r){
			$.ajax({
				url : '/qx/util!changeNextNodeUtil',
				data : {
				id : id,
				nextNode:nextNode,
				classType:classType,
				departId:departId,
				nextNodeName:nextNodeName
			},
			success : function(data) {
				var obj = eval('('+data+')');
				if (obj.result==1){
					$.messager.show({
						msg : obj.errorMsg,
						title : '提示'
					});
					refreshDom.datagrid('reload');
				} else {
					$.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
					});
				}
			}
			});
		}
	});
}




//获取操作时候
$(function(){
	$("#nextMutiShenId").hide();
	$("input[name='nextNodeId']").live('click',function(){
		var nodeSign = $(this).val();
		$.ajax({
			url : '/qx/processNode!findNextNodeToDo',
			data : {
			nextId:nodeSign
		},
		success : function(data) {
			var obj = eval('('+data+')');
			$("#puStatusId").html(obj.result);  //下一位类型
			$("#nextNodeShenId").html(obj.nextNodeName);
			if (obj.result==1){
				$("#nextShenId").html("");
				$("#nextMutiShenId").hide();
			}else if(obj.result==2){ //主管
				$("#nextShenId").html(obj.labelStr);
				$("#nextMutiShenId").hide();
			}else if(obj.result==3){ //个人职位
				$("#nextShenId").html(obj.labelStr);
				$("#nextMutiShenId").hide();
			}else if(obj.result==4){
				$("#nextShenId").html("");
				var posUrl = "/qx/processNode!findNextNodePosUser?nextId="+nodeSign;
				$("#posMutiId").combobox({ 
					url:posUrl,
					textField: "text",
					valueField: "id",
					multiple: false,
					panelHeight: "auto",
					editable:false,
					onChange: function(oldVal, newVal)  {
					$("#userMutiId").combobox({ 
						url:"/qx/processNode!findNextNodeUsers?nextId="+nodeSign+"&posCode="+oldVal,
						textField: "text",
						valueField: "id",
						multiple: false,
						panelHeight: "auto",
						editable:false,
						onLoadSuccess: function () { 
						var data = $('#userMutiId').combobox('getData');
						if (data.length > 0) {
							$('#userMutiId').combobox('select', data[0].id);
						} 
					}
					});
				},
				onLoadSuccess: function () { 
					var data = $('#posMutiId').combobox('getData');
					if (data.length > 0) {
						$('#posMutiId').combobox('select', data[0].id);
						//用户
						$("#userMutiId").combobox({ 
							url:"/qx/processNode!findNextNodeUsers?nextId="+nodeSign+"&posCode="+data[0].id,
							textField: "text",
							valueField: "id",
							multiple: false,
							panelHeight: "auto",
							editable:false,
							onLoadSuccess: function () { 
							var data = $('#userMutiId').combobox('getData');
							if (data.length > 0) {
								$('#userMutiId').combobox('select', data[0].id);
							} 
						}
						});
					} 
				}
				});

				$("#nextMutiShenId").show();
			}   //end 4
			else if(obj.result==0){
				$("#nextMutiShenId").hide();
				$("#nextShenId").html("");
			}
		}
		});
	});
})






















/****
 * 确认信息
 ****/
opt.reply = function(type,tabDom){
	if(type=='workPlan'){
		var row = tabDom.datagrid('getSelected');
		if (row) {
			var userStatus = row.userComStatus;
			$('#workPlan_replyId').show();
			$('#workPlan_replyId').dialog({
				title: '确认工作计划',
				width: $(window).width() * 0.8,
				height: $(window).height() * 0.90,
				padding: 5,
				modal: true,
				buttons: [{
					text: '提交',
					handler: function() {
					saveItemToComfirm();
				}
				},
				{
					text: '取消',
					handler: function() {
					$('#workPlan_replyId').dialog('close');
				}
				}]
			});

			$("#wpCodeId").html(row.wpCode);
			$("#titId").html(row.title);
			$("#jieshouId").html(row.departUserName);
			$("#dateId").html(row.yearMonth);
			//显示
			$.ajax({
				url: "/qx/workItem!queryWorkItemToComfirm",
				data: {
				wpCode:row.wpCode,
				departUser:row.departUser

			},
			success: function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					var htmlStr1 = "<tr align='center'><th style='width:70px;'>序号</th><th style='width:500px;'>工作项</th><th style='width:100px;'>主管确认</th><th style='width:100px;'>职工确认</th></tr>";
					for ( var i = 0; i < obj.datas.length; i++) {
						var xl = i+1;
						var dcom = "";
						var ecom = "";
						if(obj.status==1){   //主管自己
							if(obj.datas[i].directorCofirm==1){
								dcom = dcom+"<input type='checkbox' name='dcomfirm'  checked='checked' class='dcomfirm' value='"+obj.datas[i].id+"'>";
							}else{
								dcom = dcom+"<input type='checkbox' name='dcomfirm' class='dcomfirm' value='"+obj.datas[i].id+"'>";
							}

							if(obj.datas[i].employeeConfirm==1){
								if(userStatus==2){
									ecom = ecom+"已确认&nbsp;<img src='/qx/images/icon/can.png'>"
								}else{
									ecom = ecom+"<input type='checkbox' name='ecomfirm'  checked='checked' class='ecomfirm' value='"+obj.datas[i].id+"'>"
								}
							}else{
								ecom = ecom+"<input type='checkbox' name='ecomfirm' class='ecomfirm' value='"+obj.datas[i].id+"'>"
							}
						}else if(obj.status==2){  //主管别人
							if(obj.datas[i].directorCofirm==1){
								dcom = dcom+"<input type='checkbox' name='dcomfirm'  checked='checked' class='dcomfirm' value='"+obj.datas[i].id+"'>";
							}else{
								dcom = dcom+"<input type='checkbox' name='dcomfirm' class='dcomfirm' value='"+obj.datas[i].id+"'>";
							}

							if(obj.datas[i].employeeConfirm==1){
								ecom = ecom+"已确认&nbsp;<img src='/qx/images/icon/can.png'>";
							}else{
								ecom = ecom+"未确认&nbsp;<img src='/qx/images/icon/not.png'>";
							}
						}else if(obj.status==3){  //员工自己
							if(obj.datas[i].directorCofirm==1){
								dcom = dcom+"已确认&nbsp;<img src='/qx/images/icon/can.png'>";
							}else{
								dcom = dcom+"未确认&nbsp;<img src='/qx/images/icon/not.png'>";
							}

							if(obj.datas[i].employeeConfirm==1){
								if(userStatus==2){
									ecom = ecom+"已确认&nbsp;<img src='/qx/images/icon/can.png'>"
								}else{
									ecom = ecom+"<input type='checkbox' name='ecomfirm'  checked='checked' class='ecomfirm' value='"+obj.datas[i].id+"'>"
								}
							}else{
								ecom = ecom+"<input type='checkbox' name='ecomfirm' class='ecomfirm' value='"+obj.datas[i].id+"'>"
							}
						}
						htmlStr1 = htmlStr1 + "<tr align='center'><td>"+xl+"</td><td style='text-align:left;'>"+obj.datas[i].workItem+"</td>" +
						"<td>"+dcom+"</td>" +
						"<td>"+ecom+"</td>" +
						"</tr>";
					}
					$(".confrimTabId").html(htmlStr1);
				}
			}
			});
		}else{
			$.messager.alert('提示', '请选择你要确认的工作计划!', 'warning');
		}
	}
}




//接收人
opt.activity = function(type){
	var row = $('#marketingManagement_lagrid').datagrid('getSelected');
	if (row) {
		if(row.marketingInfoType=="营销活动"){
			$('#setMarkeingUserId').dialog('open').dialog('setTitle',"设置营销活动");
			$("#check_allgroup").removeAttr("checked");
			$("#marketinId").html(row.id);
			//加载角色
			$.ajax({
				url : '/qx/marketingManagement!queryAllMarketingManagementToUser',
				data : {id:row.id},
				success : function(data) {
					var obj = eval('('+data+')');
					if (obj.result==1){
						var butHtml = "";
						for ( var i = 0; i < obj.datas.length; i++) {
							if(obj.datas[i].sign==1){
								butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='usersChe' checked='checked' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
							}else{
								butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='usersChe' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
							}

							if(i%4==0&&i!=0){
								butHtml = butHtml+ "<br/><br/>"
							}
						}
						
						if(butHtml==""){
							$("#zzUserDiv").html("<div style='color:red;font-size:15px;font-weight:800;text-align: center;vertical-align:middle;line-height:200px;'>没有相关的站长!</div>");
						}else{
							$("#zzUserDiv").html(butHtml);
						}
					} else {
						$.messager.show({
							msg :  "加载失败!",
							title : '提示'
						});
					}
				}
			});
		}else{
			$('#setMarkeingTargetId').dialog('open').dialog('setTitle',"设置营销目标");
			$("#check_allgroup").removeAttr("checked");
			$("#marId").html(row.id);
			//加载角色
			$.ajax({
				url : '/qx/marketingManagement!queryAllMarketingManagementToUser',
				data : {id:row.id},
				success : function(data) {
					var obj = eval('('+data+')');
					if (obj.result==1){
						var butHtml = "";
						for ( var i = 0; i < obj.datas.length; i++) {
							if(obj.datas[i].sign==1){
								butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='radio' name='userIdRadio' class='usersChe' checked='checked' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
							}else{
								butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='radio' name='userIdRadio' class='usersChe' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
							}

							if(i%4==0&&i!=0){
								butHtml = butHtml+ "<br/><br/>"
							}
						}
						
						if(butHtml==""){
							$("#zhUserDiv").html("<div style='color:red;font-size:15px;font-weight:800;text-align: center;vertical-align:middle;line-height:200px;'>没有相关的站长!</div>");
						}else{
							$("#zhUserDiv").html(butHtml);
						}
						
					} else {
						$.messager.show({
							msg : "加载失败!",
							title : '提示'
						});
					}
				}
			});
		}
	}else{
		$.messager.alert('提示', '请选择你要设置的接收人!', 'warning');
	}
}