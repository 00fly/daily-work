<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>My JSP 'orgList.jsp' starting page</title>
		<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
		<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
		<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
		<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
		<link rel="stylesheet" type="text/css"
			href="css/mast.css">
		<script type="text/javascript">
	var toolbar;
	$(function(){ 
	var treegrid = $('#orgTree').treegrid({    
    url:'org!queryOrgList',  
    title: '组织机构列表',  
    animate: true,  
    fit: true,//自动大小 
    idField:'id',    
    treeField:'orgName', 
    lines:true,   
    columns:[[    
        {field:'id',title:'id',width:300,hidden:true},  
        {field:'orgName',title:'组织机构名称',width:300},   
        {field:'orgCode',title:'组织机构编码',width:200},  
        {field:'orgDuty',title:'职责',width:250},    
        {field:'enabled',title:'是否可用',width:80,align:'center',formatter: imgcheckbox}
    ]],
    toolbar:[{
				text : '展开',
				iconCls : 'icon-expend',
				handler : function() {
					var node = treegrid.treegrid('getSelected');
					if (node) {
						treegrid.treegrid('expandAll', node.id);
					} else {
						treegrid.treegrid('expandAll');
					}
				}
			}, '-', {
				text : '折叠',
				iconCls : 'icon-suoxiao',
				handler : function() {
					var node = treegrid.treegrid('getSelected');
					if (node) {
						treegrid.treegrid('collapseAll', node.id);
					} else {
						treegrid.treegrid('collapseAll');
					}
				}
			}, '-',{
            text:'添加',
            iconCls:'icon-add',
            handler:function(){
                form.form('clear');
				var row = $('#orgTree').treegrid('getSelected');
				win.window('open');
				$("#isAbleId").attr("checked","checked");
				$("#noOlieId").attr("checked","checked");
				
				if(row){
					url = 'org!loadAddOrgInfoByCode?orgCode='+row.orgCode;
					$('#fmId').form('load',url);
				}else{
					url = 'org!loadAddOrgInfoByCode?orgCode=1';
					$('#fmId').form('load',url);
				}
				$('#comTreeId').combotree({
					url: 'org!queryOrgComTree',
					required: true,
					onLoadSuccess : function(row, data) {
					var t = $(this);
					if (data) {
						$(data).each(function(index, d) {
							if (this.state == 'closed') {
								t.tree('expandAll');
							}
						});
					}
				}
				});
            }
        },'-',{
            text:'修改',
            iconCls:'icon-edit',
            handler:function(){
            	var row = $('#orgTree').treegrid('getSelected');
				if(row){
					win.window('open');
					url = 'org!loadOrgInfoByCode?orgCode='+row.orgCode;
					$('#fmId').form('load',url);
					//
					$('#comTreeId').combotree({
						url: 'org!queryOrgComTree',
						required: true,
						onLoadSuccess : function(row, data) {
						var t = $(this);
						if (data) {
							$(data).each(function(index, d) {
								if (this.state == 'closed') {
									t.tree('expandAll');
								}
							});
						}
					}
					});
					
				}else{
					$.messager.alert('提示','请选择你要修改的组织机构!','warning');
				}
        }
        },'-',{
            text:'删除',
            iconCls:'icon-remove',
            handler:function(){
            var row = $('#orgTree').treegrid('getSelected');
			if(row){
				$.messager.confirm('提示','你确定删除【'+row.orgName+'】?',function(r){
					if (r){
						$.ajax({
							type: "POST",
							url: "org!deleteOrgInfo",
							data: {orgCode:row.orgCode},
							success: function(data){
								var obj = eval('('+data+')');
								if (obj.result==1){
									/*
									for ( var i = 0; i < obj.datas.length; i++) {
										$("tr[node-id='"+obj.datas[i].orgCode+"']").remove();
									}
									 */
									$('#orgTree').treegrid("reload");
									$.messager.show({    // show error message
										title: '提示',
										msg: obj.errorMsg
									});
	
								} else if(obj.result==3){
									$.messager.show({    // show error message
										title: '提示',
										msg: obj.errorMsg
									});
								}else{
									$.messager.show({    // show error message
										title: '提示',
										msg: obj.errorMsg
									});
								}
							}
						});
					}
				});
			}else{
				$.messager.alert('提示','请选择你要删除的机构!','warning');
			}
            }
        },'-',{
            text:'刷新',
            iconCls:'icon-reload',
            handler:function(){
              $('#orgTree').treegrid("reload");
            }
        }],onLoadSuccess : function(row, data) {
							var t = $(this);
							if (data) {
								$(data).each(function(index, d) {
									if (this.state == 'closed') {
										t.treegrid('expandAll');
									}
								});
							}
			 }
	});   

    //end 列表
    //win面板
    $('#btn-save,#btn-cancel').linkbutton();
	win = $('#provider-window').window({
		closed:true,
		modal : true
	});
	form = win.find('form');
});
        
    var win;
	var form;
	var url;
    
    //保存组织
    function saveOrg(){
       url = "org!saveOrgByPar"
       $('#fmId').form('submit',{
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(data){
                    var obj = eval('('+data+')');
                    if (obj.result==1){
                        $('#provider-window').dialog('close');        // close the dialog
                        $('#orgTree').treegrid('reload');    // reload the user data
                    } else {
                          $.messager.show({
                            title: '提示',
                            msg: obj.errorMsg
                        });
                    }
                }
            });
    }
    
    
   //是否可用图标显示
	var imgcheckbox = function (cellvalue, options, rowObject) {
        return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
    }; 
    
    
    //设置部门负责人
    function giveOrgToLeader(){
        var row = $('#orgTree').treegrid('getSelected');
		if (row) {
		        $('#orgLeaderId').show();
		        $("#leaderFrm").form('clear');
		        $("#orgId").html(row.orgCode);
				$('#orgLeaderId').dialog({ 
				title:'设置负责人-部门【'+row.orgName+'】', 
				width:$(window).width() * 0.5,
				height:$(window).height() * 0.55,
				padding:5,
				modal:true,
				buttons: [{ 
					text: '提交', 
					handler: function() { 
						saveOrgToLeader();
					} 
				}, { 
					text: '取消', 
					handler: function() { 
						$('#orgLeaderId').dialog('close'); 
					} 
				}] 
				}); 
				
				
				
				
				//设置关联方法的值
				var orgUrl = "user!queryLeaderByCode?orgCode="+row.orgCode;
				$("#optId").combobox({
					url:orgUrl,
					textField: "text",
					valueField: "id",
					multiple: false,
					panelHeight: "auto",
					onLoadSuccess: function() {
						//设置执行方法
						$("#optId").combobox('setValue',row.orgLeader);		
					}
				});
				
		}else{
		     $.messager.alert('提示', '请选择你要设置的部门!', 'warning');
		}
    }
    
    
    //保存
    function saveOrgToLeader(){
            var userId = $("#optId").combobox('getValue');
            var orgCode = $("#orgId").html();
            $.ajax({
				url : 'org!saveLeaderToOrg',
				data : {
					userId:userId,
					orgCode:orgCode
				},
				success : function(data) {
					var obj = eval('('+data+')');
	                if (obj.result==1){
					    $.messager.show({
							msg :  "设置领导成功!",
							title : '提示'
					    });
					    $('#orgTree').treegrid("reload");
					    $('#orgLeaderId').dialog('close'); 
					} else {
						$.messager.show({
							msg :  "设置领导失败!",
							title : '提示'
					    });
					}
				}
			  });
    }
	</script>
	</head>

	<body class="easyui-layout">
	    <!-- 主栏 -->
		<div region="center" border="false">
			<table id="orgTree" data-options="toolbar:toolbar">
			</table>
		</div>
		<!-- end主栏 -->
		
		<!-- 添加 -->
		<div id="provider-window" title="编辑组织机构"  style="width:500px;height:350px;" data-options="minimizable:false,maximizable:false,collapsible:false">
		<div style="padding:20px 30px 40px 30px;">
			<form method="post" id="fmId">
				<input type="text" name="orgCode" style="display:none"/>
				<table width="350px" align="center" style="align: center" border="0" class="tab">
					<tr>
						<td class="form_label" style="width:100px;text-align:right;">部门名称：</td>
						<td class="form_content" >
						<input name="orgName" type="text" style="width:200px;" class="easyui-validatebox" maxlength="32" data-options="required:true,validType:'length[1,32]'"></input>
						</td>
					</tr>
					<tr>
						<td class="form_label" style="width:100px;text-align:right;">是否可用：</td>
						<td class="form_content" >
                            <input type="radio" name="enabled" value="1" id="isAbleId"/>可用&nbsp;&nbsp;
                            <input type="radio" name="enabled" value="0" id="noAbleId"/>禁用&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       </td>
					</tr>
					<tr>
						<td class="form_label" style="width:100px;text-align:right;">父&nbsp;节&nbsp;点：</td>
						<td class="form_content" ><input id="comTreeId" class="txt" name="parCode" style="width:200px;"></td>
					</tr>
					<tr>
						<td class="form_label" style="width:100px;text-align:right;">部门职责：</td>
						<td class="form_content" ><textarea style="height:60px;width:200px" name="orgDuty"></textarea></td>
					</tr>
					<tr>
						<td class="form_label" colspan="2" style="text-align: center">
							<a href="javascript:void(0)" onclick="saveOrg()" id="btn-save" >保存</a>
							<a href="javascript:void(0)" onclick="javascript:$('#provider-window').dialog('close')" id="btn-cancel" >取消</a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<!-- end添加 -->
	    <!-- 设置领导 -->
		<div id="orgLeaderId" style="display:none;">
		    <form action="" id="leaderFrm">
		     <span id="orgId" style="display:none;"></span>
		     <div align="center">
		        <br/>
		        <div style="font-size:20px;">设置部门负责人</div>
		        <br/>
		        <br/>
		        <br/>
		        负责人：<input type='text' id="optId" name="opter">
		     </div>
		    </form>
		</div>
	    <!-- end设置领导 -->
	</body>
</html>
