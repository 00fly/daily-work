<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
        <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
		<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
		<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
		<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript"
			src="js/syUtil.js"></script>
		<script type="text/javascript"
			src="js/jwoptUtil.js"></script>
		<link rel="stylesheet" type="text/css"
			href="css/myCss.css">
		<link rel="stylesheet" type="text/css"
			href="css/mast.css">
    <script type="text/javascript" charset="utf-8">
    var searchPositionForm;
	var addCount = 0;
	var url ;
	$(function() {
	    searchPositionForm = $('#searPositionfm').form();
	    
		$('#position_list').datagrid({ 
	        title:'职位列表', 
	        url:"position!queryPositionAnsPage?orgCode="+0,
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:10,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'poliId',title:'编码',width:200,hidden:'true'},
				{field:'names',title:'名称',width:200,align:'center'},
				{field:'posumber',title:'职位编号',width:200,align:'center'},
				{field:'orgName',title:'所属部门',width:200,align:'center'}
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	                opt.add('position'); 
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	                opt.edit('position');  //titleId
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	                opt.remove('position');
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				   opt.refresh('position');
				}
			}]
	    }); 
	    //组织树
	    $('#treeOrg').tree({
               url:'org!queryOrgTreeToUser?parCode=1',
               lines:true,
               animate:true,
			   onBeforeExpand:function(node,param){                         
			        $('#treeOrg').tree('options').url = "org!queryOrgTreeToUser?parCode="+node.id;  
		       },
               onClick:function(node){  
			        $("#orgCodeId").html(node.id);
			        $("#orgCodeName").html(node.text);
			        var url = "position!queryPositionAnsPage?orgCode="+node.id;
			        $('#position_list').datagrid({
					    url:url
					});
			   },onLoadSuccess : function(row, data) {
							var t = $(this);
							if (data) {
								$(data).each(function(index, d) {
									if (this.state == 'closed') {
										t.tree('expandAll');
									}
								});
							}
			 }
		});
	});
	
	
	 //查询
    function searchPosition(){
        $('#position_list').datagrid('load', sy.serializeObject(searchPositionForm));
    }
    //重置
    function crearPosition(){
        searchPositionForm.find('input').val('');
    }
	
	
	//刷新组织tree
	function refreshUserOrg(){
	    $('#treeOrg').tree({
               url:'org!queryOrgTreeToUser?parCode=1',
               lines:true,
               animate:true,
			   onBeforeExpand:function(node,param){                         
			        $('#treeOrg').tree('options').url = "org!queryOrgTreeToUser?parCode="+node.id;  
		       },
               onClick:function(node){  
			        $("#orgCodeId").html(node.id);
			        $("#orgCodeName").html(node.text);
			        var url = "position!queryPositionAnsPage?orgCode="+node.id;
			        $('#position_list').datagrid({
					    url:url
					});
			   },onLoadSuccess : function(row, data) {
							var t = $(this);
							if (data) {
								$(data).each(function(index, d) {
									if (this.state == 'closed') {
										t.tree('expandAll');
									}
								});
							}
			 }
		});
	}
	
	
	//展开
	function openUserOrg(){
	    $('#treeOrg').tree('expandAll');
	}
	
	//折叠
	function closeUserOrg(){
	   $('#treeOrg').tree('collapseAll');
	}
   var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
   };  
        
	var win;
	var form;
    
	// 保存用户
    function savePostion(){
        var orgCode = $("#orgCodeId").html();
        var id = $("#posId").html();
     	    var url = "position!savePosition?orgCode="+orgCode+"&id="+id;
    		$('#posfm').form('submit', {
            url: url,
            onSubmit: function() {
                return $(this).form('validate');
            },
            success: function(data) {
                var obj = eval('(' + data + ')');
                if (obj.result == 1) {
                    $('#position_dg').dialog('close');
                    $('#position_list').datagrid("reload");
                    $.messager.show({
                        title: '提示',
                        msg: obj.errorMsg
                    });
                } else {
                    $.messager.show({
                        title: '提示',
                        msg: obj.errorMsg
                    });
                }
            }
        });
    }
    
    //刷新用户
    function refreshUser(){
       $('#position_list').datagrid('reload');
    }
    
    
    // 验证
    function setValidation(){
		$("#name").validatebox({
			required: true,
			validType:['length[0,20]'],
			missingMessage:"用户名称不能为空",
			invalidMessage:"长度不能超过10个汉字"
		});
	}
	// 关闭窗口
    function closeWindow(){
    	win.window('close');
    }
    
    
    //设置职位负责人
    function givePosToLeader(){
        var row = $('#position_list').datagrid('getSelected');
		if (row) {
		        $('#posLeaderId').show();
		        $("#posId").html(row.poliId);
				$('#posLeaderId').dialog({ 
				title:'设置负责人-职位【'+row.names+'】', 
				width:$(window).width() * 0.5,
				height:$(window).height() * 0.55,
				padding:5,
				modal:true,
				buttons: [{ 
					text: '提交', 
					handler: function() { 
						saveOrgToLeader();
					} 
				}, { 
					text: '取消', 
					handler: function() { 
						$('#posLeaderId').dialog('close'); 
					} 
				}] 
				}); 
				
				
				//设置关联方法的值
				var orgUrl = "user!queryLeaderByCode?orgCode="+row.orgCode;
				$("#optId").combobox({
					url:orgUrl,
					textField: "text",
					valueField: "id",
					multiple: false,
					panelHeight: "auto",
				});
				
				//设置执行方法
				$("#optId").combobox('setValue',row.orgLeader);
		}else{
		     $.messager.alert('提示', '请选择你要设置的职位!', 'warning');
		}
    }
    
</script>
<style type="text/css">
.form_label{
   text-align:right;
}

.ssId{
   width:140px;
}

.bgcolor{
   background-color:#F2F2F2;
}
</style>
</head>
<body class="easyui-layout">
    <div data-options="region:'west',split:true,title:'组织机构',iconCls:'icon-book'"
				style="width: 250px;height:100%">
				<div style="border: 1px solid #ddd;">
					<div id="toolbar" style="vertical-align: middle">
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-expend" plain="true" onclick="openUserOrg()">展开</a>
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-suoxiao" plain="true" onclick="closeUserOrg()">折叠</a>
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload" plain="true" onclick="refreshUserOrg()">刷新</a>
					</div>
				</div>
				<div>
					<ul id="treeOrg" class="ztree">
					</ul>
				</div>
	</div>
	<div region="center" border="false">
	      <div class="easyui-layout" data-options="fit:true">
                <div data-options="region:'north',split:true,border:false,title:'查询栏'" style="height:95px;background-color:#F0F0F0;">
                   <fieldset>
					  <legend>查询</legend>
					  <form method="post" id="searPositionfm">  
					    名称: <input type="text" name="names" class="txt"/>&nbsp;
					  <a href="javascript:void(0)" onclick="searchPosition()" class="easyui-linkbutton" >查询</a>
					  <a href="javascript:void(0)" onclick="crearPosition()" class="easyui-linkbutton"  >清空</a>
					  </form>
					</fieldset>
                </div>
                <div data-options="region:'center',border:false">
	                <table id="position_list" cellspacing="0" cellpadding="0" >  
			        </table>
                </div>
          </div>
	</div>
	<span id="codeDIVId" style="display:none;"></span>
       <!-- 添加职位界面 -->
       <div id="position_dg" class="easyui-dialog"
			style="width: 400px; height: 250px; text-align: center; padding: 10px 20px"
			closed="true" buttons="#postion-buttons"   data-options="modal:true">
			<h2 id="titleId">
				添加职位
			</h2>
			<form id="posfm" method="post" novalidate>
			    <span id="posId" style="display:none;"></span>
			    <span id="orgCodeId" style="display:none;"></span>
				<center>
				<table class='tab' cellpadding="7">
				   <tr>
				      <td>
				         <label>
						 所属单位:
					     </label>
				      </td>
				      <td><span id="orgCodeName"></span></td>
				   </tr>
				    <tr>
				      <td>
				         	<label>
							职位名称:
							</label>
				      </td>
				      <td><input name="names" id="namesId" type="text" class="easyui-validatebox" required="true"></td>
				   </tr>
				</table>
				</center>
			</form>
		</div>
		<div id="postion-buttons">
			<a href="javascript:void(0)" class="easyui-linkbutton"
				onclick="savePostion()">保存</a>
			<a href="javascript:void(0)" class="easyui-linkbutton"
				onclick="javascript:$('#position_dg').dialog('close')">取消</a>
		</div>
	   <!-- end添加用户界面 -->
	   
	   
	    <!-- 设置领导 -->
		<div id="posLeaderId" style="display:none;">
		    <form action="" id="leaderFrm">
		     <span id="posId" style="display:none;"></span>
		     <div align="center">
		        <br/>
		        <div style="font-size:20px;">设置职位负责人</div>
		        <br/>
		        <br/>
		        <br/>
		        负责人：<input type='text' id="optId" name="opter">
		     </div>
		    </form>
		</div>
	    <!-- end设置领导 -->
	   
</body>
</html>