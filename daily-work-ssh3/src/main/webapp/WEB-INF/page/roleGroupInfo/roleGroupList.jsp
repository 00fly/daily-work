<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>My JSP 'buttonList.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
	<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
	<link rel="stylesheet" type="text/css"
			href="css/myCss.css">
	<script type="text/javascript">
	$(function(){
	    $('#roleGroupgrid').datagrid({ 
	        title:'角色组列表', 
	        url:"rolegroup!queryRoleListByPage",
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:10,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        fitColumns:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'groupId',title:'编码',width:200,hidden:'true'},
				{field:'groupName',title:'名称',width:150,align:'center'},
				{field:'enabled',title:'是否可用',width:90,align:'center', formatter: imgcheckbox},
				{field:'groupDesc',title:'备注',width:200,align:'center'},
				{field:'optStr',title:'操作',width:250,align:'center'}
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	                $('#addRoleGroupId').dialog('open').dialog('setTitle','添加');
					$("#titleId").html("添加角色组");
					$('#fmbut').form('clear');
					$("#isAbleId").attr("checked","checked");
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	               var row = $('#roleGroupgrid').datagrid('getSelected');
					if (row){
						$('#addRoleGroupId').dialog('open').dialog('setTitle','修改');
						$("#titleId").html("修改角色组");
						url = 'rolegroup!loadRoleGroupById?groupId='+row.groupId;
						$('#fmbut').form('load',url);
					}else{
						$.messager.alert('提示','请选择你要修改的角色组!','warning');
					}
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	                var row = $('#roleGroupgrid').datagrid('getSelected');
					if (row){
						$.messager.confirm('提示','你确定删除【'+row.groupName+'】?',function(r){
							if (r){
								$.ajax({
									type: "POST",
									url: "rolegroup!deleteRoleGroup",
									data: {groupId:row.groupId},
									success: function(data){
										var obj = eval('('+data+')');
										if (obj.result==1){
											$('#roleGroupgrid').datagrid("reload"); 
											$.messager.show({    // show error message
												title: '提示',
												msg: obj.errorMsg
											}); 
										} else {
											$.messager.show({    // show error message
												title: '提示',
												msg: obj.errorMsg
											});
										}
									}
								});
							}
						});
					}else{
						$.messager.alert('提示','请选择你要删除的角色组!','warning');
					}
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				   $('#roleGroupgrid').datagrid("reload");
				}
			}]
	    });
	});
	
	
	//保存按键
	function saveRoleGroup(){
	        url = "rolegroup!saveRoleGroup"
            $('#fmbut').form('submit',{
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(data){
                    var obj = eval('('+data+')');
                    if (obj.result==2){
                        $.messager.show({
                            title: 'Error',
                            msg: result.errorMsg
                        });
                    } else {
                        $('#addRoleGroupId').dialog('close');        // close the dialog
                        $('#roleGroupgrid').datagrid("reload");    // reload the user data
                    }
                }
            });
	}
	
	
	//设置角色组
	function giveRoleToGroup(groupId){
	   $('#setRoleId').dialog('open').dialog('setTitle','设置角色');
	   $("#roleToGroId").html(groupId);
	   $("#check_group").removeAttr("checked");
	   
	   //加载按钮
	   $.ajax({
			url : 'rolegroup!queryAllRolesToGroup',
			data : {groupId:groupId},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
					var butHtml = "";
					for ( var i = 0; i < obj.datas.length; i++) {
						if(obj.datas[i].sign==1){
						   butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='groupCk' checked='checked' value='"+obj.datas[i].roleId+"'"+obj.datas[i].roleName+"/>"+obj.datas[i].roleName+"</a>";
						}else{
						   butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='groupCk' value='"+obj.datas[i].roleId+"'"+obj.datas[i].roleName+"/>"+obj.datas[i].roleName+"</a>";
						}
						
				        if(i%4==0&&i!=0){
				           butHtml = butHtml+ "<br/><br/>"
				        }
				    }
				    
				    $("#rolesDiv").html(butHtml);
				    editRow = undefined;
				} else {
					$.messager.show({
					msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		});
	}
	
	
	//设置用户角色组
	function giveUserToGroup(groupId,groupName){
	    $('#userdiaId').show();
	    $("#usersDiv").html("");
	    $("#roleGroIdToUser").html(groupId);
		$('#userdiaId').dialog({ 
		title:'设置用户-角色组'+groupName, 
		width:$(window).width() * 0.8,
		height:$(window).height() * 0.95,
		padding:5,
		modal:true,
		buttons: [{ 
			text: '提交', 
			handler: function() { 
				saveUserRoleGroup();
			} 
		}, { 
			text: '取消', 
			handler: function() { 
				$('#userdiaId').dialog('close'); 
			} 
		}] 
		}); 
		
		
		//组织树
	    $('#treeOrg').tree({
               url:'org!queryOrgTreeToUser?parCode=1',
               lines:true,
               animate:true,
			   onBeforeExpand:function(node,param){                         
			        $('#treeOrg').tree('options').url = "org!queryOrgTreeToUser?parCode="+node.id;  
		       },
               onClick:function(node){  
			        $("#orgCodeToUser").html(node.id);
			        var userUrl = "user!queryAllUserByOrgCodeToGroup?orgCode="+node.id+"&groupId="+groupId;
			         $.ajax({
						   type: "POST",
						   url: userUrl,
						   success: function(data){
							   var obj = eval('('+data+')');
	                           if (obj.result==1){
	                                var userHtml = "";
	                                if(obj.datas.length==0){
	                                    userHtml="<div style='color:red;position:absolute;top:50%;left:45%'>没有你要找的数据!</div>"
	                                }else{
	                                    for ( var i = 0; i < obj.datas.length; i++) {
										if(obj.datas[i].sign==1){
										   	   userHtml = userHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' checked='checked' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
										}else{
										   	   userHtml = userHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' value='"+obj.datas[i].userId+"'/>"+obj.datas[i].userName+"</a>";
										}
									        if(i%6==0&&i!=0){
									           userHtml = userHtml+ "<br/><br/>"
									        }
								        }
	                                }
								    $("#usersDiv").html(userHtml);
	                           } else {
								 $.messager.show({    // show error message
								     title: '提示',
								     msg: obj.errorMsg
								 });
	                           }
						   }
						});
			  },onLoadSuccess : function(row, data) {
						var t = $(this);
						if (data) {
								$(data).each(function(index, d) {
									if (this.state == 'closed') {
									t.tree('expandAll');
								}
							});
						}
			 }
		});
	}
	
	//保存角色与组关联表
	function saveRolesToGroup(){
	    var groupId = $("#roleToGroId").html();
	    var str = $("input[type='checkbox']");
		var objarray = str.length;
		var strBuffer = "";
		for (i = 0; i < objarray; i++) {
			if (str[i].checked == true) {
					modelid = str[i].value;
					strBuffer += modelid;
					strBuffer += ",";
		    }
		}
		
		if($("input[type='checkbox']:checked").size()>0){
		    $.ajax({
			url : 'rolegroup!saveRoleToGroup',
			data : {
				strBuffer:strBuffer,
				groupId:groupId
			},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
				    $.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				    $('#setRoleId').dialog('close'); 
				} else {
					$.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		  });
		}else{
		   $.messager.show({
				msg :  "请选择你要设置的角色!",
				title : '提示'
				});
		}
	}
	
	
	//保存用户角色组
	function saveUserRoleGroup(){
	    var groupId = $("#roleGroIdToUser").html();
	    var orgCode = $("#orgCodeToUser").html();
	    var str = $("#usersDiv input[type='checkbox']");
		var objarray = str.length;
		var strBuffer = "";
		for (i = 0; i < objarray; i++) {
			if (str[i].checked == true) {
					modelid = str[i].value;
					strBuffer += modelid;
					strBuffer += ",";
		    }
		}
		if($("#usersDiv input[type='checkbox']:checked").size()>0){
		    $.ajax({
			url : 'rolegroup!saveUserToRoleGroup',
			data : {
				strBuffer:strBuffer,
				groupId:groupId,
				orgCode:orgCode
			},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
				    $.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				    $('#userdiaId').dialog('close'); 
				} else {
					$.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		  });
		}else{
		   $.messager.show({
				msg :  "请选择你要设置的角色!",
				title : '提示'
				});
		}
	    
	}
	
	//是否可用图标显示
	var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
    }; 
    
    
    $(function(){
         $("#check_group").click(function(){
        	if(this.checked){ 
				$(".groupCk").each(function(){this.checked=true;}); 
			}else{ 
				$(".groupCk").each(function(){this.checked=false;}); 
			} 
	    });
	    
    });
    
	</script>

    <style type="text/css">
    .form_label{
    text-align:right;
    }
    </style>
  
  <body class="easyui-layout">
        <div region="center" border="false">
			<table id="roleGroupgrid">
			</table>
		</div>
		
 <!-- 添加用户界面 -->
  <div id="addRoleGroupId" class="easyui-dialog" data-options="modal:true"
			style="width: 400px; height: 300px; text-align: center; padding: 10px 20px"
			closed="true" >
			<form method="post" id="fmbut" >
				<h2 id="titleId"></h2>
				<input name="groupId" style="display:none;"/>
				<table width="300px" align="center" style="align: center" border="0">
					<tr>
						<td class="form_label" style="width:150xp;">名称：</td>
						<td class="form_content" ><input name="groupName" class="easyui-validatebox" data-options="required:true" maxlength="32"></input></td>
					</tr>
					<tr>
						<td class="form_label">备注：</td>
						<td class="form_content" ><textarea style="height:60px;width:200px" name="groupDesc"></textarea></td>
					</tr>
					<tr>
						<td class="form_label">是否可用：</td>
						<td class="form_content" >
						<input type="radio" name="enabled" value="1" id="isAbleId"/>可用&nbsp;&nbsp;
                        <input type="radio" name="enabled" value="0" id="noAbleId"/>禁用&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td class="form_label" colspan="2" style="text-align: center">
							<a href="javascript:void(0)" onclick="saveRoleGroup()" class="easyui-linkbutton">保存</a>
							<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#addRoleGroupId').dialog('close')">取消</a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	<!-- end添加用户界面 -->
	
	<!-- 设置角色 -->
    <div id="setRoleId" class="easyui-dialog"
			style="width: 700px; height: 500px;  padding: 5px 5px;"
			closed="true" data-options="modal:true,buttons: [{
                    text:'保存',
                    handler:function(){
                       saveRolesToGroup();
                    }
                },{
                    text:'取消',
                    handler:function(){
                       $('#setRoleId').dialog('close');
                    }
                }]">
			<form method="post" id="fmbut" >
				<span id="roleToGroId" style="display:none;"></span>
				<div><p><img src="images/icon/duoxuan.png"/><span style="font-size:16px;font-weight:700;">&nbsp;请选择页面角色</span></p></div>
				<div><hr/></div>
				<div><input type="checkbox" id="check_group"/><span style="font-size:16px;">全选/反选</span></div>
				<br/>
				<div id="rolesDiv" class="abox" style="width:98%"></div>
			</form>
		</div>
		
		<!-- 设置用户 -->
		<div id="userdiaId" style="display:none;">
		     <span id="roleGroIdToUser" style="display:none;"></span>
		     <span id="orgCodeToUser" style="display:none;"></span>
		     <div class="easyui-layout" data-options="fit:true">
		       	<div data-options="region:'west',split:true,title:'组织机构'" style="width:350px">
		       	    <div>
						<ul id="treeOrg" class="ztree">
						</ul>
					</div>
		       	</div>
	            <div data-options="region:'center',title:'选择用户'" style="padding:10px;">
	                 <div id="usersDiv" class="Cbox" style="width:95%">
	                       
	                 </div>
	            </div>
		     </div>
		</div>
  </body>
</html>
