<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>邮件</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
	<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="js/syUtil.js" ></script>
	<link rel="stylesheet" type="text/css"
			href="css/mast.css">
	<script type="text/javascript">
	
	$(function(){
	    $('#sendEmailGrid').datagrid({ 
	        title:'发送邮箱列表', 
	        url:"sendEmail!querySendEmailByPage",
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:10,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'id',title:'编码',width:200,hidden:'true'},
				{field:'name',title:'名称',width:150,align:'center'},
				{field:'email',title:'邮箱账号',width:180,align:'center'},
				{field:'password',title:'独立密码',width:180,align:'center'},
				{field:'smtp',title:'SMTP服务器',width:180,align:'center'},
				
				{field:'memo',title:'备注',width:250,align:'center'}
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	                $.ajax({
						type: "POST",
						url: "sendEmail!checkIsSendEmail",
						data: {},
						success: function(data){
							var obj = eval('('+data+')');
							if (obj.result==1){
							   $.messager.show({    // show error message
									title: '提示',
									msg: "<span style='font-size:15px;'>已经存在发送邮箱了,不需要重复添加,可以修改现有邮箱!</span>"
								});
							}else if(obj.result==2){
							     	$('#addSendEmailId').show();
									$('#sefrm').form('clear');
									$("#titleId").html("添加发送邮箱");
									$('#tabId').val(0);
									$('#addSendEmailId').dialog({
										title: '添加发送邮箱',
										width: 430,
										height: 350,
										padding: 5,
										modal: true,
										buttons: [{
											text: '提交',
											handler: function() {
											 saveSendEmail();
										}
										},
										{
											text: '取消',
											handler: function() {
											$('#addSendEmailId').dialog('close');
										}
										}]
									});
							}else{
								$.messager.show({    // show error message
									title: '提示',
									msg: "请检查网络是否链接正常!"
								});
							}
						}
					});
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	               var row = $('#sendEmailGrid').datagrid('getSelected');
					if (row){
						$('#addSendEmailId').show();
						$('#sefrm').form('clear');
						$('#seId').val(row.id);
						$('#addSendEmailId').dialog({
							title: '修改发送邮箱',
							width: 430,
						    height: 350,
							padding: 5,
							modal: true,
							buttons: [{
								text: '提交',
								handler: function() {
								saveSendEmail();
							}
							},
							{
								text: '取消',
								handler: function() {
								$('#addSendEmailId').dialog('close');
							}
							}]
						});
						
						$("#seId").val(row.id);
						$("#nameId").val(row.name);
						$("#memoId").val(row.memo);
						$("#passwordId").val(row.password);
						$("#emailId").val(row.email);
					}else{
						$.messager.alert('提示','请选择你要修改的发送邮箱!','warning');
					}
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	                var row = $('#sendEmailGrid').datagrid('getSelected');
					if (row){
						$.messager.confirm('提示','你确定删除【'+row.name+'】?',function(r){
							if (r){
								$.ajax({
									type: "POST",
									url: "sendEmail!deleteSendEmail",
									data: {id:row.id},
									success: function(data){
										var obj = eval('('+data+')');
										if (obj.result==1){
											$('#sendEmailGrid').datagrid("reload"); 
											$.messager.show({    // show error message
												title: '提示',
												msg: obj.errorMsg
											}); 
										} else if(obj.result==2){
											$.messager.show({    // show error message
												title: '提示',
												msg: obj.errorMsg
											});
										}else{
											$.messager.show({    // show error message
												title: '提示',
												msg: "请检查网络是否链接正常!"
											});
										}
									}
								});
							}
						});
					}else{
						$.messager.alert('提示','请选择你要删除的发送邮箱!','info');
					}
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				   $('#sendEmailGrid').datagrid("reload"); 
				}
			}]
	    });
	});
	
	//是否可用图标显示
	var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
    }; 
    
    //保存发送邮箱
    function saveSendEmail(){
    //判断值是否为合法
	var flag = false;
	$('#sefrm').form('submit', {
		onSubmit: function() {
		flag = $(this).form('validate');
	}
	});

	if(!flag){
		return flag;
	}
	//end判断值是否为合法
	
    $.ajax({
		type: "POST",
		url: "sendEmail!saveSendEmail",
		data: {
			id:$("#seId").val(),
			name:$("#nameId").val(),  //名称
			email:$("#emailId").val(),  //邮件
			password:$("#passwordId").val(),  //密码
			memo:$("#memoId").val(),  //备注
		},
		success: function(data){
			var obj = eval('('+data+')');
			if (obj.result==1){
				$('#sendEmailGrid').datagrid("reload"); 
				$('#addSendEmailId').dialog('close');
				$.messager.show({    // show error message
					title: '提示',
					msg: obj.errorMsg
				}); 
			} else if(obj.result==2){
				$.messager.show({    // show error message
					title: '提示',
					msg: obj.errorMsg
				});
			}else {
				$.messager.show({    // show error message
					title: '提示',
					msg: "请检查网络是否链接正常!"
				});
			}
		}
	});
    }
    
    $(function(){
         $("#check_group").click(function(){
        	if(this.checked){ 
				$(".groupCk").each(function(){this.checked=true;}); 
			}else{ 
				$(".groupCk").each(function(){this.checked=false;}); 
			} 
	    });
	    
    });
    
	</script>

    <style type="text/css">
    .form_label{
    text-align:right;
    }
    </style>
  
  <body class="easyui-layout">
        <div region="center" border="false">
			<table id="sendEmailGrid">
			</table>
		</div>
	
         <!-- 添加发送邮箱界面 -->
  			<div id="addSendEmailId" >
			<center>
			<form method="post" id="sefrm" >
				<br/>
				<br/>
				<input name="id" id="seId" style="display:none;">
				<br/>
				<br/>
				<table width="300px" align="center" style="align: center;" cellpadding="7" border="0">
					<tr>
						<td class="form_label" style="width:150xp;">名称：</td>
						<td class="form_content" ><input type="text" name="name" id="nameId" class="easyui-validatebox" data-options="required:true" maxlength="10"></input></td>
					</tr>
					<tr>
						<td class="form_label" style="width:150xp;">邮箱账号：</td>
						<td class="form_content" ><input type="text" name="email" id="emailId" class="easyui-validatebox" data-options="required:true,validType:'email'" maxlength="50"></input></td>
					</tr>
					<tr>
						<td class="form_label" style="width:150xp;">独立密码：</td>
						<td class="form_content" ><input type="text" name="password" id="passwordId" placeholder=邮箱登陆密码  class="easyui-validatebox" data-options="required:true" maxlength="50"></input>&nbsp;&nbsp;<a href="http://www.baidu.com/s?wd=独立密码&ie=utf-8" target="_blank" title="什么是独立密码？">百度?</a></td>
					</tr>
					<tr>
						<td class="form_label">备注：</td>
						<td class="form_content" >
						<input type="text" name="memo" id="memoId">
						</td>
					</tr>
				</table>
			</form>
			</center>
		</div>
		
  </body>
</html>
