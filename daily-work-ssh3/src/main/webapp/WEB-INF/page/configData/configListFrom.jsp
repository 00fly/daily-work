<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>数据字典</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
		<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
		<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
		<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>

        <link rel="stylesheet" type="text/css"
			href="css/mast.css">
		<script type="text/javascript">
        var url;
        //新添
        function newUser(){
            $('#dlg').dialog('open').dialog('setTitle','添加');
            $("#titleId").html("添加字典类别");
            $('#fm').form('clear');
            $("#typeId").show();
            $("#ydataTypeId").attr("checked","checked");
        }
        
        
        //修改
        function editUser(){
            var row = $('#treeDemo').tree('getSelected');
            if (row){
                $.ajax({
				   type: "POST",
				   url: "config!checkConfig",
				   data: {dataCode:row.id},
				   success: function(data){
				    var obj = eval('('+data+')');
                        if (obj.result==1){
                           	$('#dlg').dialog('open').dialog('setTitle','修改');
			                $("#titleId").html("修改字典类别");
			                url = 'config!loadConfigByCode?dataCode='+row.id;
			                $('#fm').form('load',url);
			                $("#typeId").hide();
                        } else if (obj.result==2){
                            $.messager.show({    // show error message
                                title: '提示',
                                msg: obj.errorMsg
                            });
                        }else {
                             $.messager.show({    // show error message
                                title: '提示',
                                msg: "请检查网络是否正常!"
                            });
                        
                        }
				   }
				});
            }else{
                $.messager.alert('提示','请选择你要修改的字典类别!','warning');
            }
        }
        
        //保存
        function saveUser(){
            url = "config!saveConfigData"
            $('#fm').form('submit',{
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(data){
                    var obj = eval('('+data+')');
                    if (obj.result==2){
                        $.messager.show({
                            title: 'Error',
                            msg: obj.errorMsg
                        });
                    } else {
                        $('#dlg').dialog('close');        // close the dialog
                        $('#treeDemo').tree('reload');    // reload the user data
                        $('#dagrid').datagrid('reload');
                    }
                }
            });
        }
        
        //删除
        function destroyUser(){
            var row = $('#treeDemo').tree('getSelected');
            if (row){
                $.messager.confirm('提示','你确定删除【'+row.text+'】?',function(r){
                    if (r){
                        $.ajax({
						   type: "POST",
						   url: "config!deleteConfig",
						   data: {dataCode:row.id},
						   success: function(data){
						    var obj = eval('('+data+')');
                            if (obj.result==1){
                                $('#treeDemo').tree('reload');    // reload the user data
                                 $('#dagrid').datagrid('reload');
                            } else if (obj.result==2){
                                $.messager.show({    // show error message
                                    title: '提示',
                                    msg: obj.errorMsg
                                });
                            }else {
                                 $.messager.show({    // show error message
                                    title: '提示',
                                    msg: "请检查网络是否正常!"
                                });
                            
                            }
						   }
						});
                    }
                });
            }else{
                $.messager.alert('提示','请选择你要删除的字典类别!','warning');
            }
        }
        
        //刷新父字典
        function refreshUser(){
           $('#treeDemo').tree('reload');
        }
        
        
        //初始化
        $(function(){
           $('#treeDemo').tree({
               url:'config!loadConfigTree'
           });
           
           //字典子操作
            $('#treeDemo').tree({
				onClick: function(node){
				    url = "config!querySonConfigByCode?dataCode="+node.id;
				    $('#dagrid').datagrid({
					    url:url,
					    fit: true,//自动大小 
					    columns:[[
					        {field:'dataCode',title:'类型编码',width:200,align:'center',hidden:'true'},
					        {field:'dataName',title:'名称',width:200,align:'center'},
					        {field:'dataDomain',title:'域名',width:200,align:'center'},
					        {field:'dataTypeCode',title:'数据编码',width:180,align:'center'},
					        {field:'order',title:'排序',width:80,align:'center'},
					        {field:'enabled',title:'是否可用',width:80,align:'center', formatter: imgcheckbox },
					        {field:'isopt',title:'是否可操作',width:200,align:'center',hidden:'true'},
					        {field:'isoptName',title:'是否可修改',width:100,align:'center'}
					        
					    ]],
					    singleSelect:"true",
					    toolbar: [{
							iconCls: 'icon-add',
							text:'添加',
							handler: function(){
							  	var row = $('#treeDemo').tree('getSelected');
								if(row){
									if(row.id==999){
										$('#dlg').dialog('open').dialog('setTitle','添加');
										$("#titleId").html("添加字典类别");
										$('#fm').form('clear');
									}else{
										$.ajax({
											url : 'config!loadConfigByCode',
											data : {
											dataCode : row.id
										},
										success : function(data) {
											var obj = eval('('+data+')');
											if(obj.dataType==1){
												$('#dlgson').dialog('open').dialog('setTitle','添加');
												$("#sonTitleId").html("添加字典");
												$('#fmson').form('clear');
												$("#dtId").hide();
												$("#dataTypeCodeId").val("cjw_1");
												$("#sortId").numberspinner('setValue', 1);
												$("#isAbleId").attr("checked","checked");
												$("#sonDataCode").val(row.id);
											}else{
												$('#dlgson').dialog('open').dialog('setTitle','添加');
												$("#sonTitleId").html("添加字典");
												$('#fmson').form('clear');
												$("#dtId").show();
												$("#sortId").numberspinner('setValue', 1);
												$("#isAbleId").attr("checked","checked");
												$("#sonDataCode").val(row.id);
											}
										  }
										});
									}
								}else{
									$.messager.alert('提示','请选择所属的字典类别!','warning');
								}
							}
						},'-',{
							iconCls: 'icon-edit',
							text:'修改',
							handler: function(){
							var row = $('#dagrid').datagrid('getSelected');
							if (row){
								if(row.isopt==1){
									$.messager.alert('提示','你没有权限修改系统级配置!','warning');
								}else{
									$('#dlgson').dialog('open').dialog('setTitle','修改');
									$("#sonTitleId").html("修改字典");
									
									$.ajax({
										url : 'config!loadSonConfigByCode',
										data : {
										dataCode : row.dataCode
									},
									success : function(data) {
										var obj = eval('('+data+')');
										if(obj.dataTypeCode=='cjw_1'){
											$("#dtId").hide();
										}else{
											$("#dtId").show();
										}
								 	 }
									});
									url = 'config!loadSonConfigByCode?dataCode='+row.dataCode;
									$('#fmson').form('load',url);
								}
							}else{
								$.messager.alert('提示','请选择你要修改的字典!','warning');
							}
                            }
						},'-',{
							iconCls: 'icon-remove',
							text:'删除',
							handler: function(){
							var row = $('#dagrid').datagrid('getSelected');
							if (row){
								if(row.isopt==1){
									$.messager.alert('提示','你没有权限删除系统级配置!','warning');
								}else{
									$.messager.confirm('提示','你确定删除【'+row.dataName+'】?',function(r){
										if (r){
											$.ajax({
												type: "POST",
												url: "config!deleteConfig",
												data: {dataCode:row.dataCode},
												success: function(data){
													var obj = eval('('+data+')');
													if (obj.result==1){
														$('#dagrid').datagrid('reload');    // reload the user data
													} else {
														$.messager.show({    // show error message
															title: '提示',
															msg: obj.errorMsg
														});
													}
												}
											});
										}
									});
								}
							}else{
								$.messager.alert('提示','请选择你要删除的字典!','warning');
							}
							}
						},'-',{
							iconCls: 'icon-reload',
							text:'刷新',
							handler: function(){
							   $('#dagrid').datagrid('reload');
							}
						}]
					});
				}
			});
			
			
        });
        
        var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
        };  
           
        
        //保存子配置
        function saveConfig(){
           url = "config!saveSonConfigData"
            $('#fmson').form('submit',{
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(data){
                    var obj = eval('('+data+')');
                    if (obj.result==1){
                        $('#dlgson').dialog('close');        // close the dialog
                        $('#dagrid').datagrid('reload');    // reload the user data
                    } else {
                          $.messager.show({
                            title: 'Error',
                            msg: obj.errorMsg
                        });
                    }
                }
            });
        }
        
    </script>

	</head>

	<body class="easyui-layout">
			<div data-options="region:'west',split:true,title:'字典类别',iconCls:'icon-book'"
				style="width: 300px;height:100%">
				<!-- toolBar栏 -->
				<div style="border: 1px solid #ddd;">
					<div id="toolbar" style="vertical-align: middle">
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-add" plain="true" onclick="newUser()">添加</a>
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-edit" plain="true" onclick="editUser()">修改</a>
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-remove" plain="true" onclick="destroyUser()">删除</a>
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload" plain="true" onclick="refreshUser()">刷新</a>
					</div>
				</div>
				<!-- end toolBar栏 -->
				<div>
					<ul id="treeDemo" class="ztree">
					</ul>
				</div>
			</div>
			<div id="tb"
				data-options="region:'center',title:'字典数据',iconCls:'icon-book'"
				style="height: auto" >
				<!-------------------------------详细信息展示表格----------------------------------->
				<table id="dagrid" style="width: auto" 
					iconcls="icon-view"></table>
			</div>

		<div id="dlg" class="easyui-dialog"
			style="width: 400px; height: 280px; text-align: center; padding: 10px 20px"
			closed="true" buttons="#dlg-buttons"   data-options="modal:true">
			<h2 id="titleId">
				添加字典类别
			</h2>
			<form id="fm" method="post" novalidate>
			    <input name="dataCode" style="display:none;"/>
				<div class="fitem">
					<label>
						名称:
					</label>
					<input name="dataName" class="easyui-validatebox" required="true">
				</div>
				<br />
				<div class="fitem">
					<label>
						域名:
					</label>
					<input name="dataDomain" class="easyui-validatebox" required="true">
				</div>
				<br />
				<div class="fitem" id="typeId">
					<label>
						类型:
					</label>
					<input type="radio" name="dataType" value="1" id="ydataTypeId"/>默认编号&nbsp;
                        <input type="radio" name="dataType" value="0" id="ndataTypeId"/>自定义编号&nbsp;
				</div>
			</form>
		</div>
		<div id="dlg-buttons">
			<a href="javascript:void(0)" class="easyui-linkbutton"
				onclick="saveUser()">保存</a>
			<a href="javascript:void(0)" class="easyui-linkbutton"
				onclick="javascript:$('#dlg').dialog('close')">取消</a>
		</div>
		<!-- 子配置框 -->
		<div id="dlgson" class="easyui-dialog"
			style="width: 400px; height: 320px; text-align: center; padding: 10px 20px"
			closed="true" buttons="#dlgson-buttons" data-options="modal:true">
			<h2 id="sonTitleId">
				添加字典
			</h2>
			<form id="fmson" method="post" novalidate>
			    <input name="dataCode" id="sonDataCode" style="display:none;"/>
				<div class="fitem">
					<label>
						字典名称:
					</label>
					<input name="dataName" id="datanameId" class="easyui-validatebox" required="true">
				</div>
				<br />
				<div class="fitem">
					<label>
						字典排序:
					</label>
					<input id="sortId" class="easyui-numberspinner" name="dataOrder" data-options="min:1,max:100,editable:false" style="width:155px;" ></input>
				</div>
				<br/>
				<div class="fitem" id="dtId">
					<label>
						数据编码:
					</label>
					<input name="dataTypeCode" id="dataTypeCodeId" class="easyui-validatebox" required="true">
				</div>
				<br/>
				<div class="fitem">
					<label>
						是否可用:
					</label>
					<input type="radio" name="enabled" value="1" id="isAbleId"/>可用&nbsp;&nbsp;
                    <input type="radio" name="enabled" value="0" id="noAbleId"/>禁用&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</div>
			</form>
		</div>
		<div id="dlgson-buttons">
			<a href="javascript:void(0)" class="easyui-linkbutton"
				onclick="saveConfig()">保存</a>
			<a href="javascript:void(0)" class="easyui-linkbutton"
				onclick="javascript:$('#dlgson').dialog('close')">取消</a>
		</div>
		<!-- end 子配置框 -->
	</body>
</html>
