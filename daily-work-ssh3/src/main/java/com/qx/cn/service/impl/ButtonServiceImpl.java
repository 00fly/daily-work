package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.Button;
import com.qx.cn.service.ButtonService;

@Service
public class ButtonServiceImpl implements ButtonService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List queryButtonsByPage(int page, int rows)
    {
        int firstPage = rows * (page - 1);
        int lastPage = rows * page - 1;
        String sql = "select * from sys_button au where au.i_button_id is not null order by au.c_button_sort asc limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public List<Button> queryAllButtons()
    {
        String hql = "from Button order by buttonSort asc ";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean saveButton(Button button)
    {
        baseDAO.saveOrUpdate(button);
        return true;
    }
    
    @Override
    public boolean deleteButton(Long buttonId)
    {
        baseDAO.deleteById(Button.class, buttonId);
        return true;
    }
    
    @Override
    public Button loadButtonById(Long buttonId)
    {
        return (Button)baseDAO.loadById(Button.class, buttonId);
    }
}