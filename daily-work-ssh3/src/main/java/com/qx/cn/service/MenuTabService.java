package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.MenuTabs;

public interface MenuTabService
{
    public Boolean saveMenuTab(MenuTabs paramMenuTabs);
    
    public List<MenuTabs> queryMenuTabByCode(String paramString);
    
    public boolean deleteMenuTab(String paramString);
    
    public boolean deleteTabMenu(Long paramLong);
}