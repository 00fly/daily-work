package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.Menu;
import com.qx.cn.tool.TreeNode;
import com.qx.cn.vo.MenuVO;

public interface MenuService
{
    public List<MenuVO> queryTreeGrid(Menu paramMenu);
    
    public List<TreeNode> queryComTree(Menu paramMenu, boolean paramBoolean);
    
    public List<Menu> queryMenuListByAsc(String paramString);
    
    public List<Menu> queryAllMenuListByAsc(String paramString);
    
    public boolean saveMenu(Menu paramMenu);
    
    public Menu loadMenuByCode(String paramString);
    
    public boolean deleteMenuByCode(String paramString);
    
    public boolean updateMenuInfo(MenuVO paramMenuVO);
    
    public List<MenuVO> queryMenusAndButTreeGrid(MenuVO paramMenuVO);
    
    public Menu loadMenuByURL(String paramString);
    
    public Menu loadMenuByType(String paramString);
    
    public List<Menu> queryMenuListByRoleAsc(String paramString);
    
    public List<Menu> queryMenuByType(String paramString);
    
    public List queryAllMenuURL();
    
}