package com.qx.cn.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.SendDate;
import com.qx.cn.service.SendDateService;

@Service
public class SendDateServiceImpl implements SendDateService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List querySendDateByPage(SendDate sdObj, int page, int rows)
    {
        int firstPage = rows * (page - 1);
        int lastPage = rows;
        String sqlStr = "";
        
        if (StringUtils.isNotEmpty(sdObj.getName()))
        {
            sqlStr = sqlStr + " and c_name like '%" + sdObj.getName() + "%'";
        }
        if (StringUtils.isNotEmpty(sdObj.getType()))
        {
            sqlStr = sqlStr + " and c_type = '" + sdObj.getType() + "'";
        }
        
        String sql = "select * from sys_send_date au where au.i_id is not null " + sqlStr + " order by au.c_name desc limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public boolean deleteSendDate(int id)
    {
        baseDAO.deleteById(SendDate.class, Integer.valueOf(id));
        return true;
    }
    
    @Override
    public SendDate loadSendDateById(int id)
    {
        return (SendDate)baseDAO.loadById(SendDate.class, Integer.valueOf(id));
    }
    
    @Override
    public List<SendDate> queryAllSendDates(SendDate sdObj)
    {
        String sqlStr = "";
        
        if ((!"".equals(sdObj.getName())) && (sdObj.getName() != null))
        {
            sqlStr = sqlStr + " and name like '%" + sdObj.getName() + "%'";
        }
        
        String hql = "from SendDate where name is not null " + sqlStr;
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean saveSendDate(SendDate reSendDate)
    {
        baseDAO.saveOrUpdate(reSendDate);
        return true;
    }
    
    @Override
    public SendDate queryAllSendDateByDate(String name, String type)
    {
        String hql = "from SendDate where name = '" + name + "'" + " and type like '%" + type + "%'";
        return (SendDate)baseDAO.loadByHql(hql);
    }
    
    @Override
    public SendDate querySendDateByAuto()
    {
        String hql = "from SendDate where name is not null order by name desc ";
        SendDate sendDate = new SendDate();
        List sendDates = baseDAO.query(hql);
        if (sendDates.size() > 0)
        {
            sendDate = (SendDate)sendDates.get(0);
        }
        
        return sendDate;
    }
}