package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.Button;

public interface ButtonService
{
    public List queryButtonsByPage(int paramInt1, int paramInt2);
    
    public List<Button> queryAllButtons();
    
    public boolean saveButton(Button paramButton);
    
    public boolean deleteButton(Long paramLong);
    
    public Button loadButtonById(Long paramLong);
}