package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.WorkSendGroup;

public interface WorkSendGroupService
{
    public List queryWorkSendGroupByPage(WorkSendGroup paramWorkSendGroup, int paramInt1, int paramInt2);
    
    public List<WorkSendGroup> queryAllWorkSendGroups(WorkSendGroup paramWorkSendGroup);
    
    public boolean deleteWorkSendGroup(int paramInt);
    
    public WorkSendGroup loadWorkSendGroupById(int paramInt);
    
    public boolean saveWorkSendGroup(WorkSendGroup paramWorkSendGroup);
    
    public List<WorkSendGroup> queryIsNotUserByUserId(Long paramLong);
}