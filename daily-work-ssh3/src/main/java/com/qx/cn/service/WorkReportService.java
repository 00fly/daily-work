package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.User;
import com.qx.cn.model.WorkReport;

public interface WorkReportService
{
    public List queryWorkReportByPage(WorkReport paramWorkReport, int paramInt1, int paramInt2);
    
    public List<WorkReport> queryAllWorkReports(WorkReport paramWorkReport);
    
    public boolean deleteWorkReport(int paramInt);
    
    public WorkReport loadWorkReportById(int paramInt);
    
    public boolean saveWorkReport(WorkReport paramWorkReport);
    
    public boolean queryCheckIsWorkReports(String date, Long userId, String type);
    
    public WorkReport loadWorkReportByUserAndDate(String date, Long userId);
    
    public WorkReport loadWorkReportByUserAndName(String name, Long userId);
    
    public List<WorkReport> queryAllWorkReportsByDate(String startDate, String endDate, User user, String type);
    
    public boolean deleteWorkReportCode(String paramString);
}