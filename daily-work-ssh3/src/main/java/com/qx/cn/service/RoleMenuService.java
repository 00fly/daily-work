package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.RoleMenu;

public interface RoleMenuService
{
    public boolean saveRoleMenu(RoleMenu roleMenu);
    
    public RoleMenu loadRoleMenuByCode(String paramString1, String paramString2);
    
    public List<RoleMenu> queryRoleMenusById(String roleId);
    
    public boolean deleteRoleMenusById(String roleId);
    
    public boolean updateRoleButsByMenuCode(String roleId, String paramString2, String paramString3);
    
    public boolean updateRoleButsByRoleId(String roleId);
    
    public boolean loadRoleMenuByAll(String paramString1, String paramString2, Long paramLong);
    
    public boolean deleteRoleMenusByCode(String paramString);
    
    public boolean loadRoleMenuByAllTabs(String paramString1, String paramString2, Long paramLong);
    
    public boolean updateRoleTabsByRoleId(String roleId);
    
    public boolean updateRoleTabsByMenuCode(String paramString1, String paramString2, String paramString3);
    
    public List<RoleMenu> queryRoleTabsById(String paramString1, String paramString2);
    
    public List<RoleMenu> queryRoleMenusByIdAndBT(String paramString);
    
    public List<RoleMenu> queryRoleButsById(String paramString1, String paramString2);
}