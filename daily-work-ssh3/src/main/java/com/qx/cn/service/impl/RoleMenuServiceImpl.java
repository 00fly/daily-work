package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.RoleMenu;
import com.qx.cn.service.RoleMenuService;

@Service
public class RoleMenuServiceImpl implements RoleMenuService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public boolean saveRoleMenu(RoleMenu roleMenu)
    {
        baseDAO.saveOrUpdate(roleMenu);
        return true;
    }
    
    @Override
    public RoleMenu loadRoleMenuByCode(String roleId, String menuCode)
    {
        String hql = "from RoleMenu rm where rm.menuId = '" + menuCode + "' and rm.roleId = '" + roleId + "'";
        return (RoleMenu)baseDAO.loadByHql(hql);
    }
    
    @Override
    public List<RoleMenu> queryRoleMenusById(String roleId)
    {
        String hql = "from RoleMenu rm where rm.roleId = '" + roleId + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteRoleMenusById(String roleId)
    {
        String hql = "delete from RoleMenu rm where rm.roleId = '" + roleId + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public boolean updateRoleButsByMenuCode(String roleId, String menuCode, String buttons)
    {
        String hql = "update RoleMenu r set r.buttonIds = '" + buttons + "' where r.roleId = '" + roleId + "' and r.menuId = '" + menuCode + "'";
        baseDAO.update(hql);
        return true;
    }
    
    @Override
    public boolean updateRoleButsByRoleId(String roleId)
    {
        String hql = "update RoleMenu r set r.buttonIds = null where r.roleId = '" + roleId + "'";
        baseDAO.update(hql);
        return true;
    }
    
    @Override
    public boolean loadRoleMenuByAll(String roleId, String menuCode, Long buttonId)
    {
        String hql = "from RoleMenu rm where rm.menuId = '" + menuCode + "' and rm.roleId = '" + roleId + "' and rm.buttonIds like '%" + buttonId + "%'";
        RoleMenu roleMenu = (RoleMenu)baseDAO.loadByHql(hql);
        if (roleMenu == null)
        {
            return false;
        }
        return true;
    }
    
    @Override
    public boolean loadRoleMenuByAllTabs(String roleId, String menuCode, Long tabId)
    {
        String hql = "from RoleMenu rm where rm.menuId = '" + menuCode + "' and rm.roleId = '" + roleId + "' and (rm.tabIds like '%," + tabId + ",%' or rm.tabIds like '" + tabId + ",%')";
        RoleMenu roleMenu = (RoleMenu)baseDAO.loadByHql(hql);
        if (roleMenu == null)
        {
            return false;
        }
        return true;
    }
    
    @Override
    public boolean deleteRoleMenusByCode(String menuCode)
    {
        String hql = "delete from RoleMenu rm where rm.menuId = '" + menuCode + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public boolean updateRoleTabsByRoleId(String roleId)
    {
        String hql = "update RoleMenu r set r.tabIds = null where r.roleId = '" + roleId + "'";
        baseDAO.update(hql);
        return true;
    }
    
    @Override
    public boolean updateRoleTabsByMenuCode(String roleId, String menuCode, String tabs)
    {
        String hql = "update RoleMenu r set r.tabIds = '" + tabs + "' where r.roleId = '" + roleId + "' and r.menuId = '" + menuCode + "'";
        baseDAO.update(hql);
        return true;
    }
    
    @Override
    public List<RoleMenu> queryRoleTabsById(String roleStr, String menuCode)
    {
        String hql = "from RoleMenu rm where rm.menuId = '" + menuCode + "' and rm.roleId = '" + roleStr + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<RoleMenu> queryRoleMenusByIdAndBT(String roleId)
    {
        String hql = "from RoleMenu rm where rm.roleId = '" + roleId + "' and (rm.tabIds is not null or rm.buttonIds is not null)";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<RoleMenu> queryRoleButsById(String roleStr, String menuCode)
    {
        String hql = "from RoleMenu rm where rm.menuId = '" + menuCode + "' and rm.roleId = '" + roleStr + "'";
        return baseDAO.query(hql);
    }
}