package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.Role;
import com.qx.cn.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List queryRolesByPage(int page, int rows)
    {
        int firstPage = rows * (page - 1);
        int lastPage = rows;
        String sql = "select * from sys_role au where au.i_role_id is not null limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public List<Role> queryAllRoles()
    {
        String hql = "from Role ";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteRole(Long roleId)
    {
        baseDAO.deleteById(Role.class, roleId);
        return true;
    }
    
    @Override
    public boolean saveRole(Role role)
    {
        baseDAO.saveOrUpdate(role);
        return true;
    }
    
    @Override
    public Role loadRoleById(Long roleId)
    {
        return (Role)baseDAO.loadById(Role.class, roleId);
    }
}