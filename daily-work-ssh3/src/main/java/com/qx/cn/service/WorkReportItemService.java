package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.WorkReportItem;

public interface WorkReportItemService
{
    public List<WorkReportItem> queryWorkReportItems(String paramString);
    
    public boolean saveWorkReportItem(WorkReportItem paramWorkReportItem);
    
    public WorkReportItem loadWorkItem(Integer paramInteger);
    
    public boolean deleteWorkItemById(Integer paramInteger);
    
    public boolean deleteWorkItemByCode(String paramString);
}