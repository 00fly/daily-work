package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.UserRoleGroup;
import com.qx.cn.service.UserRoleGroupService;

@Service
public class UserRoleGroupServiceImpl implements UserRoleGroupService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List<UserRoleGroup> queryUsersToGroupByOrgCode(String orgCode, Long groupId)
    {
        String hql = "from UserRoleGroup urg where urg.orgCode = '" + orgCode + "' and urg.groupId = " + groupId;
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteUserGroupRoles(String orgCode, Long groupId)
    {
        String hql = "delete from UserRoleGroup urg where urg.orgCode = '" + orgCode + "' and urg.groupId = " + groupId;
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public boolean saveUserRoleGroup(UserRoleGroup userRoleGroup)
    {
        baseDAO.saveOrUpdate(userRoleGroup);
        return true;
    }
    
    @Override
    public List<UserRoleGroup> queryGroupsByUserId(Long userId)
    {
        String hql = "from UserRoleGroup urg where urg.userId = " + userId;
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteGroupsUser(Long userId)
    {
        String hql = "delete from UserRoleGroup urg where urg.userId = " + userId;
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public boolean saveUserGroup(UserRoleGroup userGroup)
    {
        baseDAO.saveOrUpdate(userGroup);
        return true;
    }
    
    @Override
    public boolean deleteGroupsUserByGroupId(Long groupId)
    {
        String hql = "delete from UserRoleGroup urg where urg.groupId = " + groupId;
        baseDAO.deleteByHql(hql);
        return true;
    }
}