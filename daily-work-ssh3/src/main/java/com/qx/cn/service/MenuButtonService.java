package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.MenuButton;

public interface MenuButtonService
{
    public Boolean saveMenuButtin(MenuButton paramMenuButton);
    
    public List<MenuButton> queryMenuButtonByCode(String paramString);
    
    public boolean deleteMenuButton(String paramString);
    
    public List<MenuButton> queryButtonsByMenuCode(String paramString);
    
    public boolean deleteButtonMenu(String paramString);
}