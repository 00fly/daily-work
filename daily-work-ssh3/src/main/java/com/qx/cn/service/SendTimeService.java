package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.SendTime;

public interface SendTimeService
{
    public boolean saveSendTime(SendTime paramSendTime);
    
    public boolean deleteSendTime();
    
    public List<SendTime> queryAllTimeToShow();
}