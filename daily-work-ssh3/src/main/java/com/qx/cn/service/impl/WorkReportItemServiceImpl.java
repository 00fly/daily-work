package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.WorkReportItem;
import com.qx.cn.service.WorkReportItemService;

@Service
public class WorkReportItemServiceImpl implements WorkReportItemService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List<WorkReportItem> queryWorkReportItems(String wrCode)
    {
        String hql = "from WorkReportItem wr where wr.reportCode = '" + wrCode + "' order by wr.type, wr.id asc";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean saveWorkReportItem(WorkReportItem mpd)
    {
        baseDAO.saveOrUpdate(mpd);
        return true;
    }
    
    @Override
    public WorkReportItem loadWorkItem(Integer id)
    {
        return (WorkReportItem)baseDAO.loadById(WorkReportItem.class, id);
    }
    
    @Override
    public boolean deleteWorkItemById(Integer id)
    {
        baseDAO.deleteById(WorkReportItem.class, id);
        return true;
    }
    
    @Override
    public boolean deleteWorkItemByCode(String reportCode)
    {
        String hql = "delete from WorkReportItem wr where wr.reportCode = '" + reportCode + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
}