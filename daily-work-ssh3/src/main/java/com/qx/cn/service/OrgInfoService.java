package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.OrgInfo;
import com.qx.cn.tool.TreeNode;
import com.qx.cn.vo.OrgInfoVO;

public interface OrgInfoService
{
    public List<OrgInfo> queryOrgList();
    
    public List<OrgInfo> queryOrgListByAsc(String paramString);
    
    public boolean saveOrgInfo(OrgInfo paramOrgInfo);
    
    public List<OrgInfo> queryOrgListToUserByAsc(String paramString);
    
    public OrgInfo loadOrgByCode(String paramString);
    
    public List<OrgInfoVO> queryOrgListTreeGrid(OrgInfo paramOrgInfo);
    
    public boolean deleteOrgListByCode(String paramString);
    
    public List<TreeNode> queryComTree(OrgInfo paramOrgInfo, boolean paramBoolean);
    
    public List<OrgInfo> queryAllOrgList();
    
    public OrgInfo queryByName(String paramString);
    
    public List<OrgInfo> queryAllOrgListBydepartCode(String paramString);
    
    public OrgInfo loadOrgListBydepartCode(String paramString);
}