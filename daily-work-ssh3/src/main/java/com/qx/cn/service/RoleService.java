package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.Role;

public interface RoleService
{
    public List queryRolesByPage(int page, int rows);
    
    public List<Role> queryAllRoles();
    
    public boolean saveRole(Role role);
    
    public boolean deleteRole(Long roleId);
    
    public Role loadRoleById(Long id);
}