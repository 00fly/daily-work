package com.qx.cn.model;

import java.io.Serializable;

public class Button implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 4243905998736602443L;
    
    private Long buttonId;
    
    private String buttonName;
    
    private String buttonIconImg;
    
    private Integer enabled;
    
    private String methodName;
    
    private Integer buttonSort;
    
    public String getButtonName()
    {
        return this.buttonName;
    }
    
    public void setButtonName(String buttonName)
    {
        this.buttonName = buttonName;
    }
    
    public String getButtonIconImg()
    {
        return this.buttonIconImg;
    }
    
    public void setButtonIconImg(String buttonIconImg)
    {
        this.buttonIconImg = buttonIconImg;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMethodName()
    {
        return this.methodName;
    }
    
    public void setMethodName(String methodName)
    {
        this.methodName = methodName;
    }
    
    public Long getButtonId()
    {
        return this.buttonId;
    }
    
    public void setButtonId(Long buttonId)
    {
        this.buttonId = buttonId;
    }
    
    public Integer getButtonSort()
    {
        return this.buttonSort;
    }
    
    public void setButtonSort(Integer buttonSort)
    {
        this.buttonSort = buttonSort;
    }
}