package com.qx.cn.model;

import java.io.Serializable;

public class UserRoleGroup implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -3985402462001446173L;
    
    private Long id;
    
    private Long userId;
    
    private Long groupId;
    
    private String orgCode;
    
    public String getOrgCode()
    {
        return this.orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public Long getUserId()
    {
        return this.userId;
    }
    
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }
    
    public Long getGroupId()
    {
        return this.groupId;
    }
    
    public void setGroupId(Long groupId)
    {
        this.groupId = groupId;
    }
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
}