package com.qx.cn.model;

import java.io.Serializable;

public class ConfigData implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -1988777467746087578L;
    
    private Integer id;
    
    private String configDomainName;
    
    private String dataCode;
    
    private String dataName;
    
    private Integer enabled;
    
    private Integer order;
    
    private Integer dataType;
    
    private String dataTypeCode;
    
    private Integer isopt;
    
    public String getConfigDomainName()
    {
        return this.configDomainName;
    }
    
    public void setConfigDomainName(String configDomainName)
    {
        this.configDomainName = configDomainName;
    }
    
    public String getDataCode()
    {
        return this.dataCode;
    }
    
    public void setDataCode(String dataCode)
    {
        this.dataCode = dataCode;
    }
    
    public String getDataName()
    {
        return this.dataName;
    }
    
    public void setDataName(String dataName)
    {
        this.dataName = dataName;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public Integer getOrder()
    {
        return this.order;
    }
    
    public void setOrder(Integer order)
    {
        this.order = order;
    }
    
    public Integer getDataType()
    {
        return this.dataType;
    }
    
    public void setDataType(Integer dataType)
    {
        this.dataType = dataType;
    }
    
    public String getDataTypeCode()
    {
        return this.dataTypeCode;
    }
    
    public void setDataTypeCode(String dataTypeCode)
    {
        this.dataTypeCode = dataTypeCode;
    }
    
    public Integer getIsopt()
    {
        return this.isopt;
    }
    
    public void setIsopt(Integer isopt)
    {
        this.isopt = isopt;
    }
    
    public Integer getId()
    {
        return this.id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
}