package com.qx.cn.model;

import java.io.Serializable;

public class RoleMenu implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 7288545209993490714L;
    
    private Long id;
    
    private String roleId;
    
    private String menuId;
    
    private String buttonIds;
    
    private String tabIds;
    
    public String getRoleId()
    {
        return this.roleId;
    }
    
    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }
    
    public String getMenuId()
    {
        return this.menuId;
    }
    
    public void setMenuId(String menuId)
    {
        this.menuId = menuId;
    }
    
    public String getButtonIds()
    {
        return this.buttonIds;
    }
    
    public void setButtonIds(String buttonIds)
    {
        this.buttonIds = buttonIds;
    }
    
    public String getTabIds()
    {
        return this.tabIds;
    }
    
    public void setTabIds(String tabIds)
    {
        this.tabIds = tabIds;
    }
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
}