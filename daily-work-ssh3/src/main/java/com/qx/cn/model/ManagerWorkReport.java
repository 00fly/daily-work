package com.qx.cn.model;

import java.io.Serializable;

public class ManagerWorkReport implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -7597419978225887314L;
    
    private Integer id;
    
    private String name;
    
    private String date;
    
    private Integer receiverId;
    
    private String userIds;
    
    public Integer getId()
    {
        return this.id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getDate()
    {
        return this.date;
    }
    
    public void setDate(String date)
    {
        this.date = date;
    }
    
    public Integer getReceiverId()
    {
        return this.receiverId;
    }
    
    public void setReceiverId(Integer receiverId)
    {
        this.receiverId = receiverId;
    }
    
    public String getUserIds()
    {
        return this.userIds;
    }
    
    public void setUserIds(String userIds)
    {
        this.userIds = userIds;
    }
}