package com.qx.cn.model;

import java.io.Serializable;

public class Role implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -6108732324059325539L;
    
    private Long roleId;
    
    private String roleName;
    
    private Integer enabled;
    
    private String roleDesc;
    
    public String getRoleName()
    {
        return this.roleName;
    }
    
    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getRoleDesc()
    {
        return this.roleDesc;
    }
    
    public void setRoleDesc(String roleDesc)
    {
        this.roleDesc = roleDesc;
    }
    
    public Long getRoleId()
    {
        return this.roleId;
    }
    
    public void setRoleId(Long roleId)
    {
        this.roleId = roleId;
    }
}