package com.qx.cn.model;

import java.io.Serializable;

public class Menu implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 7732306790798475569L;
    
    private Long menuId;
    
    private String menuCode;
    
    private String menuName;
    
    private Integer isLeafMenu;
    
    private String menuURL;
    
    private Integer enabled;
    
    private String menuDesc;
    
    private Integer menuSort;
    
    public String getMenuCode()
    {
        return this.menuCode;
    }
    
    public void setMenuCode(String menuCode)
    {
        this.menuCode = menuCode;
    }
    
    public String getMenuName()
    {
        return this.menuName;
    }
    
    public void setMenuName(String menuName)
    {
        this.menuName = menuName;
    }
    
    public Integer getIsLeafMenu()
    {
        return this.isLeafMenu;
    }
    
    public void setIsLeafMenu(Integer isLeafMenu)
    {
        this.isLeafMenu = isLeafMenu;
    }
    
    public String getMenuURL()
    {
        return this.menuURL;
    }
    
    public void setMenuURL(String menuURL)
    {
        this.menuURL = menuURL;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMenuDesc()
    {
        return this.menuDesc;
    }
    
    public void setMenuDesc(String menuDesc)
    {
        this.menuDesc = menuDesc;
    }
    
    public Integer getMenuSort()
    {
        return this.menuSort;
    }
    
    public void setMenuSort(Integer menuSort)
    {
        this.menuSort = menuSort;
    }
    
    public Long getMenuId()
    {
        return this.menuId;
    }
    
    public void setMenuId(Long menuId)
    {
        this.menuId = menuId;
    }
}