package com.qx.cn.vo;

public class OrgInfoVO
{
    private Long orgId;
    
    private String orgCode;
    
    private String orgName;
    
    private String orgDuty;
    
    private Integer enabled;
    
    private String memo;
    
    private String optHeader;
    
    private Long orgLeader;
    
    private String departCode;
    
    private String orgPId;
    
    private String id;
    
    private String state;
    
    public String getId()
    {
        return this.id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getOrgCode()
    {
        return this.orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public String getOrgName()
    {
        return this.orgName;
    }
    
    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }
    
    public String getOrgDuty()
    {
        return this.orgDuty;
    }
    
    public void setOrgDuty(String orgDuty)
    {
        this.orgDuty = orgDuty;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMemo()
    {
        return this.memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
    
    public String getOrgPId()
    {
        return this.orgPId;
    }
    
    public void setOrgPId(String orgPId)
    {
        this.orgPId = orgPId;
    }
    
    public String getState()
    {
        return this.state;
    }
    
    public void setState(String state)
    {
        this.state = state;
    }
    
    public Long getOrgId()
    {
        return this.orgId;
    }
    
    public void setOrgId(Long orgId)
    {
        this.orgId = orgId;
    }
    
    public String getOptHeader()
    {
        return this.optHeader;
    }
    
    public void setOptHeader(String optHeader)
    {
        this.optHeader = optHeader;
    }
    
    public Long getOrgLeader()
    {
        return this.orgLeader;
    }
    
    public void setOrgLeader(Long orgLeader)
    {
        this.orgLeader = orgLeader;
    }
    
    public String getDepartCode()
    {
        return this.departCode;
    }
    
    public void setDepartCode(String departCode)
    {
        this.departCode = departCode;
    }
}