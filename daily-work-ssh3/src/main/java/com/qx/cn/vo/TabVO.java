package com.qx.cn.vo;

public class TabVO
{
    private Long id;
    
    private String tabName;
    
    private Integer enabled;
    
    private Integer isopt;
    
    private Integer sign;
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public String getTabName()
    {
        return this.tabName;
    }
    
    public void setTabName(String tabName)
    {
        this.tabName = tabName;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public Integer getIsopt()
    {
        return this.isopt;
    }
    
    public void setIsopt(Integer isopt)
    {
        this.isopt = isopt;
    }
    
    public Integer getSign()
    {
        return this.sign;
    }
    
    public void setSign(Integer sign)
    {
        this.sign = sign;
    }
}