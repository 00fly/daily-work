package com.qx.cn.vo;

public class ProjectManagementTraceVO
{
    private Long id;
    
    private Long projectId;
    
    private Long userId;
    
    private String userName;
    
    private String submitDate;
    
    private String handleCase;
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public Long getProjectId()
    {
        return this.projectId;
    }
    
    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }
    
    public Long getUserId()
    {
        return this.userId;
    }
    
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }
    
    public String getUserName()
    {
        return this.userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public String getSubmitDate()
    {
        return this.submitDate;
    }
    
    public void setSubmitDate(String submitDate)
    {
        this.submitDate = submitDate;
    }
    
    public String getHandleCase()
    {
        return this.handleCase;
    }
    
    public void setHandleCase(String handleCase)
    {
        this.handleCase = handleCase;
    }
}