package com.qx.cn.vo;

public class RoleGroupVO
{
    private Long groupId;
    
    private String groupName;
    
    private Integer enabled;
    
    private String groupDesc;
    
    private Integer sign;
    
    public String getGroupName()
    {
        return this.groupName;
    }
    
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getGroupDesc()
    {
        return this.groupDesc;
    }
    
    public void setGroupDesc(String groupDesc)
    {
        this.groupDesc = groupDesc;
    }
    
    public Integer getSign()
    {
        return this.sign;
    }
    
    public void setSign(Integer sign)
    {
        this.sign = sign;
    }
    
    public Long getGroupId()
    {
        return this.groupId;
    }
    
    public void setGroupId(Long groupId)
    {
        this.groupId = groupId;
    }
}