package com.qx.cn.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.ConfigData;
import com.qx.cn.service.ConfigDataService;

@ParentPackage("netxzt")
@Namespace("/")
@InterceptorRefs({@InterceptorRef("content")})
@Results({@Result(name = "outlogin", location = "/admin!loseSession"), @Result(name = "getOutlogin", location = "/admin!otherlogin"),
    @Result(name = "configListFrom", location = "/WEB-INF/page/configData/configListFrom.jsp")})
@Action(value = "config", interceptorRefs = {@InterceptorRef("loginedCheck")})
public class ConfigDataAction extends BaseAction
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -3852016493038608171L;
    
    @Autowired
    private ConfigDataService configDataService;
    
    private Integer dataOrder;
    
    private String dataName;
    
    private String dataDomain;
    
    private String dataCode;
    
    private Integer enabled;
    
    private Integer dataType;
    
    private String dataTypeCode;
    
    public String configListFrom()
    {
        return "configListFrom";
    }
    
    public String loadConfigTree()
    {
        List<ConfigData> configDatas = configDataService.queryConfigByPar();
        JSONArray arrJson = new JSONArray();
        for (ConfigData config : configDatas)
        {
            JSONObject json = new JSONObject();
            json.put("id", config.getDataCode());
            json.put("text", config.getDataName());
            arrJson.add(json);
        }
        JSONObject object = new JSONObject();
        object.put("id", Integer.valueOf(999));
        object.put("text", "数据字典管理");
        object.put("children", arrJson);
        try
        {
            PrintWriter out = response.getWriter();
            out.print("[" + object.toString() + "]");
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveConfigData()
    {
        ConfigData configData = null;
        boolean flag = false;
        String errorMsg = "";
        if ((dataCode != null) && (!"".equals(dataCode)))
        {
            ConfigData conf = configDataService.loadConfigByDOMAndOrder(dataDomain);
            if (conf == null)
            {
                ConfigData data = configDataService.loadConfigByCode(dataCode);
                data.setDataName(dataName);
                data.setConfigDomainName(dataDomain);
                flag = configDataService.saveConfig(data);
                configDataService.updateDataByCode(dataDomain, dataCode);
            }
            else if (dataCode.equals(conf.getDataCode()))
            {
                ConfigData data = configDataService.loadConfigByCode(dataCode);
                data.setDataName(dataName);
                data.setConfigDomainName(dataDomain);
                data.setIsopt(0);
                flag = configDataService.saveConfig(data);
                configDataService.updateDataByCode(dataDomain, dataCode);
            }
            else
            {
                flag = false;
                errorMsg = "该" + dataDomain + "域名已经存在,请选择其他域名!";
            }
        }
        else
        {
            ConfigData conf = configDataService.loadConfigByDOMAndOrder(dataDomain);
            if (conf == null)
            {
                configData = new ConfigData();
                List<ConfigData> configDatas = configDataService.queryConfigByPar();
                if (configDatas.size() > 0)
                {
                    StringBuffer codeBuf = new StringBuffer();
                    String code = (configDatas.get(configDatas.size() - 1)).getDataCode();
                    Integer num = Integer.valueOf(code).intValue() + 1;
                    if (num.toString().length() == 1)
                    {
                        codeBuf.append("00").append(num);
                    }
                    else if (num.toString().length() == 2)
                    {
                        codeBuf.append("0").append(num);
                    }
                    else
                    {
                        codeBuf.append(num);
                    }
                    configData.setDataCode(codeBuf.toString());
                }
                else
                {
                    configData.setDataCode("001");
                }
                configData.setDataType(dataType);
                configData.setDataName(dataName);
                configData.setConfigDomainName(dataDomain);
                configData.setEnabled(1);
                configData.setOrder(null);
                configData.setIsopt(0);
                flag = configDataService.saveConfig(configData);
            }
            else
            {
                flag = false;
                errorMsg = "该" + dataDomain + "域名已经存在,请选择其他域名!";
            }
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", errorMsg);
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String deleteConfig()
    {
        boolean flag = false;
        String msg = "删除数据字典失败!";
        ConfigData configData = configDataService.loadConfigByCode(dataCode);
        if (configData != null)
        {
            if (configData.getIsopt().intValue() == 1)
            {
                msg = "你没有权限删除系统级别的数据!";
                flag = false;
            }
            else
            {
                flag = configDataService.deleteConfigByCode(dataCode);
            }
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", msg);
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String loadConfigByCode()
    {
        ConfigData configData = configDataService.loadConfigByCode(dataCode);
        JSONObject object = new JSONObject();
        object.put("dataCode", configData.getDataCode());
        object.put("dataName", configData.getDataName());
        object.put("dataDomain", configData.getConfigDomainName());
        object.put("dataType", configData.getDataType());
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String querySonConfigByCode()
    {
        List<ConfigData> configDatas = configDataService.querySonConfigByParCode(dataCode, 1);
        JSONArray arrJson = new JSONArray();
        for (ConfigData config : configDatas)
        {
            JSONObject json = new JSONObject();
            json.put("dataCode", config.getDataCode());
            json.put("dataName", config.getDataName());
            json.put("enabled", config.getEnabled());
            if (config.getDataTypeCode() == null)
            {
                json.put("dataTypeCode", config.getDataCode());
            }
            else
            {
                json.put("dataTypeCode", config.getDataTypeCode());
            }
            json.put("dataDomain", config.getConfigDomainName());
            if ((config.getIsopt() == null) || (config.getIsopt().intValue() == 0))
            {
                json.put("isopt", Integer.valueOf(0));
                json.put("isoptName", "可以");
            }
            else
            {
                json.put("isopt", config.getIsopt());
                json.put("isoptName", "<span style='color:#D3D3D3'>不可</span>");
            }
            
            json.put("order", config.getOrder());
            arrJson.add(json);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(arrJson.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveSonConfigData()
    {
        if (dataCode.length() == 3)
        {
            ConfigData configPar = configDataService.loadConfigByCode(dataCode);
            ConfigData config = new ConfigData();
            List<ConfigData> configDatas = configDataService.querySonConfigByParCode(dataCode, 2);
            config.setDataName(dataName);
            config.setEnabled(enabled);
            config.setOrder(dataOrder);
            if (configDatas.size() == 0)
            {
                config.setDataCode(dataCode + "001");
            }
            else
            {
                StringBuffer codeBuf = new StringBuffer();
                String codeStr = (configDatas.get(configDatas.size() - 1)).getDataCode();
                Integer codeInt = Integer.valueOf(codeStr).intValue() + 1;
                if (codeInt.toString().length() == 4)
                {
                    codeBuf.append("00").append(codeInt.toString());
                }
                else if (codeInt.toString().length() == 5)
                {
                    codeBuf.append("0").append(codeInt.toString());
                }
                else if (codeInt.toString().length() == 6)
                {
                    codeBuf.append(codeInt.toString());
                }
                config.setDataCode(codeBuf.toString());
            }
            
            if (dataTypeCode.equals("cjw_1"))
            {
                config.setDataTypeCode(null);
            }
            else
            {
                config.setDataTypeCode(dataTypeCode);
            }
            config.setConfigDomainName(configPar.getConfigDomainName());
            configDataService.saveConfig(config);
        }
        else
        {
            ConfigData condata = configDataService.loadConfigByCode(dataCode);
            condata.setDataName(dataName);
            condata.setEnabled(enabled);
            condata.setOrder(dataOrder);
            if (dataTypeCode.equals("cjw_1"))
            {
                condata.setDataTypeCode(null);
            }
            else
            {
                condata.setDataTypeCode(dataTypeCode);
            }
            configDataService.saveConfig(condata);
        }
        
        JSONObject object = new JSONObject();
        object.put("result", 1);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String loadSonConfigByCode()
    {
        ConfigData configData = configDataService.loadConfigByCode(dataCode);
        JSONObject object = new JSONObject();
        object.put("dataCode", configData.getDataCode());
        object.put("dataName", configData.getDataName());
        object.put("dataOrder", configData.getOrder());
        object.put("enabled", configData.getEnabled());
        object.put("dataTypeCode", configData.getDataTypeCode());
        if (configData.getDataTypeCode() != null)
        {
            object.put("dataTypeCode", configData.getDataTypeCode());
        }
        else
        {
            object.put("dataTypeCode", "cjw_1");
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryConfigByDomainCode()
    {
        ConfigData configData = configDataService.loadConfigByDOMAndOrder(dataDomain);
        int type = 1;
        List<ConfigData> configDatas = configDataService.queryConfigByDataDomain(dataDomain, type);
        JSONArray arrJson = new JSONArray();
        for (ConfigData config : configDatas)
        {
            JSONObject json = new JSONObject();
            if (configData.getDataType().intValue() == 1)
            {
                json.put("id", config.getDataCode());
            }
            else
            {
                json.put("id", config.getDataTypeCode());
            }
            json.put("text", config.getDataName());
            arrJson.add(json);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(arrJson.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String checkConfig()
    {
        boolean flag = false;
        String msg = "修改数据字典失败!";
        ConfigData configData = configDataService.loadConfigByCode(dataCode);
        if (configData != null)
        {
            if (configData.getIsopt() != null)
            {
                if (configData.getIsopt().intValue() == 1)
                {
                    msg = "你没有权限修改系统级别的数据!";
                    flag = false;
                }
                else
                {
                    flag = true;
                }
            }
            else
            {
                flag = true;
            }
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", msg);
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public Integer getDataOrder()
    {
        return dataOrder;
    }
    
    public void setDataOrder(Integer dataOrder)
    {
        this.dataOrder = dataOrder;
    }
    
    public String getDataName()
    {
        return dataName;
    }
    
    public void setDataName(String dataName)
    {
        this.dataName = dataName;
    }
    
    public String getDataDomain()
    {
        return dataDomain;
    }
    
    public void setDataDomain(String dataDomain)
    {
        this.dataDomain = dataDomain;
    }
    
    public String getDataCode()
    {
        return dataCode;
    }
    
    public void setDataCode(String dataCode)
    {
        this.dataCode = dataCode;
    }
    
    public Integer getEnabled()
    {
        return enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public Integer getDataType()
    {
        return dataType;
    }
    
    public void setDataType(Integer dataType)
    {
        this.dataType = dataType;
    }
    
    public String getDataTypeCode()
    {
        return dataTypeCode;
    }
    
    public void setDataTypeCode(String dataTypeCode)
    {
        this.dataTypeCode = dataTypeCode;
    }
}