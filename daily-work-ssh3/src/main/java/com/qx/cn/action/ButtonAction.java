package com.qx.cn.action;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.Button;
import com.qx.cn.model.MenuButton;
import com.qx.cn.service.ButtonService;
import com.qx.cn.service.MenuButtonService;
import com.qx.cn.vo.ButtonVO;

@ParentPackage("netxzt")
@Namespace("/")
@InterceptorRefs({@InterceptorRef("content")})
@Results({@Result(name = "outlogin", location = "/admin!loseSession"), @Result(name = "getOutlogin", location = "/admin!otherlogin"),
    @Result(name = "buttonlist", location = "/WEB-INF/page/buttonInfo/buttonList.jsp")})
@Action(value = "button", interceptorRefs = {@InterceptorRef("loginedCheck")})
public class ButtonAction extends BaseAction
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 3807386899510039416L;
    
    private int page;
    
    private int rows;
    
    private File some;
    
    private String someFileName;
    
    private String someContentType;
    
    private String imagePath;
    
    private Long buttonId;
    
    private String buttonName;
    
    private String methodName;
    
    private Integer enabled;
    
    private String menuCode;
    
    private Integer buttonSort;
    
    @Autowired
    private ButtonService buttonService;
    
    @Autowired
    private MenuButtonService menuButtonService;
    
    public String getMenuCode()
    {
        return menuCode;
    }
    
    public void setMenuCode(String menuCode)
    {
        this.menuCode = menuCode;
    }
    
    public String buttonListForm()
    {
        return "buttonlist";
    }
    
    @SuppressWarnings("rawtypes")
    public String queryButtonListByPage()
    {
        List results = buttonService.queryButtonsByPage(page, rows);
        JSONArray arrJson = new JSONArray();
        if (!results.isEmpty())
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("buttonId", (Integer)row[0]);
                json.put("buttonName", (String)row[1]);
                json.put("enabled", (Integer)row[3]);
                json.put("methodName", (String)row[4]);
                json.put("buttonSort", (Integer)row[5]);
                arrJson.add(json);
            }
        }
        
        JSONObject object = new JSONObject();
        object.put("total", results.size());
        object.put("rows", arrJson);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveButton()
        throws IOException
    {
        boolean flag = true;
        if (buttonId != null)
        {
            Button updBut = buttonService.loadButtonById(buttonId);
            updBut.setButtonName(buttonName);
            updBut.setEnabled(enabled);
            updBut.setButtonSort(buttonSort);
            updBut.setMethodName(methodName);
            flag = true;
            buttonService.saveButton(updBut);
        }
        else
        {
            Button button = new Button();
            button.setButtonName(buttonName);
            button.setEnabled(enabled);
            button.setMethodName(methodName);
            button.setButtonSort(buttonSort);
            buttonService.saveButton(button);
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "添加或者修改按键错误!");
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String deleteButton()
    {
        menuButtonService.deleteButtonMenu(String.valueOf(buttonId));
        boolean flag = buttonService.deleteButton(buttonId);
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除按键成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除按键失败");
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String loadButtonById()
    {
        JSONObject object = new JSONObject();
        Button button = buttonService.loadButtonById(buttonId);
        object.put("buttonId", button.getButtonId());
        
        object.put("buttonName", button.getButtonName());
        object.put("enabled", button.getEnabled());
        object.put("buttonSort", button.getButtonSort());
        object.put("methodName", button.getMethodName());
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryAllButtonToMenu()
    {
        List<Button> buttons = buttonService.queryAllButtons();
        List<MenuButton> menuButtons = menuButtonService.queryMenuButtonByCode(menuCode);
        List<ButtonVO> buttonVOs = new ArrayList<>();
        for (Button button : buttons)
        {
            int sign = 0;
            ButtonVO buttonVO = new ButtonVO();
            for (MenuButton menuBut : menuButtons)
            {
                if (button.getButtonId().equals(menuBut.getButtonId()))
                {
                    sign = 1;
                }
            }
            buttonVO.setButtonId(button.getButtonId());
            buttonVO.setButtonName(button.getButtonName());
            buttonVO.setSign(Integer.valueOf(sign));
            buttonVOs.add(buttonVO);
        }
        JSONObject object = new JSONObject();
        object.put("datas", buttonVOs);
        object.put("result", 1);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(JSON.toJSONString(object));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public int getPage()
    {
        return page;
    }
    
    public void setPage(int page)
    {
        this.page = page;
    }
    
    public int getRows()
    {
        return rows;
    }
    
    public void setRows(int rows)
    {
        this.rows = rows;
    }
    
    public String getSomeFileName()
    {
        return someFileName;
    }
    
    public void setSomeFileName(String someFileName)
    {
        this.someFileName = someFileName;
    }
    
    public String getSomeContentType()
    {
        return someContentType;
    }
    
    public void setSomeContentType(String someContentType)
    {
        this.someContentType = someContentType;
    }
    
    public String getImagePath()
    {
        return imagePath;
    }
    
    public void setImagePath(String imagePath)
    {
        this.imagePath = imagePath;
    }
    
    public String getButtonName()
    {
        return buttonName;
    }
    
    public void setButtonName(String buttonName)
    {
        this.buttonName = buttonName;
    }
    
    public String getMethodName()
    {
        return methodName;
    }
    
    public void setMethodName(String methodName)
    {
        this.methodName = methodName;
    }
    
    public Integer getEnabled()
    {
        return enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public File getSome()
    {
        return some;
    }
    
    public void setSome(File some)
    {
        this.some = some;
    }
    
    public Long getButtonId()
    {
        return buttonId;
    }
    
    public void setButtonId(Long buttonId)
    {
        this.buttonId = buttonId;
    }
    
    public Integer getButtonSort()
    {
        return buttonSort;
    }
    
    public void setButtonSort(Integer buttonSort)
    {
        this.buttonSort = buttonSort;
    }
}