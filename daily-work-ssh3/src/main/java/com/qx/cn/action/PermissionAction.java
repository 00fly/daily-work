package com.qx.cn.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.User;
import com.qx.cn.service.UserService;
import com.qx.cn.tool.DateForm;
import com.qx.cn.tool.MD5;

/**
 * 
 * 
 * @author 00fly
 * @version [版本号, 2016-5-15]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@ParentPackage("netxzt")
@Namespace("/")
@InterceptorRefs({@InterceptorRef("content")})
@Results({@Result(name = "outlogin", location = "/admin!loseSession"), @Result(name = "getOutlogin", location = "/admin!otherlogin"),
    @Result(name = "index", location = "/WEB-INF/page/user/index.jsp"), @Result(name = "listForm", location = "/WEB-INF/page/permission/listForm.jsp")})
@Action(value = "permission", interceptorRefs = {@InterceptorRef("loginedCheck")})
public class PermissionAction extends BaseAction
{
    
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -2604777986320062945L;
    
    private Long userId;
    
    private String userLoginName;
    
    private String userName;
    
    private String telephone;
    
    private Integer enabled;
    
    private String workNumber;
    
    private String orgCode;
    
    private String pinyin;
    
    private int page;
    
    private int rows;
    
    @Autowired
    private UserService userService;
    
    public String listForm()
    {
        return "listForm";
    }
    
    public String queryUserByCodeAnsPage()
    {
        User uss = new User();
        uss.setUserLoginName(userLoginName);
        uss.setUserName(userName);
        uss.setWorkNumber(workNumber);
        uss.setOrgCode(orgCode);
        uss.setSpellLong(pinyin);
        List results = userService.queryUserByPageAndCodeSQL(uss, page, rows);
        JSONArray arrJson = new JSONArray();
        if (!results.isEmpty())
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("userId", row[0]);
                json.put("userName", row[3]);
                json.put("userLoginName", row[1]);
                json.put("workNumber", row[7]);
                json.put("enabled", row[13]);
                arrJson.add(json);
            }
        }
        
        JSONObject object = new JSONObject();
        List users = userService.queryUserPageSizeByCode(uss);
        if (!users.isEmpty())
        {
            object.put("total", Integer.valueOf(users.size()));
        }
        object.put("rows", arrJson);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveUserByOrgCode()
    {
        boolean flag = false;
        if (userId == null)
        {
            User user = new User();
            user.setOrgCode(orgCode);
            user.setUserLoginName(userLoginName);
            user.setUserLoginPwd(MD5.toMD5("123456"));
            user.setCreateTime(DateForm.SimpleDateTime(new Date()));
            user.setTelePhone(telephone);
            user.setEnabled(enabled);
            user.setUserName(userName);
            // flag = userService.saveUserByCode(user);
        }
        else
        {
            User updUser = userService.loadUserById(userId);
            updUser.setOrgCode(orgCode);
            updUser.setTelePhone(telephone);
            updUser.setEnabled(enabled);
            updUser.setUserName(userName);
            // flag = userService.saveUserByCode(updUser);
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("msg", "添加成功,账号:" + userLoginName + "&nbsp;密码:123456");
        }
        else
        {
            object.put("result", 2);
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String getUserLoginName()
    {
        return userLoginName;
    }
    
    public void setUserLoginName(String userLoginName)
    {
        this.userLoginName = userLoginName;
    }
    
    public String getUserName()
    {
        return userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public String getWorkNumber()
    {
        return workNumber;
    }
    
    public void setWorkNumber(String workNumber)
    {
        this.workNumber = workNumber;
    }
    
    public String getOrgCode()
    {
        return orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public String getPinyin()
    {
        return pinyin;
    }
    
    public void setPinyin(String pinyin)
    {
        this.pinyin = pinyin;
    }
    
    public int getPage()
    {
        return page;
    }
    
    public void setPage(int page)
    {
        this.page = page;
    }
    
    public int getRows()
    {
        return rows;
    }
    
    public void setRows(int rows)
    {
        this.rows = rows;
    }
}
