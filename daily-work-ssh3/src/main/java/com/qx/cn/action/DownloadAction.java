package com.qx.cn.action;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class DownloadAction extends ActionSupport
{
    private static final long serialVersionUID = 1L;
    
    private String fileName;
    
    private String fileRealName;
    
    public void setFileName()
    {
        String fname = ServletActionContext.getRequest().getParameter("name");
        String frealname = ServletActionContext.getRequest().getParameter("realname");
        try
        {
            frealname = URLDecoder.decode(frealname, "utf-8");
            frealname = URLDecoder.decode(frealname, "utf-8");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        fileName = fname;
        fileRealName = frealname;
    }
    
    public String getFileName()
        throws UnsupportedEncodingException
    {
        fileRealName = new String(fileRealName.getBytes(), "ISO-8859-1");
        return fileRealName;
    }
    
    public InputStream getDownloadFile()
    {
        setFileName();
        InputStream inputStream = ServletActionContext.getServletContext().getResourceAsStream("/" + fileName);
        return inputStream;
    }
    
    @Override
    public String execute()
    {
        return "success";
    }
}