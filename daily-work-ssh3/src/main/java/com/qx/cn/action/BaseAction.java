package com.qx.cn.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.util.ServletContextAware;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 
 * action 超类
 * 
 * @author 00fly
 * @version [版本号, 2016-5-9]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class BaseAction extends ActionSupport implements ServletRequestAware, ServletResponseAware, ServletContextAware, SessionAware
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 2640087118150427365L;
    
    protected ActionContext actionCtx = ActionContext.getContext();
    
    protected HttpServletRequest request;
    
    protected HttpServletResponse response;
    
    protected Map<String, Object> session;
    
    protected ServletContext application;
    
    public void actionContext(String key, Object value)
    {
        actionCtx.put(key, value);
    }
    
    protected Object get(String key)
    {
        return actionCtx.get(key);
    }
    
    public ActionContext getActionCtx()
    {
        return actionCtx;
    }
    
    public ServletContext getApplication()
    {
        return application;
    }
    
    public HttpServletRequest getRequest()
    {
        return request;
    }
    
    public HttpServletResponse getResponse()
    {
        return response;
    }
    
    public Map<String, Object> getSession()
    {
        return session;
    }
    
    public void setActionCtx(ActionContext actionCtx)
    {
        this.actionCtx = actionCtx;
    }
    
    public void setApplication(ServletContext application)
    {
        this.application = application;
    }
    
    public void setRequest(HttpServletRequest request)
    {
        this.request = request;
    }
    
    public void setResponse(HttpServletResponse response)
    {
        this.response = response;
    }
    
    @Override
    public void setServletContext(ServletContext application)
    {
        this.application = application;
    }
    
    @Override
    public void setServletRequest(HttpServletRequest request)
    {
        this.request = request;
    }
    
    @Override
    public void setServletResponse(HttpServletResponse response)
    {
        this.response = response;
    }
    
    @Override
    public void setSession(Map<String, Object> session)
    {
        this.session = session;
    }
    
    public String toRealPath(String path)
    {
        return application.getRealPath(path);
    }
    
    public void writeJson(Object object)
    {
        try
        {
            String json = JSON.toJSONStringWithDateFormat(object, "yyyy-MM-dd HH:mm:ss", new SerializerFeature[0]);
            ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
            ServletActionContext.getResponse().getWriter().write(json);
            ServletActionContext.getResponse().getWriter().flush();
            ServletActionContext.getResponse().getWriter().close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}