package com.qx.cn.tool;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TreeNode implements Serializable
{
    private String id;
    
    private String text;
    
    private String iconCls;
    
    private Boolean checked = Boolean.valueOf(false);
    
    private Map<String, Object> attributes;
    
    private List<TreeNode> children;
    
    private String state = "open";
    
    public String getId()
    {
        return this.id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getText()
    {
        return this.text;
    }
    
    public void setText(String text)
    {
        this.text = text;
    }
    
    public String getIconCls()
    {
        return this.iconCls;
    }
    
    public void setIconCls(String iconCls)
    {
        this.iconCls = iconCls;
    }
    
    public Boolean getChecked()
    {
        return this.checked;
    }
    
    public void setChecked(Boolean checked)
    {
        this.checked = checked;
    }
    
    public Map<String, Object> getAttributes()
    {
        return this.attributes;
    }
    
    public void setAttributes(Map<String, Object> attributes)
    {
        this.attributes = attributes;
    }
    
    public List<TreeNode> getChildren()
    {
        return this.children;
    }
    
    public void setChildren(List<TreeNode> children)
    {
        this.children = children;
    }
    
    public String getState()
    {
        return this.state;
    }
    
    public void setState(String state)
    {
        this.state = state;
    }
}