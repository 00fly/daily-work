package com.qx.cn.tool;

import java.util.Comparator;

import com.qx.cn.model.Menu;

public class MenuComparator implements Comparator<Menu>
{
    @Override
    public int compare(Menu o1, Menu o2)
    {
        long i1 = Long.valueOf(o1.getMenuCode()) != null ? Long.valueOf(o1.getMenuCode()).longValue() : -1L;
        long i2 = Long.valueOf(o2.getMenuCode()) != null ? Long.valueOf(o2.getMenuCode()).longValue() : -1L;
        return (int)(i1 - i2);
    }
}