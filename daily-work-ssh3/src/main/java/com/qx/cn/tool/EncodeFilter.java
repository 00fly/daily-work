package com.qx.cn.tool;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EncodeFilter implements Filter
{
    private FilterConfig filterConfig = null;
    
    @Override
    public void destroy()
    {
        this.filterConfig = null;
    }
    
    @Override
    public void doFilter(ServletRequest req, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        String encoding = this.filterConfig.getInitParameter("encoding");
        
        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse resp = (HttpServletResponse)response;
        request.setCharacterEncoding(encoding);
        resp.setContentType("text/html;charset=" + encoding);
        
        if (request.getMethod().equalsIgnoreCase("get"))
        {
            Map<String, String[]> paramMap = req.getParameterMap();
            String[] queryStringArray = {""};
            if (request.getQueryString() != null)
            {
                queryStringArray = request.getQueryString().split("&");
            }
            for (int i = 0; i < queryStringArray.length; i++)
            {
                queryStringArray[i] = queryStringArray[i].replaceAll("(.*)=(.*)", "$1");
            }
            Set<String> keySet = paramMap.keySet();
            for (String key : keySet)
            {
                boolean isFromGet = false;
                for (String paramFromGet : queryStringArray)
                {
                    if (key.equals(paramFromGet))
                    {
                        isFromGet = true;
                    }
                }
                if (isFromGet)
                {
                    String[] paramArray = (String[])paramMap.get(key);
                    for (int i = 0; i < paramArray.length; i++)
                    {
                        paramArray[i] = new String(paramArray[i].getBytes("iso-8859-1"), encoding);
                    }
                }
            }
        }
        chain.doFilter(req, resp);
    }
    
    @Override
    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        this.filterConfig = filterConfig;
    }
}