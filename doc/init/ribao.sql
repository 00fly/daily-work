/*
Navicat MySQL Data Transfer

Source Server         : cjw
Source Server Version : 50096
Source Host           : localhost:3306
Source Database       : ribao

Target Server Type    : MYSQL
Target Server Version : 50096
File Encoding         : 65001

Date: 2015-01-18 01:41:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for busi_manager_work_report
-- ----------------------------
DROP TABLE IF EXISTS `busi_manager_work_report`;
CREATE TABLE `busi_manager_work_report` (
  `i_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(50) default NULL,
  `d_date` varchar(50) default NULL,
  `i_receiver_id` int(11) default NULL,
  `i_user_ids` varchar(255) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of busi_manager_work_report
-- ----------------------------

-- ----------------------------
-- Table structure for busi_work_report
-- ----------------------------
DROP TABLE IF EXISTS `busi_work_report`;
CREATE TABLE `busi_work_report` (
  `i_id` int(11) NOT NULL auto_increment,
  `c_work_report_name` varchar(50) default NULL,
  `d_create_time` varchar(50) default NULL,
  `d_send_time` varchar(50) default NULL,
  `i_user_id` int(11) default NULL,
  `c_status` int(11) default NULL,
  `c_memo` varchar(50) default NULL,
  `c_report_code` varchar(50) default NULL,
  `c_user_name` varchar(50) default NULL,
  `c_org_name` varchar(50) default NULL,
  `c_postion` varchar(50) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of busi_work_report
-- ----------------------------

-- ----------------------------
-- Table structure for busi_work_report_item
-- ----------------------------
DROP TABLE IF EXISTS `busi_work_report_item`;
CREATE TABLE `busi_work_report_item` (
  `i_id` int(11) NOT NULL auto_increment,
  `c_content` varchar(500) default NULL,
  `d_create_time` varchar(50) default NULL,
  `i_report_code` varchar(50) default NULL,
  `c_work_time` varchar(50) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of busi_work_report_item
-- ----------------------------

-- ----------------------------
-- Table structure for sys_button
-- ----------------------------
DROP TABLE IF EXISTS `sys_button`;
CREATE TABLE `sys_button` (
  `i_button_id` int(11) NOT NULL auto_increment,
  `c_button_name` varchar(50) default NULL,
  `c_button_icon_img` varchar(200) default NULL,
  `i_enabled` int(11) default NULL,
  `c_method_name` varchar(100) default NULL,
  `c_button_sort` int(4) default NULL,
  PRIMARY KEY  (`i_button_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_button
-- ----------------------------
INSERT INTO `sys_button` VALUES ('1', '查看', '', '1', null, '5');
INSERT INTO `sys_button` VALUES ('6', '修改', '', '1', null, '2');
INSERT INTO `sys_button` VALUES ('7', '删除', '', '1', null, '3');
INSERT INTO `sys_button` VALUES ('8', '添加', '', '1', null, '1');
INSERT INTO `sys_button` VALUES ('9', '刷新', '', '1', null, '4');
INSERT INTO `sys_button` VALUES ('10', '保存', null, '1', '菜单管理', '6');
INSERT INTO `sys_button` VALUES ('11', '展开', null, '1', '菜单管理', '7');
INSERT INTO `sys_button` VALUES ('12', '折叠', null, '1', '菜单管理', '8');
INSERT INTO `sys_button` VALUES ('13', '取消修改', null, '1', '菜单管理', '9');
INSERT INTO `sys_button` VALUES ('14', '取消选中', null, '1', '菜单管理', '10');

-- ----------------------------
-- Table structure for sys_config_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_config_data`;
CREATE TABLE `sys_config_data` (
  `i_id` int(11) NOT NULL auto_increment,
  `c_config_domain_name` varchar(50) default NULL,
  `c_data_code` varchar(50) default NULL,
  `c_data_name` varchar(100) default NULL,
  `i_order` int(4) default NULL,
  `i_enabled` int(4) default NULL,
  `c_datatype` int(4) default NULL,
  `c_datatype_code` varchar(50) default NULL,
  `c_is_opt` int(4) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_config_data
-- ----------------------------
INSERT INTO `sys_config_data` VALUES ('1', 'test1', '001', '测试一', null, '1', '1', null, '0');
INSERT INTO `sys_config_data` VALUES ('2', 'test1', '001001', '测试', '1', '1', null, null, null);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `i_menu_id` int(11) NOT NULL auto_increment,
  `c_menu_code` varchar(100) default NULL,
  `c_menu_name` varchar(100) default NULL,
  `c_is_leaf_menu` int(4) default NULL,
  `c_menu_url` varchar(100) default NULL,
  `i_enabled` int(4) default NULL,
  `c_menu_desc` varchar(500) default NULL,
  `c_menu_sort` int(4) default NULL,
  PRIMARY KEY  (`i_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('20', '1', '根节点', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('21', '1001002', '用户管理', '1', 'user!queryUserFrom', '1', null, '4');
INSERT INTO `sys_menu` VALUES ('22', '1001003', '机构管理', '1', 'org!orgList', '1', null, '2');
INSERT INTO `sys_menu` VALUES ('23', '1001', '系统管理', '0', '#', '1', null, '1');
INSERT INTO `sys_menu` VALUES ('24', '1001001', '数据字典', '1', 'config!configListFrom', '1', null, '1');
INSERT INTO `sys_menu` VALUES ('25', '1001006', '菜单管理', '1', 'menu!menuListForm', '1', null, '5');
INSERT INTO `sys_menu` VALUES ('28', '1001005', '角色管理', '1', 'role!roleListForm', '1', null, '8');
INSERT INTO `sys_menu` VALUES ('33', '1002', '邮箱管理', '0', '#', '1', null, '2');
INSERT INTO `sys_menu` VALUES ('34', '1002001', '发送邮箱管理', '1', 'sendEmail!sendEmailForm', '1', null, '1');
INSERT INTO `sys_menu` VALUES ('35', '1002002', '发送时间管理', '1', 'sendTime!sendTimeForm', '1', null, '2');
INSERT INTO `sys_menu` VALUES ('36', '1002003', '日期管理', '1', 'sendDate!sendDateForm', '1', null, '3');
INSERT INTO `sys_menu` VALUES ('37', '1002004', '发送组', '1', 'workSendGroup!workSendGroupForm', '1', null, '4');
INSERT INTO `sys_menu` VALUES ('38', '1003', '业务管理', '0', '#', '1', null, '3');
INSERT INTO `sys_menu` VALUES ('39', '1003001', '日报管理', '1', 'workReport!workReportForm', '1', null, '1');
INSERT INTO `sys_menu` VALUES ('40', '1004', '高层管理', '0', '#', '1', null, '4');
INSERT INTO `sys_menu` VALUES ('41', '1004001', '日报查看', '1', 'manageWorkReport!manageworkReportForm', '1', null, '1');

-- ----------------------------
-- Table structure for sys_menu_button
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_button`;
CREATE TABLE `sys_menu_button` (
  `i_id` int(11) NOT NULL auto_increment,
  `i_menu_id` varchar(50) default NULL,
  `i_button_id` int(11) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_button
-- ----------------------------
INSERT INTO `sys_menu_button` VALUES ('1', '1001002', '8');
INSERT INTO `sys_menu_button` VALUES ('2', '1001002', '6');
INSERT INTO `sys_menu_button` VALUES ('3', '1001002', '7');
INSERT INTO `sys_menu_button` VALUES ('4', '1001002', '9');
INSERT INTO `sys_menu_button` VALUES ('19', '1001006', '8');
INSERT INTO `sys_menu_button` VALUES ('20', '1001006', '6');
INSERT INTO `sys_menu_button` VALUES ('21', '1001006', '7');
INSERT INTO `sys_menu_button` VALUES ('22', '1001006', '9');
INSERT INTO `sys_menu_button` VALUES ('23', '1001006', '1');
INSERT INTO `sys_menu_button` VALUES ('24', '1001006', '10');
INSERT INTO `sys_menu_button` VALUES ('25', '1001006', '11');
INSERT INTO `sys_menu_button` VALUES ('26', '1001006', '12');
INSERT INTO `sys_menu_button` VALUES ('27', '1001006', '13');
INSERT INTO `sys_menu_button` VALUES ('28', '1001006', '14');
INSERT INTO `sys_menu_button` VALUES ('29', '1001005', '8');
INSERT INTO `sys_menu_button` VALUES ('30', '1001005', '8');
INSERT INTO `sys_menu_button` VALUES ('31', '1001005', '6');
INSERT INTO `sys_menu_button` VALUES ('32', '1001005', '6');
INSERT INTO `sys_menu_button` VALUES ('33', '1001005', '7');
INSERT INTO `sys_menu_button` VALUES ('34', '1001005', '7');
INSERT INTO `sys_menu_button` VALUES ('35', '1001005', '9');
INSERT INTO `sys_menu_button` VALUES ('36', '1001005', '9');
INSERT INTO `sys_menu_button` VALUES ('37', '1001005', '1');
INSERT INTO `sys_menu_button` VALUES ('38', '1001005', '1');
INSERT INTO `sys_menu_button` VALUES ('44', '1001003', '8');
INSERT INTO `sys_menu_button` VALUES ('45', '1001003', '6');
INSERT INTO `sys_menu_button` VALUES ('46', '1001003', '7');
INSERT INTO `sys_menu_button` VALUES ('47', '1001003', '9');
INSERT INTO `sys_menu_button` VALUES ('48', '1001003', '1');
INSERT INTO `sys_menu_button` VALUES ('49', '1001003', '11');
INSERT INTO `sys_menu_button` VALUES ('50', '1001003', '12');

-- ----------------------------
-- Table structure for sys_menu_tabs
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_tabs`;
CREATE TABLE `sys_menu_tabs` (
  `i_id` int(11) NOT NULL auto_increment,
  `i_menu_id` varchar(50) default NULL,
  `i_tab_id` int(11) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_tabs
-- ----------------------------

-- ----------------------------
-- Table structure for sys_org_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_org_info`;
CREATE TABLE `sys_org_info` (
  `i_org_id` int(11) NOT NULL auto_increment,
  `c_org_code` varchar(100) default NULL,
  `c_org_name` varchar(100) default NULL,
  `c_org_duty` varchar(200) default NULL,
  `i_enabled` int(4) default NULL,
  `c_memo` varchar(200) default NULL,
  `c_org_pid` varchar(50) default NULL,
  `c_org_leader` int(10) default NULL,
  PRIMARY KEY  (`i_org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_org_info
-- ----------------------------
INSERT INTO `sys_org_info` VALUES ('1', '1', '根节点', null, null, null, null, null);
INSERT INTO `sys_org_info` VALUES ('2', '1001', 'XX网络科技', '', '1', null, null, null);
INSERT INTO `sys_org_info` VALUES ('3', '1001001', '研发部', '', '1', null, null, null);
INSERT INTO `sys_org_info` VALUES ('4', '1001002', '经理部', '', '1', null, null, null);
INSERT INTO `sys_org_info` VALUES ('5', '1001003', '营销部', '', '1', null, null, null);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `i_role_id` int(11) NOT NULL auto_increment,
  `c_role_name` varchar(100) default NULL,
  `i_enabled` int(4) default NULL,
  `c_role_desc` varchar(200) default NULL,
  PRIMARY KEY  (`i_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '系统管理员', '1', null);
INSERT INTO `sys_role` VALUES ('2', '普通员工', '1', '');
INSERT INTO `sys_role` VALUES ('3', '人事管理员', '1', '');
INSERT INTO `sys_role` VALUES ('4', '高层管理者', '1', '');

-- ----------------------------
-- Table structure for sys_rolegroup_role_mapping
-- ----------------------------
DROP TABLE IF EXISTS `sys_rolegroup_role_mapping`;
CREATE TABLE `sys_rolegroup_role_mapping` (
  `i_id` int(11) NOT NULL auto_increment,
  `i_group_id` int(11) default NULL,
  `i_role_id` int(11) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_rolegroup_role_mapping
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_group`;
CREATE TABLE `sys_role_group` (
  `i_group_id` int(11) NOT NULL auto_increment,
  `c_group_name` varchar(100) default NULL,
  `i_enabled` int(4) default NULL,
  `c_group_desc` varchar(200) default NULL,
  PRIMARY KEY  (`i_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_group
-- ----------------------------
INSERT INTO `sys_role_group` VALUES ('1', '史蒂夫', '1', '');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `i_id` int(11) NOT NULL auto_increment,
  `i_role_id` int(11) default NULL,
  `i_menu_id` varchar(50) default NULL,
  `c_button_ids` varchar(50) default NULL,
  `c_tab_ids` varchar(50) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('81', '2', '1003', null, null);
INSERT INTO `sys_role_menu` VALUES ('82', '2', '1003001', null, null);
INSERT INTO `sys_role_menu` VALUES ('83', '1', '1001', null, null);
INSERT INTO `sys_role_menu` VALUES ('84', '1', '1001001', null, null);
INSERT INTO `sys_role_menu` VALUES ('85', '1', '1001003', null, null);
INSERT INTO `sys_role_menu` VALUES ('86', '1', '1001002', null, null);
INSERT INTO `sys_role_menu` VALUES ('87', '1', '1001006', null, null);
INSERT INTO `sys_role_menu` VALUES ('88', '1', '1001005', null, null);
INSERT INTO `sys_role_menu` VALUES ('89', '1', '1002', null, null);
INSERT INTO `sys_role_menu` VALUES ('90', '1', '1002001', null, null);
INSERT INTO `sys_role_menu` VALUES ('91', '1', '1002002', null, null);
INSERT INTO `sys_role_menu` VALUES ('92', '1', '1002003', null, null);
INSERT INTO `sys_role_menu` VALUES ('93', '1', '1002004', null, null);
INSERT INTO `sys_role_menu` VALUES ('94', '1', '1003', null, null);
INSERT INTO `sys_role_menu` VALUES ('95', '1', '1003001', null, null);
INSERT INTO `sys_role_menu` VALUES ('96', '1', '1004', null, null);
INSERT INTO `sys_role_menu` VALUES ('97', '1', '1004001', null, null);
INSERT INTO `sys_role_menu` VALUES ('109', '3', '1001003', null, null);
INSERT INTO `sys_role_menu` VALUES ('110', '3', '1001', null, null);
INSERT INTO `sys_role_menu` VALUES ('111', '3', '1001002', null, null);
INSERT INTO `sys_role_menu` VALUES ('112', '3', '1002002', null, null);
INSERT INTO `sys_role_menu` VALUES ('113', '3', '1002', null, null);
INSERT INTO `sys_role_menu` VALUES ('114', '3', '1002003', null, null);
INSERT INTO `sys_role_menu` VALUES ('115', '3', '1002004', null, null);
INSERT INTO `sys_role_menu` VALUES ('116', '3', '1003', null, null);
INSERT INTO `sys_role_menu` VALUES ('117', '3', '1003001', null, null);
INSERT INTO `sys_role_menu` VALUES ('118', '4', '1004', null, null);
INSERT INTO `sys_role_menu` VALUES ('119', '4', '1004001', null, null);

-- ----------------------------
-- Table structure for sys_send_date
-- ----------------------------
DROP TABLE IF EXISTS `sys_send_date`;
CREATE TABLE `sys_send_date` (
  `i_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(50) default NULL,
  `c_week_date` varchar(50) default NULL,
  `c_memo` varchar(50) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_send_date
-- ----------------------------

-- ----------------------------
-- Table structure for sys_send_email
-- ----------------------------
DROP TABLE IF EXISTS `sys_send_email`;
CREATE TABLE `sys_send_email` (
  `i_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(50) default NULL,
  `c_email` varchar(50) default NULL,
  `c_password` varchar(50) default NULL,
  `c_memo` varchar(50) default NULL,
  `c_stmp` varchar(50) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_send_email
-- ----------------------------

-- ----------------------------
-- Table structure for sys_send_time
-- ----------------------------
DROP TABLE IF EXISTS `sys_send_time`;
CREATE TABLE `sys_send_time` (
  `i_id` int(11) NOT NULL auto_increment,
  `d_start_time` varchar(50) default NULL,
  `d_end_time` varchar(50) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_send_time
-- ----------------------------
INSERT INTO `sys_send_time` VALUES ('1', '17:00', '20:50');

-- ----------------------------
-- Table structure for sys_tabs
-- ----------------------------
DROP TABLE IF EXISTS `sys_tabs`;
CREATE TABLE `sys_tabs` (
  `i_id` int(11) NOT NULL auto_increment,
  `c_tab_name` varchar(50) default NULL,
  `i_enabled` int(4) default NULL,
  `i_is_opt` int(4) default NULL,
  `c_memo` varchar(100) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_tabs
-- ----------------------------
INSERT INTO `sys_tabs` VALUES ('1', '选项卡一', '1', null, '12');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `i_user_id` int(11) NOT NULL auto_increment,
  `c_userloginname` varchar(50) default NULL,
  `c_userloginpwd` varchar(100) default NULL,
  `c_username` varchar(50) default NULL,
  `c_org_code` varchar(100) default NULL,
  `c_org_code_attach` varchar(500) default NULL,
  `c_telephone` varchar(50) default NULL,
  `c_work_number` varchar(50) default NULL,
  `c_position_name` varchar(100) default NULL,
  `d_createtime` varchar(50) default NULL,
  `i_logincount` int(11) default NULL,
  `c_spelllong` varchar(50) default NULL,
  `c_spellshort` varchar(50) default NULL,
  `i_enabled` int(4) default NULL,
  `c_memo` varchar(500) default NULL,
  PRIMARY KEY  (`i_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('2', 'zhangsan', 'e10adc3949ba59abbe56e057f20f883e', '张三', '1001001', '1001001,', '1345041XXXX', '1129411884@qq.com', null, '2014-09-22 10:46:30', null, '', '', '1', '');
INSERT INTO `sys_user` VALUES ('3', 'lisi', 'e10adc3949ba59abbe56e057f20f883e', '李四', '1001001', null, '', '690691565@qq.com', null, '2014-11-04 02:01:46', null, '', '', '1', '');
INSERT INTO `sys_user` VALUES ('4', 'wangwu', 'e10adc3949ba59abbe56e057f20f883e', '王五', '1001001', null, '', '1101362282@qq.com', null, '2014-11-04 02:03:06', null, '', '', '1', '');
INSERT INTO `sys_user` VALUES ('5', 'mayun', 'e10adc3949ba59abbe56e057f20f883e', '马云', '1001001', null, '', '442171865@qq.com', null, '2014-11-04 02:03:49', null, '', '', '1', '');
INSERT INTO `sys_user` VALUES ('6', 'liyanhong', 'e10adc3949ba59abbe56e057f20f883e', '李彦宏', '1001001', null, '', '578917839@qq.com', null, '2014-11-04 02:05:00', null, '', '', '1', '');
INSERT INTO `sys_user` VALUES ('7', 'mahuateng', '96e79218965eb72c92a549dd5a330112', '马化腾', '1001001', null, '', '454596285@qq.com', null, '2014-11-04 02:06:08', null, '', '', '1', '');
INSERT INTO `sys_user` VALUES ('8', 'aobanma', 'e10adc3949ba59abbe56e057f20f883e', '奥巴马', '1001002', null, '', '1129411884@qq.com', null, '2014-11-04 02:08:56', null, '', '', '1', '');
INSERT INTO `sys_user` VALUES ('10', 'xidada', 'e10adc3949ba59abbe56e057f20f883e', '习大大', '1001002', null, '', '1107977130@qq.com', null, '2014-11-04 02:10:09', null, '', '', '1', '');
INSERT INTO `sys_user` VALUES ('11', 'xiaoliang', 'e10adc3949ba59abbe56e057f20f883e', '小亮', '1001003', null, '', '842495323@qq.com', null, '2014-11-04 02:11:11', null, '', '', '1', '');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `i_id` int(11) NOT NULL auto_increment,
  `i_user_id` int(11) default NULL,
  `i_role_id` int(11) default NULL,
  `i_org_code` varchar(50) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('2', '2', '1', '1001001');
INSERT INTO `sys_user_role` VALUES ('7', '11', '2', '1001003');
INSERT INTO `sys_user_role` VALUES ('9', '2', '2', '1001001');
INSERT INTO `sys_user_role` VALUES ('10', '3', '2', '1001001');
INSERT INTO `sys_user_role` VALUES ('11', '4', '2', '1001001');
INSERT INTO `sys_user_role` VALUES ('12', '5', '2', '1001001');
INSERT INTO `sys_user_role` VALUES ('13', '6', '2', '1001001');
INSERT INTO `sys_user_role` VALUES ('14', '7', '2', '1001001');
INSERT INTO `sys_user_role` VALUES ('15', '4', '3', '1001001');
INSERT INTO `sys_user_role` VALUES ('16', '8', '4', '1001002');
INSERT INTO `sys_user_role` VALUES ('22', '10', '4', '1001002');
INSERT INTO `sys_user_role` VALUES ('23', '10', '3', '1001002');

-- ----------------------------
-- Table structure for sys_user_role_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role_group`;
CREATE TABLE `sys_user_role_group` (
  `i_id` int(11) NOT NULL auto_increment,
  `i_user_id` int(11) default NULL,
  `i_group_id` int(11) default NULL,
  `i_org_code` varchar(50) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role_group
-- ----------------------------

-- ----------------------------
-- Table structure for sys_work_send_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_work_send_group`;
CREATE TABLE `sys_work_send_group` (
  `i_id` int(11) NOT NULL auto_increment,
  `c_group_name` varchar(50) default NULL,
  `c_sender_ids` varchar(300) default NULL,
  `c_worker_ids` varchar(300) default NULL,
  `c_creat_time` varchar(50) default NULL,
  `c_enabled` int(11) default NULL,
  `c_memo` varchar(50) default NULL,
  `c_time_tip` int(4) default NULL,
  PRIMARY KEY  (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_work_send_group
-- ----------------------------
INSERT INTO `sys_work_send_group` VALUES ('2', 'XX科技', '2,', '2,3,4,5,6,7,11,', '2014-10-26', '1', '', '0');
INSERT INTO `sys_work_send_group` VALUES ('3', '史蒂夫', '1,', '2,', '2014-10-26', '0', '电风扇', '1');
