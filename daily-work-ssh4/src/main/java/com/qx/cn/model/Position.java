package com.qx.cn.model;

import java.io.Serializable;

public class Position implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -2175314582479128394L;
    
    private Long poliId;
    
    private String names;
    
    private String posumber;
    
    private Long did;
    
    private String orgCode;
    
    private Integer showHide;
    
    public Position()
    {
    }
    
    public Position(String names, String posumber, Long did, String orgCode)
    {
        this.names = names;
        this.posumber = posumber;
        this.did = did;
        this.orgCode = orgCode;
    }
    
    public Long getPoliId()
    {
        return this.poliId;
    }
    
    public void setPoliId(Long poliId)
    {
        this.poliId = poliId;
    }
    
    public String getNames()
    {
        return this.names;
    }
    
    public void setNames(String names)
    {
        this.names = names;
    }
    
    public String getPosumber()
    {
        return this.posumber;
    }
    
    public void setPosumber(String posumber)
    {
        this.posumber = posumber;
    }
    
    public Long getDid()
    {
        return this.did;
    }
    
    public void setDid(Long did)
    {
        this.did = did;
    }
    
    public String getOrgCode()
    {
        return this.orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public Integer getShowHide()
    {
        return this.showHide;
    }
    
    public void setShowHide(Integer showHide)
    {
        this.showHide = showHide;
    }
}