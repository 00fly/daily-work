package com.qx.cn.model;

import java.io.Serializable;

public class RoleGroupMapping implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1957797512216588663L;
    
    private String id;
    
    private String roleId;
    
    private Long groupId;
    
    public String getId()
    {
        return this.id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getRoleId()
    {
        return this.roleId;
    }
    
    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }
    
    public Long getGroupId()
    {
        return this.groupId;
    }
    
    public void setGroupId(Long groupId)
    {
        this.groupId = groupId;
    }
}