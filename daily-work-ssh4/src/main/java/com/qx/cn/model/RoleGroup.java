package com.qx.cn.model;

import java.io.Serializable;

public class RoleGroup implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -2019019909629407336L;
    
    private Long groupId;
    
    private String groupName;
    
    private Integer enabled;
    
    private String groupDesc;
    
    public String getGroupName()
    {
        return this.groupName;
    }
    
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getGroupDesc()
    {
        return this.groupDesc;
    }
    
    public void setGroupDesc(String groupDesc)
    {
        this.groupDesc = groupDesc;
    }
    
    public Long getGroupId()
    {
        return this.groupId;
    }
    
    public void setGroupId(Long groupId)
    {
        this.groupId = groupId;
    }
}