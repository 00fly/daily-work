package com.qx.cn.model;

import java.io.Serializable;

public class MenuTabs implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 6915218613384708034L;
    
    private Long id;
    
    private String menuId;
    
    private Long tabId;
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public String getMenuId()
    {
        return this.menuId;
    }
    
    public void setMenuId(String menuId)
    {
        this.menuId = menuId;
    }
    
    public Long getTabId()
    {
        return this.tabId;
    }
    
    public void setTabId(Long tabId)
    {
        this.tabId = tabId;
    }
}