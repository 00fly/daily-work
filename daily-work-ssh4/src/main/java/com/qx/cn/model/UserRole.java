package com.qx.cn.model;

import java.io.Serializable;

public class UserRole implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -7434340192215268974L;
    
    private Long id;
    
    private Long userId;
    
    private Long roleId;
    
    private String orgCode;
    
    public String getOrgCode()
    {
        return this.orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public Long getUserId()
    {
        return this.userId;
    }
    
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }
    
    public Long getRoleId()
    {
        return this.roleId;
    }
    
    public void setRoleId(Long roleId)
    {
        this.roleId = roleId;
    }
}