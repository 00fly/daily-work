package com.qx.cn.model;

import java.io.Serializable;

public class WorkReportItem implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 7659709459200512165L;
    
    private Integer id;
    
    private String content;
    
    private String createTime;
    
    private String reportCode;
    
    private String workTime;
    
    private String hopeFinishDate; // 要求完成时间
    
    private String actualFinishDate; // 实际完成时间
    
    private String remarks; // 备注
    
    private Integer type; // 类型
    
    public Integer getId()
    {
        return this.id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getContent()
    {
        return this.content;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public String getCreateTime()
    {
        return this.createTime;
    }
    
    public void setCreateTime(String createTime)
    {
        this.createTime = createTime;
    }
    
    public String getReportCode()
    {
        return this.reportCode;
    }
    
    public void setReportCode(String reportCode)
    {
        this.reportCode = reportCode;
    }
    
    public String getWorkTime()
    {
        return this.workTime;
    }
    
    public void setWorkTime(String workTime)
    {
        this.workTime = workTime;
    }
    
    public String getHopeFinishDate()
    {
        return hopeFinishDate;
    }
    
    public void setHopeFinishDate(String hopeFinishDate)
    {
        this.hopeFinishDate = hopeFinishDate;
    }
    
    public String getActualFinishDate()
    {
        return actualFinishDate;
    }
    
    public void setActualFinishDate(String actualFinishDate)
    {
        this.actualFinishDate = actualFinishDate;
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }
    
    public Integer getType()
    {
        return type;
    }
    
    public void setType(Integer type)
    {
        this.type = type;
    }
}