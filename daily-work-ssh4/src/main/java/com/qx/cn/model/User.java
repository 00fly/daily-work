package com.qx.cn.model;

import java.io.Serializable;

public class User implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 1992295269306965619L;
    
    private Long userId;
    
    private String userLoginName;
    
    private String userLoginPwd;
    
    private String userName;
    
    private String orgCode;
    
    private String orgCodeAttach;
    
    private String telePhone;
    
    private String workNumber;
    
    private String positionName;
    
    private String createTime;
    
    private Integer loginCount;
    
    private String spellLong;
    
    private String spellShort;
    
    private Integer enabled;
    
    private String memo;
    
    public Long getUserId()
    {
        return this.userId;
    }
    
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }
    
    public String getUserLoginName()
    {
        return this.userLoginName;
    }
    
    public void setUserLoginName(String userLoginName)
    {
        this.userLoginName = userLoginName;
    }
    
    public String getUserLoginPwd()
    {
        return this.userLoginPwd;
    }
    
    public void setUserLoginPwd(String userLoginPwd)
    {
        this.userLoginPwd = userLoginPwd;
    }
    
    public String getUserName()
    {
        return this.userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public String getOrgCode()
    {
        return this.orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public String getOrgCodeAttach()
    {
        return this.orgCodeAttach;
    }
    
    public void setOrgCodeAttach(String orgCodeAttach)
    {
        this.orgCodeAttach = orgCodeAttach;
    }
    
    public String getTelePhone()
    {
        return this.telePhone;
    }
    
    public void setTelePhone(String telePhone)
    {
        this.telePhone = telePhone;
    }
    
    public String getWorkNumber()
    {
        return this.workNumber;
    }
    
    public void setWorkNumber(String workNumber)
    {
        this.workNumber = workNumber;
    }
    
    public String getPositionName()
    {
        return this.positionName;
    }
    
    public void setPositionName(String positionName)
    {
        this.positionName = positionName;
    }
    
    public String getCreateTime()
    {
        return this.createTime;
    }
    
    public void setCreateTime(String createTime)
    {
        this.createTime = createTime;
    }
    
    public Integer getLoginCount()
    {
        return this.loginCount;
    }
    
    public void setLoginCount(Integer loginCount)
    {
        this.loginCount = loginCount;
    }
    
    public String getSpellLong()
    {
        return this.spellLong;
    }
    
    public void setSpellLong(String spellLong)
    {
        this.spellLong = spellLong;
    }
    
    public String getSpellShort()
    {
        return this.spellShort;
    }
    
    public void setSpellShort(String spellShort)
    {
        this.spellShort = spellShort;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMemo()
    {
        return this.memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
}