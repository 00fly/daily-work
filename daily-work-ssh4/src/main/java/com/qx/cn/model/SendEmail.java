package com.qx.cn.model;

import java.io.Serializable;

public class SendEmail implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -827892556821459529L;
    
    private Integer id;
    
    private String name;
    
    private String email;
    
    private String password;
    
    private String meno;
    
    private String stmp;
    
    public Integer getId()
    {
        return this.id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getEmail()
    {
        return this.email;
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public String getPassword()
    {
        return this.password;
    }
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    public String getMeno()
    {
        return this.meno;
    }
    
    public void setMeno(String meno)
    {
        this.meno = meno;
    }
    
    public String getStmp()
    {
        return this.stmp;
    }
    
    public void setStmp(String stmp)
    {
        this.stmp = stmp;
    }
}