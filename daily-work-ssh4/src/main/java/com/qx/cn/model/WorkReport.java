package com.qx.cn.model;

import java.io.Serializable;

public class WorkReport implements Serializable, Comparable<WorkReport>
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 8779735279794814331L;
    
    private Integer id;
    
    private String workReportName;
    
    private String createTime;
    
    private String sendTime;
    
    private Integer userId;
    
    private Integer status;
    
    private String memo;
    
    private String userName;
    
    private String orgName;
    
    private String position;
    
    private String reportCode;
    
    private String type;
    
    public Integer getId()
    {
        return this.id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getWorkReportName()
    {
        return this.workReportName;
    }
    
    public void setWorkReportName(String workReportName)
    {
        this.workReportName = workReportName;
    }
    
    public String getCreateTime()
    {
        return this.createTime;
    }
    
    public void setCreateTime(String createTime)
    {
        this.createTime = createTime;
    }
    
    public String getSendTime()
    {
        return this.sendTime;
    }
    
    public void setSendTime(String sendTime)
    {
        this.sendTime = sendTime;
    }
    
    public Integer getUserId()
    {
        return this.userId;
    }
    
    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }
    
    public Integer getStatus()
    {
        return this.status;
    }
    
    public void setStatus(Integer status)
    {
        this.status = status;
    }
    
    public String getMemo()
    {
        return this.memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
    
    public String getReportCode()
    {
        return this.reportCode;
    }
    
    public void setReportCode(String reportCode)
    {
        this.reportCode = reportCode;
    }
    
    public String getUserName()
    {
        return this.userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public String getOrgName()
    {
        return this.orgName;
    }
    
    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }
    
    public String getPosition()
    {
        return this.position;
    }
    
    public void setPosition(String position)
    {
        this.position = position;
    }
    
    public String getType()
    {
        return type;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
    
    @Override
    public int compareTo(WorkReport it)
    {
        return this.getCreateTime().compareTo(it.getCreateTime());
    }
    
}