package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.User;

public interface UserService
{
    public boolean saveUserByCode(User paramUser);
    
    public List<User> queryUserByPageAndCode(String paramString, int paramInt1, int paramInt2);
    
    public boolean deleteUserByUserId(Long paramLong);
    
    public User loadUserById(Long paramLong);
    
    public long getTotalUsers(String paramString);
    
    public List queryUserByPageAndCodeSQL(User paramUser, int paramInt1, int paramInt2);
    
    public List<User> queryUserByCode(String paramString);
    
    public boolean deleteUserByOrgCode(String paramString);
    
    public boolean updateUserByChangeOrg(String paramString1, String paramString2);
    
    public User loadAdminByPwdAndAccount(String paramString1, String paramString2);
    
    public List<User> queryUserPageSizeByCode(User paramUser);
    
    public List<User> queryUserByLikeCode(String paramString);
    
    public List<User> queryUsersByPosCode(String paramString);
    
    public User loadUserByOrgAndId(String paramString1, String paramString2);
    
    public List<User> queryAllUser();
}