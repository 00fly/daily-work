package com.qx.cn.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.User;
import com.qx.cn.model.WorkReport;
import com.qx.cn.service.WorkReportService;

@Service
public class WorkReportServiceImpl implements WorkReportService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List queryWorkReportByPage(WorkReport sdObj, int page, int rows)
    {
        int firstPage = rows * (page - 1);
        int lastPage = rows;
        String sqlStr = "";
        if ((!"".equals(sdObj.getWorkReportName())) && (sdObj.getWorkReportName() != null))
        {
            sqlStr = " and c_work_report_name like '%" + sdObj.getWorkReportName() + "%'";
        }
        if (StringUtils.isNotEmpty(sdObj.getType()))
        {
            sqlStr += " and c_type='" + sdObj.getType() + "'";
        }
        String sql = "select * from busi_work_report au where au.i_user_id = " + sdObj.getUserId() + sqlStr + " order by au.d_create_time desc limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public boolean deleteWorkReport(int id)
    {
        baseDAO.deleteById(WorkReport.class, Integer.valueOf(id));
        return true;
    }
    
    @Override
    public WorkReport loadWorkReportById(int id)
    {
        return (WorkReport)baseDAO.loadById(WorkReport.class, Integer.valueOf(id));
    }
    
    @Override
    public List<WorkReport> queryAllWorkReports(WorkReport sdObj)
    {
        String sqlStr = "";
        
        if ((!"".equals(sdObj.getWorkReportName())) && (sdObj.getWorkReportName() != null))
        {
            sqlStr = sqlStr + " and workReportName like '%" + sdObj.getWorkReportName() + "%'";
        }
        if (StringUtils.isNotEmpty(sdObj.getType()))
        {
            sqlStr += " and c_type='" + sdObj.getType() + "'";
        }
        String hql = "from WorkReport where userId = " + sdObj.getUserId() + sqlStr;
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean saveWorkReport(WorkReport reWorkReport)
    {
        baseDAO.saveOrUpdate(reWorkReport);
        return true;
    }
    
    public List<WorkReport> queryAllWorkReport()
    {
        String hql = "from WorkReport ";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean queryCheckIsWorkReports(String date, Long userId, String type)
    {
        String hql = "from WorkReport where createTime like '" + date + "%' and userId=" + userId + " and type='" + type + "'";
        boolean flag = false;
        List workReports = baseDAO.query(hql);
        if (workReports.size() > 0)
        {
            flag = false;
        }
        else
        {
            flag = true;
        }
        return flag;
    }
    
    @Override
    public WorkReport loadWorkReportByUserAndDate(String date, Long userId)
    {
        String hql = "from WorkReport where createTime like '" + date + "%' and userId=" + userId;
        return (WorkReport)baseDAO.loadByHql(hql);
    }
    
    @Override
    public WorkReport loadWorkReportByUserAndName(String name, Long userId)
    {
        String hql = "from WorkReport where workReportName like '" + name + "%' and userId=" + userId;
        return (WorkReport)baseDAO.loadByHql(hql);
    }
    
    @Override
    public List<WorkReport> queryAllWorkReportsByDate(String startDate, String endDate, User user, String type)
    {
        String hql = "from WorkReport where createTime <= '" + endDate + " 23:59:59' and createTime >='" + startDate + "' and userId=" + user.getUserId() + " and type='" + type + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteWorkReportCode(String reportCode)
    {
        String hql = "delete from WorkReport wr where wr.reportCode = '" + reportCode + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
}