package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.Tab;

public interface TabService
{
    public List queryTabsByPage(Tab paramTab, int paramInt1, int paramInt2);
    
    public List<Tab> queryAllTabs();
    
    public boolean saveTab(Tab paramTab);
    
    public boolean deleteTab(Long paramLong);
    
    public Tab loadTabById(Long paramLong);
}