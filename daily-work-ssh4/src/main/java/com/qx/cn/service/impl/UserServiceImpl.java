package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.User;
import com.qx.cn.service.UserService;

@Service
public class UserServiceImpl implements UserService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public boolean saveUserByCode(User user)
    {
        baseDAO.saveOrUpdate(user);
        return true;
    }
    
    @Override
    public List<User> queryUserByPageAndCode(String orgCode, int page, int rows)
    {
        String hql = "from User u where u.orgCode = '" + orgCode + "'";
        return baseDAO.query(hql, page, rows, new Object[0]);
    }
    
    @Override
    public boolean deleteUserByUserId(Long userId)
    {
        baseDAO.deleteById(User.class, userId);
        return true;
    }
    
    @Override
    public User loadUserById(Long userId)
    {
        User user = (User)baseDAO.loadById(User.class, userId);
        return user;
    }
    
    @Override
    public long getTotalUsers(String orgCode)
    {
        String hql = "select count(*) from User u where u.orgCode = '" + orgCode + "'";
        return baseDAO.countByHql(hql);
    }
    
    @Override
    public List queryUserByPageAndCodeSQL(User uss, int page, int rows)
    {
        String sqlStr = "";
        
        if ((!"".equals(uss.getUserName())) && (uss.getUserName() != null))
        {
            sqlStr = sqlStr + " and C_USERNAME like '%" + uss.getUserName() + "%'";
        }
        
        if ((!"".equals(uss.getUserLoginName())) && (uss.getUserLoginName() != null))
        {
            sqlStr = sqlStr + " and C_USERLOGINNAME like '%" + uss.getUserLoginName() + "%'";
        }
        
        if ((!"".equals(uss.getWorkNumber())) && (uss.getWorkNumber() != null))
        {
            sqlStr = sqlStr + " and C_WORK_NUMBER like '%" + uss.getWorkNumber() + "%'";
        }
        
        if ((!"".equals(uss.getSpellLong())) && (uss.getSpellLong() != null))
        {
            sqlStr = sqlStr + " and C_SPELLLONG like '%" + uss.getSpellLong() + "%' or C_SPELLSHORT like '%" + uss.getSpellLong() + "%'";
        }
        
        int firstPage = rows * (page - 1);
        int lastPage = rows;
        String sql = "select * from (select * from SYS_USER au where au.c_org_code = '" + uss.getOrgCode() + "'" + sqlStr + " order by c_spellshort asc) a limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public List<User> queryUserByCode(String orgCode)
    {
        String hql = "from User u where u.orgCode = '" + orgCode + "' order by spellShort asc";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteUserByOrgCode(String orgCode)
    {
        String hql = "delete from User user where user.orgCode like '" + orgCode + "%'";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public boolean updateUserByChangeOrg(String orgCode, String orgCode2)
    {
        String hql = "update User user set orgCode = '" + orgCode2 + "' where user.orgCode = '" + orgCode + "'";
        baseDAO.update(hql);
        return true;
    }
    
    @Override
    public User loadAdminByPwdAndAccount(String userAccount, String pwd)
    {
        String hql = "from User u where u.userLoginName = '" + userAccount + "' and u.userLoginPwd = '" + pwd + "' and u.enabled =1";
        User user = (User)baseDAO.loadByHql(hql);
        return user;
    }
    
    @Override
    public List<User> queryUserPageSizeByCode(User uss)
    {
        String sqlStr = "";
        
        if ((!"".equals(uss.getUserName())) && (uss.getUserName() != null))
        {
            sqlStr = sqlStr + " and userName like '%" + uss.getUserName() + "%'";
        }
        
        if ((!"".equals(uss.getUserLoginName())) && (uss.getUserLoginName() != null))
        {
            sqlStr = sqlStr + " and userLoginName like '%" + uss.getUserLoginName() + "%'";
        }
        
        if ((!"".equals(uss.getWorkNumber())) && (uss.getWorkNumber() != null))
        {
            sqlStr = sqlStr + " and workNumber like '%" + uss.getWorkNumber() + "%'";
        }
        
        if ((!"".equals(uss.getSpellLong())) && (uss.getSpellLong() != null))
        {
            sqlStr = sqlStr + " and spellLong like '%" + uss.getSpellLong() + "%' or spellShort like '%" + uss.getSpellLong() + "%'";
        }
        
        String sql = "from User u where u.orgCode like '" + uss.getOrgCode() + "%'" + sqlStr;
        return baseDAO.query(sql);
    }
    
    @Override
    public List<User> queryUserByLikeCode(String orgCode)
    {
        String hql = "from User u where u.orgCode like '" + orgCode + "%'";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<User> queryUsersByPosCode(String posCode)
    {
        String hql = "from User u where u.positionName = '" + posCode + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public User loadUserByOrgAndId(String orgCode, String string)
    {
        return null;
    }
    
    @Override
    public List<User> queryAllUser()
    {
        String hql = "from User ";
        return baseDAO.query(hql);
    }
}