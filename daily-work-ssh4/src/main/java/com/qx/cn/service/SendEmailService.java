package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.SendDate;
import com.qx.cn.model.SendEmail;
import com.qx.cn.model.SendTime;
import com.qx.cn.model.User;

public interface SendEmailService
{
    List querySendEmailByPage(int page, int rows);
    
    boolean deleteSendEmail(int id);
    
    SendEmail loadSendEmailById(int id);
    
    List<SendEmail> queryAllSendEmails();
    
    boolean saveSendEmail(SendEmail reSendEmail);
    
    List<SendTime> querySendTimes();
    
    boolean checkIsSendDate(String date);
    
    SendDate queryBySendDate(String date);
    
    boolean notSend(String date);
    
    List queryWorKReportItem(String reportCode);
    
    List querySendEmail();
    
    List queryWorkSendGroup();
    
    User loadUserById(Integer id);
    
    void updateWorkSendGroupTimeTip(int id, int status);
    
    void insertManageWorkReport(Integer reUserId, String cWorkerIds, String groupName, String type);
    
}