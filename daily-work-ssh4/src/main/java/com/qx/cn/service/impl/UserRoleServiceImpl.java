package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.UserRole;
import com.qx.cn.service.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List<UserRole> queryUsersToRoleByOrgCode(String orgCode, String roleId)
    {
        String hql = "from UserRole ur where ur.orgCode = '" + orgCode + "' and ur.roleId = '" + roleId + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteUserRoles(String orgCode, String roleId)
    {
        String hql = "delete from UserRole urg where urg.orgCode = '" + orgCode + "' and urg.roleId = '" + roleId + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public List<UserRole> queryUserRolesById(String orgCode, String roleId)
    {
        String hql = "from UserRole urg where urg.orgCode = '" + orgCode + "' and urg.roleId = '" + roleId + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean saveUserRole(UserRole userRole)
    {
        baseDAO.saveOrUpdate(userRole);
        return true;
    }
    
    @Override
    public List<UserRole> queryRolesByUserId(Long userId)
    {
        String hql = "from UserRole ur where ur.userId = " + userId;
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteRolesUser(Long userId)
    {
        String hql = "delete from UserRole ur where ur.userId = " + userId;
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public boolean deleteUserRoles(String roleId)
    {
        String hql = "delete from UserRole ur where ur.roleId = '" + roleId + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
}