package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.Position;

public interface PositionService
{
    public List queryPositionByPageAndCodeSQL(Position paramPosition, int paramInt1, int paramInt2);
    
    public List<Position> queryPositionPageSizeByCode(Position paramPosition);
    
    public boolean savePosition(Position paramPosition);
    
    public boolean deletePosition(Long paramLong);
    
    public Position loadPositionById(Long paramLong);
    
    public List<Position> queryPositionByCode(String paramString);
    
    public List<Position> queryPositionByOrgCode(String paramString);
    
    public List<Position> queryAllPosition();
    
    public List<Position> queryAllPositionByShow();
    
    public List<Position> queryPositionByPosCode(String paramString);
    
    public List<Position> queryAllPositionByA();
}