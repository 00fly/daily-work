package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.SendDate;

public interface SendDateService
{
    public List querySendDateByPage(SendDate paramSendDate, int paramInt1, int paramInt2);
    
    public List<SendDate> queryAllSendDates(SendDate paramSendDate);
    
    public boolean deleteSendDate(int paramInt);
    
    public SendDate loadSendDateById(int paramInt);
    
    public boolean saveSendDate(SendDate date);
    
    public SendDate queryAllSendDateByDate(String date, String type);
    
    public SendDate querySendDateByAuto();
}