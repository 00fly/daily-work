package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.UserRoleGroup;

public interface UserRoleGroupService
{
    public List<UserRoleGroup> queryUsersToGroupByOrgCode(String paramString, Long paramLong);
    
    public boolean deleteUserGroupRoles(String paramString, Long paramLong);
    
    public boolean saveUserRoleGroup(UserRoleGroup paramUserRoleGroup);
    
    public List<UserRoleGroup> queryGroupsByUserId(Long paramLong);
    
    public boolean deleteGroupsUser(Long paramLong);
    
    public boolean saveUserGroup(UserRoleGroup paramUserRoleGroup);
    
    public boolean deleteGroupsUserByGroupId(Long paramLong);
}