package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.Tab;
import com.qx.cn.service.TabService;

@Service
public class TabServiceImpl implements TabService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public boolean deleteTab(Long id)
    {
        baseDAO.deleteById(Tab.class, id);
        return true;
    }
    
    @Override
    public Tab loadTabById(Long id)
    {
        return (Tab)baseDAO.loadById(Tab.class, id);
    }
    
    @Override
    public List queryTabsByPage(Tab tab, int page, int rows)
    {
        int firstPage = rows * (page - 1);
        int lastPage = rows;
        String sql = "select * from sys_tabs au where au.i_id is not null limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public boolean saveTab(Tab tab)
    {
        baseDAO.saveOrUpdate(tab);
        return true;
    }
    
    @Override
    public List<Tab> queryAllTabs()
    {
        return baseDAO.query("from Tab");
    }
}