package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.ManagerWorkReport;

public interface ManageWorkReportService
{
    public ManagerWorkReport queryById(Integer id);
    
    public List queryWorkReportByPage(ManagerWorkReport paramManagerWorkReport, int paramInt1, int paramInt2);
    
    public List<ManagerWorkReport> queryAllMWorkReports(ManagerWorkReport paramManagerWorkReport);
}