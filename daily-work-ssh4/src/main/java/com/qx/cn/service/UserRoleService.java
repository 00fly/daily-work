package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.UserRole;

public interface UserRoleService
{
    public List<UserRole> queryUsersToRoleByOrgCode(String paramString1, String paramString2);
    
    public List<UserRole> queryUserRolesById(String orgCode, String roleId);
    
    public boolean deleteUserRoles(String paramString1, String paramString2);
    
    public boolean saveUserRole(UserRole paramUserRole);
    
    public List<UserRole> queryRolesByUserId(Long paramLong);
    
    public boolean deleteRolesUser(Long paramLong);
    
    public boolean deleteUserRoles(String roleId);
}