package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.ConfigData;

public interface ConfigDataService
{
    public List<ConfigData> queryConfigByPar();
    
    public boolean saveConfig(ConfigData paramConfigData);
    
    public boolean deleteConfigByCode(String paramString);
    
    public ConfigData loadConfigByCode(String paramString);
    
    public List<ConfigData> querySonConfigByParCode(String paramString, int paramInt);
    
    public List<ConfigData> queryConfigByDataDomain(String paramString, int paramInt);
    
    public void updateDataByCode(String paramString1, String paramString2);
    
    public ConfigData loadConfigByDOMAndOrder(String paramString);
}