package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.Position;
import com.qx.cn.service.PositionService;

@Service
public class PositionServiceImpl implements PositionService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List queryPositionByPageAndCodeSQL(Position position, int page, int rows)
    {
        String sqlStr = "";
        if ((!"".equals(position.getNames())) && (position.getNames() != null))
        {
            sqlStr = sqlStr + " and au.NAMES like '%" + position.getNames() + "%'";
        }
        String sql = "SELECT * FROM (SELECT a.*, ROWNUM rnum  FROM (SELECT * FROM POSITIONLISTTAB au where au.C_ORG_CODE = '" + position.getOrgCode() + "'" + sqlStr + " ) a " + " WHERE ROWNUM <= "
            + rows + "*" + page + ") WHERE rnum >= " + rows + "*( " + page + " - 1) + 1 ";
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public List<Position> queryPositionPageSizeByCode(Position position)
    {
        String hqlStr = "";
        
        if ((!"".equals(position.getNames())) && (position.getNames() != null))
        {
            hqlStr = hqlStr + "and p.names like '%" + position.getNames() + "%'";
        }
        
        String hql = "from Position p where p.orgCode = '" + position.getOrgCode() + "'" + hqlStr;
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean savePosition(Position position)
    {
        baseDAO.saveOrUpdate(position);
        return true;
    }
    
    @Override
    public boolean deletePosition(Long id)
    {
        String hql = "delete from Position tp where tp.poliId = '" + id + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public Position loadPositionById(Long id)
    {
        long poliId = id.longValue();
        return (Position)baseDAO.loadById(Position.class, Long.valueOf(poliId));
    }
    
    @Override
    public List<Position> queryPositionByCode(String orgCode)
    {
        String hql = "from Position p where p.orgCode = '" + orgCode + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<Position> queryPositionByOrgCode(String orgCode)
    {
        String hql = "from Position p where p.orgCode='" + orgCode + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<Position> queryAllPosition()
    {
        String hql = "from Position p where p.orgCode is not null ";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<Position> queryAllPositionByShow()
    {
        String hql = "from Position p where p.orgCode is not null and p.showHide = 1 order by p.orgCode asc ";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<Position> queryPositionByPosCode(String posName)
    {
        String hql = "from Position p where p.orgCode is not null and p.names like '%" + posName + "%' order by p.orgCode asc ";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<Position> queryAllPositionByA()
    {
        String hql = "from Position p where p.orgCode is not null  ";
        return baseDAO.query(hql);
    }
}