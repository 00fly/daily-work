package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.RoleGroup;

public interface RoleGroupService
{
    public List<RoleGroup> queryAllRoleGroups();
    
    public List queryRoleGroupsByPage(int paramInt1, int paramInt2);
    
    public boolean deleteRoleGroup(Long paramLong);
    
    public RoleGroup loadRoleGroupById(Long paramLong);
    
    public boolean saveRoleGroup(RoleGroup paramRoleGroup);
}