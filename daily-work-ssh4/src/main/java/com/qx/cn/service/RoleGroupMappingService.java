package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.RoleGroupMapping;

public interface RoleGroupMappingService
{
    public List<RoleGroupMapping> queryGroupRolesById(Long paramLong);
    
    public boolean deleteGroupRoles(Long paramLong);
    
    public boolean saveRoleGroup(RoleGroupMapping paramRoleGroupMapping);
    
    public boolean deleteRoleAtMapping(String roleId);
}