package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.RoleGroupMapping;
import com.qx.cn.service.RoleGroupMappingService;

@Service
public class RoleGroupMappingServiceImpl implements RoleGroupMappingService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List<RoleGroupMapping> queryGroupRolesById(Long groupId)
    {
        String hql = "from RoleGroupMapping rgm where rgm.groupId = " + groupId;
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteGroupRoles(Long groupId)
    {
        String hql = "delete from RoleGroupMapping rgm where rgm.groupId = " + groupId;
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public boolean saveRoleGroup(RoleGroupMapping roleGroupMapping)
    {
        baseDAO.saveOrUpdate(roleGroupMapping);
        return true;
    }
    
    @Override
    public boolean deleteRoleAtMapping(String roleId)
    {
        String hql = "delete from RoleGroupMapping rgm where rgm.roleId = '" + roleId + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
}