package com.qx.cn.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.WorkReportItem;
import com.qx.cn.service.WorkReportItemService;
import com.qx.cn.tool.DateForm;

@ParentPackage("netxzt")
@Namespace("/")
@InterceptorRefs({@InterceptorRef("content")})
@Results({@Result(name = "outlogin", location = "/admin!loseSession"), @Result(name = "getOutlogin", location = "/admin!otherlogin"),
    @Result(name = "index", location = "/WEB-INF/page/user/index.jsp"), @Result(name = "workReportForm", location = "/WEB-INF/page/workReportInfo/workReportList.jsp")})
@Action(value = "workReportItem", interceptorRefs = {@InterceptorRef("loginedCheck")})
public class WorkReportItemAction extends BaseAction
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 2657974568829023382L;
    
    @Autowired
    private WorkReportItemService workReportItemService;
    
    private String wrCode;
    
    private String content;
    
    private Integer id;
    
    private String workTime;
    
    private String hopeFinishDate; // 要求完成时间
    
    private String actualFinishDate; // 实际完成时间
    
    private String remarks; // 备注
    
    private Integer type; // 类型
    
    private static Pattern pattern = Pattern.compile("([1-9]+[0-9]*|0)(\\.[\\d]+)?");
    
    public String queryWorkItemList()
    {
        JSONArray arrJson = new JSONArray();
        List<WorkReportItem> wItems = workReportItemService.queryWorkReportItems(wrCode);
        int i = 0;
        for (WorkReportItem wpItem : wItems)
        {
            JSONObject json = new JSONObject();
            i++;
            json.put("id", wpItem.getId());
            json.put("xulei", Integer.valueOf(i));
            json.put("content", wpItem.getContent());
            json.put("workTime", wpItem.getWorkTime());
            json.put("hopeFinishDate", wpItem.getHopeFinishDate());
            json.put("actualFinishDate", wpItem.getActualFinishDate());
            json.put("remarks", wpItem.getRemarks());
            json.put("wrCode", wpItem.getReportCode());
            json.put("type", wpItem.getType());
            arrJson.add(json);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(JSON.toJSON(arrJson));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveWorkItem()
    {
        String msg = "";
        boolean flag = false;
        
        if (StringUtils.isEmpty(hopeFinishDate)) // 日报
        {
            if (StringUtils.isEmpty(workTime))
            {
                msg = "<span style='font-size:15px;'>时间不能为空!</span>";
                flag = false;
            }
            else
            {
                boolean checkFlag = isDecimal(workTime);
                if (checkFlag)
                {
                    WorkReportItem mpd = new WorkReportItem();
                    mpd.setContent(content);
                    mpd.setReportCode(wrCode);
                    mpd.setCreateTime(DateForm.SimpleDateTime(new Date()));
                    mpd.setWorkTime(workTime);
                    mpd.setRemarks(remarks);
                    flag = workReportItemService.saveWorkReportItem(mpd);
                }
                else
                {
                    msg = "<span style='font-size:15px;'>工作时间格式不正确!</span>";
                    flag = false;
                }
            }
        }
        else
        { // 周报
            WorkReportItem mpd = new WorkReportItem();
            mpd.setContent(content);
            mpd.setReportCode(wrCode);
            mpd.setCreateTime(DateForm.SimpleDateTime(new Date()));
            mpd.setType(type == null ? 1 : type);
            // 1/2/3代表:本周工作/下周计划/本周小结
            mpd.setHopeFinishDate(hopeFinishDate);
            if (1 == type)
            {
                mpd.setActualFinishDate(actualFinishDate);
            }
            mpd.setRemarks(remarks);
            flag = workReportItemService.saveWorkReportItem(mpd);
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("msg", "设置工作项成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("msg", msg);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public static boolean isDecimal(String str)
    {
        return pattern.matcher(str).matches();
    }
    
    public String updateWorkItem()
    {
        String msg = "";
        boolean flag = false;
        if (StringUtils.isEmpty(hopeFinishDate)) // 日报
        {
            if (("".equals(workTime)) || (workTime == null))
            {
                msg = "时间不能为空!";
                flag = false;
            }
            else
            {
                boolean checkFlag = isDecimal(workTime);
                if (checkFlag)
                {
                    WorkReportItem mWorkItem = workReportItemService.loadWorkItem(id);
                    mWorkItem.setContent(content);
                    mWorkItem.setWorkTime(workTime);
                    mWorkItem.setRemarks(remarks);
                    flag = workReportItemService.saveWorkReportItem(mWorkItem);
                }
                else
                {
                    msg = "工作时间格式不正确!";
                    flag = false;
                }
            }
        }
        else
        { // 周报
            WorkReportItem mWorkItem = workReportItemService.loadWorkItem(id);
            mWorkItem.setContent(content);
            mWorkItem.setHopeFinishDate(hopeFinishDate);
            mWorkItem.setActualFinishDate(actualFinishDate);
            mWorkItem.setRemarks(remarks);
            mWorkItem.setType(type == null ? 1 : type);
            flag = workReportItemService.saveWorkReportItem(mWorkItem);
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("msg", "设置工作项成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("msg", msg);
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String deleteWorkItemById()
    {
        boolean flag = false;
        flag = workReportItemService.deleteWorkItemById(id);
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String getWrCode()
    {
        return wrCode;
    }
    
    public void setWrCode(String wrCode)
    {
        this.wrCode = wrCode;
    }
    
    public String getContent()
    {
        return content;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public Integer getId()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getWorkTime()
    {
        return workTime;
    }
    
    public void setWorkTime(String workTime)
    {
        this.workTime = workTime;
    }
    
    public String getHopeFinishDate()
    {
        return hopeFinishDate;
    }
    
    public void setHopeFinishDate(String hopeFinishDate)
    {
        this.hopeFinishDate = hopeFinishDate;
    }
    
    public String getActualFinishDate()
    {
        return actualFinishDate;
    }
    
    public void setActualFinishDate(String actualFinishDate)
    {
        this.actualFinishDate = actualFinishDate;
    }
    
    public String getRemarks()
    {
        return remarks;
    }
    
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }
    
    public Integer getType()
    {
        return type;
    }
    
    public void setType(Integer type)
    {
        this.type = type;
    }
    
}