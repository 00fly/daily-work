package com.qx.cn.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.SendDate;
import com.qx.cn.model.SendTime;
import com.qx.cn.model.User;
import com.qx.cn.service.SendDateService;
import com.qx.cn.service.SendTimeService;
import com.qx.cn.service.WorkReportService;
import com.qx.cn.tool.DateForm;

@ParentPackage("netxzt")
@Namespace("/")
@InterceptorRefs({@InterceptorRef("content")})
@Results({@Result(name = "outlogin", location = "/admin!loseSession"), @Result(name = "getOutlogin", location = "/admin!otherlogin"),
    @Result(name = "index", location = "/WEB-INF/page/user/index.jsp"), @Result(name = "sendTimeForm", location = "/WEB-INF/page/sendTimeInfo/sendTimeList.jsp")})
@Action(value = "sendTime", interceptorRefs = {@InterceptorRef("loginedCheck")})
public class SendTimeAction extends BaseAction
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -7007902012155959144L;
    
    private String startTime;
    
    private String endTime;
    
    @Autowired
    private SendTimeService sendTimeService;
    
    @Autowired
    private SendDateService sendDateService;
    
    @Autowired
    private WorkReportService workReportService;
    
    public String sendTimeForm()
    {
        return "sendTimeForm";
    }
    
    public String querySendTimeToShow()
    {
        JSONObject object = new JSONObject();
        List<SendTime> sendTimes = sendTimeService.queryAllTimeToShow();
        if (sendTimes.size() > 0)
        {
            object.put("startTimeShow", ((SendTime)sendTimes.get(0)).getStartTime());
            object.put("endTimeShow", ((SendTime)sendTimes.get(0)).getEndTime());
        }
        else
        {
            object.put("startTimeShow", "00:00");
            object.put("endTimeShow", "00:00");
        }
        
        object.put("result", 1);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveSendTime()
    {
        boolean flag = false;
        
        sendTimeService.deleteSendTime();
        
        SendTime sendTime = new SendTime();
        sendTime.setEndTime(endTime);
        sendTime.setStartTime(startTime);
        flag = sendTimeService.saveSendTime(sendTime);
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置发送时间成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置发送时间失败!");
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryInfoToIndex()
        throws ParseException
    {
        User user = (User)ServletActionContext.getRequest().getSession().getAttribute("user");
        List<SendTime> sendTimes = sendTimeService.queryAllTimeToShow();
        JSONObject object = new JSONObject();
        String rq = DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd");
        String dgr = rq.substring(8, 10);
        String xq = DateForm.dateToXingQi(rq);
        if (sendTimes.size() > 0)
        {
            object.put("sdt", sendTimes.get(0).getStartTime());
            object.put("edt", sendTimes.get(0).getEndTime());
        }
        else
        {
            object.put("sdt", "00:00");
            object.put("edt", "00:00");
        }
        
        StringBuilder msg = new StringBuilder("今天<span style='color:red;'>不用</span>上交日报、周报");
        SendDate sendDate = sendDateService.queryAllSendDateByDate(DateForm.SimpleDate(new Date()), "week");
        if (sendDate != null)
        {
            msg = new StringBuilder("<span style='color:red;'>今天需要上交日报、周报</span>");
            boolean dayFlag = workReportService.queryCheckIsWorkReports(DateForm.SimpleDate(new Date()), user.getUserId(), "day");
            if (dayFlag)
            {
                msg.append("<br/><span style='color:red;'>您还没有上交日报</span>");
            }
            else
            {
                msg.append("<br/><span style='color:blue;'>您已经上交日报</span>");
            }
            
            boolean weekFlag = workReportService.queryCheckIsWorkReports(DateForm.SimpleDate(new Date()), user.getUserId(), "week");
            
            if (weekFlag)
            {
                msg.append("<br/><span style='color:red;'>您还没有上交周报</span>");
            }
            else
            {
                msg.append("<br/><span style='color:blue;'>您已经上交周报</span>");
            }
        }
        else
        {
            sendDate = sendDateService.queryAllSendDateByDate(DateForm.SimpleDate(new Date()), "day");
            if (sendDate != null)
            {
                msg = new StringBuilder("<span style='color:red;'>今天需要上交日报</span>");
                boolean dayFlag = workReportService.queryCheckIsWorkReports(DateForm.SimpleDate(new Date()), user.getUserId(), "day");
                if (dayFlag)
                {
                    msg.append("<br/><span style='color:red;'>您还没有上交日报</span>");
                }
                else
                {
                    msg.append("<br/><span style='color:blue;'>您已经上交日报</span>");
                }
            }
        }
        object.put("isNextPost", msg);
        object.put("rq", rq);
        object.put("xq", xq);
        object.put("result", 1);
        object.put("dgr", dgr);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String getStartTime()
    {
        return startTime;
    }
    
    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }
    
    public String getEndTime()
    {
        return endTime;
    }
    
    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }
}