package com.qx.cn.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.SendEmail;
import com.qx.cn.service.SendEmailService;

@ParentPackage("netxzt")
@Namespace("/")
@InterceptorRefs({@InterceptorRef("content")})
@Results({@Result(name = "outlogin", location = "/admin!loseSession"), @Result(name = "getOutlogin", location = "/admin!otherlogin"),
    @Result(name = "index", location = "/WEB-INF/page/user/index.jsp"), @Result(name = "sendEmailForm", location = "/WEB-INF/page/sendEmailInfo/sendEmailList.jsp")})
@Action(value = "sendEmail", interceptorRefs = {@InterceptorRef("loginedCheck")})
public class SendEmailAction extends BaseAction
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -4139458065097175010L;
    
    private int page;
    
    private int rows;
    
    private Integer id;
    
    private String name;
    
    private String email;
    
    private String password;
    
    private String memo;
    
    @Autowired
    private SendEmailService sendEmailService;
    
    public String sendEmailForm()
    {
        return "sendEmailForm";
    }
    
    public String querySendEmailByPage()
    {
        List results = sendEmailService.querySendEmailByPage(page, rows);
        JSONArray arrJson = new JSONArray();
        if (!results.isEmpty())
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("id", (Integer)row[0]);
                json.put("name", (String)row[1]);
                json.put("email", (String)row[2]);
                json.put("password", (String)row[3]);
                json.put("memo", (String)row[4]);
                json.put("smtp", (String)row[5]);
                arrJson.add(json);
            }
        }
        
        JSONObject object = new JSONObject();
        object.put("total", Integer.valueOf(results.size()));
        object.put("rows", arrJson);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String checkIsSendEmail()
    {
        JSONObject object = new JSONObject();
        List sendEmails = sendEmailService.queryAllSendEmails();
        if (sendEmails.size() > 0)
        {
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveSendEmail()
        throws IOException
    {
        boolean flag = false;
        if (id != null)
        {
            SendEmail upSendEmail = sendEmailService.loadSendEmailById(id.intValue());
            upSendEmail.setName(name);
            upSendEmail.setEmail(email);
            upSendEmail.setPassword(password);
            
            String emStr = email.substring(email.lastIndexOf("@") + 1, email.length());
            upSendEmail.setStmp("smtp." + emStr);
            upSendEmail.setMeno(memo);
            flag = sendEmailService.saveSendEmail(upSendEmail);
        }
        else
        {
            SendEmail sendEmail = new SendEmail();
            sendEmail.setName(name);
            sendEmail.setEmail(email);
            String emStr = email.substring(email.lastIndexOf("@"), email.length());
            sendEmail.setStmp("smtp." + emStr);
            sendEmail.setPassword(password);
            sendEmail.setMeno(memo);
            flag = sendEmailService.saveSendEmail(sendEmail);
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置发送邮箱成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置发送邮箱失败!");
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String deleteSendEmail()
    {
        boolean flag = sendEmailService.deleteSendEmail(id.intValue());
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除发送邮箱成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除发送邮箱失败");
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public int getPage()
    {
        return page;
    }
    
    public void setPage(int page)
    {
        this.page = page;
    }
    
    public int getRows()
    {
        return rows;
    }
    
    public void setRows(int rows)
    {
        this.rows = rows;
    }
    
    public Integer getId()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getEmail()
    {
        return email;
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public void setPassword(String password)
    {
        this.password = password;
    }
    
    public String getMemo()
    {
        return memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
}