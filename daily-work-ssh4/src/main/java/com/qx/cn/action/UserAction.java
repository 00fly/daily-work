package com.qx.cn.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.OrgInfo;
import com.qx.cn.model.Position;
import com.qx.cn.model.Role;
import com.qx.cn.model.User;
import com.qx.cn.model.UserRole;
import com.qx.cn.model.UserRoleGroup;
import com.qx.cn.model.WorkSendGroup;
import com.qx.cn.service.OrgInfoService;
import com.qx.cn.service.PositionService;
import com.qx.cn.service.RoleService;
import com.qx.cn.service.UserRoleGroupService;
import com.qx.cn.service.UserRoleService;
import com.qx.cn.service.UserService;
import com.qx.cn.service.WorkSendGroupService;
import com.qx.cn.tool.DateForm;
import com.qx.cn.tool.MD5;
import com.qx.cn.vo.UserVO;

@ParentPackage("netxzt")
@Namespace("/")
@InterceptorRefs({@InterceptorRef("content")})
@Results({@Result(name = "outlogin", location = "/admin!loseSession"), @Result(name = "getOutlogin", location = "/admin!otherlogin"),
    @Result(name = "index", location = "/WEB-INF/page/user/index.jsp"), @Result(name = "queryUserFrom", location = "/WEB-INF/page/user/queryUserFrom.jsp")})
@Action(value = "user", interceptorRefs = {@InterceptorRef("loginedCheck")})
public class UserAction extends BaseAction
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 4121910410622827072L;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private RoleService roleService;
    
    @Autowired
    private OrgInfoService orgInfoService;
    
    @Autowired
    private UserRoleService userRoleService;
    
    @Autowired
    private PositionService positionService;
    
    @Autowired
    private WorkSendGroupService workSendGroupService;
    
    @Autowired
    private UserRoleGroupService userRoleGroupService;
    
    private String coreUserBuffer;
    
    private Long userId;
    
    private String orgCode;
    
    private String userLoginName;
    
    private String meetersBuffer;
    
    private String userName;
    
    private String telephone;
    
    private String posCode;
    
    private String parCode;
    
    private String workNumber;
    
    private String positionName;
    
    private String spellLong;
    
    private String spellShort;
    
    private Integer enabled;
    
    private String userMemo;
    
    private Long groupId;
    
    private String roleId;
    
    private int page;
    
    private int rows;
    
    private String strBuffer;
    
    private String pinyin;
    
    private Long id;
    
    private Long meetId;
    
    private Long projectId;
    
    private String receiversBuffer;
    
    private String departCode;
    
    private Integer jid;
    
    private Integer jsz;
    
    private String oldPwd;
    
    private String newPwd;
    
    public String queryUserFrom()
    {
        return "queryUserFrom";
    }
    
    public String queryUserByCodeAnsPage()
    {
        User uss = new User();
        uss.setUserLoginName(userLoginName);
        uss.setUserName(userName);
        uss.setWorkNumber(workNumber);
        uss.setOrgCode(orgCode);
        uss.setSpellLong(pinyin);
        List results = userService.queryUserByPageAndCodeSQL(uss, page, rows);
        JSONArray arrJson = new JSONArray();
        if (results.size() > 0)
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("userId", row[0]);
                json.put("userName", row[3]);
                json.put("userLoginName", row[1]);
                json.put("positionId", row[8]);
                json.put("sort", row[12]);
                if ((row[8] != null) && (!"".equals(row[8])))
                {
                    Position position = positionService.loadPositionById(Long.valueOf((String)row[8]));
                    if (position != null)
                    {
                        json.put("positionName", position.getNames());
                    }
                    else
                    {
                        json.put("positionName", "");
                    }
                }
                else
                {
                    json.put("positionName", "");
                }
                
                json.put("workNumber", row[7]);
                json.put("enabled", row[13]);
                
                String gonghaoStr = "";
                if (((String)row[7] == null) || ("".equals(row[7])))
                {
                    gonghaoStr = "暂缺";
                }
                else
                {
                    gonghaoStr = (String)row[7];
                }
                
                String telStr = "";
                if (((String)row[6] == null) || ("".equals(row[6])))
                {
                    telStr = "暂缺";
                }
                else
                {
                    telStr = (String)row[6];
                }
                
                String posStr = "";
                if (((String)row[8] == null) || ("".equals(row[8])))
                {
                    posStr = "暂缺";
                }
                else
                {
                    posStr = (String)row[8];
                }
                
                String orgCodeStr = (String)row[5];
                StringBuilder orgBuffer = new StringBuilder();
                if ((orgCodeStr != null) && (!"".equals(orgCodeStr)))
                {
                    String[] orgstr = orgCodeStr.split(",");
                    for (int j = 0; j < orgstr.length; j++)
                    {
                        OrgInfo orgInfo = orgInfoService.loadOrgByCode(orgstr[j]);
                        if (orgInfo != null)
                        {
                            if (i == orgstr.length - 2)
                            {
                                orgBuffer.append(orgInfo.getOrgName()).append(".");
                            }
                            else
                            {
                                orgBuffer.append(orgInfo.getOrgName()).append(",");
                            }
                        }
                        
                    }
                    
                }
                
                List<UserRole> userRoles = userRoleService.queryRolesByUserId(Long.valueOf(((Integer)row[0]).longValue()));
                StringBuilder roleBuffer = new StringBuilder();
                for (UserRole userRole : userRoles)
                {
                    Role role = roleService.loadRoleById(Long.valueOf(userRole.getRoleId().longValue()));
                    if (role != null)
                    {
                        roleBuffer.append(role.getRoleName()).append(",");
                    }
                    
                }
                
                String detailInfo =
                    "<ul><li>姓名：" + (String)row[3] + "</li><li>账号：" + (String)row[1] + "</li><li>工号：" + gonghaoStr + "</li>" + "<li>电话：" + telStr + "</li>" + "<li>职位：" + posStr + "</li>" + "<li>创建时间："
                        + (String)row[9] + "&nbsp;&nbsp;</li>" + "<li>所属机构：" + orgBuffer.toString() + "&nbsp;&nbsp;</li>" + "<li>所属角色：" + roleBuffer.toString() + "&nbsp;&nbsp;</li>" + "</ul>";
                json.put("detailInfo", detailInfo);
                String optRoleUrl = "<span style='color:#00C;cursor:pointer;' onclick=giveRoleToUser('" + row[0] + "','【" + (String)row[3] + "】')>[分配角色]</span>&nbsp;";
                json.put("optUrl", optRoleUrl);
                arrJson.add(json);
            }
        }
        
        JSONObject object = new JSONObject();
        List users = userService.queryUserPageSizeByCode(uss);
        if (users != null)
        {
            object.put("total", Integer.valueOf(users.size()));
        }
        object.put("rows", arrJson);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveUserByOrgCode()
    {
        boolean flag = false;
        
        if (userId == null)
        {
            User user = new User();
            user.setOrgCode(orgCode);
            user.setUserLoginName(userLoginName);
            user.setUserLoginPwd(MD5.toMD5("123456"));
            user.setCreateTime(DateForm.SimpleDateTime(new Date()));
            user.setTelePhone(telephone);
            user.setEnabled(enabled);
            user.setPositionName(positionName);
            user.setSpellLong(spellLong);
            user.setSpellShort(spellShort);
            user.setWorkNumber(workNumber.trim());
            user.setUserName(userName);
            user.setMemo(userMemo);
            flag = userService.saveUserByCode(user);
        }
        else
        {
            User updUser = userService.loadUserById(userId);
            updUser.setOrgCode(orgCode);
            updUser.setTelePhone(telephone);
            updUser.setEnabled(enabled);
            updUser.setPositionName(positionName);
            updUser.setSpellLong(spellLong);
            updUser.setSpellShort(spellShort);
            updUser.setWorkNumber(workNumber.trim());
            updUser.setUserName(userName);
            updUser.setMemo(userMemo);
            flag = userService.saveUserByCode(updUser);
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("msg", "添加成功,账号:" + userLoginName + "&nbsp;密码:123456");
        }
        else
        {
            object.put("result", 2);
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveOrgsToUser()
    {
        boolean flag = true;
        JSONObject object = new JSONObject();
        User user = userService.loadUserById(userId);
        user.setOrgCodeAttach(strBuffer);
        flag = userService.saveUserByCode(user);
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "分配组织机构成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "分配组织机构失败!");
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String deleteUser()
    {
        boolean flag1 = userRoleGroupService.deleteGroupsUser(userId);
        boolean flag2 = userRoleService.deleteRolesUser(userId);
        boolean flag = userService.deleteUserByUserId(userId);
        
        JSONObject object = new JSONObject();
        if ((flag) && (flag1) && (flag2))
        {
            object.put("result", 1);
            object.put("errorMsg", "删除用户成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除用户失败!");
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String loadUserByUserId()
    {
        User user = userService.loadUserById(userId);
        JSONObject object = new JSONObject();
        object.put("userId", user.getUserId());
        object.put("userLoginName", user.getUserLoginName());
        object.put("userName", user.getUserName());
        object.put("telephone", user.getTelePhone());
        object.put("workNumber", user.getWorkNumber());
        object.put("positionName", user.getPositionName());
        object.put("spellLong", user.getSpellLong());
        object.put("spellShort", user.getSpellShort());
        object.put("enabled", user.getEnabled());
        object.put("userMemo", user.getMemo());
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryAllUserByOrgCodeToGroup()
    {
        List<User> users = userService.queryUserByCode(orgCode);
        List<UserRoleGroup> userGroups = userRoleGroupService.queryUsersToGroupByOrgCode(orgCode, groupId);
        List<UserVO> userVOs = new ArrayList<>();
        for (User user : users)
        {
            int sign = 0;
            UserVO userVO = new UserVO();
            for (UserRoleGroup urg : userGroups)
            {
                if (user.getUserId().equals(urg.getUserId()))
                {
                    sign = 1;
                }
            }
            
            userVO.setUserId(user.getUserId());
            userVO.setUserName(user.getUserName());
            userVO.setSign(Integer.valueOf(sign));
            userVOs.add(userVO);
        }
        JSONObject object = new JSONObject();
        object.put("datas", userVOs);
        object.put("result", 1);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(JSON.toJSONString(object));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryAllUserByOrgCodeToRole()
    {
        List<User> users = userService.queryUserByCode(orgCode);
        List<UserRole> userRoles = userRoleService.queryUsersToRoleByOrgCode(orgCode, roleId);
        List<UserVO> userVOs = new ArrayList<>();
        for (User user : users)
        {
            int sign = 0;
            UserVO userVO = new UserVO();
            for (UserRole ur : userRoles)
            {
                if (user.getUserId().equals(Long.valueOf(ur.getUserId().longValue())))
                {
                    sign = 1;
                }
            }
            
            userVO.setUserId(user.getUserId());
            userVO.setUserName(user.getUserName());
            if (user.getPositionName() != null)
            {
                Position position = positionService.loadPositionById(Long.valueOf(user.getPositionName()));
                if (position != null)
                {
                    userVO.setPositionName(position.getNames());
                }
                else
                {
                    userVO.setPositionName("");
                }
            }
            else
            {
                userVO.setPositionName("");
            }
            
            userVO.setSign(Integer.valueOf(sign));
            userVOs.add(userVO);
        }
        
        JSONObject object = new JSONObject();
        object.put("datas", userVOs);
        object.put("result", 1);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(JSON.toJSONString(object));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryUserByPosCode()
    {
        List<User> users = userService.queryUsersByPosCode(posCode);
        JSONArray arrJson = new JSONArray();
        for (User user : users)
        {
            JSONObject json = new JSONObject();
            json.put("id", user.getUserId());
            json.put("text", user.getUserName());
            arrJson.add(json);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(arrJson.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryUserByLikeCode()
    {
        List<User> users = userService.queryUserByLikeCode(orgCode);
        JSONArray arrJson = new JSONArray();
        for (User user : users)
        {
            JSONObject json = new JSONObject();
            json.put("id", user.getUserId());
            json.put("text", user.getUserName());
            arrJson.add(json);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(arrJson.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryLeaderByCode()
    {
        List<User> users = userService.queryUserByCode(orgCode);
        JSONArray arrJson = new JSONArray();
        for (User user : users)
        {
            JSONObject json = new JSONObject();
            json.put("id", user.getUserId());
            json.put("text", user.getUserName());
            arrJson.add(json);
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(JSON.toJSON(arrJson));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryOrgsAndCodeUserTreeGrid()
    {
        String sta = "open";
        List<OrgInfo> orgInfos = orgInfoService.queryOrgListToUserByAsc(parCode);
        JSONArray jsonArray = new JSONArray();
        for (OrgInfo org : orgInfos)
        {
            List sonOrgs = orgInfoService.queryOrgListByAsc(org.getOrgCode());
            JSONObject obj = new JSONObject();
            if (sonOrgs.size() > 1)
            {
                sta = "closed";
            }
            else
            {
                sta = "open";
            }
            
            obj.put("id", org.getOrgCode());
            obj.put("orgName", org.getOrgName());
            obj.put("state", sta);
            
            List<User> users = userService.queryUserByCode(org.getOrgCode());
            WorkSendGroup workSendGroup = workSendGroupService.loadWorkSendGroupById(jid.intValue());
            String selectUserOpt = "";
            for (User user : users)
            {
                boolean flag = false;
                if (workSendGroup.getSenderIds() != null)
                {
                    String[] uIds = workSendGroup.getSenderIds().split(",");
                    for (int j = 0; j < uIds.length; j++)
                    {
                        if (user.getUserId().equals(Long.valueOf(uIds[j])))
                        {
                            flag = true;
                        }
                    }
                }
                
                if (flag)
                {
                    selectUserOpt = selectUserOpt + "<span style='width:70px;display:block;float:left;'><input type='checkbox' checked='checked' class='but" + user.getOrgCode() + " coreUseres but"
                        + parCode + "' value='" + user.getUserId() + "'/>" + user.getUserName() + "</span>";
                }
                else
                {
                    selectUserOpt = selectUserOpt + "<span style='width:70px;display:block;float:left;'><input type='checkbox' class='but" + user.getOrgCode() + " coreUseres but" + parCode
                        + "' value='" + user.getUserId() + "'/>" + user.getUserName() + "</span>";
                }
            }
            obj.put("selectCodeUserOpt", selectUserOpt);
            
            jsonArray.add(obj);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(jsonArray.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String queryOrgsAndCodeYuanGongTreeGrid()
    {
        String sta = "open";
        List<OrgInfo> orgInfos = orgInfoService.queryOrgListToUserByAsc(parCode);
        JSONArray jsonArray = new JSONArray();
        for (OrgInfo org : orgInfos)
        {
            List sonOrgs = orgInfoService.queryOrgListByAsc(org.getOrgCode());
            JSONObject obj = new JSONObject();
            if (sonOrgs.size() > 1)
            {
                sta = "closed";
            }
            else
            {
                sta = "open";
            }
            
            obj.put("id", org.getOrgCode());
            obj.put("orgName", org.getOrgName());
            obj.put("state", sta);
            
            List<User> users = userService.queryUserByCode(org.getOrgCode());
            WorkSendGroup workSendGroup = workSendGroupService.loadWorkSendGroupById(jid.intValue());
            String selectUserOpt = "";
            for (User user : users)
            {
                boolean flag = false;
                if (workSendGroup.getWorkerIds() != null)
                {
                    String[] uIds = workSendGroup.getWorkerIds().split(",");
                    for (int j = 0; j < uIds.length; j++)
                    {
                        if (user.getUserId().equals(Long.valueOf(uIds[j])))
                        {
                            flag = true;
                        }
                    }
                }
                
                if (flag)
                {
                    selectUserOpt = selectUserOpt + "<span style='width:70px;display:block;float:left;'><input type='checkbox' checked='checked' class='but" + user.getOrgCode() + " yuanGongCla but"
                        + parCode + "' value='" + user.getUserId() + "'/>" + user.getUserName() + "</span>";
                }
                else
                {
                    selectUserOpt = selectUserOpt + "<span style='width:70px;display:block;float:left;'><input type='checkbox' class='but" + user.getOrgCode() + " yuanGongCla but" + parCode
                        + "' value='" + user.getUserId() + "'/>" + user.getUserName() + "</span>";
                }
            }
            obj.put("selectYuanGongOpt", selectUserOpt);
            jsonArray.add(obj);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(jsonArray.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveJieShouZhe()
    {
        boolean flag = false;
        WorkSendGroup workSendGroup = workSendGroupService.loadWorkSendGroupById(jsz.intValue());
        if (workSendGroup != null)
        {
            if ((coreUserBuffer != null) && (!"".equals(coreUserBuffer)))
            {
                workSendGroup.setSenderIds(coreUserBuffer.toString());
            }
            else
            {
                workSendGroup.setEnabled(Integer.valueOf(0));
                workSendGroup.setSenderIds(null);
            }
            flag = workSendGroupService.saveWorkSendGroup(workSendGroup);
        }
        JSONObject object = new JSONObject();
        
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置接收者成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置接收者失败!");
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveYuanGong()
    {
        boolean flag = false;
        WorkSendGroup workSendGroup = workSendGroupService.loadWorkSendGroupById(jsz.intValue());
        if (workSendGroup != null)
        {
            if ((coreUserBuffer != null) && (!"".equals(coreUserBuffer)))
            {
                workSendGroup.setWorkerIds(coreUserBuffer.toString());
            }
            else
            {
                workSendGroup.setEnabled(Integer.valueOf(0));
                workSendGroup.setWorkerIds(null);
            }
            flag = workSendGroupService.saveWorkSendGroup(workSendGroup);
        }
        JSONObject object = new JSONObject();
        
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置员工成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置员工失败!");
        }
        
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String changePwd()
    {
        JSONObject object = new JSONObject();
        User user = (User)ServletActionContext.getRequest().getSession().getAttribute("user");
        if (MD5.toMD5(oldPwd).equals(user.getUserLoginPwd()))
        {
            user.setUserLoginPwd(MD5.toMD5(newPwd));
            userService.saveUserByCode(user);
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String loadUserToChange()
    {
        JSONObject object = new JSONObject();
        User user = (User)ServletActionContext.getRequest().getSession().getAttribute("user");
        object.put("name", user.getUserName());
        object.put("userAccount", user.getUserLoginName());
        object.put("phone", user.getTelePhone());
        
        if (user.getPositionName() != null)
        {
            Position position = positionService.loadPositionById(Long.valueOf(user.getPositionName()));
            if (position != null)
            {
                object.put("position", position.getNames());
            }
            else
            {
                object.put("position", "");
            }
        }
        else
        {
            object.put("position", "");
        }
        
        object.put("result", 1);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String changePerson()
    {
        JSONObject object = new JSONObject();
        User user = (User)ServletActionContext.getRequest().getSession().getAttribute("user");
        user.setTelePhone(telephone);
        boolean flag = userService.saveUserByCode(user);
        if (flag)
        {
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * 保存session
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    public String keepSession()
    {
        return "1";
    }
    
    public String getOrgCode()
    {
        return orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public String getUserLoginName()
    {
        return userLoginName;
    }
    
    public void setUserLoginName(String userLoginName)
    {
        this.userLoginName = userLoginName;
    }
    
    public String getUserName()
    {
        return userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public String getTelephone()
    {
        return telephone;
    }
    
    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }
    
    public String getWorkNumber()
    {
        return workNumber;
    }
    
    public void setWorkNumber(String workNumber)
    {
        this.workNumber = workNumber;
    }
    
    public String getPositionName()
    {
        return positionName;
    }
    
    public void setPositionName(String positionName)
    {
        this.positionName = positionName;
    }
    
    public String getSpellLong()
    {
        return spellLong;
    }
    
    public void setSpellLong(String spellLong)
    {
        this.spellLong = spellLong;
    }
    
    public String getSpellShort()
    {
        return spellShort;
    }
    
    public void setSpellShort(String spellShort)
    {
        this.spellShort = spellShort;
    }
    
    public Integer getEnabled()
    {
        return enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getUserMemo()
    {
        return userMemo;
    }
    
    public void setUserMemo(String userMemo)
    {
        this.userMemo = userMemo;
    }
    
    public int getPage()
    {
        return page;
    }
    
    public void setPage(int page)
    {
        this.page = page;
    }
    
    public int getRows()
    {
        return rows;
    }
    
    public void setRows(int rows)
    {
        this.rows = rows;
    }
    
    public String getRoleId()
    {
        return roleId;
    }
    
    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }
    
    public String getStrBuffer()
    {
        return strBuffer;
    }
    
    public void setStrBuffer(String strBuffer)
    {
        this.strBuffer = strBuffer;
    }
    
    public String getPinyin()
    {
        return pinyin;
    }
    
    public void setPinyin(String pinyin)
    {
        this.pinyin = pinyin;
    }
    
    public String index()
    {
        return "index";
    }
    
    public String getPosCode()
    {
        return posCode;
    }
    
    public void setPosCode(String posCode)
    {
        this.posCode = posCode;
    }
    
    public Long getUserId()
    {
        return userId;
    }
    
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }
    
    public String getParCode()
    {
        return parCode;
    }
    
    public void setParCode(String parCode)
    {
        this.parCode = parCode;
    }
    
    public String getCoreUserBuffer()
    {
        return coreUserBuffer;
    }
    
    public void setCoreUserBuffer(String coreUserBuffer)
    {
        this.coreUserBuffer = coreUserBuffer;
    }
    
    public Long getId()
    {
        return id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public Long getProjectId()
    {
        return projectId;
    }
    
    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }
    
    public String getReceiversBuffer()
    {
        return receiversBuffer;
    }
    
    public void setReceiversBuffer(String receiversBuffer)
    {
        this.receiversBuffer = receiversBuffer;
    }
    
    public String getDepartCode()
    {
        return departCode;
    }
    
    public void setDepartCode(String departCode)
    {
        this.departCode = departCode;
    }
    
    public String getMeetersBuffer()
    {
        return meetersBuffer;
    }
    
    public void setMeetersBuffer(String meetersBuffer)
    {
        this.meetersBuffer = meetersBuffer;
    }
    
    public Long getMeetId()
    {
        return meetId;
    }
    
    public void setMeetId(Long meetId)
    {
        this.meetId = meetId;
    }
    
    public Long getGroupId()
    {
        return groupId;
    }
    
    public void setGroupId(Long groupId)
    {
        this.groupId = groupId;
    }
    
    public Integer getJid()
    {
        return jid;
    }
    
    public void setJid(Integer jid)
    {
        this.jid = jid;
    }
    
    public Integer getJsz()
    {
        return jsz;
    }
    
    public void setJsz(Integer jsz)
    {
        this.jsz = jsz;
    }
    
    public String getOldPwd()
    {
        return oldPwd;
    }
    
    public void setOldPwd(String oldPwd)
    {
        this.oldPwd = oldPwd;
    }
    
    public String getNewPwd()
    {
        return newPwd;
    }
    
    public void setNewPwd(String newPwd)
    {
        this.newPwd = newPwd;
    }
}