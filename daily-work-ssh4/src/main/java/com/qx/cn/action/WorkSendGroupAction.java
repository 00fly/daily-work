package com.qx.cn.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.User;
import com.qx.cn.model.WorkSendGroup;
import com.qx.cn.service.UserService;
import com.qx.cn.service.WorkSendGroupService;
import com.qx.cn.tool.DateForm;

@ParentPackage("netxzt")
@Namespace("/")
@InterceptorRefs({@InterceptorRef("content")})
@Results({@Result(name = "outlogin", location = "/admin!loseSession"), @Result(name = "getOutlogin", location = "/admin!otherlogin"),
    @Result(name = "index", location = "/WEB-INF/page/user/index.jsp"), @Result(name = "workSendGroupForm", location = "/WEB-INF/page/workSendGroup/workSendGroupList.jsp")})
@Action(value = "workSendGroup", interceptorRefs = {@InterceptorRef("loginedCheck")})
public class WorkSendGroupAction extends BaseAction
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -7257894263533460515L;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private WorkSendGroupService workSendGroupService;
    
    private int page;
    
    private int rows;
    
    private Integer id;
    
    private String groupName;
    
    private String senderIds;
    
    private String workerIds;
    
    private String creatTime;
    
    private Integer enabled;
    
    private String memo;
    
    private Integer status;
    
    public String workSendGroupForm()
    {
        return "workSendGroupForm";
    }
    
    public String queryWorkSendGroupByPage()
    {
        WorkSendGroup workSendGroup = new WorkSendGroup();
        workSendGroup.setGroupName(groupName);
        List results = workSendGroupService.queryWorkSendGroupByPage(workSendGroup, page, rows);
        JSONArray arrJson = new JSONArray();
        if (results.size() > 0)
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("id", row[0]);
                json.put("groupName", row[1]);
                
                if ((String)row[2] != null)
                {
                    String[] senduId = row[2].toString().split(",");
                    StringBuffer sendBuf = new StringBuffer();
                    for (int x = 0; x < senduId.length; x++)
                    {
                        User user = userService.loadUserById(Long.valueOf(senduId[x]));
                        if (user != null)
                        {
                            if (x == senduId.length - 1)
                            {
                                sendBuf.append(user.getUserName());
                            }
                            else
                            {
                                sendBuf.append(user.getUserName()).append(",");
                            }
                        }
                    }
                    
                    json.put("senderIds", sendBuf.toString());
                }
                else
                {
                    json.put("senderIds", "暂无");
                }
                
                if ((String)row[3] != null)
                {
                    String[] senduId = row[3].toString().split(",");
                    StringBuffer sendBuf = new StringBuffer();
                    for (int x = 0; x < senduId.length; x++)
                    {
                        User user = userService.loadUserById(Long.valueOf(senduId[x]));
                        if (user != null)
                        {
                            if (x == senduId.length - 1)
                            {
                                sendBuf.append(user.getUserName());
                            }
                            else
                            {
                                sendBuf.append(user.getUserName()).append(",");
                            }
                        }
                    }
                    
                    json.put("workerIds", sendBuf.toString());
                }
                else
                {
                    json.put("workerIds", "暂无");
                }
                json.put("creatTime", row[4]);
                if (row[5] == null)
                {
                    json.put("enabled",
                        "<a href='javascript:;' style='color:red;font-weight:800' onclick=setEnabled(1," + row[0] + ",'" + (String)row[1] + "','" + (String)row[2] + "','" + (String)row[3]
                            + "')>启用</a>");
                }
                else if (((Integer)row[5]).intValue() == 0)
                {
                    json.put("enabled",
                        "<a href='javascript:;' style='color:red;font-weight:800' onclick=setEnabled(1," + row[0] + ",'" + (String)row[1] + "','" + (String)row[2] + "','" + (String)row[3]
                            + "')>启用</a>");
                }
                else
                {
                    json.put("enabled",
                        "<a href='javascript:;' style='color:blue;font-weight:800' onclick=setEnabled(0," + row[0] + ",'" + (String)row[1] + "','" + (String)row[2] + "','" + (String)row[3]
                            + "')>禁用</a>");
                }
                json.put("memo", row[6]);
                arrJson.add(json);
            }
        }
        
        JSONObject object = new JSONObject();
        object.put("total", Integer.valueOf(workSendGroupService.queryAllWorkSendGroups(workSendGroup).size()));
        object.put("rows", arrJson);
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String saveWorkSendGroup()
        throws IOException
    {
        boolean flag = false;
        if (id != null)
        {
            WorkSendGroup upWorkSendGroup = workSendGroupService.loadWorkSendGroupById(id.intValue());
            upWorkSendGroup.setMemo(memo);
            upWorkSendGroup.setGroupName(groupName);
            
            flag = workSendGroupService.saveWorkSendGroup(upWorkSendGroup);
        }
        else
        {
            WorkSendGroup workSendGroup = new WorkSendGroup();
            workSendGroup.setMemo(memo);
            workSendGroup.setGroupName(groupName);
            
            workSendGroup.setTimeTip(Integer.valueOf(0));
            workSendGroup.setCreatTime(DateForm.SimpleDate(new Date()));
            flag = workSendGroupService.saveWorkSendGroup(workSendGroup);
        }
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置发送组成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置发送组失败!");
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String deleteWorkSendGroup()
    {
        boolean flag = workSendGroupService.deleteWorkSendGroup(id.intValue());
        
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除发送组成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除发送组失败");
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public String enabledWorkSendGroup()
    {
        boolean flag = false;
        String title = "";
        WorkSendGroup workSendGroup = workSendGroupService.loadWorkSendGroupById(id.intValue());
        workSendGroup.setEnabled(status);
        flag = workSendGroupService.saveWorkSendGroup(workSendGroup);
        if (status.intValue() == 1)
        {
            title = "启用";
        }
        else
        {
            title = "禁用";
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", title + "发送组成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", title + "发送组失败");
        }
        try
        {
            PrintWriter out = response.getWriter();
            out.print(object.toString());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    
    public int getPage()
    {
        return page;
    }
    
    public void setPage(int page)
    {
        this.page = page;
    }
    
    public int getRows()
    {
        return rows;
    }
    
    public void setRows(int rows)
    {
        this.rows = rows;
    }
    
    public Integer getId()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getGroupName()
    {
        return groupName;
    }
    
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }
    
    public String getSenderIds()
    {
        return senderIds;
    }
    
    public void setSenderIds(String senderIds)
    {
        this.senderIds = senderIds;
    }
    
    public String getWorkerIds()
    {
        return workerIds;
    }
    
    public void setWorkerIds(String workerIds)
    {
        this.workerIds = workerIds;
    }
    
    public String getCreatTime()
    {
        return creatTime;
    }
    
    public void setCreatTime(String creatTime)
    {
        this.creatTime = creatTime;
    }
    
    public Integer getEnabled()
    {
        return enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMemo()
    {
        return memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
    
    public Integer getStatus()
    {
        return status;
    }
    
    public void setStatus(Integer status)
    {
        this.status = status;
    }
}