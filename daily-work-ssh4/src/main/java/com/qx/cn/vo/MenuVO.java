package com.qx.cn.vo;

public class MenuVO
{
    private Long menuId;
    
    private String menuCode;
    
    private String menuName;
    
    private Integer isLeafMenu;
    
    private String menuURL;
    
    private Integer enabled;
    
    private String menuDesc;
    
    private Integer menuSort;
    
    private String id;
    
    private String state;
    
    private String pmenuName;
    
    private String parCode;
    
    private String optUrl;
    
    private String optShow;
    
    private String roleId;
    
    public String getRoleId()
    {
        return this.roleId;
    }
    
    public void setRoleId(String roleId)
    {
        this.roleId = roleId;
    }
    
    public String getParCode()
    {
        return this.parCode;
    }
    
    public void setParCode(String parCode)
    {
        this.parCode = parCode;
    }
    
    public String getOptUrl()
    {
        return this.optUrl;
    }
    
    public void setOptUrl(String optUrl)
    {
        this.optUrl = optUrl;
    }
    
    public String getPmenuName()
    {
        return this.pmenuName;
    }
    
    public void setPmenuName(String pmenuName)
    {
        this.pmenuName = pmenuName;
    }
    
    public String getMenuCode()
    {
        return this.menuCode;
    }
    
    public void setMenuCode(String menuCode)
    {
        this.menuCode = menuCode;
    }
    
    public String getMenuName()
    {
        return this.menuName;
    }
    
    public void setMenuName(String menuName)
    {
        this.menuName = menuName;
    }
    
    public Integer getIsLeafMenu()
    {
        return this.isLeafMenu;
    }
    
    public void setIsLeafMenu(Integer isLeafMenu)
    {
        this.isLeafMenu = isLeafMenu;
    }
    
    public String getMenuURL()
    {
        return this.menuURL;
    }
    
    public void setMenuURL(String menuURL)
    {
        this.menuURL = menuURL;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMenuDesc()
    {
        return this.menuDesc;
    }
    
    public void setMenuDesc(String menuDesc)
    {
        this.menuDesc = menuDesc;
    }
    
    public String getId()
    {
        return this.id;
    }
    
    public void setId(String id)
    {
        this.id = id;
    }
    
    public String getState()
    {
        return this.state;
    }
    
    public void setState(String state)
    {
        this.state = state;
    }
    
    public Integer getMenuSort()
    {
        return this.menuSort;
    }
    
    public void setMenuSort(Integer menuSort)
    {
        this.menuSort = menuSort;
    }
    
    public String getOptShow()
    {
        return this.optShow;
    }
    
    public void setOptShow(String optShow)
    {
        this.optShow = optShow;
    }
    
    public Long getMenuId()
    {
        return this.menuId;
    }
    
    public void setMenuId(Long menuId)
    {
        this.menuId = menuId;
    }
}