package com.qx.cn.vo;

public class RoleVO
{
    private Long roleId;
    
    private String roleName;
    
    private Integer enabled;
    
    private String roleDesc;
    
    private Integer sign;
    
    public Integer getSign()
    {
        return this.sign;
    }
    
    public void setSign(Integer sign)
    {
        this.sign = sign;
    }
    
    public String getRoleName()
    {
        return this.roleName;
    }
    
    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getRoleDesc()
    {
        return this.roleDesc;
    }
    
    public void setRoleDesc(String roleDesc)
    {
        this.roleDesc = roleDesc;
    }
    
    public Long getRoleId()
    {
        return this.roleId;
    }
    
    public void setRoleId(Long roleId)
    {
        this.roleId = roleId;
    }
}