package com.qx.cn.vo;

public class MarketUserVO
{
    private Long poliId;
    
    private String names;
    
    private String posumber;
    
    private Long did;
    
    private String orgCode;
    
    private String userId;
    
    private String userName;
    
    private int sign;
    
    public Long getPoliId()
    {
        return this.poliId;
    }
    
    public void setPoliId(Long poliId)
    {
        this.poliId = poliId;
    }
    
    public String getNames()
    {
        return this.names;
    }
    
    public void setNames(String names)
    {
        this.names = names;
    }
    
    public String getPosumber()
    {
        return this.posumber;
    }
    
    public void setPosumber(String posumber)
    {
        this.posumber = posumber;
    }
    
    public Long getDid()
    {
        return this.did;
    }
    
    public void setDid(Long did)
    {
        this.did = did;
    }
    
    public String getOrgCode()
    {
        return this.orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public String getUserId()
    {
        return this.userId;
    }
    
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    
    public String getUserName()
    {
        return this.userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public int getSign()
    {
        return this.sign;
    }
    
    public void setSign(int sign)
    {
        this.sign = sign;
    }
}