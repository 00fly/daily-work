package com.qx.cn.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BaseDAOImpl implements BaseDAO
{
    @Autowired
    protected SessionFactory sessionFactory;
    
    @Override
    public long count(Class clazz)
    {
        String hql = "select count(*) from " + clazz.getSimpleName();
        Query query = getSession().createQuery(hql);
        query.setMaxResults(1);
        return Long.parseLong(query.uniqueResult().toString());
    }
    
    @Override
    public long countByHql(String countHql, Object... args)
    {
        Query query = getSession().createQuery(countHql);
        if ((args != null) && (args.length > 0))
        {
            for (int i = 0; i < args.length; i++)
            {
                query.setParameter(i, args[i]);
            }
        }
        query.setMaxResults(1);
        return Long.parseLong(query.uniqueResult().toString());
    }
    
    @Override
    public int deleteByHql(String hql, Object... args)
    {
        return update(hql, args);
    }
    
    @Override
    public void deleteById(Class clazz, Serializable id)
    {
        Object object = loadById(clazz, id);
        getSession().delete(object);
    }
    
    public Session getSession()
    {
        return this.sessionFactory.getCurrentSession();
    }
    
    @Override
    public Object loadByHql(String hql, Object... args)
    {
        List<?> list = query(hql, args);
        if (!list.isEmpty())
        {
            return list.get(0);
        }
        return null;
    }
    
    @Override
    public Object loadById(Class clazz, Serializable id)
    {
        return getSession().get(clazz, id);
    }
    
    @Override
    public List query(Class clazz)
    {
        String hql = "from " + clazz.getSimpleName() + " as a order by a.id desc";
        return query(hql);
    }
    
    @Override
    public List query(String hql, int pageNo, int rows, Object[] args)
    {
        Query query = getSession().createQuery(hql);
        query.setFirstResult((pageNo - 1) * rows);
        query.setMaxResults(rows);
        if ((args != null) && (args.length > 0))
        {
            for (int i = 0; i < args.length; i++)
            {
                query.setParameter(i, args[i]);
            }
        }
        return query.list();
    }
    
    @Override
    public List query(String hql, Object... args)
    {
        Query query = getSession().createQuery(hql);
        if ((args != null) && (args.length > 0))
        {
            for (int i = 0; i < args.length; i++)
            {
                query.setParameter(i, args[i]);
            }
        }
        return query.list();
    }
    
    @Override
    public List queryBySql(String sql, Object... args)
    {
        SQLQuery query = getSession().createSQLQuery(sql);
        if ((args != null) && (args.length > 0))
        {
            for (int i = 0; i < args.length; i++)
            {
                query.setParameter(i, args[i]);
            }
        }
        return query.list();
    }
    
    @Override
    public void saveOrUpdate(Object obj)
    {
        getSession().saveOrUpdate(obj);
    }
    
    @Override
    public int update(String hql, Object... args)
    {
        Query query = getSession().createQuery(hql);
        if ((args != null) && (args.length > 0))
        {
            for (int i = 0; i < args.length; i++)
            {
                query.setParameter(i, args[i]);
            }
        }
        return query.executeUpdate();
    }
}