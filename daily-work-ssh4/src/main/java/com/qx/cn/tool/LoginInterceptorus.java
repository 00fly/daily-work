package com.qx.cn.tool;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginInterceptorus extends AbstractInterceptor
{
    private static final long serialVersionUID = 7384059050774374164L;
    
    @Override
    public String intercept(ActionInvocation invocation)
        throws Exception
    {
        return invocation.invoke();
    }
}