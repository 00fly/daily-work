package com.qx.cn.tool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TreeJson implements Serializable
{
    private String orgCode;
    
    private String orgName;
    
    private String pOrgCode;
    
    private Integer enabled;
    
    private String state;
    
    private List<TreeJson> children = new ArrayList();
    
    public static List<TreeJson> formatTree(List<TreeJson> list)
    {
        TreeJson root = new TreeJson();
        TreeJson node = new TreeJson();
        List treelist = new ArrayList();
        List parentnodes = new ArrayList();
        List jsonlist = new ArrayList();
        
        if ((list != null) && (list.size() > 0))
        {
            root = (TreeJson)list.get(0);
            
            for (int i = 1; i < list.size(); i++)
            {
                node = (TreeJson)list.get(i);
                String nodeCodeStr = node.getOrgCode().substring(0, node.getOrgCode().length() - 3);
                if (nodeCodeStr.equals(root.getOrgCode()))
                {
                    parentnodes.add(node);
                    node.setpOrgCode(nodeCodeStr);
                    jsonlist.add(node);
                    root.getChildren().add(node);
                }
                else
                {
                    getChildrenNodes(parentnodes, node);
                    parentnodes.add(node);
                    node.setpOrgCode(nodeCodeStr);
                    jsonlist.add(node);
                }
            }
        }
        
        treelist.add(root);
        return jsonlist;
    }
    
    private static void getChildrenNodes(List<TreeJson> parentnodes, TreeJson node)
    {
        for (int i = parentnodes.size() - 1; i >= 0; i--)
        {
            TreeJson pnode = (TreeJson)parentnodes.get(i);
            String pnodeCodeStr = pnode.getOrgCode().substring(0, pnode.getOrgCode().length() - 3);
            
            if (pnodeCodeStr.equals(node.getOrgCode()))
            {
                pnode.setState("closed");
                pnode.getChildren().add(node);
                return;
            }
            
            parentnodes.remove(i);
        }
    }
    
    public String getOrgCode()
    {
        return this.orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public String getOrgName()
    {
        return this.orgName;
    }
    
    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public List<TreeJson> getChildren()
    {
        return this.children;
    }
    
    public void setChildren(List<TreeJson> children)
    {
        this.children = children;
    }
    
    public String getpOrgCode()
    {
        return this.pOrgCode;
    }
    
    public void setpOrgCode(String pOrgCode)
    {
        this.pOrgCode = pOrgCode;
    }
    
    public String getState()
    {
        return this.state;
    }
    
    public void setState(String state)
    {
        this.state = state;
    }
}