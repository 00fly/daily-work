package com.qx.cn.tool;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;

public class DateForm
{
    public static String SimpleDate(Date date)
    {
        return DateFormatUtils.format(date, "yyyy-MM-dd");
    }
    
    public static String SimpleDateTime(Date date)
    {
        return DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss");
    }
    
    public static String dateToXingQi(String dateStr)
        throws ParseException
    {
        String[] day = {"日", "一", "二", "三", "四", "五", "六"};
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date d = df.parse(dateStr);
        String xqStr = "星期" + day[d.getDay()];
        return xqStr;
    }
    
    public static Date StrToDate(String dateStr)
        throws ParseException
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = df.parse(dateStr);
        return date;
    }
    
    public static Date StrToDateTime(String dateStr)
        throws ParseException
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = df.parse(dateStr);
        return date;
    }
}