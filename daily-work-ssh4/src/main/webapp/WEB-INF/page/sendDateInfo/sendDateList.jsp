<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>邮件</title> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
	<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="js/syUtil.js"></script>
	<link rel="stylesheet" type="text/css"
			href="css/mast.css">
	<script type="text/javascript">
	
	$(function(){
	    searchForm = $('#searchFormSD').form();
	    $('#sendDateGrid').datagrid({ 
	        title:'日期列表', 
	        url:"sendDate!querySendDateByPage",
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:10,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'id',title:'编码',width:200,hidden:'true'},
				{field:'name',title:'名称',width:150,align:'center'},
				{field:'weekDate',title:'日期',width:180,align:'center'},
				{field:'status',title:'状态',width:100,align:'center'},
				{field:'type',title:'类型',width:100,align:'center'},
				{field:'memo',title:'备注',width:250,align:'center'}
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	                $('#addSendDateId').show();
					$('#sdfrm').form('clear');
					$("#titleId").html("添加日期");
					$('#tabId').val(0);
					$('input:radio:first').attr('checked', 'true'); 
					$('#addSendDateId').dialog({
						title: '添加日期',
						width: 430,
						height: 300,
						padding: 5,
						modal: true,
						buttons: [{
							text: '提交',
							handler: function() {
							 saveSendDate();
						}
						},
						{
							text: '取消',
							handler: function() {
							$('#addSendDateId').dialog('close');
						}
						}]
					});
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	               var row = $('#sendDateGrid').datagrid('getSelected');
					if (row){
						$('#addSendDateId').show(); 
						$('#sdfrm').form('clear');
						$('#seId').val(row.id);
						$('#addSendDateId').dialog({
							title: '修改日期',
							width: 430,
						    height: 300,
							padding: 5,
							modal: true,
							buttons: [{
								text: '提交',
								handler: function() {
								updateSendDate();
							}
							},
							{
								text: '取消',
								handler: function() {
								$('#addSendDateId').dialog('close');
							}
							}]
						});
						
						$("#seId").val(row.id);
						$("#nameId").datebox('setValue', row.name);
						$("#memoId").val(row.memo); 
						if(row.type=="日报"){ 
							$('input:radio:first').attr('checked', 'true');  //选中首个radio
						} else {
							$('input:radio:last').attr('checked', 'true');  //选中后一个radio
						}
					}else{
						$.messager.alert('提示','请选择你要修改的日期!','warning');
					}
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	                var row = $('#sendDateGrid').datagrid('getSelected');
					if (row){
					    if(row.status=="过期"){
					       $.messager.alert('提示','该日期已经过期,不可以删除!','info');
					       return false;
					    }else{
						        $.messager.confirm('提示','你确定删除【'+row.name+'】?',function(r){
								if (r){
									$.ajax({
										type: "POST",
										url: "sendDate!deleteSendDate",
										data: {id:row.id},
										success: function(data){
											var obj = eval('('+data+')');
											if (obj.result==1){
												$('#sendDateGrid').datagrid("reload"); 
												$.messager.show({    // show error message
													title: '提示',
													msg: obj.errorMsg
												}); 
											} else if(obj.result==2){
												$.messager.show({    // show error message
													title: '提示',
													msg: obj.errorMsg
												});
											}else{
												$.messager.show({    // show error message
													title: '提示',
													msg: "请检查网络是否链接正常!"
												});
											}
										}
									});
								}
							});
					    }
					}else{
						$.messager.alert('提示','请选择你要删除的日期!','info');
					}
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				   $('#sendDateGrid').datagrid("reload"); 
				}
			},'-',{
				iconCls: 'icon-hide',
				text:'增加1周',
				handler: function(){
				   $.messager.confirm('提示', '确定自动增加1周吗?', function(r){
		                if (r){
		                   $.ajax({
								type: "POST",
								url: "sendDate!addSendDateByAuto",
								data: {},
								success: function(data){
									var obj = eval('('+data+')');
									if (obj.result==1){
										$('#sendDateGrid').datagrid("reload"); 
										$.messager.show({    // show error message
											title: '提示',
											msg: obj.errorMsg
										}); 
									} else if(obj.result==2){
										$.messager.show({    // show error message
											title: '提示',
											msg: obj.errorMsg
										});
									}else{
										$.messager.show({    // show error message
											title: '提示',
											msg: "请检查网络是否链接正常!"
										});
									}
								}
							});
		                }
		           });

				}
			}]
	    });
	});
	
	//是否可用图标显示
	var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
    }; 
    
    //保存日期
    function saveSendDate(){
    	var name = $('#nameId').datebox('getValue');
    	if(name==""){
    	    $.messager.show({
					title: '提示',
					msg: "日期不能为空!"
			}); 
			return false;
    	} 
    $.ajax({
		type: "POST",
		url: "sendDate!checkSaveOrUpdate",
		data: {
			id:$("#seId").val(),
			name:$('#nameId').datebox('getValue')
		},
		success: function(data){
			var ob = eval('('+data+')');
			if (ob.result==1){
					$.ajax({
					type: "POST",
					url: "sendDate!saveSendDate",
					data: {
						id:$("#seId").val(),
						name:$('#nameId').datebox('getValue'),  //名称
						type:$('input:radio:checked').val(), //类型
						memo:$("#memoId").val()  //备注
					},
					success: function(data){
						var obj = eval('('+data+')');
						if (obj.result==1){
							$('#sendDateGrid').datagrid("reload"); 
							$('#addSendDateId').dialog('close');
							$.messager.show({    // show error message
								title: '提示',
								msg: obj.errorMsg
							}); 
						} else if(obj.result==2){
							$.messager.show({    // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}else {
							$.messager.show({    // show error message
								title: '提示',
								msg: "请检查网络是否链接正常!"
							});
						}
					}
				});
			} else if(ob.result==2){
				$.messager.show({    // show error message
					title: '提示',
					msg: ob.errorMsg
				});
			}else {
				$.messager.show({    // show error message
					title: '提示',
					msg: "请检查网络是否链接正常!"
				});
			}
		}
	}); 
  }
    
    //更新日期
    function updateSendDate(){
    	var name = $('#nameId').datebox('getValue');
    	if(name==""){
    	    $.messager.show({
					title: '提示',
					msg: "日期不能为空!"
			}); 
			return false;
    	}  
		$.ajax({
		type: "POST",
		url: "sendDate!saveSendDate",
		data: {
			id:$("#seId").val(),
			name:$('#nameId').datebox('getValue'),  //名称
			type:$('input:radio:checked').val(), //类型
			memo:$("#memoId").val()  //备注
		},
		success: function(data){
			var obj = eval('('+data+')');
			if (obj.result==1){
				$('#sendDateGrid').datagrid("reload"); 
				$('#addSendDateId').dialog('close');
				$.messager.show({    // show error message
					title: '提示',
					msg: obj.errorMsg
				}); 
			} else if(obj.result==2){
				$.messager.show({    // show error message
					title: '提示',
					msg: obj.errorMsg
				});
			}else {
				$.messager.show({    // show error message
					title: '提示',
					msg: "请检查网络是否链接正常!"
				});
			}
		 } 
	}); 
  }
    
    
    $(function(){
         $("#check_group").click(function(){
        	if(this.checked){ 
				$(".groupCk").each(function(){this.checked=true;}); 
			}else{ 
				$(".groupCk").each(function(){this.checked=false;}); 
			} 
	    }); 
    });
    
    
    //检索
    function searchUser(){
        $('#sendDateGrid').datagrid('load', sy.serializeObject(searchForm));
    }
    //重置
    function crearUser(){
        searchForm.find("input[type!='button']").val('');
    } 
	</script>

    <style type="text/css">
    .form_label{
    text-align:right;
    }
    </style>
  
  <body class="easyui-layout">
        <div data-options="region:'north',split:true,border:false,title:'查询栏'" style="height:95px;background-color:#F0F0F0;">
                 <fieldset>
			  <legend>查询</legend>
			  <form method="post" id="searchFormSD">  
			        日期: <input type="text" name="name" class="easyui-datebox" style="width:155px;"/>&nbsp;
			    <input type="button" onclick="searchUser()" value='检索'></input>
				<input type="button" onclick="crearUser()" value='清空'></input>
			  </form>
			</fieldset>
        </div>
        <div region="center" border="false">
			<table id="sendDateGrid">
			</table>
		</div>
	
         <!-- 添加日期界面 -->
  			<div id="addSendDateId" >
			<center>
			<form method="post" id="sdfrm" >
				<br/>
				<br/>
				<input name="id" id="seId" style="display:none;">
				<br/>
				<br/>
				<table width="300px" align="center" style="align: center;" cellpadding="7" border="0">
					<tr>
						<td class="form_label" style="width:150xp;">日期：</td>
						<td class="form_content"><input type="text" name="name" id="nameId" class="easyui-datebox easyui-validatebox" data-options="required:true" maxlength="10"></input></td>
					</tr>
					<tr>
						<td class="form_label" style="width:150xp;">类型：</td>
						<td class="form_content">
							<input type="radio" name="type" value="day"/>日报 &nbsp;
							<input type="radio" name="type" value="day,week"/>日报、周报
						</td>
					</tr>
					<tr>
						<td class="form_label">备注：</td>
						<td class="form_content" >
						<input type="text" name="memo" id="memoId">
						</td>
					</tr>
				</table>
			</form>
			</center>
		</div>
		
  </body>
</html>
