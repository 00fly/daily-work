<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>发送时间管理</title> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
	<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
    <link rel="stylesheet" type="text/css"
			href="css/mast.css">
    <script type="text/javascript">
       $(function(){
              $.ajax({
					type: "POST",
					url: "sendTime!querySendTimeToShow",
					data: {
					},
					success: function(data){
						var obj = eval('('+data+')');
						if (obj.result==1){
						   $("#startTimeId").val(obj.startTimeShow);
						   $("#endTimeId").val(obj.endTimeShow);
						   $("#showSTId").html(obj.startTimeShow);
						   $("#showETId").html(obj.endTimeShow);
						}else {
							$.messager.show({    // show error message
								title: '提示',
								msg: "请检查网络是否链接正常!"
							});
						}
					}
				});
				
          $("#saveTimeId").bind('click',function(){
              var startTime = $("#startTimeId").timespinner('getValue');  
              var endTime = $("#endTimeId").timespinner('getValue');
              if(startTime==""){
                $.messager.alert('提示','允许上交时间不能为空!','info');
                 return false;
              }
              
              if(startTime<"08:00"){
                 $.messager.alert('提示','允许上交不能小于08:00!','info');
                 return false;
              }
              //结束时间
              if(endTime==""){
                 $.messager.alert('提示','截止上交时间不能为空!','info');
                 return false;
              }
              
              if(endTime>"21:00"){
                 $.messager.alert('提示','截止上交时间不能大于21:00!','info');
                 return false;
              }
              
               //结束时间
              if(startTime>endTime){
                 $.messager.alert('提示','截止上交时间不能小于允许上交时间!','info');
                 return false;
              }
              
              $.ajax({
					type: "POST",
					url: "sendTime!saveSendTime",
					data: {
						startTime:startTime,
						endTime:endTime //名称
					},
					success: function(data){
						var obj = eval('('+data+')');
						if (obj.result==1){
							$.ajax({
								type: "POST",
								url: "sendTime!querySendTimeToShow",
								data: {
								},
								success: function(data){
									var obj = eval('('+data+')');
									if (obj.result==1){
									   $("#startTimeId").val(obj.startTimeShow);
									   $("#endTimeId").val(obj.endTimeShow);
									   $("#showSTId").html(obj.startTimeShow);
									   $("#showETId").html(obj.endTimeShow);
									}else {
										$.messager.show({    // show error message
											title: '提示',
											msg: "请检查网络是否链接正常!"
										});
									}
								}
							});
							
							$.messager.show({    // show error message
								title: '提示',
								msg: obj.errorMsg
							}); 
						} else if(obj.result==2){
							$.messager.show({    // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}else {
							$.messager.show({    // show error message
								title: '提示',
								msg: "请检查网络是否链接正常!"
							});
						}
					}
				});
          }); 
       });
     
    </script>
  </head>
  
  <body class="easyui-layout">
        <div region="center" border="false">
			<center>
			<table cellpadding="10" align="center">
		         <tr align="center">
		           <th colspan=4>可设置时间范围为<span style='color:red;'>【08:00~21:00】</span></th>
		        </tr>
		        <tr>
		            <th>允许上交时间:<input type="text" class="easyui-timespinner" name="startTime" id="startTimeId" style="width:80px;"></th>
		            <th>至</th>
		            <th>截止上交时间:<input type="text" class="easyui-timespinner" name="endTime" id="endTimeId" style="width:80px;"></th>
		            <th>&nbsp;&nbsp;<input type="button" id="saveTimeId" value="保存"/></th>
		        </tr>
		    </table>
		    
		    <hr/>
		    <table cellpadding="200">
		         <tr>
		            <td><div title="允许上交时间" id="showSTId" style="font-size:50px;font-weight:800"></div></td>
		            <td><div title="截止上交时间" id="showETId" style="font-size:50px;font-weight:800"></div></td>
		         </tr>
		    </table>
		    </center>
		</div>
  </body>
</html>
