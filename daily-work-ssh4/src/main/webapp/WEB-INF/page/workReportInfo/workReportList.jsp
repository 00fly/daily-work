<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>日报</title> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
	<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="js/syUtil.js"></script>
	<link rel="stylesheet" type="text/css"
			href="css/mast.css">
	<script type="text/javascript">
	var editRow = undefined;
	var datagrid;
	$(function(){
	    searchForm = $('#searchFormSD').form();
	    $('#workReportGrid').datagrid({ 
	        title:'日报列表', 
	        url:"workReport!queryWorkReportByPage",
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:10,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'id',title:'编码',width:200,hidden:'true'},
				{field:'workReportName',title:'名称',width:200,align:'center'},
				{field:'sendTime',title:'日期',width:120,align:'center'},
				{field:'createTime',title:'创建时间',width:200,align:'center'},
				{field:'itemNum',title:'日报项数',width:100,align:'center'},
				{field:'sumTime',title:'时间(小时)',width:100,align:'center'},
				{field:'status',title:'状态',width:100,align:'center'},
				{field:'wrCode',title:'编码',width:200,hidden:'true'}
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	                 $.ajax({
                        url: "workReport!checkSendTime",
                        data: {},
                        success: function (data) {
                            var obj = eval('(' + data + ')');
                            if (obj.result == 1) {
                                 	//添加项
                                 	$('#addWorkReportId').show();
									$('#wsgfrm').form('clear');
									$("#titleId").html("添加日报");
									$('#tabId').val(0);
									$('#addWorkReportId').dialog({
										title: '添加日报',
										width: 815,
										height: 400,
										padding: 5,
										modal: true,
										buttons: [{
											text: '提交',
											handler: function() {
											 saveWorkReport();
										}
										},
										{
											text: '关闭',
											handler: function() {
											$.messager.confirm('提示','你确定放弃此次添加?',function(r){
												if (r){
													$('#addWorkReportId').dialog('close');
												}
											});
											
										}
										}]
									});
									
					
									//工作项
									var wrCode = sy.UUID();
					                $("#wrCodeId").html(wrCode);
	
	                                datagrid = $('#dgId').datagrid({
	                                    url: 'workReportItem!queryWorkItemList?wrCode=' + wrCode,
	                                    //title: '日报项',
	                                    idField: 'id',
	                                    singleSelect: true,
	                                    columns: [
	                                        [{
	                                            field: 'id',
	                                            width: 80,
	                                            hidden: 'true',
	                                            title: 'Item ID'
	                                        },
	                                        {
	                                            field: 'planId',
	                                            width: 80,
	                                            hidden: 'true',
	                                            title: 'Item ID'
	                                        },
	                                        {
	                                            field: 'xulei',
	                                            width: 30,
	                                            title: '序号',
	                                            align: 'center',
	                                        },
	                                        {
	                                            field: 'content',
	                                            width: 480,
	                                            editor: {
	                                                type: 'validatebox',
	                                                options: {
	                                                    required: true
	                                                }
	                                            },
	                                            title: '日报内容'
	                                        },{
												field : 'workTime',
												title : '时间(小时)',
												width : 80,
												align:'center',
												editor : {
													type : 'combobox',
													options : {
														valueField : 'value',
														textField : 'label',
														panelHeight: "auto",
														data : [{
														    "value":1,
														    "label":"1",
														    "selected":true
														},{
														    "value":2,
														    "label":"2"
														},{
														    "value":3,
														    "label":"3"
														},{
														    "value":4,
														    "label":"4"
														},{
														    "value":5,
														    "label":"5"
														},{
														    "value":6,
														    "label":"6"
														},{
														    "value":7,
														    "label":"7"
														},{
														    "value":8,
														    "label":"8"
														}]
													}
												}
											},
	                                        {
	                                            field: 'remarks',
	                                            width: 180,
	                                            editor: {
	                                                type: 'text'
	                                            },
	                                            title: '备注'
	                                        }]
	                                    ],
	                                    toolbar: [{
	                                        text: '增加',
	                                        iconCls: 'icon-add',
	                                        handler: function () {
	                                            add();
	                                        }
	                                    }, '-',
	                                    {
	                                        text: '删除',
	                                        iconCls: 'icon-remove',
	                                        handler: function () {
	                                            del();
	                                        }
	                                    }, '-',
	                                    {
	                                        text: '修改',
	                                        iconCls: 'icon-edit',
	                                        handler: function () {
	                                            edit();
	                                        }
	                                    }, '-',
	                                    {
	                                        text: '保存',
	                                        iconCls: 'icon-save',
	                                        handler: function () {
	                                            if (editRow != undefined) {
	                                                datagrid.datagrid('endEdit', editRow);
	                                            }
	                                        }
	                                    }, '-',
	                                    {
	                                        text: '取消编辑',
	                                        iconCls: 'icon-undo',
	                                        handler: function () {
	                                            datagrid.datagrid('unselectAll');
	                                            datagrid.datagrid('rejectChanges');
	                                            editRow = undefined;
	                                        }
	                                    }, '-',
	                                    {
	                                        text: '取消选中',
	                                        iconCls: 'icon-undo',
	                                        handler: function () {
	                                            datagrid.datagrid('unselectAll');
	                                        }
	                                    }, '-'],
	                                    onLoadSuccess: function () {
	                                        editRow = undefined;
	                                    },
	                                    onDblClickRow: function (rowIndex, rowData) {
	                                        if (editRow != undefined) {
	                                            datagrid.datagrid('endEdit', editRow);
	                                            $(".datagrid-editable-input").height(25);
	                                        }
	
	                                        if (editRow == undefined) {
	                                            datagrid.datagrid('beginEdit', rowIndex);
	                                            editRow = rowIndex;
	                                            datagrid.datagrid('unselectAll');
	                                            $(".datagrid-editable-input").height(25);
	                                        }
	                                    },
	                                    onAfterEdit: function (rowIndex, rowData, changes) {
	                                        var inserted = datagrid.datagrid('getChanges', 'inserted');
	                                        var updated = datagrid.datagrid('getChanges', 'updated');
	                                        if (inserted.length < 1 && updated.length < 1) {
	                                            editRow = undefined;
	                                            datagrid.datagrid('unselectAll');
	                                            return;
	                                        }
	
	                                        var url = '';
	                                        if (inserted.length > 0) {
	                                            url = 'workReportItem!saveWorkItem?wrCode=' + wrCode;
	                                        }
	                                        if (updated.length > 0) {
	                                            url = 'workReportItem!updateWorkItem';
	                                        }
	
	                                        $.ajax({
	                                            url: url,
	                                            data: rowData,
	                                            success: function (data) {
	                                                var obj = eval('(' + data + ')');
	                                                if (obj.result == 1) {
	                                                    datagrid.datagrid('acceptChanges');
	                                                    $.messager.show({
	                                                        msg: obj.msg,
	                                                        title: '提示'
	                                                    });
	                                                    editRow = undefined;
	                                                    $('#dgId').datagrid('reload');
	                                                } else if(obj.result == 2){
	                                                    datagrid.datagrid('beginEdit', editRow);
	                                                    $(".datagrid-editable-input").height(25);
	                                                     $.messager.show({
	                                                            msg: obj.msg,
	                                                            title: '提示'
	                                                     });
	                                                }else{
	                                                	$.messager.show({
	                                                        msg: "请检查网络是否链接正常!",
	                                                        title: '提示'
	                                                    });
	                                                }
	                                                datagrid.datagrid('unselectAll');
	                                            }
	                                        });
	                                    },
	                                    onRowContextMenu: function (e, rowIndex, rowData) {
	                                        e.preventDefault();
	                                        $(this).datagrid('unselectAll');
	                                        $(this).datagrid('selectRow', rowIndex);
	                                        $('#menu').menu('show', {
	                                            left: e.pageX,
	                                            top: e.pageY
	                                        });
	                                    }
	                                });
	                                
	                                //end 工作项
                            } else if(obj.result==2){
                                $.messager.alert('提示', obj.errorMsg, 'info');
                            }else{
	                            $.messager.show({
									msg : "请检查网络是否链接正常!",
									title : '提示'
								});
                            }
                        }
                    });
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	               var row = $('#workReportGrid').datagrid('getSelected');
					if (row){
					     //检测是否可以删除
						 $.ajax({
							url : 'workReport!checkSendTimeToUpdate',
							data : {
							createTime:row.createTime,
							id:row.id
						},
						success : function(data) {
							var obj1 = eval('('+data+')');
							if (obj1.result==1){
						$('#addWorkReportId').show();
						$('#wsgfrm').form('clear');
						$('#seId').val(row.id);
						$('#addWorkReportId').dialog({
							title: '修改日报',
							width: 815,
						    height: 400,
							padding: 5,
							modal: true,
							buttons: [{
								text: '提交',
								handler: function() {
								saveWorkReport();
							}
							},
							{
								text: '关闭',
								handler: function() {
								$('#addWorkReportId').dialog('close');
							}
							}]
						});
						
						$("#seId").val(row.id);
						$("#workReportNameId").val(row.workReportName);
						
						
						                //修改项
						                var wrCode = row.wrCode;
	
	                                    datagrid = $('#dgId').datagrid({
	                                        url: 'workReportItem!queryWorkItemList?wrCode=' + wrCode,
	                                        idField: 'id',
	                                        singleSelect: true,
	                                        columns: [
	                                            [{
	                                                field: 'id',
	                                                width: 80,
	                                                hidden: 'true',
	                                                title: 'Item ID'
	                                            },
	                                            {
	                                                field: 'planId',
	                                                width: 80,
	                                                hidden: 'true',
	                                                title: 'Item ID'
	                                            },
	                                            {
	                                                field: 'xulei',
	                                                width: 30,
	                                                title: '序号',
	                                                align: 'center',
	                                            },
	                                            {
	                                                field: 'content',
	                                                width: 480,
	                                                editor: {
	                                                    type: 'validatebox',
	                                                    options: {
	                                                        required: true
	                                                    }
	                                                },
	                                                title: '项内容'
		                                            },
		                                            {
													field : 'workTime',
													title : '时间(小时)',
													width : 80,
													align:'center',
													editor : {
														type : 'combobox',
														options : {
															valueField : 'value',
															textField : 'label',
															panelHeight: "auto",
															data : [{
															    "value":1,
															    "label":"1"
															},{
															    "value":2,
															    "label":"2"
															},{
															    "value":3,
															    "label":"3"
															},{
															    "value":4,
															    "label":"4"
															},{
															    "value":5,
															    "label":"5"
															},{
															    "value":6,
															    "label":"6"
															},{
															    "value":7,
															    "label":"7"
															},{
															    "value":8,
															    "label":"8"
															}]
														}
													}
												},
		                                        {
		                                            field: 'remarks',
		                                            width: 180,
		                                            editor: {
		                                                type: 'text'
		                                            },
		                                            title: '备注'
		                                        }
												]
	                                        ],
	                                        toolbar: [{
	                                            text: '增加',
	                                            iconCls: 'icon-add',
	                                            handler: function () {
	                                                add();
	                                            }
	                                        }, '-',
	                                        {
	                                            text: '删除',
	                                            iconCls: 'icon-remove',
	                                            handler: function () {
	                                                del();
	                                            }
	                                        }, '-',
	                                        {
	                                            text: '修改',
	                                            iconCls: 'icon-edit',
	                                            handler: function () {
	                                                edit();
	                                            }
	                                        }, '-',
	                                        {
	                                            text: '保存',
	                                            iconCls: 'icon-save',
	                                            handler: function () {
	                                                if (editRow != undefined) {
	                                                    datagrid.datagrid('endEdit', editRow);
	                                                }
	                                            }
	                                        }, '-',
	                                        {
	                                            text: '取消编辑',
	                                            iconCls: 'icon-undo',
	                                            handler: function () {
	                                                datagrid.datagrid('unselectAll');
	                                                datagrid.datagrid('rejectChanges');
	                                                editRow = undefined;
	                                            }
	                                        }, '-',
	                                        {
	                                            text: '取消选中',
	                                            iconCls: 'icon-undo',
	                                            handler: function () {
	                                                datagrid.datagrid('unselectAll');
	                                            }
	                                        }, '-'],
	                                        onDblClickRow: function (rowIndex, rowData) {
	                                            if (editRow != undefined) {
	                                                datagrid.datagrid('endEdit', editRow);
	                                                $(".datagrid-editable-input").height(25);
	                                            }
	
	                                            if (editRow == undefined) {
	                                                datagrid.datagrid('beginEdit', rowIndex);
	                                                editRow = rowIndex;
	                                                datagrid.datagrid('unselectAll');
	                                                $(".datagrid-editable-input").height(25);
	                                            }
	                                        },
	                                        onAfterEdit: function (rowIndex, rowData, changes) {
	                                            var inserted = datagrid.datagrid('getChanges', 'inserted');
	                                            var updated = datagrid.datagrid('getChanges', 'updated');
	                                            if (inserted.length < 1 && updated.length < 1) {
	                                                editRow = undefined;
	                                                datagrid.datagrid('unselectAll');
	                                                return;
	                                            }
	
	                                            var url = '';
	                                            if (inserted.length > 0) {
	                                                url = 'workReportItem!saveWorkItem?wrCode=' + wrCode;
	                                            }
	                                            if (updated.length > 0) {
	                                                url = 'workReportItem!updateWorkItem';
	                                            }
	
	                                            $.ajax({
	                                                url: url,
	                                                data: rowData,
	                                                success: function (data) {
	                                                    var obj = eval('(' + data + ')');
	                                                    if (obj.result == 1) {
	                                                        datagrid.datagrid('acceptChanges');
	                                                        $.messager.show({
	                                                            msg: obj.msg,
	                                                            title: '提示'
	                                                        });
	                                                        editRow = undefined;
	                                                        datagrid.datagrid('reload');
	                                                    } else if(obj.result==2){
	                                                        datagrid.datagrid('beginEdit', editRow);
	                                                        $.messager.show({
	                                                            msg: obj.msg,
	                                                            title: '提示'
	                                                        });
	                                                    }else{
	                                                        $.messager.show({
	                                                            msg: "请检查网络是否正常!",
	                                                            title: '提示'
	                                                        });
	                                                    }
	                                                    datagrid.datagrid('unselectAll');
	                                                }
	                                            });
	                                        },
	                                        onRowContextMenu: function (e, rowIndex, rowData) {
	                                            e.preventDefault();
	                                            $(this).datagrid('unselectAll');
	                                            $(this).datagrid('selectRow', rowIndex);
	                                            $('#menu').menu('show', {
	                                                left: e.pageX,
	                                                top: e.pageY
	                                            });
	                                        }
	                                    });
							} else if(obj1.result==2){
							     $.messager.alert('提示', obj1.errorMsg, 'info');
							}else {
								$.messager.show({    // show error message
									title: '提示',
									msg: "请检查网络是否链接正常!"
								});
							}
						}
						});
					}else{
						$.messager.alert('提示','请选择你要修改的日报!','warning');
					}
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	                var row = $('#workReportGrid').datagrid('getSelected');
					if (row){
					    //检测是否可以删除
						 $.ajax({
							url : 'workReport!checkSendTimeToDel',
							data : {
							createTime:row.createTime,
							id:row.id
						},
						success : function(data) {
							var obj1 = eval('('+data+')');
							if (obj1.result==1){
									$.messager.confirm('提示','你确定删除【'+row.workReportName+'】?',function(r){
									if (r){
										$.ajax({
											type: "POST",
											url: "workReport!deleteWorkReport",
											data: {id:row.id},
											success: function(data){
												var obj = eval('('+data+')');
												if (obj.result==1){
													$('#workReportGrid').datagrid("reload"); 
													$.messager.show({    // show error message
														title: '提示',
														msg: obj.errorMsg
													}); 
													
												} else if(obj.result==2){
													$.messager.show({    // show error message
														title: '提示',
														msg: obj.errorMsg
													});
												}else{
													$.messager.show({    // show error message
														title: '提示',
														msg: "请检查网络是否链接正常!"
													});
												}
											}
										});
									}
								});
							} else if(obj1.result==2){
							     $.messager.alert('提示', obj1.errorMsg, 'info');
							}else {
								$.messager.show({    // show error message
									title: '提示',
									msg: "请检查网络是否链接正常!"
								});
							}
						}
						});
					}else{
						$.messager.alert('提示','请选择你要删除的日报!','info');
					}
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				   $('#workReportGrid').datagrid("reload"); 
				}
			},'-',{
				iconCls: 'icon-see',
				text:'查看',
				handler: function(){
				   seeWorkReport();
				}
			},'-',{
				iconCls: 'icon-count',
				text:'历史',
				handler: function(){
				   seeAllWorkReport();
				}
			}]
	    });
	});
	
	//是否可用图标显示
	var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
    }; 
    
    //查看日报
    function seeWorkReport(){
 	    var row = $('#workReportGrid').datagrid('getSelected');
		if (row){
	       $("#seeWorkReportId").show();
	       $('#seeWorkReportId').dialog({
				title: '查看日报',
				width: $(window).width() * 0.85,
				height: $(window).height() * 0.6,
				padding: 5,
				modal: true,
				resizable:true,
				buttons: [
				{
					text: '关闭',
					handler: function() {
					$('#seeWorkReportId').dialog('close');
				}
				}]
			});
			
			//获取后台的数据
			$.ajax({
				type: "POST",
				url: "workReport!lookWorkReport",
				data: {id:row.id},
				success: function(data){
					var obj = eval('('+data+')');
					if (obj.result==1){
					    var dataHtml = "<tr style='background-color:#E8E8E8;'><th style='width:50px;'>项数</th><th style='width:600px;'>内容</th><th style='width:80px;'>时间(小时)</th><th style='width:200px;'>备注</th></tr>";
						for(var i=0;i<obj.datas.length;i++){
						   dataHtml = dataHtml+"<tr><td align='center'>"+(i+1)+"</td><td>"+obj.datas[i].content+"</td><td align='center'>"+obj.datas[i].workTime+"</td><td align='center'>"+obj.datas[i].remarks+"</td></tr>";
						}
						$("#workReportInfoId").html(dataHtml);
					}else{
						$.messager.show({    // show error message
							title: '提示',
							msg: "请检查网络是否链接正常!"
						});
					}
				}
			});
		}else{
		     $.messager.alert('提示', "请选择要查看的日报", 'info');
		}
    }
    
    function seeAllWorkReport(){
           $("#seeAllWorkReportId").show();
	       $('#seeAllWorkReportId').dialog({
				title: '查看日报总汇',
				width: $(window).width() * 0.85,
				height: $(window).height() * 0.8,
				padding: 5,
				modal: true,
				resizable:true,
				buttons: [
				{
					text: '关闭',
					handler: function() {
					$('#seeAllWorkReportId').dialog('close');
				}
				}]
			});
    
    }
    
    //添加
	function add() {
	    if (editRow != undefined) {
	        datagrid.datagrid('endEdit', editRow);
	        $(".datagrid-editable-input").height(22);
	    }
	
	    if (editRow == undefined) {
	        datagrid.datagrid('unselectAll');
	        var row = {
	            cid: sy.UUID()
	        };
	        datagrid.datagrid('insertRow', {
	            index: 0,
	            row: row
	        });
	        editRow = 0;
	        datagrid.datagrid('selectRow', 0);
	        datagrid.datagrid('beginEdit', 0);
	        $(".datagrid-editable-input").height(25);
	    }
	}
	
	//删除
	function del() {
	    var row = datagrid.datagrid('getSelected');
	    var ids = [];
	    if (row) {
	        $.messager.confirm('提示', '您要删除当前所选项目？', function (r) {
	            if (r) {
	                $.ajax({
	                    url: 'workReportItem!deleteWorkItemById',
	                    data: {
	                        id: row.id
	                    },
	                    dataType: 'json',
	                    success: function (response) {
	                        datagrid.datagrid('load');
	                        datagrid.datagrid('unselectAll');
	                        $.messager.show({
	                            title: '提示',
	                            msg: '删除成功！'
	                        });
	                    }
	                });
	            }
	        });
	    } else {
	        $.messager.alert('提示', '请选择要删除的记录！', 'error');
	    }
	}
	
	
	//修改 
	function edit() {
	    var rows = datagrid.datagrid('getSelections');
	    if (rows.length == 1) {
	        if (editRow != undefined) {
	            datagrid.datagrid('endEdit', editRow);
	            $(".datagrid-editable-input").height(25);
	        }
	
	        if (editRow == undefined) {
	            editRow = datagrid.datagrid('getRowIndex', rows[0]);
	            datagrid.datagrid('beginEdit', editRow);
	            datagrid.datagrid('unselectAll');
	            $(".datagrid-editable-input").height(25);
	        }
	    } else {
	        $.messager.show({
	            msg: '请选择一项进行修改！',
	            title: '错误'
	        });
	    }
	}
	
	
    //保存日报
    function saveWorkReport(){
    	//var workReportName = $('#workReportNameId').val();
    	 $.messager.confirm('提示', "<span style='font-size:15px;'>您确认已经保存所有的日报项内容了吗？</span>", function (r) {
	       if (r) { 
	        //保存操作
			$.ajax({
					type: "POST",
					url: "workReport!saveWorkReport",
					data: {
						id:$("#seId").val(),
						//workReportName:workReportName,
						wrCode:$("#wrCodeId").html()
					},
					success: function(data){
						var obj = eval('('+data+')');
						if (obj.result==1){
							$('#workReportGrid').datagrid("reload"); 
							$('#addWorkReportId').dialog('close');
							$.messager.show({    // show error message
								title: '提示',
								msg: obj.errorMsg
							}); 
							
						} else if(obj.result==2){
							$.messager.show({    // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}else {
							$.messager.show({    // show error message
								title: '提示',
								msg: "请检查网络是否链接正常!"
							});
						}
					}
				});
	   }
    })
    }
    
    $(function(){
         $("#check_group").click(function(){
        	if(this.checked){ 
				$(".groupCk").each(function(){this.checked=true;}); 
			}else{ 
				$(".groupCk").each(function(){this.checked=false;}); 
			} 
	    });
	    
	    //搜索
	    $("#ssButId").bind('click',function(){
	        var startDate = $("#startDate").datebox('getValue');
	        var endDate = $("#endDate").datebox('getValue');
	        //获取后台的数据
			$.ajax({
				type: "POST",
				url: "workReport!lookALLworkReportByDate",
				data: {
					startDate:startDate,
					endDate:endDate
				},
				success: function(data){
					var obj = eval('('+data+')');
					if (obj.result==1){
						$("#allWorkReportInfoId").html(obj.content);
					}else{
						$.messager.show({    // show error message
							title: '提示',
							msg: "请检查网络是否链接正常!"
						});
					}
				}
			});
	    });
    });
    
    
    //检索
    function searchUser(){
        $('#workReportGrid').datagrid('load', sy.serializeObject(searchForm));
    }
    //重置
    function crearUser(){
        searchForm.find("input[type!='button']").val('');
    } 
	</script>

    <style type="text/css">
    .form_label{
    text-align:right;
    }
    </style>
  
  <body class="easyui-layout">
        <div data-options="region:'north',split:true,border:false,title:'查询栏'" style="height:95px;background-color:#F0F0F0;">
                 <fieldset>
			  <legend>查询</legend>
			  <form method="post" id="searchFormSD">  
			        日报名称: <input type="text" name="workReportName"  style="width:155px;"/>&nbsp;
			    <input type="button" onclick="searchUser()" value='检索'></input>
				<input type="button" onclick="crearUser()" value='清空'></input>
			  </form>
			</fieldset>
        </div>
        <div region="center" border="false">
			<table id="workReportGrid">
			</table>
		</div>
	
         <!-- 添加日报界面 -->
		 <div id="addWorkReportId" >
			<center>
			<span id="wrCodeId" style="display:none;"></span>
			<form method="post" id="wsgfrm" >
				<input name="id" id="seId" style="display:none;">
		        <div>
		           	<center>
						<table id="dgId" style="width: 800px;"></table>
					</center>
		        </div>
			</form>
			</center>
		</div>
		
		<!-- 查看日报界面 -->
		<div id="seeWorkReportId" >
		<center>
		    <br/>
			<table id="workReportInfoId" align="center" border="1" cellpadding="10" width="980px">
			
			</table>
		</center>
		</div>
		
		<!-- 查看日报界面 -->
		<div id="seeAllWorkReportId" >
		<center>
		    <table align="center" cellpadding="10" width="500px">
		    <tr>
		        <th>从&nbsp;<input id="startDate" type="text" class="easyui-datebox easyui-validatebox" data-options="required:true"/>&nbsp;至&nbsp;<input id="endDate" type="text" class="easyui-datebox easyui-validatebox" data-options="required:true"/>&nbsp;<input type='button' id='ssButId' value='搜索'/>
		    </tr>
		    </table>
			<table id="allWorkReportInfoId" align="center" cellpadding="5" width="960px" class='tab'>
			
			</table>
		</center>
		</div>
		
  </body>
</html>
