<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>日报列表</title> 
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css"	href="js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="js/easyUI/themes/icon.css">
	<link rel="stylesheet" type="text/css"  href="css/mast.css">
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="js/syUtil.js"></script>
	<script type="text/javascript">
	var editRow = undefined;
	var datagrid;
	$(function(){
	    searchForm = $('#searchFormSD').form();
	    $('#manageWorkReportGrid').datagrid({ 
	        title:'汇总列表', 
	        url:"manageWorkReport!manageworkReportByPage",
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:10,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'id',title:'编码',width:200,hidden:'true'},
				{field:'name',title:'名称',width:200,align:'center'},
				{field:'date',title:'日期',width:100,align:'center'},
				{field:'userNames',title:'需提交报告员工',width:500,align:'center'},
				{field:'userNum',title:'人数',width:80,align:'center'},
				{field:'noUserNames',title:'未提交报告员工',width:100,align:'center',
				 formatter: function(value, row, index) {  
                    var abValue = "<span style='color:#00C;'>查看</span>";  
                    var content = "<div href='javascript:;' style='cursor:pointer' title='" + value + "' class='note'>" + abValue + "</div>";  
                    return content;  
                   } 
				},
				{field:'userIds',title:'用户ID',width:100,align:'center',hidden:'true'}
	        ]],
	        toolbar: [{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				   $('#manageWorkReportGrid').datagrid("reload"); 
				}
			},'-',{
				iconCls: 'icon-see',
				text:'查看',
				handler: function(){
				   seeWorkReport();
				}
			},'-',{
				iconCls: 'icon-save',
				text:'导出',
				handler: function(){
				   excelWorkReport();
				}
			}],
			onLoadSuccess:function(data)  
             {  
                $(".note").tooltip(  
                    {
                    position: 'left',  
                    onShow: function(){  
                        $(this).tooltip('tip').css({   
                            //width:'300',  
                            boxShadow: '1px 1px 3px #292929'                          
                        });  
                    }  
                }  
                );  
             }  
	    });
	});
	 
    
    //查看日报
    function seeWorkReport(){
 	    var row = $('#manageWorkReportGrid').datagrid('getSelected');
		if (row){
	       $("#seeWorkReportId").show();
	       $('#seeWorkReportId').dialog({
				title: '查看汇总',
				width: 1100,
				height: 500,
				padding: 5,
				modal: true,
				resizable:true,
				buttons: [
				{
					text: '关闭',
					handler: function() {
					$('#seeWorkReportId').dialog('close');
				}
				}]
			});
			
			//获取后台的数据
			$.ajax({
				type: "POST",
				url: "manageWorkReport!lookManageWorkReport",
				data: {
					id:row.id
				},
				success: function(data){
					var obj = eval('('+data+')');
					if (obj.result==1){
						$("#workReportInfoId").html(obj.content);
					}else{
						$.messager.show({    // show error message
							title: '提示',
							msg: "请检查网络是否链接正常!"
						});
					}
				}
			});
		}else{
		     $.messager.alert('提示', "请选择要查看的汇总报表", 'info');
		}
    }; 
    
    
    //excel导出日报
    function excelWorkReport(){
 	    var row = $('#manageWorkReportGrid').datagrid('getSelected');
		if (row){
		   window.open("manageWorkReport!exportExcel?id=" + row.id);
		}else{
		   $.messager.alert('提示', "请选择要导出的汇总报表", 'info');
		}
    }; 
    
    $(function(){
         $("#check_group").click(function(){
        	if(this.checked){ 
				$(".groupCk").each(function(){this.checked=true;}); 
			}else{ 
				$(".groupCk").each(function(){this.checked=false;}); 
			} 
	    });
	    
    });
    
    
    //检索
    function searchUser(){
        $('#manageWorkReportGrid').datagrid('load', sy.serializeObject(searchForm));
    }
    
    //重置
    function crearUser(){
        searchForm.find("input[type!='button']").val('');
    }    
	</script>
    <style type="text/css">
    .form_label{
    text-align:right;
    }
    </style>  
  <body class="easyui-layout">
        <div data-options="region:'north',split:true,border:false,title:'查询栏'" style="height:95px;background-color:#F0F0F0;">
                 <fieldset>
			  <legend>查询</legend>
			  <form method="post" id="searchFormSD">  
			        名称: <input type="text" name="workReportName"  style="width:155px;"/>&nbsp;
			    <input type="button" onclick="searchUser()" value='检索'></input>
				<input type="button" onclick="crearUser()" value='清空'></input>
			  </form>
			</fieldset>
        </div>
        <div region="center" border="false">
			<table id="manageWorkReportGrid">
			</table>
		</div>
		
		<!-- 查看日报界面 --> 
		<div id="seeWorkReportId" >
			<center>
			<table id="workReportInfoId" class='tab' cellpadding='5'  style='width:1000px;'>
			</table>
			</center>
		</div>		
  </body>
</html>
