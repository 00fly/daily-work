package com.qx.cn.main;

import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * 
 * MainRun
 * 
 * @author 00fly
 * @version [版本号, 2018-11-01]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class MainRun
{
    /**
     * Spring管理hibernate 演示
     * 
     * @param args
     * @see [类、类#方法、类#成员]
     */
    public static void main(String[] args)
    {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext();
        context.setValidating(false);
        context.load("classpath:applicationContext.xml");
        context.refresh();
        int i = 1;
        for (String beanName : context.getBeanDefinitionNames())
        {
            System.out.println(i + ".\t" + beanName);
            i++;
        }
        context.close();
    }
}
