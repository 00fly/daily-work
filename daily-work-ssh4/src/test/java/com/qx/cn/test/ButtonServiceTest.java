package com.qx.cn.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.model.Button;
import com.qx.cn.service.ButtonService;

/**
 * Junit 单元测试<BR>
 * 
 * @author 00fly
 * @version [版本号, 2018年8月8日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/applicationContext.xml"})
public class ButtonServiceTest
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ButtonServiceTest.class);
    
    @Autowired
    ButtonService buttonService;
    
    @Autowired
    ApplicationContext applicationContext;
    
    @Before
    public void before()
    {
        LOGGER.info("★★★★★★★★ ApplicationContext = {}", applicationContext);
        int i = 1;
        for (String beanName : applicationContext.getBeanDefinitionNames())
        {
            LOGGER.info("{}.\t{}", i, beanName);
            i++;
        }
    }
    
    @Test
    public void test()
    {
        List<Button> data = buttonService.queryAllButtons();
        LOGGER.info("Junit 单元测试    queryAllButtons: {}", data);
    }
}
