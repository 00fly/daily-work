/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50559
Source Host           : localhost:3306
Source Database       : dayreport

Target Server Type    : MYSQL
Target Server Version : 50559
File Encoding         : 65001

Date: 2018-07-22 17:34:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for busi_manager_work_report
-- ----------------------------
DROP TABLE IF EXISTS `busi_manager_work_report`;
CREATE TABLE `busi_manager_work_report` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(50) DEFAULT NULL,
  `d_date` varchar(50) DEFAULT NULL,
  `i_receiver_id` int(11) DEFAULT NULL,
  `i_user_ids` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1017 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of busi_manager_work_report
-- ----------------------------
INSERT INTO `busi_manager_work_report` VALUES ('279', '2016-05-06日报-研发', '2016-05-06', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('280', '2016-05-06周报-研发', '2016-05-06', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('281', '2016-05-06日报-研发', '2016-05-06', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('282', '2016-05-06周报-研发', '2016-05-06', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('283', '2016-05-06日报-研发', '2016-05-06', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('284', '2016-05-06周报-研发', '2016-05-06', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('285', '2016-05-06日报-研发', '2016-05-06', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('286', '2016-05-06周报-研发', '2016-05-06', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('287', '2016-05-06日报-研发', '2016-05-06', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('288', '2016-05-06周报-研发', '2016-05-06', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('289', '2016-05-06日报-研发', '2016-05-06', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('290', '2016-05-06周报-研发', '2016-05-06', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('291', '2016-05-06日报-研发', '2016-05-06', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('292', '2016-05-06周报-研发', '2016-05-06', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('293', '2016-05-06日报-研发', '2016-05-06', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('294', '2016-05-06周报-研发', '2016-05-06', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('295', '2016-05-06日报-研发', '2016-05-06', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('296', '2016-05-06周报-研发', '2016-05-06', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('297', '2016-05-06日报-研发', '2016-05-06', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('298', '2016-05-06周报-研发', '2016-05-06', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('299', '2016-05-06日报-研发', '2016-05-06', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('300', '2016-05-06周报-研发', '2016-05-06', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('301', '2016-05-06日报-研发', '2016-05-06', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('302', '2016-05-06周报-研发', '2016-05-06', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('303', '2016-05-06日报-研发', '2016-05-06', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('304', '2016-05-06周报-研发', '2016-05-06', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('305', '2016-05-09日报-研发', '2016-05-09', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('306', '2016-05-09日报-研发', '2016-05-09', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('307', '2016-05-09日报-研发', '2016-05-09', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('308', '2016-05-09日报-研发', '2016-05-09', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('309', '2016-05-09日报-研发', '2016-05-09', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('310', '2016-05-09日报-研发', '2016-05-09', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('311', '2016-05-09日报-研发', '2016-05-09', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('312', '2016-05-09日报-研发', '2016-05-09', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('313', '2016-05-09日报-研发', '2016-05-09', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('314', '2016-05-09日报-研发', '2016-05-09', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('315', '2016-05-09日报-研发', '2016-05-09', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('316', '2016-05-09日报-研发', '2016-05-09', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('317', '2016-05-09日报-研发', '2016-05-09', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('318', '2016-05-10日报-研发', '2016-05-10', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('319', '2016-05-10周报-研发', '2016-05-10', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('320', '2016-05-10日报-研发', '2016-05-10', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('321', '2016-05-10周报-研发', '2016-05-10', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('322', '2016-05-10日报-研发', '2016-05-10', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('323', '2016-05-10周报-研发', '2016-05-10', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('324', '2016-05-10日报-研发', '2016-05-10', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('325', '2016-05-10周报-研发', '2016-05-10', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('326', '2016-05-10日报-研发', '2016-05-10', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('327', '2016-05-10周报-研发', '2016-05-10', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('328', '2016-05-10日报-研发', '2016-05-10', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('329', '2016-05-10周报-研发', '2016-05-10', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('330', '2016-05-10日报-研发', '2016-05-10', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('331', '2016-05-10周报-研发', '2016-05-10', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('332', '2016-05-10日报-研发', '2016-05-10', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('333', '2016-05-10周报-研发', '2016-05-10', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('334', '2016-05-10日报-研发', '2016-05-10', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('335', '2016-05-10周报-研发', '2016-05-10', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('336', '2016-05-10日报-研发', '2016-05-10', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('337', '2016-05-10周报-研发', '2016-05-10', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('338', '2016-05-10日报-研发', '2016-05-10', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('339', '2016-05-10周报-研发', '2016-05-10', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('340', '2016-05-10日报-研发', '2016-05-10', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('341', '2016-05-10周报-研发', '2016-05-10', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('342', '2016-05-10日报-研发', '2016-05-10', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('343', '2016-05-10周报-研发', '2016-05-10', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('344', '2016-05-13日报-研发', '2016-05-13', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('345', '2016-05-13周报-研发', '2016-05-13', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('346', '2016-05-13日报-研发', '2016-05-13', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('347', '2016-05-13周报-研发', '2016-05-13', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('348', '2016-05-13日报-研发', '2016-05-13', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('349', '2016-05-13周报-研发', '2016-05-13', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('350', '2016-05-13日报-研发', '2016-05-13', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('351', '2016-05-13周报-研发', '2016-05-13', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('352', '2016-05-13日报-研发', '2016-05-13', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('353', '2016-05-13周报-研发', '2016-05-13', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('354', '2016-05-13日报-研发', '2016-05-13', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('355', '2016-05-13周报-研发', '2016-05-13', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('356', '2016-05-13日报-研发', '2016-05-13', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('357', '2016-05-13周报-研发', '2016-05-13', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('358', '2016-05-13日报-研发', '2016-05-13', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('359', '2016-05-13周报-研发', '2016-05-13', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('360', '2016-05-13日报-研发', '2016-05-13', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('361', '2016-05-13周报-研发', '2016-05-13', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('362', '2016-05-13日报-研发', '2016-05-13', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('363', '2016-05-13周报-研发', '2016-05-13', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('364', '2016-05-13日报-研发', '2016-05-13', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('365', '2016-05-13周报-研发', '2016-05-13', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('366', '2016-05-13日报-研发', '2016-05-13', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('367', '2016-05-13周报-研发', '2016-05-13', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('368', '2016-05-13日报-研发', '2016-05-13', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('369', '2016-05-13周报-研发', '2016-05-13', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('370', '2016-05-16日报-研发', '2016-05-16', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('371', '2016-05-16日报-研发', '2016-05-16', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('372', '2016-05-16日报-研发', '2016-05-16', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('373', '2016-05-16日报-研发', '2016-05-16', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('374', '2016-05-16日报-研发', '2016-05-16', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('375', '2016-05-16日报-研发', '2016-05-16', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('376', '2016-05-16日报-研发', '2016-05-16', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('377', '2016-05-16日报-研发', '2016-05-16', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('378', '2016-05-16日报-研发', '2016-05-16', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('379', '2016-05-16日报-研发', '2016-05-16', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('380', '2016-05-16日报-研发', '2016-05-16', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('381', '2016-05-16日报-研发', '2016-05-16', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('382', '2016-05-16日报-研发', '2016-05-16', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('383', '2016-05-17日报-研发', '2016-05-17', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('384', '2016-05-17日报-研发', '2016-05-17', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('385', '2016-05-17日报-研发', '2016-05-17', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('386', '2016-05-17日报-研发', '2016-05-17', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('387', '2016-05-17日报-研发', '2016-05-17', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('388', '2016-05-17日报-研发', '2016-05-17', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('389', '2016-05-17日报-研发', '2016-05-17', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('390', '2016-05-17日报-研发', '2016-05-17', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('391', '2016-05-17日报-研发', '2016-05-17', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('392', '2016-05-17日报-研发', '2016-05-17', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('393', '2016-05-17日报-研发', '2016-05-17', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('394', '2016-05-17日报-研发', '2016-05-17', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('395', '2016-05-17日报-研发', '2016-05-17', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('396', '2016-05-18日报-研发', '2016-05-18', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('397', '2016-05-18日报-研发', '2016-05-18', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('398', '2016-05-18日报-研发', '2016-05-18', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('399', '2016-05-18日报-研发', '2016-05-18', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('400', '2016-05-18日报-研发', '2016-05-18', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('401', '2016-05-18日报-研发', '2016-05-18', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('402', '2016-05-18日报-研发', '2016-05-18', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('403', '2016-05-18日报-研发', '2016-05-18', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('404', '2016-05-18日报-研发', '2016-05-18', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('405', '2016-05-18日报-研发', '2016-05-18', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('406', '2016-05-18日报-研发', '2016-05-18', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('407', '2016-05-18日报-研发', '2016-05-18', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('408', '2016-05-18日报-研发', '2016-05-18', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('409', '2016-05-20日报-研发', '2016-05-20', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('410', '2016-05-20周报-研发', '2016-05-20', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('411', '2016-05-20日报-研发', '2016-05-20', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('412', '2016-05-20周报-研发', '2016-05-20', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('413', '2016-05-20日报-研发', '2016-05-20', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('414', '2016-05-20周报-研发', '2016-05-20', '2', '2,6,8,17,3,10,14,18,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('415', '2016-05-20日报-研发', '2016-05-20', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('416', '2016-05-20周报-研发', '2016-05-20', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('417', '2016-05-20日报-研发', '2016-05-20', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('418', '2016-05-20周报-研发', '2016-05-20', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('419', '2016-05-20日报-研发', '2016-05-20', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('420', '2016-05-20周报-研发', '2016-05-20', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('421', '2016-05-20日报-研发', '2016-05-20', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('422', '2016-05-20周报-研发', '2016-05-20', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('423', '2016-05-20日报-研发', '2016-05-20', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('424', '2016-05-20周报-研发', '2016-05-20', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('425', '2016-05-20日报-研发', '2016-05-20', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('426', '2016-05-20周报-研发', '2016-05-20', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('427', '2016-05-20日报-研发', '2016-05-20', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('428', '2016-05-20周报-研发', '2016-05-20', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('429', '2016-05-20日报-研发', '2016-05-20', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('430', '2016-05-20周报-研发', '2016-05-20', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('431', '2016-05-20日报-研发', '2016-05-20', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('432', '2016-05-20周报-研发', '2016-05-20', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('433', '2016-05-20日报-研发', '2016-05-20', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('434', '2016-05-20周报-研发', '2016-05-20', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('435', '2016-05-27日报-研发', '2016-05-27', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('436', '2016-05-27周报-研发', '2016-05-27', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('437', '2016-05-27日报-研发', '2016-05-27', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('438', '2016-05-27周报-研发', '2016-05-27', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('439', '2016-05-27日报-研发', '2016-05-27', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('440', '2016-05-27周报-研发', '2016-05-27', '2', '2,6,8,17,3,10,14,18,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('441', '2016-05-27日报-研发', '2016-05-27', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('442', '2016-05-27周报-研发', '2016-05-27', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('443', '2016-05-27日报-研发', '2016-05-27', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('444', '2016-05-27周报-研发', '2016-05-27', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('445', '2016-05-27日报-研发', '2016-05-27', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('446', '2016-05-27周报-研发', '2016-05-27', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('447', '2016-05-27日报-研发', '2016-05-27', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('448', '2016-05-27周报-研发', '2016-05-27', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('449', '2016-05-27日报-研发', '2016-05-27', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('450', '2016-05-27周报-研发', '2016-05-27', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('451', '2016-05-27日报-研发', '2016-05-27', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('452', '2016-05-27周报-研发', '2016-05-27', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('453', '2016-05-27日报-研发', '2016-05-27', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('454', '2016-05-27周报-研发', '2016-05-27', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('455', '2016-05-27日报-研发', '2016-05-27', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('456', '2016-05-27周报-研发', '2016-05-27', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('457', '2016-05-27日报-研发', '2016-05-27', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('458', '2016-05-27周报-研发', '2016-05-27', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('459', '2016-05-27日报-研发', '2016-05-27', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('460', '2016-05-27周报-研发', '2016-05-27', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('461', '2016-06-03日报-研发', '2016-06-03', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('462', '2016-06-03周报-研发', '2016-06-03', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('463', '2016-06-03日报-研发', '2016-06-03', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('464', '2016-06-03周报-研发', '2016-06-03', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('465', '2016-06-03日报-研发', '2016-06-03', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('466', '2016-06-03周报-研发', '2016-06-03', '2', '2,6,8,17,3,10,14,18,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('467', '2016-06-03日报-研发', '2016-06-03', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('468', '2016-06-03周报-研发', '2016-06-03', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('469', '2016-06-03日报-研发', '2016-06-03', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('470', '2016-06-03周报-研发', '2016-06-03', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('471', '2016-06-03日报-研发', '2016-06-03', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('472', '2016-06-03周报-研发', '2016-06-03', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('473', '2016-06-03日报-研发', '2016-06-03', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('474', '2016-06-03周报-研发', '2016-06-03', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('475', '2016-06-03日报-研发', '2016-06-03', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('476', '2016-06-03周报-研发', '2016-06-03', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('477', '2016-06-03日报-研发', '2016-06-03', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('478', '2016-06-03周报-研发', '2016-06-03', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('479', '2016-06-03日报-研发', '2016-06-03', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('480', '2016-06-03周报-研发', '2016-06-03', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('481', '2016-06-03日报-研发', '2016-06-03', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('482', '2016-06-03周报-研发', '2016-06-03', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('483', '2016-06-03日报-研发', '2016-06-03', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('484', '2016-06-03周报-研发', '2016-06-03', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('485', '2016-06-03日报-研发', '2016-06-03', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('486', '2016-06-03周报-研发', '2016-06-03', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('487', '2016-06-12日报-研发', '2016-06-12', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('488', '2016-06-12周报-研发', '2016-06-12', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('489', '2016-06-12日报-研发', '2016-06-12', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('490', '2016-06-12周报-研发', '2016-06-12', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('491', '2016-06-12日报-研发', '2016-06-12', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('492', '2016-06-12周报-研发', '2016-06-12', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('493', '2016-06-12日报-研发', '2016-06-12', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('494', '2016-06-12周报-研发', '2016-06-12', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('495', '2016-06-12日报-研发', '2016-06-12', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('496', '2016-06-12周报-研发', '2016-06-12', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('497', '2016-06-12日报-研发', '2016-06-12', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('498', '2016-06-12周报-研发', '2016-06-12', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('499', '2016-06-12日报-研发', '2016-06-12', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('500', '2016-06-12周报-研发', '2016-06-12', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('501', '2016-06-12日报-研发', '2016-06-12', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('502', '2016-06-12周报-研发', '2016-06-12', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('503', '2016-06-12日报-研发', '2016-06-12', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('504', '2016-06-12周报-研发', '2016-06-12', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('505', '2016-06-12日报-研发', '2016-06-12', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('506', '2016-06-12周报-研发', '2016-06-12', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('507', '2016-06-12日报-研发', '2016-06-12', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('508', '2016-06-12周报-研发', '2016-06-12', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('509', '2016-06-12日报-研发', '2016-06-12', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('510', '2016-06-12周报-研发', '2016-06-12', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('511', '2016-06-12日报-研发', '2016-06-12', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('512', '2016-06-12周报-研发', '2016-06-12', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('513', '2016-06-17日报-研发', '2016-06-17', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('514', '2016-06-17周报-研发', '2016-06-17', '20', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('515', '2016-06-17日报-研发', '2016-06-17', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('516', '2016-06-17周报-研发', '2016-06-17', '1', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('517', '2016-06-17日报-研发', '2016-06-17', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('518', '2016-06-17周报-研发', '2016-06-17', '2', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('519', '2016-06-17日报-研发', '2016-06-17', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('520', '2016-06-17周报-研发', '2016-06-17', '3', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('521', '2016-06-17日报-研发', '2016-06-17', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('522', '2016-06-17周报-研发', '2016-06-17', '4', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('523', '2016-06-17日报-研发', '2016-06-17', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('524', '2016-06-17周报-研发', '2016-06-17', '6', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('525', '2016-06-17日报-研发', '2016-06-17', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('526', '2016-06-17周报-研发', '2016-06-17', '8', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('527', '2016-06-17日报-研发', '2016-06-17', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('528', '2016-06-17周报-研发', '2016-06-17', '10', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('529', '2016-06-17日报-研发', '2016-06-17', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('530', '2016-06-17周报-研发', '2016-06-17', '14', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('531', '2016-06-17日报-研发', '2016-06-17', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('532', '2016-06-17周报-研发', '2016-06-17', '16', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('533', '2016-06-17日报-研发', '2016-06-17', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('534', '2016-06-17周报-研发', '2016-06-17', '17', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('535', '2016-06-17日报-研发', '2016-06-17', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('536', '2016-06-17周报-研发', '2016-06-17', '18', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('537', '2016-06-17日报-研发', '2016-06-17', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('538', '2016-06-17周报-研发', '2016-06-17', '21', '2,3,4,6,8,10,14,16,17,18,21,');
INSERT INTO `busi_manager_work_report` VALUES ('539', '2016-06-24日报-研发', '2016-06-24', '1', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('540', '2016-06-24周报-研发', '2016-06-24', '1', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('541', '2016-06-24日报-研发', '2016-06-24', '2', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('542', '2016-06-24周报-研发', '2016-06-24', '2', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('543', '2016-06-24日报-研发', '2016-06-24', '3', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('544', '2016-06-24周报-研发', '2016-06-24', '3', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('545', '2016-06-24日报-研发', '2016-06-24', '6', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('546', '2016-06-24周报-研发', '2016-06-24', '6', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('547', '2016-06-24日报-研发', '2016-06-24', '8', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('548', '2016-06-24周报-研发', '2016-06-24', '8', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('549', '2016-06-24日报-研发', '2016-06-24', '17', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('550', '2016-06-24周报-研发', '2016-06-24', '17', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('551', '2016-06-24日报-研发', '2016-06-24', '14', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('552', '2016-06-24周报-研发', '2016-06-24', '14', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('553', '2016-06-24日报-研发', '2016-06-24', '10', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('554', '2016-06-24周报-研发', '2016-06-24', '10', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('555', '2016-06-24日报-研发', '2016-06-24', '21', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('556', '2016-06-24周报-研发', '2016-06-24', '21', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('557', '2016-06-24日报-研发', '2016-06-24', '4', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('558', '2016-06-24周报-研发', '2016-06-24', '4', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('559', '2016-06-24日报-研发', '2016-06-24', '16', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('560', '2016-06-24周报-研发', '2016-06-24', '16', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('561', '2016-07-01日报-研发', '2016-07-01', '1', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('562', '2016-07-01周报-研发', '2016-07-01', '1', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('563', '2016-07-01日报-研发', '2016-07-01', '2', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('564', '2016-07-01周报-研发', '2016-07-01', '2', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('565', '2016-07-01日报-研发', '2016-07-01', '3', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('566', '2016-07-01周报-研发', '2016-07-01', '3', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('567', '2016-07-01日报-研发', '2016-07-01', '6', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('568', '2016-07-01周报-研发', '2016-07-01', '6', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('569', '2016-07-01日报-研发', '2016-07-01', '8', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('570', '2016-07-01周报-研发', '2016-07-01', '8', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('571', '2016-07-01日报-研发', '2016-07-01', '17', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('572', '2016-07-01周报-研发', '2016-07-01', '17', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('573', '2016-07-01日报-研发', '2016-07-01', '14', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('574', '2016-07-01周报-研发', '2016-07-01', '14', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('575', '2016-07-01日报-研发', '2016-07-01', '10', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('576', '2016-07-01周报-研发', '2016-07-01', '10', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('577', '2016-07-01日报-研发', '2016-07-01', '21', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('578', '2016-07-01周报-研发', '2016-07-01', '21', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('579', '2016-07-01日报-研发', '2016-07-01', '4', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('580', '2016-07-01周报-研发', '2016-07-01', '4', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('581', '2016-07-01日报-研发', '2016-07-01', '16', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('582', '2016-07-01周报-研发', '2016-07-01', '16', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('583', '2016-07-08日报-研发', '2016-07-08', '1', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('584', '2016-07-08周报-研发', '2016-07-08', '1', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('585', '2016-07-08日报-研发', '2016-07-08', '2', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('586', '2016-07-08周报-研发', '2016-07-08', '2', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('587', '2016-07-08日报-研发', '2016-07-08', '3', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('588', '2016-07-08周报-研发', '2016-07-08', '3', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('589', '2016-07-08日报-研发', '2016-07-08', '6', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('590', '2016-07-08周报-研发', '2016-07-08', '6', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('591', '2016-07-08日报-研发', '2016-07-08', '8', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('592', '2016-07-08周报-研发', '2016-07-08', '8', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('593', '2016-07-08日报-研发', '2016-07-08', '17', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('594', '2016-07-08周报-研发', '2016-07-08', '17', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('595', '2016-07-08日报-研发', '2016-07-08', '14', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('596', '2016-07-08周报-研发', '2016-07-08', '14', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('597', '2016-07-08日报-研发', '2016-07-08', '10', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('598', '2016-07-08周报-研发', '2016-07-08', '10', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('599', '2016-07-08日报-研发', '2016-07-08', '21', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('600', '2016-07-08周报-研发', '2016-07-08', '21', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('601', '2016-07-08日报-研发', '2016-07-08', '4', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('602', '2016-07-08周报-研发', '2016-07-08', '4', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('603', '2016-07-08日报-研发', '2016-07-08', '16', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('604', '2016-07-08周报-研发', '2016-07-08', '16', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('605', '2016-07-15日报-研发', '2016-07-15', '1', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('606', '2016-07-15周报-研发', '2016-07-15', '1', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('607', '2016-07-15日报-研发', '2016-07-15', '2', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('608', '2016-07-15周报-研发', '2016-07-15', '2', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('609', '2016-07-15日报-研发', '2016-07-15', '3', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('610', '2016-07-15周报-研发', '2016-07-15', '3', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('611', '2016-07-15日报-研发', '2016-07-15', '6', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('612', '2016-07-15周报-研发', '2016-07-15', '6', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('613', '2016-07-15日报-研发', '2016-07-15', '8', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('614', '2016-07-15周报-研发', '2016-07-15', '8', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('615', '2016-07-15日报-研发', '2016-07-15', '17', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('616', '2016-07-15周报-研发', '2016-07-15', '17', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('617', '2016-07-15日报-研发', '2016-07-15', '14', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('618', '2016-07-15周报-研发', '2016-07-15', '14', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('619', '2016-07-15日报-研发', '2016-07-15', '10', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('620', '2016-07-15周报-研发', '2016-07-15', '10', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('621', '2016-07-15日报-研发', '2016-07-15', '21', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('622', '2016-07-15周报-研发', '2016-07-15', '21', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('623', '2016-07-15日报-研发', '2016-07-15', '4', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('624', '2016-07-15周报-研发', '2016-07-15', '4', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('625', '2016-07-15日报-研发', '2016-07-15', '16', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('626', '2016-07-15周报-研发', '2016-07-15', '16', '2,3,6,8,17,14,10,21,4,16,');
INSERT INTO `busi_manager_work_report` VALUES ('649', '2016-07-22日报-研发', '2016-07-22', '1', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('650', '2016-07-22周报-研发', '2016-07-22', '1', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('651', '2016-07-22日报-研发', '2016-07-22', '2', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('652', '2016-07-22周报-研发', '2016-07-22', '2', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('653', '2016-07-22日报-研发', '2016-07-22', '3', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('654', '2016-07-22周报-研发', '2016-07-22', '3', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('655', '2016-07-22日报-研发', '2016-07-22', '6', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('656', '2016-07-22周报-研发', '2016-07-22', '6', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('657', '2016-07-22日报-研发', '2016-07-22', '8', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('658', '2016-07-22周报-研发', '2016-07-22', '8', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('659', '2016-07-22日报-研发', '2016-07-22', '17', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('660', '2016-07-22周报-研发', '2016-07-22', '17', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('661', '2016-07-22日报-研发', '2016-07-22', '14', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('662', '2016-07-22周报-研发', '2016-07-22', '14', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('663', '2016-07-22日报-研发', '2016-07-22', '10', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('664', '2016-07-22周报-研发', '2016-07-22', '10', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('665', '2016-07-22日报-研发', '2016-07-22', '21', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('666', '2016-07-22周报-研发', '2016-07-22', '21', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('667', '2016-07-22日报-研发', '2016-07-22', '4', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('668', '2016-07-22周报-研发', '2016-07-22', '4', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('669', '2016-07-22日报-研发', '2016-07-22', '16', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('670', '2016-07-22周报-研发', '2016-07-22', '16', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('671', '2016-07-22日报-研发', '2016-07-22', '24', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('672', '2016-07-22周报-研发', '2016-07-22', '24', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('673', '2016-07-29日报-研发', '2016-07-29', '1', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('674', '2016-07-29周报-研发', '2016-07-29', '1', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('675', '2016-07-29日报-研发', '2016-07-29', '2', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('676', '2016-07-29周报-研发', '2016-07-29', '2', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('677', '2016-07-29日报-研发', '2016-07-29', '3', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('678', '2016-07-29周报-研发', '2016-07-29', '3', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('679', '2016-07-29日报-研发', '2016-07-29', '6', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('680', '2016-07-29周报-研发', '2016-07-29', '6', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('681', '2016-07-29日报-研发', '2016-07-29', '8', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('682', '2016-07-29周报-研发', '2016-07-29', '8', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('683', '2016-07-29日报-研发', '2016-07-29', '17', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('684', '2016-07-29周报-研发', '2016-07-29', '17', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('685', '2016-07-29日报-研发', '2016-07-29', '14', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('686', '2016-07-29周报-研发', '2016-07-29', '14', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('687', '2016-07-29日报-研发', '2016-07-29', '10', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('688', '2016-07-29周报-研发', '2016-07-29', '10', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('689', '2016-07-29日报-研发', '2016-07-29', '21', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('690', '2016-07-29周报-研发', '2016-07-29', '21', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('693', '2016-07-29日报-研发', '2016-07-29', '16', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('694', '2016-07-29周报-研发', '2016-07-29', '16', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('695', '2016-07-29日报-研发', '2016-07-29', '24', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('696', '2016-07-29周报-研发', '2016-07-29', '24', '2,3,6,8,17,14,10,21,4,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('697', '2016-08-05日报-研发', '2016-08-05', '2', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('698', '2016-08-05周报-研发', '2016-08-05', '2', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('699', '2016-08-05日报-研发', '2016-08-05', '6', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('700', '2016-08-05周报-研发', '2016-08-05', '6', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('701', '2016-08-05日报-研发', '2016-08-05', '8', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('702', '2016-08-05周报-研发', '2016-08-05', '8', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('703', '2016-08-05日报-研发', '2016-08-05', '17', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('704', '2016-08-05周报-研发', '2016-08-05', '17', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('705', '2016-08-05日报-研发', '2016-08-05', '14', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('706', '2016-08-05周报-研发', '2016-08-05', '14', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('707', '2016-08-05日报-研发', '2016-08-05', '10', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('708', '2016-08-05周报-研发', '2016-08-05', '10', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('709', '2016-08-05日报-研发', '2016-08-05', '21', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('710', '2016-08-05周报-研发', '2016-08-05', '21', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('711', '2016-08-05日报-研发', '2016-08-05', '16', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('712', '2016-08-05周报-研发', '2016-08-05', '16', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('713', '2016-08-05日报-研发', '2016-08-05', '24', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('714', '2016-08-05周报-研发', '2016-08-05', '24', '2,6,8,17,14,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('715', '2016-08-12日报-研发', '2016-08-12', '2', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('716', '2016-08-12周报-研发', '2016-08-12', '2', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('717', '2016-08-12日报-研发', '2016-08-12', '6', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('718', '2016-08-12周报-研发', '2016-08-12', '6', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('719', '2016-08-12日报-研发', '2016-08-12', '8', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('720', '2016-08-12周报-研发', '2016-08-12', '8', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('721', '2016-08-12日报-研发', '2016-08-12', '17', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('722', '2016-08-12周报-研发', '2016-08-12', '17', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('723', '2016-08-12日报-研发', '2016-08-12', '10', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('724', '2016-08-12周报-研发', '2016-08-12', '10', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('725', '2016-08-12日报-研发', '2016-08-12', '21', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('726', '2016-08-12周报-研发', '2016-08-12', '21', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('727', '2016-08-12日报-研发', '2016-08-12', '16', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('728', '2016-08-12周报-研发', '2016-08-12', '16', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('729', '2016-08-12日报-研发', '2016-08-12', '24', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('730', '2016-08-12周报-研发', '2016-08-12', '24', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('731', '2016-08-12日报-研发', '2016-08-12', '25', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('732', '2016-08-12周报-研发', '2016-08-12', '25', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('733', '2016-08-19日报-研发', '2016-08-19', '2', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('734', '2016-08-19周报-研发', '2016-08-19', '2', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('735', '2016-08-19日报-研发', '2016-08-19', '6', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('736', '2016-08-19周报-研发', '2016-08-19', '6', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('737', '2016-08-19日报-研发', '2016-08-19', '8', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('738', '2016-08-19周报-研发', '2016-08-19', '8', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('739', '2016-08-19日报-研发', '2016-08-19', '17', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('740', '2016-08-19周报-研发', '2016-08-19', '17', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('741', '2016-08-19日报-研发', '2016-08-19', '10', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('742', '2016-08-19周报-研发', '2016-08-19', '10', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('743', '2016-08-19日报-研发', '2016-08-19', '21', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('744', '2016-08-19周报-研发', '2016-08-19', '21', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('745', '2016-08-19日报-研发', '2016-08-19', '16', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('746', '2016-08-19周报-研发', '2016-08-19', '16', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('747', '2016-08-19日报-研发', '2016-08-19', '24', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('748', '2016-08-19周报-研发', '2016-08-19', '24', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('749', '2016-08-19日报-研发', '2016-08-19', '25', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('750', '2016-08-19周报-研发', '2016-08-19', '25', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('751', '2016-08-26日报-研发', '2016-08-26', '2', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('752', '2016-08-26周报-研发', '2016-08-26', '2', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('753', '2016-08-26日报-研发', '2016-08-26', '6', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('754', '2016-08-26周报-研发', '2016-08-26', '6', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('755', '2016-08-26日报-研发', '2016-08-26', '8', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('756', '2016-08-26周报-研发', '2016-08-26', '8', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('757', '2016-08-26日报-研发', '2016-08-26', '17', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('758', '2016-08-26周报-研发', '2016-08-26', '17', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('759', '2016-08-26日报-研发', '2016-08-26', '10', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('760', '2016-08-26周报-研发', '2016-08-26', '10', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('761', '2016-08-26日报-研发', '2016-08-26', '21', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('762', '2016-08-26周报-研发', '2016-08-26', '21', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('763', '2016-08-26日报-研发', '2016-08-26', '16', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('764', '2016-08-26周报-研发', '2016-08-26', '16', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('765', '2016-08-26日报-研发', '2016-08-26', '24', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('766', '2016-08-26周报-研发', '2016-08-26', '24', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('767', '2016-08-26日报-研发', '2016-08-26', '25', '2,6,8,17,10,21,16,24,25,');
INSERT INTO `busi_manager_work_report` VALUES ('768', '2016-08-26周报-研发', '2016-08-26', '25', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('769', '2016-09-02日报-研发', '2016-09-02', '2', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('770', '2016-09-02周报-研发', '2016-09-02', '2', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('771', '2016-09-02日报-研发', '2016-09-02', '6', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('772', '2016-09-02周报-研发', '2016-09-02', '6', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('773', '2016-09-02日报-研发', '2016-09-02', '8', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('774', '2016-09-02周报-研发', '2016-09-02', '8', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('775', '2016-09-02日报-研发', '2016-09-02', '17', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('776', '2016-09-02周报-研发', '2016-09-02', '17', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('777', '2016-09-02日报-研发', '2016-09-02', '10', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('778', '2016-09-02周报-研发', '2016-09-02', '10', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('779', '2016-09-02日报-研发', '2016-09-02', '25', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('780', '2016-09-02周报-研发', '2016-09-02', '25', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('781', '2016-09-02日报-研发', '2016-09-02', '21', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('782', '2016-09-02周报-研发', '2016-09-02', '21', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('783', '2016-09-02日报-研发', '2016-09-02', '16', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('784', '2016-09-02周报-研发', '2016-09-02', '16', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('785', '2016-09-02日报-研发', '2016-09-02', '24', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('786', '2016-09-02周报-研发', '2016-09-02', '24', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('787', '2016-09-09日报-研发', '2016-09-09', '2', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('788', '2016-09-09周报-研发', '2016-09-09', '2', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('789', '2016-09-09日报-研发', '2016-09-09', '6', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('790', '2016-09-09周报-研发', '2016-09-09', '6', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('791', '2016-09-09日报-研发', '2016-09-09', '8', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('792', '2016-09-09周报-研发', '2016-09-09', '8', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('793', '2016-09-09日报-研发', '2016-09-09', '17', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('794', '2016-09-09周报-研发', '2016-09-09', '17', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('795', '2016-09-09日报-研发', '2016-09-09', '10', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('796', '2016-09-09周报-研发', '2016-09-09', '10', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('797', '2016-09-09日报-研发', '2016-09-09', '25', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('798', '2016-09-09周报-研发', '2016-09-09', '25', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('799', '2016-09-09日报-研发', '2016-09-09', '21', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('800', '2016-09-09周报-研发', '2016-09-09', '21', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('801', '2016-09-09日报-研发', '2016-09-09', '16', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('802', '2016-09-09周报-研发', '2016-09-09', '16', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('803', '2016-09-09日报-研发', '2016-09-09', '24', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('804', '2016-09-09周报-研发', '2016-09-09', '24', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('805', '2016-09-18日报-研发', '2016-09-18', '2', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('806', '2016-09-18周报-研发', '2016-09-18', '2', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('807', '2016-09-18日报-研发', '2016-09-18', '6', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('808', '2016-09-18周报-研发', '2016-09-18', '6', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('809', '2016-09-18日报-研发', '2016-09-18', '8', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('810', '2016-09-18周报-研发', '2016-09-18', '8', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('811', '2016-09-18日报-研发', '2016-09-18', '17', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('812', '2016-09-18周报-研发', '2016-09-18', '17', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('813', '2016-09-18日报-研发', '2016-09-18', '10', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('814', '2016-09-18周报-研发', '2016-09-18', '10', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('815', '2016-09-18日报-研发', '2016-09-18', '25', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('816', '2016-09-18周报-研发', '2016-09-18', '25', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('817', '2016-09-18日报-研发', '2016-09-18', '21', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('818', '2016-09-18周报-研发', '2016-09-18', '21', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('819', '2016-09-18日报-研发', '2016-09-18', '16', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('820', '2016-09-18周报-研发', '2016-09-18', '16', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('821', '2016-09-18日报-研发', '2016-09-18', '24', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('822', '2016-09-18周报-研发', '2016-09-18', '24', '2,6,8,17,10,25,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('841', '2016-09-23日报-研发', '2016-09-23', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('842', '2016-09-23周报-研发', '2016-09-23', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('843', '2016-09-23日报-研发', '2016-09-23', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('844', '2016-09-23周报-研发', '2016-09-23', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('845', '2016-09-23日报-研发', '2016-09-23', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('846', '2016-09-23周报-研发', '2016-09-23', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('847', '2016-09-23日报-研发', '2016-09-23', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('848', '2016-09-23周报-研发', '2016-09-23', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('849', '2016-09-23日报-研发', '2016-09-23', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('850', '2016-09-23周报-研发', '2016-09-23', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('851', '2016-09-23日报-研发', '2016-09-23', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('852', '2016-09-23周报-研发', '2016-09-23', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('853', '2016-09-23日报-研发', '2016-09-23', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('854', '2016-09-23周报-研发', '2016-09-23', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('855', '2016-09-23日报-研发', '2016-09-23', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('856', '2016-09-23周报-研发', '2016-09-23', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('857', '2016-09-30日报-研发', '2016-09-30', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('858', '2016-09-30周报-研发', '2016-09-30', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('859', '2016-09-30日报-研发', '2016-09-30', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('860', '2016-09-30周报-研发', '2016-09-30', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('861', '2016-09-30日报-研发', '2016-09-30', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('862', '2016-09-30周报-研发', '2016-09-30', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('863', '2016-09-30日报-研发', '2016-09-30', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('864', '2016-09-30周报-研发', '2016-09-30', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('865', '2016-09-30日报-研发', '2016-09-30', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('866', '2016-09-30周报-研发', '2016-09-30', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('867', '2016-09-30日报-研发', '2016-09-30', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('868', '2016-09-30周报-研发', '2016-09-30', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('869', '2016-09-30日报-研发', '2016-09-30', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('870', '2016-09-30周报-研发', '2016-09-30', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('871', '2016-09-30日报-研发', '2016-09-30', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('872', '2016-09-30周报-研发', '2016-09-30', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('873', '2016-10-14日报-研发', '2016-10-14', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('874', '2016-10-14周报-研发', '2016-10-14', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('875', '2016-10-14日报-研发', '2016-10-14', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('876', '2016-10-14周报-研发', '2016-10-14', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('877', '2016-10-14日报-研发', '2016-10-14', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('878', '2016-10-14周报-研发', '2016-10-14', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('879', '2016-10-14日报-研发', '2016-10-14', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('880', '2016-10-14周报-研发', '2016-10-14', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('881', '2016-10-14日报-研发', '2016-10-14', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('882', '2016-10-14周报-研发', '2016-10-14', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('883', '2016-10-14日报-研发', '2016-10-14', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('884', '2016-10-14周报-研发', '2016-10-14', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('885', '2016-10-14日报-研发', '2016-10-14', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('886', '2016-10-14周报-研发', '2016-10-14', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('887', '2016-10-14日报-研发', '2016-10-14', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('888', '2016-10-14周报-研发', '2016-10-14', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('889', '2016-10-21日报-研发', '2016-10-21', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('890', '2016-10-21周报-研发', '2016-10-21', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('891', '2016-10-21日报-研发', '2016-10-21', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('892', '2016-10-21周报-研发', '2016-10-21', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('893', '2016-10-21日报-研发', '2016-10-21', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('894', '2016-10-21周报-研发', '2016-10-21', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('895', '2016-10-21日报-研发', '2016-10-21', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('896', '2016-10-21周报-研发', '2016-10-21', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('897', '2016-10-21日报-研发', '2016-10-21', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('898', '2016-10-21周报-研发', '2016-10-21', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('899', '2016-10-21日报-研发', '2016-10-21', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('900', '2016-10-21周报-研发', '2016-10-21', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('901', '2016-10-21日报-研发', '2016-10-21', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('902', '2016-10-21周报-研发', '2016-10-21', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('903', '2016-10-21日报-研发', '2016-10-21', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('904', '2016-10-21周报-研发', '2016-10-21', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('905', '2016-10-28日报-研发', '2016-10-28', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('906', '2016-10-28周报-研发', '2016-10-28', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('907', '2016-10-28日报-研发', '2016-10-28', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('908', '2016-10-28周报-研发', '2016-10-28', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('909', '2016-10-28日报-研发', '2016-10-28', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('910', '2016-10-28周报-研发', '2016-10-28', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('911', '2016-10-28日报-研发', '2016-10-28', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('912', '2016-10-28周报-研发', '2016-10-28', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('913', '2016-10-28日报-研发', '2016-10-28', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('914', '2016-10-28周报-研发', '2016-10-28', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('915', '2016-10-28日报-研发', '2016-10-28', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('916', '2016-10-28周报-研发', '2016-10-28', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('917', '2016-10-28日报-研发', '2016-10-28', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('918', '2016-10-28周报-研发', '2016-10-28', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('919', '2016-10-28日报-研发', '2016-10-28', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('920', '2016-10-28周报-研发', '2016-10-28', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('921', '2016-11-04日报-研发', '2016-11-04', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('922', '2016-11-04周报-研发', '2016-11-04', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('923', '2016-11-04日报-研发', '2016-11-04', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('924', '2016-11-04周报-研发', '2016-11-04', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('925', '2016-11-04日报-研发', '2016-11-04', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('926', '2016-11-04周报-研发', '2016-11-04', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('927', '2016-11-04日报-研发', '2016-11-04', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('928', '2016-11-04周报-研发', '2016-11-04', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('929', '2016-11-04日报-研发', '2016-11-04', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('930', '2016-11-04周报-研发', '2016-11-04', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('931', '2016-11-04日报-研发', '2016-11-04', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('932', '2016-11-04周报-研发', '2016-11-04', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('933', '2016-11-04日报-研发', '2016-11-04', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('934', '2016-11-04周报-研发', '2016-11-04', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('935', '2016-11-04日报-研发', '2016-11-04', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('936', '2016-11-04周报-研发', '2016-11-04', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('937', '2016-12-09日报-研发', '2016-12-09', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('938', '2016-12-09周报-研发', '2016-12-09', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('939', '2016-12-09日报-研发', '2016-12-09', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('940', '2016-12-09周报-研发', '2016-12-09', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('941', '2016-12-09日报-研发', '2016-12-09', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('942', '2016-12-09周报-研发', '2016-12-09', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('943', '2016-12-09日报-研发', '2016-12-09', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('944', '2016-12-09周报-研发', '2016-12-09', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('945', '2016-12-09日报-研发', '2016-12-09', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('946', '2016-12-09周报-研发', '2016-12-09', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('947', '2016-12-09日报-研发', '2016-12-09', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('948', '2016-12-09周报-研发', '2016-12-09', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('949', '2016-12-09日报-研发', '2016-12-09', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('950', '2016-12-09周报-研发', '2016-12-09', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('951', '2016-12-09日报-研发', '2016-12-09', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('952', '2016-12-09周报-研发', '2016-12-09', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('953', '2016-12-12日报-研发', '2016-12-12', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('954', '2016-12-12日报-研发', '2016-12-12', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('955', '2016-12-12日报-研发', '2016-12-12', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('956', '2016-12-12日报-研发', '2016-12-12', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('957', '2016-12-12日报-研发', '2016-12-12', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('958', '2016-12-12日报-研发', '2016-12-12', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('959', '2016-12-12日报-研发', '2016-12-12', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('960', '2016-12-12日报-研发', '2016-12-12', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('961', '2016-12-13日报-研发', '2016-12-13', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('962', '2016-12-13日报-研发', '2016-12-13', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('963', '2016-12-13日报-研发', '2016-12-13', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('964', '2016-12-13日报-研发', '2016-12-13', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('965', '2016-12-13日报-研发', '2016-12-13', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('966', '2016-12-13日报-研发', '2016-12-13', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('967', '2016-12-13日报-研发', '2016-12-13', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('968', '2016-12-13日报-研发', '2016-12-13', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('969', '2016-12-14日报-研发', '2016-12-14', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('970', '2016-12-14日报-研发', '2016-12-14', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('971', '2016-12-14日报-研发', '2016-12-14', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('972', '2016-12-14日报-研发', '2016-12-14', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('973', '2016-12-14日报-研发', '2016-12-14', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('974', '2016-12-14日报-研发', '2016-12-14', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('975', '2016-12-14日报-研发', '2016-12-14', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('976', '2016-12-14日报-研发', '2016-12-14', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('977', '2016-12-16日报-研发', '2016-12-16', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('978', '2016-12-16周报-研发', '2016-12-16', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('979', '2016-12-16日报-研发', '2016-12-16', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('980', '2016-12-16周报-研发', '2016-12-16', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('981', '2016-12-16日报-研发', '2016-12-16', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('982', '2016-12-16周报-研发', '2016-12-16', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('983', '2016-12-16日报-研发', '2016-12-16', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('984', '2016-12-16周报-研发', '2016-12-16', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('985', '2016-12-16日报-研发', '2016-12-16', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('986', '2016-12-16周报-研发', '2016-12-16', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('987', '2016-12-16日报-研发', '2016-12-16', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('988', '2016-12-16周报-研发', '2016-12-16', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('989', '2016-12-16日报-研发', '2016-12-16', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('990', '2016-12-16周报-研发', '2016-12-16', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('991', '2016-12-16日报-研发', '2016-12-16', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('992', '2016-12-16周报-研发', '2016-12-16', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('993', '2016-12-19日报-研发', '2016-12-19', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('994', '2016-12-19日报-研发', '2016-12-19', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('995', '2016-12-19日报-研发', '2016-12-19', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('996', '2016-12-19日报-研发', '2016-12-19', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('997', '2016-12-19日报-研发', '2016-12-19', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('998', '2016-12-19日报-研发', '2016-12-19', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('999', '2016-12-19日报-研发', '2016-12-19', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1000', '2016-12-19日报-研发', '2016-12-19', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1001', '2016-12-23日报-研发', '2016-12-23', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1002', '2016-12-23周报-研发', '2016-12-23', '2', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1003', '2016-12-23日报-研发', '2016-12-23', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1004', '2016-12-23周报-研发', '2016-12-23', '6', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1005', '2016-12-23日报-研发', '2016-12-23', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1006', '2016-12-23周报-研发', '2016-12-23', '8', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1007', '2016-12-23日报-研发', '2016-12-23', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1008', '2016-12-23周报-研发', '2016-12-23', '17', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1009', '2016-12-23日报-研发', '2016-12-23', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1010', '2016-12-23周报-研发', '2016-12-23', '10', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1011', '2016-12-23日报-研发', '2016-12-23', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1012', '2016-12-23周报-研发', '2016-12-23', '21', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1013', '2016-12-23日报-研发', '2016-12-23', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1014', '2016-12-23周报-研发', '2016-12-23', '16', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1015', '2016-12-23日报-研发', '2016-12-23', '24', '2,6,8,17,10,21,16,24,');
INSERT INTO `busi_manager_work_report` VALUES ('1016', '2016-12-23周报-研发', '2016-12-23', '24', '2,6,8,17,10,21,16,24,');

-- ----------------------------
-- Table structure for busi_work_report
-- ----------------------------
DROP TABLE IF EXISTS `busi_work_report`;
CREATE TABLE `busi_work_report` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_work_report_name` varchar(50) DEFAULT NULL,
  `d_create_time` varchar(50) DEFAULT NULL,
  `d_send_time` varchar(50) DEFAULT NULL,
  `i_user_id` int(11) DEFAULT NULL,
  `c_status` int(11) DEFAULT NULL,
  `c_memo` varchar(50) DEFAULT NULL,
  `c_report_code` varchar(50) DEFAULT NULL,
  `c_user_name` varchar(50) DEFAULT NULL,
  `c_org_name` varchar(50) DEFAULT NULL,
  `c_postion` varchar(50) DEFAULT NULL,
  `c_type` varchar(10) NOT NULL DEFAULT 'day',
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of busi_work_report
-- ----------------------------
INSERT INTO `busi_work_report` VALUES ('2', '2016-05-04日报', '2016-05-04 09:01:30', null, '2', null, null, 'beb26b06-5c74-8ac9-1a8c-823af26ec694', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('3', '2016-05-06周报', '2016-05-06 13:56:57', null, '21', null, null, '87ac96e5-7e14-8909-2284-3f462dded64d', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('4', '2016-05-06周报', '2016-05-06 13:57:36', null, '2', null, null, '6df559d0-95a0-2375-6370-63e18c09bd58', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('5', '2016-05-06周报', '2016-05-06 13:57:58', null, '10', null, null, '7c99af89-b52d-0792-1c47-9f98cf922e85', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('6', '2016-05-06周报', '2016-05-06 14:02:00', null, '14', null, null, '9acb8aac-c24a-d72b-9b40-ed95f889ba0e', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('7', '2016-05-06周报', '2016-05-06 14:03:57', null, '18', null, null, '8e564912-9e1e-8ce8-5574-bbee73aa7cdf', '彭sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('8', '2016-05-06周报', '2016-05-06 14:19:14', null, '8', null, null, '9c2ffbba-c636-d3ec-d463-4198f09afffd', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('9', '2016-05-06周报', '2016-05-06 14:44:30', null, '4', null, null, '519471ff-d3eb-8473-c904-a58e6dcb0687', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('10', '2016-05-06周报', '2016-05-06 14:57:09', null, '6', null, null, '92f3c223-d8f8-c9cc-b927-d8e0b6356a51', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('11', '2016-05-06周报', '2016-05-06 15:07:40', null, '17', null, null, '92555c50-643d-b405-1950-11e6e7cf25e3', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('12', '2016-05-06周报', '2016-05-06 15:10:56', null, '16', null, null, '0e2ec0cd-6588-5e91-74e1-644c3da80a55', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('13', '2016-05-09日报', '2016-05-09 17:01:40', null, '2', null, null, 'c1fc49c8-994e-47d0-e58d-4457668997ab', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('14', '2016-05-10周报', '2016-05-10 10:32:49', null, '2', null, null, '01b7f087-447a-0a98-3027-5dac55275a87', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('15', '2016-05-13周报', '2016-05-13 09:28:56', null, '10', null, null, 'c1449fae-c3bd-5e9e-b128-c0f8f6ea950c', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('16', '2016-05-13周报', '2016-05-13 14:16:49', null, '16', null, null, '1e6075ff-f1d9-1989-783e-775b24888225', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('17', '2016-05-13周报', '2016-05-13 16:04:22', null, '4', null, null, '5942fe7d-998b-d05d-1135-87905c776429', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('18', '2016-05-13周报', '2016-05-13 16:18:04', null, '18', null, null, 'e51ee87c-746d-743d-87d6-c241fd7613a6', '彭sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('19', '2016-05-13周报', '2016-05-13 16:35:07', null, '6', null, null, 'a850c1ba-5ea7-16ea-234b-e32bdd316590', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('20', '2016-05-13周报', '2016-05-13 17:26:17', null, '17', null, null, '0c73e4f2-69d2-ae66-1f55-f2e5f6a0af8e', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('21', '2016-05-13周报', '2016-05-13 17:29:04', null, '8', null, null, '63d6bfd3-d774-f433-431a-217632ad9d6e', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('22', '2016-05-13周报', '2016-05-13 11:34:46', null, '2', null, null, '3780fd15-fb8c-aa54-4c66-fe22e79fd5a2', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('23', '2016-05-13周报', '2016-05-13 12:17:20', null, '14', null, null, '21139ded-b2ef-6824-c8b2-0522b7d3b013', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('24', '2016-05-13周报', '2016-05-13 12:20:59', null, '21', null, null, '8f28aa65-fcc8-7cd0-c6eb-dd502f194fa1', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('25', '2016-05-16日报', '2016-05-16 09:27:53', null, '2', null, null, 'f86c7064-e750-634b-6bdd-e247c22e4204', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('26', '2016-05-17日报', '2016-05-17 09:36:32', null, '2', null, null, '57b40ebb-8e7b-dee7-1874-772bfd342520', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('27', '2016-05-20周报', '2016-05-20 12:18:25', null, '14', null, null, '07343625-b1b2-4a8a-affb-b30dd8e234f3', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('28', '2016-05-20周报', '2016-05-20 12:21:03', null, '21', null, null, '057b8a4d-4e4a-0920-010a-c5a65efcea76', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('29', '2016-05-20周报', '2016-05-20 12:23:08', null, '8', null, null, '4033d779-0ff7-5fec-a7dd-b2a059f27fc1', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('30', '2016-05-20周报', '2016-05-20 12:27:08', null, '2', null, null, 'ea5fc7f7-b69c-560b-2cf7-f3ce22e20c2f', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('31', '2016-05-20周报', '2016-05-20 12:29:14', null, '17', null, null, '2286e697-5d34-eef6-b9bc-cfb820f2b2b8', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('32', '2016-05-20周报', '2016-05-20 12:40:45', null, '6', null, null, 'f63eeb45-58b9-8b4d-4c88-3a20e07eee11', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('33', '2016-05-20周报', '2016-05-20 12:55:40', null, '18', null, null, '3e856795-6c77-376c-07a4-06f3027f1d47', '彭sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('34', '2016-05-20周报', '2016-05-20 13:07:46', null, '16', null, null, '57f9702d-06e1-4460-45ae-6a9fb7c2d3a8', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('35', '2016-05-20周报', '2016-05-20 13:13:13', null, '4', null, null, '3e46a022-015c-97d5-546d-0931c87a33b7', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('36', '2016-05-20周报', '2016-05-20 13:32:19', null, '10', null, null, '95553e9f-0e03-bbc0-dd10-63968da90d82', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('38', '2016-05-27周报', '2016-05-27 15:03:02', null, '10', null, null, '14af8ce8-c730-906c-e6fd-e93996435fd3', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('39', '2016-05-27周报', '2016-05-27 15:05:17', null, '14', null, null, '98be8a69-f0d7-0399-e500-1807523eaa5b', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('40', '2016-05-27周报', '2016-05-27 15:06:40', null, '16', null, null, '3a6d4302-7ada-2d35-7e1a-92f2cb5f70c3', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('41', '2016-05-27周报', '2016-05-27 15:10:48', null, '8', null, null, '2052de05-3662-c0db-9b31-6c46447cd8e7', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('42', '2016-05-27周报', '2016-05-27 15:13:06', null, '2', null, null, 'fea18a1a-b939-527c-8c3d-06e747d7aa48', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('43', '2016-05-27周报', '2016-05-27 15:16:17', null, '17', null, null, 'cd7925af-8c87-67fb-481e-f0b637a6ef8d', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('44', '2016-05-27周报', '2016-05-27 15:19:06', null, '4', null, null, 'c21aa095-9308-e3c0-760f-880577905e4d', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('45', '2016-05-27周报', '2016-05-27 15:20:37', null, '18', null, null, '3e498e80-f829-fdcc-2ffe-64bdff8d6dce', '彭sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('46', '2016-05-27周报', '2016-05-27 15:24:26', null, '6', null, null, '8097126b-c619-0573-e8cf-ca26c2295089', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('47', '2016-05-27周报', '2016-05-27 15:42:50', null, '21', null, null, 'b111c10e-e578-94ec-796e-6b75c8dda237', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('48', '2016-06-03周报', '2016-06-03 12:52:07', null, '16', null, null, 'b9b4d748-e41e-d8b5-5031-85449d9aefe4', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('49', '2016-06-03周报', '2016-06-03 12:56:28', null, '2', null, null, '11bfea93-020b-2343-795a-ca5f10346605', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('50', '2016-06-03周报', '2016-06-03 12:57:20', null, '8', null, null, '81ec1482-ecc5-1c61-a2d8-88d1266dd2d5', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('51', '2016-06-03周报', '2016-06-03 13:00:31', null, '14', null, null, '37b64d5d-419d-91ac-1e3f-68e48e37a411', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('52', '2016-06-03周报', '2016-06-03 13:03:45', null, '18', null, null, '2388651e-35ca-fd1a-ac55-b38f04d29d08', '彭sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('53', '2016-06-03周报', '2016-06-03 13:11:23', null, '4', null, null, 'f46fd507-b664-0035-0edc-ce0d776b8185', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('54', '2016-06-03周报', '2016-06-03 13:11:31', null, '17', null, null, 'e391308b-7c55-a97c-264a-159feeda3871', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('55', '2016-06-03周报', '2016-06-03 13:23:04', null, '21', null, null, '7167ad46-3894-7f3e-d218-6246cf9f68b2', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('56', '2016-06-03周报', '2016-06-03 13:25:40', null, '6', null, null, 'e1a5d50a-2bb2-37cb-74ec-079c7804c2c6', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('57', '2016-06-03周报', '2016-06-03 13:32:25', null, '10', null, null, 'dbeed7f6-e7c1-7abc-5fb0-16fe2f303325', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('58', '2016-06-12周报', '2016-06-12 10:59:01', null, '14', null, null, '7900db9c-ad79-be14-80d6-7647ee2466f1', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('59', '2016-06-12周报', '2016-06-12 11:06:37', null, '10', null, null, 'b22d4049-cd22-8630-e2f2-9f4194ec3639', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('60', '2016-06-12周报', '2016-06-12 11:09:28', null, '17', null, null, 'dd57af48-7507-28d1-3337-7faa685a3b62', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('61', '2016-06-12周报', '2016-06-12 11:15:08', null, '8', null, null, 'ca0604e5-f554-c39a-49fa-d6bf5d316bdc', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('62', '2016-06-12周报', '2016-06-12 12:30:30', null, '2', null, null, '15e266fd-74bb-66af-194f-96323c724e4e', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('63', '2016-06-12周报', '2016-06-12 12:45:38', null, '16', null, null, '3248f6aa-0804-7d76-ad69-7d0df10b6915', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('64', '2016-06-12周报', '2016-06-12 12:48:26', null, '6', null, null, '5b09fde9-cd02-fac5-65da-854f52baf1d8', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('65', '2016-06-12周报', '2016-06-12 13:30:25', null, '4', null, null, '8ab9a34a-afbb-afa4-7abb-736eb1c6b180', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('66', '2016-06-12周报', '2016-06-12 17:02:22', null, '18', null, null, '050bc35f-d7fd-d521-4ad8-bf624abb2c37', '彭sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('67', '2016-06-17周报', '2016-06-17 12:44:28', null, '3', null, null, '215ccc2e-3cc6-2c72-c420-9898199dee7f', '夏sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('68', '2016-06-17周报', '2016-06-17 12:48:49', null, '10', null, null, 'ea7c9d40-97c6-23b9-778e-e0c26cb04517', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('69', '2016-06-17周报', '2016-06-17 13:10:04', null, '17', null, null, '21f38525-77a6-d1f5-f630-0b313b28fbe4', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('70', '2016-06-17周报', '2016-06-17 13:15:03', null, '14', null, null, 'f127d079-9549-0f84-6dbd-ac416c101eeb', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('71', '2016-06-17周报', '2016-06-17 14:48:46', null, '2', null, null, '1d327954-2220-8b21-d791-6c945ae610b3', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('72', '2016-06-17周报', '2016-06-17 14:50:51', null, '6', null, null, 'bd8cd283-68bd-f0df-e322-f926b0699de2', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('73', '2016-06-17周报', '2016-06-17 14:59:54', null, '21', null, null, '38286ba3-e77d-c329-98c5-28aaaada4c31', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('74', '2016-06-17周报', '2016-06-17 15:10:40', null, '4', null, null, '2418e300-0241-84f0-c004-da902f1fcd42', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('75', '2016-06-17周报', '2016-06-17 17:05:50', null, '8', null, null, '08a69432-8088-74d0-dac9-2b8fdad06760', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('76', '2016-06-24周报', '2016-06-24 13:50:55', null, '4', null, null, '44b6c9fa-cdbc-da47-9f39-454af513ccf2', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('77', '2016-06-24周报', '2016-06-24 14:26:56', null, '2', null, null, 'b5e24278-e5bc-15c6-cb35-aa0c8e397a7a', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('78', '2016-06-24周报', '2016-06-24 14:27:52', null, '16', null, null, 'b8d32a13-8d56-1b65-b39c-28d33a36bc6b', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('79', '2016-06-24周报', '2016-06-24 14:31:06', null, '10', null, null, '51c87f75-7af9-20b1-64b7-ba4cf4a3faea', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('80', '2016-06-24周报', '2016-06-24 14:32:12', null, '17', null, null, '55f1a369-3327-28e9-9830-dbe6077b0c8b', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('81', '2016-06-24周报', '2016-06-24 14:34:59', null, '8', null, null, '2e7688e8-edb3-cf87-b2fa-c34e90672653', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('82', '2016-06-24周报', '2016-06-24 14:38:32', null, '14', null, null, '481fd16f-625c-2739-bfe2-b56c31050a76', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('83', '2016-06-24周报', '2016-06-24 14:45:36', null, '6', null, null, '84570a62-6399-ffaf-ddd2-69ea4fe964f5', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('84', '2016-06-24周报', '2016-06-24 15:01:07', null, '21', null, null, 'ae68ea4a-c7d2-a556-678d-7ec69b4492ee', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('85', '2016-07-01周报', '2016-07-01 15:05:07', null, '4', null, null, '333572cf-14fd-f5bd-8b74-697c855f08f6', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('86', '2016-07-01周报', '2016-07-01 15:10:50', null, '16', null, null, '83277caf-5ea6-b83d-d51c-41a727900826', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('87', '2016-07-01周报', '2016-07-01 15:11:07', null, '2', null, null, '34b28c7f-424c-5d9f-e25d-be78799af53f', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('88', '2016-07-01周报', '2016-07-01 15:12:57', null, '8', null, null, '48aaac81-928c-532d-5f63-0f4d5d9f4b80', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('89', '2016-07-01周报', '2016-07-01 15:14:03', null, '10', null, null, '8a3a72fc-b7bc-701b-3d7a-a91cc38607ec', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('90', '2016-07-01周报', '2016-07-01 15:20:45', null, '17', null, null, '3f4e10c4-f4d4-4d55-fe41-f266202bfcc1', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('91', '2016-07-01周报', '2016-07-01 15:32:16', null, '6', null, null, '16482650-7f8c-fb8d-da55-90bbfa1e4a2a', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('92', '2016-07-01周报', '2016-07-01 15:34:18', null, '21', null, null, '1e2c89a0-5468-d75a-b601-299e3d549065', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('93', '2016-07-01周报', '2016-07-01 15:38:27', null, '14', null, null, '93265a14-b0f7-0d5b-cb11-5172a8c0c5ea', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('94', '2016-07-08周报', '2016-07-08 12:46:09', null, '8', null, null, '5366c3d9-41af-99bb-95a1-13533c25add0', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('95', '2016-07-08周报', '2016-07-08 13:45:58', null, '17', null, null, 'ea0885e1-4443-2868-1f77-5d764a2e27e3', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('96', '2016-07-08周报', '2016-07-08 14:00:50', null, '16', null, null, '654f40a5-a452-d045-831d-99e81a482369', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('97', '2016-07-08周报', '2016-07-08 14:10:33', null, '10', null, null, '7f519e53-670e-bed9-08e3-b739ab9635be', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('98', '2016-07-08周报', '2016-07-08 14:13:56', null, '6', null, null, '79a80034-ed81-f793-046a-6a7f8513027d', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('99', '2016-07-08周报', '2016-07-08 14:53:59', null, '4', null, null, '8175f8c4-84ad-1775-25d3-533d600dd893', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('100', '2016-07-08周报', '2016-07-08 15:26:25', null, '2', null, null, '52392445-6d9a-279c-9343-4cc780d93d81', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('101', '2016-07-08周报', '2016-07-08 15:38:24', null, '14', null, null, '51ea720b-6e3e-764e-03ec-6d509782844f', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('102', '2016-07-08周报', '2016-07-08 15:40:03', null, '21', null, null, '2ea2ff1a-c587-ee73-c216-156b9581c0ec', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('103', '2016-07-15周报', '2016-07-15 12:37:01', null, '8', null, null, '8e67b994-6433-7e82-3c32-86b033f52608', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('104', '2016-07-15周报', '2016-07-15 12:48:03', null, '4', null, null, 'f9ba9d94-bb11-7c9b-0261-76c7f54adfdd', '林姐', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('105', '2016-07-15周报', '2016-07-15 12:53:26', null, '16', null, null, '07906c43-970d-27d4-765a-1eb9823afa05', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('106', '2016-07-15周报', '2016-07-15 12:53:42', null, '14', null, null, '29b5f634-5f15-8be5-aa0c-39927ee4d757', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('107', '2016-07-15周报', '2016-07-15 13:01:37', null, '17', null, null, '7bf69b33-c99b-a47e-a268-b78cc47112a8', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('108', '2016-07-15周报', '2016-07-15 13:06:44', null, '2', null, null, '1eb041f7-029a-b54f-378f-a6684c96e9d9', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('109', '2016-07-15周报', '2016-07-15 13:15:45', null, '6', null, null, '196a3669-44f2-a056-e988-edd36e29bbf2', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('110', '2016-07-15周报', '2016-07-15 14:18:05', null, '10', null, null, 'fedf5e14-2182-aa74-8802-3bbb237e7538', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('111', '2016-07-15周报', '2016-07-15 14:23:58', null, '21', null, null, '9b59f1c0-2358-5135-1797-5c55b76c9bfc', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('112', '2016-07-22周报', '2016-07-22 13:01:04', null, '8', null, null, 'd6624aac-afe5-4131-2e07-966b05b92afb', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('113', '2016-07-22周报', '2016-07-22 13:03:46', null, '14', null, null, '8d2bc31b-d008-2a1e-6264-ec7d7f0df0ad', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('114', '2016-07-22周报', '2016-07-22 13:10:46', null, '16', null, null, '9b9d187e-0b29-83c9-be8d-6e6fc45b8624', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('115', '2016-07-22周报', '2016-07-22 13:27:35', null, '17', null, null, '6ca8844e-4b68-8cb1-1204-f936be2840f2', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('116', '2016-07-22周报', '2016-07-22 13:37:11', null, '2', null, null, 'bda9825e-a4d8-530e-d585-e0dcd08562a1', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('117', '2016-07-22周报', '2016-07-22 13:50:09', null, '6', null, null, 'fb231fc0-24d8-3041-855a-1b6d2c509330', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('118', '2016-07-22周报', '2016-07-22 14:11:32', null, '24', null, null, '2a38ed1b-9c88-f339-7bba-586aa6c84e5c', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('119', '2016-07-22周报', '2016-07-22 14:43:18', null, '21', null, null, '4699ffcb-5c60-7e6d-088a-75214094ba9a', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('120', '2016-07-29周报', '2016-07-29 12:44:05', null, '16', null, null, 'e8bd5723-4c2e-710a-f99a-5ae710e9bc57', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('121', '2016-07-29周报', '2016-07-29 12:45:32', null, '14', null, null, '909200b9-3d9e-8c28-dc73-ba4b9ba9baad', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('122', '2016-07-29周报', '2016-07-29 12:49:39', null, '8', null, null, '8a529327-7ff5-e1e3-81a0-801074be3b56', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('123', '2016-07-29周报', '2016-07-29 12:57:38', null, '2', null, null, '43af7393-3c7d-21dd-0ce3-f94843908d02', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('124', '2016-07-29周报', '2016-07-29 12:58:34', null, '17', null, null, 'cba12c76-d6b9-a8f0-a097-b08e3d327673', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('125', '2016-07-29周报', '2016-07-29 13:02:26', null, '6', null, null, '4624861f-5315-2d87-e32f-df0f499457e7', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('126', '2016-07-29周报', '2016-07-29 13:07:00', null, '10', null, null, 'c8ee6191-7a2e-4443-a84e-41a4ff4e2bf5', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('127', '2016-07-29周报', '2016-07-29 13:50:05', null, '24', null, null, '4751f70a-3ab2-f31f-7223-50e428e4d9e4', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('128', '2016-07-29周报', '2016-07-29 14:59:49', null, '21', null, null, 'b4766a98-1a6d-e3ac-86b4-5e829a8f2878', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('129', '2016-08-05周报', '2016-08-05 13:35:48', null, '14', null, null, '88724137-e67a-cdfb-a459-327c2fa29c89', '章sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('130', '2016-08-05周报', '2016-08-05 13:37:40', null, '21', null, null, '7112a741-cfea-38a2-ab21-90594c76fef3', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('131', '2016-08-05周报', '2016-08-05 13:37:58', null, '8', null, null, 'ac337ccc-aa1f-40f7-a7d5-a46e6ef1291e', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('132', '2016-08-05周报', '2016-08-05 13:38:27', null, '24', null, null, '7b598070-d177-87b3-b548-e9a2c1084755', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('133', '2016-08-05周报', '2016-08-05 13:39:40', null, '16', null, null, '3eb0f500-850c-8757-270f-7e5bc8879178', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('134', '2016-08-05周报', '2016-08-05 13:40:15', null, '17', null, null, 'b66d934e-f1b4-0175-c65b-db21cfede21f', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('135', '2016-08-05周报', '2016-08-05 13:40:39', null, '10', null, null, 'bb3b3dc4-4d5c-d23a-a087-ae76812c8a77', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('136', '2016-08-05周报', '2016-08-05 13:53:35', null, '6', null, null, '74d49c5d-5de7-8569-dea7-e291a8073d4c', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('137', '2016-08-05周报', '2016-08-05 13:55:55', null, '2', null, null, '630b092a-9eee-bbc3-a6b7-174dbf66f04d', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('138', '2016-08-12周报', '2016-08-12 13:42:40', null, '16', null, null, 'c0fdc8a3-a180-dc68-d4ff-30db2eabf005', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('139', '2016-08-12周报', '2016-08-12 13:44:11', null, '8', null, null, 'd2031b14-e7cd-f2c5-3921-42b3582a1fe4', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('140', '2016-08-12周报', '2016-08-12 14:03:48', null, '17', null, null, 'f4ac2cfe-0ed4-5aab-5b8e-e3f5e4df361f', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('141', '2016-08-12周报', '2016-08-12 14:07:31', null, '24', null, null, '2e8c8405-180e-d117-e6c8-4469dff47ae1', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('142', '2016-08-12周报', '2016-08-12 14:12:09', null, '6', null, null, '991d0668-be59-d8ef-f494-a9332c282870', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('143', '2016-08-12周报', '2016-08-12 14:22:34', null, '21', null, null, '72dd3fd2-9362-df78-16fc-2d57b7edd209', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('144', '2016-08-12周报', '2016-08-12 14:25:53', null, '2', null, null, '516a51de-ea46-c73d-291f-b6e6f26de80d', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('145', '2016-08-12周报', '2016-08-12 15:47:01', null, '10', null, null, 'a6e72a10-2ef2-4d8e-fc2a-d429effb98aa', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('146', '2016-08-12周报', '2016-08-12 15:55:03', null, '25', null, null, '61dd6ca0-f46f-bec5-72c0-f629baf6b602', '王sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('147', '2016-08-19周报', '2016-08-19 13:28:47', null, '16', null, null, '8656b230-b979-ea82-b771-138e3f185a7a', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('148', '2016-08-19周报', '2016-08-19 13:36:44', null, '8', null, null, '5222681c-d2b9-d245-cb2a-2938e6aa3042', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('149', '2016-08-19周报', '2016-08-19 13:41:00', null, '2', null, null, 'be301ab6-38d0-1aa2-ddac-fde6a3df6761', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('150', '2016-08-19周报', '2016-08-19 13:44:27', null, '24', null, null, '683967f4-5bcd-47bd-9486-9479c5595e08', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('151', '2016-08-19周报', '2016-08-19 13:52:45', null, '17', null, null, '9b6af913-1fb4-3310-d402-5bb6da82551b', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('152', '2016-08-19周报', '2016-08-19 13:53:35', null, '6', null, null, 'c27d45bd-1db3-c34f-3cc5-86df06e9f1b7', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('153', '2016-08-19周报', '2016-08-19 14:27:03', null, '10', null, null, '02b76556-0b95-124f-5e89-916210e3e4a1', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('154', '2016-08-19日报', '2016-08-19 15:03:46', null, '25', null, null, '94aca2c8-ba08-6bba-4b2b-f18cffb1fd74', '王sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('157', '2016-08-19周报', '2016-08-19 15:32:40', null, '25', null, null, '3ca32c59-a7b0-9edc-e24d-388e28c59e61', '王sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('158', '2016-08-26周报', '2016-08-26 12:53:56', null, '17', null, null, 'd4f127e4-ff5b-8ee1-0db7-1ebe823c8974', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('159', '2016-08-26周报', '2016-08-26 13:00:44', null, '25', null, null, 'e5dbc744-6f81-d99b-ce10-22fc18e88099', '王sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('160', '2016-08-26周报', '2016-08-26 13:03:45', null, '2', null, null, '3efeaf3b-3dff-1f49-f89d-8f9fdab330f0', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('161', '2016-08-26周报', '2016-08-26 13:04:43', null, '10', null, null, 'bb994556-2301-76d7-a345-27e2eb0964ce', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('162', '2016-08-26周报', '2016-08-26 13:06:22', null, '6', null, null, 'c646b7f1-d5f5-a041-a308-632e8d554854', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('163', '2016-08-26周报', '2016-08-26 13:10:13', null, '16', null, null, 'b613a49c-3833-f1c1-9597-d459a9c76099', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('164', '2016-08-26周报', '2016-08-26 13:19:54', null, '21', null, null, 'c353e072-e103-52bc-96b4-99af8a36ec0c', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('165', '2016-08-26周报', '2016-08-26 13:36:45', null, '8', null, null, '176fb5a5-0244-ab50-e8a6-b82570cc1216', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('166', '2016-08-26周报', '2016-08-26 13:50:07', null, '24', null, null, 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('167', '2016-09-02周报', '2016-09-02 10:48:41', null, '16', null, null, '72bcd514-f3cd-572d-9a02-eaa4e37098bb', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('168', '2016-09-02周报', '2016-09-02 10:52:53', null, '17', null, null, 'fd315549-4366-687c-cbd7-0ebce3412ffb', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('169', '2016-09-02周报', '2016-09-02 10:54:36', null, '2', null, null, 'd56f958a-f62c-bdeb-967e-2534c761e43b', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('170', '2016-09-02周报', '2016-09-02 10:56:15', null, '8', null, null, 'd4226435-5785-ff89-74a8-9e0f4443c017', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('171', '2016-09-02周报', '2016-09-02 10:59:56', null, '6', null, null, '070d4224-2ddb-33a9-0084-4093d21d5059', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('172', '2016-09-02周报', '2016-09-02 11:07:36', null, '21', null, null, 'f0add40f-6bce-5b91-5c82-2815a6ae3b7c', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('173', '2016-09-02周报', '2016-09-02 11:12:58', null, '10', null, null, '9e65cd70-676e-5b43-3a45-d5e05998cb77', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('174', '2016-09-02周报', '2016-09-02 11:15:08', null, '25', null, null, '1a99a80a-26c3-42bb-decb-381d4d4dbb03', '王sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('175', '2016-09-02周报', '2016-09-02 12:44:51', null, '24', null, null, 'fb16052d-a386-6cb8-76ad-59cbf63e4b4a', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('176', '2016-09-09周报', '2016-09-09 13:11:34', null, '16', null, null, '71b8d79b-a548-7c22-34c0-c44b2f8f7757', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('177', '2016-09-09周报', '2016-09-09 13:16:01', null, '17', null, null, '0e89d21d-ec11-ebe5-cf53-6559a675e027', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('178', '2016-09-09周报', '2016-09-09 13:27:44', null, '25', null, null, '5683591b-f2d6-15bf-e45e-7fa2b6b51e07', '王sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('179', '2016-09-09周报', '2016-09-09 13:28:01', null, '8', null, null, '89beab77-12f7-436f-4e1d-e45df4750eef', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('180', '2016-09-09周报', '2016-09-09 13:30:13', null, '21', null, null, '6e9a8e92-ba3e-3039-1393-57be9874a2ab', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('181', '2016-09-09周报', '2016-09-09 13:37:29', null, '24', null, null, '9379bfc7-81e8-f9bf-ad74-a71a58d39b95', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('182', '2016-09-09周报', '2016-09-09 13:47:22', null, '10', null, null, 'ffef8bc5-702b-4bfb-ee52-6fb1e52e41d9', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('183', '2016-09-09周报', '2016-09-09 13:56:27', null, '2', null, null, '567be40c-41e8-6839-25dd-6a1b288085d1', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('184', '2016-09-18周报', '2016-09-18 16:40:09', null, '17', null, null, '94e8c94a-7d2c-f116-af7f-8a7c90c28575', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('185', '2016-09-18周报', '2016-09-18 16:50:58', null, '8', null, null, '327a2e3f-e032-8b7c-345c-ad75df9d6671', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('186', '2016-09-18周报', '2016-09-18 16:57:17', null, '16', null, null, '49e63bda-90af-6823-bcec-98212d64d424', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('187', '2016-09-18周报', '2016-09-18 16:59:01', null, '6', null, null, 'dfdfb9b0-ee1a-cd23-7745-077ff3aeabd0', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('188', '2016-09-18周报', '2016-09-18 17:17:03', null, '10', null, null, 'c9a480f3-2a8d-378e-2652-1e76c25198cd', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('189', '2016-09-18周报', '2016-09-18 17:18:09', null, '24', null, null, 'cf9011fd-336d-b663-ca6e-8708dbe65bfe', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('190', '2016-09-18周报', '2016-09-18 08:39:59', null, '2', null, null, 'd33a5b6b-ae1d-72fc-6e78-276d21594147', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('191', '2016-09-23周报', '2016-09-23 13:24:43', null, '10', null, null, 'fd756f51-eb07-cdef-9578-8557a41ba367', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('192', '2016-09-23周报', '2016-09-23 13:28:11', null, '2', null, null, 'f23de0be-6fc5-88bc-1be4-75d9899afa88', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('193', '2016-09-23周报', '2016-09-23 13:30:11', null, '24', null, null, '6949ed1f-b812-41f1-6e98-718bae92ca69', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('194', '2016-09-23周报', '2016-09-23 13:34:05', null, '21', null, null, '1cdb6e27-0051-0ce3-7324-136ddad05798', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('195', '2016-09-23周报', '2016-09-23 13:37:12', null, '16', null, null, 'ae817d41-2bd1-4fdd-7004-f614145bdf05', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('196', '2016-09-23周报', '2016-09-23 13:57:55', null, '6', null, null, '192adb08-d801-9ae3-b1a8-224d63269915', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('197', '2016-09-23周报', '2016-09-23 14:06:08', null, '17', null, null, '60082cca-c7bb-60c9-ff2b-658551a5aa94', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('198', '2016-09-23周报', '2016-09-23 14:09:47', null, '8', null, null, '893ab7f5-f480-9c30-402d-d51c0bc31228', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('199', '2016-09-30周报', '2016-09-30 15:09:50', null, '24', null, null, '91b261d4-c1b5-f751-751c-36f83b36a9b8', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('200', '2016-09-30周报', '2016-09-30 15:12:43', null, '10', null, null, '0a20be13-d81d-872d-2252-019d693fe2ae', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('201', '2016-09-30周报', '2016-09-30 15:16:32', null, '16', null, null, '5198069a-7b30-fae0-2eb6-7d75772299a1', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('202', '2016-09-30周报', '2016-09-30 15:17:10', null, '17', null, null, '699a3e29-68ab-9e2f-17cb-06be1dd0da08', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('203', '2016-09-30周报', '2016-09-30 15:17:29', null, '2', null, null, 'f1e9c6c3-b436-240c-d607-6c54a5db1dce', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('204', '2016-09-30周报', '2016-09-30 15:20:40', null, '21', null, null, 'ed874ad4-f4bf-27e1-35bc-2cf6334c7a10', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('205', '2016-09-30周报', '2016-09-30 15:22:41', null, '6', null, null, '4b897d83-0b23-56da-7579-5dc9bb7fb237', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('206', '2016-10-14周报', '2016-10-14 13:26:19', null, '8', null, null, 'a2d8888a-4b61-9af7-fa9d-5006f306df63', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('207', '2016-10-14周报', '2016-10-14 13:33:28', null, '2', null, null, '7e8d912b-9caf-dced-4c58-32f8e3f54f54', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('208', '2016-10-14周报', '2016-10-14 13:40:56', null, '17', null, null, 'a30a2bc3-9572-b23d-bbb8-5e14af6f0cc6', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('209', '2016-10-14周报', '2016-10-14 13:44:16', null, '24', null, null, '8989e27f-fc57-3c6c-a827-983ce013ac1f', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('210', '2016-10-14周报', '2016-10-14 14:14:16', null, '16', null, null, '15b958e4-42af-21d1-d926-22c4f705ff1d', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('211', '2016-10-14周报', '2016-10-14 14:18:38', null, '6', null, null, '9057493b-51d9-d1fa-91bc-f9188cff8c4d', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('212', '2016-10-14周报', '2016-10-14 16:10:04', null, '10', null, null, 'efc13656-203d-7436-f336-6613f67a3577', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('213', '2016-10-14周报', '2016-10-14 16:11:58', null, '21', null, null, '62467bcb-a7d5-5e49-a426-171abe5b019f', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('214', '2016-10-21周报', '2016-10-21 14:30:18', null, '8', null, null, 'f3ac95b4-59c8-91a6-3bc9-3d5227531a79', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('215', '2016-10-21周报', '2016-10-21 14:33:56', null, '10', null, null, '27033497-8d10-6e49-cfc7-0eba7c0c92c1', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('216', '2016-10-21周报', '2016-10-21 14:34:48', null, '6', null, null, '76d46e6b-1436-07ed-17ee-479c1e8e12cd', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('217', '2016-10-21周报', '2016-10-21 14:37:09', null, '17', null, null, 'beb2ea75-8c54-5fc1-cf1a-7284a7b73355', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('218', '2016-10-21周报', '2016-10-21 14:50:02', null, '21', null, null, 'c50fc16f-5cfa-8327-8769-90f1ce67dd8f', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('219', '2016-10-21周报', '2016-10-21 14:57:20', null, '2', null, null, 'a37e4848-bdc5-52f6-b407-9fd2996879cb', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('220', '2016-10-21周报', '2016-10-21 14:57:39', null, '24', null, null, '9e1616db-9eb8-3076-3bbf-36088906489c', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('221', '2016-10-21周报', '2016-10-21 15:16:26', null, '16', null, null, '25a2f58a-6e78-b429-6586-3b7f620805f6', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('222', '2016-10-28周报', '2016-10-28 14:40:58', null, '24', null, null, '086e208c-30eb-215e-f96e-446d139a8828', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('223', '2016-10-28周报', '2016-10-28 14:41:22', null, '8', null, null, '7c3ee76a-74cc-6d90-94ee-065bafcb1864', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('224', '2016-10-28周报', '2016-10-28 14:42:33', null, '10', null, null, 'c55da744-d21e-7be1-effe-4f5b47cfcd0e', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('225', '2016-10-28周报', '2016-10-28 14:50:50', null, '16', null, null, '36673390-0daa-2d87-cab3-2fcec10c616f', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('226', '2016-10-28周报', '2016-10-28 14:53:43', null, '2', null, null, '438c7440-e20a-4145-16ca-5ac13ef29b9a', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('227', '2016-10-28周报', '2016-10-28 14:55:16', null, '6', null, null, '73701326-6b7e-9594-dae2-09c5bb69ad8a', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('228', '2016-10-28周报', '2016-10-28 15:07:24', null, '17', null, null, '76307f6f-fcc5-c642-00fb-e8d6515ccfac', '陈android', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('229', '2016-10-28周报', '2016-10-28 17:18:04', null, '21', null, null, 'f11ae4eb-e2c4-fc7d-4465-a0edc48a191c', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('230', '2016-11-04周报', '2016-11-04 16:12:42', null, '24', null, null, 'c37d0abf-46e4-4406-e15e-65b6b3d2932e', '王MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('231', '2016-12-09周报', '2016-12-09 17:07:43', null, '2', null, null, 'e234e786-dad4-cfa2-97b4-a67c57f87054', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('232', '2016-12-12日报', '2016-12-12 14:50:39', null, '2', null, null, '2f8a2e1d-7253-8e3d-2021-8287e88e05ba', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('233', '2016-12-13日报', '2016-12-13 15:33:34', null, '2', null, null, '082a2af5-3413-8014-2277-b141e11831b8', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('234', '2016-12-14日报', '2016-12-14 17:12:08', null, '2', null, null, 'e42d20bd-c5ba-2462-f41a-a9dab3ca0cf6', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('235', '2016-12-16日报', '2016-12-16 15:38:15', null, '2', null, null, '58de2dbf-e18c-794a-531f-16fbcc70a380', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('236', '2016-12-19日报', '2016-12-19 13:58:56', null, '2', null, null, '63ced581-d3fd-6383-c1c4-ee9851f57a4b', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('237', '2016-12-23日报', '2016-12-23 13:15:56', null, '2', null, null, 'b71e3ac0-f8d1-3737-791d-f47fdb16a9d8', '陈sir', '研发部', null, 'day');
INSERT INTO `busi_work_report` VALUES ('238', '2016-12-23周报', '2016-12-23 13:19:20', null, '8', null, null, '776e8283-44b0-c400-3508-1e3a8f7f4071', '莫sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('239', '2016-12-23周报', '2016-12-23 13:27:44', null, '2', null, null, '5e13b71d-7da5-790a-98a4-e8d2101672b4', '陈sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('240', '2016-12-23周报', '2016-12-23 13:31:46', null, '10', null, null, 'd580f45e-8ff0-3a55-05a9-259b73d40519', '田MM', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('241', '2016-12-23周报', '2016-12-23 13:40:26', null, '6', null, null, '03bd508d-d3a1-ff4e-c4a5-ddb9c11b7f39', '张sir', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('242', '2016-12-23周报', '2016-12-23 14:13:33', null, '21', null, null, 'e868f485-57e7-83ae-c942-72c4789bc0ac', '彭博士', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('243', '2016-12-23周报', '2016-12-23 15:14:29', null, '16', null, null, 'a4ab8a37-2020-526a-d3e4-2268ef088f19', '陆妹子', '研发部', null, 'week');
INSERT INTO `busi_work_report` VALUES ('244', '2016-12-23周报', '2016-12-23 15:39:28', null, '17', null, null, 'ad85d145-a9d4-d093-504a-773aa412a173', '陈android', '研发部', null, 'week');

-- ----------------------------
-- Table structure for busi_work_report_item
-- ----------------------------
DROP TABLE IF EXISTS `busi_work_report_item`;
CREATE TABLE `busi_work_report_item` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_content` varchar(500) DEFAULT NULL,
  `d_create_time` varchar(50) DEFAULT NULL,
  `i_report_code` varchar(100) DEFAULT NULL,
  `c_work_time` varchar(50) DEFAULT NULL,
  `hope_finish_date` varchar(10) DEFAULT NULL,
  `actual_finish_date` varchar(10) DEFAULT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1482 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of busi_work_report_item
-- ----------------------------
INSERT INTO `busi_work_report_item` VALUES ('1', '2016-05-04 08:46:31 工作内容', '2016-05-04 08:46:31', 'ca22f65a-5d43-64c1-799a-0a110a79a722', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('2', '2016-05-04 08:46:48 工作内容', '2016-05-04 08:46:48', 'ca22f65a-5d43-64c1-799a-0a110a79a722', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('3', '2016-05-04 09:01:27 工作内容', '2016-05-04 09:01:27', 'beb26b06-5c74-8ac9-1a8c-823af26ec694', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('4', '2016-05-04 13:46:05 工作内容', '2016-05-04 13:46:05', 'beb26b06-5c74-8ac9-1a8c-823af26ec694', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('5', '2016-05-04 15:56:05 工作内容', '2016-05-04 15:56:05', '8085bf03-e79b-5f67-a417-10edad470298', '4', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('7', '2016-05-06 13:47:23 工作内容', '2016-05-06 13:47:23', 'da271b8d-39bf-a2d4-6688-f45ee409e4dc', null, '2016-05-05', '2016-04-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('8', '2016-05-06 13:56:16 工作内容', '2016-05-06 13:56:16', '6df559d0-95a0-2375-6370-63e18c09bd58', null, '2016-05-03', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('9', '2016-05-06 13:56:25 工作内容', '2016-05-06 13:56:25', '6df559d0-95a0-2375-6370-63e18c09bd58', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('10', '2016-05-06 13:56:54 工作内容', '2016-05-06 13:56:54', '87ac96e5-7e14-8909-2284-3f462dded64d', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('12', '2016-05-06 13:58:00 工作内容', '2016-05-06 13:58:00', '87ac96e5-7e14-8909-2284-3f462dded64d', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('15', '2016-05-06 14:01:53 工作内容', '2016-05-06 14:01:53', '9acb8aac-c24a-d72b-9b40-ed95f889ba0e', null, '2016-05-03', '2016-05-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('16', '2016-05-06 14:02:20 工作内容', '2016-05-06 14:02:20', '8e564912-9e1e-8ce8-5574-bbee73aa7cdf', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('17', '2016-05-06 14:02:52 工作内容', '2016-05-06 14:02:52', '87ac96e5-7e14-8909-2284-3f462dded64d', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('18', '2016-05-06 14:03:26 工作内容', '2016-05-06 14:03:26', '8e564912-9e1e-8ce8-5574-bbee73aa7cdf', null, '2016-05-13', '2016-05-13', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('19', '2016-05-06 14:08:55 工作内容', '2016-05-06 14:08:55', '9acb8aac-c24a-d72b-9b40-ed95f889ba0e', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('21', '2016-05-06 14:11:22 工作内容', '2016-05-06 14:11:22', '9c2ffbba-c636-d3ec-d463-4198f09afffd', null, '2016-05-03', '2016-05-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('22', '2016-05-06 14:12:53 工作内容', '2016-05-06 14:12:53', '9c2ffbba-c636-d3ec-d463-4198f09afffd', null, '2016-05-03', '2016-05-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('23', '2016-05-06 14:13:43 工作内容', '2016-05-06 14:13:43', '9c2ffbba-c636-d3ec-d463-4198f09afffd', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('24', '2016-05-06 14:14:17 工作内容', '2016-05-06 14:14:17', '7c99af89-b52d-0792-1c47-9f98cf922e85', null, '2016-05-13', '2016-05-13', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('25', '2016-05-06 14:14:34 工作内容', '2016-05-06 14:14:34', '9acb8aac-c24a-d72b-9b40-ed95f889ba0e', null, '2016-05-05', '2016-05-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('27', '2016-05-06 14:15:29 工作内容', '2016-05-06 14:15:29', '9c2ffbba-c636-d3ec-d463-4198f09afffd', null, '2016-05-05', '2016-05-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('28', '2016-05-06 14:16:23 工作内容', '2016-05-06 14:16:23', '9c2ffbba-c636-d3ec-d463-4198f09afffd', null, '2016-05-05', '2016-05-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('29', '2016-05-06 14:16:27 工作内容', '2016-05-06 14:16:27', '9acb8aac-c24a-d72b-9b40-ed95f889ba0e', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('30', '2016-05-06 14:17:33 工作内容', '2016-05-06 14:17:33', '9c2ffbba-c636-d3ec-d463-4198f09afffd', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('31', '2016-05-06 14:17:45 工作内容', '2016-05-06 14:17:45', '9c2ffbba-c636-d3ec-d463-4198f09afffd', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('32', '2016-05-06 14:18:26 工作内容', '2016-05-06 14:18:26', '9acb8aac-c24a-d72b-9b40-ed95f889ba0e', null, '2016-05-09', '2016-05-13', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('33', '2016-05-06 14:18:33 工作内容', '2016-05-06 14:18:33', '519471ff-d3eb-8473-c904-a58e6dcb0687', null, '2016-05-03', '2016-05-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('34', '2016-05-06 14:19:42 工作内容', '2016-05-06 14:19:42', '519471ff-d3eb-8473-c904-a58e6dcb0687', null, '2016-05-03', '2016-05-03', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('35', '2016-05-06 14:20:30 工作内容', '2016-05-06 14:20:30', '519471ff-d3eb-8473-c904-a58e6dcb0687', null, '2016-05-03', '2016-05-03', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('36', '2016-05-06 14:25:14 工作内容', '2016-05-06 14:25:14', '6df559d0-95a0-2375-6370-63e18c09bd58', null, '2016-05-13', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('37', '2016-05-06 14:36:03 工作内容', '2016-05-06 14:36:03', '519471ff-d3eb-8473-c904-a58e6dcb0687', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('38', '2016-05-06 14:36:46 工作内容', '2016-05-06 14:36:46', '519471ff-d3eb-8473-c904-a58e6dcb0687', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('39', '2016-05-06 14:38:01 工作内容', '2016-05-06 14:38:01', '519471ff-d3eb-8473-c904-a58e6dcb0687', null, '2016-05-05', '2016-05-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('40', '2016-05-06 14:43:18 工作内容', '2016-05-06 14:43:18', '519471ff-d3eb-8473-c904-a58e6dcb0687', null, '2016-05-05', '2016-05-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('41', '2016-05-06 14:43:52 工作内容', '2016-05-06 14:43:52', '519471ff-d3eb-8473-c904-a58e6dcb0687', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('42', '2016-05-06 14:44:10 工作内容', '2016-05-06 14:44:10', '7bfcbe5b-6f13-db56-2a58-6096f084e9ab', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('43', '2016-05-06 14:45:05 工作内容', '2016-05-06 14:45:05', 'c5132a7e-99ae-63d4-1989-abb3dca24982', null, '2016-05-03', '2016-05-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('44', '2016-05-06 14:45:36 工作内容', '2016-05-06 14:45:36', 'c5132a7e-99ae-63d4-1989-abb3dca24982', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('45', '2016-05-06 14:47:42 工作内容', '2016-05-06 14:47:42', '92f3c223-d8f8-c9cc-b927-d8e0b6356a51', null, '2016-05-03', '2016-05-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('46', '2016-05-06 14:48:07 工作内容', '2016-05-06 14:48:07', '92f3c223-d8f8-c9cc-b927-d8e0b6356a51', null, '2016-05-05', '2016-05-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('47', '2016-05-06 14:51:13 工作内容', '2016-05-06 14:51:13', '92f3c223-d8f8-c9cc-b927-d8e0b6356a51', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('48', '2016-05-06 14:51:35 工作内容', '2016-05-06 14:51:35', '92f3c223-d8f8-c9cc-b927-d8e0b6356a51', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('49', '2016-05-06 14:52:12 工作内容', '2016-05-06 14:52:12', '92f3c223-d8f8-c9cc-b927-d8e0b6356a51', null, '2016-05-13', '2016-05-13', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('50', '2016-05-06 14:54:31 工作内容', '2016-05-06 14:54:31', '92f3c223-d8f8-c9cc-b927-d8e0b6356a51', null, '2016-05-13', '2016-05-13', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('51', '2016-05-06 14:57:22 工作内容', '2016-05-06 14:57:22', '92555c50-643d-b405-1950-11e6e7cf25e3', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('52', '2016-05-06 14:58:05 工作内容', '2016-05-06 14:58:05', '92555c50-643d-b405-1950-11e6e7cf25e3', null, '2016-05-05', '2016-05-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('53', '2016-05-06 15:01:10 工作内容', '2016-05-06 15:01:10', '6df559d0-95a0-2375-6370-63e18c09bd58', null, '2016-05-13', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('54', '2016-05-06 15:01:36 工作内容', '2016-05-06 15:01:36', '6df559d0-95a0-2375-6370-63e18c09bd58', null, '2016-05-13', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('55', '2016-05-06 15:10:20 工作内容', '2016-05-06 15:10:20', '92555c50-643d-b405-1950-11e6e7cf25e3', null, '2016-05-15', '2016-05-15', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('56', '2016-05-06 15:10:24 工作内容', '2016-05-06 15:10:24', '0e2ec0cd-6588-5e91-74e1-644c3da80a55', null, '2016-05-03', '2016-05-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('57', '2016-05-06 15:10:36 工作内容', '2016-05-06 15:10:36', '8e564912-9e1e-8ce8-5574-bbee73aa7cdf', null, '2016-05-06', '2016-05-06', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('58', '2016-05-06 15:10:39 工作内容', '2016-05-06 15:10:39', '7c99af89-b52d-0792-1c47-9f98cf922e85', null, '2016-05-03', '2016-05-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('59', '2016-05-06 15:11:06 工作内容', '2016-05-06 15:11:06', '9acb8aac-c24a-d72b-9b40-ed95f889ba0e', null, '2016-05-03', '2016-05-06', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('60', '2016-05-06 15:11:20 工作内容', '2016-05-06 15:11:20', '7c99af89-b52d-0792-1c47-9f98cf922e85', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('61', '2016-05-06 15:12:21 工作内容', '2016-05-06 15:12:21', '92555c50-643d-b405-1950-11e6e7cf25e3', null, '2016-05-08', '2016-05-08', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('62', '2016-05-06 15:12:35 工作内容', '2016-05-06 15:12:35', '7c99af89-b52d-0792-1c47-9f98cf922e85', null, '2016-05-05', '2016-05-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('63', '2016-05-06 15:14:22 工作内容', '2016-05-06 15:14:22', '9c2ffbba-c636-d3ec-d463-4198f09afffd', null, '2016-05-10', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('64', '2016-05-06 15:15:35 工作内容', '2016-05-06 15:15:35', '0e2ec0cd-6588-5e91-74e1-644c3da80a55', null, '2016-05-03', '2016-05-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('66', '2016-05-06 15:16:29 工作内容', '2016-05-06 15:16:29', '0e2ec0cd-6588-5e91-74e1-644c3da80a55', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('67', '2016-05-06 15:17:07 工作内容', '2016-05-06 15:17:07', '0e2ec0cd-6588-5e91-74e1-644c3da80a55', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('68', '2016-05-06 15:17:45 工作内容', '2016-05-06 15:17:45', '0e2ec0cd-6588-5e91-74e1-644c3da80a55', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('69', '2016-05-06 15:18:05 工作内容', '2016-05-06 15:18:05', '0e2ec0cd-6588-5e91-74e1-644c3da80a55', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('70', '2016-05-06 15:18:51 工作内容', '2016-05-06 15:18:51', '7c99af89-b52d-0792-1c47-9f98cf922e85', null, '2016-05-06', '2016-05-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('71', '2016-05-06 15:19:11 工作内容', '2016-05-06 15:19:11', '0e2ec0cd-6588-5e91-74e1-644c3da80a55', null, '2016-05-13', '2016-05-13', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('72', '2016-05-06 15:19:33 工作内容', '2016-05-06 15:19:33', '0e2ec0cd-6588-5e91-74e1-644c3da80a55', null, '2016-05-13', '2016-05-13', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('73', '2016-05-06 15:25:58 工作内容', '2016-05-06 15:25:58', '7c99af89-b52d-0792-1c47-9f98cf922e85', null, '2016-05-06', '2016-05-06', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('74', '2016-05-06 15:27:56 工作内容', '2016-05-06 15:27:56', '92f3c223-d8f8-c9cc-b927-d8e0b6356a51', null, '2016-05-06', '2016-05-06', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('75', '2016-05-06 15:36:16 工作内容', '2016-05-06 15:36:16', '6df559d0-95a0-2375-6370-63e18c09bd58', null, '2016-05-06', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('76', '2016-05-06 16:09:14 工作内容', '2016-05-06 16:09:14', '0e2ec0cd-6588-5e91-74e1-644c3da80a55', null, '2016-05-06', '2016-05-06', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('78', '2016-05-09 09:00:44 工作内容', '2016-05-09 09:00:44', 'c1fc49c8-994e-47d0-e58d-4457668997ab', '3', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('79', '2016-05-09 09:01:04 工作内容', '2016-05-09 09:01:04', 'c1fc49c8-994e-47d0-e58d-4457668997ab', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('80', '2016-05-09 09:01:37 工作内容', '2016-05-09 09:01:37', 'c1fc49c8-994e-47d0-e58d-4457668997ab', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('81', '2016-05-09 17:42:33 工作内容', '2016-05-09 17:42:33', 'c1fc49c8-994e-47d0-e58d-4457668997ab', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('82', '2016-05-10 10:10:30 工作内容', '2016-05-10 10:10:30', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('83', '2016-05-10 10:13:36 工作内容', '2016-05-10 10:13:36', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('84', '2016-05-10 10:18:19 工作内容', '2016-05-10 10:18:19', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('85', '2016-05-10 10:19:22 工作内容', '2016-05-10 10:19:22', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('86', '2016-05-10 10:20:50 工作内容', '2016-05-10 10:20:50', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('87', '2016-05-10 10:22:31 工作内容', '2016-05-10 10:22:31', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('88', '2016-05-10 10:22:55 工作内容', '2016-05-10 10:22:55', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('89', '2016-05-10 10:24:24 工作内容', '2016-05-10 10:24:24', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('90', '2016-05-10 10:24:56 工作内容', '2016-05-10 10:24:56', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('91', '2016-05-10 10:25:48 工作内容', '2016-05-10 10:25:48', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('92', '2016-05-10 10:26:43 工作内容', '2016-05-10 10:26:43', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('93', '2016-05-10 10:29:13 工作内容', '2016-05-10 10:29:13', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('94', '2016-05-10 10:32:32 工作内容', '2016-05-10 10:32:32', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('95', '2016-05-10 10:34:36 工作内容', '2016-05-10 10:34:36', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('96', '2016-05-10 10:35:27 工作内容', '2016-05-10 10:35:27', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('97', '2016-05-10 10:35:56 工作内容', '2016-05-10 10:35:56', '01b7f087-447a-0a98-3027-5dac55275a87', null, '2016-05-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('98', '2016-05-13 09:21:33 工作内容', '2016-05-13 09:21:33', 'c1449fae-c3bd-5e9e-b128-c0f8f6ea950c', null, '2016-05-09', '2016-05-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('99', '2016-05-13 09:22:46 工作内容', '2016-05-13 09:22:46', 'c1449fae-c3bd-5e9e-b128-c0f8f6ea950c', null, '2016-05-11', '2016-05-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('100', '2016-05-13 09:23:43 工作内容', '2016-05-13 09:23:43', 'c1449fae-c3bd-5e9e-b128-c0f8f6ea950c', null, '2016-05-12', '2016-05-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('101', '2016-05-13 09:25:46 工作内容', '2016-05-13 09:25:46', 'c1449fae-c3bd-5e9e-b128-c0f8f6ea950c', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('102', '2016-05-13 09:28:12 工作内容', '2016-05-13 09:28:12', 'c1449fae-c3bd-5e9e-b128-c0f8f6ea950c', null, '2016-05-16', '2016-05-16', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('103', '2016-05-13 14:16:42 工作内容', '2016-05-13 14:16:42', '1e6075ff-f1d9-1989-783e-775b24888225', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('104', '2016-05-13 14:19:57 工作内容', '2016-05-13 14:19:57', '1e6075ff-f1d9-1989-783e-775b24888225', null, '2016-05-04', '2016-05-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('106', '2016-05-13 14:22:12 工作内容', '2016-05-13 14:22:12', '1e6075ff-f1d9-1989-783e-775b24888225', null, '2016-05-20', '2016-05-20', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('107', '2016-05-13 16:00:39 工作内容', '2016-05-13 16:00:39', '5942fe7d-998b-d05d-1135-87905c776429', null, '2016-05-09', '2016-05-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('108', '2016-05-13 16:01:44 工作内容', '2016-05-13 16:01:44', '5942fe7d-998b-d05d-1135-87905c776429', null, '2016-05-10', '2016-05-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('109', '2016-05-13 16:02:25 工作内容', '2016-05-13 16:02:25', '5942fe7d-998b-d05d-1135-87905c776429', null, '2016-05-10', '2016-05-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('110', '2016-05-13 16:03:17 工作内容', '2016-05-13 16:03:17', '5942fe7d-998b-d05d-1135-87905c776429', null, '2016-05-12', '2016-05-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('111', '2016-05-13 16:04:05 工作内容', '2016-05-13 16:04:05', '5942fe7d-998b-d05d-1135-87905c776429', null, '持续', '持续', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('112', '2016-05-13 16:10:18 工作内容', '2016-05-13 16:10:18', 'e51ee87c-746d-743d-87d6-c241fd7613a6', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('113', '2016-05-13 16:14:48 工作内容', '2016-05-13 16:14:48', 'e51ee87c-746d-743d-87d6-c241fd7613a6', null, '2016-05-20', '2016-05-20', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('114', '2016-05-13 16:25:46 工作内容', '2016-05-13 16:25:46', '43172a76-0d2e-be97-59cc-4ce825b3bc70', null, '2016-05-12', '2016-05-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('115', '2016-05-13 16:26:40 工作内容', '2016-05-13 16:26:40', '43172a76-0d2e-be97-59cc-4ce825b3bc70', null, '2016-05-11', '2016-05-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('116', '2016-05-13 16:27:20 工作内容', '2016-05-13 16:27:20', '43172a76-0d2e-be97-59cc-4ce825b3bc70', null, '2016-05-20', '2016-05-20', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('117', '2016-05-13 16:33:03 工作内容', '2016-05-13 16:33:03', 'a850c1ba-5ea7-16ea-234b-e32bdd316590', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('118', '2016-05-13 16:33:34 工作内容', '2016-05-13 16:33:34', 'a850c1ba-5ea7-16ea-234b-e32bdd316590', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('119', '2016-05-13 16:34:11 工作内容', '2016-05-13 16:34:11', 'a850c1ba-5ea7-16ea-234b-e32bdd316590', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('120', '2016-05-13 16:34:45 工作内容', '2016-05-13 16:34:45', 'a850c1ba-5ea7-16ea-234b-e32bdd316590', null, '持续', '持续', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('121', '2016-05-13 17:12:32 工作内容', '2016-05-13 17:12:32', 'd96faa82-547b-c690-6c55-95228cceebef', null, '2016-05-10', '2016-05-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('122', '2016-05-13 17:14:07 工作内容', '2016-05-13 17:14:07', 'd96faa82-547b-c690-6c55-95228cceebef', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('123', '2016-05-13 17:23:30 工作内容', '2016-05-13 17:23:30', '0c73e4f2-69d2-ae66-1f55-f2e5f6a0af8e', null, '2016-05-10', '2016-05-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('124', '2016-05-13 17:23:56 工作内容', '2016-05-13 17:23:56', '63d6bfd3-d774-f433-431a-217632ad9d6e', null, '2016-05-09', '2016-05-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('125', '2016-05-13 17:24:32 工作内容', '2016-05-13 17:24:32', '0c73e4f2-69d2-ae66-1f55-f2e5f6a0af8e', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('126', '2016-05-13 17:24:55 工作内容', '2016-05-13 17:24:55', '63d6bfd3-d774-f433-431a-217632ad9d6e', null, '2016-05-11', '2016-05-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('127', '2016-05-13 17:24:58 工作内容', '2016-05-13 17:24:58', '0c73e4f2-69d2-ae66-1f55-f2e5f6a0af8e', null, '2016-05-13', '2016-05-13', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('128', '2016-05-13 17:25:41 工作内容', '2016-05-13 17:25:41', '63d6bfd3-d774-f433-431a-217632ad9d6e', null, '2016-05-10', '2016-05-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('129', '2016-05-13 17:26:12 工作内容', '2016-05-13 17:26:12', '0c73e4f2-69d2-ae66-1f55-f2e5f6a0af8e', null, '2016-05-13', '2016-05-13', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('130', '2016-05-13 17:26:23 工作内容', '2016-05-13 17:26:23', '63d6bfd3-d774-f433-431a-217632ad9d6e', null, '2016-05-12', '2016-05-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('131', '2016-05-13 17:29:52 工作内容', '2016-05-13 17:29:52', '63d6bfd3-d774-f433-431a-217632ad9d6e', null, '2016-05-16', '2016-05-16', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('132', '2016-05-13 11:34:38 工作内容', '2016-05-13 11:34:38', '3780fd15-fb8c-aa54-4c66-fe22e79fd5a2', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('133', '2016-05-13 11:36:50 工作内容', '2016-05-13 11:36:50', '3780fd15-fb8c-aa54-4c66-fe22e79fd5a2', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('134', '2016-05-13 11:38:25 工作内容', '2016-05-13 11:38:25', '3780fd15-fb8c-aa54-4c66-fe22e79fd5a2', null, '2016-05-13', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('135', '2016-05-13 11:41:47 工作内容', '2016-05-13 11:41:47', '3780fd15-fb8c-aa54-4c66-fe22e79fd5a2', null, '2016-05-18', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('136', '2016-05-13 11:42:38 工作内容', '2016-05-13 11:42:38', '3780fd15-fb8c-aa54-4c66-fe22e79fd5a2', null, '2016-05-18', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('137', '2016-05-13 11:44:50 工作内容', '2016-05-13 11:44:50', '3780fd15-fb8c-aa54-4c66-fe22e79fd5a2', null, '2016-05-13', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('138', '2016-05-13 12:15:35 工作内容', '2016-05-13 12:15:35', '21139ded-b2ef-6824-c8b2-0522b7d3b013', null, '2016-05-10', '2016-05-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('139', '2016-05-13 12:15:53 工作内容', '2016-05-13 12:15:53', '21139ded-b2ef-6824-c8b2-0522b7d3b013', null, '2016-05-11', '2016-05-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('140', '2016-05-13 12:16:15 工作内容', '2016-05-13 12:16:15', '21139ded-b2ef-6824-c8b2-0522b7d3b013', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('141', '2016-05-13 12:17:00 工作内容', '2016-05-13 12:17:00', '21139ded-b2ef-6824-c8b2-0522b7d3b013', null, '2016-05-17', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('142', '2016-05-13 12:17:17 工作内容', '2016-05-13 12:17:17', '21139ded-b2ef-6824-c8b2-0522b7d3b013', null, '2016-05-18', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('143', '2016-05-13 12:20:55 工作内容', '2016-05-13 12:20:55', '8f28aa65-fcc8-7cd0-c6eb-dd502f194fa1', null, '2016-05-20', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('144', '2016-05-13 12:24:11 工作内容', '2016-05-13 12:24:11', '8f28aa65-fcc8-7cd0-c6eb-dd502f194fa1', null, '2016-05-13', '2016-05-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('145', '2016-05-16 09:24:28 工作内容', '2016-05-16 09:24:28', 'f86c7064-e750-634b-6bdd-e247c22e4204', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('146', '2016-05-16 09:25:22 工作内容', '2016-05-16 09:25:22', 'f86c7064-e750-634b-6bdd-e247c22e4204', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('147', '2016-05-16 09:25:59 工作内容', '2016-05-16 09:25:59', 'f86c7064-e750-634b-6bdd-e247c22e4204', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('148', '2016-05-16 09:26:37 工作内容', '2016-05-16 09:26:37', 'f86c7064-e750-634b-6bdd-e247c22e4204', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('149', '2016-05-17 09:35:32 工作内容', '2016-05-17 09:35:32', '57b40ebb-8e7b-dee7-1874-772bfd342520', '3', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('150', '2016-05-17 09:36:25 工作内容', '2016-05-17 09:36:25', '57b40ebb-8e7b-dee7-1874-772bfd342520', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('151', '2016-05-20 12:13:37 工作内容', '2016-05-20 12:13:37', '07343625-b1b2-4a8a-affb-b30dd8e234f3', null, '2016-05-31', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('152', '2016-05-20 12:14:54 工作内容', '2016-05-20 12:14:54', '07343625-b1b2-4a8a-affb-b30dd8e234f3', null, '2016-05-20', '2016-05-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('153', '2016-05-20 12:15:45 工作内容', '2016-05-20 12:15:45', '07343625-b1b2-4a8a-affb-b30dd8e234f3', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('154', '2016-05-20 12:16:19 工作内容', '2016-05-20 12:16:19', '07343625-b1b2-4a8a-affb-b30dd8e234f3', null, '2016-05-31', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('155', '2016-05-20 12:17:51 工作内容', '2016-05-20 12:17:51', '07343625-b1b2-4a8a-affb-b30dd8e234f3', null, '2016-05-26', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('156', '2016-05-20 12:20:40 工作内容', '2016-05-20 12:20:40', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-19', '2016-05-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('157', '2016-05-20 12:20:52 工作内容', '2016-05-20 12:20:52', '4033d779-0ff7-5fec-a7dd-b2a059f27fc1', null, '2016-05-18', '2016-05-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('158', '2016-05-20 12:21:04 工作内容', '2016-05-20 12:21:04', '2286e697-5d34-eef6-b9bc-cfb820f2b2b8', null, '2016-05-18', '2016-05-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('159', '2016-05-20 12:21:20 工作内容', '2016-05-20 12:21:20', '4033d779-0ff7-5fec-a7dd-b2a059f27fc1', null, '2016-05-19', '2016-05-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('160', '2016-05-20 12:21:29 工作内容', '2016-05-20 12:21:29', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('162', '2016-05-20 12:21:44 工作内容', '2016-05-20 12:21:44', '4033d779-0ff7-5fec-a7dd-b2a059f27fc1', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('163', '2016-05-20 12:22:07 工作内容', '2016-05-20 12:22:07', 'b9d57023-91f7-8685-876b-1188c5540424', null, '2016-05-16', '2016-05-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('164', '2016-05-20 12:22:31 工作内容', '2016-05-20 12:22:31', '4033d779-0ff7-5fec-a7dd-b2a059f27fc1', null, '2016-05-23', '2016-05-23', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('165', '2016-05-20 12:22:56 工作内容', '2016-05-20 12:22:56', 'b9d57023-91f7-8685-876b-1188c5540424', null, '2016-05-17', '2016-05-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('166', '2016-05-20 12:23:02 工作内容', '2016-05-20 12:23:02', '4033d779-0ff7-5fec-a7dd-b2a059f27fc1', null, '2016-05-27', '2016-05-27', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('167', '2016-05-20 12:23:37 工作内容', '2016-05-20 12:23:37', '2286e697-5d34-eef6-b9bc-cfb820f2b2b8', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('168', '2016-05-20 12:23:58 工作内容', '2016-05-20 12:23:58', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('169', '2016-05-20 12:24:13 工作内容', '2016-05-20 12:24:13', '2286e697-5d34-eef6-b9bc-cfb820f2b2b8', null, '2016-05-20', '2016-05-20', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('170', '2016-05-20 12:24:43 工作内容', '2016-05-20 12:24:43', 'ea5fc7f7-b69c-560b-2cf7-f3ce22e20c2f', null, '2016-05-17', '2016-05-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('171', '2016-05-20 12:24:44 工作内容', '2016-05-20 12:24:44', 'b9d57023-91f7-8685-876b-1188c5540424', null, '2016-05-18', '2016-05-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('172', '2016-05-20 12:25:02 工作内容', '2016-05-20 12:25:02', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('173', '2016-05-20 12:25:42 工作内容', '2016-05-20 12:25:42', '2286e697-5d34-eef6-b9bc-cfb820f2b2b8', null, '2016-05-29', '2016-05-29', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('174', '2016-05-20 12:25:47 工作内容', '2016-05-20 12:25:47', 'ea5fc7f7-b69c-560b-2cf7-f3ce22e20c2f', null, '2016-05-18', '2016-05-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('175', '2016-05-20 12:25:54 工作内容', '2016-05-20 12:25:54', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('176', '2016-05-20 12:26:45 工作内容', '2016-05-20 12:26:45', 'b9d57023-91f7-8685-876b-1188c5540424', null, '2016-05-17', '2016-05-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('177', '2016-05-20 12:28:12 工作内容', '2016-05-20 12:28:12', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '持续', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('178', '2016-05-20 12:28:25 工作内容', '2016-05-20 12:28:25', 'f63eeb45-58b9-8b4d-4c88-3a20e07eee11', null, '2016-05-16', '2016-05-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('179', '2016-05-20 12:28:36 工作内容', '2016-05-20 12:28:36', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-23', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('180', '2016-05-20 12:29:09 工作内容', '2016-05-20 12:29:09', 'ea5fc7f7-b69c-560b-2cf7-f3ce22e20c2f', null, '2016-05-18', '2016-05-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('181', '2016-05-20 12:29:12 工作内容', '2016-05-20 12:29:12', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-23', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('182', '2016-05-20 12:29:22 工作内容', '2016-05-20 12:29:22', 'f63eeb45-58b9-8b4d-4c88-3a20e07eee11', null, '2016-05-17', '2016-05-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('183', '2016-05-20 12:29:35 工作内容', '2016-05-20 12:29:35', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-16', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('184', '2016-05-20 12:29:43 工作内容', '2016-05-20 12:29:43', 'ea5fc7f7-b69c-560b-2cf7-f3ce22e20c2f', null, '2016-05-18', '2016-05-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('185', '2016-05-20 12:30:23 工作内容', '2016-05-20 12:30:23', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-27', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('186', '2016-05-20 12:31:02 工作内容', '2016-05-20 12:31:02', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-23', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('187', '2016-05-20 12:31:51 工作内容', '2016-05-20 12:31:51', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-26', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('188', '2016-05-20 12:32:09 工作内容', '2016-05-20 12:32:09', 'f63eeb45-58b9-8b4d-4c88-3a20e07eee11', null, '2016-05-17', '2016-05-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('189', '2016-05-20 12:32:17 工作内容', '2016-05-20 12:32:17', 'ea5fc7f7-b69c-560b-2cf7-f3ce22e20c2f', null, '2016-05-19', '2016-05-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('190', '2016-05-20 12:33:28 工作内容', '2016-05-20 12:33:28', '057b8a4d-4e4a-0920-010a-c5a65efcea76', null, '2016-05-17', '2016-05-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('191', '2016-05-20 12:33:31 工作内容', '2016-05-20 12:33:31', 'f63eeb45-58b9-8b4d-4c88-3a20e07eee11', null, '2016-05-18', '2016-05-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('192', '2016-05-20 12:34:15 工作内容', '2016-05-20 12:34:15', 'ea5fc7f7-b69c-560b-2cf7-f3ce22e20c2f', null, '2016-05-23', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('193', '2016-05-20 12:35:00 工作内容', '2016-05-20 12:35:00', 'f63eeb45-58b9-8b4d-4c88-3a20e07eee11', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('194', '2016-05-20 12:35:53 工作内容', '2016-05-20 12:35:53', 'ea5fc7f7-b69c-560b-2cf7-f3ce22e20c2f', null, '2016-05-25', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('195', '2016-05-20 12:36:15 工作内容', '2016-05-20 12:36:15', 'f63eeb45-58b9-8b4d-4c88-3a20e07eee11', null, '持续', '持续', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('196', '2016-05-20 12:36:55 工作内容', '2016-05-20 12:36:55', 'ea5fc7f7-b69c-560b-2cf7-f3ce22e20c2f', null, '2016-05-26', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('197', '2016-05-20 12:39:04 工作内容', '2016-05-20 12:39:04', 'f63eeb45-58b9-8b4d-4c88-3a20e07eee11', null, '持续', '持续', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('198', '2016-05-20 12:46:46 工作内容', '2016-05-20 12:46:46', '4033d779-0ff7-5fec-a7dd-b2a059f27fc1', null, '2016-05-24', '2016-05-24', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('199', '2016-05-20 12:47:21 工作内容', '2016-05-20 12:47:21', '3e856795-6c77-376c-07a4-06f3027f1d47', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('200', '2016-05-20 12:49:51 工作内容', '2016-05-20 12:49:51', '3e856795-6c77-376c-07a4-06f3027f1d47', null, '2016-05-27', '2016-05-27', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('201', '2016-05-20 13:05:35 工作内容', '2016-05-20 13:05:35', '57f9702d-06e1-4460-45ae-6a9fb7c2d3a8', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('202', '2016-05-20 13:06:10 工作内容', '2016-05-20 13:06:10', '3e46a022-015c-97d5-546d-0931c87a33b7', null, '2016-05-16', '2016-05-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('203', '2016-05-20 13:06:15 工作内容', '2016-05-20 13:06:15', '57f9702d-06e1-4460-45ae-6a9fb7c2d3a8', null, '2016-05-27', '2016-05-27', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('204', '2016-05-20 13:07:23 工作内容', '2016-05-20 13:07:23', '3e46a022-015c-97d5-546d-0931c87a33b7', null, '2016-05-18', '2016-05-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('205', '2016-05-20 13:07:42 工作内容', '2016-05-20 13:07:42', '57f9702d-06e1-4460-45ae-6a9fb7c2d3a8', null, '2016-05-27', '2016-05-27', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('206', '2016-05-20 13:08:12 工作内容', '2016-05-20 13:08:12', '3e46a022-015c-97d5-546d-0931c87a33b7', null, '2016-05-19', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('207', '2016-05-20 13:12:16 工作内容', '2016-05-20 13:12:16', '3e46a022-015c-97d5-546d-0931c87a33b7', null, '2016-05-24', '未知（下周持续）', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('208', '2016-05-20 13:27:20 工作内容', '2016-05-20 13:27:20', '95553e9f-0e03-bbc0-dd10-63968da90d82', null, '2016-05-16', '2016-05-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('209', '2016-05-20 13:28:03 工作内容', '2016-05-20 13:28:03', '95553e9f-0e03-bbc0-dd10-63968da90d82', null, '2016-05-17', '2016-05-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('210', '2016-05-20 13:28:57 工作内容', '2016-05-20 13:28:57', '95553e9f-0e03-bbc0-dd10-63968da90d82', null, '2016-05-18', '2016-05-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('211', '2016-05-20 13:29:41 工作内容', '2016-05-20 13:29:41', '95553e9f-0e03-bbc0-dd10-63968da90d82', null, '2016-05-19', '2016-05-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('212', '2016-05-20 13:30:15 工作内容', '2016-05-20 13:30:15', '95553e9f-0e03-bbc0-dd10-63968da90d82', null, '2016-05-20', '2016-05-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('213', '2016-05-20 13:30:58 工作内容', '2016-05-20 13:30:58', '95553e9f-0e03-bbc0-dd10-63968da90d82', null, '2016-05-23', '2016-05-23', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('214', '2016-05-20 13:32:16 工作内容', '2016-05-20 13:32:16', '95553e9f-0e03-bbc0-dd10-63968da90d82', null, '2016-05-20', '2016-05-20', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('215', '2016-05-27 12:35:56 工作内容', '2016-05-27 12:35:56', '5ec09bf8-bfff-8f4a-b374-f51487892d03', null, '2016-05-31', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('216', '2016-05-27 12:36:31 工作内容', '2016-05-27 12:36:31', '5ec09bf8-bfff-8f4a-b374-f51487892d03', null, '2016-05-26', '2016-05-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('219', '2016-05-27 14:54:41 工作内容', '2016-05-27 14:54:41', '14af8ce8-c730-906c-e6fd-e93996435fd3', null, '2016-05-23', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('220', '2016-05-27 14:55:14 工作内容', '2016-05-27 14:55:14', '14af8ce8-c730-906c-e6fd-e93996435fd3', null, '2016-05-24', '2016-05-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('221', '2016-05-27 14:55:48 工作内容', '2016-05-27 14:55:48', '14af8ce8-c730-906c-e6fd-e93996435fd3', null, '2016-05-25', '2016-05-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('222', '2016-05-27 14:56:12 工作内容', '2016-05-27 14:56:12', '14af8ce8-c730-906c-e6fd-e93996435fd3', null, '2016-05-26', '2016-05-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('223', '2016-05-27 14:57:21 工作内容', '2016-05-27 14:57:21', '14af8ce8-c730-906c-e6fd-e93996435fd3', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('224', '2016-05-27 15:01:34 工作内容', '2016-05-27 15:01:34', '14af8ce8-c730-906c-e6fd-e93996435fd3', null, '2016-05-27', '2016-05-27', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('225', '2016-05-27 15:02:59 工作内容', '2016-05-27 15:02:59', '14af8ce8-c730-906c-e6fd-e93996435fd3', null, '2016-05-27', '2016-05-27', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('226', '2016-05-27 15:03:20 工作内容', '2016-05-27 15:03:20', '98be8a69-f0d7-0399-e500-1807523eaa5b', null, '2016-05-31', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('227', '2016-05-27 15:03:48 工作内容', '2016-05-27 15:03:48', '98be8a69-f0d7-0399-e500-1807523eaa5b', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('228', '2016-05-27 15:04:20 工作内容', '2016-05-27 15:04:20', '98be8a69-f0d7-0399-e500-1807523eaa5b', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('229', '2016-05-27 15:04:33 工作内容', '2016-05-27 15:04:33', '98be8a69-f0d7-0399-e500-1807523eaa5b', null, '2016-05-31', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('230', '2016-05-27 15:05:14 工作内容', '2016-05-27 15:05:14', '3a6d4302-7ada-2d35-7e1a-92f2cb5f70c3', null, '2016-05-23', '2016-05-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('231', '2016-05-27 15:05:45 工作内容', '2016-05-27 15:05:45', '3a6d4302-7ada-2d35-7e1a-92f2cb5f70c3', null, '2016-05-24', '2016-05-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('232', '2016-05-27 15:05:56 工作内容', '2016-05-27 15:05:56', '98be8a69-f0d7-0399-e500-1807523eaa5b', null, '2016-05-31', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('234', '2016-05-27 15:06:08 工作内容', '2016-05-27 15:06:08', '3a6d4302-7ada-2d35-7e1a-92f2cb5f70c3', null, '2016-05-26', '2016-05-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('235', '2016-05-27 15:06:36 工作内容', '2016-05-27 15:06:36', '3a6d4302-7ada-2d35-7e1a-92f2cb5f70c3', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('236', '2016-05-27 15:09:21 工作内容', '2016-05-27 15:09:21', '2052de05-3662-c0db-9b31-6c46447cd8e7', null, '2016-05-31', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('237', '2016-05-27 15:09:28 工作内容', '2016-05-27 15:09:28', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-05-25', '2016-05-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('238', '2016-05-27 15:10:02 工作内容', '2016-05-27 15:10:02', '2052de05-3662-c0db-9b31-6c46447cd8e7', null, '2016-05-26', '2016-05-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('239', '2016-05-27 15:10:11 工作内容', '2016-05-27 15:10:11', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-05-26', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('240', '2016-05-27 15:11:57 工作内容', '2016-05-27 15:11:57', 'cd7925af-8c87-67fb-481e-f0b637a6ef8d', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('241', '2016-05-27 15:12:23 工作内容', '2016-05-27 15:12:23', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-05-27', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('242', '2016-05-27 15:13:11 工作内容', '2016-05-27 15:13:11', 'cd7925af-8c87-67fb-481e-f0b637a6ef8d', null, '2016-06-03', '2016-06-03', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('243', '2016-05-27 15:13:30 工作内容', '2016-05-27 15:13:30', '2052de05-3662-c0db-9b31-6c46447cd8e7', null, '2016-06-03', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('244', '2016-05-27 15:13:42 工作内容', '2016-05-27 15:13:42', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-05-30', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('245', '2016-05-27 15:14:17 工作内容', '2016-05-27 15:14:17', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-05-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('246', '2016-05-27 15:14:38 工作内容', '2016-05-27 15:14:38', 'cd7925af-8c87-67fb-481e-f0b637a6ef8d', null, '2016-05-27', '2016-05-27', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('247', '2016-05-27 15:14:40 工作内容', '2016-05-27 15:14:40', 'c21aa095-9308-e3c0-760f-880577905e4d', null, '2016-05-24', '2016-05-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('249', '2016-05-27 15:14:53 工作内容', '2016-05-27 15:14:53', 'c21aa095-9308-e3c0-760f-880577905e4d', null, '2016-05-25', '2016-05-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('250', '2016-05-27 15:14:59 工作内容', '2016-05-27 15:14:59', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-06-01', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('251', '2016-05-27 15:15:25 工作内容', '2016-05-27 15:15:25', 'c21aa095-9308-e3c0-760f-880577905e4d', null, '2016-05-26', '2016-05-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('252', '2016-05-27 15:15:38 工作内容', '2016-05-27 15:15:38', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-06-03', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('253', '2016-05-27 15:15:47 工作内容', '2016-05-27 15:15:47', '8097126b-c619-0573-e8cf-ca26c2295089', null, '2016-06-03', '2016-06-03', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('254', '2016-05-27 15:16:28 工作内容', '2016-05-27 15:16:28', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-06-03', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('255', '2016-05-27 15:16:47 工作内容', '2016-05-27 15:16:47', 'c21aa095-9308-e3c0-760f-880577905e4d', null, '2016-05-26', '2016-05-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('256', '2016-05-27 15:17:42 工作内容', '2016-05-27 15:17:42', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-05-27', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('257', '2016-05-27 15:17:49 工作内容', '2016-05-27 15:17:49', 'c21aa095-9308-e3c0-760f-880577905e4d', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('258', '2016-05-27 15:17:52 工作内容', '2016-05-27 15:17:52', '8097126b-c619-0573-e8cf-ca26c2295089', null, '2016-06-03', '2016-06-03', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('259', '2016-05-27 15:18:36 工作内容', '2016-05-27 15:18:36', '8097126b-c619-0573-e8cf-ca26c2295089', null, '2016-05-23', '2016-05-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('260', '2016-05-27 15:19:14 工作内容', '2016-05-27 15:19:14', '8097126b-c619-0573-e8cf-ca26c2295089', null, '2016-05-26', '2016-05-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('261', '2016-05-27 15:19:20 工作内容', '2016-05-27 15:19:20', '3e498e80-f829-fdcc-2ffe-64bdff8d6dce', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('262', '2016-05-27 15:20:26 工作内容', '2016-05-27 15:20:26', '8097126b-c619-0573-e8cf-ca26c2295089', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('263', '2016-05-27 15:21:51 工作内容', '2016-05-27 15:21:51', '3e498e80-f829-fdcc-2ffe-64bdff8d6dce', null, '2016-06-03', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('265', '2016-05-27 15:22:25 工作内容', '2016-05-27 15:22:25', 'fea18a1a-b939-527c-8c3d-06e747d7aa48', null, '2016-05-23', '2016-05-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('267', '2016-05-27 15:32:16 工作内容', '2016-05-27 15:32:16', 'b111c10e-e578-94ec-796e-6b75c8dda237', null, '2016-05-23', '2016-05-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('268', '2016-05-27 15:32:35 工作内容', '2016-05-27 15:32:35', 'b111c10e-e578-94ec-796e-6b75c8dda237', null, '2016-05-23', '2016-05-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('269', '2016-05-27 15:32:51 工作内容', '2016-05-27 15:32:51', 'b111c10e-e578-94ec-796e-6b75c8dda237', null, '2016-05-23', '2016-05-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('270', '2016-05-27 15:33:23 工作内容', '2016-05-27 15:33:23', 'b111c10e-e578-94ec-796e-6b75c8dda237', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('271', '2016-05-27 15:33:51 工作内容', '2016-05-27 15:33:51', 'b111c10e-e578-94ec-796e-6b75c8dda237', null, '持续', '持续', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('272', '2016-05-27 15:34:20 工作内容', '2016-05-27 15:34:20', 'b111c10e-e578-94ec-796e-6b75c8dda237', null, '2016-05-26', '2016-05-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('273', '2016-05-27 15:34:57 工作内容', '2016-05-27 15:34:57', 'b111c10e-e578-94ec-796e-6b75c8dda237', null, '2016-05-27', '2016-05-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('275', '2016-05-27 15:39:23 工作内容', '2016-05-27 15:39:23', 'b111c10e-e578-94ec-796e-6b75c8dda237', null, '2016-06-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('278', '2016-05-27 15:46:14 工作内容', '2016-05-27 15:46:14', '1e6ede70-bcd6-ac71-72bc-d4c6e3752988', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('279', '2016-05-27 15:55:28 工作内容', '2016-05-27 15:55:28', '3e498e80-f829-fdcc-2ffe-64bdff8d6dce', null, '2016-05-27', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('280', '2016-05-27 15:58:52 工作内容', '2016-05-27 15:58:52', '3a6d4302-7ada-2d35-7e1a-92f2cb5f70c3', null, '2016-05-27', '2016-05-27', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('281', '2016-05-27 16:02:08 工作内容', '2016-05-27 16:02:08', '98be8a69-f0d7-0399-e500-1807523eaa5b', null, '2016-05-27', '2016-05-27', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('282', '2016-06-03 12:45:24 工作内容', '2016-06-03 12:45:24', '81ec1482-ecc5-1c61-a2d8-88d1266dd2d5', null, '2016-05-30', '2016-05-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('283', '2016-06-03 12:45:52 工作内容', '2016-06-03 12:45:52', '81ec1482-ecc5-1c61-a2d8-88d1266dd2d5', null, '2016-05-30', '2016-05-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('284', '2016-06-03 12:47:06 工作内容', '2016-06-03 12:47:06', '81ec1482-ecc5-1c61-a2d8-88d1266dd2d5', null, '2016-06-01', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('285', '2016-06-03 12:47:47 工作内容', '2016-06-03 12:47:47', '81ec1482-ecc5-1c61-a2d8-88d1266dd2d5', null, '2016-06-02', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('286', '2016-06-03 12:49:39 工作内容', '2016-06-03 12:49:39', '37b64d5d-419d-91ac-1e3f-68e48e37a411', null, '2016-06-01', '2016-06-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('287', '2016-06-03 12:49:49 工作内容', '2016-06-03 12:49:49', 'b9b4d748-e41e-d8b5-5031-85449d9aefe4', null, '2016-05-30', '2016-05-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('288', '2016-06-03 12:50:38 工作内容', '2016-06-03 12:50:38', '11bfea93-020b-2343-795a-ca5f10346605', null, '2016-06-03', '2016-05-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('289', '2016-06-03 12:50:44 工作内容', '2016-06-03 12:50:44', '81ec1482-ecc5-1c61-a2d8-88d1266dd2d5', null, '2016-06-02', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('290', '2016-06-03 12:50:48 工作内容', '2016-06-03 12:50:48', 'b9b4d748-e41e-d8b5-5031-85449d9aefe4', null, '2016-05-31', '2016-05-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('291', '2016-06-03 12:51:32 工作内容', '2016-06-03 12:51:32', '37b64d5d-419d-91ac-1e3f-68e48e37a411', null, '2016-06-03', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('292', '2016-06-03 12:52:02 工作内容', '2016-06-03 12:52:02', 'b9b4d748-e41e-d8b5-5031-85449d9aefe4', null, '2016-06-01', '2016-06-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('293', '2016-06-03 12:52:16 工作内容', '2016-06-03 12:52:16', '5a2a51da-1957-5502-be79-1a0c9c14fa3c', null, '2016-05-30', '2016-05-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('294', '2016-06-03 12:52:17 工作内容', '2016-06-03 12:52:17', '11bfea93-020b-2343-795a-ca5f10346605', null, '2016-05-31', '2016-05-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('295', '2016-06-03 12:52:37 工作内容', '2016-06-03 12:52:37', '37b64d5d-419d-91ac-1e3f-68e48e37a411', null, '2016-06-03', '2016-06-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('296', '2016-06-03 12:53:31 工作内容', '2016-06-03 12:53:31', '11bfea93-020b-2343-795a-ca5f10346605', null, '2016-05-31', '2016-05-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('297', '2016-06-03 12:53:33 工作内容', '2016-06-03 12:53:33', 'b9b4d748-e41e-d8b5-5031-85449d9aefe4', null, '2016-06-02', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('298', '2016-06-03 12:53:59 工作内容', '2016-06-03 12:53:59', '81ec1482-ecc5-1c61-a2d8-88d1266dd2d5', null, '2016-06-07', '2016-06-07', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('299', '2016-06-03 12:54:15 工作内容', '2016-06-03 12:54:15', '37b64d5d-419d-91ac-1e3f-68e48e37a411', null, '2016-06-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('300', '2016-06-03 12:55:02 工作内容', '2016-06-03 12:55:02', '11bfea93-020b-2343-795a-ca5f10346605', null, '2016-06-03', '2016-06-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('301', '2016-06-03 12:55:38 工作内容', '2016-06-03 12:55:38', '2388651e-35ca-fd1a-ac55-b38f04d29d08', null, '2016-06-03', '2016-06-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('303', '2016-06-03 12:55:54 工作内容', '2016-06-03 12:55:54', '37b64d5d-419d-91ac-1e3f-68e48e37a411', null, '2016-06-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('304', '2016-06-03 12:56:01 工作内容', '2016-06-03 12:56:01', '81ec1482-ecc5-1c61-a2d8-88d1266dd2d5', null, '2016-06-10', '2016-06-10', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('305', '2016-06-03 12:56:25 工作内容', '2016-06-03 12:56:25', '11bfea93-020b-2343-795a-ca5f10346605', null, '2016-06-01', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('306', '2016-06-03 12:57:09 工作内容', '2016-06-03 12:57:09', '5a2a51da-1957-5502-be79-1a0c9c14fa3c', null, '2016-05-31', '2016-05-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('307', '2016-06-03 12:57:16 工作内容', '2016-06-03 12:57:16', '81ec1482-ecc5-1c61-a2d8-88d1266dd2d5', null, '2016-06-03', '2016-06-03', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('308', '2016-06-03 12:58:29 工作内容', '2016-06-03 12:58:29', '11bfea93-020b-2343-795a-ca5f10346605', null, '2016-06-01', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('309', '2016-06-03 12:58:34 工作内容', '2016-06-03 12:58:34', '37b64d5d-419d-91ac-1e3f-68e48e37a411', null, '2016-06-03', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('310', '2016-06-03 13:00:11 工作内容', '2016-06-03 13:00:11', '37b64d5d-419d-91ac-1e3f-68e48e37a411', null, '2016-06-03', '2016-06-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('311', '2016-06-03 13:00:27 工作内容', '2016-06-03 13:00:27', '5a2a51da-1957-5502-be79-1a0c9c14fa3c', null, '2016-06-01', '2016-06-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('312', '2016-06-03 13:00:28 工作内容', '2016-06-03 13:00:28', '2388651e-35ca-fd1a-ac55-b38f04d29d08', null, '2016-06-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('313', '2016-06-03 13:00:41 工作内容', '2016-06-03 13:00:41', '11bfea93-020b-2343-795a-ca5f10346605', null, '2016-06-07', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('314', '2016-06-03 13:01:07 工作内容', '2016-06-03 13:01:07', 'f46fd507-b664-0035-0edc-ce0d776b8185', null, '2016-05-30', '2016-05-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('315', '2016-06-03 13:01:49 工作内容', '2016-06-03 13:01:49', 'f46fd507-b664-0035-0edc-ce0d776b8185', null, '2016-05-31', '2016-05-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('316', '2016-06-03 13:01:52 工作内容', '2016-06-03 13:01:52', '11bfea93-020b-2343-795a-ca5f10346605', null, '2016-06-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('317', '2016-06-03 13:01:55 工作内容', '2016-06-03 13:01:55', 'e391308b-7c55-a97c-264a-159feeda3871', null, '2016-06-03', '2016-05-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('318', '2016-06-03 13:02:51 工作内容', '2016-06-03 13:02:51', 'e1a5d50a-2bb2-37cb-74ec-079c7804c2c6', null, '2016-05-31', '2016-05-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('319', '2016-06-03 13:03:23 工作内容', '2016-06-03 13:03:23', 'f46fd507-b664-0035-0edc-ce0d776b8185', null, '2016-06-03', '2016-06-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('320', '2016-06-03 13:03:40 工作内容', '2016-06-03 13:03:40', '2388651e-35ca-fd1a-ac55-b38f04d29d08', null, '2016-06-03', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('321', '2016-06-03 13:04:11 工作内容', '2016-06-03 13:04:11', 'e391308b-7c55-a97c-264a-159feeda3871', null, '2016-06-03', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('322', '2016-06-03 13:04:15 工作内容', '2016-06-03 13:04:15', 'e1a5d50a-2bb2-37cb-74ec-079c7804c2c6', null, '2016-06-01', '2016-06-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('323', '2016-06-03 13:05:34 工作内容', '2016-06-03 13:05:34', 'e1a5d50a-2bb2-37cb-74ec-079c7804c2c6', null, '2016-06-02', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('324', '2016-06-03 13:05:57 工作内容', '2016-06-03 13:05:57', 'e391308b-7c55-a97c-264a-159feeda3871', null, '2016-06-03', '2016-06-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('325', '2016-06-03 13:06:31 工作内容', '2016-06-03 13:06:31', 'e1a5d50a-2bb2-37cb-74ec-079c7804c2c6', null, '2016-06-03', '2016-06-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('326', '2016-06-03 13:08:33 工作内容', '2016-06-03 13:08:33', 'e1a5d50a-2bb2-37cb-74ec-079c7804c2c6', null, '2016-06-06', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('327', '2016-06-03 13:09:10 工作内容', '2016-06-03 13:09:10', 'e1a5d50a-2bb2-37cb-74ec-079c7804c2c6', null, '2016-06-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('328', '2016-06-03 13:10:02 工作内容', '2016-06-03 13:10:02', 'f46fd507-b664-0035-0edc-ce0d776b8185', null, '部分完成', '跨周完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('329', '2016-06-03 13:10:04 工作内容', '2016-06-03 13:10:04', '5a2a51da-1957-5502-be79-1a0c9c14fa3c', null, '2016-06-02', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('330', '2016-06-03 13:10:07 工作内容', '2016-06-03 13:10:07', 'e391308b-7c55-a97c-264a-159feeda3871', null, '2016-06-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('331', '2016-06-03 13:10:14 工作内容', '2016-06-03 13:10:14', 'e1a5d50a-2bb2-37cb-74ec-079c7804c2c6', null, '2016-06-07', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('332', '2016-06-03 13:18:04 工作内容', '2016-06-03 13:18:04', '7167ad46-3894-7f3e-d218-6246cf9f68b2', null, '2016-05-31', '2016-05-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('333', '2016-06-03 13:18:53 工作内容', '2016-06-03 13:18:53', '7167ad46-3894-7f3e-d218-6246cf9f68b2', null, '2016-06-02', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('334', '2016-06-03 13:18:56 工作内容', '2016-06-03 13:18:56', 'dbeed7f6-e7c1-7abc-5fb0-16fe2f303325', null, '2016-05-30', '2016-05-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('335', '2016-06-03 13:21:54 工作内容', '2016-06-03 13:21:54', '7167ad46-3894-7f3e-d218-6246cf9f68b2', null, '2016-06-03', '2016-06-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('336', '2016-06-03 13:23:02 工作内容', '2016-06-03 13:23:02', '7167ad46-3894-7f3e-d218-6246cf9f68b2', null, '2016-06-03', '2016-06-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('337', '2016-06-03 13:23:52 工作内容', '2016-06-03 13:23:52', 'e1a5d50a-2bb2-37cb-74ec-079c7804c2c6', null, '2016-06-03', '2016-06-03', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('338', '2016-06-03 13:28:28 工作内容', '2016-06-03 13:28:28', 'dbeed7f6-e7c1-7abc-5fb0-16fe2f303325', null, '2016-05-31', '2016-05-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('339', '2016-06-03 13:29:35 工作内容', '2016-06-03 13:29:35', 'dbeed7f6-e7c1-7abc-5fb0-16fe2f303325', null, '2016-06-01', '2016-06-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('340', '2016-06-03 13:31:19 工作内容', '2016-06-03 13:31:19', 'dbeed7f6-e7c1-7abc-5fb0-16fe2f303325', null, '2016-06-02', '2016-06-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('341', '2016-06-03 13:32:51 工作内容', '2016-06-03 13:32:51', '7167ad46-3894-7f3e-d218-6246cf9f68b2', null, '2016-06-10', '部分完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('342', '2016-06-03 13:34:17 工作内容', '2016-06-03 13:34:17', 'dbeed7f6-e7c1-7abc-5fb0-16fe2f303325', null, '2016-06-03', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('344', '2016-06-03 13:35:44 工作内容', '2016-06-03 13:35:44', '7167ad46-3894-7f3e-d218-6246cf9f68b2', null, '2016-06-10', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('345', '2016-06-03 13:36:18 工作内容', '2016-06-03 13:36:18', 'dbeed7f6-e7c1-7abc-5fb0-16fe2f303325', null, '2016-06-03', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('346', '2016-06-03 13:36:36 工作内容', '2016-06-03 13:36:36', 'dbeed7f6-e7c1-7abc-5fb0-16fe2f303325', null, '2016-06-10', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('347', '2016-06-03 14:10:26 工作内容', '2016-06-03 14:10:26', '7167ad46-3894-7f3e-d218-6246cf9f68b2', null, '2016-06-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('348', '2016-06-12 10:52:29 工作内容', '2016-06-12 10:52:29', '7900db9c-ad79-be14-80d6-7647ee2466f1', null, '2016-06-12', '2016-06-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('349', '2016-06-12 10:53:39 工作内容', '2016-06-12 10:53:39', '7900db9c-ad79-be14-80d6-7647ee2466f1', null, '2016-06-12', '2016-06-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('350', '2016-06-12 10:57:37 工作内容', '2016-06-12 10:57:37', 'dd57af48-7507-28d1-3337-7faa685a3b62', null, '2016-06-08', '2016-06-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('351', '2016-06-12 10:57:40 工作内容', '2016-06-12 10:57:40', '7900db9c-ad79-be14-80d6-7647ee2466f1', null, '2016-06-12', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('352', '2016-06-12 10:58:57 工作内容', '2016-06-12 10:58:57', '7900db9c-ad79-be14-80d6-7647ee2466f1', null, '2016-06-15', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('353', '2016-06-12 10:59:05 工作内容', '2016-06-12 10:59:05', 'dd57af48-7507-28d1-3337-7faa685a3b62', null, '2016-06-08', '2016-06-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('354', '2016-06-12 11:02:56 工作内容', '2016-06-12 11:02:56', 'dd57af48-7507-28d1-3337-7faa685a3b62', null, '2016-06-08', '2016-06-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('355', '2016-06-12 11:05:46 工作内容', '2016-06-12 11:05:46', 'b22d4049-cd22-8630-e2f2-9f4194ec3639', null, '2016-06-06', '2016-06-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('356', '2016-06-12 11:07:11 工作内容', '2016-06-12 11:07:11', 'b22d4049-cd22-8630-e2f2-9f4194ec3639', null, '2016-06-07', '2016-06-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('358', '2016-06-12 11:08:29 工作内容', '2016-06-12 11:08:29', 'dd57af48-7507-28d1-3337-7faa685a3b62', null, '2016-06-17', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('359', '2016-06-12 11:08:35 工作内容', '2016-06-12 11:08:35', 'b22d4049-cd22-8630-e2f2-9f4194ec3639', null, '2016-06-08', '2016-06-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('360', '2016-06-12 11:09:16 工作内容', '2016-06-12 11:09:16', 'ca0604e5-f554-c39a-49fa-d6bf5d316bdc', null, '2016-06-06', '2016-06-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('361', '2016-06-12 11:09:43 工作内容', '2016-06-12 11:09:43', 'b22d4049-cd22-8630-e2f2-9f4194ec3639', null, '2016-06-08', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('362', '2016-06-12 11:10:16 工作内容', '2016-06-12 11:10:16', 'ca0604e5-f554-c39a-49fa-d6bf5d316bdc', null, '2016-06-06', '2016-06-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('363', '2016-06-12 11:11:18 工作内容', '2016-06-12 11:11:18', 'b22d4049-cd22-8630-e2f2-9f4194ec3639', null, '2016-06-17', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('364', '2016-06-12 11:11:23 工作内容', '2016-06-12 11:11:23', 'ca0604e5-f554-c39a-49fa-d6bf5d316bdc', null, '2016-06-06', '2016-06-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('365', '2016-06-12 11:12:18 工作内容', '2016-06-12 11:12:18', 'ca0604e5-f554-c39a-49fa-d6bf5d316bdc', null, '2016-06-07', '2016-06-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('366', '2016-06-12 11:14:00 工作内容', '2016-06-12 11:14:00', 'ca0604e5-f554-c39a-49fa-d6bf5d316bdc', null, '2016-06-08', '2016-06-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('367', '2016-06-12 12:30:20 工作内容', '2016-06-12 12:30:20', '15e266fd-74bb-66af-194f-96323c724e4e', null, '2016-06-07', '2016-06-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('368', '2016-06-12 12:31:53 工作内容', '2016-06-12 12:31:53', '15e266fd-74bb-66af-194f-96323c724e4e', null, '2016-06-08', '2016-06-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('369', '2016-06-12 12:34:43 工作内容', '2016-06-12 12:34:43', '15e266fd-74bb-66af-194f-96323c724e4e', null, '2016-06-13', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('370', '2016-06-12 12:36:02 工作内容', '2016-06-12 12:36:02', '5b09fde9-cd02-fac5-65da-854f52baf1d8', null, '2016-06-07', '2016-06-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('371', '2016-06-12 12:36:47 工作内容', '2016-06-12 12:36:47', '5b09fde9-cd02-fac5-65da-854f52baf1d8', null, '2016-06-08', '2016-06-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('372', '2016-06-12 12:43:18 工作内容', '2016-06-12 12:43:18', '3248f6aa-0804-7d76-ad69-7d0df10b6915', null, '2016-06-06', '2016-06-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('373', '2016-06-12 12:43:24 工作内容', '2016-06-12 12:43:24', '5b09fde9-cd02-fac5-65da-854f52baf1d8', null, '2016-06-08', '2016-06-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('374', '2016-06-12 12:44:31 工作内容', '2016-06-12 12:44:31', '3248f6aa-0804-7d76-ad69-7d0df10b6915', null, '2016-06-06', '2016-06-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('375', '2016-06-12 12:45:09 工作内容', '2016-06-12 12:45:09', '5b09fde9-cd02-fac5-65da-854f52baf1d8', null, '2016-06-17', '2016-06-17', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('376', '2016-06-12 12:47:51 工作内容', '2016-06-12 12:47:51', '5b09fde9-cd02-fac5-65da-854f52baf1d8', null, '2016-06-17', '2016-06-17', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('377', '2016-06-12 13:28:48 工作内容', '2016-06-12 13:28:48', '8ab9a34a-afbb-afa4-7abb-736eb1c6b180', null, '2016-06-06', '2016-06-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('378', '2016-06-12 13:29:09 工作内容', '2016-06-12 13:29:09', '8ab9a34a-afbb-afa4-7abb-736eb1c6b180', null, '2016-06-07', '2016-06-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('379', '2016-06-12 13:29:30 工作内容', '2016-06-12 13:29:30', '8ab9a34a-afbb-afa4-7abb-736eb1c6b180', null, '2016-06-08', '2016-06-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('380', '2016-06-12 13:30:16 工作内容', '2016-06-12 13:30:16', '8ab9a34a-afbb-afa4-7abb-736eb1c6b180', null, '2016-06-12', '2016-06-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('381', '2016-06-12 16:58:27 工作内容', '2016-06-12 16:58:27', '050bc35f-d7fd-d521-4ad8-bf624abb2c37', null, '2016-06-06', '2016-06-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('382', '2016-06-12 17:01:42 工作内容', '2016-06-12 17:01:42', '050bc35f-d7fd-d521-4ad8-bf624abb2c37', null, '2016-06-17', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('383', '2016-06-12 17:02:18 工作内容', '2016-06-12 17:02:18', '050bc35f-d7fd-d521-4ad8-bf624abb2c37', null, '2016-06-08', '2016-06-08', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('384', '2016-06-17 12:43:23 工作内容', '2016-06-17 12:43:23', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-17', '2016-06-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('385', '2016-06-17 12:43:59 工作内容', '2016-06-17 12:43:59', '21f38525-77a6-d1f5-f630-0b313b28fbe4', null, '2016-06-15', '2016-06-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('386', '2016-06-17 12:46:49 工作内容', '2016-06-17 12:46:49', 'ea7c9d40-97c6-23b9-778e-e0c26cb04517', null, '2016-06-12', '2016-06-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('387', '2016-06-17 12:47:01 工作内容', '2016-06-17 12:47:01', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-17', '2016-06-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('388', '2016-06-17 12:47:43 工作内容', '2016-06-17 12:47:43', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-17', '2016-06-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('389', '2016-06-17 12:48:46 工作内容', '2016-06-17 12:48:46', 'ea7c9d40-97c6-23b9-778e-e0c26cb04517', null, '2016-06-13', '2016-06-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('390', '2016-06-17 12:50:05 工作内容', '2016-06-17 12:50:05', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('391', '2016-06-17 12:51:19 工作内容', '2016-06-17 12:51:19', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('392', '2016-06-17 12:51:56 工作内容', '2016-06-17 12:51:56', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('393', '2016-06-17 12:52:20 工作内容', '2016-06-17 12:52:20', '21f38525-77a6-d1f5-f630-0b313b28fbe4', null, '2016-06-15', '2016-06-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('394', '2016-06-17 12:53:08 工作内容', '2016-06-17 12:53:08', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('395', '2016-06-17 12:55:02 工作内容', '2016-06-17 12:55:02', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('396', '2016-06-17 12:55:30 工作内容', '2016-06-17 12:55:30', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-17', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('397', '2016-06-17 12:55:51 工作内容', '2016-06-17 12:55:51', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-17', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('398', '2016-06-17 12:56:05 工作内容', '2016-06-17 12:56:05', '215ccc2e-3cc6-2c72-c420-9898199dee7f', null, '2016-06-17', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('399', '2016-06-17 13:01:25 工作内容', '2016-06-17 13:01:25', 'ea7c9d40-97c6-23b9-778e-e0c26cb04517', null, '2016-06-14', '2016-06-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('400', '2016-06-17 13:02:08 工作内容', '2016-06-17 13:02:08', 'ea7c9d40-97c6-23b9-778e-e0c26cb04517', null, '2016-06-15', '2016-06-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('401', '2016-06-17 13:03:04 工作内容', '2016-06-17 13:03:04', 'ea7c9d40-97c6-23b9-778e-e0c26cb04517', null, '2016-06-16', '2016-06-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('402', '2016-06-17 13:03:23 工作内容', '2016-06-17 13:03:23', 'ea7c9d40-97c6-23b9-778e-e0c26cb04517', null, '2016-06-17', '2016-06-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('403', '2016-06-17 13:04:00 工作内容', '2016-06-17 13:04:00', 'ea7c9d40-97c6-23b9-778e-e0c26cb04517', null, '2016-06-17', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('404', '2016-06-17 13:04:50 工作内容', '2016-06-17 13:04:50', 'ea7c9d40-97c6-23b9-778e-e0c26cb04517', null, '2016-06-20', '2016-06-24', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('405', '2016-06-17 13:06:51 工作内容', '2016-06-17 13:06:51', '21f38525-77a6-d1f5-f630-0b313b28fbe4', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('406', '2016-06-17 13:07:42 工作内容', '2016-06-17 13:07:42', 'f127d079-9549-0f84-6dbd-ac416c101eeb', null, '2016-06-17', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('407', '2016-06-17 13:09:06 工作内容', '2016-06-17 13:09:06', '21f38525-77a6-d1f5-f630-0b313b28fbe4', null, '2016-06-17', '2016-06-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('408', '2016-06-17 13:09:27 工作内容', '2016-06-17 13:09:27', 'f127d079-9549-0f84-6dbd-ac416c101eeb', null, '2016-06-17', '2016-06-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('409', '2016-06-17 13:10:51 工作内容', '2016-06-17 13:10:51', 'f127d079-9549-0f84-6dbd-ac416c101eeb', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('410', '2016-06-17 13:14:50 工作内容', '2016-06-17 13:14:50', 'f127d079-9549-0f84-6dbd-ac416c101eeb', null, '2016-06-10', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('411', '2016-06-17 13:28:55 工作内容', '2016-06-17 13:28:55', 'bd8cd283-68bd-f0df-e322-f926b0699de2', null, '2016-06-15', '2016-06-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('412', '2016-06-17 13:29:25 工作内容', '2016-06-17 13:29:25', 'bd8cd283-68bd-f0df-e322-f926b0699de2', null, '2016-06-16', '2016-06-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('413', '2016-06-17 13:34:10 工作内容', '2016-06-17 13:34:10', 'bd8cd283-68bd-f0df-e322-f926b0699de2', null, '2016-06-14', '2016-06-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('414', '2016-06-17 13:36:19 工作内容', '2016-06-17 13:36:19', 'bd8cd283-68bd-f0df-e322-f926b0699de2', null, '2016-06-21', '2016-06-21', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('415', '2016-06-17 13:37:02 工作内容', '2016-06-17 13:37:02', 'bd8cd283-68bd-f0df-e322-f926b0699de2', null, '2016-06-24', '2016-06-24', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('416', '2016-06-17 13:57:39 工作内容', '2016-06-17 13:57:39', 'bd8cd283-68bd-f0df-e322-f926b0699de2', null, '2016-06-17', '2016-06-17', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('417', '2016-06-17 14:38:23 工作内容', '2016-06-17 14:38:23', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-06-13', '2016-06-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('418', '2016-06-17 14:39:42 工作内容', '2016-06-17 14:39:42', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-06-14', '2016-06-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('419', '2016-06-17 14:41:02 工作内容', '2016-06-17 14:41:02', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-06-15', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('420', '2016-06-17 14:50:52 工作内容', '2016-06-17 14:50:52', 'f127d079-9549-0f84-6dbd-ac416c101eeb', null, '2016-06-17', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('421', '2016-06-17 14:52:18 工作内容', '2016-06-17 14:52:18', 'f127d079-9549-0f84-6dbd-ac416c101eeb', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('422', '2016-06-17 14:55:33 工作内容', '2016-06-17 14:55:33', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-06-20', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('424', '2016-06-17 14:59:52 工作内容', '2016-06-17 14:59:52', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-12', '2016-06-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('425', '2016-06-17 15:00:29 工作内容', '2016-06-17 15:00:29', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-17', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('426', '2016-06-17 15:00:31 工作内容', '2016-06-17 15:00:31', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-06-17', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('427', '2016-06-17 15:04:20 工作内容', '2016-06-17 15:04:20', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-17', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('429', '2016-06-17 15:06:25 工作内容', '2016-06-17 15:06:25', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-06', '2016-06-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('430', '2016-06-17 15:06:56 工作内容', '2016-06-17 15:06:56', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-01-04', '2016-06-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('431', '2016-06-17 15:07:02 工作内容', '2016-06-17 15:07:02', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-12', '2016-06-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('432', '2016-06-17 15:07:48 工作内容', '2016-06-17 15:07:48', '2418e300-0241-84f0-c004-da902f1fcd42', null, '2016-06-12', '2016-06-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('433', '2016-06-17 15:07:58 工作内容', '2016-06-17 15:07:58', '2418e300-0241-84f0-c004-da902f1fcd42', null, '2016-06-13', '2016-06-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('434', '2016-06-17 15:08:29 工作内容', '2016-06-17 15:08:29', '2418e300-0241-84f0-c004-da902f1fcd42', null, '2016-06-14', '2016-06-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('435', '2016-06-17 15:08:38 工作内容', '2016-06-17 15:08:38', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-14', '2016-06-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('436', '2016-06-17 15:08:51 工作内容', '2016-06-17 15:08:51', '2418e300-0241-84f0-c004-da902f1fcd42', null, '2016-06-14', '2016-06-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('437', '2016-06-17 15:09:07 工作内容', '2016-06-17 15:09:07', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-17', '2016-06-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('438', '2016-06-17 15:09:22 工作内容', '2016-06-17 15:09:22', '2418e300-0241-84f0-c004-da902f1fcd42', null, '2016-06-15', '2016-06-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('439', '2016-06-17 15:09:52 工作内容', '2016-06-17 15:09:52', '2418e300-0241-84f0-c004-da902f1fcd42', null, '2016-06-17', '2016-06-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('440', '2016-06-17 15:10:07 工作内容', '2016-06-17 15:10:07', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-24', '2016-06-24', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('441', '2016-06-17 15:10:27 工作内容', '2016-06-17 15:10:27', '2418e300-0241-84f0-c004-da902f1fcd42', null, '跨周完成', '跨周完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('442', '2016-06-17 15:16:38 工作内容', '2016-06-17 15:16:38', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-20', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('444', '2016-06-17 15:17:37 工作内容', '2016-06-17 15:17:37', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('445', '2016-06-17 15:17:52 工作内容', '2016-06-17 15:17:52', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('446', '2016-06-17 17:03:59 工作内容', '2016-06-17 17:03:59', '38286ba3-e77d-c329-98c5-28aaaada4c31', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('447', '2016-06-17 17:05:07 工作内容', '2016-06-17 17:05:07', '08a69432-8088-74d0-dac9-2b8fdad06760', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('448', '2016-06-17 17:05:48 工作内容', '2016-06-17 17:05:48', '08a69432-8088-74d0-dac9-2b8fdad06760', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('449', '2016-06-17 17:06:35 工作内容', '2016-06-17 17:06:35', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-06-21', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('450', '2016-06-17 17:07:25 工作内容', '2016-06-17 17:07:25', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-06-22', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('451', '2016-06-17 17:08:48 工作内容', '2016-06-17 17:08:48', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-06-23', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('452', '2016-06-17 17:13:41 工作内容', '2016-06-17 17:13:41', '1d327954-2220-8b21-d791-6c945ae610b3', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('453', '2016-06-24 13:48:50 工作内容', '2016-06-24 13:48:50', '44b6c9fa-cdbc-da47-9f39-454af513ccf2', null, '2016-06-20', '2016-06-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('454', '2016-06-24 13:49:19 工作内容', '2016-06-24 13:49:19', '44b6c9fa-cdbc-da47-9f39-454af513ccf2', null, '2016-06-22', '2016-06-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('455', '2016-06-24 13:49:45 工作内容', '2016-06-24 13:49:45', '44b6c9fa-cdbc-da47-9f39-454af513ccf2', null, '2016-06-23', '2016-06-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('456', '2016-06-24 13:50:06 工作内容', '2016-06-24 13:50:06', '44b6c9fa-cdbc-da47-9f39-454af513ccf2', null, '2016-06-23', '2016-06-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('457', '2016-06-24 13:50:27 工作内容', '2016-06-24 13:50:27', '44b6c9fa-cdbc-da47-9f39-454af513ccf2', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('458', '2016-06-24 13:50:46 工作内容', '2016-06-24 13:50:46', '44b6c9fa-cdbc-da47-9f39-454af513ccf2', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('459', '2016-06-24 14:18:14 工作内容', '2016-06-24 14:18:14', 'b5e24278-e5bc-15c6-cb35-aa0c8e397a7a', null, '2016-06-21', '2016-06-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('460', '2016-06-24 14:19:13 工作内容', '2016-06-24 14:19:13', '55f1a369-3327-28e9-9830-dbe6077b0c8b', null, '2016-06-24', '2016-06-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('461', '2016-06-24 14:19:33 工作内容', '2016-06-24 14:19:33', 'b5e24278-e5bc-15c6-cb35-aa0c8e397a7a', null, '2016-06-23', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('462', '2016-06-24 14:19:55 工作内容', '2016-06-24 14:19:55', '55f1a369-3327-28e9-9830-dbe6077b0c8b', null, '2016-06-24', '2016-06-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('463', '2016-06-24 14:20:45 工作内容', '2016-06-24 14:20:45', 'b8d32a13-8d56-1b65-b39c-28d33a36bc6b', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('464', '2016-06-24 14:20:56 工作内容', '2016-06-24 14:20:56', 'b5e24278-e5bc-15c6-cb35-aa0c8e397a7a', null, '2016-06-23', '2016-06-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('465', '2016-06-24 14:21:07 工作内容', '2016-06-24 14:21:07', '55f1a369-3327-28e9-9830-dbe6077b0c8b', null, '2016-06-24', '2016-06-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('466', '2016-06-24 14:22:30 工作内容', '2016-06-24 14:22:30', 'b5e24278-e5bc-15c6-cb35-aa0c8e397a7a', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('467', '2016-06-24 14:23:13 工作内容', '2016-06-24 14:23:13', '51c87f75-7af9-20b1-64b7-ba4cf4a3faea', null, '2016-06-20', '2016-06-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('468', '2016-06-24 14:23:26 工作内容', '2016-06-24 14:23:26', '55f1a369-3327-28e9-9830-dbe6077b0c8b', null, '2016-06-28', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('469', '2016-06-24 14:23:55 工作内容', '2016-06-24 14:23:55', '55f1a369-3327-28e9-9830-dbe6077b0c8b', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('470', '2016-06-24 14:24:15 工作内容', '2016-06-24 14:24:15', '84570a62-6399-ffaf-ddd2-69ea4fe964f5', null, '2016-06-21', '2016-06-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('471', '2016-06-24 14:24:29 工作内容', '2016-06-24 14:24:29', '51c87f75-7af9-20b1-64b7-ba4cf4a3faea', null, '2016-06-21', '2016-06-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('472', '2016-06-24 14:24:33 工作内容', '2016-06-24 14:24:33', 'b5e24278-e5bc-15c6-cb35-aa0c8e397a7a', null, '2016-06-28', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('473', '2016-06-24 14:25:02 工作内容', '2016-06-24 14:25:02', '84570a62-6399-ffaf-ddd2-69ea4fe964f5', null, '2016-06-23', '2016-06-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('474', '2016-06-24 14:25:39 工作内容', '2016-06-24 14:25:39', '51c87f75-7af9-20b1-64b7-ba4cf4a3faea', null, '2016-06-22', '2016-06-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('475', '2016-06-24 14:26:01 工作内容', '2016-06-24 14:26:01', 'b5e24278-e5bc-15c6-cb35-aa0c8e397a7a', null, '2016-06-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('476', '2016-06-24 14:26:54 工作内容', '2016-06-24 14:26:54', 'b5e24278-e5bc-15c6-cb35-aa0c8e397a7a', null, '2016-06-24', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('477', '2016-06-24 14:27:58 工作内容', '2016-06-24 14:27:58', '51c87f75-7af9-20b1-64b7-ba4cf4a3faea', null, '2016-06-23', '2016-06-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('478', '2016-06-24 14:28:27 工作内容', '2016-06-24 14:28:27', '51c87f75-7af9-20b1-64b7-ba4cf4a3faea', null, '2016-06-24', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('479', '2016-06-24 14:28:32 工作内容', '2016-06-24 14:28:32', '84570a62-6399-ffaf-ddd2-69ea4fe964f5', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('480', '2016-06-24 14:29:04 工作内容', '2016-06-24 14:29:04', 'b8d32a13-8d56-1b65-b39c-28d33a36bc6b', null, '2016-07-01', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('481', '2016-06-24 14:29:16 工作内容', '2016-06-24 14:29:16', '9c48e0df-9b38-1d57-73e3-09c8e978b9dc', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('482', '2016-06-24 14:29:20 工作内容', '2016-06-24 14:29:20', '51c87f75-7af9-20b1-64b7-ba4cf4a3faea', null, '2016-06-24', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('483', '2016-06-24 14:29:32 工作内容', '2016-06-24 14:29:32', 'b8d32a13-8d56-1b65-b39c-28d33a36bc6b', null, '2016-07-01', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('484', '2016-06-24 14:30:18 工作内容', '2016-06-24 14:30:18', '9c48e0df-9b38-1d57-73e3-09c8e978b9dc', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('485', '2016-06-24 14:30:33 工作内容', '2016-06-24 14:30:33', 'b5e24278-e5bc-15c6-cb35-aa0c8e397a7a', null, '2016-07-01', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('486', '2016-06-24 14:30:42 工作内容', '2016-06-24 14:30:42', '2e7688e8-edb3-cf87-b2fa-c34e90672653', null, '2016-06-22', '2016-06-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('487', '2016-06-24 14:30:59 工作内容', '2016-06-24 14:30:59', '51c87f75-7af9-20b1-64b7-ba4cf4a3faea', null, '2016-07-01', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('488', '2016-06-24 14:31:39 工作内容', '2016-06-24 14:31:39', '481fd16f-625c-2739-bfe2-b56c31050a76', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('489', '2016-06-24 14:32:07 工作内容', '2016-06-24 14:32:07', '481fd16f-625c-2739-bfe2-b56c31050a76', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('490', '2016-06-24 14:32:56 工作内容', '2016-06-24 14:32:56', '481fd16f-625c-2739-bfe2-b56c31050a76', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('491', '2016-06-24 14:33:03 工作内容', '2016-06-24 14:33:03', '2e7688e8-edb3-cf87-b2fa-c34e90672653', null, '2016-06-23', '2016-06-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('492', '2016-06-24 14:33:20 工作内容', '2016-06-24 14:33:20', '84570a62-6399-ffaf-ddd2-69ea4fe964f5', null, '2016-06-28', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('493', '2016-06-24 14:34:06 工作内容', '2016-06-24 14:34:06', '481fd16f-625c-2739-bfe2-b56c31050a76', null, '2016-06-24', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('495', '2016-06-24 14:35:04 工作内容', '2016-06-24 14:35:04', '481fd16f-625c-2739-bfe2-b56c31050a76', null, '2016-06-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('496', '2016-06-24 14:36:14 工作内容', '2016-06-24 14:36:14', '481fd16f-625c-2739-bfe2-b56c31050a76', null, '2016-06-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('498', '2016-06-24 14:38:27 工作内容', '2016-06-24 14:38:27', '481fd16f-625c-2739-bfe2-b56c31050a76', null, '2016-06-24', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('499', '2016-06-24 14:42:13 工作内容', '2016-06-24 14:42:13', '84570a62-6399-ffaf-ddd2-69ea4fe964f5', null, '暂未确定时间', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('500', '2016-06-24 14:45:20 工作内容', '2016-06-24 14:45:20', '84570a62-6399-ffaf-ddd2-69ea4fe964f5', null, '2016-06-24', '2016-06-24', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('501', '2016-06-24 15:01:05 工作内容', '2016-06-24 15:01:05', 'ae68ea4a-c7d2-a556-678d-7ec69b4492ee', null, '2016-06-20', '2016-06-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('502', '2016-06-24 15:02:24 工作内容', '2016-06-24 15:02:24', 'ae68ea4a-c7d2-a556-678d-7ec69b4492ee', null, '2016-06-20', '2016-06-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('503', '2016-06-24 15:03:46 工作内容', '2016-06-24 15:03:46', 'ae68ea4a-c7d2-a556-678d-7ec69b4492ee', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('504', '2016-06-24 15:04:29 工作内容', '2016-06-24 15:04:29', 'ae68ea4a-c7d2-a556-678d-7ec69b4492ee', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('505', '2016-06-24 15:04:48 工作内容', '2016-06-24 15:04:48', 'ae68ea4a-c7d2-a556-678d-7ec69b4492ee', null, '2016-06-24', '2016-06-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('506', '2016-06-24 15:07:46 工作内容', '2016-06-24 15:07:46', 'ae68ea4a-c7d2-a556-678d-7ec69b4492ee', null, '2016-06-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('507', '2016-06-24 15:08:09 工作内容', '2016-06-24 15:08:09', 'ae68ea4a-c7d2-a556-678d-7ec69b4492ee', null, '2016-06-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('508', '2016-06-24 15:08:55 工作内容', '2016-06-24 15:08:55', 'ae68ea4a-c7d2-a556-678d-7ec69b4492ee', null, '2016-06-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('509', '2016-06-24 15:22:09 工作内容', '2016-06-24 15:22:09', '84570a62-6399-ffaf-ddd2-69ea4fe964f5', null, '2016-06-30', '2016-06-30', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('510', '2016-06-24 16:57:32 工作内容', '2016-06-24 16:57:32', '2e7688e8-edb3-cf87-b2fa-c34e90672653', null, '2016-06-29', '2016-06-29', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('511', '2016-06-24 16:58:18 工作内容', '2016-06-24 16:58:18', '2e7688e8-edb3-cf87-b2fa-c34e90672653', null, '2016-06-30', '2016-06-30', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('512', '2016-06-24 16:59:40 工作内容', '2016-06-24 16:59:40', '2e7688e8-edb3-cf87-b2fa-c34e90672653', null, '2016-07-01', '2016-07-01', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('513', '2016-06-24 17:00:00 工作内容', '2016-06-24 17:00:00', '55f1a369-3327-28e9-9830-dbe6077b0c8b', null, '2016-07-01', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('514', '2016-06-24 17:02:30 工作内容', '2016-06-24 17:02:30', '481fd16f-625c-2739-bfe2-b56c31050a76', null, '2016-06-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('515', '2016-07-01 15:03:11 工作内容', '2016-07-01 15:03:11', '333572cf-14fd-f5bd-8b74-697c855f08f6', null, '2016-06-27', '2016-06-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('516', '2016-07-01 15:03:31 工作内容', '2016-07-01 15:03:31', '8a3a72fc-b7bc-701b-3d7a-a91cc38607ec', null, '2016-07-04', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('517', '2016-07-01 15:03:46 工作内容', '2016-07-01 15:03:46', '333572cf-14fd-f5bd-8b74-697c855f08f6', null, '2016-06-28', '2016-06-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('518', '2016-07-01 15:03:51 工作内容', '2016-07-01 15:03:51', '8a3a72fc-b7bc-701b-3d7a-a91cc38607ec', null, '2016-07-05', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('519', '2016-07-01 15:03:54 工作内容', '2016-07-01 15:03:54', '34b28c7f-424c-5d9f-e25d-be78799af53f', null, '2016-06-28', '2016-06-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('520', '2016-07-01 15:04:09 工作内容', '2016-07-01 15:04:09', '8a3a72fc-b7bc-701b-3d7a-a91cc38607ec', null, '2016-07-06', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('521', '2016-07-01 15:04:11 工作内容', '2016-07-01 15:04:11', '333572cf-14fd-f5bd-8b74-697c855f08f6', null, '2016-06-29', '2016-06-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('522', '2016-07-01 15:04:20 工作内容', '2016-07-01 15:04:20', '16482650-7f8c-fb8d-da55-90bbfa1e4a2a', null, '2016-06-29', '2016-06-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('523', '2016-07-01 15:04:34 工作内容', '2016-07-01 15:04:34', '333572cf-14fd-f5bd-8b74-697c855f08f6', null, '2016-06-30', '2016-06-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('524', '2016-07-01 15:04:36 工作内容', '2016-07-01 15:04:36', '8a3a72fc-b7bc-701b-3d7a-a91cc38607ec', null, '2016-07-07', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('525', '2016-07-01 15:04:56 工作内容', '2016-07-01 15:04:56', '333572cf-14fd-f5bd-8b74-697c855f08f6', null, '2016-07-01', '2016-07-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('526', '2016-07-01 15:04:59 工作内容', '2016-07-01 15:04:59', '8a3a72fc-b7bc-701b-3d7a-a91cc38607ec', null, '2016-07-08', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('527', '2016-07-01 15:05:28 工作内容', '2016-07-01 15:05:28', '34b28c7f-424c-5d9f-e25d-be78799af53f', null, '2016-06-30', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('528', '2016-07-01 15:06:49 工作内容', '2016-07-01 15:06:49', '83277caf-5ea6-b83d-d51c-41a727900826', null, '2016-06-28', '2016-06-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('529', '2016-07-01 15:07:18 工作内容', '2016-07-01 15:07:18', '8a3a72fc-b7bc-701b-3d7a-a91cc38607ec', null, '2016-07-08', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('530', '2016-07-01 15:07:59 工作内容', '2016-07-01 15:07:59', '48aaac81-928c-532d-5f63-0f4d5d9f4b80', null, '2016-06-27', '2016-06-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('531', '2016-07-01 15:08:22 工作内容', '2016-07-01 15:08:22', '34b28c7f-424c-5d9f-e25d-be78799af53f', null, '2016-07-06', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('532', '2016-07-01 15:08:41 工作内容', '2016-07-01 15:08:41', '48aaac81-928c-532d-5f63-0f4d5d9f4b80', null, '2016-06-27', '2016-06-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('533', '2016-07-01 15:09:36 工作内容', '2016-07-01 15:09:36', '83277caf-5ea6-b83d-d51c-41a727900826', null, '2016-06-29', '2016-06-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('534', '2016-07-01 15:09:58 工作内容', '2016-07-01 15:09:58', '83277caf-5ea6-b83d-d51c-41a727900826', null, '2016-06-30', '2016-06-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('535', '2016-07-01 15:10:57 工作内容', '2016-07-01 15:10:57', '34b28c7f-424c-5d9f-e25d-be78799af53f', null, '2016-06-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('536', '2016-07-01 15:11:17 工作内容', '2016-07-01 15:11:17', '48aaac81-928c-532d-5f63-0f4d5d9f4b80', null, '2016-07-08', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('537', '2016-07-01 15:11:27 工作内容', '2016-07-01 15:11:27', '16482650-7f8c-fb8d-da55-90bbfa1e4a2a', null, '2016-06-30', '2016-06-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('538', '2016-07-01 15:11:37 工作内容', '2016-07-01 15:11:37', '48aaac81-928c-532d-5f63-0f4d5d9f4b80', null, '2016-07-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('539', '2016-07-01 15:13:57 工作内容', '2016-07-01 15:13:57', '8a3a72fc-b7bc-701b-3d7a-a91cc38607ec', null, '2016-07-15', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('540', '2016-07-01 15:14:22 工作内容', '2016-07-01 15:14:22', 'a15a6361-0ba8-30be-5a74-6677960932dc', null, '2016-06-30', '2016-06-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('541', '2016-07-01 15:14:46 工作内容', '2016-07-01 15:14:46', 'a15a6361-0ba8-30be-5a74-6677960932dc', null, '2016-06-28', '2016-06-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('542', '2016-07-01 15:15:17 工作内容', '2016-07-01 15:15:17', '34b28c7f-424c-5d9f-e25d-be78799af53f', null, '2016-06-28', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('543', '2016-07-01 15:16:34 工作内容', '2016-07-01 15:16:34', 'a15a6361-0ba8-30be-5a74-6677960932dc', null, '2016-07-01', '2016-07-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('544', '2016-07-01 15:17:51 工作内容', '2016-07-01 15:17:51', '3f4e10c4-f4d4-4d55-fe41-f266202bfcc1', null, '2016-06-30', '2016-06-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('545', '2016-07-01 15:18:13 工作内容', '2016-07-01 15:18:13', '3f4e10c4-f4d4-4d55-fe41-f266202bfcc1', null, '2016-06-28', '2016-06-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('546', '2016-07-01 15:18:38 工作内容', '2016-07-01 15:18:38', '83277caf-5ea6-b83d-d51c-41a727900826', null, '2016-07-01', '2016-07-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('548', '2016-07-01 15:19:36 工作内容', '2016-07-01 15:19:36', '3f4e10c4-f4d4-4d55-fe41-f266202bfcc1', null, '2016-07-01', '2016-07-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('549', '2016-07-01 15:19:57 工作内容', '2016-07-01 15:19:57', '83277caf-5ea6-b83d-d51c-41a727900826', null, '2016-07-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('550', '2016-07-01 15:20:43 工作内容', '2016-07-01 15:20:43', '3f4e10c4-f4d4-4d55-fe41-f266202bfcc1', null, '2016-07-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('551', '2016-07-01 15:20:47 工作内容', '2016-07-01 15:20:47', '16482650-7f8c-fb8d-da55-90bbfa1e4a2a', null, '2016-07-01', '2016-07-01', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('552', '2016-07-01 15:20:48 工作内容', '2016-07-01 15:20:48', '83277caf-5ea6-b83d-d51c-41a727900826', null, '2016-07-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('553', '2016-07-01 15:22:53 工作内容', '2016-07-01 15:22:53', '16482650-7f8c-fb8d-da55-90bbfa1e4a2a', null, '2016-07-08', '2016-07-08', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('554', '2016-07-01 15:30:05 工作内容', '2016-07-01 15:30:05', '16482650-7f8c-fb8d-da55-90bbfa1e4a2a', null, '2016-07-04', '2016-07-04', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('555', '2016-07-01 15:30:41 工作内容', '2016-07-01 15:30:41', '16482650-7f8c-fb8d-da55-90bbfa1e4a2a', null, '2016-07-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('556', '2016-07-01 15:32:49 工作内容', '2016-07-01 15:32:49', '1e2c89a0-5468-d75a-b601-299e3d549065', null, '2016-07-01', '2016-07-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('557', '2016-07-01 15:33:37 工作内容', '2016-07-01 15:33:37', '93265a14-b0f7-0d5b-cb11-5172a8c0c5ea', null, '2016-07-01', '2016-07-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('558', '2016-07-01 15:33:39 工作内容', '2016-07-01 15:33:39', '1e2c89a0-5468-d75a-b601-299e3d549065', null, '2016-07-01', '2016-07-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('559', '2016-07-01 15:34:10 工作内容', '2016-07-01 15:34:10', '93265a14-b0f7-0d5b-cb11-5172a8c0c5ea', null, '2016-07-01', '2016-07-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('560', '2016-07-01 15:34:14 工作内容', '2016-07-01 15:34:14', '1e2c89a0-5468-d75a-b601-299e3d549065', null, '2016-06-27', '2016-06-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('561', '2016-07-01 15:34:56 工作内容', '2016-07-01 15:34:56', '1e2c89a0-5468-d75a-b601-299e3d549065', null, '2016-07-01', '2016-07-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('562', '2016-07-01 15:35:41 工作内容', '2016-07-01 15:35:41', '93265a14-b0f7-0d5b-cb11-5172a8c0c5ea', null, '2016-07-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('563', '2016-07-01 15:35:48 工作内容', '2016-07-01 15:35:48', '1e2c89a0-5468-d75a-b601-299e3d549065', null, '2016-07-06', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('564', '2016-07-01 15:36:37 工作内容', '2016-07-01 15:36:37', '93265a14-b0f7-0d5b-cb11-5172a8c0c5ea', null, '2016-07-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('565', '2016-07-01 15:36:49 工作内容', '2016-07-01 15:36:49', '1e2c89a0-5468-d75a-b601-299e3d549065', null, '2016-07-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('566', '2016-07-01 15:38:16 工作内容', '2016-07-01 15:38:16', '93265a14-b0f7-0d5b-cb11-5172a8c0c5ea', null, '2016-07-01', '2016-07-01', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('567', '2016-07-01 09:18:56 工作内容', '2016-07-01 09:18:56', '34b28c7f-424c-5d9f-e25d-be78799af53f', null, '2016-07-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('568', '2016-07-08 12:42:34 工作内容', '2016-07-08 12:42:34', '5366c3d9-41af-99bb-95a1-13533c25add0', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('569', '2016-07-08 12:44:00 工作内容', '2016-07-08 12:44:00', '5366c3d9-41af-99bb-95a1-13533c25add0', null, '2016-07-07', '2016-07-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('570', '2016-07-08 12:44:34 工作内容', '2016-07-08 12:44:34', '5366c3d9-41af-99bb-95a1-13533c25add0', null, '2016-07-07', '2016-07-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('571', '2016-07-08 12:45:37 工作内容', '2016-07-08 12:45:37', '5366c3d9-41af-99bb-95a1-13533c25add0', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('572', '2016-07-08 12:49:03 工作内容', '2016-07-08 12:49:03', 'b7671863-e591-deb6-3bc6-0463d1e02e35', null, '2016-07-04', '2016-07-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('573', '2016-07-08 12:49:46 工作内容', '2016-07-08 12:49:46', 'b7671863-e591-deb6-3bc6-0463d1e02e35', null, '2016-07-05', '2016-07-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('574', '2016-07-08 12:50:51 工作内容', '2016-07-08 12:50:51', '79a80034-ed81-f793-046a-6a7f8513027d', null, '2016-07-04', '2016-07-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('575', '2016-07-08 12:51:39 工作内容', '2016-07-08 12:51:39', '79a80034-ed81-f793-046a-6a7f8513027d', null, '2016-07-06', '2016-07-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('576', '2016-07-08 12:53:32 工作内容', '2016-07-08 12:53:32', '79a80034-ed81-f793-046a-6a7f8513027d', null, '2016-07-08', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('577', '2016-07-08 12:54:29 工作内容', '2016-07-08 12:54:29', '79a80034-ed81-f793-046a-6a7f8513027d', null, '2016-07-15', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('579', '2016-07-08 12:59:25 工作内容', '2016-07-08 12:59:25', '79a80034-ed81-f793-046a-6a7f8513027d', null, '2016-07-15', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('580', '2016-07-08 13:04:37 工作内容', '2016-07-08 13:04:37', '79a80034-ed81-f793-046a-6a7f8513027d', null, '2016-07-15', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('581', '2016-07-08 13:40:52 工作内容', '2016-07-08 13:40:52', 'ea0885e1-4443-2868-1f77-5d764a2e27e3', null, '2016-07-04', '2016-07-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('582', '2016-07-08 13:41:12 工作内容', '2016-07-08 13:41:12', '7f519e53-670e-bed9-08e3-b739ab9635be', null, '2016-07-04', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('583', '2016-07-08 13:41:30 工作内容', '2016-07-08 13:41:30', 'ea0885e1-4443-2868-1f77-5d764a2e27e3', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('584', '2016-07-08 13:41:32 工作内容', '2016-07-08 13:41:32', '7f519e53-670e-bed9-08e3-b739ab9635be', null, '2016-07-05', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('585', '2016-07-08 13:41:55 工作内容', '2016-07-08 13:41:55', '7f519e53-670e-bed9-08e3-b739ab9635be', null, '2016-07-06', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('586', '2016-07-08 13:42:11 工作内容', '2016-07-08 13:42:11', '7f519e53-670e-bed9-08e3-b739ab9635be', null, '2016-07-07', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('587', '2016-07-08 13:42:30 工作内容', '2016-07-08 13:42:30', '7f519e53-670e-bed9-08e3-b739ab9635be', null, '2016-07-08', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('588', '2016-07-08 13:43:08 工作内容', '2016-07-08 13:43:08', '7f519e53-670e-bed9-08e3-b739ab9635be', null, '2016-07-11', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('589', '2016-07-08 13:43:14 工作内容', '2016-07-08 13:43:14', 'ea0885e1-4443-2868-1f77-5d764a2e27e3', null, '2016-07-06', '2016-07-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('590', '2016-07-08 13:44:33 工作内容', '2016-07-08 13:44:33', 'ea0885e1-4443-2868-1f77-5d764a2e27e3', null, '2016-07-07', '2016-07-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('591', '2016-07-08 13:45:07 工作内容', '2016-07-08 13:45:07', 'ea0885e1-4443-2868-1f77-5d764a2e27e3', null, '2016-07-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('592', '2016-07-08 13:45:29 工作内容', '2016-07-08 13:45:29', 'ea0885e1-4443-2868-1f77-5d764a2e27e3', null, '2016-07-15', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('593', '2016-07-08 13:55:02 工作内容', '2016-07-08 13:55:02', '5366c3d9-41af-99bb-95a1-13533c25add0', null, '2016-07-15', '2016-07-15', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('594', '2016-07-08 13:59:26 工作内容', '2016-07-08 13:59:26', '654f40a5-a452-d045-831d-99e81a482369', null, '2016-07-04', '2016-07-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('595', '2016-07-08 13:59:50 工作内容', '2016-07-08 13:59:50', '654f40a5-a452-d045-831d-99e81a482369', null, '2016-07-05', '2016-07-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('596', '2016-07-08 14:00:48 工作内容', '2016-07-08 14:00:48', '654f40a5-a452-d045-831d-99e81a482369', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('597', '2016-07-08 14:10:29 工作内容', '2016-07-08 14:10:29', '7f519e53-670e-bed9-08e3-b739ab9635be', null, '2016-07-15', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('598', '2016-07-08 14:11:23 工作内容', '2016-07-08 14:11:23', '70b29e19-7005-ad97-0c92-6f0e639521da', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('599', '2016-07-08 14:23:18 工作内容', '2016-07-08 14:23:18', '79a80034-ed81-f793-046a-6a7f8513027d', null, '2016-07-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('600', '2016-07-08 14:42:29 工作内容', '2016-07-08 14:42:29', '70b29e19-7005-ad97-0c92-6f0e639521da', null, '2016-07-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('601', '2016-07-08 14:42:55 工作内容', '2016-07-08 14:42:55', '70b29e19-7005-ad97-0c92-6f0e639521da', null, '2016-07-13', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('602', '2016-07-08 14:43:45 工作内容', '2016-07-08 14:43:45', '70b29e19-7005-ad97-0c92-6f0e639521da', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('603', '2016-07-08 14:47:06 工作内容', '2016-07-08 14:47:06', '70b29e19-7005-ad97-0c92-6f0e639521da', null, '2016-07-08', '2016-07-08', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('604', '2016-07-08 14:50:10 工作内容', '2016-07-08 14:50:10', 'a9ec1b94-de9d-869a-28eb-09735efc601e', '6', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('605', '2016-07-08 14:51:46 工作内容', '2016-07-08 14:51:46', '8175f8c4-84ad-1775-25d3-533d600dd893', null, '2016-07-04', '2016-07-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('606', '2016-07-08 14:52:04 工作内容', '2016-07-08 14:52:04', '8175f8c4-84ad-1775-25d3-533d600dd893', null, '2016-07-05', '2016-07-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('607', '2016-07-08 14:52:24 工作内容', '2016-07-08 14:52:24', '8175f8c4-84ad-1775-25d3-533d600dd893', null, '2016-07-06', '2016-07-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('608', '2016-07-08 14:52:45 工作内容', '2016-07-08 14:52:45', '8175f8c4-84ad-1775-25d3-533d600dd893', null, '2016-07-07', '2016-07-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('609', '2016-07-08 14:53:10 工作内容', '2016-07-08 14:53:10', '8175f8c4-84ad-1775-25d3-533d600dd893', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('610', '2016-07-08 14:53:26 工作内容', '2016-07-08 14:53:26', '8175f8c4-84ad-1775-25d3-533d600dd893', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('611', '2016-07-08 14:53:50 工作内容', '2016-07-08 14:53:50', '8175f8c4-84ad-1775-25d3-533d600dd893', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('612', '2016-07-08 15:21:24 工作内容', '2016-07-08 15:21:24', '52392445-6d9a-279c-9343-4cc780d93d81', null, '2016-07-06', '2016-07-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('613', '2016-07-08 15:23:30 工作内容', '2016-07-08 15:23:30', '52392445-6d9a-279c-9343-4cc780d93d81', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('614', '2016-07-08 15:24:51 工作内容', '2016-07-08 15:24:51', '52392445-6d9a-279c-9343-4cc780d93d81', null, '2016-07-08', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('615', '2016-07-08 15:26:23 工作内容', '2016-07-08 15:26:23', '52392445-6d9a-279c-9343-4cc780d93d81', null, '2016-07-13', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('616', '2016-07-08 15:36:06 工作内容', '2016-07-08 15:36:06', '51ea720b-6e3e-764e-03ec-6d509782844f', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('617', '2016-07-08 15:36:30 工作内容', '2016-07-08 15:36:30', '51ea720b-6e3e-764e-03ec-6d509782844f', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('618', '2016-07-08 15:36:58 工作内容', '2016-07-08 15:36:58', '51ea720b-6e3e-764e-03ec-6d509782844f', null, '2016-07-07', '2016-07-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('619', '2016-07-08 15:37:26 工作内容', '2016-07-08 15:37:26', '52392445-6d9a-279c-9343-4cc780d93d81', null, '2016-07-15', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('620', '2016-07-08 15:37:32 工作内容', '2016-07-08 15:37:32', '51ea720b-6e3e-764e-03ec-6d509782844f', null, '2016-07-13', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('621', '2016-07-08 15:38:20 工作内容', '2016-07-08 15:38:20', '51ea720b-6e3e-764e-03ec-6d509782844f', null, '2016-07-08', '2016-07-08', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('622', '2016-07-08 15:40:02 工作内容', '2016-07-08 15:40:02', '2ea2ff1a-c587-ee73-c216-156b9581c0ec', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('623', '2016-07-08 15:40:50 工作内容', '2016-07-08 15:40:50', '2ea2ff1a-c587-ee73-c216-156b9581c0ec', null, '2016-07-06', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('624', '2016-07-08 15:42:20 工作内容', '2016-07-08 15:42:20', '2ea2ff1a-c587-ee73-c216-156b9581c0ec', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('625', '2016-07-08 15:43:40 工作内容', '2016-07-08 15:43:40', '2ea2ff1a-c587-ee73-c216-156b9581c0ec', null, '2016-07-08', '2016-07-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('626', '2016-07-08 15:44:06 工作内容', '2016-07-08 15:44:06', '2ea2ff1a-c587-ee73-c216-156b9581c0ec', null, '2016-07-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('627', '2016-07-08 15:46:23 工作内容', '2016-07-08 15:46:23', '2ea2ff1a-c587-ee73-c216-156b9581c0ec', null, '2016-07-15', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('628', '2016-07-08 15:56:32 工作内容', '2016-07-08 15:56:32', '52392445-6d9a-279c-9343-4cc780d93d81', null, '2016-07-15', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('629', '2016-07-15 12:34:19 工作内容', '2016-07-15 12:34:19', '8e67b994-6433-7e82-3c32-86b033f52608', null, '2016-07-11', '2016-07-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('630', '2016-07-15 12:35:25 工作内容', '2016-07-15 12:35:25', '8e67b994-6433-7e82-3c32-86b033f52608', null, '2016-07-12', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('631', '2016-07-15 12:36:06 工作内容', '2016-07-15 12:36:06', '8e67b994-6433-7e82-3c32-86b033f52608', null, '2016-07-13', '2016-07-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('632', '2016-07-15 12:36:57 工作内容', '2016-07-15 12:36:57', '8e67b994-6433-7e82-3c32-86b033f52608', null, '2016-07-14', '2016-07-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('633', '2016-07-15 12:38:36 工作内容', '2016-07-15 12:38:36', '3733e028-2c4a-6b88-7edc-4231b1b97a5b', null, '2016-07-12', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('634', '2016-07-15 12:40:36 工作内容', '2016-07-15 12:40:36', '3733e028-2c4a-6b88-7edc-4231b1b97a5b', null, '2016-07-15', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('635', '2016-07-15 12:41:44 工作内容', '2016-07-15 12:41:44', '3733e028-2c4a-6b88-7edc-4231b1b97a5b', null, '2016-07-22', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('636', '2016-07-15 12:44:07 工作内容', '2016-07-15 12:44:07', '3733e028-2c4a-6b88-7edc-4231b1b97a5b', null, '2016-07-14', '2016-07-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('637', '2016-07-15 12:44:57 工作内容', '2016-07-15 12:44:57', '3733e028-2c4a-6b88-7edc-4231b1b97a5b', null, '2016-07-22', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('638', '2016-07-15 12:45:04 工作内容', '2016-07-15 12:45:04', 'f9ba9d94-bb11-7c9b-0261-76c7f54adfdd', null, '2016-07-11', '2016-07-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('639', '2016-07-15 12:45:34 工作内容', '2016-07-15 12:45:34', 'f9ba9d94-bb11-7c9b-0261-76c7f54adfdd', null, '2016-07-12', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('640', '2016-07-15 12:45:56 工作内容', '2016-07-15 12:45:56', 'f9ba9d94-bb11-7c9b-0261-76c7f54adfdd', null, '2016-07-12', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('641', '2016-07-15 12:46:21 工作内容', '2016-07-15 12:46:21', 'f9ba9d94-bb11-7c9b-0261-76c7f54adfdd', null, '2016-07-13', '2016-07-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('642', '2016-07-15 12:46:45 工作内容', '2016-07-15 12:46:45', '29b5f634-5f15-8be5-aa0c-39927ee4d757', null, '2016-07-12', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('643', '2016-07-15 12:47:13 工作内容', '2016-07-15 12:47:13', 'f9ba9d94-bb11-7c9b-0261-76c7f54adfdd', null, '2016-07-13', '2016-07-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('644', '2016-07-15 12:47:17 工作内容', '2016-07-15 12:47:17', '29b5f634-5f15-8be5-aa0c-39927ee4d757', null, '2016-07-14', '2016-07-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('645', '2016-07-15 12:47:39 工作内容', '2016-07-15 12:47:39', 'f9ba9d94-bb11-7c9b-0261-76c7f54adfdd', null, '2016-07-14', '2016-07-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('646', '2016-07-15 12:48:45 工作内容', '2016-07-15 12:48:45', '29b5f634-5f15-8be5-aa0c-39927ee4d757', null, '2016-07-14', '2016-07-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('647', '2016-07-15 12:51:06 工作内容', '2016-07-15 12:51:06', '29b5f634-5f15-8be5-aa0c-39927ee4d757', null, '2016-07-15', '2016-07-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('648', '2016-07-15 12:51:49 工作内容', '2016-07-15 12:51:49', '07906c43-970d-27d4-765a-1eb9823afa05', null, '2016-07-13', '2016-07-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('649', '2016-07-15 12:52:20 工作内容', '2016-07-15 12:52:20', '07906c43-970d-27d4-765a-1eb9823afa05', null, '2016-07-14', '2016-07-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('650', '2016-07-15 12:52:42 工作内容', '2016-07-15 12:52:42', 'fedf5e14-2182-aa74-8802-3bbb237e7538', null, '2016-07-11', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('651', '2016-07-15 12:52:59 工作内容', '2016-07-15 12:52:59', 'fedf5e14-2182-aa74-8802-3bbb237e7538', null, '2016-07-12', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('652', '2016-07-15 12:53:02 工作内容', '2016-07-15 12:53:02', '29b5f634-5f15-8be5-aa0c-39927ee4d757', null, '2016-07-15', '2016-07-15', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('653', '2016-07-15 12:53:14 工作内容', '2016-07-15 12:53:14', '07906c43-970d-27d4-765a-1eb9823afa05', null, '2016-07-15', '2016-07-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('654', '2016-07-15 12:55:31 工作内容', '2016-07-15 12:55:31', '7bf69b33-c99b-a47e-a268-b78cc47112a8', null, '2016-07-13', '2016-07-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('655', '2016-07-15 12:56:42 工作内容', '2016-07-15 12:56:42', '3733e028-2c4a-6b88-7edc-4231b1b97a5b', null, '2016-07-12', '2016-07-12', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('656', '2016-07-15 12:56:44 工作内容', '2016-07-15 12:56:44', 'fedf5e14-2182-aa74-8802-3bbb237e7538', null, '2016-07-13', '2016-07-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('657', '2016-07-15 12:57:10 工作内容', '2016-07-15 12:57:10', '7bf69b33-c99b-a47e-a268-b78cc47112a8', null, '2016-07-14', '2016-07-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('658', '2016-07-15 12:57:11 工作内容', '2016-07-15 12:57:11', 'fedf5e14-2182-aa74-8802-3bbb237e7538', null, '2016-07-15', '2016-07-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('659', '2016-07-15 12:57:40 工作内容', '2016-07-15 12:57:40', 'fedf5e14-2182-aa74-8802-3bbb237e7538', null, '2016-07-15', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('660', '2016-07-15 12:57:53 工作内容', '2016-07-15 12:57:53', '7bf69b33-c99b-a47e-a268-b78cc47112a8', null, '2016-07-15', '2016-07-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('661', '2016-07-15 13:00:38 工作内容', '2016-07-15 13:00:38', '1eb041f7-029a-b54f-378f-a6684c96e9d9', null, '2016-07-11', '2016-07-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('662', '2016-07-15 13:00:39 工作内容', '2016-07-15 13:00:39', '7bf69b33-c99b-a47e-a268-b78cc47112a8', null, '2016-07-22', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('663', '2016-07-15 13:00:54 工作内容', '2016-07-15 13:00:54', '1eb041f7-029a-b54f-378f-a6684c96e9d9', null, '2016-07-12', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('664', '2016-07-15 13:01:13 工作内容', '2016-07-15 13:01:13', '1eb041f7-029a-b54f-378f-a6684c96e9d9', null, '2016-07-13', '2016-07-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('665', '2016-07-15 13:02:16 工作内容', '2016-07-15 13:02:16', '1eb041f7-029a-b54f-378f-a6684c96e9d9', null, '2016-07-14', '2016-07-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('666', '2016-07-15 13:04:17 工作内容', '2016-07-15 13:04:17', '1eb041f7-029a-b54f-378f-a6684c96e9d9', null, '2016-07-15', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('667', '2016-07-15 13:04:47 工作内容', '2016-07-15 13:04:47', '1eb041f7-029a-b54f-378f-a6684c96e9d9', null, '2016-07-20', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('668', '2016-07-15 13:11:58 工作内容', '2016-07-15 13:11:58', '196a3669-44f2-a056-e988-edd36e29bbf2', null, '2016-07-12', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('669', '2016-07-15 13:12:55 工作内容', '2016-07-15 13:12:55', '1eb041f7-029a-b54f-378f-a6684c96e9d9', null, '2016-07-13', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('670', '2016-07-15 13:13:49 工作内容', '2016-07-15 13:13:49', '196a3669-44f2-a056-e988-edd36e29bbf2', null, '2016-07-14', '2016-07-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('671', '2016-07-15 13:14:24 工作内容', '2016-07-15 13:14:24', '196a3669-44f2-a056-e988-edd36e29bbf2', null, '2016-07-15', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('672', '2016-07-15 13:14:39 工作内容', '2016-07-15 13:14:39', '196a3669-44f2-a056-e988-edd36e29bbf2', null, '2016-07-22', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('673', '2016-07-15 13:15:09 工作内容', '2016-07-15 13:15:09', '196a3669-44f2-a056-e988-edd36e29bbf2', null, '2016-07-22', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('674', '2016-07-15 13:15:43 工作内容', '2016-07-15 13:15:43', '196a3669-44f2-a056-e988-edd36e29bbf2', null, '2016-07-15', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('675', '2016-07-15 14:18:03 工作内容', '2016-07-15 14:18:03', 'fedf5e14-2182-aa74-8802-3bbb237e7538', null, '2016-07-22', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('676', '2016-07-15 14:18:17 工作内容', '2016-07-15 14:18:17', '7bf69b33-c99b-a47e-a268-b78cc47112a8', null, '2016-07-22', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('677', '2016-07-15 14:23:55 工作内容', '2016-07-15 14:23:55', '9b59f1c0-2358-5135-1797-5c55b76c9bfc', null, '2016-07-12', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('678', '2016-07-15 14:24:44 工作内容', '2016-07-15 14:24:44', '9b59f1c0-2358-5135-1797-5c55b76c9bfc', null, '2016-07-08', '2016-07-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('679', '2016-07-15 14:25:10 工作内容', '2016-07-15 14:25:10', '9b59f1c0-2358-5135-1797-5c55b76c9bfc', null, '2016-07-15', '2016-07-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('680', '2016-07-15 14:26:15 工作内容', '2016-07-15 14:26:15', '9b59f1c0-2358-5135-1797-5c55b76c9bfc', null, '2016-07-22', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('681', '2016-07-15 14:28:42 工作内容', '2016-07-15 14:28:42', '29b5f634-5f15-8be5-aa0c-39927ee4d757', null, '2016-07-22', '2016-07-22', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('682', '2016-07-15 14:59:24 工作内容', '2016-07-15 14:59:24', '8e67b994-6433-7e82-3c32-86b033f52608', null, '2016-07-15', '2016-07-15', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('683', '2016-07-15 15:02:10 工作内容', '2016-07-15 15:02:10', '8e67b994-6433-7e82-3c32-86b033f52608', null, '2016-07-15', '2016-07-15', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('684', '2016-07-22 13:00:14 工作内容', '2016-07-22 13:00:14', 'e609ebaf-9692-20f2-4826-378bfeca490b', null, '2016-07-27', '2016-07-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('685', '2016-07-22 13:00:24 工作内容', '2016-07-22 13:00:24', 'd6624aac-afe5-4131-2e07-966b05b92afb', null, '2016-07-19', '2016-07-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('686', '2016-07-22 13:00:44 工作内容', '2016-07-22 13:00:44', 'e609ebaf-9692-20f2-4826-378bfeca490b', null, '2016-07-21', '2016-07-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('687', '2016-07-22 13:01:22 工作内容', '2016-07-22 13:01:22', 'e609ebaf-9692-20f2-4826-378bfeca490b', null, '2016-07-29', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('688', '2016-07-22 13:02:46 工作内容', '2016-07-22 13:02:46', 'd6624aac-afe5-4131-2e07-966b05b92afb', null, '2016-07-22', '2016-07-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('690', '2016-07-22 13:02:49 工作内容', '2016-07-22 13:02:49', '8d2bc31b-d008-2a1e-6264-ec7d7f0df0ad', null, '2016-07-20', '2016-07-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('691', '2016-07-22 13:03:03 工作内容', '2016-07-22 13:03:03', '36f4efdf-fb4a-6686-36b4-1f136b4fb6cf', null, '2016-07-21', '2016-07-21', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('692', '2016-07-22 13:03:16 工作内容', '2016-07-22 13:03:16', '8d2bc31b-d008-2a1e-6264-ec7d7f0df0ad', null, '2016-07-21', '2016-07-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('693', '2016-07-22 13:03:42 工作内容', '2016-07-22 13:03:42', '8d2bc31b-d008-2a1e-6264-ec7d7f0df0ad', null, '2016-07-29', '2016-07-29', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('694', '2016-07-22 13:04:44 工作内容', '2016-07-22 13:04:44', 'fb231fc0-24d8-3041-855a-1b6d2c509330', null, '2016-07-21', '2016-07-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('695', '2016-07-22 13:04:47 工作内容', '2016-07-22 13:04:47', '8d2bc31b-d008-2a1e-6264-ec7d7f0df0ad', null, '2016-07-22', '2016-07-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('696', '2016-07-22 13:07:39 工作内容', '2016-07-22 13:07:39', 'fb231fc0-24d8-3041-855a-1b6d2c509330', null, '2016-07-22', '2016-07-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('697', '2016-07-22 13:07:48 工作内容', '2016-07-22 13:07:48', 'fb231fc0-24d8-3041-855a-1b6d2c509330', null, '2016-07-29', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('698', '2016-07-22 13:09:49 工作内容', '2016-07-22 13:09:49', '9b9d187e-0b29-83c9-be8d-6e6fc45b8624', null, '2016-07-20', '2016-07-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('699', '2016-07-22 13:10:43 工作内容', '2016-07-22 13:10:43', '9b9d187e-0b29-83c9-be8d-6e6fc45b8624', null, '2016-07-21', '2016-07-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('700', '2016-07-22 13:17:19 工作内容', '2016-07-22 13:17:19', '6ca8844e-4b68-8cb1-1204-f936be2840f2', null, '2016-07-22', '2016-07-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('701', '2016-07-22 13:18:00 工作内容', '2016-07-22 13:18:00', 'fb231fc0-24d8-3041-855a-1b6d2c509330', null, '2016-07-25', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('702', '2016-07-22 13:21:14 工作内容', '2016-07-22 13:21:14', '6ca8844e-4b68-8cb1-1204-f936be2840f2', null, '2016-07-20', '2016-07-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('703', '2016-07-22 13:22:23 工作内容', '2016-07-22 13:22:23', '6ca8844e-4b68-8cb1-1204-f936be2840f2', null, '2016-07-21', '2016-07-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('704', '2016-07-22 13:26:04 工作内容', '2016-07-22 13:26:04', '6ca8844e-4b68-8cb1-1204-f936be2840f2', null, '2016-07-22', '2016-07-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('705', '2016-07-22 13:26:44 工作内容', '2016-07-22 13:26:44', '6ca8844e-4b68-8cb1-1204-f936be2840f2', null, '2016-07-25', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('706', '2016-07-22 13:27:27 工作内容', '2016-07-22 13:27:27', '6ca8844e-4b68-8cb1-1204-f936be2840f2', null, '2016-07-29', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('707', '2016-07-22 13:36:02 工作内容', '2016-07-22 13:36:02', 'bda9825e-a4d8-530e-d585-e0dcd08562a1', null, '2016-07-22', '2016-07-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('708', '2016-07-22 13:36:37 工作内容', '2016-07-22 13:36:37', 'bda9825e-a4d8-530e-d585-e0dcd08562a1', null, '2016-07-22', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('709', '2016-07-22 13:39:14 工作内容', '2016-07-22 13:39:14', 'bda9825e-a4d8-530e-d585-e0dcd08562a1', null, '2016-07-29', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('711', '2016-07-22 14:04:28 工作内容', '2016-07-22 14:04:28', '2a38ed1b-9c88-f339-7bba-586aa6c84e5c', null, '2016-07-21', '2016-07-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('712', '2016-07-22 14:07:27 工作内容', '2016-07-22 14:07:27', '2a38ed1b-9c88-f339-7bba-586aa6c84e5c', null, '2016-07-29', '2016-07-29', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('713', '2016-07-22 14:09:40 工作内容', '2016-07-22 14:09:40', '2a38ed1b-9c88-f339-7bba-586aa6c84e5c', null, '2016-07-29', '2016-07-29', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('714', '2016-07-22 14:43:14 工作内容', '2016-07-22 14:43:14', '4699ffcb-5c60-7e6d-088a-75214094ba9a', null, '2016-07-22', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('715', '2016-07-22 14:45:44 工作内容', '2016-07-22 14:45:44', 'bda9825e-a4d8-530e-d585-e0dcd08562a1', null, '2016-07-22', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('716', '2016-07-22 14:49:35 工作内容', '2016-07-22 14:49:35', '4699ffcb-5c60-7e6d-088a-75214094ba9a', null, '2016-07-22', '2016-07-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('717', '2016-07-22 14:50:11 工作内容', '2016-07-22 14:50:11', '4699ffcb-5c60-7e6d-088a-75214094ba9a', null, '2016-07-22', '2016-07-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('718', '2016-07-22 14:51:48 工作内容', '2016-07-22 14:51:48', '4699ffcb-5c60-7e6d-088a-75214094ba9a', null, '2016-07-22', '2016-07-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('719', '2016-07-22 14:55:16 工作内容', '2016-07-22 14:55:16', '4699ffcb-5c60-7e6d-088a-75214094ba9a', null, '2016-07-22', '2016-07-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('720', '2016-07-22 14:56:16 工作内容', '2016-07-22 14:56:16', '4699ffcb-5c60-7e6d-088a-75214094ba9a', null, '2016-07-22', '2016-07-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('721', '2016-07-22 14:59:35 工作内容', '2016-07-22 14:59:35', '4699ffcb-5c60-7e6d-088a-75214094ba9a', null, '2016-07-22', '2016-07-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('722', '2016-07-22 15:00:31 工作内容', '2016-07-22 15:00:31', '4699ffcb-5c60-7e6d-088a-75214094ba9a', null, '2016-07-19', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('723', '2016-07-22 15:02:35 工作内容', '2016-07-22 15:02:35', '4699ffcb-5c60-7e6d-088a-75214094ba9a', null, '2016-07-22', '2016-07-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('724', '2016-07-29 12:42:33 工作内容', '2016-07-29 12:42:33', '4624861f-5315-2d87-e32f-df0f499457e7', null, '2016-07-28', '2016-07-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('725', '2016-07-29 12:43:41 工作内容', '2016-07-29 12:43:41', 'e8bd5723-4c2e-710a-f99a-5ae710e9bc57', null, '2016-07-29', '2016-07-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('726', '2016-07-29 12:43:52 工作内容', '2016-07-29 12:43:52', '4624861f-5315-2d87-e32f-df0f499457e7', null, '2016-07-29', '2016-07-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('727', '2016-07-29 12:44:01 工作内容', '2016-07-29 12:44:01', 'e8bd5723-4c2e-710a-f99a-5ae710e9bc57', null, '2016-07-29', '2016-07-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('728', '2016-07-29 12:44:15 工作内容', '2016-07-29 12:44:15', '909200b9-3d9e-8c28-dc73-ba4b9ba9baad', null, '2016-07-27', '2016-07-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('729', '2016-07-29 12:44:47 工作内容', '2016-07-29 12:44:47', '4624861f-5315-2d87-e32f-df0f499457e7', null, '2016-07-27', '2016-07-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('730', '2016-07-29 12:44:51 工作内容', '2016-07-29 12:44:51', '3282b798-fb9e-5b98-c7bb-a7f6cc3d3007', null, '2016-07-26', '2016-07-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('731', '2016-07-29 12:44:55 工作内容', '2016-07-29 12:44:55', '909200b9-3d9e-8c28-dc73-ba4b9ba9baad', null, '2016-07-29', '2016-07-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('732', '2016-07-29 12:45:28 工作内容', '2016-07-29 12:45:28', '909200b9-3d9e-8c28-dc73-ba4b9ba9baad', null, '2016-07-31', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('733', '2016-07-29 12:45:59 工作内容', '2016-07-29 12:45:59', '4624861f-5315-2d87-e32f-df0f499457e7', null, '2016-07-26', '2016-07-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('734', '2016-07-29 12:46:57 工作内容', '2016-07-29 12:46:57', '8a529327-7ff5-e1e3-81a0-801074be3b56', null, '2016-07-26', '2016-07-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('735', '2016-07-29 12:47:33 工作内容', '2016-07-29 12:47:33', '8a529327-7ff5-e1e3-81a0-801074be3b56', null, '2016-07-28', '2016-07-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('736', '2016-07-29 12:47:51 工作内容', '2016-07-29 12:47:51', '3282b798-fb9e-5b98-c7bb-a7f6cc3d3007', null, '2016-07-29', '2016-07-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('737', '2016-07-29 12:48:13 工作内容', '2016-07-29 12:48:13', '4624861f-5315-2d87-e32f-df0f499457e7', null, '2016-08-05', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('738', '2016-07-29 12:48:24 工作内容', '2016-07-29 12:48:24', '8a529327-7ff5-e1e3-81a0-801074be3b56', null, '2016-07-30', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('739', '2016-07-29 12:48:31 工作内容', '2016-07-29 12:48:31', '43af7393-3c7d-21dd-0ce3-f94843908d02', null, '2016-07-25', '2016-07-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('740', '2016-07-29 12:49:21 工作内容', '2016-07-29 12:49:21', '3282b798-fb9e-5b98-c7bb-a7f6cc3d3007', null, '2016-07-27', '2016-07-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('742', '2016-07-29 12:50:06 工作内容', '2016-07-29 12:50:06', '3282b798-fb9e-5b98-c7bb-a7f6cc3d3007', null, '2016-07-28', '2016-07-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('743', '2016-07-29 12:50:59 工作内容', '2016-07-29 12:50:59', '43af7393-3c7d-21dd-0ce3-f94843908d02', null, '2016-07-26', '2016-07-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('744', '2016-07-29 12:51:57 工作内容', '2016-07-29 12:51:57', 'cba12c76-d6b9-a8f0-a097-b08e3d327673', null, '2016-07-26', '2016-07-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('745', '2016-07-29 12:52:52 工作内容', '2016-07-29 12:52:52', 'cba12c76-d6b9-a8f0-a097-b08e3d327673', null, '2016-07-29', '2016-07-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('746', '2016-07-29 12:53:22 工作内容', '2016-07-29 12:53:22', 'cba12c76-d6b9-a8f0-a097-b08e3d327673', null, '2016-07-27', '2016-07-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('747', '2016-07-29 12:54:03 工作内容', '2016-07-29 12:54:03', 'cba12c76-d6b9-a8f0-a097-b08e3d327673', null, '2016-07-28', '2016-07-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('748', '2016-07-29 12:55:46 工作内容', '2016-07-29 12:55:46', 'cba12c76-d6b9-a8f0-a097-b08e3d327673', null, '2016-07-29', '2016-07-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('749', '2016-07-29 12:55:51 工作内容', '2016-07-29 12:55:51', '43af7393-3c7d-21dd-0ce3-f94843908d02', null, '2016-07-29', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('750', '2016-07-29 12:57:29 工作内容', '2016-07-29 12:57:29', '43af7393-3c7d-21dd-0ce3-f94843908d02', null, '2016-07-29', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('751', '2016-07-29 12:58:12 工作内容', '2016-07-29 12:58:12', 'cba12c76-d6b9-a8f0-a097-b08e3d327673', null, '2016-08-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('752', '2016-07-29 13:00:10 工作内容', '2016-07-29 13:00:10', 'c8ee6191-7a2e-4443-a84e-41a4ff4e2bf5', null, '2016-07-25', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('753', '2016-07-29 13:00:41 工作内容', '2016-07-29 13:00:41', 'cba12c76-d6b9-a8f0-a097-b08e3d327673', null, '2016-07-29', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('754', '2016-07-29 13:00:50 工作内容', '2016-07-29 13:00:50', '43af7393-3c7d-21dd-0ce3-f94843908d02', null, '2016-07-28', '2016-07-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('756', '2016-07-29 13:02:35 工作内容', '2016-07-29 13:02:35', 'c8ee6191-7a2e-4443-a84e-41a4ff4e2bf5', null, '2016-07-26', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('757', '2016-07-29 13:02:54 工作内容', '2016-07-29 13:02:54', '43af7393-3c7d-21dd-0ce3-f94843908d02', null, '2016-08-01', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('758', '2016-07-29 13:03:06 工作内容', '2016-07-29 13:03:06', 'c8ee6191-7a2e-4443-a84e-41a4ff4e2bf5', null, '2016-07-20', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('759', '2016-07-29 13:04:03 工作内容', '2016-07-29 13:04:03', 'c8ee6191-7a2e-4443-a84e-41a4ff4e2bf5', null, '2016-07-28', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('760', '2016-07-29 13:04:20 工作内容', '2016-07-29 13:04:20', 'c8ee6191-7a2e-4443-a84e-41a4ff4e2bf5', null, '2016-07-29', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('761', '2016-07-29 13:04:31 工作内容', '2016-07-29 13:04:31', '43af7393-3c7d-21dd-0ce3-f94843908d02', null, '2016-08-04', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('762', '2016-07-29 13:05:08 工作内容', '2016-07-29 13:05:08', 'c8ee6191-7a2e-4443-a84e-41a4ff4e2bf5', null, '2016-07-29', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('763', '2016-07-29 13:05:52 工作内容', '2016-07-29 13:05:52', '43af7393-3c7d-21dd-0ce3-f94843908d02', null, '2016-08-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('764', '2016-07-29 13:06:13 工作内容', '2016-07-29 13:06:13', 'c8ee6191-7a2e-4443-a84e-41a4ff4e2bf5', null, '2016-08-05', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('765', '2016-07-29 13:33:59 工作内容', '2016-07-29 13:33:59', 'cba12c76-d6b9-a8f0-a097-b08e3d327673', null, '2016-07-28', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('766', '2016-07-29 13:43:15 工作内容', '2016-07-29 13:43:15', '4751f70a-3ab2-f31f-7223-50e428e4d9e4', null, '2016-07-25', '2016-07-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('767', '2016-07-29 13:45:25 工作内容', '2016-07-29 13:45:25', '4751f70a-3ab2-f31f-7223-50e428e4d9e4', null, '2016-07-27', '2016-07-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('768', '2016-07-29 13:47:46 工作内容', '2016-07-29 13:47:46', '4751f70a-3ab2-f31f-7223-50e428e4d9e4', null, '2016-07-28', '2016-07-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('769', '2016-07-29 13:48:18 工作内容', '2016-07-29 13:48:18', '4751f70a-3ab2-f31f-7223-50e428e4d9e4', null, '2016-07-29', '2016-07-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('770', '2016-07-29 13:48:55 工作内容', '2016-07-29 13:48:55', '4751f70a-3ab2-f31f-7223-50e428e4d9e4', null, '2016-08-01', '2016-08-05', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('771', '2016-07-29 13:49:26 工作内容', '2016-07-29 13:49:26', '4751f70a-3ab2-f31f-7223-50e428e4d9e4', null, '2016-08-01', '2016-08-05', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('772', '2016-07-29 14:51:32 工作内容', '2016-07-29 14:51:32', 'e8bd5723-4c2e-710a-f99a-5ae710e9bc57', null, '2016-08-05', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('773', '2016-07-29 14:52:37 工作内容', '2016-07-29 14:52:37', 'b4766a98-1a6d-e3ac-86b4-5e829a8f2878', null, '2016-07-25', '2016-07-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('774', '2016-07-29 14:55:32 工作内容', '2016-07-29 14:55:32', 'b4766a98-1a6d-e3ac-86b4-5e829a8f2878', null, '2016-07-29', '2016-07-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('775', '2016-07-29 14:58:15 工作内容', '2016-07-29 14:58:15', 'b4766a98-1a6d-e3ac-86b4-5e829a8f2878', null, '2016-08-03', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('776', '2016-07-29 15:11:02 工作内容', '2016-07-29 15:11:02', '8a529327-7ff5-e1e3-81a0-801074be3b56', null, '2016-08-01', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('777', '2016-08-05 13:32:02 工作内容', '2016-08-05 13:32:02', 'b66d934e-f1b4-0175-c65b-db21cfede21f', null, '2016-08-02', '2016-08-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('778', '2016-08-05 13:33:02 工作内容', '2016-08-05 13:33:02', 'b66d934e-f1b4-0175-c65b-db21cfede21f', null, '2016-08-04', '2016-08-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('779', '2016-08-05 13:33:42 工作内容', '2016-08-05 13:33:42', 'b66d934e-f1b4-0175-c65b-db21cfede21f', null, '2016-08-03', '2016-08-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('780', '2016-08-05 13:34:34 工作内容', '2016-08-05 13:34:34', '88724137-e67a-cdfb-a459-327c2fa29c89', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('781', '2016-08-05 13:35:02 工作内容', '2016-08-05 13:35:02', '7b598070-d177-87b3-b548-e9a2c1084755', null, '2016-08-01', '2016-08-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('782', '2016-08-05 13:35:24 工作内容', '2016-08-05 13:35:24', 'ac337ccc-aa1f-40f7-a7d5-a46e6ef1291e', null, '2016-08-01', '2016-08-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('783', '2016-08-05 13:35:45 工作内容', '2016-08-05 13:35:45', '88724137-e67a-cdfb-a459-327c2fa29c89', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('784', '2016-08-05 13:35:50 工作内容', '2016-08-05 13:35:50', 'b66d934e-f1b4-0175-c65b-db21cfede21f', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('785', '2016-08-05 13:36:07 工作内容', '2016-08-05 13:36:07', 'bb3b3dc4-4d5c-d23a-a087-ae76812c8a77', null, '2016-08-01', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('786', '2016-08-05 13:36:09 工作内容', '2016-08-05 13:36:09', '7b598070-d177-87b3-b548-e9a2c1084755', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('787', '2016-08-05 13:36:56 工作内容', '2016-08-05 13:36:56', 'b66d934e-f1b4-0175-c65b-db21cfede21f', null, '2016-08-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('788', '2016-08-05 13:36:57 工作内容', '2016-08-05 13:36:57', 'ac337ccc-aa1f-40f7-a7d5-a46e6ef1291e', null, '2016-08-03', '2016-08-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('789', '2016-08-05 13:36:59 工作内容', '2016-08-05 13:36:59', 'bb3b3dc4-4d5c-d23a-a087-ae76812c8a77', null, '2016-08-02', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('790', '2016-08-05 13:37:05 工作内容', '2016-08-05 13:37:05', '3eb0f500-850c-8757-270f-7e5bc8879178', null, '2016-08-02', '2016-08-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('791', '2016-08-05 13:37:11 工作内容', '2016-08-05 13:37:11', '7b598070-d177-87b3-b548-e9a2c1084755', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('792', '2016-08-05 13:37:15 工作内容', '2016-08-05 13:37:15', 'b66d934e-f1b4-0175-c65b-db21cfede21f', null, '2016-08-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('793', '2016-08-05 13:37:20 工作内容', '2016-08-05 13:37:20', 'ac337ccc-aa1f-40f7-a7d5-a46e6ef1291e', null, '2016-08-04', '2016-08-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('794', '2016-08-05 13:37:37 工作内容', '2016-08-05 13:37:37', '7112a741-cfea-38a2-ab21-90594c76fef3', null, '2016-08-03', '2016-08-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('795', '2016-08-05 13:37:37 工作内容', '2016-08-05 13:37:37', 'bb3b3dc4-4d5c-d23a-a087-ae76812c8a77', null, '2016-08-03', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('796', '2016-08-05 13:37:53 工作内容', '2016-08-05 13:37:53', '3eb0f500-850c-8757-270f-7e5bc8879178', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('797', '2016-08-05 13:37:55 工作内容', '2016-08-05 13:37:55', 'ac337ccc-aa1f-40f7-a7d5-a46e6ef1291e', null, '2016-08-09', '2016-08-09', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('798', '2016-08-05 13:38:02 工作内容', '2016-08-05 13:38:02', '630b092a-9eee-bbc3-a6b7-174dbf66f04d', null, '2016-08-01', '2016-08-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('799', '2016-08-05 13:38:14 工作内容', '2016-08-05 13:38:14', '7112a741-cfea-38a2-ab21-90594c76fef3', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('800', '2016-08-05 13:38:20 工作内容', '2016-08-05 13:38:20', '3eb0f500-850c-8757-270f-7e5bc8879178', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('801', '2016-08-05 13:38:40 工作内容', '2016-08-05 13:38:40', '3eb0f500-850c-8757-270f-7e5bc8879178', null, '2016-08-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('802', '2016-08-05 13:38:46 工作内容', '2016-08-05 13:38:46', 'bb3b3dc4-4d5c-d23a-a087-ae76812c8a77', null, '2016-08-04', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('803', '2016-08-05 13:39:02 工作内容', '2016-08-05 13:39:02', '7112a741-cfea-38a2-ab21-90594c76fef3', null, '2016-08-03', '2016-08-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('804', '2016-08-05 13:39:14 工作内容', '2016-08-05 13:39:14', 'bb3b3dc4-4d5c-d23a-a087-ae76812c8a77', null, '2016-08-05', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('805', '2016-08-05 13:39:15 工作内容', '2016-08-05 13:39:15', '3eb0f500-850c-8757-270f-7e5bc8879178', null, '2016-08-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('806', '2016-08-05 13:39:37 工作内容', '2016-08-05 13:39:37', '7112a741-cfea-38a2-ab21-90594c76fef3', null, '2016-08-04', '2016-08-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('807', '2016-08-05 13:39:42 工作内容', '2016-08-05 13:39:42', 'bb3b3dc4-4d5c-d23a-a087-ae76812c8a77', null, '2016-08-05', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('808', '2016-08-05 13:39:57 工作内容', '2016-08-05 13:39:57', '7112a741-cfea-38a2-ab21-90594c76fef3', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('809', '2016-08-05 13:40:19 工作内容', '2016-08-05 13:40:19', '7b598070-d177-87b3-b548-e9a2c1084755', null, '2016-08-12', '2016-08-12', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('810', '2016-08-05 13:40:36 工作内容', '2016-08-05 13:40:36', 'bb3b3dc4-4d5c-d23a-a087-ae76812c8a77', null, '2016-08-12', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('811', '2016-08-05 13:41:14 工作内容', '2016-08-05 13:41:14', '7b598070-d177-87b3-b548-e9a2c1084755', null, '2016-08-12', '2016-08-12', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('812', '2016-08-05 13:42:02 工作内容', '2016-08-05 13:42:02', '7112a741-cfea-38a2-ab21-90594c76fef3', null, '2016-08-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('813', '2016-08-05 13:42:21 工作内容', '2016-08-05 13:42:21', '630b092a-9eee-bbc3-a6b7-174dbf66f04d', null, '2016-08-02', '2016-08-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('814', '2016-08-05 13:42:43 工作内容', '2016-08-05 13:42:43', '7112a741-cfea-38a2-ab21-90594c76fef3', null, '2016-08-08', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('815', '2016-08-05 13:43:13 工作内容', '2016-08-05 13:43:13', '7112a741-cfea-38a2-ab21-90594c76fef3', null, '2016-08-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('816', '2016-08-05 13:44:32 工作内容', '2016-08-05 13:44:32', '7112a741-cfea-38a2-ab21-90594c76fef3', null, '2016-08-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('817', '2016-08-05 13:47:03 工作内容', '2016-08-05 13:47:03', '630b092a-9eee-bbc3-a6b7-174dbf66f04d', null, '2016-08-03', '2016-08-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('818', '2016-08-05 13:49:03 工作内容', '2016-08-05 13:49:03', '74d49c5d-5de7-8569-dea7-e291a8073d4c', null, '2016-08-03', '2016-08-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('819', '2016-08-05 13:49:36 工作内容', '2016-08-05 13:49:36', '74d49c5d-5de7-8569-dea7-e291a8073d4c', null, '2016-08-04', '2016-08-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('820', '2016-08-05 13:50:23 工作内容', '2016-08-05 13:50:23', '74d49c5d-5de7-8569-dea7-e291a8073d4c', null, '2016-08-05', '2016-08-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('821', '2016-08-05 13:50:42 工作内容', '2016-08-05 13:50:42', '630b092a-9eee-bbc3-a6b7-174dbf66f04d', null, '2016-08-04', '2016-08-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('822', '2016-08-05 13:51:05 工作内容', '2016-08-05 13:51:05', '74d49c5d-5de7-8569-dea7-e291a8073d4c', null, '2016-08-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('823', '2016-08-05 13:51:10 工作内容', '2016-08-05 13:51:10', '7b598070-d177-87b3-b548-e9a2c1084755', null, '2016-08-08', '2016-08-08', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('824', '2016-08-05 13:52:29 工作内容', '2016-08-05 13:52:29', '630b092a-9eee-bbc3-a6b7-174dbf66f04d', null, '2016-08-05', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('825', '2016-08-05 13:53:33 工作内容', '2016-08-05 13:53:33', '74d49c5d-5de7-8569-dea7-e291a8073d4c', null, '2016-08-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('826', '2016-08-05 13:55:52 工作内容', '2016-08-05 13:55:52', '630b092a-9eee-bbc3-a6b7-174dbf66f04d', null, '2016-08-05', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('827', '2016-08-05 13:58:38 工作内容', '2016-08-05 13:58:38', '630b092a-9eee-bbc3-a6b7-174dbf66f04d', null, '2016-08-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('828', '2016-08-05 14:03:11 工作内容', '2016-08-05 14:03:11', '74d49c5d-5de7-8569-dea7-e291a8073d4c', null, '2016-08-12', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('829', '2016-08-12 13:37:30 工作内容', '2016-08-12 13:37:30', 'c0fdc8a3-a180-dc68-d4ff-30db2eabf005', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('830', '2016-08-12 13:39:32 工作内容', '2016-08-12 13:39:32', 'c0fdc8a3-a180-dc68-d4ff-30db2eabf005', null, '2016-08-09', '2016-08-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('831', '2016-08-12 13:40:14 工作内容', '2016-08-12 13:40:14', 'c0fdc8a3-a180-dc68-d4ff-30db2eabf005', null, '2016-08-10', '2016-08-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('832', '2016-08-12 13:41:48 工作内容', '2016-08-12 13:41:48', 'd2031b14-e7cd-f2c5-3921-42b3582a1fe4', null, '2016-08-08', '2016-08-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('833', '2016-08-12 13:42:07 工作内容', '2016-08-12 13:42:07', 'c0fdc8a3-a180-dc68-d4ff-30db2eabf005', null, '2016-08-11', '2016-08-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('834', '2016-08-12 13:42:13 工作内容', '2016-08-12 13:42:13', 'd2031b14-e7cd-f2c5-3921-42b3582a1fe4', null, '2016-08-10', '2016-08-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('835', '2016-08-12 13:42:34 工作内容', '2016-08-12 13:42:34', 'c0fdc8a3-a180-dc68-d4ff-30db2eabf005', null, '2016-08-12', '2016-08-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('836', '2016-08-12 13:42:52 工作内容', '2016-08-12 13:42:52', 'd2031b14-e7cd-f2c5-3921-42b3582a1fe4', null, '2016-08-11', '2016-08-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('837', '2016-08-12 13:44:09 工作内容', '2016-08-12 13:44:09', 'd2031b14-e7cd-f2c5-3921-42b3582a1fe4', null, '2016-08-19', '2016-08-19', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('838', '2016-08-12 13:44:23 工作内容', '2016-08-12 13:44:23', '991d0668-be59-d8ef-f494-a9332c282870', null, '2016-08-09', '2016-08-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('839', '2016-08-12 13:45:04 工作内容', '2016-08-12 13:45:04', '991d0668-be59-d8ef-f494-a9332c282870', null, '2016-08-10', '2016-08-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('840', '2016-08-12 13:45:31 工作内容', '2016-08-12 13:45:31', '991d0668-be59-d8ef-f494-a9332c282870', null, '2016-08-12', '2016-08-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('841', '2016-08-12 13:46:40 工作内容', '2016-08-12 13:46:40', '991d0668-be59-d8ef-f494-a9332c282870', null, '2016-08-12', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('842', '2016-08-12 13:49:06 工作内容', '2016-08-12 13:49:06', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('843', '2016-08-12 13:50:46 工作内容', '2016-08-12 13:50:46', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('844', '2016-08-12 13:53:29 工作内容', '2016-08-12 13:53:29', 'f4ac2cfe-0ed4-5aab-5b8e-e3f5e4df361f', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('845', '2016-08-12 13:53:55 工作内容', '2016-08-12 13:53:55', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('846', '2016-08-12 13:54:37 工作内容', '2016-08-12 13:54:37', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-09', '2016-08-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('847', '2016-08-12 13:55:37 工作内容', '2016-08-12 13:55:37', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-10', '2016-08-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('848', '2016-08-12 13:55:50 工作内容', '2016-08-12 13:55:50', 'f4ac2cfe-0ed4-5aab-5b8e-e3f5e4df361f', null, '2016-08-12', '2016-08-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('849', '2016-08-12 13:56:20 工作内容', '2016-08-12 13:56:20', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-10', '2016-08-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('850', '2016-08-12 13:57:29 工作内容', '2016-08-12 13:57:29', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-12', '2016-08-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('851', '2016-08-12 13:58:01 工作内容', '2016-08-12 13:58:01', 'f4ac2cfe-0ed4-5aab-5b8e-e3f5e4df361f', null, '2016-08-10', '2016-08-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('852', '2016-08-12 13:58:22 工作内容', '2016-08-12 13:58:22', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-19', '2016-08-19', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('853', '2016-08-12 13:58:54 工作内容', '2016-08-12 13:58:54', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-19', '2016-08-19', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('854', '2016-08-12 13:59:07 工作内容', '2016-08-12 13:59:07', 'f4ac2cfe-0ed4-5aab-5b8e-e3f5e4df361f', null, '2016-08-11', '2016-08-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('855', '2016-08-12 13:59:46 工作内容', '2016-08-12 13:59:46', '991d0668-be59-d8ef-f494-a9332c282870', null, '2016-08-19', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('856', '2016-08-12 14:00:04 工作内容', '2016-08-12 14:00:04', '1dce60a1-1e9b-d657-0fe4-835e266ad464', null, '2016-08-19', '2016-08-19', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('857', '2016-08-12 14:01:37 工作内容', '2016-08-12 14:01:37', 'f4ac2cfe-0ed4-5aab-5b8e-e3f5e4df361f', null, '2016-08-12', '2016-08-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('858', '2016-08-12 14:01:42 工作内容', '2016-08-12 14:01:42', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('859', '2016-08-12 14:02:17 工作内容', '2016-08-12 14:02:17', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('860', '2016-08-12 14:02:51 工作内容', '2016-08-12 14:02:51', 'f4ac2cfe-0ed4-5aab-5b8e-e3f5e4df361f', null, '2016-08-21', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('861', '2016-08-12 14:03:01 工作内容', '2016-08-12 14:03:01', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('862', '2016-08-12 14:03:19 工作内容', '2016-08-12 14:03:19', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-09', '2016-08-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('863', '2016-08-12 14:04:01 工作内容', '2016-08-12 14:04:01', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-09', '2016-08-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('864', '2016-08-12 14:04:56 工作内容', '2016-08-12 14:04:56', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-10', '2016-08-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('865', '2016-08-12 14:05:39 工作内容', '2016-08-12 14:05:39', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-12', '2016-08-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('866', '2016-08-12 14:06:06 工作内容', '2016-08-12 14:06:06', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-19', '2016-08-19', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('867', '2016-08-12 14:06:48 工作内容', '2016-08-12 14:06:48', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-19', '2016-08-19', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('868', '2016-08-12 14:09:20 工作内容', '2016-08-12 14:09:20', '2e8c8405-180e-d117-e6c8-4469dff47ae1', null, '2016-08-19', '2016-08-19', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('869', '2016-08-12 14:11:22 工作内容', '2016-08-12 14:11:22', '516a51de-ea46-c73d-291f-b6e6f26de80d', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('870', '2016-08-12 14:14:05 工作内容', '2016-08-12 14:14:05', '516a51de-ea46-c73d-291f-b6e6f26de80d', null, '2016-08-09', '2016-08-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('871', '2016-08-12 14:16:37 工作内容', '2016-08-12 14:16:37', '516a51de-ea46-c73d-291f-b6e6f26de80d', null, '2016-08-10', '2016-08-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('872', '2016-08-12 14:20:14 工作内容', '2016-08-12 14:20:14', '72dd3fd2-9362-df78-16fc-2d57b7edd209', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('873', '2016-08-12 14:20:30 工作内容', '2016-08-12 14:20:30', '72dd3fd2-9362-df78-16fc-2d57b7edd209', null, '2016-08-08', '2016-08-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('874', '2016-08-12 14:21:58 工作内容', '2016-08-12 14:21:58', '72dd3fd2-9362-df78-16fc-2d57b7edd209', null, '2016-08-05', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('875', '2016-08-12 14:22:28 工作内容', '2016-08-12 14:22:28', '72dd3fd2-9362-df78-16fc-2d57b7edd209', null, '2016-08-11', '2016-08-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('876', '2016-08-12 14:23:04 工作内容', '2016-08-12 14:23:04', '516a51de-ea46-c73d-291f-b6e6f26de80d', null, '2016-08-11', '2016-08-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('877', '2016-08-12 14:23:51 工作内容', '2016-08-12 14:23:51', '72dd3fd2-9362-df78-16fc-2d57b7edd209', null, '2016-08-12', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('878', '2016-08-12 14:24:23 工作内容', '2016-08-12 14:24:23', '72dd3fd2-9362-df78-16fc-2d57b7edd209', null, '2016-08-12', '2016-08-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('879', '2016-08-12 14:24:41 工作内容', '2016-08-12 14:24:41', '516a51de-ea46-c73d-291f-b6e6f26de80d', null, '2016-08-12', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('880', '2016-08-12 14:27:48 工作内容', '2016-08-12 14:27:48', '516a51de-ea46-c73d-291f-b6e6f26de80d', null, '2016-08-16', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('881', '2016-08-12 14:33:52 工作内容', '2016-08-12 14:33:52', '72dd3fd2-9362-df78-16fc-2d57b7edd209', null, '2016-08-07', '2016-08-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('882', '2016-08-12 14:35:13 工作内容', '2016-08-12 14:35:13', '72dd3fd2-9362-df78-16fc-2d57b7edd209', null, '2016-08-19', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('883', '2016-08-12 14:35:56 工作内容', '2016-08-12 14:35:56', '72dd3fd2-9362-df78-16fc-2d57b7edd209', null, '2016-08-19', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('884', '2016-08-12 15:05:13 工作内容', '2016-08-12 15:05:13', 'c0fdc8a3-a180-dc68-d4ff-30db2eabf005', null, '2016-08-19', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('887', '2016-08-12 15:39:05 工作内容', '2016-08-12 15:39:05', 'a6e72a10-2ef2-4d8e-fc2a-d429effb98aa', null, '2016-08-08', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('888', '2016-08-12 15:40:45 工作内容', '2016-08-12 15:40:45', 'a6e72a10-2ef2-4d8e-fc2a-d429effb98aa', null, '2016-08-09', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('889', '2016-08-12 15:41:07 工作内容', '2016-08-12 15:41:07', 'a6e72a10-2ef2-4d8e-fc2a-d429effb98aa', null, '2016-08-10', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('890', '2016-08-12 15:41:29 工作内容', '2016-08-12 15:41:29', 'a6e72a10-2ef2-4d8e-fc2a-d429effb98aa', null, '2016-08-11', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('891', '2016-08-12 15:44:43 工作内容', '2016-08-12 15:44:43', 'a6e72a10-2ef2-4d8e-fc2a-d429effb98aa', null, '2016-08-12', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('892', '2016-08-12 15:46:02 工作内容', '2016-08-12 15:46:02', 'a6e72a10-2ef2-4d8e-fc2a-d429effb98aa', null, '2016-08-12', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('893', '2016-08-12 15:46:49 工作内容', '2016-08-12 15:46:49', 'a6e72a10-2ef2-4d8e-fc2a-d429effb98aa', null, '2016-08-19', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('894', '2016-08-12 15:53:13 工作内容', '2016-08-12 15:53:13', '61dd6ca0-f46f-bec5-72c0-f629baf6b602', null, '2016-08-12', '2016-08-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('895', '2016-08-12 15:55:01 工作内容', '2016-08-12 15:55:01', '61dd6ca0-f46f-bec5-72c0-f629baf6b602', null, '2016-08-19', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('896', '2016-08-12 17:04:01 工作内容', '2016-08-12 17:04:01', 'f4ac2cfe-0ed4-5aab-5b8e-e3f5e4df361f', null, '2016-08-21', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('897', '2016-08-19 13:26:11 工作内容', '2016-08-19 13:26:11', '5222681c-d2b9-d245-cb2a-2938e6aa3042', null, '2016-08-16', '2016-08-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('898', '2016-08-19 13:26:49 工作内容', '2016-08-19 13:26:49', '5222681c-d2b9-d245-cb2a-2938e6aa3042', null, '2016-08-17', '2016-08-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('899', '2016-08-19 13:27:17 工作内容', '2016-08-19 13:27:17', '8656b230-b979-ea82-b771-138e3f185a7a', null, '2016-08-15', '2016-08-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('900', '2016-08-19 13:27:37 工作内容', '2016-08-19 13:27:37', '8656b230-b979-ea82-b771-138e3f185a7a', null, '2016-08-16', '2016-08-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('901', '2016-08-19 13:27:40 工作内容', '2016-08-19 13:27:40', '5222681c-d2b9-d245-cb2a-2938e6aa3042', null, '2016-08-18', '2016-08-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('902', '2016-08-19 13:28:27 工作内容', '2016-08-19 13:28:27', '8656b230-b979-ea82-b771-138e3f185a7a', null, '2016-08-19', '2016-08-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('903', '2016-08-19 13:28:52 工作内容', '2016-08-19 13:28:52', 'c27d45bd-1db3-c34f-3cc5-86df06e9f1b7', null, '2016-08-16', '2016-08-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('904', '2016-08-19 13:29:36 工作内容', '2016-08-19 13:29:36', 'c27d45bd-1db3-c34f-3cc5-86df06e9f1b7', null, '2016-08-18', '2016-08-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('905', '2016-08-19 13:30:03 工作内容', '2016-08-19 13:30:03', '8656b230-b979-ea82-b771-138e3f185a7a', null, '2016-08-26', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('908', '2016-08-19 13:31:55 工作内容', '2016-08-19 13:31:55', 'c27d45bd-1db3-c34f-3cc5-86df06e9f1b7', null, '2016-08-19', '2016-08-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('909', '2016-08-19 13:34:08 工作内容', '2016-08-19 13:34:08', 'be301ab6-38d0-1aa2-ddac-fde6a3df6761', null, '2016-08-16', '2016-08-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('910', '2016-08-19 13:35:09 工作内容', '2016-08-19 13:35:09', 'c27d45bd-1db3-c34f-3cc5-86df06e9f1b7', null, '2016-08-26', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('911', '2016-08-19 13:35:37 工作内容', '2016-08-19 13:35:37', 'c27d45bd-1db3-c34f-3cc5-86df06e9f1b7', null, '2016-08-26', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('912', '2016-08-19 13:35:51 工作内容', '2016-08-19 13:35:51', '5222681c-d2b9-d245-cb2a-2938e6aa3042', null, '2016-08-19', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('913', '2016-08-19 13:36:09 工作内容', '2016-08-19 13:36:09', '683967f4-5bcd-47bd-9486-9479c5595e08', null, '2016-08-16', '2016-08-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('914', '2016-08-19 13:36:30 工作内容', '2016-08-19 13:36:30', 'be301ab6-38d0-1aa2-ddac-fde6a3df6761', null, '2016-08-17', '2016-08-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('915', '2016-08-19 13:36:37 工作内容', '2016-08-19 13:36:37', '5222681c-d2b9-d245-cb2a-2938e6aa3042', null, '2016-08-23', '2016-08-23', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('916', '2016-08-19 13:36:44 工作内容', '2016-08-19 13:36:44', '683967f4-5bcd-47bd-9486-9479c5595e08', null, '2016-08-17', '2016-08-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('917', '2016-08-19 13:37:24 工作内容', '2016-08-19 13:37:24', '683967f4-5bcd-47bd-9486-9479c5595e08', null, '2016-08-18', '2016-08-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('918', '2016-08-19 13:38:17 工作内容', '2016-08-19 13:38:17', '683967f4-5bcd-47bd-9486-9479c5595e08', null, '2016-08-19', '2016-08-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('919', '2016-08-19 13:39:14 工作内容', '2016-08-19 13:39:14', '683967f4-5bcd-47bd-9486-9479c5595e08', null, '2016-08-15', '2016-08-15', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('920', '2016-08-19 13:39:38 工作内容', '2016-08-19 13:39:38', '683967f4-5bcd-47bd-9486-9479c5595e08', null, '2016-08-26', '2016-08-26', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('921', '2016-08-19 13:40:43 工作内容', '2016-08-19 13:40:43', 'be301ab6-38d0-1aa2-ddac-fde6a3df6761', null, '2016-08-18', '2016-08-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('922', '2016-08-19 13:42:03 工作内容', '2016-08-19 13:42:03', '9b6af913-1fb4-3310-d402-5bb6da82551b', null, '2016-08-19', '2016-08-16', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('923', '2016-08-19 13:44:03 工作内容', '2016-08-19 13:44:03', '9b6af913-1fb4-3310-d402-5bb6da82551b', null, '2016-08-19', '2016-08-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('924', '2016-08-19 13:44:06 工作内容', '2016-08-19 13:44:06', '683967f4-5bcd-47bd-9486-9479c5595e08', null, '2016-08-19', '2016-08-19', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('925', '2016-08-19 13:45:07 工作内容', '2016-08-19 13:45:07', '9b6af913-1fb4-3310-d402-5bb6da82551b', null, '2016-08-19', '2016-08-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('926', '2016-08-19 13:46:09 工作内容', '2016-08-19 13:46:09', '9b6af913-1fb4-3310-d402-5bb6da82551b', null, '2016-08-18', '2016-08-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('927', '2016-08-19 13:52:43 工作内容', '2016-08-19 13:52:43', '9b6af913-1fb4-3310-d402-5bb6da82551b', null, '2016-08-28', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('928', '2016-08-19 14:07:50 工作内容', '2016-08-19 14:07:50', 'be301ab6-38d0-1aa2-ddac-fde6a3df6761', null, '2016-08-19', '2016-08-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('929', '2016-08-19 14:09:02 工作内容', '2016-08-19 14:09:02', 'be301ab6-38d0-1aa2-ddac-fde6a3df6761', null, '2016-08-24', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('930', '2016-08-19 14:23:29 工作内容', '2016-08-19 14:23:29', '02b76556-0b95-124f-5e89-916210e3e4a1', null, '2016-08-15', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('931', '2016-08-19 14:23:55 工作内容', '2016-08-19 14:23:55', '02b76556-0b95-124f-5e89-916210e3e4a1', null, '2016-08-16', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('932', '2016-08-19 14:24:20 工作内容', '2016-08-19 14:24:20', '02b76556-0b95-124f-5e89-916210e3e4a1', null, '2016-08-17', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('933', '2016-08-19 14:24:35 工作内容', '2016-08-19 14:24:35', '02b76556-0b95-124f-5e89-916210e3e4a1', null, '2016-08-18', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('934', '2016-08-19 14:25:30 工作内容', '2016-08-19 14:25:30', '02b76556-0b95-124f-5e89-916210e3e4a1', null, '2016-08-19', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('935', '2016-08-19 14:26:28 工作内容', '2016-08-19 14:26:28', '02b76556-0b95-124f-5e89-916210e3e4a1', null, '2016-08-19', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('936', '2016-08-19 14:27:01 工作内容', '2016-08-19 14:27:01', '02b76556-0b95-124f-5e89-916210e3e4a1', null, '2016-08-19', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('937', '2016-08-19 14:58:28 工作内容', '2016-08-19 14:58:28', '94aca2c8-ba08-6bba-4b2b-f18cffb1fd74', '8', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('938', '2016-08-19 14:59:57 工作内容', '2016-08-19 14:59:57', '94aca2c8-ba08-6bba-4b2b-f18cffb1fd74', '8', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('939', '2016-08-19 15:00:59 工作内容', '2016-08-19 15:00:59', '94aca2c8-ba08-6bba-4b2b-f18cffb1fd74', '8', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('940', '2016-08-19 15:02:28 工作内容', '2016-08-19 15:02:28', '94aca2c8-ba08-6bba-4b2b-f18cffb1fd74', '8', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('941', '2016-08-19 15:03:26 工作内容', '2016-08-19 15:03:26', '94aca2c8-ba08-6bba-4b2b-f18cffb1fd74', '8', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('944', '2016-08-19 15:30:16 工作内容', '2016-08-19 15:30:16', '5222681c-d2b9-d245-cb2a-2938e6aa3042', null, '2016-08-25', '2016-08-25', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('946', '2016-08-19 15:32:11 工作内容', '2016-08-19 15:32:11', '3ca32c59-a7b0-9edc-e24d-388e28c59e61', null, '2016-08-19', '2016-08-19', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('947', '2016-08-19 15:32:33 工作内容', '2016-08-19 15:32:33', '3ca32c59-a7b0-9edc-e24d-388e28c59e61', null, '2016-08-26', '2016-08-26', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('948', '2016-08-26 12:46:11 工作内容', '2016-08-26 12:46:11', 'c646b7f1-d5f5-a041-a308-632e8d554854', null, '2016-08-24', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('949', '2016-08-26 12:46:55 工作内容', '2016-08-26 12:46:55', 'c646b7f1-d5f5-a041-a308-632e8d554854', null, '2016-08-26', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('950', '2016-08-26 12:47:33 工作内容', '2016-08-26 12:47:33', 'c646b7f1-d5f5-a041-a308-632e8d554854', null, '2016-08-25', '2016-08-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('951', '2016-08-26 12:48:30 工作内容', '2016-08-26 12:48:30', 'c646b7f1-d5f5-a041-a308-632e8d554854', null, '2016-08-31', '2016-08-31', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('952', '2016-08-26 12:48:39 工作内容', '2016-08-26 12:48:39', 'd4f127e4-ff5b-8ee1-0db7-1ebe823c8974', null, '2016-08-21', '2016-08-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('953', '2016-08-26 12:51:10 工作内容', '2016-08-26 12:51:10', 'c646b7f1-d5f5-a041-a308-632e8d554854', null, '2016-08-23', '2016-08-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('954', '2016-08-26 12:51:47 工作内容', '2016-08-26 12:51:47', 'd4f127e4-ff5b-8ee1-0db7-1ebe823c8974', null, '2016-08-23', '2016-08-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('955', '2016-08-26 12:53:54 工作内容', '2016-08-26 12:53:54', 'd4f127e4-ff5b-8ee1-0db7-1ebe823c8974', null, '2016-08-26', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('957', '2016-08-26 12:55:50 工作内容', '2016-08-26 12:55:50', 'd4f127e4-ff5b-8ee1-0db7-1ebe823c8974', null, '2016-08-26', '2016-08-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('958', '2016-08-26 12:56:18 工作内容', '2016-08-26 12:56:18', 'e5dbc744-6f81-d99b-ce10-22fc18e88099', null, '2016-09-02', '2016-09-02', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('959', '2016-08-26 12:57:51 工作内容', '2016-08-26 12:57:51', '3efeaf3b-3dff-1f49-f89d-8f9fdab330f0', null, '2016-08-22', '2016-08-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('960', '2016-08-26 12:59:16 工作内容', '2016-08-26 12:59:16', '3efeaf3b-3dff-1f49-f89d-8f9fdab330f0', null, '2016-08-23', '2016-08-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('961', '2016-08-26 12:59:23 工作内容', '2016-08-26 12:59:23', 'bb994556-2301-76d7-a345-27e2eb0964ce', null, '2016-08-22', '2016-08-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('962', '2016-08-26 13:00:43 工作内容', '2016-08-26 13:00:43', 'bb994556-2301-76d7-a345-27e2eb0964ce', null, '2016-08-23', '2016-08-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('963', '2016-08-26 13:01:17 工作内容', '2016-08-26 13:01:17', 'bb994556-2301-76d7-a345-27e2eb0964ce', null, '2016-08-24', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('964', '2016-08-26 13:01:40 工作内容', '2016-08-26 13:01:40', 'bb994556-2301-76d7-a345-27e2eb0964ce', null, '2016-08-25', '2016-08-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('965', '2016-08-26 13:02:07 工作内容', '2016-08-26 13:02:07', '3efeaf3b-3dff-1f49-f89d-8f9fdab330f0', null, '2016-08-24', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('966', '2016-08-26 13:02:23 工作内容', '2016-08-26 13:02:23', 'd4f127e4-ff5b-8ee1-0db7-1ebe823c8974', null, '2016-09-04', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('967', '2016-08-26 13:02:36 工作内容', '2016-08-26 13:02:36', 'bb994556-2301-76d7-a345-27e2eb0964ce', null, '2016-08-26', '2016-08-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('968', '2016-08-26 13:03:14 工作内容', '2016-08-26 13:03:14', 'bb994556-2301-76d7-a345-27e2eb0964ce', null, '2016-08-26', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('969', '2016-08-26 13:03:41 工作内容', '2016-08-26 13:03:41', '3efeaf3b-3dff-1f49-f89d-8f9fdab330f0', null, '2016-08-25', '2016-08-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('970', '2016-08-26 13:04:06 工作内容', '2016-08-26 13:04:06', 'c646b7f1-d5f5-a041-a308-632e8d554854', null, '2016-09-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('971', '2016-08-26 13:04:40 工作内容', '2016-08-26 13:04:40', 'bb994556-2301-76d7-a345-27e2eb0964ce', null, '2016-09-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('972', '2016-08-26 13:08:23 工作内容', '2016-08-26 13:08:23', 'b613a49c-3833-f1c1-9597-d459a9c76099', null, '2016-08-26', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('973', '2016-08-26 13:09:40 工作内容', '2016-08-26 13:09:40', 'b613a49c-3833-f1c1-9597-d459a9c76099', null, '2016-08-23', '2016-08-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('974', '2016-08-26 13:09:59 工作内容', '2016-08-26 13:09:59', 'b613a49c-3833-f1c1-9597-d459a9c76099', null, '2016-09-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('975', '2016-08-26 13:16:22 工作内容', '2016-08-26 13:16:22', '3efeaf3b-3dff-1f49-f89d-8f9fdab330f0', null, '2016-09-01', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('976', '2016-08-26 13:19:51 工作内容', '2016-08-26 13:19:51', 'c353e072-e103-52bc-96b4-99af8a36ec0c', null, '2016-08-26', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('977', '2016-08-26 13:25:23 工作内容', '2016-08-26 13:25:23', '3efeaf3b-3dff-1f49-f89d-8f9fdab330f0', null, '2016-08-26', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('978', '2016-08-26 13:31:55 工作内容', '2016-08-26 13:31:55', '5c20dd6e-5b08-9873-8453-c82380d139b2', null, '2016-08-17', '2016-08-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('979', '2016-08-26 13:32:49 工作内容', '2016-08-26 13:32:49', '5c20dd6e-5b08-9873-8453-c82380d139b2', null, '2016-08-25', '2016-08-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('980', '2016-08-26 13:34:00 工作内容', '2016-08-26 13:34:00', '176fb5a5-0244-ab50-e8a6-b82570cc1216', null, '2016-08-24', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('981', '2016-08-26 13:34:36 工作内容', '2016-08-26 13:34:36', '176fb5a5-0244-ab50-e8a6-b82570cc1216', null, '2016-08-25', '2016-08-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('982', '2016-08-26 13:35:31 工作内容', '2016-08-26 13:35:31', '176fb5a5-0244-ab50-e8a6-b82570cc1216', null, '2016-08-26', '2016-08-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('983', '2016-08-26 13:36:37 工作内容', '2016-08-26 13:36:37', '176fb5a5-0244-ab50-e8a6-b82570cc1216', null, '2016-08-31', '2016-08-31', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('984', '2016-08-26 13:40:47 工作内容', '2016-08-26 13:40:47', '3efeaf3b-3dff-1f49-f89d-8f9fdab330f0', null, '2016-08-26', '2016-08-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('985', '2016-08-26 13:44:52 工作内容', '2016-08-26 13:44:52', 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', null, '2016-08-22', '2016-08-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('986', '2016-08-26 13:45:29 工作内容', '2016-08-26 13:45:29', 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', null, '2016-08-23', '2016-08-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('987', '2016-08-26 13:46:17 工作内容', '2016-08-26 13:46:17', 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', null, '2016-08-24', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('988', '2016-08-26 13:46:40 工作内容', '2016-08-26 13:46:40', 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', null, '2016-08-23', '2016-08-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('989', '2016-08-26 13:47:14 工作内容', '2016-08-26 13:47:14', 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', null, '2016-08-24', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('990', '2016-08-26 13:47:53 工作内容', '2016-08-26 13:47:53', 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', null, '2016-08-26', '2016-08-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('991', '2016-08-26 13:48:47 工作内容', '2016-08-26 13:48:47', 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', null, '2016-08-26', '2016-08-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('992', '2016-08-26 13:49:12 工作内容', '2016-08-26 13:49:12', 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', null, '2016-09-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('993', '2016-08-26 13:49:40 工作内容', '2016-08-26 13:49:40', 'cb2c8a8b-e812-dc06-bafe-58ec94b29555', null, '2016-09-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('995', '2016-08-26 14:01:30 工作内容', '2016-08-26 14:01:30', 'c353e072-e103-52bc-96b4-99af8a36ec0c', null, '2016-08-24', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('996', '2016-08-26 14:01:53 工作内容', '2016-08-26 14:01:53', 'c353e072-e103-52bc-96b4-99af8a36ec0c', null, '2016-08-26', '2016-08-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('997', '2016-08-26 14:02:29 工作内容', '2016-08-26 14:02:29', 'c353e072-e103-52bc-96b4-99af8a36ec0c', null, '2016-08-26', '2016-08-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('998', '2016-08-26 14:03:00 工作内容', '2016-08-26 14:03:00', 'c353e072-e103-52bc-96b4-99af8a36ec0c', null, '2016-08-24', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('999', '2016-08-26 14:03:52 工作内容', '2016-08-26 14:03:52', 'c353e072-e103-52bc-96b4-99af8a36ec0c', null, '2016-08-26', '2016-08-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1000', '2016-08-26 14:04:28 工作内容', '2016-08-26 14:04:28', 'c353e072-e103-52bc-96b4-99af8a36ec0c', null, '2016-08-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1001', '2016-08-26 14:04:56 工作内容', '2016-08-26 14:04:56', 'e5dbc744-6f81-d99b-ce10-22fc18e88099', null, '2016-08-26', '2016-08-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1002', '2016-08-26 14:08:24 工作内容', '2016-08-26 14:08:24', 'c353e072-e103-52bc-96b4-99af8a36ec0c', null, '2016-09-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1003', '2016-08-26 14:15:00 工作内容', '2016-08-26 14:15:00', 'e5dbc744-6f81-d99b-ce10-22fc18e88099', null, '2016-08-22', '2016-08-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1004', '2016-08-26 14:16:03 工作内容', '2016-08-26 14:16:03', 'e5dbc744-6f81-d99b-ce10-22fc18e88099', null, '2016-08-23', '2016-08-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1005', '2016-08-26 14:17:28 工作内容', '2016-08-26 14:17:28', 'e5dbc744-6f81-d99b-ce10-22fc18e88099', null, '2016-08-24', '2016-08-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1006', '2016-08-26 14:19:03 工作内容', '2016-08-26 14:19:03', 'e5dbc744-6f81-d99b-ce10-22fc18e88099', null, '2016-08-25', '2016-08-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1007', '2016-08-26 14:22:54 工作内容', '2016-08-26 14:22:54', 'e5dbc744-6f81-d99b-ce10-22fc18e88099', null, '2016-08-26', '2016-08-26', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1008', '2016-09-02 10:44:09 工作内容', '2016-09-02 10:44:09', '070d4224-2ddb-33a9-0084-4093d21d5059', null, '2016-09-01', '2016-09-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1009', '2016-09-02 10:44:48 工作内容', '2016-09-02 10:44:48', 'fd315549-4366-687c-cbd7-0ebce3412ffb', null, '2016-09-02', '2016-09-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1010', '2016-09-02 10:45:34 工作内容', '2016-09-02 10:45:34', '070d4224-2ddb-33a9-0084-4093d21d5059', null, '2016-08-30', '2016-08-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1012', '2016-09-02 10:46:57 工作内容', '2016-09-02 10:46:57', '070d4224-2ddb-33a9-0084-4093d21d5059', null, '2016-09-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1013', '2016-09-02 10:47:30 工作内容', '2016-09-02 10:47:30', '72bcd514-f3cd-572d-9a02-eaa4e37098bb', null, '2016-08-30', '2016-08-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1014', '2016-09-02 10:48:04 工作内容', '2016-09-02 10:48:04', '72bcd514-f3cd-572d-9a02-eaa4e37098bb', null, '2016-09-02', '2016-09-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1015', '2016-09-02 10:48:17 工作内容', '2016-09-02 10:48:17', '070d4224-2ddb-33a9-0084-4093d21d5059', null, '2016-09-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1016', '2016-09-02 10:48:45 工作内容', '2016-09-02 10:48:45', '070d4224-2ddb-33a9-0084-4093d21d5059', null, '2016-08-30', '2016-08-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1017', '2016-09-02 10:49:21 工作内容', '2016-09-02 10:49:21', '72bcd514-f3cd-572d-9a02-eaa4e37098bb', null, '2016-09-09', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1019', '2016-09-02 10:49:52 工作内容', '2016-09-02 10:49:52', 'fd315549-4366-687c-cbd7-0ebce3412ffb', null, '2016-09-05', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1020', '2016-09-02 10:51:00 工作内容', '2016-09-02 10:51:00', 'd56f958a-f62c-bdeb-967e-2534c761e43b', null, '2016-08-31', '2016-08-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1021', '2016-09-02 10:52:13 工作内容', '2016-09-02 10:52:13', 'd56f958a-f62c-bdeb-967e-2534c761e43b', null, '2016-09-01', '2016-09-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1022', '2016-09-02 10:52:30 工作内容', '2016-09-02 10:52:30', 'd4226435-5785-ff89-74a8-9e0f4443c017', null, '2016-08-31', '2016-08-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1023', '2016-09-02 10:52:49 工作内容', '2016-09-02 10:52:49', 'fd315549-4366-687c-cbd7-0ebce3412ffb', null, '2016-09-09', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1024', '2016-09-02 10:52:58 工作内容', '2016-09-02 10:52:58', 'd56f958a-f62c-bdeb-967e-2534c761e43b', null, '2016-09-02', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1025', '2016-09-02 10:53:51 工作内容', '2016-09-02 10:53:51', 'd4226435-5785-ff89-74a8-9e0f4443c017', null, '2016-09-01', '2016-09-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1026', '2016-09-02 10:54:29 工作内容', '2016-09-02 10:54:29', 'd56f958a-f62c-bdeb-967e-2534c761e43b', null, '2016-09-02', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1027', '2016-09-02 10:55:14 工作内容', '2016-09-02 10:55:14', 'd4226435-5785-ff89-74a8-9e0f4443c017', null, '2016-09-05', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1028', '2016-09-02 10:56:06 工作内容', '2016-09-02 10:56:06', 'd4226435-5785-ff89-74a8-9e0f4443c017', null, '2016-09-09', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1029', '2016-09-02 10:56:25 工作内容', '2016-09-02 10:56:25', 'd56f958a-f62c-bdeb-967e-2534c761e43b', null, '2016-09-02', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1030', '2016-09-02 10:59:02 工作内容', '2016-09-02 10:59:02', '9e65cd70-676e-5b43-3a45-d5e05998cb77', null, '2016-08-29', '2016-08-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1031', '2016-09-02 10:59:13 工作内容', '2016-09-02 10:59:13', 'fd315549-4366-687c-cbd7-0ebce3412ffb', null, '2016-08-29', '2016-08-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1032', '2016-09-02 10:59:35 工作内容', '2016-09-02 10:59:35', '070d4224-2ddb-33a9-0084-4093d21d5059', null, '2016-09-09', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1033', '2016-09-02 11:01:31 工作内容', '2016-09-02 11:01:31', 'd56f958a-f62c-bdeb-967e-2534c761e43b', null, '2016-09-07', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1034', '2016-09-02 11:03:47 工作内容', '2016-09-02 11:03:47', 'd56f958a-f62c-bdeb-967e-2534c761e43b', null, '2016-09-08', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1035', '2016-09-02 11:04:58 工作内容', '2016-09-02 11:04:58', '9e65cd70-676e-5b43-3a45-d5e05998cb77', null, '2016-08-30', '2016-08-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1036', '2016-09-02 11:05:17 工作内容', '2016-09-02 11:05:17', '9e65cd70-676e-5b43-3a45-d5e05998cb77', null, '2016-08-31', '2016-08-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1037', '2016-09-02 11:07:33 工作内容', '2016-09-02 11:07:33', 'f0add40f-6bce-5b91-5c82-2815a6ae3b7c', null, '2016-08-29', '2016-08-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1038', '2016-09-02 11:07:38 工作内容', '2016-09-02 11:07:38', '607aebf0-0a70-1ecd-d8f6-160e4fe327d9', null, '2016-09-05', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1039', '2016-09-02 11:08:13 工作内容', '2016-09-02 11:08:13', '9e65cd70-676e-5b43-3a45-d5e05998cb77', null, '2016-09-01', '2016-09-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1040', '2016-09-02 11:08:32 工作内容', '2016-09-02 11:08:32', '607aebf0-0a70-1ecd-d8f6-160e4fe327d9', null, '2016-09-06', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1041', '2016-09-02 11:09:05 工作内容', '2016-09-02 11:09:05', 'f0add40f-6bce-5b91-5c82-2815a6ae3b7c', null, '2016-09-02', '未完成', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1043', '2016-09-02 11:09:11 工作内容', '2016-09-02 11:09:11', '9e65cd70-676e-5b43-3a45-d5e05998cb77', null, '2016-09-02', '2016-09-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1044', '2016-09-02 11:10:07 工作内容', '2016-09-02 11:10:07', 'f0add40f-6bce-5b91-5c82-2815a6ae3b7c', null, '2016-09-06', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1045', '2016-09-02 11:11:01 工作内容', '2016-09-02 11:11:01', 'f0add40f-6bce-5b91-5c82-2815a6ae3b7c', null, '2016-09-09', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1046', '2016-09-02 11:11:13 工作内容', '2016-09-02 11:11:13', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-09-05', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1047', '2016-09-02 11:11:45 工作内容', '2016-09-02 11:11:45', 'f0add40f-6bce-5b91-5c82-2815a6ae3b7c', null, '2016-09-09', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1048', '2016-09-02 11:11:54 工作内容', '2016-09-02 11:11:54', '9e65cd70-676e-5b43-3a45-d5e05998cb77', null, '2016-09-02', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1049', '2016-09-02 11:12:55 工作内容', '2016-09-02 11:12:55', '9e65cd70-676e-5b43-3a45-d5e05998cb77', null, '2016-09-12', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1050', '2016-09-02 11:14:58 工作内容', '2016-09-02 11:14:58', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-09-06', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1051', '2016-09-02 11:17:06 工作内容', '2016-09-02 11:17:06', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-09-07', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1053', '2016-09-02 11:18:31 工作内容', '2016-09-02 11:18:31', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-09-08', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1054', '2016-09-02 11:19:03 工作内容', '2016-09-02 11:19:03', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-09-09', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1055', '2016-09-02 11:21:59 工作内容', '2016-09-02 11:21:59', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-08-29', '2016-08-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1056', '2016-09-02 11:25:03 工作内容', '2016-09-02 11:25:03', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-08-30', '2016-08-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1057', '2016-09-02 11:26:22 工作内容', '2016-09-02 11:26:22', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-08-31', '2016-08-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1058', '2016-09-02 11:32:20 工作内容', '2016-09-02 11:32:20', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-09-01', '2016-09-01', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1059', '2016-09-02 12:33:29 工作内容', '2016-09-02 12:33:29', 'fb16052d-a386-6cb8-76ad-59cbf63e4b4a', null, '2016-09-02', '2016-09-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1060', '2016-09-02 12:33:57 工作内容', '2016-09-02 12:33:57', '1a99a80a-26c3-42bb-decb-381d4d4dbb03', null, '2016-09-02', '2016-09-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1061', '2016-09-02 12:34:58 工作内容', '2016-09-02 12:34:58', 'fb16052d-a386-6cb8-76ad-59cbf63e4b4a', null, '2016-09-02', '2016-09-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1062', '2016-09-02 12:40:11 工作内容', '2016-09-02 12:40:11', 'fb16052d-a386-6cb8-76ad-59cbf63e4b4a', null, '2016-09-02', '2016-09-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1063', '2016-09-02 12:40:52 工作内容', '2016-09-02 12:40:52', 'fb16052d-a386-6cb8-76ad-59cbf63e4b4a', null, '2016-09-02', '2016-09-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1064', '2016-09-02 12:41:47 工作内容', '2016-09-02 12:41:47', 'fb16052d-a386-6cb8-76ad-59cbf63e4b4a', null, '2016-09-02', '2016-09-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1065', '2016-09-02 12:42:28 工作内容', '2016-09-02 12:42:28', 'fb16052d-a386-6cb8-76ad-59cbf63e4b4a', null, '2016-09-09', '2016-09-09', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1066', '2016-09-02 12:43:07 工作内容', '2016-09-02 12:43:07', 'fb16052d-a386-6cb8-76ad-59cbf63e4b4a', null, '2016-09-09', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1067', '2016-09-02 12:44:01 工作内容', '2016-09-02 12:44:01', 'fb16052d-a386-6cb8-76ad-59cbf63e4b4a', null, '2016-09-09', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1068', '2016-09-09 13:06:17 工作内容', '2016-09-09 13:06:17', '5683591b-f2d6-15bf-e45e-7fa2b6b51e07', null, '2016-09-05', '2016-09-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1069', '2016-09-09 13:06:44 工作内容', '2016-09-09 13:06:44', '5683591b-f2d6-15bf-e45e-7fa2b6b51e07', null, '2016-09-06', '2016-09-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1070', '2016-09-09 13:07:36 工作内容', '2016-09-09 13:07:36', '71b8d79b-a548-7c22-34c0-c44b2f8f7757', null, '2016-09-16', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1071', '2016-09-09 13:07:57 工作内容', '2016-09-09 13:07:57', '0e89d21d-ec11-ebe5-cf53-6559a675e027', null, '2016-09-05', '2016-09-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1072', '2016-09-09 13:09:40 工作内容', '2016-09-09 13:09:40', '71b8d79b-a548-7c22-34c0-c44b2f8f7757', null, '2016-09-09', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1073', '2016-09-09 13:10:08 工作内容', '2016-09-09 13:10:08', '71b8d79b-a548-7c22-34c0-c44b2f8f7757', null, '2016-09-16', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1074', '2016-09-09 13:11:53 工作内容', '2016-09-09 13:11:53', '5683591b-f2d6-15bf-e45e-7fa2b6b51e07', null, '2016-09-07', '2016-09-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1075', '2016-09-09 13:13:42 工作内容', '2016-09-09 13:13:42', '0e89d21d-ec11-ebe5-cf53-6559a675e027', null, '2016-09-09', '2016-09-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1076', '2016-09-09 13:15:23 工作内容', '2016-09-09 13:15:23', '0e89d21d-ec11-ebe5-cf53-6559a675e027', null, '2016-09-09', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1077', '2016-09-09 13:15:52 工作内容', '2016-09-09 13:15:52', '0e89d21d-ec11-ebe5-cf53-6559a675e027', null, '2016-09-07', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1078', '2016-09-09 13:16:55 工作内容', '2016-09-09 13:16:55', '5683591b-f2d6-15bf-e45e-7fa2b6b51e07', null, '2016-09-08', '2016-09-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1079', '2016-09-09 13:17:21 工作内容', '2016-09-09 13:17:21', '9379bfc7-81e8-f9bf-ad74-a71a58d39b95', null, '2016-09-05', '2016-09-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1080', '2016-09-09 13:17:52 工作内容', '2016-09-09 13:17:52', '9379bfc7-81e8-f9bf-ad74-a71a58d39b95', null, '2016-09-06', '2016-09-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1081', '2016-09-09 13:18:32 工作内容', '2016-09-09 13:18:32', '9379bfc7-81e8-f9bf-ad74-a71a58d39b95', null, '2016-09-06', '2016-09-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1082', '2016-09-09 13:20:01 工作内容', '2016-09-09 13:20:01', 'aea3e3e9-e8cd-06db-92e3-b2e2f4bae048', null, '2016-09-05', '2016-09-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1083', '2016-09-09 13:20:54 工作内容', '2016-09-09 13:20:54', 'aea3e3e9-e8cd-06db-92e3-b2e2f4bae048', null, '2016-09-06', '2016-09-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1084', '2016-09-09 13:21:55 工作内容', '2016-09-09 13:21:55', '9379bfc7-81e8-f9bf-ad74-a71a58d39b95', null, '2016-09-09', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1085', '2016-09-09 13:22:10 工作内容', '2016-09-09 13:22:10', 'aea3e3e9-e8cd-06db-92e3-b2e2f4bae048', null, '2016-09-08', '2016-09-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1086', '2016-09-09 13:23:19 工作内容', '2016-09-09 13:23:19', 'aea3e3e9-e8cd-06db-92e3-b2e2f4bae048', null, '2016-09-09', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1087', '2016-09-09 13:25:10 工作内容', '2016-09-09 13:25:10', '89beab77-12f7-436f-4e1d-e45df4750eef', null, '2016-09-05', '2016-09-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1088', '2016-09-09 13:25:34 工作内容', '2016-09-09 13:25:34', '89beab77-12f7-436f-4e1d-e45df4750eef', null, '2016-09-06', '2016-09-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1089', '2016-09-09 13:26:23 工作内容', '2016-09-09 13:26:23', '89beab77-12f7-436f-4e1d-e45df4750eef', null, '2016-09-08', '2016-09-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1090', '2016-09-09 13:26:56 工作内容', '2016-09-09 13:26:56', '89beab77-12f7-436f-4e1d-e45df4750eef', null, '2016-09-09', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1091', '2016-09-09 13:27:23 工作内容', '2016-09-09 13:27:23', '89beab77-12f7-436f-4e1d-e45df4750eef', null, '2016-09-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1092', '2016-09-09 13:27:30 工作内容', '2016-09-09 13:27:30', '5683591b-f2d6-15bf-e45e-7fa2b6b51e07', null, '2016-09-09', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1093', '2016-09-09 13:27:58 工作内容', '2016-09-09 13:27:58', '89beab77-12f7-436f-4e1d-e45df4750eef', null, '2016-09-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1095', '2016-09-09 13:30:11 工作内容', '2016-09-09 13:30:11', '6e9a8e92-ba3e-3039-1393-57be9874a2ab', null, '2016-09-06', '2016-09-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1096', '2016-09-09 13:32:23 工作内容', '2016-09-09 13:32:23', '6e9a8e92-ba3e-3039-1393-57be9874a2ab', null, '2016-09-09', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1097', '2016-09-09 13:34:12 工作内容', '2016-09-09 13:34:12', '6e9a8e92-ba3e-3039-1393-57be9874a2ab', null, '2016-09-09', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1098', '2016-09-09 13:34:12 工作内容', '2016-09-09 13:34:12', '6e9a8e92-ba3e-3039-1393-57be9874a2ab', null, '2016-09-06', '2016-09-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1099', '2016-09-09 13:34:58 工作内容', '2016-09-09 13:34:58', '9379bfc7-81e8-f9bf-ad74-a71a58d39b95', null, '2016-09-09', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1100', '2016-09-09 13:35:47 工作内容', '2016-09-09 13:35:47', '9379bfc7-81e8-f9bf-ad74-a71a58d39b95', null, '2016-09-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1101', '2016-09-09 13:35:58 工作内容', '2016-09-09 13:35:58', '6e9a8e92-ba3e-3039-1393-57be9874a2ab', null, '2016-09-09', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1102', '2016-09-09 13:36:14 工作内容', '2016-09-09 13:36:14', '9379bfc7-81e8-f9bf-ad74-a71a58d39b95', null, '2016-09-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1103', '2016-09-09 13:37:20 工作内容', '2016-09-09 13:37:20', '9379bfc7-81e8-f9bf-ad74-a71a58d39b95', null, '2016-09-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1104', '2016-09-09 13:37:39 工作内容', '2016-09-09 13:37:39', '6e9a8e92-ba3e-3039-1393-57be9874a2ab', null, '2016-09-12', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1105', '2016-09-09 13:38:23 工作内容', '2016-09-09 13:38:23', '6e9a8e92-ba3e-3039-1393-57be9874a2ab', null, '2016-09-09', '2016-09-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1106', '2016-09-09 13:39:25 工作内容', '2016-09-09 13:39:25', '6e9a8e92-ba3e-3039-1393-57be9874a2ab', null, '2016-09-12', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1107', '2016-09-09 13:39:59 工作内容', '2016-09-09 13:39:59', '04a5704d-ba61-6dd6-117f-ae22cf6d6cb8', null, '2016-09-05', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1108', '2016-09-09 13:40:27 工作内容', '2016-09-09 13:40:27', '6e9a8e92-ba3e-3039-1393-57be9874a2ab', null, '2016-09-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1109', '2016-09-09 13:40:50 工作内容', '2016-09-09 13:40:50', '04a5704d-ba61-6dd6-117f-ae22cf6d6cb8', null, '2016-09-06', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1110', '2016-09-09 13:42:04 工作内容', '2016-09-09 13:42:04', 'ffef8bc5-702b-4bfb-ee52-6fb1e52e41d9', null, '2016-09-05', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1111', '2016-09-09 13:42:29 工作内容', '2016-09-09 13:42:29', 'ffef8bc5-702b-4bfb-ee52-6fb1e52e41d9', null, '2016-09-06', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1112', '2016-09-09 13:44:30 工作内容', '2016-09-09 13:44:30', 'ffef8bc5-702b-4bfb-ee52-6fb1e52e41d9', null, '2016-09-07', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1113', '2016-09-09 13:45:15 工作内容', '2016-09-09 13:45:15', 'ffef8bc5-702b-4bfb-ee52-6fb1e52e41d9', null, '2016-09-08', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1114', '2016-09-09 13:45:42 工作内容', '2016-09-09 13:45:42', 'ffef8bc5-702b-4bfb-ee52-6fb1e52e41d9', null, '2016-09-09', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1115', '2016-09-09 13:46:53 工作内容', '2016-09-09 13:46:53', 'ffef8bc5-702b-4bfb-ee52-6fb1e52e41d9', null, '2016-09-09', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1116', '2016-09-09 13:47:20 工作内容', '2016-09-09 13:47:20', 'ffef8bc5-702b-4bfb-ee52-6fb1e52e41d9', null, '2016-09-14', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1117', '2016-09-09 13:55:24 工作内容', '2016-09-09 13:55:24', '567be40c-41e8-6839-25dd-6a1b288085d1', null, '2016-09-08', '2016-09-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1118', '2016-09-09 13:57:47 工作内容', '2016-09-09 13:57:47', '567be40c-41e8-6839-25dd-6a1b288085d1', null, '2016-09-06', '2016-09-06', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1119', '2016-09-09 13:58:12 工作内容', '2016-09-09 13:58:12', '71b8d79b-a548-7c22-34c0-c44b2f8f7757', null, '2016-09-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1120', '2016-09-09 13:58:57 工作内容', '2016-09-09 13:58:57', '567be40c-41e8-6839-25dd-6a1b288085d1', null, '2016-09-05', '2016-09-05', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1121', '2016-09-09 13:59:30 工作内容', '2016-09-09 13:59:30', '71b8d79b-a548-7c22-34c0-c44b2f8f7757', null, '2016-09-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1122', '2016-09-09 14:03:26 工作内容', '2016-09-09 14:03:26', '567be40c-41e8-6839-25dd-6a1b288085d1', null, '2016-09-09', '2016-09-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1123', '2016-09-09 14:10:32 工作内容', '2016-09-09 14:10:32', '567be40c-41e8-6839-25dd-6a1b288085d1', null, '2016-09-14', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1124', '2016-09-18 16:39:23 工作内容', '2016-09-18 16:39:23', '94e8c94a-7d2c-f116-af7f-8a7c90c28575', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1125', '2016-09-18 16:40:02 工作内容', '2016-09-18 16:40:02', '94e8c94a-7d2c-f116-af7f-8a7c90c28575', null, '2016-09-18', '2016-09-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1126', '2016-09-18 16:40:43 工作内容', '2016-09-18 16:40:43', '94e8c94a-7d2c-f116-af7f-8a7c90c28575', null, '2016-09-23', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1127', '2016-09-18 16:42:17 工作内容', '2016-09-18 16:42:17', 'dfdfb9b0-ee1a-cd23-7745-077ff3aeabd0', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1128', '2016-09-18 16:43:04 工作内容', '2016-09-18 16:43:04', 'dfdfb9b0-ee1a-cd23-7745-077ff3aeabd0', null, '2016-09-18', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1129', '2016-09-18 16:44:28 工作内容', '2016-09-18 16:44:28', 'dfdfb9b0-ee1a-cd23-7745-077ff3aeabd0', null, '2016-09-20', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1130', '2016-09-18 16:45:19 工作内容', '2016-09-18 16:45:19', 'dfdfb9b0-ee1a-cd23-7745-077ff3aeabd0', null, '2016-09-23', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1131', '2016-09-18 16:45:59 工作内容', '2016-09-18 16:45:59', '327a2e3f-e032-8b7c-345c-ad75df9d6671', null, '2016-09-12', '2016-09-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1132', '2016-09-18 16:47:40 工作内容', '2016-09-18 16:47:40', '5167f091-e61f-f60d-e8f3-233e1a42ce33', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1133', '2016-09-18 16:48:29 工作内容', '2016-09-18 16:48:29', '5167f091-e61f-f60d-e8f3-233e1a42ce33', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1134', '2016-09-18 16:48:31 工作内容', '2016-09-18 16:48:31', '327a2e3f-e032-8b7c-345c-ad75df9d6671', null, '2016-09-13', '2016-09-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1135', '2016-09-18 16:49:31 工作内容', '2016-09-18 16:49:31', '5167f091-e61f-f60d-e8f3-233e1a42ce33', null, '2016-09-18', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1136', '2016-09-18 16:49:33 工作内容', '2016-09-18 16:49:33', '327a2e3f-e032-8b7c-345c-ad75df9d6671', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1137', '2016-09-18 16:50:29 工作内容', '2016-09-18 16:50:29', '327a2e3f-e032-8b7c-345c-ad75df9d6671', null, '2016-09-18', '2016-09-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1138', '2016-09-18 16:50:45 工作内容', '2016-09-18 16:50:45', '5167f091-e61f-f60d-e8f3-233e1a42ce33', null, '2016-09-23', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1139', '2016-09-18 16:50:49 工作内容', '2016-09-18 16:50:49', '327a2e3f-e032-8b7c-345c-ad75df9d6671', null, '2016-09-23', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1140', '2016-09-18 16:51:46 工作内容', '2016-09-18 16:51:46', '5167f091-e61f-f60d-e8f3-233e1a42ce33', null, '2016-09-23', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1141', '2016-09-18 16:54:14 工作内容', '2016-09-18 16:54:14', '49e63bda-90af-6823-bcec-98212d64d424', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1142', '2016-09-18 16:54:54 工作内容', '2016-09-18 16:54:54', '49e63bda-90af-6823-bcec-98212d64d424', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1143', '2016-09-18 16:55:42 工作内容', '2016-09-18 16:55:42', '49e63bda-90af-6823-bcec-98212d64d424', null, '2016-09-18', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1144', '2016-09-18 16:56:38 工作内容', '2016-09-18 16:56:38', '49e63bda-90af-6823-bcec-98212d64d424', null, '2016-09-23', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1145', '2016-09-18 16:57:11 工作内容', '2016-09-18 16:57:11', '49e63bda-90af-6823-bcec-98212d64d424', null, '2016-09-23', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1146', '2016-09-18 17:10:10 工作内容', '2016-09-18 17:10:10', 'c9a480f3-2a8d-378e-2652-1e76c25198cd', null, '2016-09-12', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1147', '2016-09-18 17:10:39 工作内容', '2016-09-18 17:10:39', '01b5847b-cf1d-b1d9-6547-34c96ca58534', null, '2016-09-12', '2016-09-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1148', '2016-09-18 17:11:04 工作内容', '2016-09-18 17:11:04', '01b5847b-cf1d-b1d9-6547-34c96ca58534', null, '2016-09-18', '2016-09-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1149', '2016-09-18 17:11:50 工作内容', '2016-09-18 17:11:50', '01b5847b-cf1d-b1d9-6547-34c96ca58534', null, '2016-09-13', '2016-09-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1150', '2016-09-18 17:11:52 工作内容', '2016-09-18 17:11:52', 'c9a480f3-2a8d-378e-2652-1e76c25198cd', null, '2016-09-13', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1151', '2016-09-18 17:12:44 工作内容', '2016-09-18 17:12:44', '01b5847b-cf1d-b1d9-6547-34c96ca58534', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1152', '2016-09-18 17:12:58 工作内容', '2016-09-18 17:12:58', 'c9a480f3-2a8d-378e-2652-1e76c25198cd', null, '2016-09-14', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1153', '2016-09-18 17:13:17 工作内容', '2016-09-18 17:13:17', '01b5847b-cf1d-b1d9-6547-34c96ca58534', null, '2016-09-23', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1154', '2016-09-18 17:13:41 工作内容', '2016-09-18 17:13:41', '01b5847b-cf1d-b1d9-6547-34c96ca58534', null, '2016-09-23', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1155', '2016-09-18 17:13:54 工作内容', '2016-09-18 17:13:54', 'c9a480f3-2a8d-378e-2652-1e76c25198cd', null, '2016-09-18', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1156', '2016-09-18 17:15:07 工作内容', '2016-09-18 17:15:07', 'cf9011fd-336d-b663-ca6e-8708dbe65bfe', null, '2016-09-12', '2016-09-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1157', '2016-09-18 17:15:21 工作内容', '2016-09-18 17:15:21', 'c9a480f3-2a8d-378e-2652-1e76c25198cd', null, '2016-09-18', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1158', '2016-09-18 17:16:34 工作内容', '2016-09-18 17:16:34', 'cf9011fd-336d-b663-ca6e-8708dbe65bfe', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1159', '2016-09-18 17:16:59 工作内容', '2016-09-18 17:16:59', 'c9a480f3-2a8d-378e-2652-1e76c25198cd', null, '2016-09-22', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1160', '2016-09-18 17:16:59 工作内容', '2016-09-18 17:16:59', 'cf9011fd-336d-b663-ca6e-8708dbe65bfe', null, '2016-09-14', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1161', '2016-09-18 17:17:30 工作内容', '2016-09-18 17:17:30', 'cf9011fd-336d-b663-ca6e-8708dbe65bfe', null, '2016-09-13', '2016-09-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1162', '2016-09-18 17:17:50 工作内容', '2016-09-18 17:17:50', 'cf9011fd-336d-b663-ca6e-8708dbe65bfe', null, '2016-09-23', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1163', '2016-09-18 17:18:06 工作内容', '2016-09-18 17:18:06', 'cf9011fd-336d-b663-ca6e-8708dbe65bfe', null, '2016-09-23', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1164', '2016-09-18 08:39:12 工作内容', '2016-09-18 08:39:12', 'd33a5b6b-ae1d-72fc-6e78-276d21594147', null, '2016-09-13', '2016-09-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1165', '2016-09-18 08:39:51 工作内容', '2016-09-18 08:39:51', 'd33a5b6b-ae1d-72fc-6e78-276d21594147', null, '2016-09-18', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1166', '2016-09-18 08:41:36 工作内容', '2016-09-18 08:41:36', 'd33a5b6b-ae1d-72fc-6e78-276d21594147', null, '2016-09-19', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1167', '2016-09-18 08:43:14 工作内容', '2016-09-18 08:43:14', 'd33a5b6b-ae1d-72fc-6e78-276d21594147', null, '2016-09-20', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1168', '2016-09-18 08:43:51 工作内容', '2016-09-18 08:43:51', 'd33a5b6b-ae1d-72fc-6e78-276d21594147', null, '2016-09-23', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1169', '2016-09-23 13:10:03 工作内容', '2016-09-23 13:10:03', 'fd756f51-eb07-cdef-9578-8557a41ba367', null, '2016-09-18', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1170', '2016-09-23 13:11:50 工作内容', '2016-09-23 13:11:50', 'fd756f51-eb07-cdef-9578-8557a41ba367', null, '2016-09-19', '2016-09-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1171', '2016-09-23 13:15:17 工作内容', '2016-09-23 13:15:17', 'fd756f51-eb07-cdef-9578-8557a41ba367', null, '2016-09-20', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1172', '2016-09-23 13:19:41 工作内容', '2016-09-23 13:19:41', 'f23de0be-6fc5-88bc-1be4-75d9899afa88', null, '2016-09-19', '2016-09-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1173', '2016-09-23 13:19:59 工作内容', '2016-09-23 13:19:59', 'fd756f51-eb07-cdef-9578-8557a41ba367', null, '2016-09-21', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1174', '2016-09-23 13:21:00 工作内容', '2016-09-23 13:21:00', 'fd756f51-eb07-cdef-9578-8557a41ba367', null, '2016-09-22', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1175', '2016-09-23 13:21:18 工作内容', '2016-09-23 13:21:18', 'fd756f51-eb07-cdef-9578-8557a41ba367', null, '2016-09-23', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1176', '2016-09-23 13:23:08 工作内容', '2016-09-23 13:23:08', 'fd756f51-eb07-cdef-9578-8557a41ba367', null, '2016-09-23', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1177', '2016-09-23 13:23:40 工作内容', '2016-09-23 13:23:40', 'f23de0be-6fc5-88bc-1be4-75d9899afa88', null, '2016-09-21', '2016-09-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1178', '2016-09-23 13:24:13 工作内容', '2016-09-23 13:24:13', 'fd756f51-eb07-cdef-9578-8557a41ba367', null, '2016-09-30', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1179', '2016-09-23 13:27:02 工作内容', '2016-09-23 13:27:02', 'f23de0be-6fc5-88bc-1be4-75d9899afa88', null, '2016-09-22', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1180', '2016-09-23 13:27:06 工作内容', '2016-09-23 13:27:06', '6949ed1f-b812-41f1-6e98-718bae92ca69', null, '2016-09-19', '2016-09-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1181', '2016-09-23 13:27:44 工作内容', '2016-09-23 13:27:44', '6949ed1f-b812-41f1-6e98-718bae92ca69', null, '2016-09-20', '2016-09-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1182', '2016-09-23 13:27:59 工作内容', '2016-09-23 13:27:59', 'f23de0be-6fc5-88bc-1be4-75d9899afa88', null, '2016-09-23', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1183', '2016-09-23 13:28:19 工作内容', '2016-09-23 13:28:19', '6949ed1f-b812-41f1-6e98-718bae92ca69', null, '2016-09-21', '2016-09-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1184', '2016-09-23 13:28:59 工作内容', '2016-09-23 13:28:59', '6949ed1f-b812-41f1-6e98-718bae92ca69', null, '2016-09-22', '2016-09-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1185', '2016-09-23 13:29:22 工作内容', '2016-09-23 13:29:22', '6949ed1f-b812-41f1-6e98-718bae92ca69', null, '2016-09-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1186', '2016-09-23 13:29:45 工作内容', '2016-09-23 13:29:45', '6949ed1f-b812-41f1-6e98-718bae92ca69', null, '2016-09-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1187', '2016-09-23 13:30:03 工作内容', '2016-09-23 13:30:03', '6949ed1f-b812-41f1-6e98-718bae92ca69', null, '2016-09-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1188', '2016-09-23 13:32:36 工作内容', '2016-09-23 13:32:36', 'ae817d41-2bd1-4fdd-7004-f614145bdf05', null, '2016-09-23', '2016-09-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1189', '2016-09-23 13:33:18 工作内容', '2016-09-23 13:33:18', '1cdb6e27-0051-0ce3-7324-136ddad05798', null, '2016-09-23', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1190', '2016-09-23 13:33:42 工作内容', '2016-09-23 13:33:42', 'ae817d41-2bd1-4fdd-7004-f614145bdf05', null, '2016-09-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1191', '2016-09-23 13:33:59 工作内容', '2016-09-23 13:33:59', '1cdb6e27-0051-0ce3-7324-136ddad05798', null, '2016-09-23', '2016-09-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1192', '2016-09-23 13:34:24 工作内容', '2016-09-23 13:34:24', 'ae817d41-2bd1-4fdd-7004-f614145bdf05', null, '2016-09-30', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1193', '2016-09-23 13:34:49 工作内容', '2016-09-23 13:34:49', 'f23de0be-6fc5-88bc-1be4-75d9899afa88', null, '2016-09-27', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1194', '2016-09-23 13:35:12 工作内容', '2016-09-23 13:35:12', '1cdb6e27-0051-0ce3-7324-136ddad05798', null, '2016-09-23', '2016-09-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1195', '2016-09-23 13:35:20 工作内容', '2016-09-23 13:35:20', '192adb08-d801-9ae3-b1a8-224d63269915', null, '2016-09-20', '2016-09-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1196', '2016-09-23 13:35:34 工作内容', '2016-09-23 13:35:34', 'ae817d41-2bd1-4fdd-7004-f614145bdf05', null, '2016-09-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1197', '2016-09-23 13:36:36 工作内容', '2016-09-23 13:36:36', 'ae817d41-2bd1-4fdd-7004-f614145bdf05', null, '2016-09-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1198', '2016-09-23 13:37:25 工作内容', '2016-09-23 13:37:25', '192adb08-d801-9ae3-b1a8-224d63269915', null, '2016-09-30', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1199', '2016-09-23 13:37:51 工作内容', '2016-09-23 13:37:51', '1cdb6e27-0051-0ce3-7324-136ddad05798', null, '2016-09-23', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1200', '2016-09-23 13:37:57 工作内容', '2016-09-23 13:37:57', 'f23de0be-6fc5-88bc-1be4-75d9899afa88', null, '2016-09-29', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1201', '2016-09-23 13:39:08 工作内容', '2016-09-23 13:39:08', '192adb08-d801-9ae3-b1a8-224d63269915', null, '2016-09-26', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1202', '2016-09-23 13:40:01 工作内容', '2016-09-23 13:40:01', '192adb08-d801-9ae3-b1a8-224d63269915', null, '2016-09-27', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1203', '2016-09-23 13:43:06 工作内容', '2016-09-23 13:43:06', '1cdb6e27-0051-0ce3-7324-136ddad05798', null, '2016-09-23', '2016-09-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1204', '2016-09-23 13:43:37 工作内容', '2016-09-23 13:43:37', '1cdb6e27-0051-0ce3-7324-136ddad05798', null, '2016-10-07', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1205', '2016-09-23 13:43:53 工作内容', '2016-09-23 13:43:53', '1cdb6e27-0051-0ce3-7324-136ddad05798', null, '2016-10-07', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1206', '2016-09-23 13:44:57 工作内容', '2016-09-23 13:44:57', '1cdb6e27-0051-0ce3-7324-136ddad05798', null, '2016-09-27', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1207', '2016-09-23 13:52:35 工作内容', '2016-09-23 13:52:35', '192adb08-d801-9ae3-b1a8-224d63269915', null, '2016-09-22', '2016-09-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1208', '2016-09-23 14:04:22 工作内容', '2016-09-23 14:04:22', '60082cca-c7bb-60c9-ff2b-658551a5aa94', null, '2016-09-25', '2016-09-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1209', '2016-09-23 14:04:56 工作内容', '2016-09-23 14:04:56', '60082cca-c7bb-60c9-ff2b-658551a5aa94', null, '2016-09-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1210', '2016-09-23 14:05:44 工作内容', '2016-09-23 14:05:44', '60082cca-c7bb-60c9-ff2b-658551a5aa94', null, '2016-09-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1211', '2016-09-23 14:08:18 工作内容', '2016-09-23 14:08:18', '893ab7f5-f480-9c30-402d-d51c0bc31228', null, '2016-09-23', '2016-09-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1212', '2016-09-23 14:09:04 工作内容', '2016-09-23 14:09:04', '893ab7f5-f480-9c30-402d-d51c0bc31228', null, '2016-09-22', '2016-09-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1213', '2016-09-23 14:09:43 工作内容', '2016-09-23 14:09:43', '893ab7f5-f480-9c30-402d-d51c0bc31228', null, '2016-09-29', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1214', '2016-09-30 15:05:43 工作内容', '2016-09-30 15:05:43', '91b261d4-c1b5-f751-751c-36f83b36a9b8', null, '2016-09-28', '2016-09-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1215', '2016-09-30 15:06:21 工作内容', '2016-09-30 15:06:21', '91b261d4-c1b5-f751-751c-36f83b36a9b8', null, '2016-09-29', '2016-09-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1216', '2016-09-30 15:06:40 工作内容', '2016-09-30 15:06:40', '0a20be13-d81d-872d-2252-019d693fe2ae', null, '2016-09-26', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1217', '2016-09-30 15:06:57 工作内容', '2016-09-30 15:06:57', '699a3e29-68ab-9e2f-17cb-06be1dd0da08', null, '2016-09-30', '2016-09-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1218', '2016-09-30 15:06:57 工作内容', '2016-09-30 15:06:57', '91b261d4-c1b5-f751-751c-36f83b36a9b8', null, '2016-09-29', '2016-09-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1219', '2016-09-30 15:07:01 工作内容', '2016-09-30 15:07:01', '4b897d83-0b23-56da-7579-5dc9bb7fb237', null, '2016-09-28', '2016-09-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1220', '2016-09-30 15:07:33 工作内容', '2016-09-30 15:07:33', '91b261d4-c1b5-f751-751c-36f83b36a9b8', null, '2016-09-29', '2016-09-29', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1221', '2016-09-30 15:07:40 工作内容', '2016-09-30 15:07:40', '0a20be13-d81d-872d-2252-019d693fe2ae', null, '2016-09-27', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1222', '2016-09-30 15:07:55 工作内容', '2016-09-30 15:07:55', '699a3e29-68ab-9e2f-17cb-06be1dd0da08', null, '2016-09-30', '2016-09-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1223', '2016-09-30 15:08:18 工作内容', '2016-09-30 15:08:18', '91b261d4-c1b5-f751-751c-36f83b36a9b8', null, '2016-09-27', '2016-09-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1224', '2016-09-30 15:08:35 工作内容', '2016-09-30 15:08:35', '0a20be13-d81d-872d-2252-019d693fe2ae', null, '2016-09-28', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1225', '2016-09-30 15:08:44 工作内容', '2016-09-30 15:08:44', '4b897d83-0b23-56da-7579-5dc9bb7fb237', null, '2016-09-30', '2016-09-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1226', '2016-09-30 15:09:02 工作内容', '2016-09-30 15:09:02', '91b261d4-c1b5-f751-751c-36f83b36a9b8', null, '2016-10-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1227', '2016-09-30 15:09:02 工作内容', '2016-09-30 15:09:02', '0a20be13-d81d-872d-2252-019d693fe2ae', null, '2016-09-29', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1228', '2016-09-30 15:09:10 工作内容', '2016-09-30 15:09:10', 'f1e9c6c3-b436-240c-d607-6c54a5db1dce', null, '2016-09-27', '2016-09-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1229', '2016-09-30 15:09:27 工作内容', '2016-09-30 15:09:27', '699a3e29-68ab-9e2f-17cb-06be1dd0da08', null, '2016-09-30', '2016-09-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1230', '2016-09-30 15:09:37 工作内容', '2016-09-30 15:09:37', '91b261d4-c1b5-f751-751c-36f83b36a9b8', null, '2016-10-14', '2016-10-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1231', '2016-09-30 15:09:40 工作内容', '2016-09-30 15:09:40', '0a20be13-d81d-872d-2252-019d693fe2ae', null, '2016-09-30', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1232', '2016-09-30 15:10:24 工作内容', '2016-09-30 15:10:24', '0a20be13-d81d-872d-2252-019d693fe2ae', null, '2016-09-30', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1233', '2016-09-30 15:10:33 工作内容', '2016-09-30 15:10:33', '4b897d83-0b23-56da-7579-5dc9bb7fb237', null, '2016-09-30', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1234', '2016-09-30 15:10:59 工作内容', '2016-09-30 15:10:59', '699a3e29-68ab-9e2f-17cb-06be1dd0da08', null, '2016-10-14', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1235', '2016-09-30 15:12:17 工作内容', '2016-09-30 15:12:17', '4b897d83-0b23-56da-7579-5dc9bb7fb237', null, '2016-10-14', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1236', '2016-09-30 15:12:40 工作内容', '2016-09-30 15:12:40', '0a20be13-d81d-872d-2252-019d693fe2ae', null, '2016-10-14', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1237', '2016-09-30 15:13:09 工作内容', '2016-09-30 15:13:09', '5198069a-7b30-fae0-2eb6-7d75772299a1', null, '2016-09-30', '2016-09-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1238', '2016-09-30 15:13:25 工作内容', '2016-09-30 15:13:25', 'f1e9c6c3-b436-240c-d607-6c54a5db1dce', null, '2016-09-30', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1239', '2016-09-30 15:13:37 工作内容', '2016-09-30 15:13:37', '5198069a-7b30-fae0-2eb6-7d75772299a1', null, '2016-09-30', '2016-09-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1240', '2016-09-30 15:15:09 工作内容', '2016-09-30 15:15:09', '5198069a-7b30-fae0-2eb6-7d75772299a1', null, '2016-10-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1241', '2016-09-30 15:16:19 工作内容', '2016-09-30 15:16:19', '5198069a-7b30-fae0-2eb6-7d75772299a1', null, '2016-10-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1242', '2016-09-30 15:17:00 工作内容', '2016-09-30 15:17:00', 'f1e9c6c3-b436-240c-d607-6c54a5db1dce', null, '2016-09-30', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1244', '2016-09-30 15:19:44 工作内容', '2016-09-30 15:19:44', '5198069a-7b30-fae0-2eb6-7d75772299a1', null, '2016-10-07', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1245', '2016-09-30 15:20:37 工作内容', '2016-09-30 15:20:37', 'f1e9c6c3-b436-240c-d607-6c54a5db1dce', null, '2016-10-09', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1246', '2016-09-30 15:20:38 工作内容', '2016-09-30 15:20:38', 'ed874ad4-f4bf-27e1-35bc-2cf6334c7a10', null, '2016-09-27', '2016-09-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1247', '2016-09-30 15:21:23 工作内容', '2016-09-30 15:21:23', 'ed874ad4-f4bf-27e1-35bc-2cf6334c7a10', null, '2016-09-30', '2016-09-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1248', '2016-09-30 15:21:51 工作内容', '2016-09-30 15:21:51', 'f1e9c6c3-b436-240c-d607-6c54a5db1dce', null, '2016-10-12', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1249', '2016-09-30 15:22:03 工作内容', '2016-09-30 15:22:03', '4b897d83-0b23-56da-7579-5dc9bb7fb237', null, '2016-10-08', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1250', '2016-09-30 15:22:07 工作内容', '2016-09-30 15:22:07', 'ed874ad4-f4bf-27e1-35bc-2cf6334c7a10', null, '2016-09-30', '2016-09-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1251', '2016-09-30 15:22:42 工作内容', '2016-09-30 15:22:42', 'f1e9c6c3-b436-240c-d607-6c54a5db1dce', null, '2016-10-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1252', '2016-09-30 15:22:59 工作内容', '2016-09-30 15:22:59', 'ed874ad4-f4bf-27e1-35bc-2cf6334c7a10', null, '2016-10-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1253', '2016-09-30 15:24:06 工作内容', '2016-09-30 15:24:06', 'ed874ad4-f4bf-27e1-35bc-2cf6334c7a10', null, '2016-09-30', '2016-09-30', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1254', '2016-09-30 15:24:36 工作内容', '2016-09-30 15:24:36', 'ed874ad4-f4bf-27e1-35bc-2cf6334c7a10', null, '2016-10-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1255', '2016-09-30 15:41:50 工作内容', '2016-09-30 15:41:50', '4b897d83-0b23-56da-7579-5dc9bb7fb237', null, '2016-10-14', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1256', '2016-10-14 13:20:40 工作内容', '2016-10-14 13:20:40', 'a2d8888a-4b61-9af7-fa9d-5006f306df63', null, '2016-10-09', '2016-10-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1257', '2016-10-14 13:22:23 工作内容', '2016-10-14 13:22:23', 'a2d8888a-4b61-9af7-fa9d-5006f306df63', null, '2016-10-10', '2016-10-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1258', '2016-10-14 13:23:04 工作内容', '2016-10-14 13:23:04', 'a2d8888a-4b61-9af7-fa9d-5006f306df63', null, '2016-10-10', '2016-10-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1259', '2016-10-14 13:25:20 工作内容', '2016-10-14 13:25:20', 'a2d8888a-4b61-9af7-fa9d-5006f306df63', null, '2016-10-13', '2016-10-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1261', '2016-10-14 13:30:01 工作内容', '2016-10-14 13:30:01', 'a30a2bc3-9572-b23d-bbb8-5e14af6f0cc6', null, '2016-10-14', '2016-10-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1262', '2016-10-14 13:31:12 工作内容', '2016-10-14 13:31:12', 'a30a2bc3-9572-b23d-bbb8-5e14af6f0cc6', null, '2016-10-14', '2016-10-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1263', '2016-10-14 13:32:34 工作内容', '2016-10-14 13:32:34', '7e8d912b-9caf-dced-4c58-32f8e3f54f54', null, '2016-10-11', '2016-10-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1264', '2016-10-14 13:33:20 工作内容', '2016-10-14 13:33:20', '9057493b-51d9-d1fa-91bc-f9188cff8c4d', null, '2016-10-10', '2016-10-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1265', '2016-10-14 13:34:09 工作内容', '2016-10-14 13:34:09', '9057493b-51d9-d1fa-91bc-f9188cff8c4d', null, '2016-10-18', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1266', '2016-10-14 13:34:50 工作内容', '2016-10-14 13:34:50', '7e8d912b-9caf-dced-4c58-32f8e3f54f54', null, '2016-10-14', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1267', '2016-10-14 13:35:04 工作内容', '2016-10-14 13:35:04', '9057493b-51d9-d1fa-91bc-f9188cff8c4d', null, '2016-10-14', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1268', '2016-10-14 13:36:05 工作内容', '2016-10-14 13:36:05', 'a30a2bc3-9572-b23d-bbb8-5e14af6f0cc6', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1269', '2016-10-14 13:36:27 工作内容', '2016-10-14 13:36:27', '7e8d912b-9caf-dced-4c58-32f8e3f54f54', null, '2016-10-14', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1270', '2016-10-14 13:37:56 工作内容', '2016-10-14 13:37:56', 'a30a2bc3-9572-b23d-bbb8-5e14af6f0cc6', null, '2016-10-18', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1271', '2016-10-14 13:38:25 工作内容', '2016-10-14 13:38:25', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-10', '2016-10-10', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1272', '2016-10-14 13:38:59 工作内容', '2016-10-14 13:38:59', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-12', '2016-10-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1273', '2016-10-14 13:39:27 工作内容', '2016-10-14 13:39:27', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-13', '2016-10-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1274', '2016-10-14 13:40:02 工作内容', '2016-10-14 13:40:02', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-14', '2016-10-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1275', '2016-10-14 13:40:43 工作内容', '2016-10-14 13:40:43', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-14', '2016-10-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1276', '2016-10-14 13:41:28 工作内容', '2016-10-14 13:41:28', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-14', '2016-10-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1277', '2016-10-14 13:42:22 工作内容', '2016-10-14 13:42:22', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-14', '2016-10-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1278', '2016-10-14 13:42:49 工作内容', '2016-10-14 13:42:49', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1279', '2016-10-14 13:43:28 工作内容', '2016-10-14 13:43:28', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1280', '2016-10-14 13:45:10 工作内容', '2016-10-14 13:45:10', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1281', '2016-10-14 13:59:32 工作内容', '2016-10-14 13:59:32', '9057493b-51d9-d1fa-91bc-f9188cff8c4d', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1282', '2016-10-14 14:06:24 工作内容', '2016-10-14 14:06:24', '9057493b-51d9-d1fa-91bc-f9188cff8c4d', null, '2016-10-12', '2016-10-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1283', '2016-10-14 14:07:58 工作内容', '2016-10-14 14:07:58', '15b958e4-42af-21d1-d926-22c4f705ff1d', null, '2016-10-12', '2016-10-12', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1284', '2016-10-14 14:09:34 工作内容', '2016-10-14 14:09:34', '15b958e4-42af-21d1-d926-22c4f705ff1d', null, '2016-10-13', '2016-10-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1285', '2016-10-14 14:10:29 工作内容', '2016-10-14 14:10:29', '15b958e4-42af-21d1-d926-22c4f705ff1d', null, '2016-10-14', '2016-10-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1286', '2016-10-14 14:12:08 工作内容', '2016-10-14 14:12:08', '15b958e4-42af-21d1-d926-22c4f705ff1d', null, '2016-10-21', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1287', '2016-10-14 14:13:51 工作内容', '2016-10-14 14:13:51', '15b958e4-42af-21d1-d926-22c4f705ff1d', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1289', '2016-10-14 16:06:45 工作内容', '2016-10-14 16:06:45', 'efc13656-203d-7436-f336-6613f67a3577', null, '2016-10-09', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1290', '2016-10-14 16:07:12 工作内容', '2016-10-14 16:07:12', 'efc13656-203d-7436-f336-6613f67a3577', null, '2016-10-17', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1291', '2016-10-14 16:07:34 工作内容', '2016-10-14 16:07:34', 'efc13656-203d-7436-f336-6613f67a3577', null, '2016-10-11', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1292', '2016-10-14 16:08:06 工作内容', '2016-10-14 16:08:06', 'efc13656-203d-7436-f336-6613f67a3577', null, '2016-10-12', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1293', '2016-10-14 16:08:24 工作内容', '2016-10-14 16:08:24', 'efc13656-203d-7436-f336-6613f67a3577', null, '2016-10-13', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1294', '2016-10-14 16:08:39 工作内容', '2016-10-14 16:08:39', 'efc13656-203d-7436-f336-6613f67a3577', null, '2016-10-14', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1295', '2016-10-14 16:09:15 工作内容', '2016-10-14 16:09:15', 'efc13656-203d-7436-f336-6613f67a3577', null, '2016-10-14', '', '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1296', '2016-10-14 16:10:01 工作内容', '2016-10-14 16:10:01', 'efc13656-203d-7436-f336-6613f67a3577', null, '2016-10-21', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1297', '2016-10-14 16:11:51 工作内容', '2016-10-14 16:11:51', '62467bcb-a7d5-5e49-a426-171abe5b019f', null, '2016-10-14', '2016-10-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1298', '2016-10-14 16:12:59 工作内容', '2016-10-14 16:12:59', '62467bcb-a7d5-5e49-a426-171abe5b019f', null, '2016-10-12', '2016-10-13', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1299', '2016-10-14 16:21:33 工作内容', '2016-10-14 16:21:33', '62467bcb-a7d5-5e49-a426-171abe5b019f', null, '2016-10-14', '未完成', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1300', '2016-10-14 16:22:07 工作内容', '2016-10-14 16:22:07', '62467bcb-a7d5-5e49-a426-171abe5b019f', null, '2016-10-14', '2016-10-14', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1302', '2016-10-14 16:23:25 工作内容', '2016-10-14 16:23:25', '62467bcb-a7d5-5e49-a426-171abe5b019f', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1303', '2016-10-14 16:25:51 工作内容', '2016-10-14 16:25:51', '62467bcb-a7d5-5e49-a426-171abe5b019f', null, '2016-10-19', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1304', '2016-10-14 16:49:10 工作内容', '2016-10-14 16:49:10', '7e8d912b-9caf-dced-4c58-32f8e3f54f54', null, '2016-10-20', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1305', '2016-10-14 09:12:37 工作内容', '2016-10-14 09:12:37', '9057493b-51d9-d1fa-91bc-f9188cff8c4d', null, '2016-10-20', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1306', '2016-10-14 09:13:46 工作内容', '2016-10-14 09:13:46', 'a30a2bc3-9572-b23d-bbb8-5e14af6f0cc6', null, '2016-10-14', '2016-10-11', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1307', '2016-10-14 09:14:33 工作内容', '2016-10-14 09:14:33', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1308', '2016-10-14 09:14:54 工作内容', '2016-10-14 09:14:54', '8989e27f-fc57-3c6c-a827-983ce013ac1f', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1309', '2016-10-14 09:14:59 工作内容', '2016-10-14 09:14:59', 'a30a2bc3-9572-b23d-bbb8-5e14af6f0cc6', null, '2016-10-20', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1310', '2016-10-14 09:18:43 工作内容', '2016-10-14 09:18:43', '15b958e4-42af-21d1-d926-22c4f705ff1d', null, '2016-10-21', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1311', '2016-10-14 10:43:14 工作内容', '2016-10-14 10:43:14', '7e8d912b-9caf-dced-4c58-32f8e3f54f54', null, '2016-10-21', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1313', '2016-10-14 10:54:44 工作内容', '2016-10-14 10:54:44', 'a2d8888a-4b61-9af7-fa9d-5006f306df63', null, '2016-10-18', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1314', '2016-10-14 10:55:05 工作内容', '2016-10-14 10:55:05', 'a2d8888a-4b61-9af7-fa9d-5006f306df63', null, '2016-10-20', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1315', '2016-10-21 14:28:04 工作内容', '2016-10-21 14:28:04', 'f3ac95b4-59c8-91a6-3bc9-3d5227531a79', null, '2016-10-18', '2016-10-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1316', '2016-10-21 14:28:34 工作内容', '2016-10-21 14:28:34', 'f3ac95b4-59c8-91a6-3bc9-3d5227531a79', null, '2016-10-20', '2016-10-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1317', '2016-10-21 14:28:41 工作内容', '2016-10-21 14:28:41', '76d46e6b-1436-07ed-17ee-479c1e8e12cd', null, '2016-10-18', '2016-10-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1318', '2016-10-21 14:28:52 工作内容', '2016-10-21 14:28:52', '76d46e6b-1436-07ed-17ee-479c1e8e12cd', null, '2016-10-20', '2016-10-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1319', '2016-10-21 14:29:27 工作内容', '2016-10-21 14:29:27', 'beb2ea75-8c54-5fc1-cf1a-7284a7b73355', null, '2016-10-18', '2016-10-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1320', '2016-10-21 14:29:33 工作内容', '2016-10-21 14:29:33', 'f3ac95b4-59c8-91a6-3bc9-3d5227531a79', null, '2016-10-26', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1321', '2016-10-21 14:29:35 工作内容', '2016-10-21 14:29:35', '76d46e6b-1436-07ed-17ee-479c1e8e12cd', null, '2016-10-25', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1322', '2016-10-21 14:30:12 工作内容', '2016-10-21 14:30:12', 'f3ac95b4-59c8-91a6-3bc9-3d5227531a79', null, '2016-10-27', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1323', '2016-10-21 14:30:29 工作内容', '2016-10-21 14:30:29', 'beb2ea75-8c54-5fc1-cf1a-7284a7b73355', null, '2016-10-21', '2016-10-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1324', '2016-10-21 14:30:35 工作内容', '2016-10-21 14:30:35', '76d46e6b-1436-07ed-17ee-479c1e8e12cd', null, '2016-10-26', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1325', '2016-10-21 14:30:48 工作内容', '2016-10-21 14:30:48', '27033497-8d10-6e49-cfc7-0eba7c0c92c1', null, '2016-10-17', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1326', '2016-10-21 14:31:07 工作内容', '2016-10-21 14:31:07', '27033497-8d10-6e49-cfc7-0eba7c0c92c1', null, '2016-10-18', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1327', '2016-10-21 14:31:22 工作内容', '2016-10-21 14:31:22', 'beb2ea75-8c54-5fc1-cf1a-7284a7b73355', null, '2016-10-21', '2016-10-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1328', '2016-10-21 14:31:23 工作内容', '2016-10-21 14:31:23', '76d46e6b-1436-07ed-17ee-479c1e8e12cd', null, '2016-10-28', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1329', '2016-10-21 14:31:39 工作内容', '2016-10-21 14:31:39', '27033497-8d10-6e49-cfc7-0eba7c0c92c1', null, '2016-10-19', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1330', '2016-10-21 14:32:00 工作内容', '2016-10-21 14:32:00', '27033497-8d10-6e49-cfc7-0eba7c0c92c1', null, '2016-10-20', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1331', '2016-10-21 14:32:18 工作内容', '2016-10-21 14:32:18', '27033497-8d10-6e49-cfc7-0eba7c0c92c1', null, '2016-10-21', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1332', '2016-10-21 14:32:24 工作内容', '2016-10-21 14:32:24', 'beb2ea75-8c54-5fc1-cf1a-7284a7b73355', null, '2016-10-28', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1333', '2016-10-21 14:32:44 工作内容', '2016-10-21 14:32:44', '27033497-8d10-6e49-cfc7-0eba7c0c92c1', null, '2016-10-21', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1334', '2016-10-21 14:33:53 工作内容', '2016-10-21 14:33:53', '27033497-8d10-6e49-cfc7-0eba7c0c92c1', null, '2016-10-28', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1335', '2016-10-21 14:48:05 工作内容', '2016-10-21 14:48:05', 'a37e4848-bdc5-52f6-b407-9fd2996879cb', null, '2016-10-20', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1336', '2016-10-21 14:49:10 工作内容', '2016-10-21 14:49:10', 'a37e4848-bdc5-52f6-b407-9fd2996879cb', null, '2016-10-21', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1337', '2016-10-21 14:49:59 工作内容', '2016-10-21 14:49:59', 'c50fc16f-5cfa-8327-8769-90f1ce67dd8f', null, '2016-10-19', '2016-10-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1338', '2016-10-21 14:51:16 工作内容', '2016-10-21 14:51:16', 'c50fc16f-5cfa-8327-8769-90f1ce67dd8f', null, '2016-10-14', '2016-10-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1339', '2016-10-21 14:51:21 工作内容', '2016-10-21 14:51:21', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-17', '2016-10-17', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1340', '2016-10-21 14:51:53 工作内容', '2016-10-21 14:51:53', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-18', '2016-10-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1341', '2016-10-21 14:52:01 工作内容', '2016-10-21 14:52:01', 'c50fc16f-5cfa-8327-8769-90f1ce67dd8f', null, '2016-10-21', '2016-10-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1342', '2016-10-21 14:52:32 工作内容', '2016-10-21 14:52:32', 'c50fc16f-5cfa-8327-8769-90f1ce67dd8f', null, '2016-10-28', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1343', '2016-10-21 14:52:39 工作内容', '2016-10-21 14:52:39', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-19', '2016-10-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1344', '2016-10-21 14:53:43 工作内容', '2016-10-21 14:53:43', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-21', '2016-10-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1345', '2016-10-21 14:54:10 工作内容', '2016-10-21 14:54:10', 'a37e4848-bdc5-52f6-b407-9fd2996879cb', null, '2016-10-19', '2016-10-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1346', '2016-10-21 14:54:34 工作内容', '2016-10-21 14:54:34', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-21', '2016-10-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1347', '2016-10-21 14:55:07 工作内容', '2016-10-21 14:55:07', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-20', '2016-10-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1348', '2016-10-21 14:55:38 工作内容', '2016-10-21 14:55:38', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-25', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1349', '2016-10-21 14:55:56 工作内容', '2016-10-21 14:55:56', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-25', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1350', '2016-10-21 14:56:05 工作内容', '2016-10-21 14:56:05', 'a37e4848-bdc5-52f6-b407-9fd2996879cb', null, '2016-10-20', '2016-10-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1351', '2016-10-21 14:56:32 工作内容', '2016-10-21 14:56:32', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-28', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1352', '2016-10-21 14:57:35 工作内容', '2016-10-21 14:57:35', '9e1616db-9eb8-3076-3bbf-36088906489c', null, '2016-10-28', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1353', '2016-10-21 14:58:47 工作内容', '2016-10-21 14:58:47', 'a37e4848-bdc5-52f6-b407-9fd2996879cb', null, '2016-10-21', '2016-10-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1354', '2016-10-21 15:01:23 工作内容', '2016-10-21 15:01:23', 'a37e4848-bdc5-52f6-b407-9fd2996879cb', null, '2016-10-26', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1355', '2016-10-21 15:02:35 工作内容', '2016-10-21 15:02:35', 'a37e4848-bdc5-52f6-b407-9fd2996879cb', null, '2016-10-28', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1356', '2016-10-21 15:07:29 工作内容', '2016-10-21 15:07:29', '25a2f58a-6e78-b429-6586-3b7f620805f6', null, '2016-10-18', '2016-10-18', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1357', '2016-10-21 15:08:04 工作内容', '2016-10-21 15:08:04', '25a2f58a-6e78-b429-6586-3b7f620805f6', null, '2016-10-19', '2016-10-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1358', '2016-10-21 15:08:51 工作内容', '2016-10-21 15:08:51', '25a2f58a-6e78-b429-6586-3b7f620805f6', null, '2016-10-21', '2016-10-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1359', '2016-10-21 15:14:53 工作内容', '2016-10-21 15:14:53', '25a2f58a-6e78-b429-6586-3b7f620805f6', null, '2016-10-28', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1360', '2016-10-21 15:16:11 工作内容', '2016-10-21 15:16:11', '25a2f58a-6e78-b429-6586-3b7f620805f6', null, '2016-10-28', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1361', '2016-10-21 16:14:56 工作内容', '2016-10-21 16:14:56', 'beb2ea75-8c54-5fc1-cf1a-7284a7b73355', null, '2016-10-28', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1362', '2016-10-28 14:36:19 工作内容', '2016-10-28 14:36:19', '086e208c-30eb-215e-f96e-446d139a8828', null, '2016-10-24', '2016-10-24', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1363', '2016-10-28 14:36:50 工作内容', '2016-10-28 14:36:50', '086e208c-30eb-215e-f96e-446d139a8828', null, '2016-10-26', '2016-10-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1364', '2016-10-28 14:37:39 工作内容', '2016-10-28 14:37:39', '086e208c-30eb-215e-f96e-446d139a8828', null, '2016-10-26', '2016-10-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1365', '2016-10-28 14:38:29 工作内容', '2016-10-28 14:38:29', '086e208c-30eb-215e-f96e-446d139a8828', null, '2016-10-26', '2016-10-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1366', '2016-10-28 14:38:40 工作内容', '2016-10-28 14:38:40', '7c3ee76a-74cc-6d90-94ee-065bafcb1864', null, '2016-10-26', '2016-10-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1367', '2016-10-28 14:39:01 工作内容', '2016-10-28 14:39:01', '086e208c-30eb-215e-f96e-446d139a8828', null, '2016-10-26', '2016-10-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1368', '2016-10-28 14:39:25 工作内容', '2016-10-28 14:39:25', '7c3ee76a-74cc-6d90-94ee-065bafcb1864', null, '2016-10-27', '2016-10-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1369', '2016-10-28 14:39:34 工作内容', '2016-10-28 14:39:34', 'c55da744-d21e-7be1-effe-4f5b47cfcd0e', null, '2016-10-24', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1370', '2016-10-28 14:40:10 工作内容', '2016-10-28 14:40:10', '086e208c-30eb-215e-f96e-446d139a8828', null, '2016-11-04', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1371', '2016-10-28 14:40:30 工作内容', '2016-10-28 14:40:30', 'c55da744-d21e-7be1-effe-4f5b47cfcd0e', null, '2016-10-25', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1372', '2016-10-28 14:40:44 工作内容', '2016-10-28 14:40:44', '086e208c-30eb-215e-f96e-446d139a8828', null, '2016-11-04', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1373', '2016-10-28 14:40:51 工作内容', '2016-10-28 14:40:51', 'c55da744-d21e-7be1-effe-4f5b47cfcd0e', null, '2016-10-26', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1374', '2016-10-28 14:40:56 工作内容', '2016-10-28 14:40:56', '7c3ee76a-74cc-6d90-94ee-065bafcb1864', null, '2016-10-28', '2016-10-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1375', '2016-10-28 14:41:34 工作内容', '2016-10-28 14:41:34', 'c55da744-d21e-7be1-effe-4f5b47cfcd0e', null, '2016-10-28', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1376', '2016-10-28 14:41:54 工作内容', '2016-10-28 14:41:54', 'c55da744-d21e-7be1-effe-4f5b47cfcd0e', null, '2016-10-28', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1377', '2016-10-28 14:42:30 工作内容', '2016-10-28 14:42:30', 'c55da744-d21e-7be1-effe-4f5b47cfcd0e', null, '2016-11-04', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1378', '2016-10-28 14:45:00 工作内容', '2016-10-28 14:45:00', '73701326-6b7e-9594-dae2-09c5bb69ad8a', null, '2016-10-25', '2016-10-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1379', '2016-10-28 14:45:56 工作内容', '2016-10-28 14:45:56', '73701326-6b7e-9594-dae2-09c5bb69ad8a', null, '2016-10-25', '2016-10-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1380', '2016-10-28 14:46:16 工作内容', '2016-10-28 14:46:16', '73701326-6b7e-9594-dae2-09c5bb69ad8a', null, '2016-10-28', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1381', '2016-10-28 14:46:36 工作内容', '2016-10-28 14:46:36', '73701326-6b7e-9594-dae2-09c5bb69ad8a', null, '2016-11-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1382', '2016-10-28 14:47:31 工作内容', '2016-10-28 14:47:31', 'b994a991-d268-5111-3d93-e7681c62fe51', null, '2016-10-24', '2016-10-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1383', '2016-10-28 14:47:31 工作内容', '2016-10-28 14:47:31', '73701326-6b7e-9594-dae2-09c5bb69ad8a', null, '2016-11-02', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1384', '2016-10-28 14:48:09 工作内容', '2016-10-28 14:48:09', '36673390-0daa-2d87-cab3-2fcec10c616f', null, '2016-10-25', '2016-10-25', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1385', '2016-10-28 14:48:34 工作内容', '2016-10-28 14:48:34', '36673390-0daa-2d87-cab3-2fcec10c616f', null, '2016-10-28', '2016-10-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1386', '2016-10-28 14:48:58 工作内容', '2016-10-28 14:48:58', '36673390-0daa-2d87-cab3-2fcec10c616f', null, '2016-11-04', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1387', '2016-10-28 14:49:48 工作内容', '2016-10-28 14:49:48', '36673390-0daa-2d87-cab3-2fcec10c616f', null, '2016-11-04', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1388', '2016-10-28 14:52:39 工作内容', '2016-10-28 14:52:39', '438c7440-e20a-4145-16ca-5ac13ef29b9a', null, '2016-10-24', '2016-10-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1389', '2016-10-28 14:53:33 工作内容', '2016-10-28 14:53:33', '438c7440-e20a-4145-16ca-5ac13ef29b9a', null, '2016-10-26', '2016-10-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1390', '2016-10-28 15:03:58 工作内容', '2016-10-28 15:03:58', '76307f6f-fcc5-c642-00fb-e8d6515ccfac', null, '2016-10-28', '2016-10-27', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1391', '2016-10-28 15:05:41 工作内容', '2016-10-28 15:05:41', '76307f6f-fcc5-c642-00fb-e8d6515ccfac', null, '2016-10-28', '2016-10-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1392', '2016-10-28 15:06:53 工作内容', '2016-10-28 15:06:53', '76307f6f-fcc5-c642-00fb-e8d6515ccfac', null, '2016-10-28', '2016-10-26', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1393', '2016-10-28 15:07:21 工作内容', '2016-10-28 15:07:21', '76307f6f-fcc5-c642-00fb-e8d6515ccfac', null, '2016-10-28', '2016-10-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1394', '2016-10-28 15:11:41 工作内容', '2016-10-28 15:11:41', '76307f6f-fcc5-c642-00fb-e8d6515ccfac', null, '2016-11-04', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1395', '2016-10-28 15:12:13 工作内容', '2016-10-28 15:12:13', '76307f6f-fcc5-c642-00fb-e8d6515ccfac', null, '2016-11-04', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1396', '2016-10-28 15:14:18 工作内容', '2016-10-28 15:14:18', '36673390-0daa-2d87-cab3-2fcec10c616f', null, '2016-10-28', '2016-10-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1397', '2016-10-28 17:14:54 工作内容', '2016-10-28 17:14:54', '438c7440-e20a-4145-16ca-5ac13ef29b9a', null, '2016-10-31', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1398', '2016-10-28 17:16:06 工作内容', '2016-10-28 17:16:06', '438c7440-e20a-4145-16ca-5ac13ef29b9a', null, '2016-11-03', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1399', '2016-10-28 17:17:17 工作内容', '2016-10-28 17:17:17', 'f11ae4eb-e2c4-fc7d-4465-a0edc48a191c', null, '2016-10-28', '2016-10-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1400', '2016-10-28 17:17:44 工作内容', '2016-10-28 17:17:44', 'f11ae4eb-e2c4-fc7d-4465-a0edc48a191c', null, '2016-10-28', '2016-10-28', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1401', '2016-10-28 17:17:59 工作内容', '2016-10-28 17:17:59', 'f11ae4eb-e2c4-fc7d-4465-a0edc48a191c', null, '2016-11-04', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1402', '2016-10-28 17:22:40 工作内容', '2016-10-28 17:22:40', '7c3ee76a-74cc-6d90-94ee-065bafcb1864', null, '2016-11-01', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1403', '2016-10-28 17:23:17 工作内容', '2016-10-28 17:23:17', '7c3ee76a-74cc-6d90-94ee-065bafcb1864', null, '2016-11-04', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1404', '2016-11-04 16:07:24 工作内容', '2016-11-04 16:07:24', 'c37d0abf-46e4-4406-e15e-65b6b3d2932e', null, '2016-10-31', '2016-10-31', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1405', '2016-11-04 16:09:10 工作内容', '2016-11-04 16:09:10', 'c37d0abf-46e4-4406-e15e-65b6b3d2932e', null, '2016-11-03', '2016-11-03', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1406', '2016-11-04 16:10:11 工作内容', '2016-11-04 16:10:11', 'c37d0abf-46e4-4406-e15e-65b6b3d2932e', null, '2016-11-02', '2016-11-02', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1407', '2016-11-04 16:10:44 工作内容', '2016-11-04 16:10:44', 'c37d0abf-46e4-4406-e15e-65b6b3d2932e', null, '2016-11-04', '2016-11-04', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1408', '2016-11-04 16:11:47 工作内容', '2016-11-04 16:11:47', 'c37d0abf-46e4-4406-e15e-65b6b3d2932e', null, '2016-11-11', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1409', '2016-11-04 16:12:24 工作内容', '2016-11-04 16:12:24', 'c37d0abf-46e4-4406-e15e-65b6b3d2932e', null, '2016-11-11', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1410', '2016-12-09 17:05:06 工作内容', '2016-12-09 17:05:06', 'e234e786-dad4-cfa2-97b4-a67c57f87054', null, '2016-12-07', '2016-12-07', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1411', '2016-12-09 17:06:02 工作内容', '2016-12-09 17:06:02', 'e234e786-dad4-cfa2-97b4-a67c57f87054', null, '2016-12-08', '2016-12-08', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1412', '2016-12-09 17:07:36 工作内容', '2016-12-09 17:07:36', 'e234e786-dad4-cfa2-97b4-a67c57f87054', null, '2016-12-09', '2016-12-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1413', '2016-12-09 17:11:01 工作内容', '2016-12-09 17:11:01', 'e234e786-dad4-cfa2-97b4-a67c57f87054', null, '2016-12-09', '2016-12-09', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1414', '2016-12-12 14:50:37 工作内容', '2016-12-12 14:50:37', '2f8a2e1d-7253-8e3d-2021-8287e88e05ba', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1415', '2016-12-12 14:51:29 工作内容', '2016-12-12 14:51:29', '2f8a2e1d-7253-8e3d-2021-8287e88e05ba', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1416', '2016-12-12 14:52:20 工作内容', '2016-12-12 14:52:20', '2f8a2e1d-7253-8e3d-2021-8287e88e05ba', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1417', '2016-12-12 16:29:50 工作内容', '2016-12-12 16:29:50', '2f8a2e1d-7253-8e3d-2021-8287e88e05ba', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1418', '2016-12-13 15:32:31 工作内容', '2016-12-13 15:32:31', '082a2af5-3413-8014-2277-b141e11831b8', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1419', '2016-12-13 15:33:32 工作内容', '2016-12-13 15:33:32', '082a2af5-3413-8014-2277-b141e11831b8', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1420', '2016-12-14 17:11:19 工作内容', '2016-12-14 17:11:19', 'e42d20bd-c5ba-2462-f41a-a9dab3ca0cf6', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1421', '2016-12-14 17:12:01 工作内容', '2016-12-14 17:12:01', 'e42d20bd-c5ba-2462-f41a-a9dab3ca0cf6', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1422', '2016-12-14 17:14:06 工作内容', '2016-12-14 17:14:06', 'e42d20bd-c5ba-2462-f41a-a9dab3ca0cf6', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1423', '2016-12-14 17:22:27 工作内容', '2016-12-14 17:22:27', 'e42d20bd-c5ba-2462-f41a-a9dab3ca0cf6', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1424', '2016-12-16 15:37:17 工作内容', '2016-12-16 15:37:17', '58de2dbf-e18c-794a-531f-16fbcc70a380', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1425', '2016-12-16 15:38:11 工作内容', '2016-12-16 15:38:11', '58de2dbf-e18c-794a-531f-16fbcc70a380', '3', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1426', '2016-12-19 13:58:08 工作内容', '2016-12-19 13:58:08', '63ced581-d3fd-6383-c1c4-ee9851f57a4b', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1427', '2016-12-19 13:58:42 工作内容', '2016-12-19 13:58:42', '63ced581-d3fd-6383-c1c4-ee9851f57a4b', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1428', '2016-12-23 13:07:21 工作内容', '2016-12-23 13:07:21', 'd580f45e-8ff0-3a55-05a9-259b73d40519', null, '2016-12-19', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1429', '2016-12-23 13:07:37 工作内容', '2016-12-23 13:07:37', 'd580f45e-8ff0-3a55-05a9-259b73d40519', null, '2016-12-20', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1430', '2016-12-23 13:07:51 工作内容', '2016-12-23 13:07:51', 'd580f45e-8ff0-3a55-05a9-259b73d40519', null, '2016-12-21', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1431', '2016-12-23 13:08:06 工作内容', '2016-12-23 13:08:06', '776e8283-44b0-c400-3508-1e3a8f7f4071', null, '2016-12-19', '2016-12-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1432', '2016-12-23 13:08:27 工作内容', '2016-12-23 13:08:27', 'd580f45e-8ff0-3a55-05a9-259b73d40519', null, '2016-12-22', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1433', '2016-12-23 13:09:43 工作内容', '2016-12-23 13:09:43', 'd580f45e-8ff0-3a55-05a9-259b73d40519', null, '2016-12-23', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1434', '2016-12-23 13:10:11 工作内容', '2016-12-23 13:10:11', 'd580f45e-8ff0-3a55-05a9-259b73d40519', null, '2016-12-23', null, '', '3');
INSERT INTO `busi_work_report_item` VALUES ('1435', '2016-12-23 13:10:26 工作内容', '2016-12-23 13:10:26', '0cea50e5-b1a9-42d9-cad4-ea2e2f5ffd4a', null, '2016-12-20', '2016-12-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1436', '2016-12-23 13:11:07 工作内容', '2016-12-23 13:11:07', '0cea50e5-b1a9-42d9-cad4-ea2e2f5ffd4a', null, '2016-12-22', '2016-12-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1437', '2016-12-23 13:11:24 工作内容', '2016-12-23 13:11:24', '0cea50e5-b1a9-42d9-cad4-ea2e2f5ffd4a', null, '2016-12-23', '2016-12-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1438', '2016-12-23 13:12:38 工作内容', '2016-12-23 13:12:38', '0cea50e5-b1a9-42d9-cad4-ea2e2f5ffd4a', null, '2016-12-21', '2016-12-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1439', '2016-12-23 13:13:18 工作内容', '2016-12-23 13:13:18', '776e8283-44b0-c400-3508-1e3a8f7f4071', null, '2016-12-20', '2016-12-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1440', '2016-12-23 13:13:59 工作内容', '2016-12-23 13:13:59', '776e8283-44b0-c400-3508-1e3a8f7f4071', null, '2016-12-21', '2016-12-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1441', '2016-12-23 13:14:39 工作内容', '2016-12-23 13:14:39', 'b71e3ac0-f8d1-3737-791d-f47fdb16a9d8', '1', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1442', '2016-12-23 13:15:41 工作内容', '2016-12-23 13:15:41', 'b71e3ac0-f8d1-3737-791d-f47fdb16a9d8', '2', null, null, '', null);
INSERT INTO `busi_work_report_item` VALUES ('1443', '2016-12-23 13:17:34 工作内容', '2016-12-23 13:17:34', '776e8283-44b0-c400-3508-1e3a8f7f4071', null, '2016-12-23', '2016-12-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1444', '2016-12-23 13:18:12 工作内容', '2016-12-23 13:18:12', '776e8283-44b0-c400-3508-1e3a8f7f4071', null, '2016-12-27', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1445', '2016-12-23 13:18:49 工作内容', '2016-12-23 13:18:49', '776e8283-44b0-c400-3508-1e3a8f7f4071', null, '2016-12-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1446', '2016-12-23 13:18:56 工作内容', '2016-12-23 13:18:56', '03bd508d-d3a1-ff4e-c4a5-ddb9c11b7f39', null, '2016-12-20', '2016-12-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1447', '2016-12-23 13:19:39 工作内容', '2016-12-23 13:19:39', '03bd508d-d3a1-ff4e-c4a5-ddb9c11b7f39', null, '2016-12-21', '2016-12-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1448', '2016-12-23 13:20:33 工作内容', '2016-12-23 13:20:33', '03bd508d-d3a1-ff4e-c4a5-ddb9c11b7f39', null, '2016-12-22', '2016-12-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1449', '2016-12-23 13:20:59 工作内容', '2016-12-23 13:20:59', '03bd508d-d3a1-ff4e-c4a5-ddb9c11b7f39', null, '2016-12-23', '2016-12-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1450', '2016-12-23 13:21:57 工作内容', '2016-12-23 13:21:57', '03bd508d-d3a1-ff4e-c4a5-ddb9c11b7f39', null, '2016-12-19', '2016-12-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1451', '2016-12-23 13:23:29 工作内容', '2016-12-23 13:23:29', '03bd508d-d3a1-ff4e-c4a5-ddb9c11b7f39', null, '2016-12-28', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1452', '2016-12-23 13:24:38 工作内容', '2016-12-23 13:24:38', '03bd508d-d3a1-ff4e-c4a5-ddb9c11b7f39', null, '2016-12-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1453', '2016-12-23 13:24:59 工作内容', '2016-12-23 13:24:59', '5e13b71d-7da5-790a-98a4-e8d2101672b4', null, '2016-12-20', '2016-12-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1454', '2016-12-23 13:27:16 工作内容', '2016-12-23 13:27:16', '5e13b71d-7da5-790a-98a4-e8d2101672b4', null, '2016-12-21', '2016-12-21', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1455', '2016-12-23 13:29:49 工作内容', '2016-12-23 13:29:49', '5e13b71d-7da5-790a-98a4-e8d2101672b4', null, '2016-12-22', '2016-12-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1456', '2016-12-23 13:31:43 工作内容', '2016-12-23 13:31:43', 'd580f45e-8ff0-3a55-05a9-259b73d40519', null, '2016-12-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1457', '2016-12-23 13:39:51 工作内容', '2016-12-23 13:39:51', '5e13b71d-7da5-790a-98a4-e8d2101672b4', null, '2016-12-22', '2016-12-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1458', '2016-12-23 13:47:25 工作内容', '2016-12-23 13:47:25', '5e13b71d-7da5-790a-98a4-e8d2101672b4', null, '2016-12-29', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1459', '2016-12-23 13:48:41 工作内容', '2016-12-23 13:48:41', '5e13b71d-7da5-790a-98a4-e8d2101672b4', null, '2016-12-25', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1460', '2016-12-23 13:57:32 工作内容', '2016-12-23 13:57:32', '5e13b71d-7da5-790a-98a4-e8d2101672b4', null, '2016-12-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1461', '2016-12-23 14:09:19 工作内容', '2016-12-23 14:09:19', '4b0d0275-0026-9485-39c0-27394a6674cc', null, '2016-12-23', '2016-12-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1462', '2016-12-23 14:09:41 工作内容', '2016-12-23 14:09:41', '4b0d0275-0026-9485-39c0-27394a6674cc', null, '2016-12-20', '2016-12-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1463', '2016-12-23 14:10:24 工作内容', '2016-12-23 14:10:24', '4b0d0275-0026-9485-39c0-27394a6674cc', null, '2016-12-26', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1464', '2016-12-23 14:10:50 工作内容', '2016-12-23 14:10:50', '4b0d0275-0026-9485-39c0-27394a6674cc', null, '2016-12-28', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1465', '2016-12-23 14:11:19 工作内容', '2016-12-23 14:11:19', '4b0d0275-0026-9485-39c0-27394a6674cc', null, '2016-12-26', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1466', '2016-12-23 14:12:42 工作内容', '2016-12-23 14:12:42', 'e868f485-57e7-83ae-c942-72c4789bc0ac', null, '2016-12-20', '2016-12-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1467', '2016-12-23 14:12:56 工作内容', '2016-12-23 14:12:56', 'e868f485-57e7-83ae-c942-72c4789bc0ac', null, '2016-12-23', '2016-12-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1468', '2016-12-23 14:13:30 工作内容', '2016-12-23 14:13:30', 'e868f485-57e7-83ae-c942-72c4789bc0ac', null, '2016-12-26', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1469', '2016-12-23 14:14:16 工作内容', '2016-12-23 14:14:16', 'e868f485-57e7-83ae-c942-72c4789bc0ac', null, '2016-12-26', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1470', '2016-12-23 14:14:44 工作内容', '2016-12-23 14:14:44', 'e868f485-57e7-83ae-c942-72c4789bc0ac', null, '2016-12-28', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1471', '2016-12-23 15:13:23 工作内容', '2016-12-23 15:13:23', 'a4ab8a37-2020-526a-d3e4-2268ef088f19', null, '2016-12-20', '2016-12-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1472', '2016-12-23 15:13:52 工作内容', '2016-12-23 15:13:52', 'a4ab8a37-2020-526a-d3e4-2268ef088f19', null, '2016-12-20', '2016-12-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1473', '2016-12-23 15:15:12 工作内容', '2016-12-23 15:15:12', 'a4ab8a37-2020-526a-d3e4-2268ef088f19', null, '2016-12-23', '2016-12-23', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1474', '2016-12-23 15:15:12 工作内容', '2016-12-23 15:15:12', 'a4ab8a37-2020-526a-d3e4-2268ef088f19, a4ab8a37-2020-526a-d3e4-2268ef088f19', null, '2016-12-20', '2016-12-20', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1475', '2016-12-23 15:23:48 工作内容', '2016-12-23 15:23:48', 'a4ab8a37-2020-526a-d3e4-2268ef088f19', null, '2016-12-30', null, '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1476', '2016-12-23 15:31:14 工作内容', '2016-12-23 15:31:14', 'ad85d145-a9d4-d093-504a-773aa412a173', null, '2016-12-19', '2016-12-19', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1477', '2016-12-23 15:32:31 工作内容', '2016-12-23 15:32:31', 'ad85d145-a9d4-d093-504a-773aa412a173', null, '2016-12-23', '2016-12-22', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1478', '2016-12-23 15:33:48 工作内容', '2016-12-23 15:33:48', 'ad85d145-a9d4-d093-504a-773aa412a173', null, '2016-12-30', '', '', '1');
INSERT INTO `busi_work_report_item` VALUES ('1479', '2016-12-23 15:35:00 工作内容', '2016-12-23 15:35:00', 'ad85d145-a9d4-d093-504a-773aa412a173', null, '2016-12-27', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1480', '2016-12-23 15:36:41 工作内容', '2016-12-23 15:36:41', 'ad85d145-a9d4-d093-504a-773aa412a173', null, '2016-12-29', '', '', '2');
INSERT INTO `busi_work_report_item` VALUES ('1481', '2016-12-23 15:39:25 工作内容', '2016-12-23 15:39:25', 'ad85d145-a9d4-d093-504a-773aa412a173', null, '2016-12-30', '', '', '2');

-- ----------------------------
-- Table structure for sys_button
-- ----------------------------
DROP TABLE IF EXISTS `sys_button`;
CREATE TABLE `sys_button` (
  `i_button_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_button_name` varchar(50) DEFAULT NULL,
  `c_button_icon_img` varchar(200) DEFAULT NULL,
  `i_enabled` int(11) DEFAULT NULL,
  `c_method_name` varchar(100) DEFAULT NULL,
  `c_button_sort` int(4) DEFAULT NULL,
  PRIMARY KEY (`i_button_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_button
-- ----------------------------
INSERT INTO `sys_button` VALUES ('1', '查看', '', '1', null, '5');
INSERT INTO `sys_button` VALUES ('6', '修改', '', '1', null, '2');
INSERT INTO `sys_button` VALUES ('7', '删除', '', '1', null, '3');
INSERT INTO `sys_button` VALUES ('8', '添加', '', '1', null, '1');
INSERT INTO `sys_button` VALUES ('9', '刷新', '', '1', null, '4');
INSERT INTO `sys_button` VALUES ('10', '保存', null, '1', '菜单管理', '6');
INSERT INTO `sys_button` VALUES ('11', '展开', null, '1', '菜单管理', '7');
INSERT INTO `sys_button` VALUES ('12', '折叠', null, '1', '菜单管理', '8');
INSERT INTO `sys_button` VALUES ('13', '取消修改', null, '1', '菜单管理', '9');
INSERT INTO `sys_button` VALUES ('14', '取消选中', null, '1', '菜单管理', '10');

-- ----------------------------
-- Table structure for sys_config_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_config_data`;
CREATE TABLE `sys_config_data` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_config_domain_name` varchar(50) DEFAULT NULL,
  `c_data_code` varchar(50) DEFAULT NULL,
  `c_data_name` varchar(100) DEFAULT NULL,
  `i_order` int(4) DEFAULT NULL,
  `i_enabled` int(4) DEFAULT NULL,
  `c_datatype` int(4) DEFAULT NULL,
  `c_datatype_code` varchar(50) DEFAULT NULL,
  `c_is_opt` int(4) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_config_data
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `i_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_menu_code` varchar(100) DEFAULT NULL,
  `c_menu_name` varchar(100) DEFAULT NULL,
  `c_is_leaf_menu` int(4) DEFAULT NULL,
  `c_menu_url` varchar(100) DEFAULT NULL,
  `i_enabled` int(4) DEFAULT NULL,
  `c_menu_desc` varchar(500) DEFAULT NULL,
  `c_menu_sort` int(4) DEFAULT NULL,
  PRIMARY KEY (`i_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('20', '1', '根节点', null, null, null, null, null);
INSERT INTO `sys_menu` VALUES ('21', '1001002', '用户管理', '1', 'user!queryUserFrom', '1', null, '3');
INSERT INTO `sys_menu` VALUES ('22', '1001003', '机构管理', '1', 'org!orgList', '1', null, '2');
INSERT INTO `sys_menu` VALUES ('23', '1001', '系统管理', '0', '#', '1', null, '1');
INSERT INTO `sys_menu` VALUES ('25', '1001006', '菜单管理', '1', 'menu!menuListForm', '1', null, '5');
INSERT INTO `sys_menu` VALUES ('28', '1001005', '角色管理', '1', 'role!roleListForm', '1', null, '4');
INSERT INTO `sys_menu` VALUES ('33', '1002', '发送管理', '0', '#', '1', null, '2');
INSERT INTO `sys_menu` VALUES ('35', '1002002', '发送时间', '1', 'sendTime!sendTimeForm', '1', null, '2');
INSERT INTO `sys_menu` VALUES ('36', '1002003', '日期管理', '1', 'sendDate!sendDateForm', '1', null, '3');
INSERT INTO `sys_menu` VALUES ('37', '1002004', '发送组', '1', 'workSendGroup!workSendGroupForm', '1', null, '4');
INSERT INTO `sys_menu` VALUES ('38', '1003', '业务管理', '0', '#', '1', null, '3');
INSERT INTO `sys_menu` VALUES ('40', '1004', '报表汇总', '0', '#', '1', null, '4');
INSERT INTO `sys_menu` VALUES ('41', '1004001', '汇总查看', '1', 'manageWorkReport!manageworkReportForm', '1', null, '1');
INSERT INTO `sys_menu` VALUES ('43', '1003002', '我的周报', '1', 'workReportWeek!workReportForm', '1', null, '2');
INSERT INTO `sys_menu` VALUES ('44', '1002005', '邮箱设置', '1', 'sendEmail!sendEmailForm', '1', null, '1');
INSERT INTO `sys_menu` VALUES ('45', '1003003', '我的日报', '1', 'workReport!workReportForm', '1', null, '1');

-- ----------------------------
-- Table structure for sys_menu_button
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_button`;
CREATE TABLE `sys_menu_button` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_menu_id` varchar(50) DEFAULT NULL,
  `i_button_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_button
-- ----------------------------
INSERT INTO `sys_menu_button` VALUES ('1', '1001002', '8');
INSERT INTO `sys_menu_button` VALUES ('2', '1001002', '6');
INSERT INTO `sys_menu_button` VALUES ('3', '1001002', '7');
INSERT INTO `sys_menu_button` VALUES ('4', '1001002', '9');
INSERT INTO `sys_menu_button` VALUES ('19', '1001006', '8');
INSERT INTO `sys_menu_button` VALUES ('20', '1001006', '6');
INSERT INTO `sys_menu_button` VALUES ('21', '1001006', '7');
INSERT INTO `sys_menu_button` VALUES ('22', '1001006', '9');
INSERT INTO `sys_menu_button` VALUES ('23', '1001006', '1');
INSERT INTO `sys_menu_button` VALUES ('24', '1001006', '10');
INSERT INTO `sys_menu_button` VALUES ('25', '1001006', '11');
INSERT INTO `sys_menu_button` VALUES ('26', '1001006', '12');
INSERT INTO `sys_menu_button` VALUES ('27', '1001006', '13');
INSERT INTO `sys_menu_button` VALUES ('28', '1001006', '14');
INSERT INTO `sys_menu_button` VALUES ('29', '1001005', '8');
INSERT INTO `sys_menu_button` VALUES ('30', '1001005', '8');
INSERT INTO `sys_menu_button` VALUES ('31', '1001005', '6');
INSERT INTO `sys_menu_button` VALUES ('32', '1001005', '6');
INSERT INTO `sys_menu_button` VALUES ('33', '1001005', '7');
INSERT INTO `sys_menu_button` VALUES ('34', '1001005', '7');
INSERT INTO `sys_menu_button` VALUES ('35', '1001005', '9');
INSERT INTO `sys_menu_button` VALUES ('36', '1001005', '9');
INSERT INTO `sys_menu_button` VALUES ('37', '1001005', '1');
INSERT INTO `sys_menu_button` VALUES ('38', '1001005', '1');
INSERT INTO `sys_menu_button` VALUES ('44', '1001003', '8');
INSERT INTO `sys_menu_button` VALUES ('45', '1001003', '6');
INSERT INTO `sys_menu_button` VALUES ('46', '1001003', '7');
INSERT INTO `sys_menu_button` VALUES ('47', '1001003', '9');
INSERT INTO `sys_menu_button` VALUES ('48', '1001003', '1');
INSERT INTO `sys_menu_button` VALUES ('49', '1001003', '11');
INSERT INTO `sys_menu_button` VALUES ('50', '1001003', '12');

-- ----------------------------
-- Table structure for sys_menu_tabs
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_tabs`;
CREATE TABLE `sys_menu_tabs` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_menu_id` varchar(50) DEFAULT NULL,
  `i_tab_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu_tabs
-- ----------------------------

-- ----------------------------
-- Table structure for sys_org_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_org_info`;
CREATE TABLE `sys_org_info` (
  `i_org_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_org_code` varchar(100) DEFAULT NULL,
  `c_org_name` varchar(100) DEFAULT NULL,
  `c_org_duty` varchar(200) DEFAULT NULL,
  `i_enabled` int(4) DEFAULT NULL,
  `c_memo` varchar(200) DEFAULT NULL,
  `c_org_pid` varchar(50) DEFAULT NULL,
  `c_org_leader` int(10) DEFAULT NULL,
  PRIMARY KEY (`i_org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_org_info
-- ----------------------------
INSERT INTO `sys_org_info` VALUES ('1', '1', '根节点', null, null, null, null, null);
INSERT INTO `sys_org_info` VALUES ('2', '1001', 'xxx健康科技（帝都总部）', '', '1', null, null, null);
INSERT INTO `sys_org_info` VALUES ('3', '1002001', '管理部', '', '1', null, null, null);
INSERT INTO `sys_org_info` VALUES ('4', '1002002', '研发部', '', '1', null, null, null);
INSERT INTO `sys_org_info` VALUES ('6', '1002', 'xxx健康科技（sz公司）', '', '1', null, null, null);
INSERT INTO `sys_org_info` VALUES ('7', '1001001', '市场部', '', '1', null, null, null);
INSERT INTO `sys_org_info` VALUES ('8', '1001002', '商务部', '', '1', null, null, null);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `i_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_role_name` varchar(100) DEFAULT NULL,
  `i_enabled` int(4) DEFAULT NULL,
  `c_role_desc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`i_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '系统管理员', '1', null);
INSERT INTO `sys_role` VALUES ('2', '公司员工', '1', '');
INSERT INTO `sys_role` VALUES ('3', '人事管理员', '1', '');
INSERT INTO `sys_role` VALUES ('4', '高层管理者', '1', '');
INSERT INTO `sys_role` VALUES ('5', '部门主管', '1', '');
INSERT INTO `sys_role` VALUES ('6', '测试角色1', '1', '');
INSERT INTO `sys_role` VALUES ('7', '测试角色2', '1', '');
INSERT INTO `sys_role` VALUES ('8', '权限管理员', '1', '权限管理员');

-- ----------------------------
-- Table structure for sys_rolegroup_role_mapping
-- ----------------------------
DROP TABLE IF EXISTS `sys_rolegroup_role_mapping`;
CREATE TABLE `sys_rolegroup_role_mapping` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_group_id` int(11) DEFAULT NULL,
  `i_role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_rolegroup_role_mapping
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_group`;
CREATE TABLE `sys_role_group` (
  `i_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_group_name` varchar(100) DEFAULT NULL,
  `i_enabled` int(4) DEFAULT NULL,
  `c_group_desc` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`i_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_group
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_role_id` int(11) DEFAULT NULL,
  `i_menu_id` varchar(50) DEFAULT NULL,
  `c_button_ids` varchar(50) DEFAULT NULL,
  `c_tab_ids` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('145', '3', '1001003', null, null);
INSERT INTO `sys_role_menu` VALUES ('146', '3', '1001', null, null);
INSERT INTO `sys_role_menu` VALUES ('147', '3', '1001002', null, null);
INSERT INTO `sys_role_menu` VALUES ('148', '3', '1001005', null, null);
INSERT INTO `sys_role_menu` VALUES ('149', '3', '1002002', null, null);
INSERT INTO `sys_role_menu` VALUES ('150', '3', '1002', null, null);
INSERT INTO `sys_role_menu` VALUES ('151', '3', '1002003', null, null);
INSERT INTO `sys_role_menu` VALUES ('152', '3', '1002004', null, null);
INSERT INTO `sys_role_menu` VALUES ('153', '3', '1003', null, null);
INSERT INTO `sys_role_menu` VALUES ('155', '3', '1003002', null, null);
INSERT INTO `sys_role_menu` VALUES ('156', '3', '1004', null, null);
INSERT INTO `sys_role_menu` VALUES ('157', '3', '1004001', null, null);
INSERT INTO `sys_role_menu` VALUES ('165', '5', '1003', null, null);
INSERT INTO `sys_role_menu` VALUES ('167', '5', '1003002', null, null);
INSERT INTO `sys_role_menu` VALUES ('168', '5', '1004', null, null);
INSERT INTO `sys_role_menu` VALUES ('169', '5', '1004001', null, null);
INSERT INTO `sys_role_menu` VALUES ('170', '4', '1003', null, null);
INSERT INTO `sys_role_menu` VALUES ('172', '4', '1003002', null, null);
INSERT INTO `sys_role_menu` VALUES ('173', '4', '1004', null, null);
INSERT INTO `sys_role_menu` VALUES ('174', '4', '1004001', null, null);
INSERT INTO `sys_role_menu` VALUES ('194', '8', '1001', null, null);
INSERT INTO `sys_role_menu` VALUES ('195', '8', '1001003', null, null);
INSERT INTO `sys_role_menu` VALUES ('196', '8', '1001002', null, null);
INSERT INTO `sys_role_menu` VALUES ('197', '8', '1001005', null, null);
INSERT INTO `sys_role_menu` VALUES ('198', '8', '1001006', null, null);
INSERT INTO `sys_role_menu` VALUES ('199', '1', '1001', null, null);
INSERT INTO `sys_role_menu` VALUES ('200', '1', '1001003', null, null);
INSERT INTO `sys_role_menu` VALUES ('201', '1', '1001002', null, null);
INSERT INTO `sys_role_menu` VALUES ('202', '1', '1001005', null, null);
INSERT INTO `sys_role_menu` VALUES ('203', '1', '1001006', null, null);
INSERT INTO `sys_role_menu` VALUES ('204', '1', '1002', null, null);
INSERT INTO `sys_role_menu` VALUES ('205', '1', '1002005', null, null);
INSERT INTO `sys_role_menu` VALUES ('206', '1', '1002002', null, null);
INSERT INTO `sys_role_menu` VALUES ('207', '1', '1002003', null, null);
INSERT INTO `sys_role_menu` VALUES ('208', '1', '1002004', null, null);
INSERT INTO `sys_role_menu` VALUES ('209', '1', '1003003', null, null);
INSERT INTO `sys_role_menu` VALUES ('210', '1', '1003', null, null);
INSERT INTO `sys_role_menu` VALUES ('211', '2', '1003', null, null);
INSERT INTO `sys_role_menu` VALUES ('212', '2', '1003003', null, null);
INSERT INTO `sys_role_menu` VALUES ('213', '2', '1003002', null, null);
INSERT INTO `sys_role_menu` VALUES ('214', '2', '1004', null, null);
INSERT INTO `sys_role_menu` VALUES ('215', '2', '1004001', null, null);

-- ----------------------------
-- Table structure for sys_send_date
-- ----------------------------
DROP TABLE IF EXISTS `sys_send_date`;
CREATE TABLE `sys_send_date` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(50) DEFAULT NULL,
  `c_week_date` varchar(50) DEFAULT NULL,
  `c_memo` varchar(50) DEFAULT NULL,
  `c_type` varchar(10) NOT NULL DEFAULT 'day',
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_send_date
-- ----------------------------
INSERT INTO `sys_send_date` VALUES ('15', '2016-05-20', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('16', '2016-05-27', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('17', '2016-06-03', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('18', '2016-06-12', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('19', '2016-06-17', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('20', '2016-06-24', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('21', '2016-07-01', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('22', '2016-07-08', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('23', '2016-07-15', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('24', '2016-07-22', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('25', '2016-07-29', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('26', '2016-08-05', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('27', '2016-08-12', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('28', '2016-08-19', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('29', '2016-08-26', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('30', '2016-09-02', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('31', '2016-09-09', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('32', '2016-09-18', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('33', '2016-09-23', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('34', '2016-09-30', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('35', '2016-10-14', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('36', '2016-10-21', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('37', '2016-10-28', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('38', '2016-11-04', null, '', 'day,week');
INSERT INTO `sys_send_date` VALUES ('63', '2016-12-09', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('64', '2016-12-12', null, '', 'day');
INSERT INTO `sys_send_date` VALUES ('65', '2016-12-13', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('66', '2016-12-14', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('67', '2016-12-15', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('68', '2016-12-16', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('69', '2016-12-19', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('70', '2016-12-20', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('71', '2016-12-21', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('72', '2016-12-22', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('73', '2016-12-23', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('74', '2016-12-26', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('75', '2016-12-27', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('76', '2016-12-28', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('77', '2016-12-29', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('78', '2016-12-30', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('79', '2017-01-02', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('80', '2017-01-03', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('81', '2017-01-04', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('82', '2017-01-05', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('83', '2017-01-06', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('84', '2017-01-09', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('85', '2017-01-10', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('86', '2017-01-11', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('87', '2017-01-12', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('88', '2017-01-13', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('89', '2017-01-16', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('90', '2017-01-17', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('91', '2017-01-18', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('92', '2017-01-19', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('93', '2017-01-20', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('94', '2017-01-23', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('95', '2017-01-24', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('96', '2017-01-25', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('97', '2017-01-26', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('98', '2017-01-27', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('99', '2017-01-30', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('100', '2017-01-31', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('101', '2017-02-01', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('102', '2017-02-02', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('103', '2017-02-03', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('104', '2017-02-06', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('105', '2017-02-07', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('106', '2017-02-08', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('107', '2017-02-09', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('108', '2017-02-10', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('109', '2017-02-13', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('110', '2017-02-14', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('111', '2017-02-15', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('112', '2017-02-16', null, null, 'day');
INSERT INTO `sys_send_date` VALUES ('113', '2017-02-17', null, null, 'day,week');
INSERT INTO `sys_send_date` VALUES ('114', '2017-02-20', null, null, 'day');

-- ----------------------------
-- Table structure for sys_send_email
-- ----------------------------
DROP TABLE IF EXISTS `sys_send_email`;
CREATE TABLE `sys_send_email` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_name` varchar(50) DEFAULT NULL,
  `c_email` varchar(50) DEFAULT NULL,
  `c_password` varchar(50) DEFAULT NULL,
  `c_memo` varchar(50) DEFAULT NULL,
  `c_stmp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_send_email
-- ----------------------------

-- ----------------------------
-- Table structure for sys_send_time
-- ----------------------------
DROP TABLE IF EXISTS `sys_send_time`;
CREATE TABLE `sys_send_time` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `d_start_time` varchar(50) DEFAULT NULL,
  `d_end_time` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_send_time
-- ----------------------------
INSERT INTO `sys_send_time` VALUES ('1', '08:00', '23:59');

-- ----------------------------
-- Table structure for sys_tabs
-- ----------------------------
DROP TABLE IF EXISTS `sys_tabs`;
CREATE TABLE `sys_tabs` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_tab_name` varchar(50) DEFAULT NULL,
  `i_enabled` int(4) DEFAULT NULL,
  `i_is_opt` int(4) DEFAULT NULL,
  `c_memo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_tabs
-- ----------------------------
INSERT INTO `sys_tabs` VALUES ('1', '选项卡一', '1', null, '12');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `i_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_userloginname` varchar(50) DEFAULT NULL,
  `c_userloginpwd` varchar(100) DEFAULT NULL,
  `c_username` varchar(50) DEFAULT NULL,
  `c_org_code` varchar(100) DEFAULT NULL,
  `c_org_code_attach` varchar(500) DEFAULT NULL,
  `c_telephone` varchar(50) DEFAULT NULL,
  `c_work_number` varchar(50) DEFAULT NULL,
  `c_position_name` varchar(100) DEFAULT NULL,
  `d_createtime` varchar(50) DEFAULT NULL,
  `i_logincount` int(11) DEFAULT NULL,
  `c_spelllong` varchar(50) DEFAULT NULL,
  `c_spellshort` varchar(50) DEFAULT NULL,
  `i_enabled` int(4) DEFAULT NULL,
  `c_memo` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`i_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'ojs', 'e10adc3949ba59abbe56e057f20f883e', '欧js', '1002002', null, '', '', null, '2016-05-04 08:50:45', null, '', '00', '1', '');
INSERT INTO `sys_user` VALUES ('2', 'chensir', 'e10adc3949ba59abbe56e057f20f883e', '陈sir', '1002002', '1001002,', '', '', null, '2014-09-22 10:46:30', null, '', '01', '1', '');
INSERT INTO `sys_user` VALUES ('3', 'xiasir', 'e10adc3949ba59abbe56e057f20f883e', '夏sir', '1002002', '', '', '', null, '2014-11-04 02:01:46', null, '', '02', '1', '');
INSERT INTO `sys_user` VALUES ('4', 'linjie', 'e10adc3949ba59abbe56e057f20f883e', '林姐', '1002002', '', '', '', null, '2014-11-04 02:03:06', null, '', '10', '1', '');
INSERT INTO `sys_user` VALUES ('6', 'zhangsir', 'e10adc3949ba59abbe56e057f20f883e', '张sir', '1002002', '', '', '', null, '2014-11-04 02:05:00', null, '', '03', '1', '');
INSERT INTO `sys_user` VALUES ('8', 'mosir', 'e10adc3949ba59abbe56e057f20f883e', '莫sir', '1002002', '', '', '', null, '2014-11-04 02:08:56', null, '', '04', '1', '');
INSERT INTO `sys_user` VALUES ('10', 'tianmm', 'e10adc3949ba59abbe56e057f20f883e', '田妹子', '1002002', '', '', '', null, '2014-11-04 02:10:09', null, '', '07', '1', '');
INSERT INTO `sys_user` VALUES ('14', 'zhangios', 'e10adc3949ba59abbe56e057f20f883e', '章ios', '1002002', '', '', '', null, '2016-04-17 13:13:54', null, '', '06', '1', '');
INSERT INTO `sys_user` VALUES ('16', 'lumm', 'e10adc3949ba59abbe56e057f20f883e', '陆妹子', '1002002', null, '', '', null, '2016-05-04 08:51:53', null, '', '11', '1', '');
INSERT INTO `sys_user` VALUES ('17', 'chenAndroid、', 'e10adc3949ba59abbe56e057f20f883e', '陈Android、', '1002002', null, '', '', null, '2016-05-04 08:52:36', null, '', '05', '1', '');
INSERT INTO `sys_user` VALUES ('18', 'pengsir', 'e10adc3949ba59abbe56e057f20f883e', '彭sir', '1002002', null, '', '', null, '2016-05-04 08:53:12', null, '', '08', '1', '');
INSERT INTO `sys_user` VALUES ('20', 'gusir', 'e10adc3949ba59abbe56e057f20f883e', '顾sir', '1002', null, '', '', null, '2016-05-06 08:55:37', null, '', '10', '1', '');
INSERT INTO `sys_user` VALUES ('21', 'pengbs', 'e10adc3949ba59abbe56e057f20f883e', '彭博士', '1002002', null, '', '', null, '2016-05-06 13:46:59', null, '', '09', '1', '');
INSERT INTO `sys_user` VALUES ('24', 'wangmm', 'e10adc3949ba59abbe56e057f20f883e', '王MM', '1002002', null, '', '', null, '2016-07-22 13:01:12', null, '', '12', '1', '');
INSERT INTO `sys_user` VALUES ('25', 'wangsir', 'e10adc3949ba59abbe56e057f20f883e', '王sir', '1002002', null, '', '', null, '2016-08-12 12:08:03', null, '', '13', '1', '');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_user_id` int(11) DEFAULT NULL,
  `i_role_id` int(11) DEFAULT NULL,
  `i_org_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('69', '6', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('70', '8', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('80', '20', '4', '1002');
INSERT INTO `sys_user_role` VALUES ('81', '19', '5', '1002004');
INSERT INTO `sys_user_role` VALUES ('82', '13', '3', '1002003');
INSERT INTO `sys_user_role` VALUES ('83', '13', '5', '1002003');
INSERT INTO `sys_user_role` VALUES ('84', '1', '4', '1002002');
INSERT INTO `sys_user_role` VALUES ('85', '1', '5', '1002002');
INSERT INTO `sys_user_role` VALUES ('87', '17', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('88', '16', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('89', '14', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('90', '10', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('92', '4', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('95', '21', '1', '1002002');
INSERT INTO `sys_user_role` VALUES ('96', '21', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('99', '2', '1', '1002002');
INSERT INTO `sys_user_role` VALUES ('100', '2', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('101', '3', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('102', '24', '2', '1002002');
INSERT INTO `sys_user_role` VALUES ('103', '25', '2', '1002002');

-- ----------------------------
-- Table structure for sys_user_role_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role_group`;
CREATE TABLE `sys_user_role_group` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_user_id` int(11) DEFAULT NULL,
  `i_group_id` int(11) DEFAULT NULL,
  `i_org_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role_group
-- ----------------------------

-- ----------------------------
-- Table structure for sys_work_send_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_work_send_group`;
CREATE TABLE `sys_work_send_group` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_group_name` varchar(50) DEFAULT NULL,
  `c_sender_ids` varchar(300) DEFAULT NULL,
  `c_worker_ids` varchar(300) DEFAULT NULL,
  `c_creat_time` varchar(50) DEFAULT NULL,
  `c_enabled` int(11) DEFAULT NULL,
  `c_memo` varchar(50) DEFAULT NULL,
  `c_time_tip` int(4) DEFAULT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_work_send_group
-- ----------------------------
INSERT INTO `sys_work_send_group` VALUES ('1', '研发', '2,6,8,17,10,21,16,24,', '2,6,8,17,10,21,16,24,', '2016-05-04', '1', '', '1');
