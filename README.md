# daily-work

#### 项目介绍
企业工作日报系统
 
在原始版本war基础上反编译后基础上改造的工作日报系统代码

修改点：

1.业务管理已经分离出日报、周报

![输入图片说明](https://images.gitee.com/uploads/images/2018/0722/180627_5694fa33_722815.png "微信截图_20180722180456.png")

2.报表汇总包含日报/周报

![输入图片说明](https://images.gitee.com/uploads/images/2018/0722/180641_ac874078_722815.png "微信截图_20180722180530.png")

3.excel导出报表

![输入图片说明](https://images.gitee.com/uploads/images/2018/0722/180656_2d8b899f_722815.png "微信截图_20180722180559.png")


技术路线

SSH3 - Struts2   Spring3 Hibernate3 

SSH4 - Struts2   Spring4 Hibernate4

 SH4 - Springmvc Spring4 Hibernate4