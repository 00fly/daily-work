<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    //清除登录凭证
			session.removeAttribute("user");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>日报系统 v1.0</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<link rel="stylesheet" type="text/css" href="js/jquery-easyui-1.3.2/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="js/jquery-easyui-1.3.2/themes/icon.css">
<script type="text/javascript" src="js/jquery-easyui-1.3.2/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/jquery-easyui-1.3.2/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
	$(function() {
		$('#dlg').dialog({
			title : '日报系统 v1.0',
			width : 350,
			height : 220,
			closed : false,
			cache : false,
			closable : false,
			modal : true
		});
	});

	//重置
	function clearForm() {
		$("#fmLogin").form('clear');
	}

	//登录
	function submitForm() {
		$("#msgSign").html("&nbsp;");
		$("#fmLogin").form('submit', {
			url : "admin/login",
			method : 'post',
			success : function(data) {
				var obj = eval('(' + data + ')');
				if (obj.result == 1) {
					window.location = "user/index";
				} else if (obj.result == 3) {
					window.location = "user/index";
				} else if (obj.result == 2) {
					$("#msgSign").html("* " + obj.msg);
				} else {
					$("#msgSign").html("* 用户账号或密码错误!");
				}
			}
		});
	}

	document.onkeydown = function(event) {
		var e = event || window.event || arguments.callee.caller.arguments[0];
		if (e && e.keyCode == 13) { // enter 键              
			submitForm();
		}
	}
</script>
</head>

<body style="width: 100%; height: 100%;">
	<div id="dlg" style="text-align: center;">
		<br /> <br />
		<form method="post" id="fmLogin">
			账号:
			<input type="text" name="userAccount" value="chensir" />
			<br /> <br /> 密码:
			<input type="password" name="pwd" value="123456" />
			<br /> <br /> <span style="color: red;" id="msgSign">&nbsp;</span>
		</form>
		<span style="texty-align: center; padding: 1px"> <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">登录</a> <a href="javascript:void(0)" class="easyui-linkbutton"
			onclick="clearForm()">重置</a>
		</span>
	</div>
</body>
</html>
