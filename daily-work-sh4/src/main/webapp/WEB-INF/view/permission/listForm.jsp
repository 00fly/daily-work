<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head> 
<title>权限管理</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" type="text/css"	href="js/easyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"	href="js/easyUI/themes/icon.css">
<link rel="stylesheet" type="text/css"	href="css/myCss.css">
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript"	src="js/easyUI/jquery.easyui.min.js"></script>
<script type="text/javascript"	src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript"	src="js/syUtil.js"></script>
<script type="text/javascript" charset="utf-8">
	var addCount = 0;
	var url ;
	$(function() { 
		$('#list_data').datagrid({ 
	        title:'权限列表', 
	        url:"permission/queryUserByCodeAnsPage?orgCode="+0,
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:20,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'userId',title:'编码',width:100,hidden:'true'},
				{field:'userName',title:'权限名称',width:200,align:'center'},
				{field:'userLoginName',title:'访问路径',width:200,align:'center'},
				{field:'workNumber',title:'权限编码',width:200,align:'center'}, 
				{field:'enabled',title:'备注',width:160,align:'center'}
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	               var orgCodeHtml = $("#codeDIVId").html();
					if(orgCodeHtml==""){
						$.messager.alert('提示','请选择所属的菜单!','warning');
					}else{
						form.form('clear');
						win.window('open');
						win.dialog('open').dialog('setTitle','添加用户');
						$("#isAbleId").attr("checked","checked");
						$("#loginName").removeClass("bgcolor");
						$("#loginName").removeAttr('readonly'); 
					}
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	                var row = $("#list_data").datagrid("getSelected");
					if(row){
						win.dialog('open').dialog('setTitle','修改用户');
						win.form('clear');
						$("#loginName").addClass("bgcolor");
						$("#loginName").attr('readonly','readonly');
						win.form('clear');
						url = 'user/loadUserByUserId?userId='+row.userId;
						win.form('load',url);
						var orgCodeHtml = $("#codeDIVId").html();  
					}
					else
					{
						$.messager.alert('提示','请选择需要修改的用户!','warning');
						return false;
					}
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	                var rows = $("#list_data").datagrid("getSelected");
					//判断是否选择行
					if(!rows || rows.length == 0){
						$.messager.alert('提示','请选择你要删除的用户!','warning');
						return ;
					}
					$.messager.confirm('提示', '确定要删除用户【'+rows.userName+'】吗?', function(r){
						if (r){
							$.ajax({
								type: "POST",
								url: "user/deleteUser",
								data: {userId:rows.userId},
								success: function(data){
									var obj = eval('('+data+')');
									if (obj.result==1){
										$('#list_data').datagrid('reload');    // reload the user data
										$.messager.show({
											title:'提示',
											msg:obj.errorMsg,
											timeout:2000,
											showType:'fade'
										});
									} else {
										$.messager.show({    // show error message
											title: '错误',
											msg: obj.errorMsg
										});
									}
								}
							});
			
						}
					});
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				   $('#list_data').datagrid('reload');
				}
			}],
			onLoadSuccess:function(data)  
             {  
                $(".note").tooltip(  
                    {
                    position: 'left',  
                    onShow: function(){  
                        $(this).tooltip('tip').css({   
                            //width:'300',  
                            boxShadow: '1px 1px 3px #292929'                          
                        });  
                    }  
                }  
                );  
             }  
	    }); 
	    
	    
	    //组织树
	    $('#treeOrg').tree({
               url:'menu/queryMenuTreeToRole?parCode=1',
               lines:true,
               animate:true,
			   onBeforeExpand:function(node,param){                         
			        $('#treeOrg').tree('options').url = "menu/queryMenuTreeToRole?parCode="+node.id;  
		       },
               onClick:function(node){  
			        $("#codeDIVId").html(node.id);
			        var url = "permission/queryUserByCodeAnsPage?orgCode="+node.id;
			        $('#list_data').datagrid({
					    url:url
					});
			   },
			   onLoadSuccess : function(row, data) {
					var t = $(this);
					if (data) {
						$(data).each(function(index, d) {
							if (this.state == 'closed') {
								t.tree('expandAll');
							}
						});
					}
			 }
		});
	    
		$('#btn-save,#btn-cancel').linkbutton();
		win = $('#provider-window').window({
			closed:true,
			modal : true
		});
		form = win.find('form');
	}); 
	
	//刷新组织tree
	function refreshMenuTree(){
	   //组织树
	    $('#treeOrg').tree({
               url:'menu/queryMenuTreeToRole?parCode=1',
               lines:true,
               animate:true,
			   onBeforeExpand:function(node,param){                         
			        $('#treeOrg').tree('options').url = "menu/queryMenuTreeToRole?parCode="+node.id;  
		       },
               onClick:function(node){  
			        $("#codeDIVId").html(node.id);
			        var url = "permission/queryUserByCodeAnsPage?orgCode="+node.id;
			        $('#list_data').datagrid({
					    url:url
					});
			   },
			   onLoadSuccess : function(row, data) {
					var t = $(this);
					if (data) {
						$(data).each(function(index, d) {
							if (this.state == 'closed') {
								t.tree('expandAll');
							}
						});
					}
			 }
		});
	}
	
	
	//展开
	function openMenuTree(){
	    $('#treeOrg').tree('expandAll');
	}
	
	//折叠
	function closeMenuTree(){
	   $('#treeOrg').tree('collapseAll');
	}
   var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
   };  
        
	var win;
	var form;
	// 弹出新增用户
	function newUser(){
        var orgCodeHtml = $("#codeDIVId").html();
        if(orgCodeHtml==""){
            $.messager.alert('提示','请选择所属的组织机构!','warning');
        }else{
           form.form('clear');
           win.window('open');
           win.dialog('open').dialog('setTitle','添加用户');
           $("#isAbleId").attr("checked","checked");
           $("#loginName").removeClass("bgcolor");
           $("#loginName").removeAttr('readonly');
        }
    }
    
	// 保存用户
    function saveUser(){
     	var orgCodeHtml = $("#codeDIVId").html();
        $("#codeToUserId").val(orgCodeHtml);
        url = "permission/saveUserByOrgCode";
        //alert(url);
    	$("#fmUser").form('submit', {
    		url:url,
    		method:'post',
    		success:function(data){
    			var obj = eval('('+data+')');
    			if (obj.result==1){
    				$('#list_data').datagrid('reload');
    				$.messager.show({title:'提示',msg:obj.msg});
    				win.window('close');
    			} else {
    				$.messager.alert('提示','添加用户失败!','warning');
    			}
    		}
    	});
    }
    
    //刷新用户
    function refreshUser(){
       $('#list_data').datagrid('reload');
    }
    
    
    //保存核心人员
    function saveCorePerson(){
	    var checks = $(".coreUseres");
	    var strBuffer = "";
		var objarray = checks.length;
			var strBuffer = "";
			for (i = 0; i < objarray; i++) {
				if (checks[i].checked == true) {
					modelid = checks[i].value;
					strBuffer += modelid;
					strBuffer += "#";
				}
			}
			
	   //提交数据
	   $.ajax({
				url : 'user/saveCoreUser',
				data : {
					coreUserBuffer:strBuffer
				},
				success : function(data) {
					var obj = eval('('+data+')');
	                if (obj.result==1){
					    $.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					    $('#coreToUserId').dialog('close'); 
					} else {
						$.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					}
				}
			  });
    } 
    
    // 验证
    function setValidation(){
		$("#name").validatebox({
			required: true,
			validType:['length[0,20]'],
			missingMessage:"用户名称不能为空",
			invalidMessage:"长度不能超过10个汉字"
		});
	}
	// 关闭窗口
    function closeWindow(){
    	win.window('close');
    }
    
    //全选和反选
    $(function(){
   	  $("#check_all").click(function(){
			if(this.checked){ 
			  $(".roleselect").each(function(){this.checked=true;}); 
			}else{ 
			  $(".roleselect").each(function(){this.checked=false;}); 
			} 
	  });
    
    $("#check_allgroup").click(function(){
        if(this.checked){ 
			$(".groupChe").each(function(){this.checked=true;}); 
		}else{ 
			$(".groupChe").each(function(){this.checked=false;}); 
		} 
    });
    
    });
    
    
      //全选
    function allSelect(cellvalue, options, rowObject){
        return "<input onclick=selectAlls('"+options.id+"') val='1' id="+options.id+" type='checkbox'>";
    }
    
    //全部选择项
    function selectAlls(orgCode){
        var fVal = $("#"+orgCode).val();
        if(fVal){
           $(".but"+orgCode).each(function(){this.checked=true;}); 
           $("#"+orgCode).val(0)
        }
        
        if(fVal==0){
           $(".but"+orgCode).each(function(){this.checked=false;}); 
           $("#"+orgCode).val(1)
        }
    }
</script>
<style type="text/css">
.form_label{
   text-align:right;
}

.ssId{
   width:140px;
}

.bgcolor{
   background-color:#F2F2F2;
}
</style>
</head>
<body class="easyui-layout">
    <div data-options="region:'west',split:true,title:'菜单列表',iconCls:'icon-book'"
				style="width: 240px;height:100%">
				<div style="border: 1px solid #ddd;">
					<div id="toolbar" style="vertical-align: middle">
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-expend" plain="true" onclick="openMenuTree()">展开</a>
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-suoxiao" plain="true" onclick="closeMenuTree()">折叠</a>
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload" plain="true" onclick="refreshMenuTree()">刷新</a>
					</div>
				</div>
				<div>
					<ul id="treeOrg" class="ztree">
					</ul>
				</div>
	</div>
	<div region="center" border="false">
	      <div class="easyui-layout" data-options="fit:true"> 
                <div data-options="region:'center',border:false">
	                <table id="list_data" cellspacing="0" cellpadding="0" >  
			        </table>
                </div>
          </div>
	</div>
	<span id="codeDIVId" style="display:none;"></span>
	
  <!-- 添加用户界面 -->
  <div id="provider-window" title="添加用户" style="width:400px;height:440px;" data-options="minimizable:false,maximizable:false,collapsible:false">
		<div style="padding:20px 30px 40px 30px;">
			<form method="post" id="fmUser">
				<input name="orgCode" type="hidden" id="codeToUserId"></input>
				<input name="userId" type="hidden" id="userId"></input>
				<table width="300px" align="center" style="align: center" border="0">
					<tr>
						<td class="form_label">权限名称：</td>
						<td class="form_content" >
						<input name="userName" class="easyui-validatebox" maxlength="32" data-options="required:true,validType:'length[1,32]'"></input>
						</td>
					</tr>
					<tr>
						<td class="form_label">访问路径：</td>
						<td class="form_content" ><input id="loginName" name="userLoginName" class="easyui-validatebox" maxlength="32" data-options="required:true,validType:'length[1,32]'"></input></td>
					</tr>
					<tr>
						<td class="form_label">权限编码：</td>
						<td class="form_content" ><input id="code"  name="code" maxlength="11" class="easyui-validatebox" data-options="required:true,validType:'length[1,32]'"></input></td>
					</tr> 
					<tr>
						<td class="form_label">排序：</td>
						<td class="form_content" ><input name="spellShort" maxlength="32"></input></td>
					</tr> 
					<tr>
						<td class="form_label">备注：</td>
						<td class="form_content" >
                           <textarea style="height:60px;width:155px" name="userMemo"></textarea>
                        </td>
					</tr>
					<tr>
						<td class="form_label" colspan="2" style="text-align: center">
							<a href="javascript:void(0)" onclick="saveUser()" id="btn-save">保存</a>
							<a href="javascript:void(0)" onclick="closeWindow()" id="btn-cancel">取消</a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<!-- end添加用户界面 --> 
</body>
</html>