<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>My JSP 'tabList.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
	<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
	<link rel="stylesheet" type="text/css"
			href="css/mast.css">
	<script type="text/javascript">
	$(function(){
	    $('#tabGrid').datagrid({ 
	        title:'选项卡列表', 
	        url:"tab/queryTabListByPage",
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:10,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'idd',title:'编码',width:200,hidden:'true'},
				{field:'tabName',title:'名称',width:200,align:'center'},
				{field:'enabled',title:'是否可用',width:90,align:'center', formatter: imgcheckbox },
				{field:'memo',title:'备注',width:250,align:'center'},
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	               	$('#addTabId').show();
					$('#fmTab').form('clear');
					$("#titleId").html("添加选项卡");
					$('#tabId').val(0);
					$('#addTabId').dialog({
						title: '添加选项卡',
						width: 430,
						height: 300,
						padding: 5,
						modal: true,
						buttons: [{
							text: '提交',
							handler: function() {
							 saveTab();
						}
						},
						{
							text: '取消',
							handler: function() {
							$('#addTabId').dialog('close');
						}
						}]
					});
					
					$("#isAbleId").attr("checked","checked");
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	                var row = $('#tabGrid').datagrid('getSelected');
					if (row){
						if(row.isopt==1){
							$.messager.alert('提示','你没有权限修改系统级配置!','warning');
						}else{
							$('#addTabId').show();
							$('#fmTab').form('clear');
							$("#titleId").html("修改选项卡");
							$('#tabId').val(row.id);
							$('#addTabId').dialog({
								title: '添加选项卡',
								width: 430,
								height: 300,
								padding: 5,
								modal: true,
								buttons: [{
									text: '提交',
									handler: function() {
									opt.save('tab');
								}
								},
								{
									text: '取消',
									handler: function() {
									$('#addTabId').dialog('close');
								}
								}]
							});
							
							
							$("#tabNameId").val(row.tabName);
							if(row.enabled==1){
								$("#isAbleId").attr("checked","checked");
							}else{
								$("#noAbleId").attr("checked","checked");
							}
						}
					}else{
						$.messager.alert('提示','请选择你要修改的选项卡!','warning');
					}
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	                var row = $('#tabGrid').datagrid('getSelected');
					if (row){
						if(row.isopt==1){
							$.messager.alert('提示','你没有权限删除系统级配置!','warning');
						}else{
							$.messager.confirm('提示','你确定删除【'+row.tabName+'】?',function(r){
								if (r){
									$.ajax({
										type: "POST",
										url: "tab/deleteTab",
										data: {id:row.id},
										success: function(data){
											var obj = eval('('+data+')');
											if (obj.result==1){
												$('#tabGrid').datagrid('reload');    // reload the user data
											} else {
												$.messager.show({    // show error message
													title: '提示',
													msg: obj.errorMsg
												});
											}
										}
									});
								}
							});
						}
					}else{
						$.messager.alert('提示','请选择你要删除的选项卡!','warning');
					}
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				  $('#tabGrid').datagrid('reload');
				}
			}]
	    });
	});
	
	//保存按键
	function saveTab(){
	        var url1 = "tab/saveTab";
		$('#fmTab').form('submit', {
			url: url1,
			type: 'POST',
			onSubmit: function() {
			return $(this).form('validate');
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#addTabId').dialog('close');
				$('#tabGrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}
	
	
	//图标显示
	var imgShowbox = function (cellvalue, options, rowObject){
	    return "<img src='${pageContext.request.contextPath}"+options.TabIconImg+"' alt='"+options.TabName+"' title='"+options.TabName+"' style='width:16px;height:16px;'/>";
	} 
	//是否可用图标显示
	var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
    };  
	</script>

    <style type="text/css">
    .form_label{
    text-align:right;
    }
    </style>
  </head>
  
  <body class="easyui-layout">
        <div region="center" border="false">
			<table id="tabGrid">
			</table>
		</div>
		
  			<!-- 添加选项卡界面 -->
  			<div id="addTabId" >
			<center>
			<form method="post" id="fmTab" >
				<br/>
				<br/>
				<span id="titleId" style="font-size:20px;font-weight:800"></span>
				<input name="id" id="tabId" style="display:none;">
				<br/>
				<br/>
				<table width="300px" align="center" style="align: center;" cellpadding="7" border="0">
					<tr>
						<td class="form_label" style="width:150xp;">选项卡名称：</td>
						<td class="form_content" ><input type="text" name="tabName" id="tabNameId" class="easyui-validatebox" data-options="required:true" maxlength="32"></input></td>
					</tr>
					<tr>
						<td class="form_label">是否可用：</td>
						<td class="form_content" >
						<input type="radio" name="enabled" value="1" id="isAbleId"/>可用&nbsp;&nbsp;
                        <input type="radio" name="enabled" value="0" id="noAbleId"/>禁用&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td class="form_label">选项卡备注：</td>
						<td class="form_content" >
						<input type="text" name="memo" id="memoId">
						</td>
					</tr>
					
				</table>
			</form>
			</center>
		</div>
	<!-- end添加选项卡界面 -->
  </body>
</html>
