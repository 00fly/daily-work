<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head> 
	<title>My JSP 'menuList.jsp' starting page</title>
	<link rel="stylesheet" type="text/css"	href="../js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="../js/easyUI/themes/icon.css">	
	<link rel="stylesheet" type="text/css"	href="../css/myCss.css">
	<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="../js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../js/easyUI/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="../js/syUtil.js"></script>	
	<script type="text/javascript" charset="utf-8">
	var editRow;
	var editType;/*add or edit or undefined*/
	var treegrid;
	var iconData;
	$(function() {
		treegrid = $('#treegrid').treegrid({
			url : '/menu/queryMenuTreeGrid',
			toolbar : [ {
				text : '展开',
				iconCls : 'icon-expend',
				handler : function() {
					var node = treegrid.treegrid('getSelected');
					if (node) {
						treegrid.treegrid('expandAll', node.id);
					} else {
						treegrid.treegrid('expandAll');
					}
				}
			}, '-', {
				text : '折叠',
				iconCls : 'icon-suoxiao',
				handler : function() {
					var node = treegrid.treegrid('getSelected');
					if (node) {
						treegrid.treegrid('collapseAll', node.id);
					} else {
						treegrid.treegrid('collapseAll');
					}
				}
			}, '-', {
				text : '刷新',
				iconCls : 'icon-reload',
				handler : function() {
					editRow = undefined;
					editType = undefined;
					treegrid.treegrid('reload');
						}
					}, '-', {
						text : '增加',
						iconCls : 'icon-add',
						handler : function() {
							if (editRow != undefined) {
								treegrid.treegrid('endEdit', editRow.id);
							}
					
							var parStr = "";
							if (editRow == undefined) {
								var node = treegrid.treegrid('getSelected');
								if(node==null){
									parStr = 1;
								}else{
									parStr = node.id;
								}
								var row = [ {  //添加时候初始化
									id : '',
									menuName : '菜单名称',
									menuURL : '',
									isLeafMenu: 1,
									enabled : 1,
									parCode:parStr
								} ];
								treegrid.treegrid('append', {
									parent : node == null ? '' : node.id,
											data : row
								});
					
								editRow = row[0];
								editType = 'add';
								treegrid.treegrid('beginEdit', editRow.id);
								$(".datagrid-editable-input").height(22);
							}
				}
			}, '-', {
				text : '删除',
				iconCls : 'icon-remove',
				handler : function() {
					var node = treegrid.treegrid('getSelected');
					if (node) {
						$.messager.confirm('提示', '您确定要删除【' + node.menuName + '】？', function(b) {
							if (b) {
								$.ajax({
									url : 'menu/deleteMenu',
									data : {
									menuCode : node.id
								},
								success : function(data) {
									var obj = eval('('+data+')');
									if (obj.result==1){
										/*
											for ( var i = 0; i < obj.datas.length; i++) {
												$("tr[node-id='"+obj.datas[i].menuCode+"']").remove();
										    }
										 */
										$.messager.show({
											msg : obj.errorMsg,
											title : '提示'
										});
										treegrid.treegrid('reload');
										editRow = undefined;
									} else {
										$.messager.show({
											msg :  obj.errorMsg,
											title : '提示'
										});
									}
								}
								});
							}
						});
					}
				}
			}, '-', {
				text : '编辑',
				iconCls : 'icon-edit',
				handler : function() {
					var node = treegrid.treegrid('getSelected');
					if (node && node.id) {
						if (editRow != undefined) {
							treegrid.treegrid('endEdit', editRow.id);
						}
						//选择编辑
						if (editRow == undefined) {
							treegrid.treegrid('beginEdit', node.id);
							editRow = node;
							$(".datagrid-editable-input").height(22);
							editType = 'edit';
						}
					} else {   //没有选择修改项
						$.messager.show({
							msg : '请选择一项进行修改！',
							title : '提示'
						});
					}
				}
			}, '-', {
				text : '保存',
				iconCls : 'icon-save',
				handler : function() {
					//opt.save('menu');
					if (editRow != undefined) {
						treegrid.treegrid('endEdit', editRow.id);
						//treegrid.treegrid('reload');
					}
				}
			}, '-', {
				text : '取消编辑',
				iconCls : 'icon-undo',
				handler : function() {
					if (editRow) {
						treegrid.treegrid('cancelEdit', editRow.id);
						var p = treegrid.treegrid('getParent', editRow.id);
						if (p) {
							treegrid.treegrid('reload', editRow.id);
						} else {
							treegrid.treegrid('reload');
						}
						editRow = undefined;
						editType = undefined;
					}
				}
			}, '-', {
				text : '取消选中',
				iconCls : 'icon-undo',
				handler : function() {
					treegrid.treegrid('unselectAll');
				}
			}, '-' ],
			title : '菜单列表',
			fit : true,
			//fitColumns : false,
			//nowrap : true,
			animate: true,  
			border : false,
			idField:'id',    
			treeField : 'menuName',
			columns : [ [ 
			    { field:'id',title:'id',width:300,hidden:true},
			    {
				field : 'menuName',
				title : '菜单名称',
				width : 150,
				editor : {
					type : 'validatebox',
					options : {
						required : true
					}
				  }
			    },
			    {
				field : 'isLeafMenu',
				title : '是否子叶节点',
				align:"center",
				formatter: imgcheckleafbox,
				width : 130,
				editor : {
					type : 'combobox',
					options : {
						panelHeight: "auto",
						valueField : 'value',
						textField : 'label',
						data : [{
						    "value":1,
						    "label":"是",
						    "selected":true
						},{
						    "value":0,
						    "label":"否"
						}]
					}
				}
			}, {
				field : 'menuURL',
				title : '菜单地址',
				width : 250,
				editor : {
					type : 'text'
				}
			}, {
				field : 'enabled',
				title : '是否可用',
				width : 100,
				formatter: imgcheckbox,
				align:'center',
				editor : {
					type : 'combobox',
					options : {
						valueField : 'value',
						textField : 'label',
						panelHeight: "auto",
						data : [{
						    "value":1,
						    "label":"可用",
						    "selected":true
						},{
						    "value":0,
						    "label":"禁用"
						}]
					}
				}
			},{
				field : 'menuSort',
				title : '排序(整数)',
				width : 100,
				align:'center',
				editor : {
					type : 'numberbox',
					options : {
						 min:0,
						 max:100
					}
				}
			}, {
				field : 'parCode',
				title : '上级菜单',
				width : 150,
				formatter : function(value, rowData, rowIndex) {
					return rowData.pmenuName;
				},
				editor : {
					type : 'combotree',
					options : {
						url : 'menu/menuparentTree',
						animate : false,
						lines : !sy.isLessThanIe8(),
						onLoadSuccess : function(row, data) {
							var t = $(this);
							if (data) {
								$(data).each(function(index, d) {
									if (this.state == 'closed') {
										t.tree('expandAll');
									}
								});
							}
						}
					}
				}
			}, {
				field : 'pmenuName',
				title : '上级菜单',
				width : 150,
				hidden : true
			} 
			, /*{   
				field : 'optShow',
				title : '预览tab和按钮',
				width : 100,
				align:"center",
				formatter: function(value, row, index) {  
                    var content = "";
                    if(value==""){
                        content = "";
                    }else{
                        var abValue = "<span style='color:#00C;'>查看</span>";  
                        content = "<div href='javascript:;' style='cursor:pointer' title='" + value + "' class='note'>" + abValue + "</div>";  
                    }
                    return content;  
                   }  
			    }
			, {    
				field : 'optUrl',
				title : '操作',
				width : 170,
				align:"center"
			}*/] ]
			,
			onLoadSuccess : function(row, data) {  //加载展开
			     $(".note").tooltip(  
                    {
                    position: 'left',  
                    onShow: function(){  
                        $(this).tooltip('tip').css({   
                            boxShadow: '1px 1px 3px #292929'                          
                        });  
                    }  
                }  
              ); 		
			 }
			 ,
			 onDblClickRow : function(row) {  //双击编辑
				if (editRow != undefined) {
					treegrid.treegrid('endEdit', editRow.id);
				}

				if (editRow == undefined) {
					treegrid.treegrid('beginEdit', row.id);
					editRow = row;
					editType = 'edit';
					$(".datagrid-editable-input").height(22);
					treegrid.treegrid('unselectAll');
				}
			},
			onAfterEdit : function(row, changes) {
				if (editType == undefined) {
					editRow = undefined;
					treegrid.treegrid('unselectAll');
					return;
				}
				var url = '';
				if (editType == 'add') {
					url = 'menu/addMenuInfo';
				}
				if (editType == 'edit') {
					url = 'menu/editMenuInfo';
				}

				$.ajax({
					url : url,
					data : row,
					//dataType : 'json',
					success : function(data) {
					   if(data!=''){
						   var obj = eval('('+data+')');
	                       if (obj.result==1){
								$.messager.show({
									msg : obj.errorMsg,
									title : '提示'
								});
								editRow = undefined;
								editType = undefined;
								treegrid.treegrid('reload');
							} else {
								treegrid.treegrid('beginEdit', editRow.id);
								$.messager.show({
									msg : obj.errorMsg,
									title : '提示'
								});
							}
					   }
					   treegrid.treegrid('unselectAll');
					}
				});

			},
			onContextMenu : function(e, row) {
				e.preventDefault();
				$(this).treegrid('unselectAll');
				$(this).treegrid('select', row.id);
				$('#menu').menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			},onLoadSuccess : function(row, data) {  //加载展开
			     $(".note").tooltip(  
                    {
                    position: 'left',  
                    onShow: function(){  
                        $(this).tooltip('tip').css({   
                            boxShadow: '1px 1px 3px #292929'                          
                        });  
                    }  
                }  
                ); 
				var t = $(this);
				if (data) {
					$(data).each(function(index, d) {
						if (this.state == 'closed') {
							t.treegrid('expandAll');
						}
					});
				}
			},
			onExpand : function(row) {
				treegrid.treegrid('unselectAll');
			},
			onCollapse : function(row) {
				treegrid.treegrid('unselectAll');
			}
		});

	});

    //设置选项卡
    function giveTabsToMenu(menuCode){
       $("#check_allTab").removeAttr("checked");
	   $('#addTabId').dialog('open').dialog('setTitle','设置选项卡');
	   $("#menuToTabId").html(menuCode);
	   //加载按钮
	   $.ajax({
			url : 'tab!queryAllTabToMenu',
			data : {menuCode:menuCode},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
					var tabHtml = "";
					for ( var i = 0; i < obj.datas.length; i++) {
						if(obj.datas[i].sign==1){
						   tabHtml = tabHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='tabChe' checked='checked' value='"+obj.datas[i].id+"'/>"+obj.datas[i].tabName+"</a>";
						}else{
						   tabHtml = tabHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='tabChe' value='"+obj.datas[i].id+"'/>"+obj.datas[i].tabName+"</a>";
						}
						
				        if(i%4==0&&i!=0){
				           tabHtml = tabHtml+ "<br/><br/>"
				        }
				    }
				    
				    $("#tabsDiv").html(tabHtml);
				    editRow = undefined;
				} else {
					$.messager.show({
					msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		});
    }

	//设置按钮
	function giveButtonToMenu(menuCode){
	   $("#check_allBut").removeAttr("checked");
	   $('#addButId').dialog('open').dialog('setTitle','设置按钮');
	   $("#menuToButId").html(menuCode);
	   //加载按钮
	   $.ajax({
			url : 'button/queryAllButtonToMenu',
			data : {menuCode:menuCode},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
					var butHtml = "";
					for ( var i = 0; i < obj.datas.length; i++) {
						if(obj.datas[i].sign==1){
						   butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='butChe' checked='checked' value='"+obj.datas[i].buttonId+"'/>"+obj.datas[i].buttonName+"</a>";
						}else{
						   butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='butChe' value='"+obj.datas[i].buttonId+"'/>"+obj.datas[i].buttonName+"</a>";
						}
						
				        if(i%4==0&&i!=0){
				           butHtml = butHtml+ "<br/><br/>"
				        }
				    }
				    
				    $("#buttonsDiv").html(butHtml);
				    editRow = undefined;
				} else {
					$.messager.show({
					msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		});
	}
	
	
	//设置菜单按键
	function saveButToMenu(){
	    var menuCode = $("#menuToButId").html();
	    var str = $("input[type='checkbox']");
		var objarray = str.length;
		var strBuffer = "";
		for (i = 0; i < objarray; i++) {
			if (str[i].checked == true) {
					modelid = str[i].value;
					if(modelid!="on"){
						strBuffer += modelid;
						strBuffer += ",";
					}
		    }
		}
		
		    $.ajax({
			url : 'menu/saveButToMenu',
			data : {
				strBuffer:strBuffer,
				menuId:menuCode
			},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
				    $.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				    $('#addButId').dialog('close'); 
				} else {
					$.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		  });
		
	}
	
	//保存选项卡、菜单
	function saveTabToMenu(){
	    var menuCode = $("#menuToTabId").html();
	    var str = $(".tabChe");
		var objarray = str.length;
		var strBuffer = "";
		for (i = 0; i < objarray; i++) {
			if (str[i].checked == true) {
					modelid = str[i].value;
					if(modelid!='on')
					strBuffer += modelid;
					strBuffer += ",";
		    }
		}
		
		    $.ajax({
			url : 'menu/saveTabToMenu',
			data : {
				strBuffer:strBuffer,
				menuId:menuCode
			},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
				    $.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				    $('#addTabId').dialog('close'); 
				} else {
					$.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		  });
	}
	
	//是否可用图标显示
	var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="/images/icon/can.png" alt="正常" title="正常" />' : '<img src="/images/icon/not.png" alt="禁用" title="禁用" />';  
    };
    
    //是否叶子点
    var imgcheckleafbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="/images/icon/leaf.png" alt="叶子" title="叶子" />' : '<img src="/images/icon/root.png" alt="树根" title="树根" />';  
    };
    
    
    $(function(){
       $("#check_allBut").click(function(){
	        if(this.checked){ 
				$(".butChe").each(function(){this.checked=true;}); 
			}else{ 
				$(".butChe").each(function(){this.checked=false;}); 
			} 
        });
        
        
        $("#check_allTab").click(function(){
	        if(this.checked){ 
				$(".tabChe").each(function(){this.checked=true;}); 
			}else{ 
				$(".tabChe").each(function(){this.checked=false;}); 
			} 
        });
    });
	
</script>
<style type="text/css">
.buts{
   cursor:hand;
}

.datagrid-editable-input{
   height:22px;
}

input[type="text"]{
height:22px;
}
</style>
</head>
<body class="easyui-layout" fit="true">
	<!-- 菜单列表 -->
	<div region="center" border="false" style="overflow: hidden;">
		<table id="treegrid" ></table>
	</div>
      
    <!-- 隐藏按键 -->  
	<div id="menu" class="easyui-menu" style="width:120px;display: none;">
		<div onclick="append();" iconCls="icon-add">增加</div>
		<div onclick="remove();" iconCls="icon-remove">删除</div>
		<div onclick="edit();" iconCls="icon-edit">编辑</div>
	</div>
	
	<!-- 设置按钮 -->
    <div id="addButId" class="easyui-dialog"
			style="width: 500px; height: 400px;  padding: 5px 5px;"
			closed="true" data-options="modal:true,buttons: [{
                    text:'保存',
                    handler:function(){
                       saveButToMenu();
                    }
                },{
                    text:'取消',
                    handler:function(){
                       $('#addButId').dialog('close');
                    }
                }]">
			<form method="post" id="fmbut" >
				<span id="menuToButId" style="display:none;"></span>
				<div><p><img src="/images/icon/duoxuan.png"/><span style="font-size:16px;font-weight:700;">&nbsp;请选择页面按钮</span></p></div>
				<div><hr/></div>
				<div><input type="checkbox" id="check_allBut"/><span style="font-size:16px;" >全选/反选</span></div>
				<br/>
				<div id="buttonsDiv" class="Cbox" style="width:95%"></div>
			</form>
		</div>
		
		
	<!-- 设置选项卡 -->
    <div id="addTabId" class="easyui-dialog"
			style="width: 500px; height: 400px;  padding: 5px 5px;"
			closed="true" data-options="modal:true,buttons: [{
                    text:'保存',
                    handler:function(){
                       saveTabToMenu();
                    }
                },{
                    text:'取消',
                    handler:function(){
                       $('#addTabId').dialog('close');
                    }
                }]">
			<form method="post" id="fmTab" >
				<span id="menuToTabId" style="display:none;"></span>
				<div><p><img src="/images/icon/duoxuan.png"/><span style="font-size:16px;font-weight:700;">&nbsp;请选择页面选项卡</span></p></div>
				<div><hr/></div>
				<div><input type="checkbox" id="check_allTab"/><span style="font-size:16px;" >全选/反选</span></div>
				<br/>
				<div id="tabsDiv" class="Cbox" style="width:95%"></div>
			</form>
		</div>
</body>
</html>
