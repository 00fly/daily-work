<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>邮件</title>  
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css"	href="js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"	href="js/easyUI/themes/icon.css">
	<link rel="stylesheet" type="text/css"  href="css/mast.css">
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="js/syUtil.js"></script>
	<script type="text/javascript">	
	$(function(){
	    searchForm = $('#searchFormSD').form();
	    $('#workSendGroupGrid').datagrid({ 
	        title:'发送组列表', 
	        url:"workSendGroup/queryWorkSendGroupByPage",
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:10,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'id',title:'编码',width:200,hidden:'true'},
				{field:'groupName',title:'名称',width:150,align:'center'},
				{field:'senderIds',title:'接收者',width:500,align:'center'},
				{field:'workerIds',title:'员工',width:80,align:'center',
				 formatter: function(value, row, index) {  
                    var abValue = "<span style='color:#00C;'>查看</span>";  
                    var content = "<div href='javascript:;' style='cursor:pointer' title='" + value + "' class='note'>" + abValue + "</div>";  
                    return content;  
                   }  
				},
				{field:'creatTime',title:'创建时间',width:100,align:'center'},
				{field:'memo',title:'备注',width:150,align:'center'},
				{field:'enabled',title:'操作',width:80,align:'center'}
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	                $('#addWorkSendGroupId').show();
					$('#wsgfrm').form('clear');
					$("#titleId").html("添加发送组");
					$('#tabId').val(0);
					$('#addWorkSendGroupId').dialog({
						title: '添加发送组',
						width: 430,
						height: 250,
						padding: 5,
						modal: true,
						buttons: [{
							text: '提交',
							handler: function() {
							 saveWorkSendGroup();
						}
						},
						{
							text: '取消',
							handler: function() {
							$('#addWorkSendGroupId').dialog('close');
						}
						}]
					});
					$("#isAbleId").attr("checked","checked");
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	               var row = $('#workSendGroupGrid').datagrid('getSelected');
					if (row){
						$('#addWorkSendGroupId').show();
						$('#wsgfrm').form('clear');
						$('#seId').val(row.id);
						$('#addWorkSendGroupId').dialog({
							title: '修改发送组',
							width: 430,
						    height: 250,
							padding: 5,
							modal: true,
							buttons: [{
								text: '提交',
								handler: function() {
								saveWorkSendGroup();
							}
							},
							{
								text: '取消',
								handler: function() {
								$('#addWorkSendGroupId').dialog('close');
							}
							}]
						});
						
						$("#seId").val(row.id);
						$("#groupNameId").val(row.groupName);
						$("#memoId").val(row.memo);
						if(row.enabled==1){
							$("#isAbleId").attr("checked","checked");
						}else{
							$("#noAbleId").attr("checked","checked");
						}
					}else{
						$.messager.alert('提示','请选择你要修改的发送组!','warning');
					}
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	                var row = $('#workSendGroupGrid').datagrid('getSelected');
					if (row){
						$.messager.confirm('提示','你确定删除【'+row.groupName+'】?',function(r){
							if (r){
								$.ajax({
									type: "POST",
									url: "workSendGroup/deleteWorkSendGroup",
									data: {id:row.id},
									success: function(data){
										var obj = eval('('+data+')');
										if (obj.result==1){
											$('#workSendGroupGrid').datagrid("reload"); 
											$.messager.show({    // show error message
												title: '提示',
												msg: obj.errorMsg
											}); 
										} else if(obj.result==2){
											$.messager.show({    // show error message
												title: '提示',
												msg: obj.errorMsg
											});
										}else{
											$.messager.show({    // show error message
												title: '提示',
												msg: "请检查网络是否链接正常!"
											});
										}
									}
								});
							}
						});
					}else{
						$.messager.alert('提示','请选择你要删除的发送组!','info');
					}
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				   $('#workSendGroupGrid').datagrid("reload"); 
				}
			},'-',{
				iconCls: 'icon-addEmail',
				text:'设置接收者',
				handler: function(){
				   jieShouZhe(); 
				}
			},'-',{
				iconCls: 'icon-addEmail',
				text:'设置员工',
				handler: function(){
				   yuanGong(); 
				}
			}],
			onLoadSuccess:function(data)  
             {  
                $(".note").tooltip(  
                    {
                    position: 'left',  
                    onShow: function(){  
                        $(this).tooltip('tip').css({   
                            //width:'300',  
                            boxShadow: '1px 1px 3px #292929'                          
                        });  
                    }  
                }  
                );  
             }  
	    });
	});
	
	//是否可用图标显示
	var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
    }; 
    
    
    //设置接收者
    function jieShouZhe(){
    	var row = $('#workSendGroupGrid').datagrid('getSelected');
		if (row){
		    $("#jszId").html(row.id);
			$('#jieShouZheId').show();
			$('#jieShouZheId').dialog({ 
				title:"设置接收者", 
				width:$(window).width() * 0.9,
				height:$(window).height() * 0.95,
				padding:5,
				modal:true,
				buttons: [{ 
					text: '提交', 
					handler: function() { 
					saveJieShouZhe();
				} 
				}, { 
					text: '取消', 
					handler: function() { 
					$('#jieShouZheId').dialog('close'); 
				} 
				}] 
			}); 
	
			//获取部门树
			$('#jieShouZheTreeGrid').treegrid({
				url : 'user/queryOrgsAndCodeUserTreeGrid?parCode=1&jid='+row.id,
				fit : true,
				animate:true,
				border : false,
				idField:'id',    
				treeField : 'orgName',
				onBeforeExpand:function(node,param){                         
				$('#jieShouZheTreeGrid').treegrid('options').url = "user/queryOrgsAndCodeUserTreeGrid?parCode="+node.id+"&jid="+row.id;  
			}, 
			onLoadSuccess : function(row, data) {  //加载展开
				var t = $(this);
				if (data) {
					$(data).each(function(index, d) {
						if (this.state == 'closed') {
							t.treegrid('expandAll');
						}
					});
				}
			}
			});
		}else{
		    $.messager.show({
				msg :  "请选择设置的发送组",
				title : '提示'
			});
		}
    }
    
    
    //设置接收者
    function yuanGong(){
    	var row = $('#workSendGroupGrid').datagrid('getSelected');
		if (row){
		    $("#ygId").html(row.id);
			$('#yuanGongId').show();
			$('#yuanGongId').dialog({ 
				title:"设置接收者", 
				width:$(window).width() * 0.9,
				height:$(window).height() * 0.95,
				padding:5,
				modal:true,
				buttons: [{ 
					text: '提交', 
					handler: function() { 
					saveYuanGong();
				} 
				}, { 
					text: '取消', 
					handler: function() { 
					$('#yuanGongId').dialog('close'); 
				} 
				}] 
			}); 
	
			//获取部门树
			$('#yuanGongGrid').treegrid({
				url : 'user/queryOrgsAndCodeYuanGongTreeGrid?parCode=1&jid='+row.id,
				fit : true,
				animate:true,
				border : false,
				idField:'id',    
				treeField : 'orgName',
				onBeforeExpand:function(node,param){                         
				$('#yuanGongGrid').treegrid('options').url = "user/queryOrgsAndCodeYuanGongTreeGrid?parCode="+node.id+"&jid="+row.id;  
			}, 
			onLoadSuccess : function(row, data) {  //加载展开
				var t = $(this);
				if (data) {
					$(data).each(function(index, d) {
						if (this.state == 'closed') {
							t.treegrid('expandAll');
						}
					});
				}
			}
			});
		}else{
		    $.messager.show({
				msg :  "请选择设置的发送组",
				title : '提示'
			});
		}
    }
    
    
    //保存接收者
    function saveJieShouZhe(){
     	var checks = $(".coreUseres");
	    var strBuffer = "";
		var objarray = checks.length;
		var strBuffer = "";
		for (i = 0; i < objarray; i++) {
			if (checks[i].checked == true) {
				modelid = checks[i].value;
				strBuffer += modelid;
				strBuffer += ",";
			}
		}
	   var jsz = $("#jszId").html();
	   //提交数据
	   $.ajax({
				url : 'user/saveJieShouZhe',
				data : {
					coreUserBuffer:strBuffer,
					jsz:jsz
				},
				success : function(data) {
					var obj = eval('('+data+')');
	                if (obj.result==1){
					    $.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					    $('#jieShouZheId').dialog('close'); 
					     $('#workSendGroupGrid').datagrid("reload");
					} else if(obj.result==2){
					 	$.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					}else {
						$.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					}
				}
			  });
    }
    
    
    //保存员工
    function saveYuanGong(){
	    	var checks = $(".yuanGongCla");
		    var strBuffer = "";
			var objarray = checks.length;
			var strBuffer = "";
			for (i = 0; i < objarray; i++) {
				if (checks[i].checked == true) {
					modelid = checks[i].value;
					strBuffer += modelid;
					strBuffer += ",";
				}
			}
		   var jsz = $("#ygId").html();
		   
		   //提交数据
		   $.ajax({
				url : 'user/saveYuanGong',
				data : {
					coreUserBuffer:strBuffer,
					jsz:jsz
				},
				success : function(data) {
					var obj = eval('('+data+')');
	                if (obj.result==1){
					    $.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					    $('#yuanGongId').dialog('close'); 
					     $('#workSendGroupGrid').datagrid("reload");
					} else if(obj.result==2){
					 	$.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					}else {
						$.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					}
				}
			  });
    }
    
    
    
    //保存发送组
    function saveWorkSendGroup(){
    	var groupName = $('#groupNameId').val();
    	//var enabled = $("input[name='enabled']:checked").val(); 
    	if(groupName==""){
    	    $.messager.show({
				title: '提示',
				msg: "发送组名称不能为空!"
			});			
			return false;
    	}
    	
    	//保存操作
			$.ajax({
					type: "POST",
					url: "workSendGroup/saveWorkSendGroup",
					data: {
						id:$("#seId").val(),
						groupName:groupName,  //名称
						memo:$("#memoId").val()//,  //备注
						//enabled:enabled
					},
					success: function(data){
						var obj = eval('('+data+')');
						if (obj.result==1){
							$('#workSendGroupGrid').datagrid("reload"); 
							$('#addWorkSendGroupId').dialog('close');
							$.messager.show({    // show error message
								title: '提示',
								msg: obj.errorMsg
							}); 
						} else if(obj.result==2){
							$.messager.show({    // show error message
								title: '提示',
								msg: obj.errorMsg
							});
						}else {
							$.messager.show({    // show error message
								title: '提示',
								msg: "请检查网络是否链接正常!"
							});
						}
					}
				});    
    }
    
    
    //设置启用或者禁用发送组
    function setEnabled(status,id,groupName,JSZ,YG){
        if(JSZ=='null'){
            $.messager.show({    // show error message
				title: '提示',
				msg: "请设置接收者,再启用发送组!"
			});
			
			return false;
        }
        
         if(YG=='null'){
            $.messager.show({    // show error message
				title: '提示',
				msg: "请设置员工,再启用发送组!"
			});
			return false;
        }
        
        var title = "";
        if(status==1){
           title = "启用";
        }else{
           title = "禁用";
        }
        $.messager.confirm('提示','你确定'+title+'【'+groupName+'】?',function(r){
		if (r){
			$.ajax({
				type: "POST",
				url: "workSendGroup/enabledWorkSendGroup",
				data: {id:id,status:status},
				success: function(data){
					var obj = eval('('+data+')');
					if (obj.result==1){
						$('#workSendGroupGrid').datagrid("reload"); 
						$.messager.show({    // show error message
							title: '提示',
							msg: obj.errorMsg
						}); 
					} else if(obj.result==2){
						$.messager.show({    // show error message
							title: '提示',
							msg: obj.errorMsg
						});
					}else{
						$.messager.show({    // show error message
							title: '提示',
							msg: "请检查网络是否链接正常!"
						});
					}
				}
			});
		}
	});
    }
    
    $(function(){
         $("#check_group").click(function(){
        	if(this.checked){ 
				$(".groupCk").each(function(){this.checked=true;}); 
			}else{ 
				$(".groupCk").each(function(){this.checked=false;}); 
			} 
	    });	    
    });
    
    
    //检索
    function searchUser(){
        $('#workSendGroupGrid').datagrid('load', sy.serializeObject(searchForm));
    }
    //重置
    function crearUser(){
        searchForm.find("input[type!='button']").val('');
    }
    
        //全选
    function allSelect(cellvalue, options, rowObject){
        return "<input onclick=selectAlls('"+options.id+"') val='1' id="+options.id+" type='checkbox'>";
    }
    
    //全部选择项
    function selectAlls(orgCode){
        var fVal = $("#"+orgCode).val();
        if(fVal){
           $(".but"+orgCode).each(function(){this.checked=true;}); 
           $("#"+orgCode).val(0)
        }
        
        if(fVal==0){
           $(".but"+orgCode).each(function(){this.checked=false;}); 
           $("#"+orgCode).val(1)
        }
    }    
	</script>

    <style type="text/css">
    .form_label{
    text-align:right;
    }
    </style>
  
  <body class="easyui-layout">
        <div data-options="region:'north',split:true,border:false,title:'查询栏'" style="height:95px;background-color:#F0F0F0;">
                 <fieldset>
			  <legend>查询</legend>
			  <form method="post" id="searchFormSD">  
			        组名称: <input type="text" name="groupName"  style="width:155px;"/>&nbsp;
			    <input type="button" onclick="searchUser()" value='检索'></input>
				<input type="button" onclick="crearUser()" value='清空'></input>
			  </form>
			</fieldset>
        </div>
        <div region="center" border="false">
			<table id="workSendGroupGrid">
			</table>
		</div>
	
         <!-- 添加发送组界面 -->
  			<div id="addWorkSendGroupId" >
			<center>
			<form method="post" id="wsgfrm" >
				<br/>
				<br/>
				<input name="id" id="seId" style="display:none;">
				<br/>
				<br/>
				<table width="300px" align="center" style="align: center;" cellpadding="7" border="0">
					<tr>
						<td class="form_label" style="width:150xp;">组名：</td>
						<td class="form_content" ><input type="text" name="groupName" id="groupNameId" class="easyui-validatebox" data-options="required:true" maxlength="10"></input></td>
					</tr>
					<!-- 
					<tr>
						<td class="form_label" style="width:150xp;">是否可用：</td>
						<td class="form_content" >
							<input type="radio" name="enabled" value="1" id="isAbleId"/>可用&nbsp;&nbsp;
                        	<input type="radio" name="enabled" value="0" id="noAbleId"/>禁用&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
					 -->
					<tr>
						<td class="form_label">备注：</td>
						<td class="form_content" >
						<input type="text" name="memo" id="memoId">
						</td>
					</tr>
				</table>
			</form>
			</center>
		</div>
		
		<!-- 设置接收者 -->
		<div id="jieShouZheId" style="display:none;">
		     <span style="display:none;" id="jszId"></span>
		     <div class="easyui-layout" data-options="fit:true">
	                  <div title="设置接收者" style="padding:0px;height:98%;">
	                      <table id="jieShouZheTreeGrid" >
	                          <thead>
	                         <tr>
	                           <th data-options="field:'id'" width="20" hidden="true">id</th>
	                           <th data-options="field:'orgName'" width="200" align="left">部门名称</th>
	                           <th data-options="field:'aselect',formatter:allSelect" width="50"align="center">全选</th>
	                           <th data-options="field:'selectCodeUserOpt'" align="left">用户选项</th>
	                         </tr>
	                         </thead>
	                      </table>
	                  </div>
		     </div>
		</div>		
		
		<!-- 设置员工 -->
		<div id="yuanGongId" style="display:none;">
		     <span style="display:none;" id="ygId"></span>
		     <div class="easyui-layout" data-options="fit:true">
	                  <div title="设置员工" style="padding:0px;height:98%;">
	                      <table id="yuanGongGrid" >
	                          <thead>
	                         <tr>
	                           <th data-options="field:'id'" width="20" hidden="true">id</th>
	                           <th data-options="field:'orgName'" width="200" align="left">部门名称</th>
	                           <th data-options="field:'aselect',formatter:allSelect" width="50"align="center">全选</th>
	                           <th data-options="field:'selectYuanGongOpt'" align="left">用户选项</th>
	                         </tr>
	                         </thead>
	                      </table>
	                  </div>
		     </div>
		</div>		
  </body>
</html>
