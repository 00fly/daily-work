<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>My JSP 'buttonList.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css"
			href="js/easyUI/themes/icon.css">
	<script type="text/javascript"
			src="js/easyUI/jquery.easyui.min.js"></script>
	<script type="text/javascript"
			src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
	<link rel="stylesheet" type="text/css"
			href="css/mast.css">
	<script type="text/javascript">
	$(function(){
	    $('#butgrid').datagrid({ 
	        title:'按钮列表', 
	        url:"button/queryButtonListByPage",
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:10,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'buttonId',title:'编码',width:200,hidden:'true'},
				{field:'buttonName',title:'名称',width:200,align:'center'},
				{field:'enabled',title:'是否可用',width:100,align:'center', formatter: imgcheckbox },
				{field:'buttonSort',title:'排序',width:80,align:'center'},
				{field:'methodName',title:'按键备注',width:250,align:'center'}
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	              	$('#addButId').dialog('open').dialog('setTitle','添加');
					$("#titleId").html("添加按钮");
					$('#fmbut').form('clear');
					$("#isAbleId").attr("checked","checked");
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	               	var row = $('#butgrid').datagrid('getSelected');
					if (row){
						$('#addButId').dialog('open').dialog('setTitle','修改');
						$("#titleId").html("修改按钮");
						url = 'button/loadButtonById?buttonId='+row.buttonId;
						$('#fmbut').form('load',url);
					}else{
						$.messager.alert('提示','请选择你要删除的按钮!','warning');
					}
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	               	var row = $('#butgrid').datagrid('getSelected');
					if (row){
						$.messager.confirm('提示','你确定删除【'+row.buttonName+'】?',function(r){
							if (r){
								$.ajax({
									type: "POST",
									url: "button/deleteButton",
									data: {buttonId:row.buttonId},
									success: function(data){
										var obj = eval('('+data+')');
										if (obj.result==1){
											$('#butgrid').datagrid("reload"); 
											$.messager.show({    // show error message
												title: '提示',
												msg: obj.errorMsg
											}); 
										} else {
											$.messager.show({    // show error message
												title: '提示',
												msg: obj.errorMsg
											});
										}
									}
								});
							}
						});
					}else{
						$.messager.alert('提示','请选择你要删除的按钮!','warning');
					}
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				  $('#butgrid').datagrid("reload");
				}
			}]
	    });
	});
	
	//保存按键
	function saveButton(){
	        url = "button/saveButton"
            $('#fmbut').form('submit',{
                url: url,
                onSubmit: function(){
                    return $(this).form('validate');
                },
                success: function(data){
                    var obj = eval('('+data+')');
                    if (obj.result==2){
                        $.messager.show({
                            title: '提示',
                            msg: obj.errorMsg
                        });
                    } else {
                        $('#addButId').dialog('close');        // close the dialog
                        $('#butgrid').datagrid("reload");    // reload the user data
                    }
                }
            });
	}
	
	
	//图标显示
	var imgShowbox = function (cellvalue, options, rowObject){
	    return "<img src='${pageContext.request.contextPath}"+options.buttonIconImg+"' alt='"+options.buttonName+"' title='"+options.buttonName+"' style='width:16px;height:16px;'/>";
	} 
	//是否可用图标显示
	var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
    };  
	</script>

    <style type="text/css">
    .form_label{
    text-align:right;
    }
    </style>
  </head>
  
  <body class="easyui-layout">
        <div region="center" border="false">
			<table id="butgrid">
			</table>
		</div>
		
  <!-- 添加用户界面 -->
  <div id="addButId" class="easyui-dialog"
			style="width: 500px; height: 350px; text-align: center; padding: 10px 20px"
			closed="true"  data-options="modal:true">
			<center>
			<form method="post" id="fmbut"  enctype="multipart/form-data">
				<h2 id="titleId"></h2>
				<input name="buttonId" style="display:none;">
				<table width="300px" align="center" style="align: center;" cellpadding="7" border="0">
					<tr>
						<td class="form_label" style="width:150xp;">按钮名称：</td>
						<td class="form_content" ><input type="text" name="buttonName" class="easyui-validatebox" data-options="required:true" maxlength="32"></input></td>
					</tr>
					<tr>
						<td class="form_label">是否可用：</td>
						<td class="form_content" >
						<input type="radio" name="enabled" value="1" id="isAbleId"/>可用&nbsp;&nbsp;
                        <input type="radio" name="enabled" value="0" id="noAbleId"/>禁用&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td class="form_label">按键排序：</td>
						<td class="form_content" >
								<input name="buttonSort" id="buttonSort" type="text" class="easyui-numberbox" mix="1" max="100" />
						</td>
					</tr>
					<tr>
						<td class="form_label">按键备注：</td>
						<td class="form_content" >
								<input name="methodName" id="methodName" type="text" />
						</td>
					</tr>
					<tr>
						<td class="form_label" colspan="2" style="text-align: center">
							<a href="javascript:void(0)" onclick="saveButton()" class="easyui-linkbutton">保存</a>
							<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#addButId').dialog('close')">取消</a>
						</td>
					</tr>
				</table>
			</form>
			</center>
		</div>
	<!-- end添加用户界面 -->
  </body>
</html>
