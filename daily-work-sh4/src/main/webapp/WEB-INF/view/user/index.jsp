<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jstl/core_rt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>日报系统 v1.0</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" type="text/css" href="js/easyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="js/easyUI/themes/icon.css">
<link rel="stylesheet" type="text/css" href="css/mast.css">
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="js/easyUI/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
    function keepSession() {
		$.get("${ctx}/user/keepSession?d="+Math.random());
	}
    setInterval("keepSession()",600000);
     
	//利用轮询的方式提醒
	//setInterval('showAlertInfos()',10000); 
	
	//提醒执行方法
	function showAlertInfos() {
	    var maxLen = $("#alertAialog").parent().html().length;
	    if(maxLen>10000){
	            $.ajax( {
				type : "POST",
				url : "util!showAlertInfos",
				data : {
					//id : row.id
				},
				success : function(data) {
					var obj = eval('(' + data + ')');
					if (obj.result == 1) {
						 //显示提示框
						 $('#alertAialog').show(); 
	                     $('#alertAialog').dialog({ 
	                            title:"提示",
	                            width: 300,
	    					    height: 150,
	    					    left:($(window).width()-300),
	    					    top:($(window).height()-200),
	    					       onBeforeClose:function(){ 
							           //关闭提示框（不再提醒）
							            $.ajax( {
										type : "POST",
										url : "util!closeAlertInfos",
										data : {
											optType : $("#optTypeId").html()
										},
										success : function(data) {
											var obj = eval('(' + data + ')');
												if (obj.result == 1) {
												}
											}
										});
							       }
	                     });
	                     
	                     //操作类型
					     $("#optTypeId").html(obj.optType);
					     //提示信息
					     $("#remindMsgId").html(obj.remindMsg);
					     //设置查看
					     $("#seeTabId").attr('onclick',obj.urlStr);
					}
				}
			});
	    }else{
	        var isopen = $("#alertAialog").parent().css('display');
			if(isopen=='block'){
			
			
			}else{
			    $.ajax( {
				type : "POST",
				url : "util!showAlertInfos",
				data : {
					//id : row.id
				},
				success : function(data) {
						var obj = eval('(' + data + ')');
						if (obj.result == 1) {
						     //显示提示框
							 $('#alertAialog').show(); 
		                     $('#alertAialog').dialog({ 
		                            title:"提示",
		                            width: 300,
		    					    height: 150,
		    					    left:($(window).width()-300),
	    					        top:($(window).height()-200),
	    					        onBeforeClose:function(){
	    					            //关闭提示框（不再提醒）
							            $.ajax( {
										type : "POST",
										url : "util!closeAlertInfos",
										data : {
											optType : $("#optTypeId").html()
										},
										success : function(data) {
											var obj = eval('(' + data + ')');
												if (obj.result == 1) {
												}
											}
										});
							        }
		                     });
		                     
		                     //操作类型
					         $("#optTypeId").html(obj.optType);
					         //提示信息
					         $("#remindMsgId").html(obj.remindMsg);
					         //设置查看
					         $("#seeTabId").attr('onclick',obj.urlStr);
						  }
						}
					});
			}
	    }
	    
	}

	var centerTabs;
	var tabsMenu;

	$(function() {
		//Tabs菜单
		tabsMenu = $('#tabsMenu').menu(
				{
					onClick : function(item) {
						var curTabTitle = $(this).data('tabTitle');
						var type = $(item.target).attr('type');
						if (type === 'refresh') {
							refreshTab(curTabTitle);
							return;
						}

						if (type === 'close') {
							var t = centerTabs.tabs('getTab', curTabTitle);
							if (t.panel('options').closable) {
								centerTabs.tabs('close', curTabTitle);
							}
							return;
						}

						var allTabs = centerTabs.tabs('tabs');
						var closeTabsTitle = [];

						$.each(allTabs, function() {
							var opt = $(this).panel('options');
							if (opt.closable && opt.title != curTabTitle
									&& type === 'closeOther') {
								closeTabsTitle.push(opt.title);
							} else if (opt.closable && type === 'closeAll') {
								closeTabsTitle.push(opt.title);
							}
						});

						for ( var i = 0; i < closeTabsTitle.length; i++) {
							centerTabs.tabs('close', closeTabsTitle[i]);
						}
					}
				});
 
		$('.treeMenu tree-title').click(function() {
			var _tit = $(this).html(), _contant = $(this).attr(
					"href"), _tabCls = $(this).attr("tabCls");
			$('.demo-menu a').removeClass("icon-on");
			
			$(this).addClass("icon-on");
			addTab(_tit, _contant, _tabCls,0);
			return false;
		});

		centerTabs = $('#tabs').tabs( {
			fit : true,
			border : false,
			onContextMenu : function(e, title) {
				e.preventDefault();
				tabsMenu.menu('show', {
					left : e.pageX,
					top : e.pageY
				}).data('tabTitle', title);
			}
		});  
	});
	
	
	//点击菜单显示tab
	function addTab(title, href, tabCls,type) {
	    //关闭提示框
	    if(type==1){
	       $("#alertAialog").dialog('close');
	    }
            
		if (centerTabs.tabs('exists', title)) {
			centerTabs.tabs('select', title);
		} else {
			if (href) {
				var content = '<iframe scrolling="yes" frameborder="0"  src="' + href + '" style="width:100%;height:100%;">__tag_134$51_';
			} else {
				var content = '没有可以加载的页面!';
			}
			centerTabs.tabs( {
				scrollIncrement : 100
			}).tabs('add', {
				title : title,
				content : content,
				closable : true,
				iconCls : tabCls
			});
		}
	}

	//设置菜单
	$(function() {
		//点击下拉菜单
		$('.demo-menu a').click(function() {
			var _tit = $(this).html(), _contant = $(this).attr(
					"href"), _tabCls = $(this).attr("tabCls");
			$('.demo-menu a').removeClass("icon-on");
			$(this).addClass("icon-on");
			addTab(_tit, _contant, _tabCls,0);
			return false;
		});

		//创建tab
		centerTabs = $('#tabs').tabs( {
			fit : true,
			border : false,
			onContextMenu : function(e, title) {
				e.preventDefault();
				tabsMenu.menu('show', {
					left : e.pageX,
					top : e.pageY
				}).data('tabTitle', title);
			}
		});
		//初始化菜单
		
		$('#tabs').tabs({
		    border:false,
		    onSelect:function(title){
		        if(title=="首页"){
		           refreshIndex();
		        }
			}
		});
		
		//菜单树
		$('#treeMenu').tree({
			url : 'menu/queryAllMenusToTree?parCode=1',
			lines : false,
			animate : true,
			onBeforeExpand : function(node, param) {
				$('#treeMenu').tree('options').url = "menu/queryAllMenusToTree?parCode=" + node.id;
			},
			onClick : function(node) {
				var url = node.menuURL;
				var tnode = node.tnode;
				if (tnode == 0 || url.lenght < 5) {
					return false;
				} else {
					addTab(node.text, url, "icon-on",0);
					return false;
				}
			},
			onLoadSuccess : function(row, data) {
				var t = $(this);
				if (data) {
					$(data).each(function(index, d) {
						if (this.state == 'closed') {
							t.tree('expandAll');
						}
					});
				}
			} 
		});
		
		
		$.ajax({
			url : 'sendTime/queryInfoToIndex',
			data : {
		},
		success : function(data) {
			var obj1 = eval('('+data+')');
			if (obj1.result==1){
				$("#sdtId").html(obj1.sdt);
				$("#edtId").html(obj1.edt);				
				$("#xqId").html(obj1.xq);
				$("#rqId").html(obj1.rq);
				$("#dgriId").html(obj1.dgr);
				$("#postInfoId").html(obj1.postInfo);
				$("#isNextPostId").html(obj1.isNextPost);
			}else {
				$.messager.show({    // show error message
					title: '提示',
					msg: "请检查网络是否链接正常!"
				});
			}
		 }
	  }); 
	});


//日报参照后
function refreshIndex(){
   $.ajax({
			url : 'sendTime/queryInfoToIndex',
			data : {
		},
		success : function(data) {
			var obj1 = eval('('+data+')');
			if (obj1.result==1){
				$("#sdtId").html(obj1.sdt);
				$("#edtId").html(obj1.edt);				
				$("#xqId").html(obj1.xq);
				$("#rqId").html(obj1.rq);
				$("#dgriId").html(obj1.dgr);
				$("#postInfoId").html(obj1.postInfo);
				$("#isNextPostId").html(obj1.isNextPost);
			}else {
				$.messager.show({    // show error message
					title: '提示',
					msg: "请检查网络是否链接正常!"
				});
			}
		}
	  });
}
	//刷新菜单
	function refreshMenus() {
		window.location = "user/index";
	} 

	//刷新
	function refreshTab(title) {
		var tab = centerTabs.tabs('getTab', title);
		centerTabs.tabs('update', {
			tab : tab,
			options : tab.panel('options')
		});
	}
	
	//打开密码修改
	function openPWD(){
	    $('#passwordDialog').show();
	    $('#passwordDialog').dialog({
			modal : true,
			closable : true,
			buttons : [ {
				text : '提交',
				handler : function() {
				    saveNewPWD();
				}
			} ],
			onOpen : function() {
				$('#passwordDialog form :input').val('');
			}
		})
	}
	
	
	
	//修改新密码
	function saveNewPWD(){
	     //判断值是否为合法
		var flag = false;
		$('#updPwdFrm').form('submit', {
			onSubmit: function() {
			flag = $(this).form('validate');
		}
		});

		if(!flag){
			return flag;
		}
		//end判断值是否为合法
		
		var newPd = $("input[name='newPWD']").val();
		var regPd = $("input[name='regPWD']").val();
		if(newPd!=regPd){
				$.messager.show({
					title: '提示',
					msg: "两次输入的密码不相同!"
				});
				return false;
		}
		$.ajax({
			url: "user/changePwd",
			data: {
			oldPwd: $("input[name='oldPWD']").val(),
			newPwd: $("input[name='newPWD']").val()
		},success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#passwordDialog').dialog('close');
				$.messager.show({
					title: '提示',
					msg: "修改密码成功!"
				});
			} else if(obj.result==2){
				$.messager.show({
					title: '提示',
					msg: "修改密码失败,旧密码不正确!"
				});
			} else {
			    $.messager.show({
					title: '提示',
					msg: "修改密码失败,请检查网络是否连接正常!"
				});
			}
		}
		});
	}
	
	
	//修改个人信息
	function openPerson(){
	   	$('#personalDialog').show();
	    $('#personalDialog').dialog({
			modal : true,
			closable : true,
			buttons : [ {
				text : '提交',
				handler : function() {
				    savePerson();
				}
			} ],
			onOpen : function() {
			    $.ajax({
					url: "user/loadUserToChange",
					data: {
				},success: function(data) {
					var obj = eval('(' + data + ')');
					if (obj.result == 1) {
					    $("input[name='userName']").val(obj.name);
					    $("input[name='userAccount']").val(obj.userAccount);
					    $("input[name='phone']").val(obj.phone);
					    $("input[name='position']").val(obj.position); 
					    
					    $("input[name='userName']").attr('readonly','readonly');
					    $("input[name='userAccount']").attr('readonly','readonly');
					    $("input[name='position']").attr('readonly','readonly');
					    
					    $("input[name='userName']").addClass("bgcolor");
					    $("input[name='userAccount']").addClass("bgcolor");
					    $("input[name='position']").addClass("bgcolor");
					} else {
					    $.messager.show({
							title: '提示',
							msg: "修改用户信息失败,请检查网络是否连接正常!"
						});
					}
				}
				});
			}
		})
	}
	
	
	//保存个人信息
	function savePerson(){
	     $.ajax({
			url: "user/changePerson",
			data: {
			telephone: $("input[name='phone']").val(),
		},success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#personalDialog').dialog('close');
				$.messager.show({
					title: '提示',
					msg: "修改个人信息成功!"
				});
			} else if(obj.result==2){
				$.messager.show({
					title: '提示',
					msg: "修改个人信息失败!"
				});
			} else {
			    $.messager.show({
					title: '提示',
					msg: "修改个人信息失败,请检查网络是否连接正常!"
				});
			}
		}
		});
	} 
	
	//时间展示
	var weekday=new Array(7);
	weekday[0]="星期日";
	weekday[1]="星期一";
	weekday[2]="星期二";
	weekday[3]="星期三";
	weekday[4]="星期四";
	weekday[5]="星期五";
	weekday[6]="星期六";
	//从服务器上获取初始时间 ,校正 1秒
	var currentDate = new Date(<%=new java.util.Date().getTime()%>);
	function run() {
		currentDate.setSeconds(currentDate.getSeconds() + 1);
		var time = " ";
		var year = currentDate.getFullYear();
		var month = currentDate.getMonth() + 1;
		var day = currentDate.getDate();
		var hour = currentDate.getHours();
		var minute = currentDate.getMinutes();
		var second = currentDate.getSeconds();
		var weekIndex = currentDate.getDay();
		if (hour < 10) {
			time += "0" + hour;
		} else {
			time += hour;
		}
		time += ":";
		if (minute < 10) {
			time += "0" + minute;
		} else {
			time += minute;
		}
		time += ":";
		if (second < 10) {
			time += "0" + second;
		} else {
			time += second;
		}
		document.getElementById("time").innerHTML = year + "年" + month + "月" + day + "日 "+ weekday[weekIndex] + time;
	}
	window.setInterval("run();", 1000);
</script>
<style type="text/css">
.caidanUL li {
	padding: 10px;
}

.caidanUL li:hover {
	background-color: #FFFFCC;
}

a:link {
	text-decoration: none;
}

a:visited {
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}

a:active {
	text-decoration: underline;
}

.bgcolor {
	background-color: #F2F2F2;
}
</style>
</head>
<body class="easyui-layout">
	<!-- Start顶端 -->
	<div region="north" style="height: 40px; background:url(images/logo.jpg) no-repeat 10px center">
		<div style="text-align: right; margin: 0px;padding:10px">
			<b><span id="time"></span>&nbsp;<font color="red">|</font>&nbsp;您好！${sessionScope.user.userName}	<font color="red">|</font></b> &nbsp;
			<a style="float:right;"	href="javascript:;" class="easyui-menubutton" data-options="menu:'#mm1'">更多操作</a>
		</div>
		<div id="mm1" style="width:100px;">
			<div>
				<a style="font-size:12px;font-weight:normal;" href="javascript:;"
					onclick="refreshMenus();">刷新菜单</a>
			</div>
			<div>
				<a style="font-size:12px;font-weight:normal;" href="javascript:;"
					onclick="openPerson();">我的信息</a>
			</div>
			<div>
				<a style="font-size:12px;font-weight:normal;" href="javascript:;"
					onclick="openPWD();">修改密码</a>
			</div>
			<div>
				<a style="font-size:12px;font-weight:normal;" href="admin/loginout">注销</a>
			</div>
		</div>
	</div>
	<!-- End顶端 -->

	<!-- Start中间部分 -->
	<div region="center">
		<div class="easyui-tabs" fit="true" border="false" id="tabs">
			<div title="首页">
				<table width="95%" cellpadding="20" align="center">
					<tr align="center">
						<td align="center"><span style="font-size:20px;" id="rqId"></span>
							&nbsp;&nbsp;<span style="font-size:20px;" id="xqId"></span><br />
							<div
								style="float:center;width:180px;height:180px;background-color:#F0FFF0;font-size:120px;font-family:隶书;border:1px solid #BCEE68;"
								id="dgriId"></div></td>
						<td><div
								style="float:center;width:240px;height:210px;background-color:#F7F7F7;font-size:20px;border:1px solid #D1D1D1;"
								id="ribaoInfoId">
								<span style="color:red;font-size:25px;font-weight:700">提示</span><br />
								<br /> <span id="isNextPostId"></span><br /> <br /> <span
									id="postInfoId"></span>
							</div></td>
					</tr>
					<tr align="center">
						<td></td>
						<td></td>
					</tr>
					<tr align="center">
						<td><div
								style="float:center;width:420px;height:180px;background-color:#FFF8DC;border:1px solid #FFA07A;">
								<span style="font-size:20px;">允许上交时间</span><br /> <span
									style="font-size:80px;" id="sdtId"></span>
							</div></td>
						<td><div
								style="float:center;width:420px;height:180px;background-color:#FFF8DC;border:1px solid #FFA07A;">
								<span style="font-size:20px;">截止上交时间</span><br /> <span
									style="font-size:80px;" id="edtId"></span>
							</div></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<!-- End中间部分 -->

	<!-- Start左边部分 -->
	<div region="west" class="west" title="菜单栏" style="width: 180px;">
		<div>
			<ul id="treeMenu" class="ztree"></ul>
		</div>
	</div>
	<!-- End左边部分 -->

	<!-- Start tab菜单栏 -->
	<div id="tabsMenu" style="width: 120px; display: none;">
		<div type="refresh">刷新</div>
		<div class="menu-sep"></div>
		<div type="close">关闭</div>
		<div type="closeOther">关闭其他</div>
		<div type="closeAll">关闭所有</div>
	</div>
	<!-- End tab菜单栏 -->

	<!-- Start提醒提示框 -->
	<div id="alertAialog" style="display: none;">
		<div id="optTypeId" style="display: none;"></div>
		<center>
			<br> <br> <span style='color:red;font-size:15px;'
				id='remindMsgId'></span> <br> <br> <br> <input
				type="button" value="查看" id="seeTabId"></input>
		</center>
	</div>
	<!-- End提醒提示框 -->

	<!-- 修改密码 -->
	<div id="passwordDialog" title="修改"
		style="display: none;width:400px;height:300px;">
		<form method="post" class="form" id="updPwdFrm"
			onsubmit="return false;">
			<center>
				<p style="font-size:20px;font-weight:800">修改密码</p>
				<table class="tab" cellpadding="10">
					<tr>
						<th>旧密码</th>
						<th><input type="password" name="oldPWD"
							class="easyui-validatebox" data-options="required:true" /></th>
					</tr>
					<tr>
						<th>新密码</th>
						<th><input type="password" name="newPWD" id="newPWDId"
							class="easyui-validatebox" data-options="required:true" /></th>
					</tr>
					<tr>
						<th>确认密码</th>
						<th><input type="password" name="regPWD"
							class="easyui-validatebox" data-options="required:true"
							validType="eqPassword['#newPWDId']" /></th>
					</tr>
				</table>
			</center>
		</form>
	</div>

	<!-- 个人信息 -->
	<div id="personalDialog" title="修改"
		style="display: none;width:400px;height:400px;">
		<form method="post" class="form" id="updPersonFrm"
			onsubmit="return false;">
			<center>
				<p style="font-size:20px;font-weight:800">修改个人信息</p>
				<table class="tab" cellpadding="10">
					<tr>
						<th>姓名:</th>
						<th><input type="text" name="userName" /></th>
					</tr>
					<tr>
						<th>账号:</th>
						<th><input type="text" name="userAccount" /></th>
					</tr>
					<tr>
						<th>职位:</th>
						<th><input type="text" name="position" /></th>
					</tr>
					<tr>
						<th>手机:</th>
						<th><input type="text" name="phone" /></th>
					</tr>
				</table>
			</center>
		</form>
	</div>
</body>
</html>
