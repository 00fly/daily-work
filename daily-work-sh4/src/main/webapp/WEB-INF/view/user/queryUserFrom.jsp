<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>用户管理</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
      <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="js/easyUI/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="js/easyUI/themes/icon.css">
<script type="text/javascript"
	src="js/easyUI/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="js/easyUI/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript"
	src="js/syUtil.js"></script>
<link rel="stylesheet" type="text/css"
	href="css/myCss.css">
<script type="text/javascript" charset="utf-8">
    var searchForm;
	var addCount = 0;
	var url ;
	$(function() {
	    searchForm = $('#searUserfm').form();
	    
		$('#list_data').datagrid({ 
	        title:'用户列表', 
	        url:"user/queryUserByCodeAnsPage?orgCode="+0,
	        rownumbers:true,//行号 
	        nowrap:true,
	        pagination :true, 
	        pageNumber:1,
	        pageSize:20,
	        pageList:[10,20,30,40,50],
	        pagePosition:'bottom',
	        striped:true,
	        fit:true,
	        singleSelect:true,//是否单选 
	        columns:[[
				{field:'userId',title:'编码',width:200,hidden:'true'},
				{field:'userName',title:'姓名',width:100,align:'center'},
				{field:'userLoginName',title:'账号',width:120,align:'center'},
				{field:'workNumber',title:'邮箱',width:120,align:'center'},
				{field:'positionName',title:'职位',width:150,align:'center',hidden:'true'},
				{field:'positionId',title:'职位',width:150,align:'center',hidden:'true'},
				{field:'sort',title:'排序',width:50,align:'center'},
				{field:'enabled',title:'是否可用',width:80,align:'center', formatter: imgcheckbox },
				{field:'detailInfo',title:'用户详情',width:80,align:'center',
				 formatter: function(value, row, index) {  
                    var abValue = "<span style='color:#00C;'>查看</span>";  
                    var content = "<div href='javascript:;' style='cursor:pointer' title='" + value + "' class='note'>" + abValue + "</div>";  
                    return content;  
                   }  
				},
				{field:'optUrl',title:'操作',width:200,align:'center'}
	        ]],
	        toolbar: [{ 
	            text: '添加', 
	            iconCls: 'icon-add', 
	            handler: function() { 
	               var orgCodeHtml = $("#codeDIVId").html();
					if(orgCodeHtml==""){
						$.messager.alert('提示','请选择所属的组织机构!','warning');
					}else{
						form.form('clear');
						win.window('open');
						win.dialog('open').dialog('setTitle','添加用户');
						$("#isAbleId").attr("checked","checked");
						$("#loginName").removeClass("bgcolor");
						$("#loginName").removeAttr('readonly');
						/*
						var posurl = "position/queryPositionByCode?orgCode="+orgCodeHtml;
						$("#positioId").combobox({ 
							url:posurl,
							textField: "text",
							valueField: "id",
							multiple: false,
						    onLoadSuccess: function () { 
								 var data = $('#positioId').combobox('getData');
						             if (data.length > 0) {
						                 $('#positioId').combobox('select', data[0].id);
						         } 
							}
						});
						*/
					}
	            } 
	        }, '-', { 
	            text: '修改', 
	            iconCls: 'icon-edit', 
	            handler: function() { 
	                var row = $("#list_data").datagrid("getSelected");
					if(row){
						win.dialog('open').dialog('setTitle','修改用户');
						win.form('clear');
						$("#loginName").addClass("bgcolor");
						$("#loginName").attr('readonly','readonly');
						win.form('clear');
						url = 'user/loadUserByUserId?userId='+row.userId;
						win.form('load',url);
						var orgCodeHtml = $("#codeDIVId").html();
						/*
						var posurl = "position/queryPositionByCode?orgCode="+orgCodeHtml;
						$("#positioId").combobox({ 
							url:posurl,
							textField: "text",
							valueField: "id",
							multiple: false,
							onLoadSuccess: function () { 
								$("input[name='positionName']").combobox('setValue',row.positionId);
							}
						});
						*/
						
			
					}else{
						$.messager.alert('提示','请选择需要修改的用户!','warning');
						return false;
					}
	            } 
	        }, '-',{ 
	            text: '删除', 
	            iconCls: 'icon-remove', 
	            handler: function(){ 
	                var rows = $("#list_data").datagrid("getSelected");
					//判断是否选择行
					if(!rows || rows.length == 0){
						$.messager.alert('提示','请选择你要删除的用户!','warning');
						return ;
					}
					$.messager.confirm('提示', '确定要删除用户【'+rows.userName+'】吗?', function(r){
						if (r){
							$.ajax({
								type: "POST",
								url: "user/deleteUser",
								data: {userId:rows.userId},
								success: function(data){
									var obj = eval('('+data+')');
									if (obj.result==1){
										$('#list_data').datagrid('reload');    // reload the user data
										$.messager.show({
											title:'提示',
											msg:obj.errorMsg,
											timeout:2000,
											showType:'fade'
										});
									} else {
										$.messager.show({    // show error message
											title: '错误',
											msg: obj.errorMsg
										});
									}
								}
							});
			
						}
					});
	            } 
	        },'-',{
				iconCls: 'icon-reload',
				text:'刷新',
				handler: function(){
				   $('#list_data').datagrid('reload');
				}
			}],
			onLoadSuccess:function(data)  
             {  
                $(".note").tooltip(  
                    {
                    position: 'left',  
                    onShow: function(){  
                        $(this).tooltip('tip').css({   
                            //width:'300',  
                            boxShadow: '1px 1px 3px #292929'                          
                        });  
                    }  
                }  
                );  
             }  
	    }); 
	    //组织树
	    $('#treeOrg').tree({
               url:'org/queryOrgTreeToUser?parCode=1',
               lines:true,
               animate:true,
			   onBeforeExpand:function(node,param){                         
			        $('#treeOrg').tree('options').url = "org/queryOrgTreeToUser?parCode="+node.id;  
		       },
               onClick:function(node){  
			        $("#codeDIVId").html(node.id);
			        var url = "user/queryUserByCodeAnsPage?orgCode="+node.id;
			        $('#list_data').datagrid({
					    url:url
					});
			   },
			   onLoadSuccess : function(row, data) {
					var t = $(this);
					if (data) {
						$(data).each(function(index, d) {
							if (this.state == 'closed') {
								t.tree('expandAll');
							}
						});
					}
			 }
		});
	    
		$('#btn-save,#btn-cancel').linkbutton();
		win = $('#provider-window').window({
			closed:true,
			modal : true
		});
		form = win.find('form');
	});
	
	
	 //查询
    function searchUser(){
        $('#list_data').datagrid('load', sy.serializeObject(searchForm));
    }
    //重置
    function crearUser(){
        searchForm.find('input').val('');
    }
	
	
	//刷新组织tree
	function refreshUserOrg(){
	    $('#treeOrg').tree({
               url:'org/queryOrgTreeToUser?parCode=1',
               lines:true,
               animate:true,
			   onBeforeExpand:function(node,param){                         
			        $('#treeOrg').tree('options').url = "org/queryOrgTreeToUser?parCode="+node.id;  
		       },
               onClick:function(node){  
			        $("#codeDIVId").html(node.id);
			        var url = "user/queryUserByCodeAnsPage?orgCode="+node.id;
			        $('#list_data').datagrid({
					    url:url
					});
			   },onLoadSuccess : function(row, data) {
							var t = $(this);
							if (data) {
								$(data).each(function(index, d) {
									if (this.state == 'closed') {
										t.tree('expandAll');
									}
								});
							}
			 }
		});
	}
	
	
	//展开
	function openUserOrg(){
	    $('#treeOrg').tree('expandAll');
	}
	
	//折叠
	function closeUserOrg(){
	   $('#treeOrg').tree('collapseAll');
	}
   var imgcheckbox = function (cellvalue, options, rowObject) {
                return cellvalue ? '<img src="images/icon/can.png" alt="正常" title="正常" />' : '<img src="images/icon/not.png" alt="禁用" title="禁用" />';  
   };  
        
	var win;
	var form;
	// 弹出新增用户
	function newUser(){
        var orgCodeHtml = $("#codeDIVId").html();
        if(orgCodeHtml==""){
            $.messager.alert('提示','请选择所属的组织机构!','warning');
        }else{
           form.form('clear');
           win.window('open');
           win.dialog('open').dialog('setTitle','添加用户');
           $("#isAbleId").attr("checked","checked");
           $("#loginName").removeClass("bgcolor");
           $("#loginName").removeAttr('readonly');
        }
    }
    
	// 保存用户
    function saveUser(){
     	var orgCodeHtml = $("#codeDIVId").html();
        $("#codeToUserId").val(orgCodeHtml);
        url = "user/saveUserByOrgCode";
        //alert(url);
    	$("#fmUser").form('submit', {
    		url:url,
    		method:'post',
    		success:function(data){
    			var obj = eval('('+data+')');
    			if (obj.result==1){
    				$('#list_data').datagrid('reload');
    				$.messager.show({title:'提示',msg:obj.msg});
    				win.window('close');
    			} else {
    				$.messager.alert('提示','添加用户失败!','warning');
    			}
    		}
    	});
    }
    
    //刷新用户
    function refreshUser(){
       $('#list_data').datagrid('reload');
    }
    
    
    //保存核心人员
    function saveCorePerson(){
	    var checks = $(".coreUseres");
	    var strBuffer = "";
		var objarray = checks.length;
			var strBuffer = "";
			for (i = 0; i < objarray; i++) {
				if (checks[i].checked == true) {
					modelid = checks[i].value;
					strBuffer += modelid;
					strBuffer += "#";
				}
			}
			
	   //提交数据
	   $.ajax({
				url : 'user/saveCoreUser',
				data : {
					coreUserBuffer:strBuffer
				},
				success : function(data) {
					var obj = eval('('+data+')');
	                if (obj.result==1){
					    $.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					    $('#coreToUserId').dialog('close'); 
					} else {
						$.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					}
				}
			  });
    }
    //分配副机构
    function giveOrgToUser(userId,userName){
        $('#orgUserId').show();
	    $("#userIdToPan").html(userId);
		$('#orgUserId').dialog({ 
		title:'分配组织机构-用户'+userName, 
		width:$(window).width() * 0.4,
		height:$(window).height() * 0.95,
		padding:5,
		modal:true,
		buttons: [{ 
			text: '提交', 
			handler: function() { 
				saveOrgsToUser();
			} 
		}, { 
			text: '取消', 
			handler: function() { 
				$('#orgUserId').dialog('close'); 
			} 
		}] 
		}); 
		
		
		//组织机构
	    $('#orgUsersTree').tree({
               url:'org/queryGiveOrgToUser?parCode=1&userId='+userId,
               lines:true,
               animate:true,
               checkbox:true,
			   onBeforeExpand:function(node,param){                         
			        $('#orgUsersTree').tree('options').url = "org/queryGiveOrgToUser?parCode="+node.id+"&userId="+userId;  
		       },onLoadSuccess : function(row, data) {
					var t = $(this);
					if (data) {
						$(data).each(function(index, d) {
							if (this.state == 'closed') {
								t.tree('expandAll');
							}
						});
					}
			 }
		});
    }
    
    
    //分配角色用户
    function giveRoleToUser(userId,userName){
       $("#check_all").removeAttr("checked");
       $('#setRoleToUserId').dialog('open').dialog('setTitle',"设置角色-用户"+userName);
	   $("#roleToUserId").html(userId);
	   
	   //加载角色
	   $.ajax({
			url : 'role/queryAllRolesToUser',
			data : {userId:userId},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
					var butHtml = "";
					for ( var i = 0; i < obj.datas.length; i++) {
						if(obj.datas[i].sign==1){
						   butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' checked='checked' class='roleselect' value='"+obj.datas[i].roleId+"'/>"+obj.datas[i].roleName+"</a>";
						}else{
						   butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='roleselect' value='"+obj.datas[i].roleId+"'/>"+obj.datas[i].roleName+"</a>";
						}
						
				        if(i%4==0&&i!=0){
				           butHtml = butHtml+ "<br/><br/>"
				        }
				    }
				    
				    $("#rolesUserDiv").html(butHtml);
				    editRow = undefined;
				} else {
					$.messager.show({
					msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		});
    }
    
    
    //分配组给用户
    function giveRoleGroupToUser(userId,userName){
       $('#setGroupToUserId').dialog('open').dialog('setTitle',"设置角色组-用户"+userName);
	   $("#check_allgroup").removeAttr("checked");
	   $("#groupToUserId").html(userId);
	   
	   //加载角色
	   $.ajax({
			url : 'rolegroup/queryAllGroupsToUser',
			data : {userId:userId},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
					var butHtml = "";
					for ( var i = 0; i < obj.datas.length; i++) {
						if(obj.datas[i].sign==1){
						   butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='groupChe' checked='checked' value='"+obj.datas[i].groupId+"'/>"+obj.datas[i].groupName+"</a>";
						}else{
						   butHtml = butHtml+"<a href='javascript:;' style='text-align:left;'><input type='checkbox' class='groupChe' value='"+obj.datas[i].groupId+"'/>"+obj.datas[i].groupName+"</a>";
						}
						
				        if(i%4==0&&i!=0){
				           butHtml = butHtml+ "<br/><br/>"
				        }
				    }
				    
				    $("#groupsUserDiv").html(butHtml);
				} else {
					$.messager.show({
					msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		});
    }
    
    //分配角色给用户
    function saveRolesToUser(){
        var userId = $("#roleToUserId").html();
        var orgCode = $("#codeDIVId").html();
        var str = $("#rolesUserDiv input[type='checkbox']");
		var objarray = str.length;
		var strBuffer = "";
		for (i = 0; i < objarray; i++) {
			if (str[i].checked == true) {
					modelid = str[i].value;
					strBuffer += modelid;
					strBuffer += ",";
		    }
		}
		
		if($("#rolesUserDiv input[type='checkbox']:checked").size()>0){
		    $.ajax({
			url : 'role/saveRoleToUser',
			data : {
				strBuffer:strBuffer,
				userId:userId,
				orgCode:orgCode
			},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
				    $.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				    $('#setRoleToUserId').dialog('close'); 
				     $('#list_data').datagrid('reload');
				} else {
					$.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		  });
		}else{
		   $.messager.show({
				msg :  "请选择你要设置的角色!",
				title : '提示'
				});
		}
    
    }
    
    //分配组织机构给用户
    function saveOrgsToUser(){
            var userId = $("#userIdToPan").html();
            var nodes = $('#orgUsersTree').tree('getChecked');
            var strBuffer = '';
            for(var i=0; i<nodes.length; i++){
                strBuffer += nodes[i].id;
                strBuffer += ',';
            }
            
             $.ajax({
				url : 'user/saveOrgsToUser',
				data : {
					strBuffer:strBuffer,
					userId:userId//,
					//orgCode:orgCode
				},
				success : function(data) {
					var obj = eval('('+data+')');
	                if (obj.result==1){
					    $.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					    $('#orgUserId').dialog('close'); 
					    $('#list_data').datagrid('reload');
					} else {
						$.messager.show({
							msg :  obj.errorMsg,
							title : '提示'
					    });
					}
				}
			 });
    }
    
    //分配用户组
    function saveGroupsToUser(){
        var userId = $("#groupToUserId").html();
        var orgCode = $("#codeDIVId").html();
        var str = $("#groupsUserDiv input[type='checkbox']");
		var objarray = str.length;
		var strBuffer = "";
		for (i = 0; i < objarray; i++) {
			if (str[i].checked == true) {
					modelid = str[i].value;
					strBuffer += modelid;
					strBuffer += ",";
		    }
		}
		
		if($("#groupsUserDiv input[type='checkbox']:checked").size()>0){
		    $.ajax({
			url : 'rolegroup/saveGroupToUser',
			data : {
				strBuffer:strBuffer,
				userId:userId,
				orgCode:orgCode
			},
			success : function(data) {
				var obj = eval('('+data+')');
                if (obj.result==1){
				    $.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				    $('#setGroupToUserId').dialog('close'); 
				     $('#list_data').datagrid('reload');
				} else {
					$.messager.show({
						msg :  obj.errorMsg,
						title : '提示'
				    });
				}
			}
		  });
		}else{
		   $.messager.show({
				msg :  "请选择你要设置的角色!",
				title : '提示'
				});
		}
    
    }
    
    
    // 验证
    function setValidation(){
		$("#name").validatebox({
			required: true,
			validType:['length[0,20]'],
			missingMessage:"用户名称不能为空",
			invalidMessage:"长度不能超过10个汉字"
		});
	}
	// 关闭窗口
    function closeWindow(){
    	win.window('close');
    }
    
    //全选和反选
    $(function(){
   	  $("#check_all").click(function(){
			if(this.checked){ 
			  $(".roleselect").each(function(){this.checked=true;}); 
			}else{ 
			  $(".roleselect").each(function(){this.checked=false;}); 
			} 
	  });
    
    $("#check_allgroup").click(function(){
        if(this.checked){ 
			$(".groupChe").each(function(){this.checked=true;}); 
		}else{ 
			$(".groupChe").each(function(){this.checked=false;}); 
		} 
    });
    
    });
    
    
      //全选
    function allSelect(cellvalue, options, rowObject){
        return "<input onclick=selectAlls('"+options.id+"') val='1' id="+options.id+" type='checkbox'>";
    }
    
    //全部选择项
    function selectAlls(orgCode){
        var fVal = $("#"+orgCode).val();
        if(fVal){
           $(".but"+orgCode).each(function(){this.checked=true;}); 
           $("#"+orgCode).val(0)
        }
        
        if(fVal==0){
           $(".but"+orgCode).each(function(){this.checked=false;}); 
           $("#"+orgCode).val(1)
        }
    }
</script>
<style type="text/css">
.form_label{
   text-align:right;
}

.ssId{
   width:140px;
}

.bgcolor{
   background-color:#F2F2F2;
}
</style>
</head>
<body class="easyui-layout">
    <div data-options="region:'west',split:true,title:'组织机构',iconCls:'icon-book'"
				style="width: 250px;height:100%">
				<div style="border: 1px solid #ddd;">
					<div id="toolbar" style="vertical-align: middle">
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-expend" plain="true" onclick="openUserOrg()">展开</a>
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-suoxiao" plain="true" onclick="closeUserOrg()">折叠</a>
						<a href="javascript:void(0)" class="easyui-linkbutton"
							iconCls="icon-reload" plain="true" onclick="refreshUserOrg()">刷新</a>
					</div>
				</div>
				<div>
					<ul id="treeOrg" class="ztree">
					</ul>
				</div>
	</div>
	<div region="center" border="false">
	      <div class="easyui-layout" data-options="fit:true">
                <div data-options="region:'north',split:true,border:false,title:'查询栏'" style="height:95px;background-color:#F0F0F0;">
                   <fieldset>
					  <legend>查询</legend>
					  <form method="post" id="searUserfm">  
					    姓名: <input type="text" name="userName" style="width:100px;"/>&nbsp;
					    账号: <input type="text" name="userLoginName" style="width:100px;"/>&nbsp;
					    邮箱: <input type="text" name="workNumber" style="width:100px;"/>&nbsp;
					    全(缩)拼音: <input type="text" name="pinyin" style="width:100px;"/>
					  <a href="javascript:void(0)" onclick="searchUser()" class="easyui-linkbutton" >查询</a>
					  <a href="javascript:void(0)" onclick="crearUser()" class="easyui-linkbutton"  >清空</a>
					  </form>
					</fieldset>
                </div>
                <div data-options="region:'center',border:false">
	                <table id="list_data" cellspacing="0" cellpadding="0" >  
			        </table>
                </div>
          </div>
	</div>
	<span id="codeDIVId" style="display:none;"></span>
  <!-- 添加用户界面 -->
  <div id="provider-window" title="添加用户" style="width:400px;height:440px;" data-options="minimizable:false,maximizable:false,collapsible:false">
		<div style="padding:20px 30px 40px 30px;">
			<form method="post" id="fmUser">
				<input name="orgCode" type="hidden" id="codeToUserId"></input>
				<input name="userId" type="hidden" id="userId"></input>
				<table width="300px" align="center" style="align: center" border="0">
					<tr>
						<td class="form_label">用户姓名：</td>
						<td class="form_content" >
						<input name="userName" class="easyui-validatebox" maxlength="32" data-options="required:true,validType:'length[1,32]'"></input>
						</td>
					</tr>
					<tr>
						<td class="form_label">登陆账号：</td>
						<td class="form_content" ><input id="loginName" name="userLoginName" class="easyui-validatebox" maxlength="32" data-options="required:true,validType:'length[1,32]'"></input></td>
					</tr>
					<tr>
						<td class="form_label">手机号码：</td>
						<td class="form_content" ><input name="telephone" maxlength="11" class="easyui-validatebox"></input></td>
					</tr>
					<tr>
						<td class="form_label">员工邮箱：</td>
						<td class="form_content" ><input name="workNumber" class="easyui-validatebox" data-options="required:false,validType:'email'" maxlength="50"></input></td>
					</tr>
					<!-- 
					<tr>
						<td class="form_label">员工职位：</td>
						<td class="form_content" ><input name="positionName" maxlength="32" id="positioId" ></input></td>
					</tr>
					 -->
					<tr>
						<td class="form_label">姓名拼音全拼：</td>
						<td class="form_content" ><input name="spellLong" maxlength="32"></input></td>
					</tr>
					<tr>
						<td class="form_label">排序：</td>
						<td class="form_content" ><input name="spellShort" maxlength="32"></input></td>
					</tr>
					<tr>
						<td class="form_label">是否可用：</td>
						<td class="form_content" >
						<input type="radio" name="enabled" value="1" id="isAbleId"/>可用&nbsp;&nbsp;
                        <input type="radio" name="enabled" value="0" id="noAbleId"/>禁用&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td class="form_label">用户备注：</td>
						<td class="form_content" >
                           <textarea style="height:60px;width:155px" name="userMemo"></textarea>
                        </td>
					</tr>
					<tr>
						<td class="form_label" colspan="2" style="text-align: center">
							<a href="javascript:void(0)" onclick="saveUser()" id="btn-save">保存</a>
							<a href="javascript:void(0)" onclick="closeWindow()" id="btn-cancel">取消</a>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<!-- end添加用户界面 -->
	
	 <!-- 分配机构 -->
		<div id="orgUserId" style="display:none;">
		     <span id="userIdToPan" style="display:none;"></span>
		     <div class="easyui-layout" data-options="fit:true">
	             <div class="easyui-tabs" >
	                  <div title="选择机构" style="padding:10px;height:100%;">
	                       <ul id="orgUsersTree" class="easyui-tree"></ul>
	                  </div>
	            </div>
		     </div>
		</div>
		
	<!-- 设置角色 -->
    <div id="setRoleToUserId" class="easyui-dialog"
			style="width: 700px; height: 500px;  padding: 5px 5px;"
			closed="true" data-options="modal:true,buttons: [{
                    text:'保存',
                    handler:function(){
                       saveRolesToUser();
                    }
                },{
                    text:'取消',
                    handler:function(){
                       $('#setRoleToUserId').dialog('close');
                    }
                }]">
			<form method="post" id="fmbut" >
				<span id="roleToUserId" style="display:none;"></span>
				<div><p><img src="images/icon/duoxuan.png"/><span style="font-size:16px;font-weight:700;">&nbsp;请选择页面角色</span></p></div>
				<div><hr/></div>
				<div><input type="checkbox" id="check_all"/><span style="font-size:16px;">全选/反选</span></div>
				<br/>
				<div id="rolesUserDiv" class="abox" style="width:98%"></div>
			</form>
		</div>
		
	<!-- 设置角色组 -->
    <div id="setGroupToUserId" class="easyui-dialog"
			style="width: 700px; height: 500px;  padding: 5px 5px;"
			closed="true" data-options="modal:true,buttons: [{
                    text:'保存',
                    handler:function(){
                       saveGroupsToUser();
                    }
                },{
                    text:'取消',
                    handler:function(){
                       $('#setGroupToUserId').dialog('close');
                    }
                }]">
			<form method="post" id="fmbut" >
				<span id="groupToUserId" style="display:none;"></span>
				<div><p><img src="images/icon/duoxuan.png"/><span style="font-size:16px;font-weight:700;">&nbsp;请选择页面角色组</span></p></div>
				<div><hr/></div>
				<div><input type="checkbox" id="check_allgroup"/><span style="font-size:16px;">全选/反选</span></div>
				<br/>
				<div id="groupsUserDiv" class="abox" style="width:98%"></div>
			</form>
		</div>
		
		 <!-- 设置核心员工 -->
		<div id="coreToUserId" style="display:none;">
		     <div class="easyui-layout" data-options="fit:true">
	                  <div title="选择用户" style="padding:0px;height:98%;">
	                      <table id="coreUserTreeGrid" >
	                          <thead>
	                         <tr>
	                           <th data-options="field:'id'" width="20" hidden="true">id</th>
	                           <th data-options="field:'orgName'" width="200" align="left">部门名称</th>
	                           <th data-options="field:'aselect',formatter:allSelect" width="50"align="center">全选</th>
	                           <th data-options="field:'selectCodeUserOpt'" align="left">用户选项</th>
	                         </tr>
	                         </thead>
	                      </table>
	                  </div>
		     </div>
		</div>
		<!-- end核心员工 -->
</body>
</html>