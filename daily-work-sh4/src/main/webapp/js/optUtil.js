var opt = $.extend({}, opt);/* 定义全局对象 */


/***
 * 定义操作的添加
 */
opt.add = function(type){
	if(type=='menu'){   //菜单添加
		if (editRow != undefined) {
			treegrid.treegrid('endEdit', editRow.id);
		}

		var parStr = "";
		if (editRow == undefined) {
			var node = treegrid.treegrid('getSelected');
			if(node==null){
				parStr = 1;
			}else{
				parStr = node.id;
			}
			var row = [ {  //添加时候初始化
				id : '',
				menuName : '菜单名称',
				menuURL : '',
				isLeafMenu: 1,
				enabled : 1,
				parCode:parStr
			} ];
			treegrid.treegrid('append', {
				parent : node == null ? '' : node.id,
						data : row
			});

			editRow = row[0];
			editType = 'add';
			treegrid.treegrid('beginEdit', editRow.id);
			$(".datagrid-editable-input").height(22);
		}
	}else if(type=='org'){  //添加组织机构
		form.form('clear');
		var row = $('#orgTree').treegrid('getSelected');
		win.window('open');
		$("#isAbleId").attr("checked","checked");
		$("#noOlieId").attr("checked","checked");
		
		if(row){
			url = '${pageContext.request.contextPath}/org!loadAddOrgInfoByCode?orgCode='+row.orgCode;
			$('#fmId').form('load',url);
		}else{
			url = '${pageContext.request.contextPath}/org!loadAddOrgInfoByCode?orgCode=1';
			$('#fmId').form('load',url);
		}
		$('#comTreeId').combotree({
			url: '${pageContext.request.contextPath}/org!queryOrgComTree',
			required: true,
			onLoadSuccess : function(row, data) {
			var t = $(this);
			if (data) {
				$(data).each(function(index, d) {
					if (this.state == 'closed') {
						t.tree('expandAll');
					}
				});
			}
		}
		});
		
	}else if(type=='user'){   //添加用户
		var orgCodeHtml = $("#codeDIVId").html();
		if(orgCodeHtml==""){
			$.messager.alert('提示','请选择所属的组织机构!','warning');
		}else{
			form.form('clear');
			win.window('open');
			win.dialog('open').dialog('setTitle','添加用户');
			$("#isAbleId").attr("checked","checked");
			$("#loginName").removeClass("bgcolor");
			$("#loginName").removeAttr('readonly');
			/*
			var posurl = "${pageContext.request.contextPath}/position!queryPositionByCode?orgCode="+orgCodeHtml;
			$("#positioId").combobox({ 
				url:posurl,
				textField: "text",
				valueField: "id",
				multiple: false,
			    onLoadSuccess: function () { 
					 var data = $('#positioId').combobox('getData');
			             if (data.length > 0) {
			                 $('#positioId').combobox('select', data[0].id);
			         } 
				}
			});
			*/
		}
	}else if(type=='button'){   //添加按键
		$('#addButId').dialog('open').dialog('setTitle','添加');
		$("#titleId").html("添加按钮");
		$('#fmbut').form('clear');
		$("#isAbleId").attr("checked","checked");
	}else if(type=='role'){   //添加角色
		$('#addRoleId').dialog('open').dialog('setTitle','添加');
		$("#titleId").html("添加角色");
		$('#fmbut').form('clear');
		$("#isAbleId").attr("checked","checked");
	}else if(type=='roleGroup'){  //添加角色组
		$('#addRoleGroupId').dialog('open').dialog('setTitle','添加');
		$("#titleId").html("添加角色组");
		$('#fmbut').form('clear');
		$("#isAbleId").attr("checked","checked");
	}else if(type=='config'){   //添加字典
		var row = $('#treeDemo').tree('getSelected');
		if(row){
			if(row.id==999){
				$('#dlg').dialog('open').dialog('setTitle','添加');
				$("#titleId").html("添加字典类别");
				$('#fm').form('clear');
			}else{
				$.ajax({
					url : '${pageContext.request.contextPath}/config!loadConfigByCode',
					data : {
					dataCode : row.id
				},
				success : function(data) {
					var obj = eval('('+data+')');
					if(obj.dataType==1){
						$('#dlgson').dialog('open').dialog('setTitle','添加');
						$("#sonTitleId").html("添加字典");
						$('#fmson').form('clear');
						$("#dtId").hide();
						$("#dataTypeCodeId").val("cjw_1");
						$("#sortId").numberspinner('setValue', 1);
						$("#isAbleId").attr("checked","checked");
						$("#sonDataCode").val(row.id);
					}else{
						$('#dlgson').dialog('open').dialog('setTitle','添加');
						$("#sonTitleId").html("添加字典");
						$('#fmson').form('clear');
						$("#dtId").show();
						$("#sortId").numberspinner('setValue', 1);
						$("#isAbleId").attr("checked","checked");
						$("#sonDataCode").val(row.id);
					}
				  }
				});
			}
		}else{
			$.messager.alert('提示','请选择所属的字典类别!','warning');
		}
	}else if(type=='tab'){
		$('#addTabId').show();
		$('#fmTab').form('clear');
		$("#titleId").html("添加选项卡");
		$('#tabId').val(0);
		$('#addTabId').dialog({
			title: '添加选项卡',
			width: 430,
			height: 300,
			padding: 5,
			modal: true,
			buttons: [{
				text: '提交',
				handler: function() {
				opt.save('tab');
			}
			},
			{
				text: '取消',
				handler: function() {
				$('#addTabId').dialog('close');
			}
			}]
		});
		
		$("#isAbleId").attr("checked","checked");
	}
}

/****
 * 定义删除操作
 * */
opt.remove = function(type){
	if(type=='menu'){   //菜单删除
		var node = treegrid.treegrid('getSelected');
		if (node) {
			$.messager.confirm('提示', '您确定要删除【' + node.menuName + '】？', function(b) {
				if (b) {
					$.ajax({
						url : '${pageContext.request.contextPath}/menu!deleteMenu',
						data : {
						menuCode : node.id
					},
					success : function(data) {
						var obj = eval('('+data+')');
						if (obj.result==1){
							/*
								for ( var i = 0; i < obj.datas.length; i++) {
									$("tr[node-id='"+obj.datas[i].menuCode+"']").remove();
							    }
							 */
							$.messager.show({
								msg : obj.errorMsg,
								title : '提示'
							});
							treegrid.treegrid('reload');
							editRow = undefined;
						} else {
							$.messager.show({
								msg :  obj.errorMsg,
								title : '提示'
							});
						}
					}
					});
				}
			});
		}
	}else if(type=='org'){  //删除机构
		var row = $('#orgTree').treegrid('getSelected');
		if(row){
			$.messager.confirm('提示','你确定删除【'+row.orgName+'】?',function(r){
				if (r){
					$.ajax({
						type: "POST",
						url: "${pageContext.request.contextPath}/org!deleteOrgInfo",
						data: {orgCode:row.orgCode},
						success: function(data){
							var obj = eval('('+data+')');
							if (obj.result==1){
								/*
								for ( var i = 0; i < obj.datas.length; i++) {
									$("tr[node-id='"+obj.datas[i].orgCode+"']").remove();
								}
								 */
								$('#orgTree').treegrid("reload");
								$.messager.show({    // show error message
									title: '提示',
									msg: obj.errorMsg
								});

							} else if(obj.result==3){
								$.messager.show({    // show error message
									title: '提示',
									msg: obj.errorMsg
								});
							}else{
								$.messager.show({    // show error message
									title: '提示',
									msg: obj.errorMsg
								});
							}
						}
					});
				}
			});
		}else{
			$.messager.alert('提示','请选择你要删除的机构!','warning');
		}
	}else if(type=='user'){   //删除用户
		var rows = $("#list_data").datagrid("getSelected");
		//判断是否选择行
		if(!rows || rows.length == 0){
			$.messager.alert('提示','请选择你要删除的用户!','warning');
			return ;
		}
		$.messager.confirm('提示', '确定要删除用户【'+rows.userName+'】吗?', function(r){
			if (r){
				$.ajax({
					type: "POST",
					url: "${pageContext.request.contextPath}/user!deleteUser",
					data: {userId:rows.userId},
					success: function(data){
						var obj = eval('('+data+')');
						if (obj.result==1){
							$('#list_data').datagrid('reload');    // reload the user data
							$.messager.show({
								title:'提示',
								msg:obj.errorMsg,
								timeout:2000,
								showType:'fade'
							});
						} else {
							$.messager.show({    // show error message
								title: '错误',
								msg: obj.errorMsg
							});
						}
					}
				});

			}
		});
	}else if(type=='button'){   //删除按键
		var row = $('#butgrid').datagrid('getSelected');
		if (row){
			$.messager.confirm('提示','你确定删除【'+row.buttonName+'】?',function(r){
				if (r){
					$.ajax({
						type: "POST",
						url: "${pageContext.request.contextPath}/button!deleteButton",
						data: {buttonId:row.buttonId},
						success: function(data){
							var obj = eval('('+data+')');
							if (obj.result==1){
								$('#butgrid').datagrid("reload"); 
								$.messager.show({    // show error message
									title: '提示',
									msg: obj.errorMsg
								}); 
							} else {
								$.messager.show({    // show error message
									title: '提示',
									msg: obj.errorMsg
								});
							}
						}
					});
				}
			});
		}else{
			$.messager.alert('提示','请选择你要删除的按钮!','warning');
		}
	}else if(type=='role'){   //删除角色
		var row = $('#rolegrid').datagrid('getSelected');
		if (row){
			$.messager.confirm('提示','你确定删除【'+row.roleName+'】?',function(r){
				if (r){
					$.ajax({
						type: "POST",
						url: "${pageContext.request.contextPath}/role!deleteRole",
						data: {roleId:row.roleId},
						success: function(data){
							var obj = eval('('+data+')');
							if (obj.result==1){
								$('#rolegrid').datagrid("reload"); 
								$.messager.show({    // show error message
									title: '提示',
									msg: obj.errorMsg
								}); 
							} else {
								$.messager.show({    // show error message
									title: '提示',
									msg: obj.errorMsg
								});
							}
						}
					});
				}
			});
		}else{
			$.messager.alert('提示','请选择你要删除的角色!','warning');
		}
	}else if(type=='roleGroup'){  //删除角色组
		var row = $('#roleGroupgrid').datagrid('getSelected');
		if (row){
			$.messager.confirm('提示','你确定删除【'+row.groupName+'】?',function(r){
				if (r){
					$.ajax({
						type: "POST",
						url: "${pageContext.request.contextPath}/rolegroup!deleteRoleGroup",
						data: {groupId:row.groupId},
						success: function(data){
							var obj = eval('('+data+')');
							if (obj.result==1){
								$('#roleGroupgrid').datagrid("reload"); 
								$.messager.show({    // show error message
									title: '提示',
									msg: obj.errorMsg
								}); 
							} else {
								$.messager.show({    // show error message
									title: '提示',
									msg: obj.errorMsg
								});
							}
						}
					});
				}
			});
		}else{
			$.messager.alert('提示','请选择你要删除的角色组!','warning');
		}
	}else if(type=='config'){   //删除字典
		var row = $('#dagrid').datagrid('getSelected');
		if (row){
			if(row.isopt==1){
				$.messager.alert('提示','你没有权限删除系统级配置!','warning');
			}else{
				$.messager.confirm('提示','你确定删除【'+row.dataName+'】?',function(r){
					if (r){
						$.ajax({
							type: "POST",
							url: "${pageContext.request.contextPath}/config!deleteConfig",
							data: {dataCode:row.dataCode},
							success: function(data){
								var obj = eval('('+data+')');
								if (obj.result==1){
									$('#dagrid').datagrid('reload');    // reload the user data
								} else {
									$.messager.show({    // show error message
										title: '提示',
										msg: obj.errorMsg
									});
								}
							}
						});
					}
				});
			}
		}else{
			$.messager.alert('提示','请选择你要删除的字典!','warning');
		}
	}else if(type=='tab'){   //删除选项卡
		var row = $('#tabGrid').datagrid('getSelected');
		if (row){
			if(row.isopt==1){
				$.messager.alert('提示','你没有权限删除系统级配置!','warning');
			}else{
				$.messager.confirm('提示','你确定删除【'+row.tabName+'】?',function(r){
					if (r){
						$.ajax({
							type: "POST",
							url: "${pageContext.request.contextPath}/tab!deleteTab",
							data: {id:row.id},
							success: function(data){
								var obj = eval('('+data+')');
								if (obj.result==1){
									$('#tabGrid').datagrid('reload');    // reload the user data
								} else {
									$.messager.show({    // show error message
										title: '提示',
										msg: obj.errorMsg
									});
								}
							}
						});
					}
				});
			}
		}else{
			$.messager.alert('提示','请选择你要删除的选项卡!','warning');
		}
	}
}


/****
 * 定义保存操作
 * */
opt.save = function(type){
	if(type=='menu'){   //菜单保存

	}else if(type=='org'){  //保存机构机构

	}else if(type=='user'){   //保存用户
	}else if(type=='button'){   //保存按键
	}else if(type=='role'){   //保存角色
	}else if(type=='roleGroup'){  //保存角色组
	}else if(type=='config'){   //保存字典
	}else if(type=='tab'){  //选项卡
		var url1 = "${pageContext.request.contextPath}/tab!saveTab";
		$('#fmTab').form('submit', {
			url: url1,
			type: 'POST',
			onSubmit: function() {
			return $(this).form('validate');
		},
		success: function(data) {
			var obj = eval('(' + data + ')');
			if (obj.result == 1) {
				$('#addTabId').dialog('close');
				$('#tabGrid').datagrid("reload");
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			} else {
				$.messager.show({
					title: '提示',
					msg: obj.errorMsg
				});
			}
		}
		});
	}
}


/****
 * 定义更新操作
 * */
opt.edit = function(type){
	if(type=='menu'){   //菜单更新
		var node = treegrid.treegrid('getSelected');
		if (node && node.id) {
			if (editRow != undefined) {
				treegrid.treegrid('endEdit', editRow.id);
			}
			//选择编辑
			if (editRow == undefined) {
				treegrid.treegrid('beginEdit', node.id);
				editRow = node;
				$(".datagrid-editable-input").height(22);
				editType = 'edit';
			}
		} else {   //没有选择修改项
			$.messager.show({
				msg : '请选择一项进行修改！',
				title : '提示'
			});
		}
	}else if(type=='org'){  //更新组织机构
		var row = $('#orgTree').treegrid('getSelected');
		if(row){
			win.window('open');
			url = '${pageContext.request.contextPath}/org!loadOrgInfoByCode?orgCode='+row.orgCode;
			$('#fmId').form('load',url);
			//
			$('#comTreeId').combotree({
				url: '${pageContext.request.contextPath}/org!queryOrgComTree',
				required: true,
				onLoadSuccess : function(row, data) {
				var t = $(this);
				if (data) {
					$(data).each(function(index, d) {
						if (this.state == 'closed') {
							t.tree('expandAll');
						}
					});
				}
			}
			});
			
		}else{
			$.messager.alert('提示','请选择你要修改的组织机构!','warning');
		}
	}else if(type=='user'){   //更新用户
		var row = $("#list_data").datagrid("getSelected");
		if(row){
			win.dialog('open').dialog('setTitle','修改用户');
			win.form('clear');
			$("#loginName").addClass("bgcolor");
			$("#loginName").attr('readonly','readonly');
			win.form('clear');
			url = '${pageContext.request.contextPath}/user!loadUserByUserId?userId='+row.userId;
			win.form('load',url);
			var orgCodeHtml = $("#codeDIVId").html();
			/*
			var posurl = "${pageContext.request.contextPath}/position!queryPositionByCode?orgCode="+orgCodeHtml;
			$("#positioId").combobox({ 
				url:posurl,
				textField: "text",
				valueField: "id",
				multiple: false,
				onLoadSuccess: function () { 
					$("input[name='positionName']").combobox('setValue',row.positionId);
				}
			});
			*/
			

		}else{
			$.messager.alert('提示','请选择需要修改的用户!','warning');
			return false;
		}
	}else if(type=='button'){   //更新按键
		var row = $('#butgrid').datagrid('getSelected');
		if (row){
			$('#addButId').dialog('open').dialog('setTitle','修改');
			$("#titleId").html("修改按钮");
			url = '${pageContext.request.contextPath}/button!loadButtonById?buttonId='+row.buttonId;
			$('#fmbut').form('load',url);
		}else{
			$.messager.alert('提示','请选择你要删除的按钮!','warning');
		}
	}else if(type=='role'){   //更新角色
		var row = $('#rolegrid').datagrid('getSelected');
		if (row){
			$('#addRoleId').dialog('open').dialog('setTitle','修改');
			$("#titleId").html("修改角色");
			url = '${pageContext.request.contextPath}/role!loadRoleById?roleId='+row.roleId;
			$('#fmbut').form('load',url);
		}else{
			$.messager.alert('提示','请选择你要删除的角色!','warning');
		}
	}else if(type=='roleGroup'){  //更新角色组
		var row = $('#roleGroupgrid').datagrid('getSelected');
		if (row){
			$('#addRoleGroupId').dialog('open').dialog('setTitle','修改');
			$("#titleId").html("修改角色组");
			url = '${pageContext.request.contextPath}/rolegroup!loadRoleGroupById?groupId='+row.groupId;
			$('#fmbut').form('load',url);
		}else{
			$.messager.alert('提示','请选择你要删除的角色组!','warning');
		}
	}else if(type=='config'){   //更新字典
		var row = $('#dagrid').datagrid('getSelected');
		if (row){
			if(row.isopt==1){
				$.messager.alert('提示','你没有权限修改系统级配置!','warning');
			}else{
				$('#dlgson').dialog('open').dialog('setTitle','修改');
				$("#sonTitleId").html("修改字典");
				
				$.ajax({
					url : '${pageContext.request.contextPath}/config!loadSonConfigByCode',
					data : {
					dataCode : row.dataCode
				},
				success : function(data) {
					var obj = eval('('+data+')');
					if(obj.dataTypeCode=='cjw_1'){
						$("#dtId").hide();
					}else{
						$("#dtId").show();
					}
			 	 }
				});
				url = '${pageContext.request.contextPath}/config!loadSonConfigByCode?dataCode='+row.dataCode;
				$('#fmson').form('load',url);
			}
		}else{
			$.messager.alert('提示','请选择你要修改的字典!','warning');
		}
	}else if(type=='tab'){   //更新选项卡
		var row = $('#tabGrid').datagrid('getSelected');
		if (row){
			if(row.isopt==1){
				$.messager.alert('提示','你没有权限修改系统级配置!','warning');
			}else{
				$('#addTabId').show();
				$('#fmTab').form('clear');
				$("#titleId").html("修改选项卡");
				$('#tabId').val(row.id);
				$('#addTabId').dialog({
					title: '添加选项卡',
					width: 430,
					height: 300,
					padding: 5,
					modal: true,
					buttons: [{
						text: '提交',
						handler: function() {
						opt.save('tab');
					}
					},
					{
						text: '取消',
						handler: function() {
						$('#addTabId').dialog('close');
					}
					}]
				});
				
				
				$("#tabNameId").val(row.tabName);
				if(row.enabled==1){
					$("#isAbleId").attr("checked","checked");
				}else{
					$("#noAbleId").attr("checked","checked");
				}
			}
		}else{
			$.messager.alert('提示','请选择你要修改的选项卡!','warning');
		}
	}
}

/****
 * 定义刷新操作
 * */
opt.refresh = function(type){
	if(type=='menu'){   //菜单保存
	}else if(type=='org'){  //刷新机构
		$('#orgTree').treegrid("reload");
	}else if(type=='user'){   //刷新用户
		$('#list_data').datagrid('reload');
	}else if(type=='button'){   //刷新按键
		$('#butgrid').datagrid("reload");
	}else if(type=='role'){   //刷新角色
		$('#rolegrid').datagrid("reload");
	}else if(type=='roleGroup'){  //刷新角色组
		$('#roleGroupgrid').datagrid("reload");
	}else if(type=='config'){   //刷新字典
		$('#dagrid').datagrid('reload');
	}else if(type=='tab'){   //刷新字典
		$('#tabGrid').datagrid('reload');
	}
}

//定义核心人员
opt.corePerson = function(type){
	if(type=='user'){  
		$('#coreToUserId').show();
		$('#coreToUserId').dialog({ 
			title:"设置核心人员", 
			width:$(window).width() * 0.9,
			height:$(window).height() * 0.95,
			padding:5,
			modal:true,
			buttons: [{ 
				text: '提交', 
				handler: function() { 
				saveCorePerson();
			} 
			}, { 
				text: '取消', 
				handler: function() { 
				$('#coreToUserId').dialog('close'); 
			} 
			}] 
		}); 

		//获取部门树
		$('#coreUserTreeGrid').treegrid({
			url : '${pageContext.request.contextPath}/user!queryOrgsAndCodeUserTreeGrid?parCode=1',
			fit : true,
			animate:true,
			border : false,
			idField:'id',    
			treeField : 'orgName',
			onBeforeExpand:function(node,param){                         
			$('#coreUserTreeGrid').treegrid('options').url = "${pageContext.request.contextPath}/user!queryOrgsAndCodeUserTreeGrid?parCode="+node.id;  
		}, 
		onLoadSuccess : function(row, data) {  //加载展开
			var t = $(this);
			if (data) {
				$(data).each(function(index, d) {
					if (this.state == 'closed') {
						t.treegrid('expandAll');
					}
				});
			}
		}
		});
	}
}
