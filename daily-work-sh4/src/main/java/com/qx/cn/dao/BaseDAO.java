package com.qx.cn.dao;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * BaseDAO 接口
 * 
 * @author 00fly
 * @version [版本号, 2018年8月8日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface BaseDAO
{
    /**
     * 查询记录条数
     * 
     * @param clazz
     * @return
     * @see [类、类#方法、类#成员]
     */
    public long count(Class clazz);
    
    /**
     * 查询记录条数
     * 
     * @param hql
     * @return
     * @see [类、类#方法、类#成员]
     */
    public long countByHql(String countHql, Object... args);
    
    /**
     * 删除
     * 
     * @param hql
     * @see [类、类#方法、类#成员]
     */
    public int deleteByHql(String hql, Object... args);
    
    /**
     * 根据id删除
     * 
     * @param clazz
     * @param id
     * @see [类、类#方法、类#成员]
     */
    public void deleteById(Class clazz, Serializable id);
    
    public Object loadByHql(String hql, Object... args);
    
    public Object loadById(Class clazz, Serializable id);
    
    public List query(Class clazz);
    
    public List query(String hql, int page, int rows, Object[] args);
    
    public List query(String hql, Object... args);
    
    public List queryBySql(String sql, Object... args);
    
    public void saveOrUpdate(Object obj);
    
    public int update(String hql, Object... args);
    
}