package com.qx.cn.model;

import java.io.Serializable;

public class SendTime implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 859603284515370951L;
    
    private Integer id;
    
    private String startTime;
    
    private String endTime;
    
    public Integer getId()
    {
        return this.id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getStartTime()
    {
        return this.startTime;
    }
    
    public void setStartTime(String startTime)
    {
        this.startTime = startTime;
    }
    
    public String getEndTime()
    {
        return this.endTime;
    }
    
    public void setEndTime(String endTime)
    {
        this.endTime = endTime;
    }
}