package com.qx.cn.model;

import java.io.Serializable;

public class Tab implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -8560765670104305790L;
    
    private Long id;
    
    private String tabName;
    
    private Integer enabled;
    
    private Integer isopt;
    
    private String memo;
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public String getTabName()
    {
        return this.tabName;
    }
    
    public void setTabName(String tabName)
    {
        this.tabName = tabName;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public Integer getIsopt()
    {
        return this.isopt;
    }
    
    public void setIsopt(Integer isopt)
    {
        this.isopt = isopt;
    }
    
    public String getMemo()
    {
        return this.memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
}