package com.qx.cn.model;

import java.io.Serializable;

public class WorkSendGroup implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 5636671475112903001L;
    
    private Integer id;
    
    private String groupName;
    
    private String senderIds;
    
    private String workerIds;
    
    private String creatTime;
    
    private Integer enabled;
    
    private String memo;
    
    private Integer timeTip;
    
    public Integer getId()
    {
        return this.id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getGroupName()
    {
        return this.groupName;
    }
    
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }
    
    public String getWorkerIds()
    {
        return this.workerIds;
    }
    
    public void setWorkerIds(String workerIds)
    {
        this.workerIds = workerIds;
    }
    
    public String getCreatTime()
    {
        return this.creatTime;
    }
    
    public void setCreatTime(String creatTime)
    {
        this.creatTime = creatTime;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMemo()
    {
        return this.memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
    
    public String getSenderIds()
    {
        return this.senderIds;
    }
    
    public void setSenderIds(String senderIds)
    {
        this.senderIds = senderIds;
    }
    
    public Integer getTimeTip()
    {
        return this.timeTip;
    }
    
    public void setTimeTip(Integer timeTip)
    {
        this.timeTip = timeTip;
    }
}