package com.qx.cn.model;

import java.io.Serializable;

public class SendDate implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -1016017090596306799L;
    
    private Integer id;
    
    private String name;
    
    private String weekDate;
    
    private String memo;
    
    private String type;
    
    public Integer getId()
    {
        return this.id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String getMemo()
    {
        return this.memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
    
    public String getWeekDate()
    {
        return this.weekDate;
    }
    
    public void setWeekDate(String weekDate)
    {
        this.weekDate = weekDate;
    }
    
    public String getType()
    {
        return type;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
}