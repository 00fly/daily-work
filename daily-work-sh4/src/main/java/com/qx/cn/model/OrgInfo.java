package com.qx.cn.model;

import java.io.Serializable;

public class OrgInfo implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = -6976785159361402021L;
    
    private Long orgId;
    
    private String orgCode;
    
    private String orgName;
    
    private String orgDuty;
    
    private Integer enabled;
    
    private String memo;
    
    private Long orgLeader;
    
    private String orgPId;
    
    public String getOrgPId()
    {
        return this.orgPId;
    }
    
    public void setOrgPId(String orgPId)
    {
        this.orgPId = orgPId;
    }
    
    public Long getOrgId()
    {
        return this.orgId;
    }
    
    public void setOrgId(Long orgId)
    {
        this.orgId = orgId;
    }
    
    public String getOrgCode()
    {
        return this.orgCode;
    }
    
    public void setOrgCode(String orgCode)
    {
        this.orgCode = orgCode;
    }
    
    public String getOrgName()
    {
        return this.orgName;
    }
    
    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }
    
    public String getOrgDuty()
    {
        return this.orgDuty;
    }
    
    public void setOrgDuty(String orgDuty)
    {
        this.orgDuty = orgDuty;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMemo()
    {
        return this.memo;
    }
    
    public void setMemo(String memo)
    {
        this.memo = memo;
    }
    
    public Long getOrgLeader()
    {
        return this.orgLeader;
    }
    
    public void setOrgLeader(Long orgLeader)
    {
        this.orgLeader = orgLeader;
    }
}