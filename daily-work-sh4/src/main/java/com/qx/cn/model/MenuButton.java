package com.qx.cn.model;

import java.io.Serializable;

public class MenuButton implements Serializable
{
    /**
     * 注释内容
     */
    private static final long serialVersionUID = 2156054818758814390L;
    
    private Long id;
    
    private String menuId;
    
    private Long buttonId;
    
    public String getMenuId()
    {
        return this.menuId;
    }
    
    public void setMenuId(String menuId)
    {
        this.menuId = menuId;
    }
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public Long getButtonId()
    {
        return this.buttonId;
    }
    
    public void setButtonId(Long buttonId)
    {
        this.buttonId = buttonId;
    }
}