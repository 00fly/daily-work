package com.qx.cn.tool;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import com.sun.crypto.provider.SunJCE;

public class EncrypDES
{
    private KeyGenerator keygen;
    
    private SecretKey deskey;
    
    private Cipher c;
    
    private byte[] cipherByte;
    
    public EncrypDES()
        throws NoSuchAlgorithmException, NoSuchPaddingException
    {
        Security.addProvider(new SunJCE());
        this.keygen = KeyGenerator.getInstance("DES");
        this.deskey = this.keygen.generateKey();
        this.c = Cipher.getInstance("DES");
    }
    
    public byte[] Encrytor(String str)
        throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        this.c.init(1, this.deskey);
        byte[] src = str.getBytes();
        this.cipherByte = this.c.doFinal(src);
        return this.cipherByte;
    }
    
    public byte[] Decryptor(byte[] buff)
        throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException
    {
        this.c.init(2, this.deskey);
        this.cipherByte = this.c.doFinal(buff);
        return this.cipherByte;
    }
}