package com.qx.cn.vo;

public class ButtonVO
{
    private Long buttonId;
    
    private String buttonName;
    
    private String buttonIconImg;
    
    private Integer enabled;
    
    private String methodName;
    
    private Integer sign;
    
    public String getButtonName()
    {
        return this.buttonName;
    }
    
    public void setButtonName(String buttonName)
    {
        this.buttonName = buttonName;
    }
    
    public String getButtonIconImg()
    {
        return this.buttonIconImg;
    }
    
    public void setButtonIconImg(String buttonIconImg)
    {
        this.buttonIconImg = buttonIconImg;
    }
    
    public Integer getEnabled()
    {
        return this.enabled;
    }
    
    public void setEnabled(Integer enabled)
    {
        this.enabled = enabled;
    }
    
    public String getMethodName()
    {
        return this.methodName;
    }
    
    public void setMethodName(String methodName)
    {
        this.methodName = methodName;
    }
    
    public Integer getSign()
    {
        return this.sign;
    }
    
    public void setSign(Integer sign)
    {
        this.sign = sign;
    }
    
    public Long getButtonId()
    {
        return this.buttonId;
    }
    
    public void setButtonId(Long buttonId)
    {
        this.buttonId = buttonId;
    }
}