package com.qx.cn.vo;

public class UtilVO
{
    private Long id;
    
    private String methodCode;
    
    private Long userId;
    
    private Long departId;
    
    private Long positionId;
    
    private String userName;
    
    private Long processId;
    
    private int sign;
    
    private String title;
    
    private String content;
    
    private String utilDate;
    
    private String signNode;
    
    private String busiSign;
    
    private String type;
    
    private String userVoAccount;
    
    public String getUserName()
    {
        return this.userName;
    }
    
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    
    public int getSign()
    {
        return this.sign;
    }
    
    public void setSign(int sign)
    {
        this.sign = sign;
    }
    
    public String getSignNode()
    {
        return this.signNode;
    }
    
    public void setSignNode(String signNode)
    {
        this.signNode = signNode;
    }
    
    public String getMethodCode()
    {
        return this.methodCode;
    }
    
    public void setMethodCode(String methodCode)
    {
        this.methodCode = methodCode;
    }
    
    public Long getUserId()
    {
        return this.userId;
    }
    
    public void setUserId(Long userId)
    {
        this.userId = userId;
    }
    
    public Long getPositionId()
    {
        return this.positionId;
    }
    
    public void setPositionId(Long positionId)
    {
        this.positionId = positionId;
    }
    
    public Long getProcessId()
    {
        return this.processId;
    }
    
    public void setProcessId(Long processId)
    {
        this.processId = processId;
    }
    
    public String getTitle()
    {
        return this.title;
    }
    
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    public String getContent()
    {
        return this.content;
    }
    
    public void setContent(String content)
    {
        this.content = content;
    }
    
    public String getUtilDate()
    {
        return this.utilDate;
    }
    
    public void setUtilDate(String utilDate)
    {
        this.utilDate = utilDate;
    }
    
    public Long getDepartId()
    {
        return this.departId;
    }
    
    public void setDepartId(Long departId)
    {
        this.departId = departId;
    }
    
    public Long getId()
    {
        return this.id;
    }
    
    public void setId(Long id)
    {
        this.id = id;
    }
    
    public String getBusiSign()
    {
        return this.busiSign;
    }
    
    public void setBusiSign(String busiSign)
    {
        this.busiSign = busiSign;
    }
    
    public String getUserVoAccount()
    {
        return this.userVoAccount;
    }
    
    public void setUserVoAccount(String userVoAccount)
    {
        this.userVoAccount = userVoAccount;
    }
    
    public String getType()
    {
        return this.type;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
}