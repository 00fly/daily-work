package com.qx.cn.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.Button;
import com.qx.cn.model.Menu;
import com.qx.cn.model.MenuButton;
import com.qx.cn.model.MenuTabs;
import com.qx.cn.model.RoleMenu;
import com.qx.cn.model.Tab;
import com.qx.cn.model.User;
import com.qx.cn.model.UserRole;
import com.qx.cn.model.UserRoleGroup;
import com.qx.cn.service.ButtonService;
import com.qx.cn.service.MenuButtonService;
import com.qx.cn.service.MenuService;
import com.qx.cn.service.MenuTabService;
import com.qx.cn.service.RoleMenuService;
import com.qx.cn.service.TabService;
import com.qx.cn.service.UserRoleGroupService;
import com.qx.cn.service.UserRoleService;
import com.qx.cn.tool.ReDupList;
import com.qx.cn.tool.SortTool;
import com.qx.cn.tool.TreeNode;
import com.qx.cn.vo.MenuVO;

/**
 * 
 * <一句话功能简述> <功能详细描述>
 * 
 * @author 0001
 * @version [版本号, 2018年8月18日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@RequestMapping("/menu")
public class MenuController
{
    @Autowired
    HttpServletRequest request;
    
    @Autowired
    private TabService tabService;
    
    @Autowired
    private ButtonService buttonService;
    
    @Autowired
    private MenuService menuService;
    
    @Autowired
    private MenuButtonService menuButtonService;
    
    @Autowired
    private MenuTabService menuTabService;
    
    @Autowired
    private RoleMenuService roleMenuService;
    
    @Autowired
    private UserRoleService userRoleService;
    
    @Autowired
    private UserRoleGroupService userRoleGroupService;
    
    @ResponseBody
    @RequestMapping(value = "/queryMenuTreeGrid", method = RequestMethod.POST)
    public List<MenuVO> queryMenuTreeGrid(String id)
    {
        Menu menu = new Menu();
        menu.setMenuCode(id);
        return menuService.queryTreeGrid(menu);
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryMenusAndTabTreeGrid")
    public JSONArray queryMenusAndTabTreeGrid(String parCode, String roleId)
    {
        String sta = "open";
        List<Menu> menuInfos = menuService.queryMenuListByRoleAsc(parCode);
        JSONArray jsonArray = new JSONArray();
        for (Menu menu : menuInfos)
        {
            RoleMenu roleMenu = roleMenuService.loadRoleMenuByCode(roleId, menu.getMenuCode());
            JSONObject obj = new JSONObject();
            if (roleMenu != null)
            {
                List<Menu> sonMenus = menuService.queryAllMenuListByAsc(menu.getMenuCode());
                if (sonMenus.size() > 1)
                {
                    sta = "closed";
                }
                else
                {
                    sta = "open";
                }
                obj.put("state", sta);
                obj.put("id", menu.getMenuCode());
                obj.put("menuName", menu.getMenuName());
                obj.put("menuURL", menu.getMenuURL());
                obj.put("enabled", menu.getEnabled());
                obj.put("menuSort", menu.getMenuSort());
                String selectopt = "";
                if (menu.getIsLeafMenu().equals(1))
                {
                    List<Tab> tabs = tabService.queryAllTabs();
                    List<MenuTabs> menutabses = menuTabService.queryMenuTabByCode(menu.getMenuCode());
                    for (Tab tab : tabs)
                    {
                        boolean flag = false;
                        for (MenuTabs mtab : menutabses)
                        {
                            if (tab.getId().equals(mtab.getTabId()))
                            {
                                flag = true;
                            }
                        }
                        if (flag)
                        {
                            boolean deflag = roleMenuService.loadRoleMenuByAllTabs(roleId, menu.getMenuCode(), tab.getId());
                            if (deflag)
                            {
                                selectopt = selectopt + "<span style='width:100px;'><input type='checkbox' checked='checked' class='tab" + menu.getMenuCode() + " tab1s tab" + parCode + "' value='"
                                    + menu.getMenuCode() + "," + tab.getId() + "'/>" + tab.getTabName() + "</span>&nbsp;&nbsp;";
                            }
                            else
                            {
                                selectopt = selectopt + "<span style='width:100px;'><input type='checkbox' class='tab" + menu.getMenuCode() + " tab1s tab" + parCode + "' value='" + menu.getMenuCode()
                                    + "," + tab.getId() + "'/>" + tab.getTabName() + "</span>&nbsp;&nbsp;";
                            }
                        }
                        else
                        {
                            selectopt = selectopt + "<span style='width:100px;color:#EAEAEA'><input disabled='disabled' type='checkbox' value='" + tab.getId() + "'/>" + tab.getTabName()
                                + "</span>&nbsp;&nbsp;";
                        }
                    }
                    obj.put("selectTab", selectopt);
                }
                else
                {
                    obj.put("selectTab", "");
                }
                jsonArray.add(obj);
            }
        }
        return jsonArray;
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryMenusAndButTreeGrid")
    public JSONArray queryMenusAndButTreeGrid(String parCode, String roleId)
    {
        String sta = "open";
        List<Menu> menuInfos = menuService.queryMenuListByRoleAsc(parCode);
        JSONArray jsonArray = new JSONArray();
        for (Menu menu : menuInfos)
        {
            RoleMenu roleMenu = roleMenuService.loadRoleMenuByCode(roleId, menu.getMenuCode());
            JSONObject obj = new JSONObject();
            if (roleMenu != null)
            {
                List<Menu> sonMenus = menuService.queryAllMenuListByAsc(menu.getMenuCode());
                if (sonMenus.size() > 1)
                {
                    sta = "closed";
                }
                else
                {
                    sta = "open";
                }
                obj.put("state", sta);
                obj.put("id", menu.getMenuCode());
                obj.put("menuName", menu.getMenuName());
                obj.put("menuURL", menu.getMenuURL());
                obj.put("enabled", menu.getEnabled());
                String selectopt = "";
                if (menu.getIsLeafMenu().equals(1))
                {
                    List<Button> buttons = buttonService.queryAllButtons();
                    List<MenuButton> menuButtons = menuButtonService.queryMenuButtonByCode(menu.getMenuCode());
                    for (Button but : buttons)
                    {
                        boolean flag = false;
                        for (MenuButton mButton : menuButtons)
                        {
                            if (but.getButtonId().equals(mButton.getButtonId()))
                            {
                                flag = true;
                            }
                        }
                        if (flag)
                        {
                            boolean deflag = roleMenuService.loadRoleMenuByAll(roleId, menu.getMenuCode(), but.getButtonId());
                            if (deflag)
                            {
                                selectopt = selectopt + "<span style='width:100px;'><input type='checkbox' checked='checked' class='but" + menu.getMenuCode() + " buttons but" + parCode + "' value='"
                                    + menu.getMenuCode() + "," + but.getButtonId() + "'/>" + but.getButtonName() + "</span>&nbsp;&nbsp;";
                            }
                            else
                            {
                                selectopt = selectopt + "<span style='width:100px;'><input type='checkbox' class='but" + menu.getMenuCode() + " buttons but" + parCode + "' value='"
                                    + menu.getMenuCode() + "," + but.getButtonId() + "'/>" + but.getButtonName() + "</span>&nbsp;&nbsp;";
                            }
                        }
                        else
                        {
                            selectopt = selectopt + "<span style='width:100px;color:#EAEAEA'><input disabled='disabled' type='checkbox' value='" + but.getButtonId() + "'/>" + but.getButtonName()
                                + "</span>&nbsp;&nbsp;";
                        }
                    }
                    obj.put("selectopt", selectopt);
                }
                else
                {
                    obj.put("selectopt", "");
                }
                jsonArray.add(obj);
            }
        }
        return jsonArray;
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryAllMenuButtons")
    public JSONObject queryAllMenuButtons()
    {
        JSONObject object = new JSONObject();
        List<Button> buttons = buttonService.queryAllButtons();
        // actionContext("buttons", buttons);
        object.put("result", 1);
        object.put("datas", JSON.toJSON(buttons));
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/menuparentTree")
    public List<TreeNode> menuparentTree()
    {
        Menu menu = new Menu();
        return menuService.queryComTree(menu, true);
    }
    
    @ResponseBody
    @RequestMapping(value = "/addMenuInfo")
    public JSONObject addMenuInfo(Long menuId, String menuCode, String parCode, String menuName, String menuURL, Integer isLeafMenu, Integer enabled, String menuDesc, Integer menuSort)
    {
        JSONObject object = new JSONObject();
        if (menuId != null)
        {
            List<Menu> menuSizes = menuService.queryMenuListByAsc(menuCode);
            if (menuSizes.size() > 0)
            {
                object.put("result", 2);
                object.put("errorMsg", "该菜单有子菜单不可以修改!");
            }
            else
            {
                Menu reMenu = menuService.loadMenuByCode(menuCode);
                List<Menu> menus = menuService.queryAllMenuListByAsc(parCode);
                reMenu.setMenuName(menuName);
                reMenu.setMenuURL(menuURL);
                reMenu.setEnabled(enabled);
                reMenu.setIsLeafMenu(isLeafMenu);
                reMenu.setMenuDesc(menuDesc);
                reMenu.setMenuSort(menuSort);
                if (menus.size() == 1)
                {
                    reMenu.setMenuCode(parCode + "001");
                }
                else if ("1".equals(parCode))
                {
                    List<Menu> menus2 = menuService.queryMenuListByAsc("1");
                    StringBuilder codeBuf = new StringBuilder();
                    String codeStr = ((Menu)menus2.get(menus2.size() - 1)).getMenuCode();
                    String startCodeStr = codeStr.substring(0, codeStr.length() - 4);
                    String endCodeStr = codeStr.substring(codeStr.length() - 4, codeStr.length());
                    String endCodeInt = String.valueOf(Integer.valueOf(endCodeStr).intValue() + 1);
                    String endStr = "";
                    if (endCodeInt.length() == 1)
                    {
                        endStr = "000" + endCodeInt;
                    }
                    else if (endCodeInt.length() == 2)
                    {
                        endStr = "00" + endCodeInt;
                    }
                    else if (endCodeInt.length() == 3)
                    {
                        endStr = "0" + endCodeInt;
                    }
                    else if (endCodeInt.length() == 4)
                    {
                        endStr = endCodeInt;
                    }
                    codeBuf.append(startCodeStr).append(endStr);
                    reMenu.setMenuCode(codeBuf.toString());
                }
                else
                {
                    StringBuilder codeBuf = new StringBuilder();
                    String codeStr = ((Menu)menus.get(menus.size() - 1)).getMenuCode();
                    String startCodeStr = codeStr.substring(0, codeStr.length() - 4);
                    String endCodeStr = codeStr.substring(codeStr.length() - 4, codeStr.length());
                    String endCodeInt = String.valueOf(Integer.valueOf(endCodeStr).intValue() + 1);
                    String endStr = "";
                    if (endCodeInt.length() == 1)
                    {
                        endStr = "000" + endCodeInt;
                    }
                    else if (endCodeInt.length() == 2)
                    {
                        endStr = "00" + endCodeInt;
                    }
                    else if (endCodeInt.length() == 3)
                    {
                        endStr = "0" + endCodeInt;
                    }
                    else if (endCodeInt.length() == 4)
                    {
                        endStr = endCodeInt;
                    }
                    codeBuf.append(startCodeStr).append(endStr);
                    reMenu.setMenuCode(codeBuf.toString());
                }
                menuService.saveMenu(reMenu);
                object.put("result", 1);
                object.put("errorMsg", "添加成功!请手动刷新左侧功能菜单树!");
            }
        }
        else
        {
            Menu menu = new Menu();
            menu.setMenuName(menuName);
            menu.setMenuURL(menuURL);
            menu.setEnabled(enabled);
            menu.setIsLeafMenu(isLeafMenu);
            menu.setMenuDesc(menuDesc);
            menu.setMenuSort(menuSort);
            List<Menu> menus = menuService.queryAllMenuListByAsc(parCode);
            if (menus.size() == 1)
            {
                menu.setMenuCode(parCode + "001");
            }
            else if ("1".equals(parCode))
            {
                List<Menu> menus2 = menuService.queryMenuListByAsc("1");
                StringBuilder codeBuf = new StringBuilder();
                String codeStr = ((Menu)menus2.get(menus2.size() - 1)).getMenuCode();
                String startCodeStr = codeStr.substring(0, codeStr.length() - 4);
                String endCodeStr = codeStr.substring(codeStr.length() - 4, codeStr.length());
                String endCodeInt = String.valueOf(Integer.valueOf(endCodeStr).intValue() + 1);
                String endStr = "";
                if (endCodeInt.length() == 1)
                {
                    endStr = "000" + endCodeInt;
                }
                else if (endCodeInt.length() == 2)
                {
                    endStr = "00" + endCodeInt;
                }
                else if (endCodeInt.length() == 3)
                {
                    endStr = "0" + endCodeInt;
                }
                else if (endCodeInt.length() == 4)
                {
                    endStr = endCodeInt;
                }
                codeBuf.append(startCodeStr).append(endStr);
                menu.setMenuCode(codeBuf.toString());
            }
            else
            {
                StringBuilder codeBuf = new StringBuilder();
                String codeStr = ((Menu)menus.get(menus.size() - 1)).getMenuCode();
                String startCodeStr = codeStr.substring(0, codeStr.length() - 4);
                String endCodeStr = codeStr.substring(codeStr.length() - 4, codeStr.length());
                String endCodeInt = String.valueOf(Integer.valueOf(endCodeStr).intValue() + 1);
                String endStr = "";
                if (endCodeInt.length() == 1)
                {
                    endStr = "000" + endCodeInt;
                }
                else if (endCodeInt.length() == 2)
                {
                    endStr = "00" + endCodeInt;
                }
                else if (endCodeInt.length() == 3)
                {
                    endStr = "0" + endCodeInt;
                }
                else if (endCodeInt.length() == 4)
                {
                    endStr = endCodeInt;
                }
                codeBuf.append(startCodeStr).append(endStr);
                menu.setMenuCode(codeBuf.toString());
            }
            menuService.saveMenu(menu);
            object.put("result", 1);
            object.put("errorMsg", "添加成功!请手动刷新左侧功能菜单树!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/deleteMenu")
    public JSONObject deleteMenu(String menuCode)
    {
        menuTabService.deleteMenuTab(menuCode);
        boolean flag = menuService.deleteMenuByCode(menuCode);
        boolean flag1 = roleMenuService.deleteRoleMenusByCode(menuCode);
        boolean flag2 = menuButtonService.deleteMenuButton(menuCode);
        JSONObject object = new JSONObject();
        if ((flag) && (flag2) && (flag1))
        {
            object.put("result", 1);
            object.put("errorMsg", "删除菜单成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除菜单失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/editMenuInfo")
    public JSONObject editMenuInfo(Long menuId, String menuCode, String parCode, String menuName, String menuURL, Integer isLeafMenu, Integer enabled, String menuDesc, Integer menuSort)
    {
        JSONObject object = new JSONObject();
        MenuVO menuVO = new MenuVO();
        menuVO.setMenuCode(menuCode);
        menuVO.setMenuName(menuName);
        menuVO.setMenuURL(menuURL);
        menuVO.setEnabled(enabled);
        menuVO.setIsLeafMenu(isLeafMenu);
        menuVO.setMenuDesc(menuDesc);
        menuVO.setMenuId(menuId);
        menuVO.setMenuSort(menuSort);
        boolean flag = menuService.updateMenuInfo(menuVO);
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "修改菜单成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "修改菜单失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveButToMenu")
    public JSONObject saveButToMenu(Long menuId, String strBuffer)
    {
        boolean flag = true;
        JSONObject object = new JSONObject();
        String menuIdStr = String.valueOf(menuId);
        if ("".equals(strBuffer))
        {
            menuButtonService.deleteMenuButton(menuIdStr);
        }
        else
        {
            String[] butId = strBuffer.split(",");
            List<MenuButton> menuButtons = menuButtonService.queryMenuButtonByCode(menuIdStr);
            if (menuButtons.size() > 0)
            {
                boolean flag2 = menuButtonService.deleteMenuButton(menuIdStr);
                if (flag2)
                {
                    for (int i = 0; i < butId.length; i++)
                    {
                        MenuButton menuButton = new MenuButton();
                        menuButton.setMenuId(menuIdStr);
                        menuButton.setButtonId(Long.valueOf(butId[i]));
                        boolean flag1 = menuButtonService.saveMenuButtin(menuButton).booleanValue();
                        if (!flag1)
                        {
                            flag = false;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < butId.length; i++)
                {
                    MenuButton menuButton = new MenuButton();
                    menuButton.setMenuId(menuIdStr);
                    menuButton.setButtonId(Long.valueOf(butId[i]));
                    boolean flag1 = menuButtonService.saveMenuButtin(menuButton).booleanValue();
                    if (!flag1)
                    {
                        flag = false;
                    }
                }
            }
        }
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置菜单按钮成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置菜单按钮失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveTabToMenu")
    public JSONObject saveTabToMenu(Long menuId, String strBuffer)
    {
        boolean flag = true;
        JSONObject object = new JSONObject();
        String menuIdStr = String.valueOf(menuId);
        if ("".equals(strBuffer))
        {
            menuTabService.deleteMenuTab(menuIdStr);
        }
        else
        {
            String[] tabId = strBuffer.split(",");
            List<MenuTabs> menuTabses = menuTabService.queryMenuTabByCode(menuIdStr);
            if (menuTabses.size() > 0)
            {
                boolean flag2 = menuTabService.deleteMenuTab(menuIdStr);
                if (flag2)
                {
                    for (int i = 0; i < tabId.length; i++)
                    {
                        MenuTabs menuTab = new MenuTabs();
                        menuTab.setMenuId(menuIdStr);
                        menuTab.setTabId(Long.valueOf(tabId[i]));
                        boolean flag1 = menuTabService.saveMenuTab(menuTab).booleanValue();
                        if (!flag1)
                        {
                            flag = false;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < tabId.length; i++)
                {
                    MenuTabs menuTab = new MenuTabs();
                    menuTab.setMenuId(menuIdStr);
                    menuTab.setTabId(Long.valueOf(tabId[i]));
                    boolean flag1 = menuTabService.saveMenuTab(menuTab).booleanValue();
                    if (!flag1)
                    {
                        flag = false;
                    }
                }
            }
        }
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置菜单选项卡成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置菜单选项卡失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryMenuTreeToRole")
    public JSONArray queryMenuTreeToRole(String roleId, String parCode)
    {
        String sta;
        List<Menu> menuInfos = menuService.queryMenuListByRoleAsc(parCode);
        JSONArray jsonArray = new JSONArray();
        for (Menu menu : menuInfos)
        {
            JSONObject obj = new JSONObject();
            List<Menu> sonMenus = menuService.queryAllMenuListByAsc(menu.getMenuCode());
            if (sonMenus.size() > 1)
            {
                sta = "closed";
            }
            else
            {
                sta = "open";
            }
            obj.put("state", sta);
            obj.put("id", menu.getMenuCode());
            obj.put("text", menu.getMenuName());
            RoleMenu roleMenu = roleMenuService.loadRoleMenuByCode(roleId, menu.getMenuCode());
            if (roleMenu != null)
            {
                obj.put("checked", Boolean.valueOf(true));
            }
            jsonArray.add(obj);
        }
        return jsonArray;
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryAllMenusToTree")
    public JSONArray queryAllMenusToTree(String parCode)
    {
        User user = (User)request.getSession().getAttribute("user");
        List<UserRole> userRoles = userRoleService.queryRolesByUserId(user.getUserId());
        List<String> list = new ArrayList<>();
        for (UserRole userRole : userRoles)
        {
            list.add(String.valueOf(userRole.getRoleId()));
        }
        List<UserRoleGroup> userRoleGroups = userRoleGroupService.queryGroupsByUserId(user.getUserId());
        for (UserRoleGroup userRoleGroup : userRoleGroups)
        {
            List<UserRole> userRole1s = userRoleService.queryRolesByUserId(userRoleGroup.getUserId());
            for (UserRole userRole : userRole1s)
            {
                list.add(String.valueOf(userRole.getRoleId()));
            }
        }
        List<String> list2 = ReDupList.removeDuplicate(list);
        List<String> menuList = new ArrayList<>();
        for (String roleStr : list2)
        {
            List<RoleMenu> roleMenus = roleMenuService.queryRoleMenusById(roleStr);
            for (RoleMenu roleMenu : roleMenus)
            {
                menuList.add(roleMenu.getMenuId());
            }
        }
        List<String> menuList2 = ReDupList.removeDuplicate(menuList);
        List<Menu> menuVOs = new ArrayList<>();
        for (String mls : menuList2)
        {
            if (mls.substring(0, mls.length() - 3).equals(parCode))
            {
                Menu menu = menuService.loadMenuByCode(mls);
                if (menu != null)
                {
                    menuVOs.add(menu);
                }
            }
        }
        List<Menu> nMenus = SortTool.Sortlist(menuVOs);
        JSONArray jsonArray = new JSONArray();
        String sta;
        for (Menu mu : nMenus)
        {
            JSONObject obj = new JSONObject();
            obj.put("id", mu.getMenuCode());
            obj.put("text", mu.getMenuName());
            obj.put("menuURL", mu.getMenuURL().replace("!", "/"));
            obj.put("tnode", mu.getIsLeafMenu());
            boolean flag = false;
            for (String mlson : menuList2)
            {
                if (mlson.substring(0, mlson.length() - 3).equals(mu.getMenuCode()))
                {
                    flag = true;
                    break;
                }
            }
            if (flag)
            {
                sta = "closed";
            }
            else
            {
                sta = "open";
            }
            obj.put("state", sta);
            jsonArray.add(obj);
        }
        return jsonArray;
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryAllMenusToUser")
    public JSONObject queryAllMenusToUser(String menuCode)
    {
        User user = (User)request.getSession().getAttribute("user");
        List<UserRole> userRoles = userRoleService.queryRolesByUserId(user.getUserId());
        List<String> list = new ArrayList<String>();
        for (UserRole userRole : userRoles)
        {
            list.add(String.valueOf(userRole.getRoleId()));
        }
        List<UserRoleGroup> userRoleGroups = userRoleGroupService.queryGroupsByUserId(user.getUserId());
        for (UserRoleGroup userRoleGroup : userRoleGroups)
        {
            List<UserRole> userRole1s = userRoleService.queryRolesByUserId(userRoleGroup.getUserId());
            for (UserRole userRole : userRole1s)
            {
                list.add(String.valueOf(userRole.getRoleId()));
            }
        }
        List<String> list2 = ReDupList.removeDuplicate(list);
        List<String> menuList = new ArrayList<String>();
        for (String roleStr : list2)
        {
            List<RoleMenu> roleMenus = roleMenuService.queryRoleMenusById(roleStr);
            for (RoleMenu roleMenu : roleMenus)
            {
                menuList.add(roleMenu.getMenuId());
            }
        }
        List<String> menuList2 = ReDupList.removeDuplicate(menuList);
        Collections.sort(menuList2);
        JSONArray jsonArray = new JSONArray();
        JSONObject obj = new JSONObject();
        for (String mls : menuList2)
        {
            if ("".equals(menuCode))
            {
                if (mls.length() == 4)
                {
                    JSONObject object = new JSONObject();
                    Menu menu = menuService.loadMenuByCode(mls);
                    object.put("menuCode", menu.getMenuCode());
                    object.put("menuName", menu.getMenuName());
                    object.put("menuURL", menu.getMenuURL());
                    jsonArray.add(object);
                }
            }
            else if (mls.substring(0, mls.length() - 3).equals(menuCode))
            {
                JSONObject object = new JSONObject();
                Menu menu = menuService.loadMenuByCode(mls);
                object.put("menuCode", menu.getMenuCode());
                object.put("menuName", menu.getMenuName());
                object.put("menuURL", menu.getMenuURL());
                jsonArray.add(object);
            }
        }
        obj.put("datas", jsonArray);
        obj.put("result", 1);
        return obj;
    }
    
    @ResponseBody
    @RequestMapping(value = "/showHideTabs")
    public JSONObject showHideTabs(String menuType)
    {
        JSONObject obj = new JSONObject();
        Menu menu = menuService.loadMenuByType(menuType);
        User user = (User)request.getSession().getAttribute("user");
        List<UserRole> userRoles = userRoleService.queryRolesByUserId(user.getUserId());
        List<String> list = new ArrayList<>();
        for (UserRole userRole : userRoles)
        {
            list.add(String.valueOf(userRole.getRoleId()));
        }
        List<UserRoleGroup> userRoleGroups = userRoleGroupService.queryGroupsByUserId(user.getUserId());
        for (UserRoleGroup userRoleGroup : userRoleGroups)
        {
            List<UserRole> userRole1s = userRoleService.queryRolesByUserId(userRoleGroup.getUserId());
            for (UserRole userRole : userRole1s)
            {
                list.add(String.valueOf(userRole.getRoleId()));
            }
        }
        List<String> list2 = ReDupList.removeDuplicate(list);
        List<String> tabList = new ArrayList<String>();
        for (String roleStr : list2)
        {
            if (menu != null)
            {
                List<RoleMenu> roleMenus = roleMenuService.queryRoleTabsById(roleStr, menu.getMenuCode());
                for (RoleMenu roleMenu : roleMenus)
                {
                    if (roleMenu.getTabIds() != null)
                    {
                        String[] tabs = roleMenu.getTabIds().split(",");
                        for (int j = 0; j < tabs.length; j++)
                        {
                            tabList.add(tabs[j]);
                        }
                    }
                }
            }
        }
        JSONArray jsonArray = new JSONArray();
        StringBuilder tabBuffer = new StringBuilder();
        List<String> list3 = ReDupList.removeDuplicate(tabList);
        for (String tal : list3)
        {
            Tab tab = tabService.loadTabById(Long.valueOf(tal));
            if (tab != null)
            {
                JSONObject object = new JSONObject();
                object.put("tabName", tab.getTabName());
                jsonArray.add(object);
                tabBuffer.append(tab.getTabName()).append(",");
            }
        }
        obj.put("datas", jsonArray);
        obj.put("tabAll", tabBuffer.toString());
        obj.put("result", 1);
        return obj;
    }
    
    @ResponseBody
    @RequestMapping(value = "/loadButsByBusi")
    public JSONObject loadButsByBusi(String menuType)
    {
        JSONObject obj = new JSONObject();
        Menu menu = menuService.loadMenuByType(menuType);
        User user = (User)request.getSession().getAttribute("user");
        List<UserRole> userRoles = userRoleService.queryRolesByUserId(user.getUserId());
        List<String> list = new ArrayList<String>();
        for (UserRole userRole : userRoles)
        {
            list.add(String.valueOf(userRole.getRoleId()));
        }
        List<UserRoleGroup> userRoleGroups = userRoleGroupService.queryGroupsByUserId(user.getUserId());
        for (UserRoleGroup userRoleGroup : userRoleGroups)
        {
            List<UserRole> userRole1s = userRoleService.queryRolesByUserId(userRoleGroup.getUserId());
            for (UserRole userRole : userRole1s)
            {
                list.add(String.valueOf(userRole.getRoleId()));
            }
        }
        List<String> list2 = ReDupList.removeDuplicate(list);
        List<String> butList = new ArrayList<String>();
        for (String roleStr : list2)
        {
            if (menu != null)
            {
                List<RoleMenu> roleMenus = roleMenuService.queryRoleTabsById(roleStr, menu.getMenuCode());
                String[] buts;
                for (RoleMenu roleMenu : roleMenus)
                {
                    if (StringUtils.isEmpty(roleMenu.getButtonIds()))
                    {
                        continue;
                    }
                    buts = roleMenu.getButtonIds().split(",");
                    for (int j = 0; j < buts.length; j++)
                    {
                        butList.add(buts[j]);
                    }
                }
            }
        }
        StringBuilder butSb = new StringBuilder();
        List<String> list4 = ReDupList.removeDuplicate(butList);
        for (int z = 0; z < list4.size(); z++)
        {
            Button button = buttonService.loadButtonById(Long.valueOf((String)list4.get(z)));
            if (button != null)
            {
                butSb.append(button.getButtonName()).append(",");
            }
        }
        obj.put("toolObj", butSb.toString());
        obj.put("result", 1);
        return obj;
    }
}