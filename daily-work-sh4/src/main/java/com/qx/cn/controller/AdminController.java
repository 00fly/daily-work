package com.qx.cn.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.Menu;
import com.qx.cn.model.RoleMenu;
import com.qx.cn.model.User;
import com.qx.cn.model.UserRole;
import com.qx.cn.service.MenuService;
import com.qx.cn.service.RoleMenuService;
import com.qx.cn.service.UserRoleService;
import com.qx.cn.service.UserService;
import com.qx.cn.tool.MD5;
import com.qx.cn.tool.ReDupList;
import com.qx.cn.tool.SessionListener;

/**
 * AdminController
 * 
 * @author 00fly
 * @version [版本号, 2018-08-18]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@RequestMapping("/admin")
public class AdminController
{
    @Autowired
    HttpServletRequest request;
    
    @Autowired
    private UserRoleService userRoleService;
    
    @Autowired
    private RoleMenuService roleMenuService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private MenuService menuService;
    
    /**
     * 用户登录
     * 
     * @param userAccount
     * @param pwd
     * @return
     * @see [类、类#方法、类#成员]
     */
    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public JSONObject login(String userAccount, String pwd)
    {
        JSONObject object = new JSONObject();
        User user = userService.loadAdminByPwdAndAccount(userAccount, MD5.toMD5(pwd));
        if (user != null)
        {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            SessionListener.isAlreadyEnter(session, user.getUserId());
            Set<String> menuSet = new TreeSet<>();
            List<UserRole> userRoles = userRoleService.queryRolesByUserId(user.getUserId());
            List<String> list = new ArrayList<>();
            for (UserRole userRole : userRoles)
            {
                list.add(String.valueOf(userRole.getRoleId()));
            }
            List<String> list2 = ReDupList.removeDuplicate(list);
            for (String roleStr : list2)
            {
                List<RoleMenu> roleMenus = roleMenuService.queryRoleMenusById(roleStr);
                for (RoleMenu roleMenu : roleMenus)
                {
                    Menu menu = menuService.loadMenuByCode(roleMenu.getMenuId());
                    if (menu != null && StringUtils.isNotEmpty(menu.getMenuURL()))
                    {
                        menuSet.add(menu.getMenuURL());
                    }
                }
            }
            // 保存当前用户已分配菜单URL
            session.setAttribute("menuset", menuSet); // 构造返回的json数据
            if (!menuSet.isEmpty())
            {
                object.put("result", 1);
                return object;
            }
            object.put("result", 2);
            object.put("msg", "该用户还没有访问权限!");
            return object;
        }
        object.put("result", 2);
        object.put("msg", "登陆失败,用户账号或者密码不正确!");
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/appLogin", method = RequestMethod.POST)
    public JSONObject appLogin(String userAccount, String pwd)
    {
        JSONObject object = new JSONObject();
        User user = userService.loadAdminByPwdAndAccount(userAccount, MD5.toMD5(pwd));
        if (user != null)
        {
            request.getSession().setAttribute("user", user);
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
            object.put("msg", "用户账号或密码错误!");
        }
        return object;
    }
}