package com.qx.cn.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.MenuTabs;
import com.qx.cn.model.Tab;
import com.qx.cn.service.MenuTabService;
import com.qx.cn.service.TabService;
import com.qx.cn.vo.TabVO;

@Controller
@RequestMapping("/permission")
public class TabController
{
    @Autowired
    private TabService tabService;
    
    @Autowired
    private MenuTabService menuTabService;
    
    @ResponseBody
    @RequestMapping(value = "/tabListForm")
    public String tabListForm()
    {
        return "/tabInfo/tabList";
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryTabListByPage")
    public JSONObject queryTabListByPage(int page, int rows)
    {
        Tab tab = new Tab();
        List results = tabService.queryTabsByPage(tab, page, rows);
        JSONArray arrJson = new JSONArray();
        if (results.size() > 0)
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("id", row[0]);
                json.put("tabName", row[1]);
                json.put("enabled", row[2]);
                if ((Integer)row[3] == null)
                {
                    json.put("isopt", Integer.valueOf(0));
                }
                else
                {
                    json.put("isopt", 1);
                }
                json.put("memo", row[4]);
                arrJson.add(json);
            }
        }
        JSONObject object = new JSONObject();
        object.put("total", Integer.valueOf(results.size()));
        object.put("rows", arrJson);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveTab")
    public JSONObject saveTab(Long id, String tabName, Integer enabled, String menuCode, String memo)
    {
        boolean flag = false;
        if (id.longValue() != 0L)
        {
            Tab upTab = tabService.loadTabById(id);
            upTab.setTabName(tabName);
            upTab.setEnabled(enabled);
            upTab.setMemo(memo);
            flag = tabService.saveTab(upTab);
        }
        else
        {
            Tab tab = new Tab();
            tab.setTabName(tabName);
            tab.setEnabled(enabled);
            tab.setMemo(memo);
            flag = tabService.saveTab(tab);
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置选项卡成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置选项卡失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryUtilById")
    public String queryUtilById()
    {
        return null;
    }
    
    @ResponseBody
    @RequestMapping(value = "/deleteTab")
    public JSONObject deleteTab(Long id)
    {
        menuTabService.deleteTabMenu(id);
        boolean flag = tabService.deleteTab(id);
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除选项卡成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除选项卡失败");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/loadTabById")
    public Tab loadTabById(Long id)
    {
        return tabService.loadTabById(id);
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryAllTabToMenu")
    public JSONObject queryAllTabToMenu(String menuCode)
    {
        List<Tab> tabs = tabService.queryAllTabs();
        List<MenuTabs> menuTabses = menuTabService.queryMenuTabByCode(menuCode);
        List<TabVO> tabVOs = new ArrayList<>();
        for (Tab tab : tabs)
        {
            int sign = 0;
            TabVO tabVO = new TabVO();
            for (MenuTabs menuTab : menuTabses)
            {
                if (tab.getId().equals(menuTab.getTabId()))
                {
                    sign = 1;
                }
            }
            tabVO.setId(tab.getId());
            tabVO.setTabName(tab.getTabName());
            tabVO.setSign(Integer.valueOf(sign));
            tabVOs.add(tabVO);
        }
        JSONObject object = new JSONObject();
        object.put("datas", tabVOs);
        object.put("result", 1);
        return object;
    }
}