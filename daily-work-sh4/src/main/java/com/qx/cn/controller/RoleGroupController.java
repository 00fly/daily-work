package com.qx.cn.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.Role;
import com.qx.cn.model.RoleGroup;
import com.qx.cn.model.RoleGroupMapping;
import com.qx.cn.model.UserRoleGroup;
import com.qx.cn.service.RoleGroupMappingService;
import com.qx.cn.service.RoleGroupService;
import com.qx.cn.service.RoleService;
import com.qx.cn.service.UserRoleGroupService;
import com.qx.cn.vo.RoleGroupVO;
import com.qx.cn.vo.RoleVO;

@Controller
@RequestMapping("/rolegroup")
public class RoleGroupController
{
    @Autowired
    private RoleService roleService;
    
    @Autowired
    private RoleGroupService roleGroupService;
    
    @Autowired
    private UserRoleGroupService userRoleGroupService;
    
    @Autowired
    private RoleGroupMappingService roleGroupMappingService;
    
    @RequestMapping(value = "/roleGroupListForm")
    public String roleGroupListForm()
    {
        return "/roleGroupInfo/roleGroupList";
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryRoleListByPage")
    public JSONObject queryRoleListByPage(int page, int rows)
    {
        List results = roleGroupService.queryRoleGroupsByPage(page, rows);
        JSONArray arrJson = new JSONArray();
        if (results.size() > 0)
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("groupId", (Integer)row[0]);
                json.put("groupName", (String)row[1]);
                json.put("enabled", (Integer)row[2]);
                json.put("groupDesc", (String)row[3]);
                String optUrl = "<span style='color:#00C;cursor:hand;' onclick=giveRoleToGroup('" + (Integer)row[0] + "')>[设置角色]</span>&nbsp;";
                String optUser = "<span style='color:#00C;cursor:hand;' onclick=giveUserToGroup('" + (Integer)row[0] + "','【" + (String)row[1] + "】')>[设置用户]</span>";
                json.put("optStr", optUrl + optUser);
                arrJson.add(json);
            }
        }
        JSONObject object = new JSONObject();
        object.put("total", Integer.valueOf(results.size()));
        object.put("rows", arrJson);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveRoleGroup")
    public JSONObject saveRoleGroup(Long groupId, String groupName, Integer enabled, String groupDesc, int page, int rows)
    {
        if (groupId != null)
        {
            RoleGroup reRoleGroup = roleGroupService.loadRoleGroupById(groupId);
            reRoleGroup.setGroupName(groupName);
            reRoleGroup.setEnabled(enabled);
            reRoleGroup.setGroupDesc(groupDesc);
            roleGroupService.saveRoleGroup(reRoleGroup);
        }
        else
        {
            RoleGroup roleGroup = new RoleGroup();
            roleGroup.setGroupName(groupName);
            roleGroup.setEnabled(enabled);
            roleGroup.setGroupDesc(groupDesc);
            roleGroupService.saveRoleGroup(roleGroup);
        }
        JSONObject object = new JSONObject();
        object.put("result", 1);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/deleteRoleGroup")
    public JSONObject deleteRoleGroup(Long groupId)
    {
        boolean flag = roleGroupService.deleteRoleGroup(groupId);
        boolean flag1 = roleGroupMappingService.deleteGroupRoles(groupId);
        boolean flag2 = userRoleGroupService.deleteGroupsUserByGroupId(groupId);
        JSONObject object = new JSONObject();
        if (flag && flag1 && flag2)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除角色组成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除角色组失败");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/loadRoleGroupById")
    public RoleGroup loadRoleGroupById(Long groupId)
    {
        return roleGroupService.loadRoleGroupById(groupId);
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryAllRolesToGroup")
    public JSONObject queryAllRolesToGroup(Long groupId)
    {
        List<Role> roles = roleService.queryAllRoles();
        List<RoleGroupMapping> roleGroMaps = roleGroupMappingService.queryGroupRolesById(groupId);
        List<RoleVO> roleVOs = new ArrayList<>();
        for (Role role : roles)
        {
            int sign = 0;
            RoleVO roleVO = new RoleVO();
            for (RoleGroupMapping rgm : roleGroMaps)
            {
                if (role.getRoleId().toString().equals(rgm.getRoleId()))
                {
                    sign = 1;
                }
            }
            roleVO.setRoleId(role.getRoleId());
            roleVO.setRoleName(role.getRoleName());
            roleVO.setRoleDesc(role.getRoleDesc());
            roleVO.setEnabled(role.getEnabled());
            roleVO.setSign(Integer.valueOf(sign));
            roleVOs.add(roleVO);
        }
        JSONObject object = new JSONObject();
        object.put("datas", roleVOs);
        object.put("result", 1);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveRoleToGroup")
    public JSONObject saveRoleToGroup(Long groupId, String strBuffer)
    {
        boolean flag = true;
        JSONObject object = new JSONObject();
        String[] roleId = strBuffer.split(",");
        List<RoleGroupMapping> roleGroupMappings = roleGroupMappingService.queryGroupRolesById(groupId);
        if (!roleGroupMappings.isEmpty())
        {
            boolean flag2 = roleGroupMappingService.deleteGroupRoles(groupId);
            if (flag2)
            {
                for (int i = 0; i < roleId.length; i++)
                {
                    RoleGroupMapping roleGroupMapping = new RoleGroupMapping();
                    roleGroupMapping.setRoleId(roleId[i]);
                    roleGroupMapping.setGroupId(groupId);
                    boolean flag1 = roleGroupMappingService.saveRoleGroup(roleGroupMapping);
                    if (!flag1)
                    {
                        flag = false;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < roleId.length; i++)
            {
                RoleGroupMapping roleGroupMapping = new RoleGroupMapping();
                roleGroupMapping.setRoleId(roleId[i]);
                roleGroupMapping.setGroupId(groupId);
                boolean flag1 = roleGroupMappingService.saveRoleGroup(roleGroupMapping);
                if (!flag1)
                {
                    flag = false;
                }
            }
        }
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置角色组成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置角色组失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveUserToRoleGroup")
    public JSONObject saveUserToRoleGroup(Long groupId, String orgCode, String strBuffer)
    {
        boolean flag = true;
        JSONObject object = new JSONObject();
        String[] userId = strBuffer.split(",");
        List<UserRoleGroup> userRoleGroups = userRoleGroupService.queryUsersToGroupByOrgCode(orgCode, groupId);
        if (!userRoleGroups.isEmpty())
        {
            boolean flag2 = userRoleGroupService.deleteUserGroupRoles(orgCode, groupId);
            if (flag2)
            {
                for (int i = 0; i < userId.length; i++)
                {
                    UserRoleGroup userRoleGroup = new UserRoleGroup();
                    userRoleGroup.setGroupId(groupId);
                    userRoleGroup.setOrgCode(orgCode);
                    userRoleGroup.setUserId(Long.valueOf(userId[i]));
                    boolean flag1 = userRoleGroupService.saveUserRoleGroup(userRoleGroup);
                    if (!flag1)
                    {
                        flag = false;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < userId.length; i++)
            {
                UserRoleGroup userRoleGroup = new UserRoleGroup();
                userRoleGroup.setGroupId(groupId);
                userRoleGroup.setOrgCode(orgCode);
                userRoleGroup.setUserId(Long.valueOf(userId[i]));
                boolean flag1 = userRoleGroupService.saveUserRoleGroup(userRoleGroup);
                if (!flag1)
                {
                    flag = false;
                }
            }
        }
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置角色组成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置角色组失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryAllGroupsToUser")
    public JSONObject queryAllGroupsToUser(Long userId)
    {
        List<RoleGroup> groups = roleGroupService.queryAllRoleGroups();
        List<UserRoleGroup> userGroups = userRoleGroupService.queryGroupsByUserId(userId);
        List<RoleGroupVO> groupVOs = new ArrayList<>();
        for (RoleGroup group : groups)
        {
            int sign = 0;
            RoleGroupVO roleGroupVO = new RoleGroupVO();
            for (UserRoleGroup urg : userGroups)
            {
                if (urg.getGroupId().equals(group.getGroupId()))
                {
                    sign = 1;
                }
            }
            roleGroupVO.setGroupId(group.getGroupId());
            roleGroupVO.setGroupName(group.getGroupName());
            roleGroupVO.setGroupDesc(group.getGroupDesc());
            roleGroupVO.setEnabled(group.getEnabled());
            roleGroupVO.setSign(Integer.valueOf(sign));
            groupVOs.add(roleGroupVO);
        }
        JSONObject object = new JSONObject();
        object.put("datas", groupVOs);
        object.put("result", 1);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveGroupToUser")
    public JSONObject saveGroupToUser(String groupName, Integer enabled, String groupDesc, String orgCode, String strBuffer, String userId)
    {
        boolean flag = true;
        JSONObject object = new JSONObject();
        String[] groupId = strBuffer.split(",");
        List<UserRoleGroup> userGroups = userRoleGroupService.queryGroupsByUserId(Long.valueOf(userId));
        if (!userGroups.isEmpty())
        {
            if (userRoleGroupService.deleteGroupsUser(Long.valueOf(userId)))
            {
                for (int i = 0; i < groupId.length; i++)
                {
                    UserRoleGroup userGroup = new UserRoleGroup();
                    userGroup.setGroupId(Long.valueOf(groupId[i]));
                    userGroup.setUserId(Long.valueOf(userId));
                    userGroup.setOrgCode(orgCode);
                    boolean flag1 = userRoleGroupService.saveUserGroup(userGroup);
                    if (!flag1)
                    {
                        flag = false;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < groupId.length; i++)
            {
                UserRoleGroup userGroup = new UserRoleGroup();
                userGroup.setGroupId(Long.valueOf(groupId[i]));
                userGroup.setUserId(Long.valueOf(userId));
                userGroup.setOrgCode(orgCode);
                boolean flag1 = userRoleGroupService.saveUserGroup(userGroup);
                if (!flag1)
                {
                    flag = false;
                }
            }
        }
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置用户-角色组成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置用户-角色组失败!");
        }
        return object;
    }
}