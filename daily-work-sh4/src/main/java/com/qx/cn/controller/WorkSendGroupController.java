package com.qx.cn.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.User;
import com.qx.cn.model.WorkSendGroup;
import com.qx.cn.service.UserService;
import com.qx.cn.service.WorkSendGroupService;
import com.qx.cn.tool.DateForm;

@Controller
@RequestMapping("/workSendGroup")
public class WorkSendGroupController
{
    @Autowired
    private UserService userService;
    
    @Autowired
    private WorkSendGroupService workSendGroupService;
    
    @RequestMapping(value = "/workSendGroupForm")
    public String workSendGroupForm()
    {
        return "/workSendGroup/workSendGroupList";
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryWorkSendGroupByPage")
    public JSONObject queryWorkSendGroupByPage(String groupName, int page, int rows)
    {
        WorkSendGroup workSendGroup = new WorkSendGroup();
        workSendGroup.setGroupName(groupName);
        List results = workSendGroupService.queryWorkSendGroupByPage(workSendGroup, page, rows);
        JSONArray arrJson = new JSONArray();
        if (!results.isEmpty())
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("id", row[0]);
                json.put("groupName", row[1]);
                if ((String)row[2] != null)
                {
                    String[] senduId = row[2].toString().split(",");
                    StringBuilder sendBuf = new StringBuilder();
                    for (int x = 0; x < senduId.length; x++)
                    {
                        User user = userService.loadUserById(Long.valueOf(senduId[x]));
                        if (user != null)
                        {
                            if (x == senduId.length - 1)
                            {
                                sendBuf.append(user.getUserName());
                            }
                            else
                            {
                                sendBuf.append(user.getUserName()).append(",");
                            }
                        }
                    }
                    json.put("senderIds", sendBuf.toString());
                }
                else
                {
                    json.put("senderIds", "暂无");
                }
                if ((String)row[3] != null)
                {
                    String[] senduId = row[3].toString().split(",");
                    StringBuilder sendBuf = new StringBuilder();
                    for (int x = 0; x < senduId.length; x++)
                    {
                        User user = userService.loadUserById(Long.valueOf(senduId[x]));
                        if (user != null)
                        {
                            if (x == senduId.length - 1)
                            {
                                sendBuf.append(user.getUserName());
                            }
                            else
                            {
                                sendBuf.append(user.getUserName()).append(",");
                            }
                        }
                    }
                    json.put("workerIds", sendBuf.toString());
                }
                else
                {
                    json.put("workerIds", "暂无");
                }
                json.put("creatTime", row[4]);
                if (row[5] == null)
                {
                    json.put("enabled", "<a href='javascript:;' style='color:red;font-weight:800' onclick=setEnabled(1," + row[0] + ",'" + (String)row[1] + "','" + (String)row[2] + "','" + (String)row[3] + "')>启用</a>");
                }
                else if (((Integer)row[5]).intValue() == 0)
                {
                    json.put("enabled", "<a href='javascript:;' style='color:red;font-weight:800' onclick=setEnabled(1," + row[0] + ",'" + (String)row[1] + "','" + (String)row[2] + "','" + (String)row[3] + "')>启用</a>");
                }
                else
                {
                    json.put("enabled", "<a href='javascript:;' style='color:blue;font-weight:800' onclick=setEnabled(0," + row[0] + ",'" + (String)row[1] + "','" + (String)row[2] + "','" + (String)row[3] + "')>禁用</a>");
                }
                json.put("memo", row[6]);
                arrJson.add(json);
            }
        }
        JSONObject object = new JSONObject();
        object.put("total", Integer.valueOf(workSendGroupService.queryAllWorkSendGroups(workSendGroup).size()));
        object.put("rows", arrJson);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveWorkSendGroup")
    public JSONObject saveWorkSendGroup(Integer id, String groupName, String memo)
    {
        boolean flag = false;
        if (id != null)
        {
            WorkSendGroup upWorkSendGroup = workSendGroupService.loadWorkSendGroupById(id.intValue());
            upWorkSendGroup.setMemo(memo);
            upWorkSendGroup.setGroupName(groupName);
            flag = workSendGroupService.saveWorkSendGroup(upWorkSendGroup);
        }
        else
        {
            WorkSendGroup workSendGroup = new WorkSendGroup();
            workSendGroup.setMemo(memo);
            workSendGroup.setGroupName(groupName);
            workSendGroup.setTimeTip(Integer.valueOf(0));
            workSendGroup.setCreatTime(DateForm.SimpleDate(new Date()));
            flag = workSendGroupService.saveWorkSendGroup(workSendGroup);
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置发送组成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置发送组失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/deleteWorkSendGroup")
    public JSONObject deleteWorkSendGroup(Integer id)
    {
        boolean flag = workSendGroupService.deleteWorkSendGroup(id);
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除发送组成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除发送组失败");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/enabledWorkSendGroup")
    public JSONObject enabledWorkSendGroup(Integer id, Integer status)
    {
        boolean flag = false;
        String title = "";
        WorkSendGroup workSendGroup = workSendGroupService.loadWorkSendGroupById(id.intValue());
        workSendGroup.setEnabled(status);
        flag = workSendGroupService.saveWorkSendGroup(workSendGroup);
        if (status.intValue() == 1)
        {
            title = "启用";
        }
        else
        {
            title = "禁用";
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", title + "发送组成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", title + "发送组失败");
        }
        return object;
    }
}