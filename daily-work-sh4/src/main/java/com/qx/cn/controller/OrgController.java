package com.qx.cn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.OrgInfo;
import com.qx.cn.service.OrgInfoService;
import com.qx.cn.vo.OrgInfoVO;

/**
 * 
 * <一句话功能简述> <功能详细描述>
 * 
 * @author 0001
 * @version [版本号, 2018年8月19日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@RequestMapping("/org")
public class OrgController
{
    @Autowired
    private OrgInfoService orgInfoService;
    
    @ResponseBody
    @RequestMapping(value = "/queryOrgList")
    public List<OrgInfoVO> queryOrgList(String id)
    {
        OrgInfo org = new OrgInfo();
        org.setOrgCode(id);
        List<OrgInfoVO> orgInfos = orgInfoService.queryOrgListTreeGrid(org);
        return orgInfos;
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryOrgTreeToUser")
    public JSONArray queryOrgTreeToUser(String parCode)
    {
        String sta;
        List<OrgInfo> orgInfos = orgInfoService.queryOrgListToUserByAsc(parCode);
        JSONArray jsonArray = new JSONArray();
        for (OrgInfo org : orgInfos)
        {
            JSONObject obj = new JSONObject();
            List<OrgInfo> sonOrgs = orgInfoService.queryOrgListByAsc(org.getOrgCode());
            if (sonOrgs.size() > 1)
            {
                sta = "closed";
            }
            else
            {
                sta = "open";
            }
            obj.put("state", sta);
            obj.put("id", org.getOrgCode());
            obj.put("text", org.getOrgName());
            jsonArray.add(obj);
        }
        return jsonArray;
    }
}
