package com.qx.cn.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.User;
import com.qx.cn.service.UserService;
import com.qx.cn.tool.DateForm;
import com.qx.cn.tool.MD5;

/**
 * 
 * 
 * @author 00fly
 * @version [版本号, 2016-5-15]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@RequestMapping("/permission")
public class PermissionController
{
    @Autowired
    private UserService userService;
    
    @RequestMapping(value = "/listForm")
    public String listForm()
    {
        return "/permission/listForm";
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryUserByCodeAnsPage")
    public JSONObject queryUserByCodeAnsPage(String userLoginName, String userName, String workNumber, String orgCode, String pinyin, int page, int rows)
    {
        User uss = new User();
        uss.setUserLoginName(userLoginName);
        uss.setUserName(userName);
        uss.setWorkNumber(workNumber);
        uss.setOrgCode(orgCode);
        uss.setSpellLong(pinyin);
        List results = userService.queryUserByPageAndCodeSQL(uss, page, rows);
        JSONArray arrJson = new JSONArray();
        if (!results.isEmpty())
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("userId", row[0]);
                json.put("userName", row[3]);
                json.put("userLoginName", row[1]);
                json.put("workNumber", row[7]);
                json.put("enabled", row[13]);
                arrJson.add(json);
            }
        }
        JSONObject object = new JSONObject();
        List<User> users = userService.queryUserPageSizeByCode(uss);
        if (!users.isEmpty())
        {
            object.put("total", Integer.valueOf(users.size()));
        }
        object.put("rows", arrJson);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveUserByOrgCode")
    public JSONObject saveUserByOrgCode(Long userId, String userLoginName, String userName, String orgCode, String workNumber, String telephone, Integer enabled)
    {
        boolean flag = false;
        if (userId == null)
        {
            User user = new User();
            user.setOrgCode(orgCode);
            user.setUserLoginName(userLoginName);
            user.setUserLoginPwd(MD5.toMD5("123456"));
            user.setCreateTime(DateForm.SimpleDateTime(new Date()));
            user.setTelePhone(telephone);
            user.setEnabled(enabled);
            user.setUserName(userName);
        }
        else
        {
            User updUser = userService.loadUserById(userId);
            updUser.setOrgCode(orgCode);
            updUser.setTelePhone(telephone);
            updUser.setEnabled(enabled);
            updUser.setUserName(userName);
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("msg", "添加成功,账号:" + userLoginName + "&nbsp;密码:123456");
        }
        else
        {
            object.put("result", 2);
        }
        return object;
    }
}
