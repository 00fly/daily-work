package com.qx.cn.controller;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.WorkReportItem;
import com.qx.cn.service.WorkReportItemService;
import com.qx.cn.tool.DateForm;

@Controller
@RequestMapping("/workReportItem")
public class WorkReportItemController
{
    @Autowired
    private WorkReportItemService workReportItemService;
    
    private static Pattern pattern = Pattern.compile("([1-9]+[0-9]*|0)(\\.[\\d]+)?");
    
    @ResponseBody
    @RequestMapping(value = "queryWorkItemList")
    public JSONArray queryWorkItemList(String wrCode)
    {
        JSONArray arrJson = new JSONArray();
        List<WorkReportItem> wItems = workReportItemService.queryWorkReportItems(wrCode);
        int i = 0;
        for (WorkReportItem wpItem : wItems)
        {
            JSONObject json = new JSONObject();
            i++;
            json.put("id", wpItem.getId());
            json.put("xulei", Integer.valueOf(i));
            json.put("content", wpItem.getContent());
            json.put("workTime", wpItem.getWorkTime());
            json.put("hopeFinishDate", wpItem.getHopeFinishDate());
            json.put("actualFinishDate", wpItem.getActualFinishDate());
            json.put("remarks", wpItem.getRemarks());
            json.put("wrCode", wpItem.getReportCode());
            json.put("type", wpItem.getType());
            arrJson.add(json);
        }
        return arrJson;
    }
    
    /**
     * <一句话功能简述> <功能详细描述>
     * 
     * @param id
     * @param wrCode
     * @param content
     * @param workTime
     * @param hopeFinishDate 要求完成时间
     * @param actualFinishDate 实际完成时间
     * @param remarks 备注
     * @param type 类型
     * @return
     * @see [类、类#方法、类#成员]
     */
    @ResponseBody
    @RequestMapping(value = "saveWorkItem")
    public JSONObject saveWorkItem(Integer id, String wrCode, String content, String workTime, String hopeFinishDate, String actualFinishDate, String remarks, Integer type)
    {
        String msg = "";
        boolean flag = false;
        if (StringUtils.isEmpty(hopeFinishDate)) // 日报
        {
            if (StringUtils.isEmpty(workTime))
            {
                msg = "<span style='font-size:15px;'>时间不能为空!</span>";
                flag = false;
            }
            else
            {
                boolean checkFlag = isDecimal(workTime);
                if (checkFlag)
                {
                    WorkReportItem mpd = new WorkReportItem();
                    mpd.setContent(content);
                    mpd.setReportCode(wrCode);
                    mpd.setCreateTime(DateForm.SimpleDateTime(new Date()));
                    mpd.setWorkTime(workTime);
                    mpd.setRemarks(remarks);
                    flag = workReportItemService.saveWorkReportItem(mpd);
                }
                else
                {
                    msg = "<span style='font-size:15px;'>工作时间格式不正确!</span>";
                    flag = false;
                }
            }
        }
        else
        { // 周报
            WorkReportItem mpd = new WorkReportItem();
            mpd.setContent(content);
            mpd.setReportCode(wrCode);
            mpd.setCreateTime(DateForm.SimpleDateTime(new Date()));
            mpd.setType(type == null ? 1 : type);
            // 1/2/3代表:本周工作/下周计划/本周小结
            mpd.setHopeFinishDate(hopeFinishDate);
            if (1 == type)
            {
                mpd.setActualFinishDate(actualFinishDate);
            }
            mpd.setRemarks(remarks);
            flag = workReportItemService.saveWorkReportItem(mpd);
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("msg", "设置工作项成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("msg", msg);
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "updateWorkItem")
    public JSONObject updateWorkItem(Integer id, String content, String workTime, String hopeFinishDate, String actualFinishDate, String remarks, Integer type)
    {
        String msg = "";
        boolean flag = false;
        if (StringUtils.isEmpty(hopeFinishDate)) // 日报
        {
            if (StringUtils.isBlank(workTime))
            {
                msg = "时间不能为空!";
                flag = false;
            }
            else
            {
                boolean checkFlag = isDecimal(workTime);
                if (checkFlag)
                {
                    WorkReportItem mWorkItem = workReportItemService.loadWorkItem(id);
                    mWorkItem.setContent(content);
                    mWorkItem.setWorkTime(workTime);
                    mWorkItem.setRemarks(remarks);
                    flag = workReportItemService.saveWorkReportItem(mWorkItem);
                }
                else
                {
                    msg = "工作时间格式不正确!";
                    flag = false;
                }
            }
        }
        else
        { // 周报
            WorkReportItem mWorkItem = workReportItemService.loadWorkItem(id);
            mWorkItem.setContent(content);
            mWorkItem.setHopeFinishDate(hopeFinishDate);
            mWorkItem.setActualFinishDate(actualFinishDate);
            mWorkItem.setRemarks(remarks);
            mWorkItem.setType(type == null ? 1 : type);
            flag = workReportItemService.saveWorkReportItem(mWorkItem);
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("msg", "设置工作项成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("msg", msg);
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "deleteWorkItemById")
    public JSONObject deleteWorkItemById(int id)
    {
        boolean flag = false;
        flag = workReportItemService.deleteWorkItemById(id);
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
        }
        return object;
    }
    
    private boolean isDecimal(String str)
    {
        return pattern.matcher(str).matches();
    }
}