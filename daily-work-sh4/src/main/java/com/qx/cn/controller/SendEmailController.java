package com.qx.cn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.SendEmail;
import com.qx.cn.service.SendEmailService;

@Controller
@RequestMapping("/sendEmail")
public class SendEmailController
{
    @Autowired
    private SendEmailService sendEmailService;
    
    @ResponseBody
    @RequestMapping(value = "/querySendEmailByPage", method = RequestMethod.POST)
    public JSONObject querySendEmailByPage(int page, int rows)
    {
        List results = sendEmailService.querySendEmailByPage(page, rows);
        JSONArray arrJson = new JSONArray();
        if (!results.isEmpty())
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("id", (Integer)row[0]);
                json.put("name", (String)row[1]);
                json.put("email", (String)row[2]);
                json.put("password", (String)row[3]);
                json.put("memo", (String)row[4]);
                json.put("smtp", (String)row[5]);
                arrJson.add(json);
            }
        }
        JSONObject object = new JSONObject();
        object.put("total", Integer.valueOf(results.size()));
        object.put("rows", arrJson);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/checkIsSendEmail")
    public JSONObject checkIsSendEmail()
    {
        JSONObject object = new JSONObject();
        List sendEmails = sendEmailService.queryAllSendEmails();
        if (sendEmails.size() > 0)
        {
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveSendEmail")
    public JSONObject saveSendEmail(Integer id, String name, String email, String password, String memo)
    {
        boolean flag = false;
        if (id != null)
        {
            SendEmail upSendEmail = sendEmailService.loadSendEmailById(id.intValue());
            upSendEmail.setName(name);
            upSendEmail.setEmail(email);
            upSendEmail.setPassword(password);
            String emStr = email.substring(email.lastIndexOf("@") + 1, email.length());
            upSendEmail.setStmp("smtp." + emStr);
            upSendEmail.setMeno(memo);
            flag = sendEmailService.saveSendEmail(upSendEmail);
        }
        else
        {
            SendEmail sendEmail = new SendEmail();
            sendEmail.setName(name);
            sendEmail.setEmail(email);
            String emStr = email.substring(email.lastIndexOf("@"), email.length());
            sendEmail.setStmp("smtp." + emStr);
            sendEmail.setPassword(password);
            sendEmail.setMeno(memo);
            flag = sendEmailService.saveSendEmail(sendEmail);
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置发送邮箱成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置发送邮箱失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/deleteSendEmail")
    public JSONObject deleteSendEmail(Integer id)
    {
        boolean flag = sendEmailService.deleteSendEmail(id);
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除发送邮箱成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除发送邮箱失败");
        }
        return object;
    }
}