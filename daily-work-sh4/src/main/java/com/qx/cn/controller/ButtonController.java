package com.qx.cn.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.Button;
import com.qx.cn.model.MenuButton;
import com.qx.cn.service.ButtonService;
import com.qx.cn.service.MenuButtonService;
import com.qx.cn.vo.ButtonVO;

@Controller
@RequestMapping("/button")
public class ButtonController
{
    @Autowired
    private ButtonService buttonService;
    
    @Autowired
    private MenuButtonService menuButtonService;
    
    @ResponseBody
    @RequestMapping(value = "/queryButtonListByPage")
    public JSONObject queryButtonListByPage(int page, int rows)
    {
        List results = buttonService.queryButtonsByPage(page, rows);
        JSONArray arrJson = new JSONArray();
        if (!results.isEmpty())
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("buttonId", (Integer)row[0]);
                json.put("buttonName", (String)row[1]);
                json.put("enabled", (Integer)row[3]);
                json.put("methodName", (String)row[4]);
                json.put("buttonSort", (Integer)row[5]);
                arrJson.add(json);
            }
        }
        JSONObject object = new JSONObject();
        object.put("total", results.size());
        object.put("rows", arrJson);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveButton")
    public JSONObject saveButton(Long buttonId, File some, String someFileName, String someContentType, String imagePath, String buttonName, String methodName, Integer enabled, String menuCode,
        Integer buttonSort)
    {
        boolean flag = true;
        if (buttonId != null)
        {
            Button updBut = buttonService.loadButtonById(buttonId);
            updBut.setButtonName(buttonName);
            updBut.setEnabled(enabled);
            updBut.setButtonSort(buttonSort);
            updBut.setMethodName(methodName);
            flag = true;
            buttonService.saveButton(updBut);
        }
        else
        {
            Button button = new Button();
            button.setButtonName(buttonName);
            button.setEnabled(enabled);
            button.setMethodName(methodName);
            button.setButtonSort(buttonSort);
            buttonService.saveButton(button);
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "添加或者修改按键错误!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/deleteButton")
    public JSONObject deleteButton(Long buttonId)
    {
        menuButtonService.deleteButtonMenu(String.valueOf(buttonId));
        boolean flag = buttonService.deleteButton(buttonId);
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除按键成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除按键失败");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/loadButtonById")
    public JSONObject loadButtonById(Long buttonId)
    {
        JSONObject object = new JSONObject();
        Button button = buttonService.loadButtonById(buttonId);
        object.put("buttonId", button.getButtonId());
        object.put("buttonName", button.getButtonName());
        object.put("enabled", button.getEnabled());
        object.put("buttonSort", button.getButtonSort());
        object.put("methodName", button.getMethodName());
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/queryAllButtonToMenu")
    public JSONObject queryAllButtonToMenu(String menuCode)
    {
        List<Button> buttons = buttonService.queryAllButtons();
        List<MenuButton> menuButtons = menuButtonService.queryMenuButtonByCode(menuCode);
        List<ButtonVO> buttonVOs = new ArrayList<>();
        for (Button button : buttons)
        {
            int sign = 0;
            ButtonVO buttonVO = new ButtonVO();
            for (MenuButton menuBut : menuButtons)
            {
                if (button.getButtonId().equals(menuBut.getButtonId()))
                {
                    sign = 1;
                }
            }
            buttonVO.setButtonId(button.getButtonId());
            buttonVO.setButtonName(button.getButtonName());
            buttonVO.setSign(Integer.valueOf(sign));
            buttonVOs.add(buttonVO);
        }
        JSONObject object = new JSONObject();
        object.put("datas", buttonVOs);
        object.put("result", 1);
        return object;
    }
}