/*
 * 文 件 名:  UserController.java
 * 版    权:  00fly Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * 描    述:  <描述>
 * 修 改 人:  0001
 * 修改时间:  2018年8月18日
 * 跟踪单号:  <跟踪单号>
 * 修改单号:  <修改单号>
 * 修改内容:  <修改内容>
 */
package com.qx.cn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.OrgInfo;
import com.qx.cn.model.Position;
import com.qx.cn.model.Role;
import com.qx.cn.model.User;
import com.qx.cn.model.UserRole;
import com.qx.cn.service.OrgInfoService;
import com.qx.cn.service.PositionService;
import com.qx.cn.service.RoleService;
import com.qx.cn.service.UserRoleService;
import com.qx.cn.service.UserService;

/**
 * <一句话功能简述> <功能详细描述>
 * 
 * @author 0001
 * @version [版本号, 2018年8月18日]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
@Controller
@RequestMapping("/user")
public class UserController
{
    @Autowired
    private UserService userService;
    
    @Autowired
    private RoleService roleService;
    
    @Autowired
    private OrgInfoService orgInfoService;
    
    @Autowired
    private UserRoleService userRoleService;
    
    @Autowired
    private PositionService positionService;
    
    /**
     * 保存session
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    @ResponseBody
    @RequestMapping(value = "keepSession")
    public String keepSession()
    {
        return "1";
    }
    
    @ResponseBody
    @RequestMapping(value = "queryUserByCodeAnsPage")
    public JSONObject queryUserByCodeAnsPage(String userLoginName, String userName, String workNumber, String orgCode, String pinyin, int page, int rows)
    {
        User uss = new User();
        uss.setUserLoginName(userLoginName);
        uss.setUserName(userName);
        uss.setWorkNumber(workNumber);
        uss.setOrgCode(orgCode);
        uss.setSpellLong(pinyin);
        List results = userService.queryUserByPageAndCodeSQL(uss, page, rows);
        JSONArray arrJson = new JSONArray();
        if (results.size() > 0)
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("userId", row[0]);
                json.put("userName", row[3]);
                json.put("userLoginName", row[1]);
                json.put("positionId", row[8]);
                json.put("sort", row[12]);
                if ((row[8] != null) && (!"".equals(row[8])))
                {
                    Position position = positionService.loadPositionById(Long.valueOf((String)row[8]));
                    if (position != null)
                    {
                        json.put("positionName", position.getNames());
                    }
                    else
                    {
                        json.put("positionName", "");
                    }
                }
                else
                {
                    json.put("positionName", "");
                }
                json.put("workNumber", row[7]);
                json.put("enabled", row[13]);
                String gonghaoStr = "";
                if (((String)row[7] == null) || ("".equals(row[7])))
                {
                    gonghaoStr = "暂缺";
                }
                else
                {
                    gonghaoStr = (String)row[7];
                }
                String telStr = "";
                if (((String)row[6] == null) || ("".equals(row[6])))
                {
                    telStr = "暂缺";
                }
                else
                {
                    telStr = (String)row[6];
                }
                String posStr = "";
                if (((String)row[8] == null) || ("".equals(row[8])))
                {
                    posStr = "暂缺";
                }
                else
                {
                    posStr = (String)row[8];
                }
                String orgCodeStr = (String)row[5];
                StringBuilder orgBuffer = new StringBuilder();
                if ((orgCodeStr != null) && (!"".equals(orgCodeStr)))
                {
                    String[] orgstr = orgCodeStr.split(",");
                    for (int j = 0; j < orgstr.length; j++)
                    {
                        OrgInfo orgInfo = orgInfoService.loadOrgByCode(orgstr[j]);
                        if (orgInfo != null)
                        {
                            if (i == orgstr.length - 2)
                            {
                                orgBuffer.append(orgInfo.getOrgName()).append(".");
                            }
                            else
                            {
                                orgBuffer.append(orgInfo.getOrgName()).append(",");
                            }
                        }
                    }
                }
                List<UserRole> userRoles = userRoleService.queryRolesByUserId(Long.valueOf(((Integer)row[0]).longValue()));
                StringBuilder roleBuffer = new StringBuilder();
                for (UserRole userRole : userRoles)
                {
                    Role role = roleService.loadRoleById(Long.valueOf(userRole.getRoleId()));
                    if (role != null)
                    {
                        roleBuffer.append(role.getRoleName()).append(",");
                    }
                }
                String detailInfo =
                    "<ul><li>姓名：" + (String)row[3] + "</li><li>账号：" + (String)row[1] + "</li><li>工号：" + gonghaoStr + "</li>" + "<li>电话：" + telStr + "</li>" + "<li>职位：" + posStr + "</li>" + "<li>创建时间："
                        + (String)row[9] + "&nbsp;&nbsp;</li>" + "<li>所属机构：" + orgBuffer.toString() + "&nbsp;&nbsp;</li>" + "<li>所属角色：" + roleBuffer.toString() + "&nbsp;&nbsp;</li>" + "</ul>";
                json.put("detailInfo", detailInfo);
                String optRoleUrl = "<span style='color:#00C;cursor:pointer;' onclick=giveRoleToUser('" + row[0] + "','【" + (String)row[3] + "】')>[分配角色]</span>&nbsp;";
                json.put("optUrl", optRoleUrl);
                arrJson.add(json);
            }
        }
        JSONObject object = new JSONObject();
        List<User> users = userService.queryUserPageSizeByCode(uss);
        if (users != null)
        {
            object.put("total", Integer.valueOf(users.size()));
        }
        object.put("rows", arrJson);
        return object;
    }
}
