package com.qx.cn.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.SendDate;
import com.qx.cn.model.SendTime;
import com.qx.cn.model.User;
import com.qx.cn.service.SendDateService;
import com.qx.cn.service.SendTimeService;
import com.qx.cn.service.WorkReportService;
import com.qx.cn.tool.DateForm;

@Controller
@RequestMapping("/sendTime")
public class SendTimeController
{
    @Autowired
    HttpServletRequest request;
    
    @Autowired
    private SendTimeService sendTimeService;
    
    @Autowired
    private SendDateService sendDateService;
    
    @Autowired
    private WorkReportService workReportService;
    
    @ResponseBody
    @RequestMapping(value = "/queryInfoToIndex")
    public JSONObject queryInfoToIndex()
        throws ParseException
    {
        User user = (User)request.getSession().getAttribute("user");
        List<SendTime> sendTimes = sendTimeService.queryAllTimeToShow();
        JSONObject object = new JSONObject();
        String rq = DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd");
        String dgr = rq.substring(8, 10);
        String xq = DateForm.dateToXingQi(rq);
        if (sendTimes.size() > 0)
        {
            object.put("sdt", sendTimes.get(0).getStartTime());
            object.put("edt", sendTimes.get(0).getEndTime());
        }
        else
        {
            object.put("sdt", "00:00");
            object.put("edt", "00:00");
        }
        StringBuilder msg = new StringBuilder("今天<span style='color:red;'>不用</span>上交日报、周报");
        SendDate sendDate = sendDateService.queryAllSendDateByDate(DateForm.SimpleDate(new Date()), "week");
        if (sendDate != null)
        {
            msg = new StringBuilder("<span style='color:red;'>今天需要上交日报、周报</span>");
            boolean dayFlag = workReportService.queryCheckIsWorkReports(DateForm.SimpleDate(new Date()), user.getUserId(), "day");
            if (dayFlag)
            {
                msg.append("<br/><span style='color:red;'>您还没有上交日报</span>");
            }
            else
            {
                msg.append("<br/><span style='color:blue;'>您已经上交日报</span>");
            }
            boolean weekFlag = workReportService.queryCheckIsWorkReports(DateForm.SimpleDate(new Date()), user.getUserId(), "week");
            if (weekFlag)
            {
                msg.append("<br/><span style='color:red;'>您还没有上交周报</span>");
            }
            else
            {
                msg.append("<br/><span style='color:blue;'>您已经上交周报</span>");
            }
        }
        else
        {
            sendDate = sendDateService.queryAllSendDateByDate(DateForm.SimpleDate(new Date()), "day");
            if (sendDate != null)
            {
                msg = new StringBuilder("<span style='color:red;'>今天需要上交日报</span>");
                boolean dayFlag = workReportService.queryCheckIsWorkReports(DateForm.SimpleDate(new Date()), user.getUserId(), "day");
                if (dayFlag)
                {
                    msg.append("<br/><span style='color:red;'>您还没有上交日报</span>");
                }
                else
                {
                    msg.append("<br/><span style='color:blue;'>您已经上交日报</span>");
                }
            }
        }
        object.put("isNextPost", msg);
        object.put("rq", rq);
        object.put("xq", xq);
        object.put("result", 1);
        object.put("dgr", dgr);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/querySendTimeToShow")
    public JSONObject querySendTimeToShow()
    {
        JSONObject object = new JSONObject();
        List<SendTime> sendTimes = sendTimeService.queryAllTimeToShow();
        if (sendTimes.size() > 0)
        {
            object.put("startTimeShow", ((SendTime)sendTimes.get(0)).getStartTime());
            object.put("endTimeShow", ((SendTime)sendTimes.get(0)).getEndTime());
        }
        else
        {
            object.put("startTimeShow", "00:00");
            object.put("endTimeShow", "00:00");
        }
        object.put("result", 1);
        return object;
    }
}
