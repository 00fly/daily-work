package com.qx.cn.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qx.cn.model.SendDate;
import com.qx.cn.service.SendDateService;
import com.qx.cn.tool.DateForm;

@Controller
@RequestMapping("/sendDate")
public class SendDateController
{
    @Autowired
    private SendDateService sendDateService;
    
    /**
     * 00fly 新增，取值day(日报)，all(日报+周报)
     * 
     * @param page
     * @param rows
     * @param name
     * @param type
     * @return
     * @throws ParseException
     * @see [类、类#方法、类#成员]
     */
    @ResponseBody
    @RequestMapping(value = "/querySendDateByPage")
    public JSONObject querySendDateByPage(int page, int rows, String name, String type)
        throws ParseException
    {
        SendDate sdObj = new SendDate();
        sdObj.setName(name);
        sdObj.setType(type);
        List results = sendDateService.querySendDateByPage(sdObj, page, rows);
        JSONArray arrJson = new JSONArray();
        if (!results.isEmpty())
        {
            for (int i = 0; i < results.size(); i++)
            {
                Object[] row = (Object[])results.get(i);
                JSONObject json = new JSONObject();
                json.put("id", row[0]);
                json.put("name", row[1]);
                String weekDate = DateForm.dateToXingQi((String)row[1]);
                if ("星期六".equals(weekDate))
                {
                    json.put("weekDate", "<span style='color:red;'>星期六</>");
                }
                else if ("星期日".equals(weekDate))
                {
                    json.put("weekDate", "<span style='color:red;'>星期日</>");
                }
                else
                {
                    json.put("weekDate", weekDate);
                }
                json.put("memo", row[3]);
                Date dateAll = DateForm.StrToDateTime((String)row[1] + " 23:59:59");
                boolean flag = new Date().before(dateAll);
                if (flag)
                {
                    json.put("status", "<span style='color:green;'>正常</span>");
                }
                else
                {
                    json.put("status", "<span style='color:red;'>过期</span>");
                }
                type = (String)row[4];
                if ("day".equals(type))
                {
                    json.put("type", "日报");
                }
                else if ("day,week".equals(type))
                {
                    json.put("type", "<span style='color:red;'>日报、周报</span>");
                }
                arrJson.add(json);
            }
        }
        JSONObject object = new JSONObject();
        object.put("total", Integer.valueOf(sendDateService.queryAllSendDates(sdObj).size()));
        object.put("rows", arrJson);
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/addSendDateByAuto")
    public JSONObject addSendDateByAuto()
        throws ParseException
    {
        boolean flag = false;
        SendDate sendDate = sendDateService.querySendDateByAuto();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        if ((sendDate != null) && (sendDate.getName() != null))
        {
            Date myDate = formatter.parse(sendDate.getName());
            Calendar c = Calendar.getInstance();
            c.setTime(myDate);
            for (int i = 0; i < 7; i++)
            {
                c.add(5, 1);
                SendDate sendDate2 = new SendDate();
                sendDate2.setName(DateFormatUtils.format(c.getTime(), "yyyy-MM-dd"));
                int index = c.get(Calendar.DAY_OF_WEEK);
                if (index != 7 && index != 1) // 跳过周六、周日
                {
                    sendDate2.setType(index == 6 ? "day,week" : "day");// 周五
                    sendDateService.saveSendDate(sendDate2);
                }
            }
            flag = true;
        }
        else
        {
            try
            {
                Date myDate = formatter.parse(DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd"));
                Calendar c = Calendar.getInstance();
                c.setTime(myDate);
                for (int i = 0; i < 7; i++)
                {
                    c.add(5, 1);
                    SendDate sendDate2 = new SendDate();
                    sendDate2.setName(DateFormatUtils.format(c.getTime(), "yyyy-MM-dd"));
                    int index = c.get(Calendar.DAY_OF_WEEK);
                    if (index != 7 && index != 1) // 跳过周六、周日
                    {
                        sendDateService.saveSendDate(sendDate2);
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            flag = true;
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "自动添加发送日期成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "自动添加日期失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/saveSendDate")
    public JSONObject saveSendDate(Integer id, String name, String type, String memo)
    {
        boolean flag = false;
        if (id != null)
        {
            SendDate upSendDate = sendDateService.loadSendDateById(id.intValue());
            upSendDate.setType(type);
            upSendDate.setMemo(memo);
            flag = sendDateService.saveSendDate(upSendDate);
        }
        else
        {
            SendDate sendDate = new SendDate();
            sendDate.setName(name);
            sendDate.setType(type);
            sendDate.setMemo(memo);
            flag = sendDateService.saveSendDate(sendDate);
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "设置发送日期成功!");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "设置发送日期失败!");
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/checkSaveOrUpdate")
    public JSONObject checkSaveOrUpdate(Integer id, String name)
    {
        boolean flag = false;
        Integer res = 1;
        String msg = "";
        if (id != null)
        {
            SendDate sendDate = sendDateService.queryAllSendDateByDate(name, "day");
            if ((sendDate != null) && (sendDate.getId() != id))
            {
                res = 2;
                msg = "该日期已经存在，不需要重复添加";
            }
        }
        else
        {
            SendDate sendDate = sendDateService.queryAllSendDateByDate(name, "day");
            if (sendDate != null)
            {
                res = 2;
                msg = "该日期已经存在，不需要重复添加";
            }
            else
            {
                res = 1;
            }
        }
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", res);
        }
        else
        {
            object.put("result", res);
            object.put("errorMsg", msg);
        }
        return object;
    }
    
    @ResponseBody
    @RequestMapping(value = "/deleteSendDate")
    public JSONObject deleteSendDate(Integer id)
    {
        boolean flag = sendDateService.deleteSendDate(id);
        JSONObject object = new JSONObject();
        if (flag)
        {
            object.put("result", 1);
            object.put("errorMsg", "删除发送日期成功");
        }
        else
        {
            object.put("result", 2);
            object.put("errorMsg", "删除发送日期失败");
        }
        return object;
    }
}