package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.ConfigData;
import com.qx.cn.service.ConfigDataService;

@Service
@Transactional
public class ConfigDataServiceImpl implements ConfigDataService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List<ConfigData> queryConfigByPar()
    {
        String hql = "from ConfigData c where c.order = null order by c.dataCode asc";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean saveConfig(ConfigData configData)
    {
        boolean flag = true;
        baseDAO.saveOrUpdate(configData);
        return flag;
    }
    
    @Override
    public boolean deleteConfigByCode(String dataCode)
    {
        boolean flag = true;
        String hql = "delete ConfigData c where c.dataCode like '" + dataCode + "%'";
        baseDAO.deleteByHql(hql);
        return flag;
    }
    
    @Override
    public ConfigData loadConfigByCode(String dataCode)
    {
        String hql = "from ConfigData c where c.dataCode = '" + dataCode + "'";
        return (ConfigData)baseDAO.loadByHql(hql);
    }
    
    @Override
    public List<ConfigData> querySonConfigByParCode(String dataCode, int type)
    {
        String hql = null;
        if (type == 1)
        {
            hql = "from ConfigData c where  c.dataCode like '" + dataCode + "%' and c.order != null order by c.order asc";
        }
        else if (type == 2)
        {
            hql = "from ConfigData c where  c.dataCode like '" + dataCode + "%' and c.order != null order by c.dataCode asc";
        }
        return baseDAO.query(hql);
    }
    
    @Override
    public List<ConfigData> queryConfigByDataDomain(String dataDomain, int type)
    {
        String hql = "";
        if (type == 1)
        {
            hql = "from ConfigData c where  c.configDomainName = '" + dataDomain + "' and c.order != null order by c.order asc";
        }
        return baseDAO.query(hql);
    }
    
    @Override
    public void updateDataByCode(String dataDomain, String dataCode)
    {
        String hql = "update ConfigData c set c.configDomainName = '" + dataDomain + "' where c.dataCode like '" + dataCode + "%'";
        baseDAO.update(hql);
    }
    
    @Override
    public ConfigData loadConfigByDOMAndOrder(String dataDomain)
    {
        String hql = "from ConfigData c where  c.configDomainName ='" + dataDomain + "' and c.order is null ";
        return (ConfigData)baseDAO.loadByHql(hql);
    }
}