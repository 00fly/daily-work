package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.SendTime;
import com.qx.cn.service.SendTimeService;

@Service
@Transactional
public class SendTimeServiceImpl implements SendTimeService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public boolean saveSendTime(SendTime sendTime)
    {
        baseDAO.saveOrUpdate(sendTime);
        return true;
    }
    
    @Override
    public boolean deleteSendTime()
    {
        String hql = "delete from SendTime";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public List<SendTime> queryAllTimeToShow()
    {
        String hql = "from SendTime";
        return baseDAO.query(hql);
    }
}