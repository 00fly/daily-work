package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.MenuButton;

public interface MenuButtonService
{
    public Boolean saveMenuButtin(MenuButton menuButton);
    
    public List<MenuButton> queryMenuButtonByCode(String menuCode);
    
    public boolean deleteMenuButton(String menuId);
    
    public List<MenuButton> queryButtonsByMenuCode(String menuCode);
    
    public boolean deleteButtonMenu(String buttonId);
}