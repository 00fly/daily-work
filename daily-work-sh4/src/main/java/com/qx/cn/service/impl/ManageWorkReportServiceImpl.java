package com.qx.cn.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.ManagerWorkReport;
import com.qx.cn.service.ManageWorkReportService;

@Service
@Transactional
public class ManageWorkReportServiceImpl implements ManageWorkReportService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public ManagerWorkReport queryById(Integer id)
    {
        return (ManagerWorkReport)baseDAO.loadById(ManagerWorkReport.class, id);
    }
    
    @Override
    public List queryWorkReportByPage(ManagerWorkReport sdObj, int page, int rows)
    {
        int firstPage = rows * (page - 1);
        int lastPage = rows;
        String sqlStr = "";
        if (StringUtils.isNotBlank(sdObj.getName()))
        {
            sqlStr = sqlStr + " and c_name like '%" + sdObj.getName() + "%'";
        }
        String sql = "select * from busi_manager_work_report au where au.i_receiver_id = " + sdObj.getReceiverId() + sqlStr + " order by au.d_date desc limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public List<ManagerWorkReport> queryAllMWorkReports(ManagerWorkReport mWorkReport)
    {
        String sqlStr = "";
        if ((!"".equals(mWorkReport.getName())) && (mWorkReport.getName() != null))
        {
            sqlStr = sqlStr + " and name like '%" + mWorkReport.getName() + "%'";
        }
        String hql = "from ManagerWorkReport where receiverId = " + mWorkReport.getReceiverId() + sqlStr;
        return baseDAO.query(hql);
    }
}