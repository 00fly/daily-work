package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.MenuButton;
import com.qx.cn.service.MenuButtonService;

@Service
@Transactional
public class MenuButtonServiceImpl implements MenuButtonService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public Boolean saveMenuButtin(MenuButton menuButton)
    {
        baseDAO.saveOrUpdate(menuButton);
        return Boolean.valueOf(true);
    }
    
    @Override
    public List<MenuButton> queryMenuButtonByCode(String menuCode)
    {
        String hql = "from MenuButton mb where mb.menuId = '" + menuCode + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteMenuButton(String menuId)
    {
        String hql = "delete from MenuButton mb where mb.menuId = '" + menuId + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public List<MenuButton> queryButtonsByMenuCode(String menuCode)
    {
        String hql = "from MenuButton butMenu where butMenu.menuId = '" + menuCode + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean deleteButtonMenu(String buttonId)
    {
        String hql = "delete from MenuButton mb where mb.buttonId = " + Long.valueOf(buttonId);
        baseDAO.deleteByHql(hql);
        return true;
    }
}