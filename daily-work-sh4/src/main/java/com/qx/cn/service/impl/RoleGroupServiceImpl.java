package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.RoleGroup;
import com.qx.cn.service.RoleGroupService;

@Service
@Transactional
public class RoleGroupServiceImpl implements RoleGroupService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public boolean deleteRoleGroup(Long groupId)
    {
        baseDAO.deleteById(RoleGroup.class, groupId);
        return true;
    }
    
    @Override
    public RoleGroup loadRoleGroupById(Long groupId)
    {
        return (RoleGroup)baseDAO.loadById(RoleGroup.class, groupId);
    }
    
    @Override
    public List<RoleGroup> queryAllRoleGroups()
    {
        String hql = "from RoleGroup ";
        return baseDAO.query(hql);
    }
    
    @Override
    public List queryRoleGroupsByPage(int page, int rows)
    {
        int firstPage = rows * (page - 1);
        int lastPage = rows * page - 1;
        String sql = "select * from sys_role_group au where au.i_group_id is not null limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public boolean saveRoleGroup(RoleGroup reRoleGroup)
    {
        baseDAO.saveOrUpdate(reRoleGroup);
        return true;
    }
}