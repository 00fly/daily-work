package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.ConfigData;

public interface ConfigDataService
{
    public List<ConfigData> queryConfigByPar();
    
    public boolean saveConfig(ConfigData configData);
    
    public boolean deleteConfigByCode(String dataCode);
    
    public ConfigData loadConfigByCode(String dataCode);
    
    public List<ConfigData> querySonConfigByParCode(String dataCode, int type);
    
    public List<ConfigData> queryConfigByDataDomain(String dataDomain, int type);
    
    public void updateDataByCode(String dataDomain, String dataCode);
    
    public ConfigData loadConfigByDOMAndOrder(String dataDomain);
}