package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.Button;

public interface ButtonService
{
    public List queryButtonsByPage(int page, int rows);
    
    public List<Button> queryAllButtons();
    
    public boolean saveButton(Button button);
    
    public boolean deleteButton(Long buttonId);
    
    public Button loadButtonById(Long buttonId);
}