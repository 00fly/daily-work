package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.MenuTabs;
import com.qx.cn.service.MenuTabService;

@Service
@Transactional
public class MenuTabServiceImpl implements MenuTabService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public boolean deleteMenuTab(String menuId)
    {
        String hql = "delete from MenuTabs mt where mt.menuId = '" + menuId + "'";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public boolean deleteTabMenu(Long tabId)
    {
        String hql = "delete from MenuTabs mt where mt.tabId = " + tabId;
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public List<MenuTabs> queryMenuTabByCode(String menuCode)
    {
        String hql = "from MenuTabs mt where mt.menuId = '" + menuCode + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public Boolean saveMenuTab(MenuTabs menuTab)
    {
        baseDAO.saveOrUpdate(menuTab);
        return Boolean.valueOf(true);
    }
}