package com.qx.cn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.WorkSendGroup;
import com.qx.cn.service.WorkSendGroupService;

@Service
@Transactional
public class WorkSendGroupServiceImpl implements WorkSendGroupService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List queryWorkSendGroupByPage(WorkSendGroup sdObj, int page, int rows)
    {
        int firstPage = rows * (page - 1);
        int lastPage = rows;
        String sqlStr = "";
        if ((!"".equals(sdObj.getGroupName())) && (sdObj.getGroupName() != null))
        {
            sqlStr = sqlStr + " and c_group_name like '%" + sdObj.getGroupName() + "%'";
        }
        String sql = "select * from sys_work_send_group au where au.i_id is not null " + sqlStr + "  limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public boolean deleteWorkSendGroup(int id)
    {
        baseDAO.deleteById(WorkSendGroup.class, Integer.valueOf(id));
        return true;
    }
    
    @Override
    public WorkSendGroup loadWorkSendGroupById(int id)
    {
        return (WorkSendGroup)baseDAO.loadById(WorkSendGroup.class, Integer.valueOf(id));
    }
    
    @Override
    public List<WorkSendGroup> queryAllWorkSendGroups(WorkSendGroup sdObj)
    {
        String sqlStr = "";
        if ((!"".equals(sdObj.getGroupName())) && (sdObj.getGroupName() != null))
        {
            sqlStr = sqlStr + " and groupName like '%" + sdObj.getGroupName() + "%'";
        }
        String hql = "from WorkSendGroup where groupName is not null " + sqlStr;
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean saveWorkSendGroup(WorkSendGroup reWorkSendGroup)
    {
        baseDAO.saveOrUpdate(reWorkSendGroup);
        return true;
    }
    
    public WorkSendGroup queryAllWorkSendGroupByDate(String groupName)
    {
        String hql = "from WorkSendGroup where groupName = '" + groupName + "'";
        return (WorkSendGroup)baseDAO.loadByHql(hql);
    }
    
    @Override
    public List<WorkSendGroup> queryIsNotUserByUserId(Long userId)
    {
        String hql = "from WorkSendGroup where enabled = 1 and (workerIds like '" + userId + ",%' or workerIds like '%," + userId + ",%' or workerIds like '%," + userId + ",')";
        return baseDAO.query(hql);
    }
}