package com.qx.cn.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.ManagerWorkReport;
import com.qx.cn.model.SendDate;
import com.qx.cn.model.SendEmail;
import com.qx.cn.model.SendTime;
import com.qx.cn.model.User;
import com.qx.cn.service.SendEmailService;
import com.qx.cn.tool.DateForm;

@Service
@Transactional
public class SendEmailServiceImpl implements SendEmailService
{
    @Autowired
    BaseDAO baseDAO;
    
    /**
     * @param page
     * @param rows
     * @return
     */
    @Override
    public List querySendEmailByPage(int page, int rows)
    {
        int firstPage = rows * (page - 1);
        int lastPage = rows;
        String sql = "select * from sys_send_email au where au.i_id is not null limit " + firstPage + "," + lastPage;
        return baseDAO.queryBySql(sql);
    }
    
    /**
     * @param id
     * @return
     */
    @Override
    public boolean deleteSendEmail(int id)
    {
        baseDAO.deleteById(SendEmail.class, Integer.valueOf(id));
        return true;
    }
    
    /**
     * @param id
     * @return
     */
    @Override
    public SendEmail loadSendEmailById(int id)
    {
        return (SendEmail)baseDAO.loadById(SendEmail.class, Integer.valueOf(id));
    }
    
    /**
     * @return
     */
    @Override
    public List<SendEmail> queryAllSendEmails()
    {
        String hql = "from SendEmail ";
        return baseDAO.query(hql);
    }
    
    /**
     * @param reSendEmail
     * @return
     */
    @Override
    public boolean saveSendEmail(SendEmail reSendEmail)
    {
        baseDAO.saveOrUpdate(reSendEmail);
        return true;
    }
    
    /**
     * @return
     */
    @Override
    public List<SendTime> querySendTimes()
    {
        return baseDAO.query(SendTime.class);
    }
    
    /**
     * @param date
     * @return
     */
    @Override
    public boolean checkIsSendDate(String date)
    {
        List<SendDate> list = baseDAO.query("from SendDate where name = ?", date);
        return !list.isEmpty();
    }
    
    /**
     * @param date
     * @return
     */
    @Override
    public SendDate queryBySendDate(String date)
    {
        List<SendDate> list = baseDAO.query("from SendDate where name = ?", date);
        if (list.isEmpty())
        {
            return null;
        }
        return list.get(0);
    }
    
    /**
     * @param date
     * @return
     */
    @Override
    public boolean notSend(String date)
    {
        String hql = " from ManagerWorkReport where date = ? ";
        List list = baseDAO.query(hql, date);
        return list.isEmpty();
    }
    
    /**
     * @param reportCode
     * @return
     */
    @Override
    public List<?> queryWorKReportItem(String reportCode)
    {
        return baseDAO.query("from WorkReportItem where reportCode = ?", reportCode);
    }
    
    /**
     * @return
     */
    @Override
    public List<?> querySendEmail()
    {
        return baseDAO.query(SendEmail.class);
    }
    
    /**
     * @return
     */
    @Override
    public List<?> queryWorkSendGroup()
    {
        return baseDAO.query("from WorkSendGroup where enabled = 1");
    }
    
    /**
     * @param id
     * @return
     */
    @Override
    public User loadUserById(Integer id)
    {
        return (User)baseDAO.loadById(User.class, id);
    }
    
    /**
     * @param id
     * @param status
     */
    @Override
    public void updateWorkSendGroupTimeTip(int id, int status)
    {
        String hql = "update WorkSendGroup set timeTip = ? where id = ? ";
        baseDAO.update(hql, status, id);
    }
    
    /**
     * @param reUserId
     * @param cWorkerIds
     * @param groupName
     * @param type
     */
    @Override
    public void insertManageWorkReport(Integer reUserId, String cWorkerIds, String groupName, String type)
    {
        ManagerWorkReport managerWorkReport = new ManagerWorkReport();
        if (type.contains("day"))
        {
            managerWorkReport.setName(DateForm.SimpleDate(new Date()) + "日报-" + groupName);
            managerWorkReport.setDate(DateForm.SimpleDate(new Date()));
            managerWorkReport.setUserIds(cWorkerIds);
            managerWorkReport.setReceiverId(reUserId);
            baseDAO.saveOrUpdate(managerWorkReport);
        }
        if (type.contains("week"))
        {
            managerWorkReport.setName(DateForm.SimpleDate(new Date()) + "周报-" + groupName);
            managerWorkReport.setDate(DateForm.SimpleDate(new Date()));
            managerWorkReport.setUserIds(cWorkerIds);
            managerWorkReport.setReceiverId(reUserId);
            baseDAO.saveOrUpdate(managerWorkReport);
        }
    }
}