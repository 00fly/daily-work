package com.qx.cn.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.OrgInfo;
import com.qx.cn.model.User;
import com.qx.cn.service.OrgInfoService;
import com.qx.cn.tool.OrgInfoComparator;
import com.qx.cn.tool.TreeNode;
import com.qx.cn.vo.OrgInfoVO;

@Service
@Transactional
public class OrgInfoServiceImpl implements OrgInfoService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List<OrgInfo> queryOrgList()
    {
        String hql = "from OrgInfo";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<OrgInfo> queryOrgListByAsc(String parCode)
    {
        String hql = "from OrgInfo o where o.orgCode like '" + parCode + "%' order by o.orgCode asc ";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean saveOrgInfo(OrgInfo org)
    {
        baseDAO.saveOrUpdate(org);
        return true;
    }
    
    @Override
    public List<OrgInfo> queryOrgListToUserByAsc(String parCode)
    {
        String hql = "from OrgInfo o where o.orgCode like '" + parCode + "___' order by o.orgCode asc ";
        return baseDAO.query(hql);
    }
    
    @Override
    public OrgInfo loadOrgByCode(String parCode)
    {
        String hql = "from OrgInfo o where o.orgCode = '" + parCode + "'";
        return (OrgInfo)baseDAO.loadByHql(hql);
    }
    
    @Override
    public List<OrgInfoVO> queryOrgListTreeGrid(OrgInfo org)
    {
        List sonOrgs = new ArrayList();
        String hql = null;
        if ((!"".equals(org.getOrgCode())) && (org.getOrgCode() != null))
        {
            hql = "from OrgInfo o where o.orgCode like '" + org.getOrgCode() + "___' order by o.orgCode asc";
        }
        else
        {
            hql = "from OrgInfo o where o.orgCode like '1___' order by o.orgCode asc";
        }
        List<OrgInfo> orgInfos = baseDAO.query(hql);
        for (OrgInfo orgInfo : orgInfos)
        {
            OrgInfoVO orgVO = new OrgInfoVO();
            orgVO.setId(orgInfo.getOrgCode());
            orgVO.setOrgId(orgInfo.getOrgId());
            orgVO.setOrgCode(orgInfo.getOrgCode());
            orgVO.setOrgName(orgInfo.getOrgName());
            orgVO.setOrgDuty(orgInfo.getOrgDuty());
            orgVO.setEnabled(orgInfo.getEnabled());
            String olStr = "";
            if (orgInfo.getOrgLeader() != null)
            {
                String uhql = "from User u where u.userId = " + orgInfo.getOrgLeader();
                User user = (User)baseDAO.loadByHql(uhql);
                olStr = user.getUserName() + "&nbsp;<img src='/qx/images/icon/can.png' title='已设置'>";
            }
            else
            {
                olStr = "未设置&nbsp;<img src='/qx/images/icon/not.png' title='未设置'>";
            }
            orgVO.setOrgLeader(orgInfo.getOrgLeader());
            orgVO.setOptHeader(olStr);
            sonOrgs.add(orgVO);
        }
        return queryOrgsBysons(sonOrgs);
    }
    
    private List<OrgInfoVO> queryOrgsBysons(List<OrgInfoVO> sonOrgs)
    {
        List orgs = new ArrayList();
        if ((sonOrgs != null) && (sonOrgs.size() > 0))
        {
            for (OrgInfoVO son : sonOrgs)
            {
                OrgInfoVO parInfo = new OrgInfoVO();
                if (sonOrgs.size() > 0)
                {
                    BeanUtils.copyProperties(son, parInfo);
                }
                if (countChildren(son.getOrgCode()).size() > 0)
                {
                    parInfo.setState("closed");
                }
                orgs.add(parInfo);
            }
        }
        return orgs;
    }
    
    private List countChildren(String parCode)
    {
        String sql = "select * from sys_org_info org where org.c_org_code like '" + parCode + "___'";
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public boolean deleteOrgListByCode(String orgCode)
    {
        String hql = "delete from OrgInfo org where org.orgCode like '" + orgCode + "%'";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public List<TreeNode> queryComTree(OrgInfo orgInfo, boolean b)
    {
        String hql = "from OrgInfo o where o.orgCode = '1' order by o.orgCode asc ";
        if ((!"".equals(orgInfo.getOrgCode())) && (orgInfo.getOrgCode() != null))
        {
            hql = "from OrgInfo o where o.orgCode like '" + orgInfo.getOrgCode() + "___' order by o.orgCode asc";
        }
        List<OrgInfo> orgInfos = baseDAO.query(hql);
        List tree = new ArrayList();
        for (OrgInfo org : orgInfos)
        {
            tree.add(tree(org, b));
        }
        return tree;
    }
    
    private TreeNode tree(OrgInfo org, boolean b)
    {
        TreeNode node = new TreeNode();
        node.setId(org.getOrgCode());
        node.setText(org.getOrgName());
        if (countChildren(org.getOrgCode()).size() > 0)
        {
            node.setState("closed");
            if (b)
            {
                List<OrgInfo> orgs = new ArrayList(queryOrgListToUserByAsc(org.getOrgCode()));
                Collections.sort(orgs, new OrgInfoComparator());
                List children = new ArrayList();
                for (OrgInfo r : orgs)
                {
                    TreeNode tn = tree(r, true);
                    children.add(tn);
                }
                node.setChildren(children);
            }
        }
        return node;
    }
    
    @Override
    public List<OrgInfo> queryAllOrgList()
    {
        String hql = "from OrgInfo o where o.orgCode is not null and o.enabled = 1 order by o.orgCode asc ";
        return baseDAO.query(hql);
    }
    
    @Override
    public OrgInfo queryByName(String orgName)
    {
        String hql = "from OrgInfo o where orgName = " + orgName;
        return (OrgInfo)baseDAO.loadByHql(hql);
    }
    
    @Override
    public List<OrgInfo> queryAllOrgListBydepartCode(String departCode)
    {
        String hql = "from OrgInfo o where orgCode = '" + departCode + "'";
        return baseDAO.query(hql);
    }
    
    @Override
    public OrgInfo loadOrgListBydepartCode(String departCode)
    {
        String hql = "from OrgInfo o where orgCode = '" + departCode + "'";
        return (OrgInfo)baseDAO.loadByHql(hql);
    }
}