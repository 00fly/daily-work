package com.qx.cn.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qx.cn.dao.BaseDAO;
import com.qx.cn.model.Menu;
import com.qx.cn.service.MenuService;
import com.qx.cn.tool.MenuComparator;
import com.qx.cn.tool.TreeNode;
import com.qx.cn.vo.MenuVO;

@Service
@Transactional
public class MenuServiceImpl implements MenuService
{
    @Autowired
    BaseDAO baseDAO;
    
    @Override
    public List<MenuVO> queryMenusAndButTreeGrid(MenuVO meVO)
    {
        // List sonMenus = new ArrayList();
        return null;
    }
    
    @Override
    public List<MenuVO> queryTreeGrid(Menu me)
    {
        List sonMenus = new ArrayList();
        String hql = null;
        if ((!"".equals(me.getMenuCode())) && (me.getMenuCode() != null))
        {
            hql = "from Menu o where o.menuCode like '" + me.getMenuCode() + "___' order by menuSort asc";
        }
        else
        {
            hql = "from Menu o where o.menuCode like '1___' order by menuSort asc ";
        }
        List<Menu> Menus = baseDAO.query(hql);
        for (Menu menu : Menus)
        {
            MenuVO menuVO = new MenuVO();
            menuVO.setId(menu.getMenuCode());
            menuVO.setMenuId(menu.getMenuId());
            menuVO.setMenuName(menu.getMenuName());
            menuVO.setMenuCode(menu.getMenuCode());
            menuVO.setMenuURL(menu.getMenuURL());
            menuVO.setEnabled(menu.getEnabled());
            menuVO.setMenuSort(menu.getMenuSort());
            String hql1 = "from Menu o where o.menuCode = '" + menu.getMenuCode().substring(0, menu.getMenuCode().length() - 3) + "'";
            Menu menu2 = (Menu)baseDAO.loadByHql(hql1);
            menuVO.setPmenuName(menu2.getMenuName());
            menuVO.setIsLeafMenu(menu.getIsLeafMenu());
            menuVO.setParCode(menu.getMenuCode().substring(0, menu.getMenuCode().length() - 3));
            sonMenus.add(menuVO);
        }
        return queryMenusBySons(sonMenus);
    }
    
    private List<MenuVO> queryMenusBySons(List<MenuVO> sonMenus)
    {
        List mes = new ArrayList();
        if ((sonMenus != null) && (sonMenus.size() > 0))
        {
            for (MenuVO son : sonMenus)
            {
                MenuVO parInfo = new MenuVO();
                if (sonMenus.size() > 0)
                {
                    BeanUtils.copyProperties(son, parInfo);
                }
                if (countChildren(son.getMenuCode()).size() > 0)
                {
                    parInfo.setState("closed");
                }
                mes.add(parInfo);
            }
        }
        return mes;
    }
    
    @Override
    public List<TreeNode> queryComTree(Menu menu, boolean b)
    {
        String hql = "from Menu m where m.menuCode = '1' order by menuSort asc ";
        if ((!"".equals(menu.getMenuCode())) && (menu.getMenuCode() != null))
        {
            hql = "from Menu m where m.Menu like '" + menu.getMenuCode() + "___' order by menuSort asc ";
        }
        List<Menu> menus = baseDAO.query(hql);
        List tree = new ArrayList();
        for (Menu menu2 : menus)
        {
            tree.add(tree(menu2, b));
        }
        return tree;
    }
    
    private TreeNode tree(Menu menu2, boolean b)
    {
        TreeNode node = new TreeNode();
        node.setId(menu2.getMenuCode());
        node.setText(menu2.getMenuName());
        if (countChildren(menu2.getMenuCode()).size() > 0)
        {
            node.setState("closed");
            if (b)
            {
                List<Menu> mes = new ArrayList(queryMenuListByAsc(menu2.getMenuCode()));
                Collections.sort(mes, new MenuComparator());
                List children = new ArrayList();
                for (Menu m : mes)
                {
                    TreeNode tn = tree(m, true);
                    children.add(tn);
                }
                node.setChildren(children);
            }
        }
        return node;
    }
    
    @Override
    public List<Menu> queryMenuListByAsc(String parCode)
    {
        String hql = "from Menu menu where menu.menuCode like '" + parCode + "___' order by menuCode asc ";
        return baseDAO.query(hql);
    }
    
    private List<Menu> countChildren(String parCode)
    {
        String sql = "select * from sys_menu menu where menu.c_menu_code like '" + parCode + "___'";
        return baseDAO.queryBySql(sql);
    }
    
    @Override
    public List<Menu> queryAllMenuListByAsc(String parCode)
    {
        String hql = "from Menu menu where menu.menuCode like '" + parCode + "%' order by menuCode asc ";
        return baseDAO.query(hql);
    }
    
    @Override
    public boolean saveMenu(Menu menu)
    {
        baseDAO.saveOrUpdate(menu);
        return true;
    }
    
    @Override
    public Menu loadMenuByCode(String menuCode)
    {
        String hql = "from Menu m where m.menuCode = '" + menuCode + "' ";
        return (Menu)baseDAO.loadByHql(hql);
    }
    
    @Override
    public boolean deleteMenuByCode(String menuCode)
    {
        String hql = "delete from Menu menu where menu.menuCode like '" + menuCode + "%'";
        baseDAO.deleteByHql(hql);
        return true;
    }
    
    @Override
    public boolean updateMenuInfo(MenuVO menu)
    {
        Menu me = (Menu)baseDAO.loadById(Menu.class, menu.getMenuId());
        BeanUtils.copyProperties(menu, me);
        if (countChildren(menu.getMenuCode()).size() > 0)
        {
            Menu mes = (Menu)baseDAO.loadByHql("from Menu menu where menu.menuCode = '" + menu.getMenuCode().substring(0, menu.getMenuCode().length() - 3) + "'");
            if (mes != null)
            {
                if (isDown(me.getMenuCode(), mes.getMenuCode()))
                {
                    List menus = baseDAO.query("from Menu menu where menu.menuCode like '" + me.getMenuCode() + "%' order by menuSort asc ");
                    if ((menus != null) && (menus.size() > 0))
                    {
                        Menu localMenu1;
                        for (Iterator localIterator = menus.iterator(); localIterator.hasNext(); localMenu1 = (Menu)localIterator.next())
                        {
                            ;
                        }
                    }
                }
                baseDAO.saveOrUpdate(me);
            }
        }
        return true;
    }
    
    private boolean isDown(String code, String pCode)
    {
        if (code.length() > 3)
        {
            if (pCode.equals(code.substring(0, code.length() - 3)))
            {
                return true;
            }
            return isDown(code.substring(0, code.length() - 3), pCode);
        }
        return false;
    }
    
    @Override
    public Menu loadMenuByURL(String menuURL)
    {
        String hql = "from Menu m where m.menuURL like '%" + menuURL + "%'";
        return (Menu)baseDAO.loadByHql(hql);
    }
    
    @Override
    public Menu loadMenuByType(String menuType)
    {
        String hql = "from Menu m where m.menuURL like '%" + menuType + "%'";
        return (Menu)baseDAO.loadByHql(hql);
    }
    
    @Override
    public List<Menu> queryMenuListByRoleAsc(String parCode)
    {
        String hql = "from Menu menu where menu.menuCode like '" + parCode + "___' order by menuSort asc ";
        return baseDAO.query(hql);
    }
    
    @Override
    public List<Menu> queryMenuByType(String menuType)
    {
        return null;
    }
    
    @Override
    public List queryAllMenuURL()
    {
        return baseDAO.queryBySql("select distinct(c_menu_url) from sys_menu where i_enabled=1 and c_menu_url is not null");
    }
}