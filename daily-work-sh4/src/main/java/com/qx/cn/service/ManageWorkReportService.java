package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.ManagerWorkReport;

public interface ManageWorkReportService
{
    public ManagerWorkReport queryById(Integer id);
    
    public List queryWorkReportByPage(ManagerWorkReport sdObj, int page, int rows);
    
    public List<ManagerWorkReport> queryAllMWorkReports(ManagerWorkReport mWorkReport);
}