package com.qx.cn.service;

import java.util.List;

import com.qx.cn.model.Menu;
import com.qx.cn.tool.TreeNode;
import com.qx.cn.vo.MenuVO;

public interface MenuService
{
    public List<MenuVO> queryTreeGrid(Menu menu);
    
    public List<TreeNode> queryComTree(Menu menu, boolean bool);
    
    public List<Menu> queryMenuListByAsc(String parCode);
    
    public List<Menu> queryAllMenuListByAsc(String parCode);
    
    public boolean saveMenu(Menu menu);
    
    public Menu loadMenuByCode(String menuCode);
    
    public boolean deleteMenuByCode(String menuCode);
    
    public boolean updateMenuInfo(MenuVO menu);
    
    public List<MenuVO> queryMenusAndButTreeGrid(MenuVO meVO);
    
    public Menu loadMenuByURL(String menuURL);
    
    public Menu loadMenuByType(String menuType);
    
    public List<Menu> queryMenuListByRoleAsc(String parCode);
    
    public List<Menu> queryMenuByType(String menuType);
    
    public List queryAllMenuURL();
}